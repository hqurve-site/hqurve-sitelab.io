\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Writing Academic papers}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% complex conjugate
\def\conj#1{\overbar{#1}}

% Title omitted
NOTE:
The original written notes are available \myautoref[{here}]{develop--fun:extra-notes:attachment--Scanned-20231230-1103.pdf}.
This page mostly consist of the typed version of these notes for easier discovery.

The webinar was at held by the University of Technology and Applied Sciences (Ibri, Oman)
on the 26th of December 2023 and the keynote speaker was

\begin{custom-quotation}[{}][{}]
Prof. Dr. Pradeep G. Siddheshwar

Director: R \& D Cell and Centre for Mathematical Needs, CHRIST University, Bangalore, India.
\end{custom-quotation}

The webinar was scheduled for 12-1pm oman time (4am-5am local) but finished at 1:30 oman time (5:30am local).

\begin{admonition-tip}[{}]
Research means Re (repetition) search (finding)
\end{admonition-tip}

\section{Broad tips}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---broad-tips}

Read good papers from top-class journals. Good and top-class are subjective and based
on your field of interest.

\begin{itemize}
\item Look for British authored papers to help with English. Also look at references
\end{itemize}

Identify (hard-copy) papers in your area of research and note what you observe

\begin{itemize}
\item Ensure you google enough about your topic and note prominent authors.
    Also make sure that you do a good survey on your area of interest.
\item Find problems and things you like in papers. This helps you identify where you can contribute to
    the existing literature.
\item Ensure to go through references too
\end{itemize}

Other important tips:
* Avoid duplicate symbols (ie dont use \(\mu\) to represent two different quantities even if they are in
different parts of the paper)
* You must refer to the most recent literature
* Avoid self-citation

\section{Parts of paper}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---parts-of-paper}

Note that this order is the recommended order of writing (not presentation)

\subsection{Introduction}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---introduction}

In the introduction, you compare your work to the existing literature.

\begin{itemize}
\item Before identify shortcomings in existing literature and see what you can contribute.
\item Identify papers similar to your work. This allows you to determine a suitable layout for your introduction.
\item Use italics, bold and other techniques to attract the reader to keywords
\item When saying another paper is deficient, say it nicely so to not offend.
\item Identify new/innovative parts in your paper
\item Paper must be self contained and all units must be specified.
\end{itemize}

\subsection{Research and discussion}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---research-and-discussion}

\begin{itemize}
\item At this point you may want to reword the title
\item Identify and explain new things in the paper, one after another.
\end{itemize}

\subsection{Conclusion}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---conclusion}

The intent is different from the intro and discussion.

\begin{itemize}
\item It must consist entirely of new things introduced by your paper.
\item Never rush writing and take your time.
\end{itemize}

\subsection{Abstract}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---abstract}

\begin{itemize}
\item Includes everything
\item Do not repeat sentences from other parts in the paper.
\end{itemize}

\subsection{Title}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---title}

\begin{itemize}
\item Look at what you want to change after writing previous parts
\item Choose your title based on existing title. But be sure that you stand out.
\end{itemize}

\subsection{Keywords}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---keywords}

Keywords are based on what you search on google to find relevant articles when performing your research.
Make sure if you wanted to find this paper, you will use these keywords

\begin{itemize}
\item If you choose the right keywords, it increases the probability that someone sees it when searching.
\item Use only common terminology and must be based on the discussion and abstract.
\end{itemize}

\section{Publishing tips}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---publishing-tips}

\subsection{How to write acceptably to referee}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---how-to-write-acceptably-to-referee}

Look at the editorial board.
If there are people in your area of research, they are likely to be the one to review your paper.

Look at the papers from the journal and papers from some possible members of the editorial board.
If there are relevant ones, try to include them as references in your paper.

This is the ``human element''. If they realize that their paper is relevant and don't see it addressed,
they may feel unhappy. (From a more objective standpoint, they will be most familiar with what they have
written, so they may have some concerns based on their paper, so it is best to address it before it comes up).

\subsection{Identifying journals}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---identifying-journals}

\begin{itemize}
\item What are Q1, Q2 and Q3 journals?
    
    According to \url{https://www.mondragon.edu/en/web/biblioteka/publications-impact-indexes},
    these are the quartiles of the journals' ranking.
\item Look at the metrics of the different journals which you want to publish to.
\item You may use Web of Science to help identify journals and articles.
\item Be weary of journals who will publish as long as you give them money. In particular
    be weary of some open access journals since publishing open access requires payment.
    
    \begin{admonition-remark}[{}]
    I find there is a great conflict of interest. One one hand, you want everyone to read what
    you have researched. But on the other, it requires large payments to publish.
    \end{admonition-remark}
\end{itemize}

\subsection{Feedback}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---feedback}

First the pre-editing team will assess your paper for english, template usage and other general guidelines.

\begin{itemize}
\item Ensure your plagiarism is 15-20\% max.
\item Be aware of replacements made by Grammarly. Get someone to double check our work.
\item Read papers as a third person to try to identify issues.
\end{itemize}

\section{Meta comments}
\label{develop--fun:extra-notes:page--writing-academic-papers.adoc---meta-comments}

\begin{itemize}
\item Apparently starting recording in teams will mute everyone and stop video/screen share.
\item The keynote speaker said "Yeahh, tell me" just like Dr Rao.
\end{itemize}
\end{document}
