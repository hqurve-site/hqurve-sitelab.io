\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Mobius inversion}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
The \emph{mobius function}, \(\mu\), is the unqiue arithmetic function such that

\begin{equation*}
g(n) = \sum_{d|n} f(d) \iff f(n) = \sum_{d|n}\mu\left(\frac{n}{d}\right)g(d)
\end{equation*}

where \(g\) and \(f\) are arithmetic functions. This above equivalence is known as \emph{mobius inversion}.
We would determine the behavious of \(\mu\) and prove that it exhibits this property below.

\section{The value of the mobius function}
\label{develop--math2400:arithmetic-functions:page--mobius-inversion.adoc---the-value-of-the-mobius-function}

Suppose that such a function exists and let \(g(n) = 1\), then, its accompanying function \(f\) is given by

\begin{equation*}
f(n) = \sum_{d|n}\mu\left(\frac{n}{d}\right) = \sum_{d|n} \mu(d)
\end{equation*}

then, when \(n =1\), we get that

\begin{equation*}
1 = g(1) = \sum_{d|n} f(n) = f(1) = \mu(1)
\end{equation*}

then by induction on the number of terms in its unique factorization,

\begin{equation*}
f(n) = \sum_{d|n}\mu(d) = \begin{cases}
    1 &\quad\text{if } n=1\\
    0 &\quad\text{otherwise}
\end{cases}
\end{equation*}

This is the main fact which we use to uniquely define \(\mu\) and show that it satisfies the necessary conditions.

\subsection{Definition of \(\mu\)}
\label{develop--math2400:arithmetic-functions:page--mobius-inversion.adoc---definition-of-latex-backslash-latex-backslashmu-latex-backslash}

Now, let \(\{p_i\}\) be a set of primes. Then

\begin{equation*}
0 = f(p) = \mu(p) + \mu(1) \implies \mu(p) = -1
\end{equation*}

Hence,

\begin{equation*}
0 = \sum_{i=0}^k \mu(p^k) = \sum_{i=2}^k \mu(p^k)
\end{equation*}

and by induction on \(k\)

\begin{equation*}
\mu(p^k) =
\begin{cases}
    1 &\quad\text{if } k=0\\
    -1 &\quad\text{if } k=1\\
    0 &\quad\text{if } k \geq 2
\end{cases}
\end{equation*}

Next, we want to show that \(\mu\) is multiplicative. We would do this by showing \(\forall ab=n\) and \(\gcd(a,b)=1\),
\(\mu(ab) = \mu(a)\mu(b)\) by induction on \(n\).

When \(n=1\), the result is trivial since the only product is \(a=1\) and \(b=1\). Next, suppose that \(\mu(ab) = \mu(a)\mu(b)\)
for all \(ab \leq n\) and \(\gcd(a,b)=1\). Then, consider arbitrary \(ab = N+1\) where \(\gcd(a,b)=1\) and we get

\begin{equation*}
\begin{aligned}
0 &= f(ab) = \sum_{k|ab} \mu(k)
\\&= \mu(ab) + \sum_{k|ab, k < ab}\mu(k)
\\&= \mu(ab) + \sum_{a'|a, b'|b, k < ab}\mu(a'b')
\\&= \mu(ab) + \sum_{a'|a, b'|b, k < ab}\mu(a')\mu(b')
\\&= \mu(ab) - \mu(a)\mu(b) + \mu(a)\mu(b) + \sum_{a'|a, b'|b, k < ab}\mu(a')\mu(b')
\\&= \mu(ab) - \mu(a)\mu(b) + \sum_{a'|a, b'|b}\mu(a')\mu(b')
\\&= \mu(ab) - \mu(a)\mu(b) + \left(\sum_{a'|a} \mu(a')\right)\left(\sum_{b'|b}\mu(b')\right)
\\&= \mu(ab) - \mu(a)\mu(b) + f(a)f(b)
\\&= \mu(ab) - \mu(a)\mu(b)
\end{aligned}
\end{equation*}

Notice that at least one of \(f(a)\), \(f(b)\) is \(0\) since \(ab > 1\). Then we get the desired result, and by
combining with the expression for \(\mu(p^k)\), we get

\begin{equation*}
\mu(n) = \begin{cases}
    (-1)^k \quad&\text{if } n \text{ is the product of } k \text{ distinct primes}\\
    0 \quad&\text{otherwise}
\end{cases}
\end{equation*}

Perhaps this should be the definition, but I rather the way how I showed its uniqueness.

\subsection{Satisfiaction of requirements}
\label{develop--math2400:arithmetic-functions:page--mobius-inversion.adoc---satisfiaction-of-requirements}

We then need to show that this definition satisfies the necessary requirements.

First, let \(g(n) = \sum_{d|n} f(d)\), then

\begin{equation*}
\begin{aligned}
\sum_{d|n} \mu\left(\frac{n}{d}\right)g(d)
&= \sum_{dk=n} \mu(k)g(d)
\\&= \sum_{dk=n} \mu(k)\sum_{e|d}f(e)
\\&= \sum_{dk=n} \mu(k)\sum_{ec=d}f(e)
\\&= \sum_{eck=n} \mu(k)f(e)
\\&= \sum_{em=n}f(e)\sum_{ck=m} \mu(k)
\\&= \sum_{em=n}f(e)\sum_{k|m} \mu(k)
\\&= f(n)
\end{aligned}
\end{equation*}

since \(\sum_{k|m}\mu(k) = 0\) if \(e <n\).

Next, let \(f(n) = \sum_{d|n}\mu\left(\frac{n}{d}\right)g(d)\), then

\begin{equation*}
\begin{aligned}
\sum_{d|n}f(d)
&= \sum_{d|n}\sum_{e|d}\mu\left(\frac{d}{e}\right)g(e)
\\&= \sum_{d|n}\sum_{ek=d}\mu(k)g(e)
\\&= \sum_{cek=n}\mu(k)g(e)
\\&= \sum_{em=n}g(e)\sum_{ck=m}\mu(k)
\\&= \sum_{em=n}g(e)\sum_{k|m}\mu(k)
\\&= g(n)
\end{aligned}
\end{equation*}

since \(\sum_{k|m}\mu(k) =0\) if \(e<n\).

\section{Sum of Euler totient function}
\label{develop--math2400:arithmetic-functions:page--mobius-inversion.adoc---sum-of-euler-totient-function}

For all \(n \in \mathbb{N}\)

\begin{equation*}
n = \sum_{d|n} \phi(d)
\end{equation*}

and as a result, we can use mobius inversion to deduce that

\begin{equation*}
\phi(n) = \sum_{d|n} \mu\left(\frac{n}{d}\right) d
\end{equation*}

\begin{admonition-remark}[{}]
Utilizing this method to compute values of \(\phi\) seems weird since you still
need to know the factorization of \(n\) (well at least its prime divisors which requires the same amount of work),
in which case you can use a simpler formula.
\end{admonition-remark}

\begin{example}[{Proof using multiplicity}]
Let \(f(n) = \sum_{d|n}\phi(d)\). Then, since \(\phi\) is
multiplicative by using \myautoref[{this result}]{develop--math2400:arithmetic-functions:page--index.adoc---multiplicative}, we get that
\(f\) is also multiplicative. Now, all that remains is to show that \(f(p^k) = p^k\)
where \(p\) is a prime. However, this is easily obtained since

\begin{equation*}
f(p^k) = \sum_{i=0}^k \phi(p^i) = 1 + \sum_{i=1}^k (p-1)p^i = 1 + (p-1)\frac{p^k -1}{p-1}
\end{equation*}

and we are done.
\end{example}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
This was the proof presented in class
\end{admonition-note}

Consider arbirary \(n \in \mathbb{N}\), then we define

\begin{equation*}
X_d = \{a \in J_n \ | \ \gcd(a,n) = d\}
\end{equation*}

That is \(X_d\) is the preimage of \(d\) in the function \(k \mapsto \gcd(k,n)\). Then,
notice that \(\{X_d\}\) partitions \(J_n\). Also, note that there is a bijection from \(X_d\)
to \(U_{\frac{n}{d}}\) since

\begin{equation*}
a \in X_d \iff \gcd(a,n) =d \iff \gcd\left(\frac{a}{d}, \frac{n}{d}\right) \iff \frac{a}{d} \in U_{\frac{n}{d}}
\end{equation*}

hence, \(|X_d| = |U_{\frac{n}{d}}| = \phi(\frac{n}{d})\).
Additionally, note that \(d \mapsto \frac{n}{d}\)
is a permutaton of the set of divisors of \(n\), hence

\begin{equation*}
n = |J_n|
= \left|\bigsqcup_{d|n} X_d\right|
= \bigsqcup_{d|n} \left|X_d\right|
= \sum_{d|n} \phi\left(\frac{n}{d}\right)
= \sum_{d|n} \phi(d)
\end{equation*}

and we are done
\end{example}
\end{document}
