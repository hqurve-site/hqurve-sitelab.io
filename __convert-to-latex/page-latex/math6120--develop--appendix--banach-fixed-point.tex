\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Banach fixed-point theorem}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
This section describes the Banach fixed-point theorem and some of the necessary preliminaries.

\section{Preliminaries}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---preliminaries}

\subsection{Complete}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---complete}

Let \((X, d)\) be a metric space. Then, we say that a sequence
\(\{x_n\}\) is \emph{Cauchy} iff

\begin{equation*}
\forall \varepsilon > 0: \exists N \geq 0: \forall n,m \geq n: d(x_n, x_m) < \varepsilon
\end{equation*}

Note that convergence implies cauchy; however the converse is not necessarily true.
A metric space in which all cauchy sequences converge is called \emph{complete}.

\subsubsection{Examples}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---examples}

Some common complete metric spaces are

\begin{itemize}
\item \(\mathbb{R}^n\) (with any norm)
\item Closed subsets of complete metric spaces
\item Continuous functions from a compact space to a complete metric space
    where the metric on the function space is the supremum
\end{itemize}

\begin{theorem}[{Continuous functions from compact space to complete metric space}]
\begin{admonition-warning}[{}]
This is todo. We first need to verify what conditions are needed (eg properties about the function metric)
and if the statement is true.
\end{admonition-warning}

Let \(\mathcal{F}\) be a set of continuous functions from \((X, d_X)\) to \((Y, d_Y)\)
where \(X\) is compact and \(Y\) is complete. Then, \(\mathcal{F}\)
is also complete.

\begin{proof}[{}]
Since \(X\) is compact, by the below lemma, \(\mathcal{F}\) consists of uniformly continuous functions

Let \(\{f_n\}\) be a Cauchy sequence of functions. Then,
notice that \(\{f_n(x)\}\) is cauchy for each \(x \in X\) since,

\begin{equation*}
d_Y(f_n(x), f_m(x)) \leq d_F(f_n, f_m)
\end{equation*}

Therefore each \(\{f_n(x)\}\) converges and we may define \(f:X \to Y\)
by the limit value of each \(x \in X\).
We still need to prove that \(f \in \mathcal{F}\) and \(\{f_n\}\) converges to \(f\).

We first show the continuity of \(f\).
Consider arbitrary \(x \in X\) and \(B(f(x), \varepsilon)\)

First, let \(\varepsilon > 0\); then for each \(x \in X\), there exists
\(N_x\) such that for each \(n > N_x\), \(d_Y(f(x), f_n(x)) < \varepsilon\)
Consider arbitrary

First lets prove that each function is uniformly continuous.
\end{proof}
\end{theorem}

\begin{lemma}[{Uniform continuity of functions with compact domain}]
Let \(f: X \to Y\) where \((X, d_X)\) and \((Y, d_Y)\) are metric spaces
and \(X\) is compact.
Then, \(f\) is uniformly continuous.

\begin{proof}[{}]
Consider arbitrary \(\varepsilon > 0\), \(f \in \mathcal{F}\), and the set
of balls \(B\left(f(x),\frac{\varepsilon}{2}\right)\) for each \(x \in X\).
By the continuity of \(f\), there exists \(\delta_x > 0\) for each
\(x\) such that \(f(B(x, \delta_x)) \subseteq B\left(f(x),\frac{\varepsilon}{2}\right)\).
The collection of balls \(B\left(x, \frac{\delta_x}{2}\right)\)
covers \(X\) and hence has a finite subcover determined by \(X^*\).
Let \(\delta = \min_{x^* \in X^*} \delta_{x^*}\).

Now, consider any \(x_1, x_2\) with \(d(x_1, x_2) < \frac{\delta}{2}\)
there exists \(x^* \in X^*\) such that \(x_1 \in B\left(x^*, \frac{\delta_{x^*}}{2}\right)\).
Note that

\begin{equation*}
d(x^*, x_2) \leq d(x^*, x_1) + d(x_1, x_2) < \frac{\delta_{x^*}}{2} + \frac{\delta}{2} \leq \delta_{x^*}.
\end{equation*}

Therefore, \(x_1, x_2 \in B(x^*, \delta_{x^*})\) and by construction of \(\delta_{x^*}\),
\(f(x_1), f(x_2) \in B\left(f(x^*),\frac{\varepsilon}{2}\right)\)
and \(d(f(x_1), f(x_2)) < \varepsilon\).
Therefore, \(f\) is uniformly continuous.

\begin{admonition-note}[{}]
The idea of constructing the cover using \(\frac{\delta_x}{2}\) came from Rudy the Reindeer in
\url{https://math.stackexchange.com/questions/110573/continuous-mapping-on-a-compact-metric-space-is-uniformly-continuous}
\end{admonition-note}
\end{proof}
\end{lemma}

\subsection{Norms}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---norms}

Let \(V\) be a vector space field \(\mathbb{F}\) (which is a subfield of the complex numbers) and with an operation \(\|\cdot\|: V \to \mathbb{R}\).
Then, \(\|\cdot\|\) is called a \emph{norm} iff the following four properties hold
for all \(\vec{v}, \vec{w} \in V\) and \(\alpha \in \mathbb{F}\)

\begin{description}
\item[Positive definiteness] \(\|v\| \geq 0\)
\item[Coincidence] \(\|v\| = 0\) iff \(\vec{v} = \vec{0}\)
\item[Positive homogeneity] \(\|\alpha\vec{v}\| = |\alpha| \|\vec{v}\|\)
\item[Triangle inequality] \(\|\vec{v} + \vec{w}\| \leq \|\vec{v}\| + \|\vec{w}\|\)
\end{description}

Note that the norm induces a metric on the vector space by \(d(\vec{x}, \vec{y}) = \|\vec{x} - \vec{y}\|\).

\begin{admonition-caution}[{}]
Not all metrics on a vector space correspond to a norm. Specifically,
a metric does not contain enough information for positive homogeneity
\end{admonition-caution}

\subsection{Banach space}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---banach-space}

A complete normed vector space is called a \emph{Banach space}.
That is, if the metric induced by a norm is complete, the space is called a \emph{Banach space}.

\subsection{Lipschitz}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---lipschitz}

Let \((X, d)\) be a metric space. Then we say that \(T: X\to X\) satisfies
the \emph{Lipschitz condition} with constant \(K > 0\)  iff

\begin{equation*}
\forall x,y \in X: d(Tx, Ty) \leq Kd(x,y)
\end{equation*}

If this constant \(K \in (0,1)\), we say that T is a \emph{contraction}.

\section{Banach Contraction mapping theorem}
\label{develop--math6120:appendix:page--banach-fixed-point.adoc---banach-contraction-mapping-theorem}

A fixed point of a function is simply a point \(x_0\) such that \(f(x_0) = x_0\).

\begin{theorem}[{}]
Let \(T\) be a contraction from a Banach space to itself.
Then, \(T\) has a unique fixed point.

OR

Let \(T\) be a contraction from a complete metric space to itself.
Then, \(T\) has a unique fixed point.

\begin{admonition-note}[{}]
The first statement is a special case of the second.
\end{admonition-note}

\begin{proof}[{Existence}]
Let \((X, d)\) be a complete metric space and \(T\) be a contraction
with Lipschitz constant \(K \in (0,1)\).

While applying this theorem to prove Picard's local existence theorem,
it be comes apparent that we expect that repeated application of \(T\)
to converge.

Let \(x \in X\) be arbitrary and consider the sequence \(\{T^nx\}\).
We claim that this sequence is Cauchy.
Let \(r = d(Tx, x)\) and note that if \(m < n\)

\begin{equation*}
\begin{aligned}
d(T^mx, T^nx)
&\leq K^md(x, T^{n-m}x)
\\&\leq K^m\left(d(x, Tx) + d(Tx, T^2x) + \cdots d(T^{n-m-1}x, T^{n-m}x)\right)
\\&\leq K^m\sum_{i=0}^{n-m-1} d(T^ix, T^{i+1}x)
\\&\leq K^m\sum_{i=0}^{n-m-1} K^id(x, Tx)
\\&\leq rK^m \frac{K^{n-m}-1}{K-1}
\\&= r\frac{K^n-K^m}{K-1}
\end{aligned}
\end{equation*}

Let \(\varepsilon > 0\). Then, since \(|K| < 1\), there exists \(N\) such that
\(r\frac{K^p}{K-1} \leq \frac{\varepsilon}{2}\) for all \(p > N\).
Then, for any \(m,n > N\),

\begin{equation*}
d(T^mx, T^nx) \leq r\frac{|K^n-K^m|}{K-1}
\leq r\frac{K^n}{K-1} + r\frac{K^n}{K-1}
< \varepsilon
\end{equation*}

Therefore the sequence \(\{T^n x \}\) is Cauchy and has a limit point \(x^* \in X\).
We now need to show that \(x^*\) is a fixed point.
Consider \(d(x^*, Tx^*)\) and arbitrary \(\varepsilon > 0\).
Since the \(\{T^nx\}\) converges to \(x^*\), there exists \(N > 0\) such
that for each \(n > N\), \(d(x^*, T^{n}x) < \frac{\varepsilon}{1+K}\).
Then,

\begin{equation*}
d(x^*, Tx^*) \leq d(x^*, T^{N+2}x) + d(T^{N+2}x, Tx^*)
< \frac{\varepsilon}{1+K} + \frac{K\varepsilon}{1+K} = \varepsilon
\end{equation*}

Therefore,  \(d(x^*, Tx^*) < \varepsilon\) for each \(\varepsilon > 0\)
and hence \(d(x^*, Tx^*) = 0\) and \(x^*\) is a fixed point of \(T\).
\end{proof}

\begin{proof}[{Uniqueness}]
Let \(x, x^* \in X\) be two fixed points of \(T\) with Lipschitz constant
\(K \in (0,1)\).
Then,

\begin{equation*}
d(x, x^*) = d(Tx,Tx^*) \leq Kd(x,x^*)
\implies (1-K)d(x,x^*) \leq 0 \implies d(x,x^*) = 0
\end{equation*}
\end{proof}
\end{theorem}

\begin{corollary}[{Generalized Banach contraction principle}]
Let \(T: X\to X\) where \(X\) is a Banach space. Then if \(T^n\) is a contraction,
\(T\) has a unique fixed point

\begin{proof}[{}]
\begin{admonition-note}[{}]
Proof from Motaka in \url{https://math.stackexchange.com/questions/360795/generalization-of-banachs-fixed-point-theorem}
\end{admonition-note}

Suppose Banach contraction mapping theorem holds.

Let \(x^*\) be the unique fixed point of \(T^n\). Then,

\begin{equation*}
T^{n}(Tx^*) = T(T^nx^*) = Tx^*
\end{equation*}

Therefore \(Tx^*\) is another fixed point of \(T^n\). By uniqueness,
we obtain that \(Tx^* = x^*\) as desired.
\end{proof}
\end{corollary}
\end{document}
