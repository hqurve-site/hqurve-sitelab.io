\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Simple Results}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Consider the following first order linear DE defined on the open interval \(I\)

\begin{equation*}
x' + p(t)x = r(t)
\end{equation*}

On this page, we would discuss several results relating to existence and uniqueness of solutions
to this problem (together with problems with additional constraints).

\section{General solution}
\label{develop--math6120:ROOT:page--simple-results.adoc---general-solution}

Consider the following DE on interval \(I\)

\begin{equation*}
x' + p(t)x = r(t)
\end{equation*}

where \(p\) and \(r\) are continuous.
Then, the function \(\phi(t)\) is a solution iff
there exists constant \(C\) such that

\begin{itemize}
\item \(\phi(t) = Ce^{-P(t)} + R(t)e^{-P(t)}\) \(\forall t\in I\)
\item \(P(t)\) is differentiable and \(P' = p\)
\item \(R(t)\) is differentiable \(R' = re^P\)
\end{itemize}

Note that we may choose \(P\) and \(R\) to be any fixed antiderivative and then determine the value of
\(C\) (this is useful later).

Also note that this result may be simplified if the DE is homogeneous (ie \(r=0\) on \(I\)).

\begin{proof}[{}]
\begin{admonition-note}[{}]
Verifying that a function of such form is indeed a solution to the DE is simple.
\end{admonition-note}

Now, suppose that \(\phi(t)\) is a solution to the DE we need to confirm its form.
Notice that differentiable
function \(P\) and \(R\) as stated above exist by the fundamental theorem of calculus (part 1)
since \(p\) and \(r\) are both continuous. Next,
since \(\phi\) is a solution to the DE, we have that

\begin{equation*}
\begin{aligned}
&\phi'(t) + p(t)\phi(t) = r(t) \quad \forall t \in I
\\&\implies \frac{d}{dt}\left(e^{P(t)}\phi(t)\right)
    =e^{P(t)}\phi'(t) + p(t)e^{P(t)}\phi(t)
    = e^{P(t)}r(t) = \frac{d}{dt} R \quad \forall t \in I
\\&\implies \frac{d}{dt}\left(e^{P(t)}\phi(t) - R\right) = 0 \quad \forall t \in I
\\&\implies \exists C \in \mathbb{R}: e^{P(t)}\phi(t) - R = C \quad \forall t \in I
\\&\implies \exists C \in \mathbb{R}: \phi(t) = Ce^{-P(t)} + Re^{-P(t)} \quad \forall t \in I
\end{aligned}
\end{equation*}

as desired.

\begin{admonition-important}[{}]
If \(f'(t) = 0\) for all \(t \in I\), then for all \(a < b\), there exists \(c\)
such that

\begin{equation*}
\frac{f(a) - f(b)}{a-b} = f'(c) = 0
\end{equation*}

by the mean value theorem (only requires that \(f\) is continuous and differentiable).
This implies that \(f\) must be constant on \(I\)

Proof by 3Sat \url{https://math.stackexchange.com/questions/1524724/if-the-derivative-of-a-function-is-zero-is-the-function-a-constant-function}
\end{admonition-important}
\end{proof}

\subsection{Initial value problem}
\label{develop--math6120:ROOT:page--simple-results.adoc---initial-value-problem}

The following IVP has a unique solution

\begin{equation*}
x' + px = r, \quad x(t_0) = x_0
\end{equation*}

where \(p\) and \(r\) are continuous.
The solution is

\begin{equation*}
\phi(t) = x_0e^{-P(t)} + e^{-P(t)}\int_{t_0}^t e^{P(s)}r(s)\ ds\quad\forall t \in I
\end{equation*}

where \(P(t) = \int_{t_0}^t p(s) \ ds\)

\begin{proof}[{}]
Verifying that a function of such form is indeed a solution to the DE is simple.
Therefore, we know that a solution exists. Now, we just need to show that it is unique.

Let

\begin{equation*}
P(t) = \int_{t_0}^t p(s) \ ds
\quad\text{and}\quad
R(t) = \int_{t_0}^t e^{P(s)}r(s) \ ds
\end{equation*}

Then, \(P' = p\)  and \(R' = e^{P}r\).

Let \(\phi\) be a solution to the IVP. Then, it must also be a solution
to the unconstrained DE and hence there exists \(C \in \mathbb{R}\) such that

\begin{equation*}
\phi(t) = Ce^{-P} + Re^{-P}
\end{equation*}

By substituting the initial condition, we obtain

\begin{equation*}
x_0 = \phi(t_0) = Ce^{-0} + 0e^{-0} = C
\end{equation*}

Therefore \(\phi\) is uniquely determined by the initial condition.
Hence, we have proven that a solution exists and is unique.
\end{proof}

\section{Periodic functions}
\label{develop--math6120:ROOT:page--simple-results.adoc---periodic-functions}

Consider the DE defined on \(\mathbb{R}\)

\begin{equation*}
x'(t) + p(t)x(t) = 0 \quad \forall t \in \mathbb{R}
\end{equation*}

where \(p\) is continuous and periodic of period \(\xi\).
Then the above DE has a non-zero periodic function of period \(\xi\) iff
\(\exp\left[\int_0^\xi p(s) \ ds\right] = 1\).

\begin{admonition-important}[{}]
This theorem says nothing about non-periodic solutions nor solutions with different periods.
\end{admonition-important}

\begin{proof}[{}]
Let \(\phi(t)\) be a non-zero solution to the above DE. Then, from the previous results, we know that
\(\phi\) has the following form

\begin{equation*}
\phi(t) = Ce^{-P(t)}
\end{equation*}

where \(P(t) = \int_{t_0}^t p(s) \ ds\).

\begin{admonition-note}[{}]
The choice of \(t_0\) does not matter
\end{admonition-note}

Now,

\begin{equation*}
\begin{aligned}
&\phi \text{ is periodic with period } \xi
\\&\iff \forall t\in \mathbb{R}: \phi(t) = \phi(t+\xi)
\\&\iff \forall t\in \mathbb{R}: Ce^{-P(t)} = Ce^{-P(t+\xi)}
\\&\iff \forall t\in \mathbb{R}: P(t) = P(t+\xi) \quad\text{since } C\neq 0
\\&\iff \forall t\in \mathbb{R}: \int_{t_0}^t p(s) \ ds = \int_{t_0}^{t+\xi} p(s) \ ds
\\&\iff \forall t\in \mathbb{R}: \int_{0}^{t+\xi} p(s) \ ds = \int_{0}^{t} p(s) \ ds
\\&\iff \forall t\in \mathbb{R}: \int_{0}^{t+\xi} p(s) \ ds = \int_{\xi}^{t + \xi} p(s-\xi) \ ds
\\&\iff \forall t\in \mathbb{R}: \int_{0}^{t+\xi} p(s) \ ds = \int_{\xi}^{t + \xi} p(s) \ ds
\\&\iff \forall t\in \mathbb{R}: \int_{0}^{\xi} p(s) \ ds = 0
\\&\iff \int_{0}^{\xi} p(s) \ ds = 0
\\&\iff \exp\left[\int_{0}^{\xi} p(s) \ ds\right] = 1
\end{aligned}
\end{equation*}
\end{proof}

\subsection{Non-homogeneous}
\label{develop--math6120:ROOT:page--simple-results.adoc---non-homogeneous}

Consider the DE defined on \(\mathbb{R}\)

\begin{equation*}
x'(t) + p(t)x(t) = r(t) \quad \forall t \in \mathbb{R}
\end{equation*}

where \(p\) and \(r\) are continuous and periodic of period \(\xi\).
We define \(P(t) = \int_0^t p(s) \ ds\).
Then the following are true

\begin{enumerate}[label=\arabic*)]
\item A solution \(\phi(t)\) is periodic of period \(\xi\) iff \(\phi(0) = \phi(\xi)\).
\item There is exactly one periodic solution of period \(\xi\) iff \(e^{P(\xi)} \neq 1\)
\item Every solution is periodic of period \(\xi\) iff \(e^{P(\xi)} = 1\) and
    \(\int_0^\xi e^{P(s)}r(s) \ ds = 0\).
    
    \begin{admonition-note}[{}]
    If \(e^{P(\xi)} = 1\) and \(\int_0^\xi e^{P(s)}r(s) \ ds \neq 0\), there are no periodic solutions.
    \end{admonition-note}
\end{enumerate}

\begin{proof}[{Statement 1}]
Let \(\phi(t)\) be a solution to the DE, then there exists \(C \in \mathbb{R}\) such that

\begin{equation*}
\phi(t) = Ce^{-P(t)} + e^{-P(t)}\int_0^t e^{P(s)}r(s) \ ds
\end{equation*}

First, note the following property of \(P\)

\begin{equation*}
\begin{aligned}
P(t+\xi)
&= \int_0^{t+\xi} p(s) \ ds
\\&= P(t) + \int_{0}^{t+\xi} p(s) \ ds - \int_0^{t} p(s) \ ds
\\&= P(t) + \int_{0}^{t+\xi} p(s) \ ds - \int_\xi^{t+\xi} p(s-\xi) \ ds
\\&= P(t) + \int_{0}^{t+\xi} p(s) \ ds - \int_\xi^{t+\xi} p(s) \ ds
\\&= P(t) + P(\xi)
\end{aligned}
\end{equation*}

Then,

\begin{equation*}
\begin{aligned}
&\phi \text{ is periodic with period } \xi
\\&\iff \forall t \in \mathbb{R}: \phi(t) = \phi(t+\xi)
\\&\iff \forall t \in \mathbb{R}:
    Ce^{-P(t)} + e^{-P(t)}\int_0^t e^{P(s)}r(s) \ ds
    =
    Ce^{-P(t+\xi)} + e^{-P(t+\xi)}\int_0^{t+\xi} e^{P(s)}r(s) \ ds
\\&\iff \forall t \in \mathbb{R}:
    C + \int_0^t e^{P(s)}r(s) \ ds
    =
    Ce^{-P(t+\xi)+P(t)} + e^{-P(t+\xi)+P(t)}\int_0^{t+\xi} e^{P(s)}r(s) \ ds
\\&\iff \forall t \in \mathbb{R}:
    C + \int_0^t e^{P(s)}r(s) \ ds
    =
    Ce^{-P(\xi)} + e^{-P(\xi)}\int_0^{t+\xi} e^{P(s)}r(s) \ ds
\\&\iff \forall t \in \mathbb{R}:
    C
    =
    Ce^{-P(\xi)} + e^{-P(\xi)}\int_0^{\xi} e^{P(s)}r(s) \ ds
\\&\iff \forall t \in \mathbb{R}:
    \phi(0) = \phi(\xi)
\\&\iff
    \phi(0) = \phi(\xi)
\end{aligned}
\end{equation*}

since

\begin{equation*}
\forall t \in \mathbb{R}:
\int_\xi^{t+\xi} e^{P(s)}r(s) \ ds
= \int_0^{t} e^{P(s+\xi)}r(s+\xi) \ ds
= \int_0^{t} e^{P(s)+P(\xi)}r(s) \ ds
= P(\xi)\int_0^{t} e^{P(s)}r(s) \ ds
\end{equation*}
\end{proof}

\begin{proof}[{Statements 2 and 3}]
Let \(\phi\) be a periodic solution of period \(\xi\).
Then there exists \(C \in \mathbb{R}\) such that

\begin{equation*}
\phi(t) = Ce^{-P(t)} + e^{-P(t)}\int_0^t e^{P(s)}r(s) \ ds
\end{equation*}

Since \(\phi\) is periodic with period \(\xi\), statement 1 asserts that

\begin{equation*}
    C
    =
    Ce^{-P(\xi)} + e^{-P(\xi)}\int_0^{\xi} e^{P(s)}r(s) \ ds
\end{equation*}

which happens iff

\begin{equation*}
    C\left(e^{P(\xi)} - 1\right)
    =
    \int_0^{\xi} e^{P(s)}r(s) \ ds
\end{equation*}

If \(e^{P(\xi)} \neq 1\), we see that that \(C\) is uniquely determined
and hence there is only one periodic solution of period \(\xi\).
Otherwise, any \(C \in \mathbb{R}\) yield a periodic solution of period \(\xi\) and hence all
solutions are periodic with period \(\xi\) as long as the right hand side is zero.
If the right hand side is not zero, there are no periodic solutions.

Thus we have proven statements 2 and 3.
\end{proof}
\end{document}
