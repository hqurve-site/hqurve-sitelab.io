\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Categorical Data}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
If we collect data with more than one categorical variable,
we may choose to represent it using a \textbf{contingency table}.
In a contingency table, each axis (row/column) represents
a single categorical variable
and values in the table represent how many observations of a certain
axis combination there were.

\section{Distribution tests}
\label{develop--stat6160:ROOT:page--categorical.adoc---distribution-tests}

We may want to test whether the sample data in a contingency table supports
the claim that the population data follows some distribution.
To do this, we can use a statistical test which compares the expected
values (\(\mu_{ij}\)) to the observed values (\(n_{ij}\)).

\subsection{Chi-squared}
\label{develop--stat6160:ROOT:page--categorical.adoc---chi-squared}

If the expected values are not small (typically {\textgreater} 5), the chi-squared-Pearson (\(\chi^2\))
and likelihood ratio test (\(G^2\) or \(L^2\)) statistics can be used

\begin{equation*}
\chi^2 = \sum \frac{(n_{ij} - \mu_{ij})^2}{\mu_{ij}}
\quad\text{and}\quad
G^2 = 2\sum n_{ij}\log\frac{n_{ij}}{\mu_{ij}}
\end{equation*}

The distributions of these two statistics tend to a chi-squared distribution with
the number of degrees of freedom equal to

\begin{equation*}
(\#\text{ of parameters under }H_1)
-
(\#\text{ of parameters under }H_0)
\end{equation*}

\begin{admonition-tip}[{}]
\(G^2\) test has a better approximation to chi-squared distribution than Pearson.
Also, it is a more efficient estimator.
\end{admonition-tip}

\begin{proposition}[{Chi-squared test for independence}]
Suppose we have two variables \(X\) and \(Y\) with multinomial distribution.
We want to test whether these two variables are independent.

Let \(X\) have \(m\) levels with probabilities \(\pi_{1.},\ldots \pi_{m.}\).
Let \(Y\) have \(n\) levels with probabilities \(\pi_{.1},\ldots \pi_{.n}\).

Then under the null hypothesis, the probability of observing level \((i,j)\) is
\(\pi_{ij} = \pi_{i.}\pi_{.j}\).
In total there are \((m-1)+(n-1)\) parameters in this model.

However, under the alternative hypothesis that the variables are not independent
and the \((X,Y)\) follows a general multinomial distribution,
there are \(mn-1\) parameters.

Therefore, the number of degrees of freedom is

\begin{equation*}
(mn-1) - ((m-1) + (n-1)) = (m-1)(n-1)
\end{equation*}
\end{proposition}

\subsubsection{Cell residuals under independence hypothesis}
\label{develop--stat6160:ROOT:page--categorical.adoc---cell-residuals-under-independence-hypothesis}

If the Chi-squared or LR test fails, it is interesting to know which cells were responsible.
For this, we can look at the cell residuals

\begin{equation*}
\frac{n_{ij} - \hat{\mu}_{ij}}{\sqrt{\hat{\mu}_{ij} (1-\hat{p}_1)(1-\hat{p}_2)}}
\sim N(0,1)
\end{equation*}

where the denominator is the standard error of \(n_{ij} - \hat{\mu}_{ij}\) under the null hypothesis.

\subsection{Fisher's exact test}
\label{develop--stat6160:ROOT:page--categorical.adoc---fishers-exact-test}

If the expected values of the contingency table are not sufficiently large,
the chi-squared test should not be used.
Instead, Fisher's exact test can be used.

Fisher's exact test considers all contingency tables
with axis totals equal to the observed table
and finds the probability of observing the observed table or one more
\textbf{extreme}.
Typically, the odds-ratio is used to assess
how extreme the table is.

\subsubsection{2x2}
\label{develop--stat6160:ROOT:page--categorical.adoc---2x2}

For a 2x2 table, with fixed row and column totals,
the probability of observing a given table follows a hypergeometric distribution.
Consider the following table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {E} & {~E} \\
\hline
{D} & {a} & {b} \\
\hline
{~D} & {c} & {d} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Then the probability of observing this table is

\begin{equation*}
\frac{n(\text{Select }a\text{ from }a+b)n(\text{Select }c\text{ from }c+d)}{n(\text{Select }a+c\text{ from total})}
= \frac{(a+b)!(c+d)!(a+c)!(b+d)!}{(a+b+c+d)!a!b!c!d!}
\end{equation*}

Note that the initial numerator forces the row totals while the denominator forces the column total.

\begin{admonition-warning}[{}]
Dr. Tripathi said to find the p-value we add all probabilities of tables less than or equal to the probability
of the observed table, and for a two-sided test, multiply the result by 2.

I do not think this is correct, but ... for class assignments/exams, we will follow his instruction.
\end{admonition-warning}

\subsection{Matched/Paired Data}
\label{develop--stat6160:ROOT:page--categorical.adoc---matchedpaired-data}

\begin{admonition-note}[{}]
This can be generalized to more than 2 levels, but we just use two levels here for simplicity.
\end{admonition-note}

Suppose we have data which occurs in pairs. For example,
a survey may ask respondents whether they would like to pay higher taxes
and whether they would like to cut living standards.
If both answers are the same, we call them a \textbf{cordant pair};
otherwise, we call them a \textbf{discordant pair}.

\begin{table}[H]\centering
\caption{Table 8.1 from Agresti}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetCell[r=2, c=2]{}{} &  & \SetCell[r=1, c=2]{}{Cut living standards} &  \\
\hline
 &  & {Yes} & {No} \\
\hline
\SetCell[r=2, c=1]{}{Pay higher taxes} & {Yes} & {227} & {132} \\
\hline
 & {No} & {107} & {678} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

In such a case, we may expect the respondents to give the same response for both questions.
This is called \textbf{marginal homogeneity} and we test the null hypothesis \(H_0: \pi_{1.} = \pi_{.1}\)
or equivalently \(\pi_{12} = \pi_{21}\).

Under the null hypothesis there is equal chance of observations being allotted to either
of the discordant entries. Therefore \(n_{12} \sim Bin(n^*, 0.5)\)
and after approximating to normal, we obtain Wald test statistic

\begin{equation*}
Z = \frac{n_{12} - n_{21}}{\sqrt{n_{12} + n_{21}}} \sim N(0,1)
\end{equation*}

We can alternatively test \(Z^2 \sim \chi^2_1\), which is called the \textbf{McNemar test}.

We can additionally produce a confidence interval for \({p}_{1.} - p_{.1}\)
as

\begin{equation*}
\frac{n_{12} - n_{21}}{n} \pm z_{\alpha/2} \frac{1}{n}\sqrt{(n_{12} + n_{21}) - \frac{(n_{12} - n_{21})^2}{n}}
\end{equation*}

This confidence interval is produced by considering the contingency table under the alternative hypothesis
and making use of the multinomial distribution.

\subsubsection{Viewing as 2x2xK table}
\label{develop--stat6160:ROOT:page--categorical.adoc---viewing-as-2x2xk-table}

We can instead view matched data as coming from a \(2\times2\times K\) table
where each \(2\times 2\) table contains the questions as columns and the answers as rows (or vice-versa).
Under this lens, the contingency table counts the number of occurrences of each
of the 4 possible \(2\times 2\) tables in the \(2\times 2\times K\) table.

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetCell[r=2, c=2]{}{} &  & \SetCell[r=1, c=2]{}{Question 2} &  \\
\hline
 &  & {Yes} & {No} \\
\hline
\SetCell[r=2, c=1]{}{Question 1} & {Yes} & {\(\displaystyle \begin{array}{|c|c|c|}\hline & Q1 & Q2 \\\hline Yes & 1 & 1 \\\hline No & 0 & 0\\\hline\end{array}\)} & {\(\displaystyle \begin{array}{|c|c|c|}\hline & Q1 & Q2 \\\hline Yes & 1 & 0 \\\hline No & 0 & 1\\\hline\end{array}\)} \\
\hline
 & {No} & {\(\displaystyle \begin{array}{|c|c|c|}\hline & Q1 & Q2 \\\hline Yes & 0 & 1 \\\hline No & 1 & 0\\\hline\end{array}\)} & {\(\displaystyle \begin{array}{|c|c|c|}\hline & Q1 & Q2 \\\hline Yes & 0 & 0 \\\hline No & 1 & 1\\\hline\end{array}\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Let \((Y_{i1}, Y_{i2})\) be the responses for the \(i\)'th respondent for questions 1 and 2
where \(Y=1\) indicates ``yes''.
Under this framework, \(n_{ij}\) represents the number of respondents
which gave response \(i\) to the first question and response \(j\)
to the second question.

We can also perform logistic regression on this data with the model
\(logit(P(Y_{i1}=1)) = \alpha_i + \beta\)
and \(logit(P(Y_{i2}=2)) = \alpha_i\).
After some \emph{magic}, we arrive on the odds ratio estimate
\(e^\beta = \frac{n_{12}}{n_{21}}\).
For the data in table 8.1, the odds ratio of 1.23 is obtained
which indicates that there is a 23\% greater odds
of respondents answering yes to question 1 over question 2.

Alternatively, we can use the Mantel-Haenszel procedure to obtain the odds
ratio as well as the McNemar test.

\section{2x2}
\label{develop--stat6160:ROOT:page--categorical.adoc---2x2-2}

Consider a 2x2 contingency table.
In the medical setting, rows may indicate the presence of a disease
and columns may represent the exposure (eg medicine or gender).

Suppose we are analyzing the effect of some exposure (E) on the presence of a disease (D).
So, if we have two groups one with the exposure and one without the exposure, we
may have the following contingency table.

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {E} & {~E} \\
\hline
{D} & {\(Y_1\)} & {\(Y_2\)} \\
\hline
{~D} & {\(n_1 - Y_1\)} & {\(n_2 - Y_2\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

We may assume that the groups with differing exposure comes from different populations (eg male and female).
Then, \(Y_1 \sim Bin(n_1, \pi_1)\) and \(Y_2 \sim Bin(n_2, \pi_2)\).
We are interested in knowing whether the probability of having the disease depends on which
exposure group the subject is in.

If the estimates of the probabilities are not near 0 or 1, we can assess the difference

\begin{equation*}
\hat{p_1}-\hat{p_2}
\approx N\left(\pi_1 - \pi_2, \frac{\pi_1(1-\pi_1)}{n_1} + \frac{\pi_2(1-\pi_2)}{n_2}\right)
\end{equation*}

We can generate a Wald-test by substituting the estimates for the proportions into standard error.

However, if the probabilities are near 0, we may be less interested in the difference
but rather the ratio of the two probabilities.
This leads to the ideas of \textbf{relative risk} and \textbf{odds ratio}.

\begin{description}
\item[Relative Risk] The relative risk is defined as
    
    \begin{equation*}
    RR = \frac{P(D|E)}{P(D|~E)} = \frac{\pi_1}{\pi_2}
    \end{equation*}
    
    The estimate of relative risk is \(\displaystyle \frac{\hat{p_1}}{\hat{p_2}}\).
    The log of the estimate has standard error
    
    \begin{equation*}
    \hat{\mathrm{SE}}(\ln RR) =\sqrt{\frac{1-\hat{p_1}}{\hat{p_1}n_1} + \frac{1-\hat{p_2}}{\hat{p_2}n_2}}
    \end{equation*}
    
    This rule can be proven using the delta method.
\item[Odds Ratio] The odds ratio is defined as
    
    \begin{equation*}
    OR = \frac{{odds_1}}{\mathrm{odds_2}} = \frac{\pi_1/(1-\pi_1)}{\pi_2/(1-\pi_2)}
    \end{equation*}
    
    The estimate of the odds ratio is the \textbf{cross product} \(\displaystyle\frac{n_{11}n_{22}}{n_{21}n_{12}}\).
    The log of the estimate has standard error
    
    \begin{equation*}
    \hat{\mathrm{SE}}(\ln OR)
    = \sqrt{
        \frac{1}{n_{11}}
        + \frac{1}{n_{12}}
        + \frac{1}{n_{21}}
        + \frac{1}{n_{22}}
    }
    \end{equation*}
    
    This rule can be proven using the delta method.
\end{description}

Also note that
\(OR = RR\times \frac{1-\pi_2}{1-\pi_1}\)
So, when the probability of the disease is low, the odds ratio is an estimate of the relative risk.
This is important since in some studies (eg case-control), the total number of persons with the disease is controlled.
This has the effect that neither the odds ratio nor the relative risk can be computed since
the \(Y_1\) and \(Y_2\) are no longer binomial.
Instead, we can notice that the odds ratio is does not distinguish between exposure and disease
and hence we can obtain the odds ratio by swapping the roles of the exposure and disease.
This allows us to estimate the relative risk.

\subsection{Sensitivity and Specificity}
\label{develop--stat6160:ROOT:page--categorical.adoc---sensitivity-and-specificity}

Suppose we are analyzing a medical test which is to be used
to indicate the presence of a disease.
The true presence of the disease is the explanatory variable X and
the diagnosis of the test is the response variable Y.
In such a case,
there are four different outcomes

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetCell[r=2, c=2]{}{} &  & \SetCell[r=1, c=2]{}{Diagnosis (Y)} &  \\
\hline
 &  & {+} & {-} \\
\hline
\SetCell[r=2, c=1]{}{Truth (X)} & {+} & {true positive} & {false negative} \\
\hline
 & {-} & {false positive} & {true negative} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Then we define the following

\begin{itemize}
\item sensitivity \(=P(Y={\textbackslash} |{\textbackslash} X=)\) is the ability of the test correctly detect the presence of a disease
\item specificity \(=P(Y=-\ |\ X=-)\) is the ability of the test to correctly diagnose the absence of a disease
\end{itemize}

The gold standard is to have both sensitivity and specificity to be 1; however, in reality,
a trade off needs to be made.

Another problem is that a patient may not care about the sensitivity or specificity of
a test since they only know \(Y\). Instead they are interested in

\begin{itemize}
\item positive predictive value \(=P(X=+ \ | \ Y=+) = \frac{\text{sensitivity} \times \text{prevelence}}{\text{sensitivity}\times \text{preverlence} + (1-\text{specificity})\times(1-\text{preverlence})}\)
\item negative predictive value \(=P(X=- \ | \ Y=-) = \frac{\text{specificity} \times (1-\text{prevelence})}{\text{specificity}\times (1-\text{preverlence}) + (1-\text{sensitivity})\times\text{preverlence}}\)
\end{itemize}

\section{Stratified data (2x2xK)}
\label{develop--stat6160:ROOT:page--categorical.adoc---stratified-data-2x2xk}

Suppose we have a heterogeneous population.
In order to perform proper analysis, we take a stratified sample.
This results in \(K\) 2x2 contingency tables, each corresponding
to a different strata.

We can use the Mantel Haenszel procedure to combine the results
of all strata.
For the odds ratio, we have

\begin{equation*}
OR_{MH}
= \frac{\sum_{i=1}^K \frac{a_id_i}{n_i}}{\sum_{i=1}^K \frac{c_ib_i}{n_i}}
= \frac{\sum_{i=1}^K n_i \hat{p}_{11i}\hat{p}_{22i}}{\sum_{i=1}^K n_i \hat{p}_{12i}\hat{p}_{21i}}
\end{equation*}

Note that this is the same formula for the odds ratio, except the numerator
is combined using a weighted sum of the numerators of the individual odds ratios.
We can also perform a test of significance

\begin{equation*}
\chi^2
= \frac{\left[\sum_{i=1}^K (a_i - E[a_i])\right]^2}{\sum_{i=1}^K Var(a_i)}
\sim \chi^2_1
\end{equation*}

where

\begin{equation*}
\begin{aligned}
E[a_i] &= \frac{(a_i + b_i)(a_i+c_i)}{n_i} = \frac{(\text{row})(\text{col})}{\text{total}}
\\
Var[a_i] &= \frac{(\text{row}_1)(\text{row}_2)(\text{col}_1)(\text{col}_2)}{n_i^2(n_i-1)}
\end{aligned}
\end{equation*}
\end{document}
