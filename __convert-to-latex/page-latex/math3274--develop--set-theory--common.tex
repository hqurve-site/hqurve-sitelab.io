\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Common Operations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
Although it might appear that \myautoref[{}]{develop--math3274:set-theory:page--axioms.adoc} are missing some common properties,
many properties can be formulated as theorems of the stated axioms.

\section{Intersection of sets}
\label{develop--math3274:set-theory:page--common.adoc---intersection-of-sets}

Let \(\mathcal{A}\) be a non-empty collection of sets and let \(P(x) \iff \exists X \in \mathcal{A}: x \in X\).
Then let \(Y\) be an arbitrary set of \(\mathcal{A}\) and we define the \emph{intersection} of the sets in
\(\mathcal{A}\) as

\begin{equation*}
\bigcap_{X \in \mathcal{A}} X = \bigcap \mathcal{A}= \{x \in Y: \exists X \in \mathcal{A}: x \in X\}
\end{equation*}

This set is guarateed by the \myautoref[{axiom of specification}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-specification} and its uniqueness is guarateed by
the \myautoref[{axiom of extension}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-extension}.
Then notice that, as its notation implies, the intersection is independent of the choice of \(Y \in \mathcal{A}\)
since

\begin{equation*}
x \in \bigcap_{X \in \mathcal{A}} X
\iff [x \in Y \text{ and }\forall X \in \mathcal{A}: x \in X]
\iff \forall X \in \mathcal{A}: x \in X
\end{equation*}

\section{Unions and intersections of pairs of sets.}
\label{develop--math3274:set-theory:page--common.adoc---unions-and-intersections-of-pairs-of-sets}

Let \(A\) and \(B\) be sets. Then, by the \myautoref[{axiom of pairs}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-pairs-and-the-existence-of-a-set},
the set \(\{A, B\}\) exists and we can define

\begin{equation*}
A \cup B = \bigcup \{A, B\}
\quad\text{and}\quad
A \cap B = \bigcap \{A, B\}
\end{equation*}

\subsection{Basic Properties}
\label{develop--math3274:set-theory:page--common.adoc---basic-properties}

Let \(A, B, C\) be sets. Then

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(A \cup \varnothing = A\)} & {\(A \cap \varnothing = \varnothing\)} \\
\hline
{\(A \cup A = A\)} & {\(A \cap A = A\)} \\
\hline
{\(A \cup B = B \cup A\)} & {\(A \cap B = B \cap A\)} \\
\hline
{\((A \cup B)\cup C = A \cup (B \cup C)\)} & {\((A \cap B)\cap C = A \cap (B \cap C)\)} \\
\hline
{\(A \subseteq B \iff A \cup B = B\)} & {\(A \subseteq B \iff A\cap B = A\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Furthermore, both union and intersection distribute over each other

\begin{equation*}
A \cup (\cap \mathcal{D}) = \bigcap_{D \in \mathcal{D}}(A\cup D)
\quad\text{and}\quad
A \cap (\cup \mathcal{D}) = \bigcup_{D \in \mathcal{D}}(A\cap D)
\end{equation*}

where \(\mathcal{D}\) is a family of sets.

\section{Complements}
\label{develop--math3274:set-theory:page--common.adoc---complements}

Let \(A\) and \(B\) be sets. Then, we define
the \emph{difference} or \emph{relative complement} of \(B\) in \(A\)
as

\begin{equation*}
A-B = \{x \in A: x\notin B\}
\end{equation*}

Furthermore, if the set \(A\) is understood, we can simply write
\(B'\) or \(B^c\).

\subsection{Basic properties}
\label{develop--math3274:set-theory:page--common.adoc---basic-properties-2}

Let \(E, A, B, C\) be sets then

\begin{itemize}
\item \(E - (E - A) = A \cap E\)
\item \(E - \varnothing = E\)
\item \(E - E = \varnothing\)
\item \(A \cap (E-A) = \varnothing\)
\item \(A \cup (E - A) = E\)
\item \(A \subseteq B \iff A - B = \varnothing\)
\item \(E \cap (A - B) = (E\cap E) - (E\cap B)\)
\item if \(A, B, C \subseteq E\), then
    
    \begin{itemize}
    \item \(A - B = A \cap B'\)
    \item \(A \subseteq B \iff B'\subseteq A'\)
    \item \(A\cap B \subseteq (A\cap C) \cup (B\cap C')\)
    \item \(A\cup B \supseteq (A\cup C) \cap (B\cup C')\)
    \end{itemize}
\end{itemize}

\section{Symmetric Difference}
\label{develop--math3274:set-theory:page--common.adoc---symmetric-difference}

Let \(A\) and \(B\) be sets. Then we define the \emph{symmetric difference}
or \emph{boolean sum} of \(A\) and \(B\) by

\begin{equation*}
A + B = (A - B) \cup (B-A) = (A\cup B) - (A\cap B)
\end{equation*}

\subsection{Basic properties}
\label{develop--math3274:set-theory:page--common.adoc---basic-properties-3}

\begin{itemize}
\item \(A + B = B + A\)
\item \(A + (B +C) = (A+B) + C\)
\item \(A + \varnothing = A\)
\item \(A + A = \varnothing\)
\end{itemize}

\section{Unions and Intersections}
\label{develop--math3274:set-theory:page--common.adoc---unions-and-intersections}

\subsection{Basic properties}
\label{develop--math3274:set-theory:page--common.adoc---basic-properties-4}

Let \(\mathcal{C}, \mathcal{D}\) be collections of sets then

\begin{itemize}
\item \(\mathcal{C} \subseteq \mathcal{D}\) implies
    
    \begin{itemize}
    \item \(\bigcup \mathcal{C} \subseteq \bigcup\mathcal{D}\)
    \item \(\bigcap \mathcal{C} \supseteq \bigcup\mathcal{D}\) where \(\mathcal{C} \neq \varnothing\)
    \end{itemize}
\item \(A \in \mathcal{C}\) implies
    
    \begin{itemize}
    \item \(A \subseteq \bigcup \mathcal{C}\)
    \item \(A \supseteq \bigcap \mathcal{C}\)
    \end{itemize}
\end{itemize}

\subsection{Demorgan's laws}
\label{develop--math3274:set-theory:page--common.adoc---demorgans-laws}

Let \(\mathcal{C}\) be non-empty collection of sets and \(E\) be a set then

\begin{itemize}
\item \(E - \bigcup \mathcal{C} = \bigcap_{C \in \mathcal{C}} (E - C) \)
\item \(E - \bigcap \mathcal{C} = \bigcup_{C \in \mathcal{C}} (E - C) \)
\end{itemize}

\section{Unordered tuples}
\label{develop--math3274:set-theory:page--common.adoc---unordered-tuples}

Using the axiom of unions together with the \myautoref[{axiom of pairs}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-pairs-and-the-existence-of-a-set}, we can generalize the existence of a set containing \(n \geq 1\) sets.
To do this, we iteratively show that for any two sets, \(\mathcal{A}\) and \(B\), there exists a set which contains the contents
of \(\mathcal{A}\) as well as the set \(B\) itself. By the \myautoref[{axiom of pairs}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-pairs-and-the-existence-of-a-set} there exists \(C =\{B\}\). Then by
taking the union of \(\mathcal{A}\) and \(C\), we obtain
\(\mathcal{B} = \mathcal{A} \cup \{B\}\).

Therefore, for any finite sequence of sets \(X_1, \ldots X_n\), there exists a set \(D = \{X_1, \ldots X_n\}\).

\section{Power sets}
\label{develop--math3274:set-theory:page--common.adoc---power-sets}

Let \(A\) and \(B\) be sets, then

\begin{equation*}
\mathcal{P}(A) \cap \mathcal{P}(B) =\mathcal{P}(A\cap B)
\end{equation*}

and

\begin{equation*}
\mathcal{P}(A) \cup \mathcal{P}(B) \subseteq \mathcal{P}(A\cup B)
\end{equation*}

where equality holds iff \(A \subseteq B\) or \(B\subseteq A\).
\end{document}
