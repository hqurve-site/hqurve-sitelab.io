\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Markov Chains}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

{} \def\comm{\leftrightarrow}
% Title omitted
Let \(\{X_t\}\) be a collection of random variables indexed by some set \(T\)
which is usually referred to as `time'. If \(T\) is countable, we say
that the
\myautoref[{stochastic}]{develop--math2274:ROOT:page--basic-definitions.adoc---stochastic-phenomena}
process
has discrete time. Otherwise if \(T\) is equivalent to \(\mathbb{R}\)
we say that the stochastic process has continuous time.

Additionally, the set of all value of \(X_t\) is called the \emph{state space} of the processes.

\begin{admonition-note}[{}]
For the rest of this section, we would take \(T = \mathbb{N}\) and instead denote \(X_t\)
as \(X_n\).
\end{admonition-note}

Now, if \(X_n\) has a finite or countable states such that the state at point in time only depends
on the previous state, we call this process a \emph{markov chain}. That is

\begin{equation*}
P(X_{n+1} = j \ | \ X_n =i, X_{n-1} = i_{n-1}, \ldots X_0 = i_0)
= P(X_{n+1} =j\ |\ X_n = i)
\end{equation*}

We denote this probability \(p_{ij}^{(n,n+1)}\) and in general

\begin{equation*}
P(X_{m} =j\ |\ X_n =i) = p_{ij}^{(n,m)}
\end{equation*}

is the probability of transitioning from state \(i\) in step \(n\) to state \(j\) in step \(m\).
For the purposes of this course, we would only consider when the \emph{transition probability} does not depend
on the current state. That is

\begin{equation*}
p_{ij}^{(n,m)} =
P(X_{m} =j\ |\ X_n =i) =
P(X_{m-n} =j\ |\ X_0 =i) =
p_{ij}^{(m-n)}
\end{equation*}

That is the probability of transitioning from state \(i\) to state \(j\) in \(m-n\) steps.
Furthermore, for the one step transition probability, we may omit the superscript \((1)\).

\section{Transition Matrix}
\label{develop--math3278:markov-chains:page--index.adoc---transition-matrix}

\begin{admonition-note}[{}]
Yes, I know I said that we assume that we would only be focusing
on markov chains where the transition probability only depends on the
number of steps and starting
and ending states. However, the results for this section are trivially generalizable
and do not add much more bulk to the proofs.
\end{admonition-note}

As alluded to by the notation used, we actually aim to represent transition
probabilities by a matrix. Let \(P^{(n,m)}\) denote the \emph{transition matrix}
from step \(n\) to \(m\) where the \(ij\) entry is the probability
of transitioning from state \(i\) to \(j\). That is

\begin{equation*}
P^{(n,m)} = (p_{ij}^{(n,m)})
\end{equation*}

Then notice that the sum of each row must sum to one. We call any such matrix a \emph{stochastic matrix}.
Furthermore, if the sum of each column is also one, we call the matrix \emph{doubly stochastic}.

\subsection{Vector-matrix multiplication}
\label{develop--math3278:markov-chains:page--index.adoc---vector-matrix-multiplication}

By writing a transition like this, we are able to easily compute the distribution at step \(m\)
if we know that distribution at step \(n\) as it coincides with vector-matrix multiplication. That is
let \(\vec{x}\) be the row vector representation of the distribution of \(X_n\) where \(P(X_n =j) = \vec{x}_j\)
and let \(\vec{y}\) be the row vector representation of the distribution of \(X_m\). Then

\begin{equation*}
\vec{y} = \vec{x} P^{(n,m)}
\end{equation*}

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
\vec{y}_i = P(X_m = i)
&= P[(X_m =i \text{ and } X_n=1) \text{ or } (X_m = i\text{ and } X_n=2)\ldots]
\\&= \sum_{j} P(X_m = i\text{ and } X_n=j)
\\&= \sum_{j} P(X_m = i | X_n=j) P(X_n =j)
\\&= \sum_{j} P^{(n,m)}_{j,i} \vec{x}_j
\\&= \sum_{j} \vec{x}_j P^{(n,m)}_{j,i}
\\&= (\vec{x} P^{(n,m)})_i
\end{aligned}
\end{equation*}

by using the
\myautoref[{theorem of total probability}]{develop--math2274:ROOT:page--conditional-probability.adoc---theorem-of-total-probability}.
Therefore \(\vec{y} = \vec{x} P^{(n,m)}\) and perhaps with a bit of abuse of notation

\begin{equation*}
X_m = X_n P^{(n,m)}
\end{equation*}
\end{example}

\subsection{Matrix Multiplication (Chapman-Kolmogorov Theorem)}
\label{develop--math3278:markov-chains:page--index.adoc---matrix-multiplication-chapman-kolmogorov-theorem}

Likewise to computing distributions, we can compute transitions matrices by matrix multiplication.
Consider \(P^{(n,m)}\), \(P^{(m, l)}\) and \(P^{(n,l)}\) where \(n < m < l\); then

\begin{equation*}
P^{(n,m)} P^{(m,l)} = P^{(n,l)}
\end{equation*}

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
P^{(n,l)}_{ij}
= P(X_l =j | X_n = i)
&= \sum_{k} P((X_l = j \text{ and } X_m = k)| X_n =i)
\\&= \sum_{k} P(X_l = j | X_m = k | X_n =i) P(X_m =k | X_n =i)
\\&= \sum_{k} P(X_l = j | X_m = k) P(X_m =k | X_n =i)
\\&= \sum_{k} P^{(m,l)}_{kj} P^{(n,m)}_{ik}
\\&= \sum_{k} P^{(n,m)}_{ik} P^{(m,l)}_{kj}
\\&= [P^{(n,m)} P^{(m,l)}]_{ij}
\end{aligned}
\end{equation*}
\end{example}

\section{Transition Diagram}
\label{develop--math3278:markov-chains:page--index.adoc---transition-diagram}

From the transition matrix, we could develop a \emph{transition diagram}
which is a directed weighted graph where

\begin{itemize}
\item The nodes are states
\item There is an edge from \(i\) to \(j\) if \(p_{ij} > 0\)
    and its weight is \(p_{ij}\)
\end{itemize}

Additionally, as this diagram is mostly used to determine classifications,
we often omit the weights on the edges.

\section{Accessibility and classes}
\label{develop--math3278:markov-chains:page--index.adoc---accessibility-and-classes}

Firstly, we define the notion of \emph{accissibility}. We say that
state \(j\) is \emph{accessible} from state \(i\)
iff \(p^{(n)}_{ij} > 0\) for some \(n\) and we denote \(i\to j\).
Additionally, we define \(P^{(0)} = I\) (identity matrix) and
hence \(p^{(0)}_{ii} = 1 > 0\) and \(i\to i\) for all \(i\).
Therefore, \(\to\) is a transitive and reflexive operation.

\begin{example}[{Proof}]
Reflexivity comes by definition, so suppose that \(i\to j\)
and \(j\to k\). Then \(p_{ij}^{(n)} > 0\) and \(p_{jk}^{(m)}\)
for some \(m\) and \(n\). Hence

\begin{equation*}
\begin{aligned}
p_{ik}^{(m+n)}
&= P(X_{m+n} = k | X_0 = i)
\\&= \sum_{j'} P(X_{m+n} =k \text{ and } X_{n} = j' | X_0 = i)
\\&\geq P(X_{m+n} =k \text{ and } X_{n} = j | X_0 = i)
\\&= P(X_{m+n} =k | X_{n} = j | X_0 = i)P(X_{n} = j | X_0 = i)
\\&= P(X_{m+n} =k | X_{n} = j)P(X_{n} = j | X_0 = i)
\\&= P(X_{m} =k | X_{0} = j)P(X_{n} = j | X_0 = i)
\\&= p_{jk}^{(m)}p_{ij}^{(n)}
\\&> 0
\end{aligned}
\end{equation*}
\end{example}

Next, we say that \(i\) communicates with \(j\) iff
\(i \to j\) and \(j\to i\). We then write \(i \comm j\).
Then, by definition \(\comm\) is symmetric and since \(\to\)
is both reflexive and transitive, so is \(\comm\).
Then \(\comm\) is an equivalence relation and we can talk about
the equivalence classes of states. We call these classes \emph{communication classes}.
If there is only one communicating class, we say that the chain is \emph{irreducible}
otherwise, we say that it is \emph{reducible}.

\section{Closed}
\label{develop--math3278:markov-chains:page--index.adoc---closed}

We say that a communicating class \(C\) is \emph{closed} if for all \(i, j\).

\begin{equation*}
\forall n \geq 0: [i \in C \text{ and } j \notin C] \implies p_{ij}^{(n)} =0
\end{equation*}

That is, no state outside of \(C\) is accessible from within \(C\).
Furthermore, if \(C = \{i\}\) is closed then state \(i\) is said to be \emph{absorbing}

\section{Recurrent and Transient}
\label{develop--math3278:markov-chains:page--index.adoc---recurrent-and-transient}

Let

\begin{equation*}
f_{ii} = P(X_n = i,\text{ for some } i \geq 1 \ | \ X_0 = i)
\end{equation*}

Then state \(i\) is \emph{recurrent} if \(f_{ii} = 1\)
and \emph{transient} otherwise. Furthermore \(i\)
is \emph{positive recurrent} if the expected return time is finite otherwise
\(i\) is \emph{null recurrent}.

\begin{admonition-note}[{}]
For finite markov chains, all recurrent states are positive recurrent.
\end{admonition-note}

Additionally, if \(i\) is recurrent and \(i \comm j\), \(j\)
is also recurrent. Hence recurrence/transience is a class property.

Finally, if \(j\) is accessible from \(i\) and \(i\)
is recurrent, then \(i\) must be accessible from \(j\). Hence
a recurrent class must be closed. Conversely, if the class containing
\(i\) and \(j\) is closed, the its states are all recurrent.

\section{Periodicity}
\label{develop--math3278:markov-chains:page--index.adoc---periodicity}

Consider any state \(i\) and the set of all paths from \(i\)
to \(i\). Then the \emph{period} of \(i\) is defined as the gcd
of the lengths of these paths. We denote the period of \(i\)
as \(d(i)\) and if there is no path from \(i\) to \(i\),
\(d(i) = \infty\). Then

\begin{itemize}
\item If \(d(i) =1\), we say that \(i\) is \emph{aperiodic}
\item If \(d(i) > 1\), we say that \(i\) is \emph{periodic} with period \(i\)
\end{itemize}

Then if \(i\comm j\), \(d(i) = d(j)\). To see this,
let \(n\) be the be the gcd of the lengths if
all paths from \(j\) to \(j\) which pass through \(i\).
Then both \(d(i)\) and \(d(j)\) divide \(n\). Then the length
of any path from \(i\) to \(i\) passing through \(j\)
must be of length \(xn + yd(j)\) and since \(d(i)|n\), \(d(i)|d(j)\)
and similarly, \(d(j) | d(i)\). Therefore, the periodicity
is a class property.

\section{Limiting distribution}
\label{develop--math3278:markov-chains:page--index.adoc---limiting-distribution}

Firstly, we define a state which is both aperiodic and positive recurrent
to be \emph{ergodic}. Then if a markov chain is ergodic and irreducible,
\(\lim_{n\to\infty} p_{ij}^{(n)}\) exists and does not depend on \(i\).
Furthermore, if we define

\begin{equation*}
\pi_j = \lim_{i\to \infty} p_{ij}^{(n)}
\end{equation*}

Then \(\vec{\pi} = (\pi_j)\) is the solution to the following two equations

\begin{equation*}
\vec{\pi} = \vec{\pi} P \quad\text{and}\quad
\sum_j \pi_j = 1
\end{equation*}

This solution is called the \emph{limiting distribution} of the markov chain.

Additionally, if \(P\) is doubly stochastic, \(\pi_j = \frac{1}{N}\)
where \(N\) is the number of states.

\subsection{Ergodic classes}
\label{develop--math3278:markov-chains:page--index.adoc---ergodic-classes}

If you want to determine \(\pi_j\) and state \(j\)
belongs to an ergodic class, then we could apply the above theorem on the
sub markov chain (ie by extracting the necessary rows and columns).

\subsection{Transient states}
\label{develop--math3278:markov-chains:page--index.adoc---transient-states}

If state \(j\) is transient \(\pi_j = \lim_{n\to\infty} p_{ij}^{(n)} = 0\).
However, if \(C\) is an ergodic class with state \(k\), then

\begin{equation*}
\lim_{n\to\infty} p_{jk}^{(n)} = \pi_{j}(C) \pi_k
\end{equation*}

where \(\pi_j(C)\) is the probability that the markov chain would be
absorbed into \(C\) if it starts at state \(j\).
\end{document}
