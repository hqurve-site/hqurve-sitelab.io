\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Systems of Linear Differential Equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Consider the following system of first order differential equations

\begin{equation*}
\begin{aligned}
x_1'(t) &= a_{11}(t)x_1(t) + a_{12}(t)x_2(t) + \cdots a_{1n}(t)x_n(t) + b_1(t)\\
x_2'(t) &= a_{21}(t)x_1(t) + a_{22}(t)x_2(t) + \cdots a_{2n}(t)x_n(t) + b_2(t)\\
&\vdots\\
x_n'(t) &= a_{n1}(t)x_1(t) + a_{n2}(t)x_2(t) + \cdots a_{nn}(t)x_n(t) + b_n(t)\\
\end{aligned}
\end{equation*}

where the \(a_{ij}\) and \(b_i\) are continuous. Then, the above may be more compactly
written as

\begin{equation*}
X'(t) = A(t) X(t) + B(t)
\end{equation*}

where

\begin{equation*}
X(t) = \begin{bmatrix}
x_1(t) \\ x_2(t) \\ \vdots \\ x_n(t)
\end{bmatrix}
,\quad
A(t) = \begin{bmatrix}
a_{11}(t) & a_{12}(t) &\cdots & a_{1n}(t)\\
a_{21}(t) & a_{22}(t) &\cdots & a_{2n}(t)\\
\vdots &  & \ddots & \\
a_{n1}(t) & a_{n2}(t) &\cdots & a_{nn}(t)\\
\end{bmatrix}
,\quad
B(t) = \begin{bmatrix}
b_1(t) \\ b_2(t) \\ \vdots \\ b_n(t)
\end{bmatrix}
\end{equation*}

Terminology

\begin{description}
\item[Perturbed] When in the above form, \(B\) is called the \emph{perturbed term} and if \(B\neq 0\),
    the system is called \emph{perturbed} (or \emph{non-homoegenous}).
    However if \(B=0\), the system is called \emph{unperturbed} (or \emph{homoegenous}).
\item[Autonomous] If \(A(t)\) is constant, the system is called \emph{autonomous}.
\end{description}

\begin{theorem}[{Existence and Uniqueness}]
If \(A\) and \(B\) are continuous on \(I\) then the following IVP
has a unqiue solution

\begin{equation*}
X(t) = X_0 + \int_{t_0}^t A(s)X(s) + B(s) \ ds
\end{equation*}

\begin{proof}[{}]
From the non-local existence theorem for systems of differential equations,
we only need to show that

\begin{equation*}
\vec{f}(t, X) = A(t)X + B(t)
\end{equation*}

is Lipschitz.
However, this is clear to see since

\begin{equation*}
\left\|\vec{f}(t, X) - \vec{f}(t, Y)\right\|
= \left\|A(t)X - A(t)Y\right\|
\leq \|A(t)\| \ \|X-Y\|
\end{equation*}

Since \(A\) is continuous in closed interval \(I\), \(A\)  must be bounded
by some \(K\) and hence the \(\vec{f}\) is Lipschitz.

Therefore, by the non-local existence theorem, the IVP has a unique solution.
\end{proof}
\end{theorem}

\section{Homoegenous}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---homoegenous}

In this section, we consider the homogeneous system of linear differential equations.

\begin{lemma}[{Test for linear independence}]
Suppose \(X_1, \ldots X_n\) are solutions of the homogeneous system
such that

\begin{equation*}
X_i(t_0) = E_i
\end{equation*}

are linearly independent for some \(t_0\). Then, the \(X_1, \ldots X_n\) are
also linearly independent.

\begin{proof}[{}]
Simply take a linear combination of \(X_i\) and evaluate at \(t_0\).
By the linear independence of the \(E_i\), the coefficients must be all zero.
\end{proof}
\end{lemma}

\begin{lemma}[{Characterization of linearly independent solutions}]
Suppose that \(X_1, \ldots X_n\) are linearly independent solutions of the homogeneous
system.
Then for each \(t_0\), the \(X_i(t_0)\) are linearly independent.

\begin{proof}[{}]
Consider some \(t_0\) and \(c_1, \ldots c_n\) such that

\begin{equation*}
c_1X_1(t_0) + c_2X_2(t_0) + \cdots + c_nX_n(t_0) = 0
\end{equation*}

Then, by the superposition principle, \(Y = c_1X_1 + \cdots c_nX_n\)
is also a solution and has \(Y(t_0) = 0\).
Then, by existence and uniqueness, we have that \(Y\) is the zero solution.
Therefore

\begin{equation*}
c_1X_1 + \cdots c_nX_n = Y = 0
\end{equation*}

and by linear independence of the \(X_i\), \(c_1=\cdots =c_n = 0\).
\end{proof}
\end{lemma}

\begin{lemma}[{Existence of linearly independent solutions}]
Consider the homogeneous system with \(n\) equations.
Then, there exists \(X_1, \ldots X_n\) which are linearly independent
solutions of the homogeneous system.

\begin{proof}[{}]
Let each \(X_i\) be the solution of the following IVP

\begin{equation*}
X'(t) = A(t)X(t), \quad X(t_0) = E_i
\end{equation*}

for some fixed \(t_0\) where the \(E_i\) are the standard basis vectors.
Then, by the existence theorem, each of these \(X_i\) exist.
Furthermore, since there \(E_i\) are linearly independent,
the \(X_i\) are also linearly independent.
\end{proof}
\end{lemma}

\begin{theorem}[{Basis of solutions}]
Consider the homogeneous system with \(n\) equations.
If \(X_1, \ldots X_n\) are linearly independent solutions,
then \(X\) is a solution iff its it a linear combination of the \(X_i\).
Therefore, the solution set the homogeneous system is a vector space
with \(n\) dimensions.

\begin{proof}[{}]
Let \(S\) be the set of solutions of the homogeneous system.
Then, by the superposition principle, \(S\) is a subspace and is itself a vector
space.

We already know that the \(X_1, \ldots X_n\) are linearly independent.
We only need to show that they span \(S\).
Suppose that \(Y\) is in \(S\) and consider arbitrary \(t_0\).
Then, by the linear independence of the \(X_i\), the \(X_i(t_0)\) are also linearly
independent and form a basis for \(\mathbb{R}^n\).
Therefore, there exists \(c_1, \ldots c_n\) such that

\begin{equation*}
Y(t_0) = c_1X_1(t_0) + \cdots + c_nX_n(t_0)
\end{equation*}

Since \(Y\) and \(c_1X_1 + \cdots c_nX_n\) are solutions to the following IVP

\begin{equation*}
X'(t) = A(t)X(t), \quad X(t_0) = c_1X_1(t_0) + \cdots + c_nX_n(t_0)
\end{equation*}

by uniqueness of solution, \(Y = c_1X_1 + \cdots c_nX_n\) and \(S\)
is the span of the \(X_i\).
Therefore we have shown that the \(X_i\) are a basis for \(S\)
and the result follows.
\end{proof}
\end{theorem}

Using the above theorem, we can therefore speak of the \emph{fundamental matrix}.
If \(\vec{\phi}_1, \ldots \vec{\phi}_n\) are \(n\) linearly independent solutions
of the homogeneous system, then any solution is the linear combination of these vectors.
Therefore, it is useful to write the \(\vec{\phi}_i\) as columns of a matrix.
We call such a matrix a \emph{fundamental matrix} since any solution may be written
as the product of this matrix and a constant vector.

\begin{equation*}
\Phi(t) = \begin{bmatrix}
\vec{\phi}_1 & \vec{\phi}_2 & \cdots & \vec{\phi}_n
\end{bmatrix}
= \begin{bmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{bmatrix}
\end{equation*}

Note that \(\Phi\) also satisfies the homogeneous system since
each of its columns satisfy the system.

\begin{description}
\item[Solution matrix] If the \(\vec{\phi}_1, \ldots \vec{\phi}_n\) are not linearly independent, we simply
    call the matrix the \emph{solution matrix}.
\item[Standard fundamental matrix] If \(\Phi(t_0) = E\) (the identity matrix),
    \(\Phi\) is called the \emph{standard fundamental matrix}.
\end{description}

We also define the \emph{Wronskian} as

\begin{equation*}
W(t) = \det \Phi(t)
\end{equation*}

Although this may seem different from the definition of Wronskian in
\myautoref[{}]{develop--math6120:ROOT:page--linear.adoc}, they are the same when we convert the higher order differential
equation to a system.

\begin{theorem}[{Abel's Identity}]
Let \(\Phi\) be a solution matrix of the homogeneous system

\begin{equation*}
X'(t) = A(t)X(t)
\end{equation*}

on interval \(I\) containing \(t_0\).
Then

\begin{equation*}
\det\Phi(t) = \det \Phi(t_0) \exp\left[\int_{t_0}^t \tr A(s) \ ds\right ]
\end{equation*}

where \(\tr A(s) = \sum_{i=1}^n a_{ii}(s)\) is the trace of \(A(s)\).

\begin{proof}[{}]
As shown in \myautoref[{}]{develop--math6120:appendix:page--vector-functions.adoc---determinants},

\begin{equation*}
\frac{d}{dt} \det \Phi(t) =
\begin{vmatrix}
\phi_{11}' & \phi_{12}' & \cdots & \phi_{1n}'\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
+
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21}' & \phi_{22}' & \cdots & \phi_{2n}'\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
+\cdots+
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1}' & \phi_{n2}' & \cdots & \phi_{nn}'\\
\end{vmatrix}
\end{equation*}

Focus on \(\phi_{ij}\). Since \(\vec{\phi}_j\) is a solution of the homogeneous
system, we have that

\begin{equation*}
\vec{\phi}_j' = A(s)\vec{\phi}_j
\implies \phi_{ij}' = \sum_{k=1}^n a_{ik}\phi_{kj}
\end{equation*}

Therefore

\begin{equation*}
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\vdots & & \ddots & \\
\phi_{i1}' & \phi_{i2}' & \cdots & \phi_{in}' \\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
=
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\vdots & & \ddots & \\
\sum_{k=1}^n a_{ik}\phi_{k1} & \sum_{k=1}^n a_{ik}\phi_{k2} & \cdots & \sum_{k=1}^n a_{ik}\phi_{kn} \\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
=
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\vdots & & \ddots & \\
a_{ii}\phi_{i1} & a_{ii}\phi_{i2} & \cdots & a_{ii}\phi_{in} \\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
= a_{ii}\det\Phi(t)
\end{equation*}

and hence

\begin{equation*}
\frac{d}{dt}\det\Phi(t)
= \sum_{i=1}^n a_{ii} \det \Phi(t)
= \left(\tr A(t)\right)\left(\det\Phi(t)\right)
\end{equation*}

After solving the above differential equation, we obtain the desired result
\end{proof}
\end{theorem}

\begin{corollary}[{}]
A solution matrix \(\Phi\) of a homogeneous system is a fundamental matrix
iff \(\det \Phi(t) \neq 0\) on \(I\).
\end{corollary}

Note that the above corollary also tells us whether a given matrix is a solution matrix.
For example, consider the matrix

\begin{equation*}
\begin{bmatrix}
f_1 & f_2\\
0 & 0
\end{bmatrix}
\end{equation*}

where \(f_1\) and \(f_2\) are linearly independent.
This implies that the columns are also linearly independent. However,
the determinant of the above matrix is always zero.
This may seem like a contradiction to the above corollary, but rather it implies that
the premise of the theorem is not satisfied. That is, the above
matrix is not a solution matrix for any homogeneous system.

\begin{theorem}[{Fundamental matrices form an orbit when acted on by the general linear group on the right}]
Let \(S\) be the set of fundamental matrices of the homogeneous system

\begin{equation*}
X' = A(t)X
\end{equation*}

Then, \(S \neq 0\) and

\begin{itemize}
\item If \(\Phi \in S\), then for any invertible constant matrix \(C\), \(\Phi C \in S\)
\item If \(\Phi_1, \Phi_2 \in S\), there exists an invertible constant matrix
    \(C\) such that \(\Phi_1 = \Phi_2 C\)
\end{itemize}

\begin{proof}[{}]
First let \(\Phi \in S\) and \(C\) be an invertible constant matrix.
Then,

\begin{equation*}
(\Phi C)' = \Phi' C = A \Phi C = A (\Phi C)
\end{equation*}

and \(\det (\Phi C) = \det \Phi \det C \neq 0\).
Therefore \(\Phi C \in S\).

Next, suppose that \(\Phi_1, \Phi_2 \in S\).
Since \(\det \Phi_1 \neq 0\), we can define

\begin{equation*}
\Psi = \Phi_1^{-1}\Phi_2
\end{equation*}

Then,

\begin{equation*}
A(t)\Phi_2(t) = \Phi_2'(t) = (\Phi_1(t) \Psi(t) )'
= \Phi_1(t)\Psi'(t) + \Phi_1'(t)\Psi(t)
= \Phi_1(t)\Psi'(t) + A(t)\Phi_1(t)\Psi(t)
= \Phi_1(t)\Psi'(t) + A(t)\Phi_2
\end{equation*}

Therefore

\begin{equation*}
\Phi_1(t)\Psi'(t) = 0 \implies \Psi'(t) = 0 \implies \Psi \text{ is constant}
\end{equation*}

Therefore we get the desired result.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
If \(\Phi\) is a standard fundamental matrix with \(t_0=0\) of the following
autonomous system

\begin{equation*}
X'(t) = AX(t)
\end{equation*}

on interval \(I\)
where \(A\) is a constant matrix, then

\begin{equation*}
\forall s,t \in I: \Phi(s+t) = \Phi(s)\Phi(t)
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
In this proof \(t\) is treated as a constant.
\end{admonition-note}

Let

\begin{equation*}
Z_1(s) = \Phi(s+t)
,\quad\text{and}\quad
Z_2(s) = \Phi(s)\Phi(t)
\end{equation*}

Then, both \(Z_1\) and \(Z_2\) satisfy the following IVP

\begin{equation*}
Z' = AZ \quad\text{where } Z(0) = \Phi(t)
\end{equation*}

since

\begin{equation*}
\begin{aligned}
Z_1'(s) &= \frac{d}{ds}\Phi(s+t) = \Phi'(s+t) \frac{d}{ds}(s+t) = A\Phi(s+t)(1) = AZ_1(s)
\\
Z_2'(s) &= \Phi'(s)\Phi(t) = A\Phi(s)\Phi(t) = AZ_2(s)
\end{aligned}
\end{equation*}

Then, by uniqueness, \(Z_1=Z_2\) and we have the desired result.

\begin{admonition-note}[{}]
The matrix system has a unique solution since the columns may be treated individually
and each have a unique solution.
\end{admonition-note}
\end{proof}
\end{theorem}

\subsection{Autonomous Systems}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---autonomous-systems}

Consider the system

\begin{equation*}
X' = AX
\end{equation*}

where \(A\) is a constant matrix.
Then, a fundamental matrix for this system is simply \(\Phi(t) = e^{tA}\)
(the matrix exponential).

\begin{proof}[{}]
Notice that \(\Phi(t)\) is indeed a solution matrix since

\begin{equation*}
\Phi'(t)
= \frac{d}{dt} \left(E + \sum_{n=1}^\infty \frac{(tA)^n}{n!}\right)
= \frac{d}{dt} \left(tA + \sum_{n=2}^\infty \frac{t^nA^n}{n!}\right)
= A + \sum_{n=2}^\infty \frac{t^{n-1}A^{n}}{(n-1)!}
= A\left(E + \sum_{n=1}^\infty \frac{t^{n}A^{n}}{n!}\right)
= A\Phi(t)
\end{equation*}

Also, since \(\det \Phi(0) = \det E = 1 \neq 0\), \(\Phi(t)\) is a fundamental matrix.
\end{proof}

\subsubsection{Non-constant}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---non-constant}

Note that in general, \(e^{\int A(s) \ ds}\) is not a solution.
A counterexample provided by
\href{https://math.stackexchange.com/a/2047}{Tobias Kienzler on stack exchange} is
\(A(s) = \begin{bmatrix}\cos t & \sin t \\ \sin t & -\cos t\end{bmatrix}\).
In this case

\begin{equation*}
\Phi(t) = \exp\int A(s) \ ds = \begin{bmatrix}
\sin t \sinh t + \cosh t & - \cos t \sinh t\\
-\cos t \sinh t & -\sin t \sinh t + \cosh t\\
\end{bmatrix}
\end{equation*}

but

\begin{equation*}
\Phi'(t) = \begin{bmatrix}
\sin t\cosh t + \cos t\sinh t + \sinh t & -\cos t \cosh t + \sin t \sinh t\\
-\cos t\cosh t + \sin t \sinh t & -\sin t\cosh t - \cos t\sinh t + \sinh t\\
\end{bmatrix}
\end{equation*}

and

\begin{equation*}
A(t)\Phi(t) = \begin{bmatrix}
\cos t \cosh t & - \sinh t + \cosh t \sin x\\
\sinh t + \sin t \cosh t & -\cos t \cosh t
\end{bmatrix}
\end{equation*}

The problem is that \(A\) and its integral do not commute.
For this reason, \(\frac{d}{dt}\left(\int A(t) \ dt\right)^n \neq n A(t) \left(\int A(t) \ dt\right)^{n-1}\)
and the system is not satisfied.
More explicitly if \(B(t) = \int A(t) \ dt\)

\begin{equation*}
\frac{d}{dt} B(t)^n
= B'(t)B(t)^{n-1} + B(t)B'(t) B(t)^{n-2} + \cdots B(t)^{n-1}B'(t)
\end{equation*}

since \(B\) and \(B'\) do not commute, we do not get the desired result.

\subsection{Finding the matrix exponential}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---finding-the-matrix-exponential}

Although the solution to the autonomous system is quite simple, there is hidden complexity.
Primarily, how do we find the matrix exponential.
When dealing with scalar valued functions, we usually leave the exponential \(e^{\lambda t}\) as is since
it is not possible to meaningly simplify further.
However, it is possible to obtain simpler results for matrix exponentials.

Before applying any overly complex results, first see if the powers of \(A\)
form a nice sequence such that the infinite series of the exponential is easy to compute.

The simplest example is that of the diagonal matrix, in this case

\begin{equation*}
\exp\begin{bmatrix}
\lambda_1 t & & & \\
& \lambda_2 t & &\\
 && \ddots & \\
&&& \lambda_n t\\
\end{bmatrix}
=
\begin{bmatrix}
e^{\lambda_1 t} & & & \\
& e^{\lambda_2 t} & &\\
 && \ddots & \\
&&& e^{\lambda_n t}\\
\end{bmatrix}
\end{equation*}

Furthermore, if \(A = PDP^{-1}\) for some diagonal matrix \(D\) (ie \(A\) is diagonalizable),

\begin{equation*}
\exp At = P (\exp Dt) P^{-1}
\end{equation*}

Therefore, if \((\lambda_i, V_i)\) are \(n\) eigenpairs of \(A \in \mathbb{R}^{n\times n}\), the general solution is

\begin{equation*}
c_1e^{\lambda_1 t} V_1 + c_2e^{\lambda_2 t} V_2
+ \cdots + c_ne^{\lambda_n t} V_n
\end{equation*}

However, this is not always possible since the geometric multiplicity of and eigenvalue may
be less than the algebraic multiplicity.
In such a case, we must resort to \emph{generalized eigenvectors}.

\subsection{Generalized Eigenvectors}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---generalized-eigenvectors}

\begin{admonition-tip}[{}]
Please see \url{https://en.wikipedia.org/wiki/Jordan\_normal\_form\#Generalized\_eigenvectors}
\end{admonition-tip}

A vector \(V\) is called a \emph{generalized eigenvector} of \(A\) with \emph{rank} \(i\) and
eigenvalue \(\lambda\) if

\begin{equation*}
(A-\lambda E)^{i}V = \vec{0}\quad\text{and}\quad (A-\lambda E)^{i-1}V \neq \vec{0}
\end{equation*}

Using this definition, a regular eigenvector is a generalized eigenvector of rank \(1\).

Now, by letting \(Y(t) = e^{-\lambda t}X(t)\), we rewrite the system as

\begin{equation*}
Y'(t) = \frac{d}{dt}(e^{-\lambda t}X(t)) = e^{-\lambda t}X'(t) -\lambda e^{-\lambda t}X(t)
= e^{-\lambda t}\left(AX(t) - \lambda X(t)\right) = (A-\lambda E)Y(t)
\end{equation*}

Then, if \(y_0, y_1, \ldots\) form the coefficients of the taylor series expansion of \(Y(t)\), we obtain that

\begin{equation*}
y_i = (A-\lambda E)^i y_0
\end{equation*}

Therefore, if \(y_0\) is a generalized eigenvector of rank \(r\), the taylor series is finite
and

\begin{equation*}
Y(t) = \sum_{i=0}^r \frac{t^i}{i!}\left(A-\lambda E\right)^i\vec{y}_0
\end{equation*}

and

\begin{equation*}
X(t) = e^{\lambda t}\sum_{i=0}^r \frac{t^i}{i!}\left(A-\lambda E\right)^i\vec{y}_0
\end{equation*}

is a solution to the original equation.

Therefore, the plan is to find all generalized eigenvectors of \(A\)
and generate solutions using the above formula.

Note that it is not obvious that \(n\) generalized eigenvectors exist.
However, the following lemma assures this.

\begin{lemma}[{Existence of generalized eigenvectors}]
Let \(A\) be a matrix with eigenvalue \(\lambda\) with algebraic multiplicity \(m\).
Then, if the equation

\begin{equation*}
(A-\lambda E)^{j} V = \vec{0}
\end{equation*}

has \(k < m\) linear independent solutions,
the equation

\begin{equation*}
(A-\lambda E)^{j+1} V = \vec{0}
\end{equation*}

has at least \(k+1\) linearly independent solutions.
\end{lemma}

\subsection{Putzar Algorithm}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---putzar-algorithm}

Putzer's algorithm provides a conceptually simple way for computing
the matrix exponential

\begin{theorem}[{}]
Let \(\lambda_1, \ldots \lambda_n\) be the spectrum of eigenvectors
of matrix \(A\).
Then

\begin{equation*}
e^{At} = \sum_{k=0}^{n-1} p_{k+1}(t) M_k
\end{equation*}

where

\begin{equation*}
M_0 = E,
\quad
M_k = \prod_{i=1}^k(A - \lambda_i E)
\end{equation*}

and \(\vec{p}\) is a solution to

\begin{equation*}
p'(t) = \begin{bmatrix}
\lambda_1 & 0 & 0 & \cdots & 0 & 0\\
1 & \lambda_2 & 0 & \cdots & 0 & 0\\
0 & 1 & \lambda_3 & \cdots & 0 & 0\\
\vdots & & & \ddots & \vdots & \vdots\\
0 & 0 & 0 & \cdots & 1 & \lambda_n
\end{bmatrix}p(t),
\quad
p(0) = \begin{bmatrix}
1 \\ 0 \\ \vdots \\ 0
\end{bmatrix}
\end{equation*}

\begin{admonition-note}[{}]
The proof simply checks if the above form satisfies \(X'=AX\) and \(X(0)=E\).
Then, by uniqueness, \(X=e^{At}\). This proof also uses the Cayley-Hamilton theorem.

I would not add this proof until I find a good motivation for the above form.
I find the proof that it is a solution boring, what is more interesting is the development
of the solution.
\end{admonition-note}
\end{theorem}

\section{Non-homogeneous}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---non-homogeneous}

The method of solving non-homogeneous linear systems of differential equations is quite similar to solving
non-homogeneous higher order linear differential equations.
We solve it in two stages:

\begin{itemize}
\item find the general solution to the homogeneous system
\item find a particular solution to the system
\end{itemize}

We have already spoken about solutions to the homogeneous system, so now we would consider
the non-homogeneous system.
We use variation of parameters.
That is, if \(\Phi(t)\) is a fundamental matrix, we try \(\Phi(t)\vec{u}(t)\) for some vector function \(\phi{u}\).
We obtain the following lemma

\begin{lemma}[{Particular solution}]
If \(\Phi(t)\) is a fundamental matrix for the homogeneous system,
the solution to the IVP

\begin{equation*}
X' = A(t)X + B(t)
,\quad
X(t_0)=0
\end{equation*}

is given by

\begin{equation*}
\psi_p(t) = \int_{t_0}^t \Phi(t)\Phi^{-1}(s) B(s) \ ds
\end{equation*}

\begin{proof}[{}]
If we let \(\psi_p(t) = \Phi(t)u(t)\) and assume that it is a solution to the IVP, we obtain that

\begin{equation*}
\psi_p'(t) = A(t)\psi_p(t)+B(t) = A(t)\Phi(t)u(t) + B(t)
\end{equation*}

Also, by using the product rule, we obtain that

\begin{equation*}
\psi_p'(t) = \Phi'(t)u(t) + \Phi(t)u'(t) = A(t)\Phi(t)u(t) + \Phi(t)u'(t)
\end{equation*}

Therefore

\begin{equation*}
\Phi(t)u'(t) = B(t) \implies u(t) = u(t_0) + \int_{t_0}^t \Phi^{-1}(s)B(s) \ ds = \int_{t_0}^t \Phi^{-1}(s)B(s) \ ds
\end{equation*}

since \(u(t_0) = \Phi^{-1}(t_0)\psi_p(t_0) = \Phi^{-1}(t_0)\vec{0} = \vec{0}\)
and we get the desired result.

Also, by construction of \(u\) we have ensured that the desired form is indeed a solution of the IVP.

\begin{admonition-note}[{}]
If we specified that \(\psi_p(t_0) = X_0\), we could have also determined a solution
to the general IVP.
\end{admonition-note}
\end{proof}
\end{lemma}

\begin{lemma}[{Complementary solution}]
If \(\Phi(t)\) is a fundamental matrix for the homogeneous system,
the solution to the IVP

\begin{equation*}
X' = A(t)X
,\quad
X(t_0)=X_0
\end{equation*}

is given by

\begin{equation*}
\psi_c(t) = \Phi(t)\Phi^{-1}(t_0) X_0
\end{equation*}

See above lemma for proof.
\end{lemma}

\begin{theorem}[{}]
If \(\Phi(t)\) is a fundamental matrix for the homogeneous system,
the solution to the IVP

\begin{equation*}
X' = A(t)X + B(t)
,\quad
X(t_0)=X_0
\end{equation*}

is given by

\begin{equation*}
\psi(t) = \psi_c(t) + \psi_p(t)
\end{equation*}

where \(\psi_c\) and \(\psi_p\) are as defined in the previous two lemmas.
Concisely,

\begin{equation*}
\psi(t) = \Phi(t)\Phi^{-1}(t_0) X_0 + \int_{t_0}^t\Phi(t)\Phi^{-1}(s) B(s)\ ds
\end{equation*}
\end{theorem}

\subsection{Periodic}
\label{develop--math6120:ROOT:page--systems-of-linear.adoc---periodic}

In this section, we seek to determine when solutions of perturbed and non-perturbed systems
have period solutions.

\begin{admonition-note}[{}]
In this section, we assume that the coefficient matrix \(A(t)\) is periodic with the period of interest.
\end{admonition-note}

\begin{theorem}[{}]
A solution of the following system

\begin{equation*}
X'=A(t)X + B(t)
\end{equation*}

is periodic with period \(\omega\) iff

\begin{equation*}
X(\omega) = X(0)
\end{equation*}

\begin{proof}[{}]
The forward statement is clearly true. Now, suppose that \(\phi\) is a solution
to the homogeneous system and \(\phi(\omega) = \phi(0)=X_0\).
Let \(\psi(t) = \phi(t+\omega)\)
Then,

\begin{equation*}
\forall t: \psi'(t) = \phi'(t+\omega) =  A(t+\omega)\phi(t+\omega) + B(t+\omega)= A(t)\phi(t+\omega) + B(t)= A(t)\psi(t) + B(t)
\end{equation*}

Therefore both \(\phi\) and \(\psi\) are solutions to the below IVP

\begin{equation*}
X' = A(t)X + B(t), \ X(0) = X_0
\end{equation*}

and by uniqueness \(\phi(t) = \psi(t) = \phi(t+\omega)\).
\end{proof}
\end{theorem}

\begin{corollary}[{}]
Consider the below system

\begin{equation*}
X' = AX
\end{equation*}

where \(A\) is a constant matrix.
Then the system has a non-trivial periodic solution with period \(\omega\)
iff \(E-e^{A\omega}\) is singular.

\begin{admonition-note}[{}]
We state non-trivial since the zero solution is periodic with all periods.
\end{admonition-note}

\begin{proof}[{}]
From previous discussions, we know that the solution of the above system is given by

\begin{equation*}
X(t) = e^{At}C
\end{equation*}

for some constant vector \(C\).
We want that there exists non-zero \(C\) such that solution is period.

Then, from the above theorem, \(X\) is periodic iff

\begin{equation*}
C = X(0)= X(\omega) = e^{A\omega}C
\end{equation*}

iff \((E-e^{A\omega}) C = \vec{0}\). Therefore, there exists a non-trivial \(C\)
iff \(E-e^{A\omega}\) is singular.
\end{proof}
\end{corollary}

\begin{corollary}[{}]
Consider the below system

\begin{equation*}
X' = AX + B(t)
\end{equation*}

where \(A\) is a constant matrix.
Then, the above system has a unique periodic solution with period \(\omega\)
iff \(E-e^{A\omega}\) is non-singular.

\begin{admonition-note}[{}]
Stating anymore than this becomes a bit difficult since we are dealing with matrices.
But, it is possible to also determine conditions when the system has periodic solutions.
However, these conditions have to do with the column space of the particular solution.
\end{admonition-note}

\begin{proof}[{}]
From previous discussions, we know that the solution to the above system is given by

\begin{equation*}
X(t) = e^{At}C + \int_{0}^t e^{At}e^{-As} B(s) \ ds
\end{equation*}

for some constant vector \(C\).
This solution is periodic iff

\begin{equation*}
C = X(0) = X(\omega) = e^{A\omega}C + \int_{0}^\omega e^{At}e^{-As} B(s) \ ds
\end{equation*}

We may now consider the above as a system with variable \(C\).
Our differential system has a unique solution of period \(\omega\)
iff the above system has a unique solution for \(C\).
This happens iff \(E-e^{A\omega}\) is non-singular as the above system is equivalent to.

\begin{equation*}
(E - e^{A\omega})C = \int_{0}^\omega e^{At}e^{-As} B(s) \ ds
\end{equation*}
\end{proof}
\end{corollary}

Now, let us look at the nature of fundamental matrices for periodic systems.
The following theorem allows us to determine the solution of a periodic system by knowing
only the solution over one period.

\begin{theorem}[{}]
If \(\Phi(t)\) is a fundamental matrix of

\begin{equation*}
X' = A(t)X
\end{equation*}

then \(\Phi(t + \omega)\) is also a fundamental matrix.
Furthermore, there exists a constant matrix \(D\) and non-singular \(P(t)\) such that

\begin{equation*}
P(t+\omega) = P(t), \quad\text{and}\quad \Phi(t) = P(t)e^{tD}
\end{equation*}

Also, by applying the transformation \(X = P(t)Z\), we obtain

\begin{equation*}
Z'=DZ
\end{equation*}

\begin{proof}[{}]
Just verify that \(\det \Phi(t+\omega) \neq 0\) and \(\Phi(t+\omega)\) is indeed a solution.

Next, since \(\Phi(t+w)\) is a solution, there exists non-singular matrix
\(B\) such that

\begin{equation*}
\Phi(t+\omega) = \Phi(t)B
\end{equation*}

Also, since \(B\) is non-singular, there exists a matrix \(D\) such that \(B = e^{wD}\).
Note that the matrix logarithm exists iff the matrix is invertible
(see \url{https://en.wikipedia.org/wiki/Logarithm\_of\_a\_matrix\#Existence} ).
Now, let \(P(t) = \Phi(t)e^{-tD}\).
Then,

\begin{equation*}
P(t+\omega) = \Phi(t+\omega) e^{-(t+\omega)D} = \Phi(t)e^{wD}e^{-(t+\omega)D}
= \Phi(t)e^{-tD} = P(t)
\end{equation*}

Therefore we have found \(P\).
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Consider the following system

\begin{equation*}
X' = A(t)X
\end{equation*}

then, there exists \(D\) such that for
any fundamental matrix \(\Phi(t)\),
there exists non-singular \(Q\) such that

\begin{equation*}
\Phi(t+\omega) = \Phi(t)Qe^{\omega D}Q^{-1}
\end{equation*}

Furthermore, if \(\Phi(0) = E\), there exists a unique set of constants
\(\lambda_1, \ldots \lambda_n\) and \(r_1, \ldots r_n\) (called \emph{eigen multipliers}
and \emph{eigen exponents} of \(A\) respectively) such that

\begin{equation*}
\det \Phi(\omega) = \lambda_1\lambda_2 \cdots\lambda_n = e^{(r_1+r_2+\cdots+r_n)\omega}
\end{equation*}

Also the constants \(\lambda_1, \ldots \lambda_n\) are the eigenvalues of \(e^{\omega D}\)
and the constants \(r_1, \ldots r_n\) are the eigenvalues of \(D\)
and neither depend on the particular choice of \(D\).

\begin{proof}[{}]
Consider fundamental matrix \(\Psi(t)\).
Then, since \(\Psi(t+\omega)\) is also fundamental,
there exists non-singular \(C\) such that \(\Psi(t+\omega) = \Psi(t)C\).
Also, since \(C\) is non-singular, there exists \(D\) such that
\(C = e^{\omega D}\).

Now, consider arbitrary fundamental matrix \(\Psi\).
Then, there exists \(Q\) such that \(\Phi(t)Q = \Psi(t)\).
Therefore

\begin{equation*}
\Phi(t+\omega) = \Psi(t +\omega)Q^{-1} = \Psi(t)e^{\omega D}Q^{-1} = \Phi(t)Qe^{\omega D}Q^{-1}
\end{equation*}

Therefore, we have proven the first claim.

Note that if we had chosen a different \(\Psi(t)\), we may have obtained a different
\(D\), say \(D_1\).
However, the eigenvalues of \(e^{\omega D_1}\) and \(D_1\) would be the same
since there exists \(Q_1\) such that \(e^{\omega D_1} = Q_1e^{\omega D}Q_1^{-1} = e^{\omega Q_1DQ_1^{-1}}\)
(ie \(D\) is similar to \(D_1\) and \(e^{D}\) is similar to \(e^{D_1}\)).

Now, let \(\lambda_1, \ldots \lambda_n\) be the eigenvalues of \(e^{\omega D}\)
and suppose that \(\Phi(0) = E\).
Then, from the Jordan-canonical form of \(Qe^{e^\omega D}Q^{-1}\), we have
that

\begin{equation*}
\det \Phi(\omega) = \det (\Phi(0) Q e^{\omega D}Q^{-1}) =\det E \det Q \det(e^{\omega D}) \det Q^{-1}
= \det (e^{\omega D}) = \lambda_1\lambda_2\cdots\lambda_n
\end{equation*}

Also, if we let \(r_1, \ldots r_n\) be the eigenvalues of \(D\), we have that

\begin{equation*}
\det \Phi(\omega) = \det (e^{\omega D}) = e^{\omega \tr D} = e^{(r_1 + r_2 + \cdots +r_n)\omega}
\end{equation*}
\end{proof}
\end{theorem}
\end{document}
