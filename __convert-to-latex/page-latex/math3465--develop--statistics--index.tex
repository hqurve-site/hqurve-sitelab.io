\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Properties of Statistics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
\section{Sufficiency}
\label{develop--math3465:statistics:page--index.adoc---sufficiency}

Let \(T\) be a statistic based on the sample \(X_1, \ldots X_n\) from a population with parameter \(\theta\).
Then \(T\) is \emph{sufficient} for \(\theta\) if it contains all the information from the sample which is
necessary to estimate \(\theta\). That is, the distribution of \(X_1, \ldots X_n\) given
some specified value of \(T\) does not depend on \(\theta\).

\begin{admonition-important}[{}]
\(T\) itself is not an estimator for \(\theta\) but rather contains all the information necessary
to estimate \(\theta\). In other words, any estimator for \(\theta\) can be constructed using \(T\)
instead of the sample itself. Then immediately, if \(T\) is sufficient, any bijective mapping applied to \(T\)
is also sufficient. Additionally, it is trivially true that the sample itself is sufficient for estimating the parameter.
\end{admonition-important}

\subsection{Fisher's factorization Criterion}
\label{develop--math3465:statistics:page--index.adoc---fishers-factorization-criterion}

\begin{admonition-remark}[{}]
Yes, \myautoref[{that}]{develop--math2276:ROOT:page--block-designs.adoc---fishers-inequality} Fisher.
\end{admonition-remark}

\begin{custom-quotation}[{}][{}]
A statistic \(\vec{T}\) is sufficient for \(\vec{\theta}\) iff the joint pdf of \(X_1, \ldots X_n\)
can be written as

\begin{equation*}
f(\vec{x}; \vec\theta) = u(\vec{x})v(\vec{T};\vec{\theta})
\end{equation*}
\end{custom-quotation}

For understanding, \(v\) can be seen as the joint pdf of \(\vec{T}\) and \(u\) is the conditional
pdf of \(\vec{x}\) given \(\vec{T}\). (Note that \(u\) and \(v\) need not be these functions,
but if they are, this works).

\subsection{Exponential Family of Distributions}
\label{develop--math3465:statistics:page--index.adoc---exponential-family-of-distributions}

Let \(X\) be a random variable with pdf \(f\) which depends on parameters \(\vec{\theta}\).
Then \(X\) belongs to the \emph{exponential family} of distributions if

\begin{equation*}
f(x; \vec{\theta}) = a(\vec{\theta})b(x)e^{\vec{c}(\vec{\theta})\cdot \vec{d}(x)}
\end{equation*}

where \(c\) and \(d\) are functions which output vectors with the same length as \(\theta\).

\begin{admonition-caution}[{}]
\(x\) is a single random variable.
\end{admonition-caution}

Then, if we have a random sample of \(\{X_i\}\), we can immediately see that the joint pdf is

\begin{equation*}
\begin{aligned}
f(\vec{x};\vec{\theta})
&= \prod_{i=1}^n a(\vec\theta)b(x_i)e^{\vec{c}(\vec{\theta}) \cdot \vec{d}(x_i)}
\\&=  a(\vec\theta)^n \left(\prod_{i=1}^n b(x_i)\right) \exp\left\{\sum_{i=1}^n\vec{c}(\vec{\theta}) \cdot \vec{d}(x_i)\right\}
\\&=  \left(\prod_{i=1}^n b(x_i)\right) a(\vec\theta)^n \exp\left\{\vec{c}(\vec{\theta})\cdot \sum_{i=1}^n \vec{d}(x_i)\right\}
\end{aligned}
\end{equation*}

Then, by comparing by the Fisher factorization criterion, we see that if we let \(T = \sum_{i=1}^n \vec{d}(x_i)\),
we obtain a sufficient vector of statistics for \(\vec\theta\).

\subsubsection{Maximum likelihood estimator}
\label{develop--math3465:statistics:page--index.adoc---maximum-likelihood-estimator}

\begin{admonition-tip}[{}]
See more \myautoref[{here}]{develop--math3465:point-estimators:page--construction.adoc---maximum-likelihood-estimator}.
\end{admonition-tip}

By taking the logarithm of the above function, we get that our log
like-likelihood function is

\begin{equation*}
L(\vec{x}; \vec{\theta}) = \ln\left(\prod_{i=1}^n b(x_i)\right) + n\ln(a(\vec{\theta})) + \vec{c}(\vec{\theta})\cdot \vec{T}
\end{equation*}

And we see that \(\vec{T}\) is indeed sufficient since all the information needed to estimate \(\vec{\theta}\) is given by \(\vec{T}\).
And if we take derivatives, we see that we need to solve

\begin{equation*}
\frac{na'(\vec{\theta})}{a(\vec{\theta})} + \vec{c}'(\vec{\theta})\cdot\vec{T} = 0
\end{equation*}

where \(\vec{c}'\) is the element-wise derivative since we are just using it to avoid summation formulae.

\subsubsection{Examples}
\label{develop--math3465:statistics:page--index.adoc---examples}

The following distributions are exponential

\begin{description}
\item[Bernoulli] \(
    f(x; p)
    = p^x(1-p)^{1-x}
    = (1-p)e^{x\ln(p) -x \ln(1-p)}
    = (1-p)e^{x(\ln(p) - \ln(1-p))}
    \)
    
    Therefore a sufficient statistic for \(p\) is \(\sum X_i\)
\end{description}

\begin{description}
\item[Binomial (with \(n\) known)] \(
    f(x; p)
    = \binom{n}{x} p^{x}(1-p)^{n-x}
    = \binom{n}{x} (1-p)^n e^{x\ln(p) - x\ln(1-p)}
    = \binom{n}{x} (1-p)^n e^{x(\ln(p) - \ln(1-p))}
    \)
    
    Therefore a sufficient statistic for \(p\) is \(\sum X_i\)
\end{description}

\begin{description}
\item[Multinomial (with \(n\) and \(m\) known)] 
    
    \begin{equation*}
    \begin{aligned}
    &f(x_1, \ldots x_m; a_1,\ldots a_m)
    \\&= \binom{n}{x_1, \ldots x_m} \prod_{i=1}^m a_i^{x_i}
    \\&= \binom{n}{x_1, \ldots x_{m-1}, \left(n-\sum_{i=1}^{m-1} x_i\right) } \left(\prod_{i=1}^{m-1} a_i^{x_i}\right) \left(1-\sum_{i=1}^{m-1} a_i\right)^{n-\sum_{i=1}^{m-1} x_i}
    \\&= \binom{n}{x_1, \ldots x_{m-1}, \left(n-\sum_{i=1}^{m-1} x_i\right)} \exp\left\{\sum_{i=1}^{m-1} x_i\ln(a_i) - \sum_{i=1}^{m-1}x_i\ln\left(1-\sum_{i=1}^{m-1} a_i\right) \right\} \left(1-\sum_{i=1}^{m-1} a_i\right)^n
    \\&= \binom{n}{x_1, \ldots x_{m-1}, \left(n-\sum_{i=1}^{m-1} x_i\right)}\left(1-\sum_{i=1}^{m-1} a_i\right)^n \exp\left\{\sum_{i=1}^{m-1} x_i\left(\ln(a_i) - \ln\left(1-\sum_{j=1}^{m-1} a_j\right)\right) \right\}
    \end{aligned}
    \end{equation*}
    
    Therefore a sufficient statistic for \((a_1, \ldots a_{m-1})\) is \(\left(\sum X_{1j}, \sum X_{2j}, \ldots \sum X_{(m-1)j}\right)\) where \(X_{ij}\)
    is the \(j\)'th sample of the \(i\)'th type.
\end{description}

\begin{description}
\item[Poisson] \(
    f(x; \lambda)
    = \frac{e^{-\lambda} \lambda^x}{x!}
    = \frac{1}{x!}e^{-\lambda} e^{x\ln(\lambda)}
    \)
    
    Therefore a sufficient statistic for \(\lambda\) is \(\sum X_i\)
\end{description}

\begin{description}
\item[Normal] \(
    f(x; \mu, \sigma^2)
    = \frac{1}{\sqrt{2\pi\sigma^2}}e^{\frac{-(x-\mu)^2}{2\sigma^2}}
    = \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left\{\frac{-\mu^2}{2\sigma^2}\right\} \exp\left\{-\frac{1}{2\sigma^2}x^2 + \frac{\mu}{\sigma^2}x\right\}
    \)
    
    Therefore a sufficient statistic for \((\mu, \sigma^2)\) is \(\left(\sum X_i, \sum X_i^2\right)\).
\end{description}

\begin{description}
\item[Gamma] \(
    f(x; \alpha, \beta)
    = \frac{1}{\beta^\alpha \Gamma(\alpha)}x^{\alpha-1} e^{\frac{-x}{\beta}}
    = \frac{1}{\beta^\alpha \Gamma(\alpha)} \exp\left\{\ln(x)(\alpha-1) - x\frac{1}{\beta}\right\}
    \)
    
    Therefore a sufficient statistic for \((\alpha, \beta)\) is \(\left(\sum \ln X_i, \sum X_i\right)\) then by using
    logarithm rules, we see that an equivalent sufficient statistic is \(\left(\prod X_i, \sum X_i\right)\)
\end{description}

\begin{description}
\item[Beta] \(
    f(x; \alpha, \beta)
    = \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha)\Gamma(\beta)} x^{\alpha-1}(1-x)^{\beta - 1}
    = \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha)\Gamma(\beta)} \exp\left\{(\alpha-1)\ln(x) + (\beta-1)\ln(1-x)\right\}
    \)
    
    Therefore a sufficient statistic for \((\alpha, \beta)\) is \(\left(\sum \ln X_i, \sum \ln(1-X_i)\right)\) then by using
    logarithm rules, we see that an equivalent sufficient statistic is \(\left(\prod X_i, \prod (1-X_i)\right)\)
\end{description}

\begin{admonition-remark}[{}]
I am not including the support into the above functions. But we could
simply multiply \(b(x)\) by \(\mathbb{1}_I(x)\) where \(I\) is the support.
\end{admonition-remark}

\section{Ancillary}
\label{develop--math3465:statistics:page--index.adoc---ancillary}

Let \(T\) be a statistic based on the sample \(X_1, \ldots X_n\) from a population with
parameter \(\theta\). Then, \(T\) is an \emph{ancillary statistic} if the distribution of \(T\)
does not depend on \(\theta\).

Therefore, if \(S\) is sufficient for \(\theta\), the distribution of \(T\) given
a value for \(S\) is an ancillary statistic.

\section{Complete}
\label{develop--math3465:statistics:page--index.adoc---complete}

Let \(T\) be a statistic based on the sample \(X_1, \ldots X_n\) from a population with
parameter \(\theta\). Then, \(T\) is a \emph{complete statistic} if for any function, \(h\)

\begin{equation*}
[\forall \theta:  E(h(T)) = 0] \implies h(t) = 0
\end{equation*}

\subsection{Basu's theorem on complete sufficient statistics and ancillary statistics}
\label{develop--math3465:statistics:page--index.adoc---basus-theorem-on-complete-sufficient-statistics-and-ancillary-statistics}

If \(U\) is a complete sufficient statistic for \(\theta\) and \(W\)
is ancillary for \(\theta\), then \(U\) and \(W\) are independent.
\end{document}
