\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Interior, closure and limit points}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In previous courses, we defined interior, closure and limit points for the real line and metric spaces in general.
In this page, we give an definition for topological spaces which is compatible with previous definitions.

Let \((X, \mathcal{T})\) be a topological space. Then, we define the interior and closure of a set \(A\subseteq X\)
as below

\begin{equation*}
\interior(A) = \bigcup_{U\text{ is open and } U \subseteq A} U
\quad\text{and}\quad
\closure(A) = \bigcap_{F\text{ is closed and } A \subseteq F} F
\end{equation*}

Equivalently, we may define \(\interior(A)\) to be the largest open set in \(A\)
and \(\closure(A)\) to be the smallest closed set containing \(A\).
These two facts are easy to verify since arbitrary unions of open sets are open and arbitrary intersections of
closed sets are closed.

However, notice that these are not our usual definitions of interior and closure.
In Math2277, we defined

\begin{itemize}
\item the interior to be the set of points \(x\) which are in an open neighbourhood which is contained in \(A\)
    \myautoref[{[ref]}]{develop--math2277:ROOT:page--topological-properties-of-the-reals.adoc--interior-exterior-boundary}
\item the closure to be the set of points \(x\) for which all open neighbourhoods containing \(x\) intersect \(A\)
    \myautoref[{[ref]}]{develop--math2277:ROOT:page--topological-properties-of-the-reals.adoc---closure}
\end{itemize}

In fact, these definitions are equivalent to the ones given above.
That is,

\begin{equation*}
\interior(A) = \left\{x \in X \ \middle| \ \exists U \in \mathcal{T}: x \in U \subseteq A\right\}
\end{equation*}

\begin{equation*}
\closure(A) = \left\{x \in X \ \middle| \ \forall U \in \mathcal{T}: x \in U \implies U \cap A \neq \varnothing\right\}
\end{equation*}

\begin{proof}[{}]
The proof of the two definitions of interior being equivalent follows directly from the definition of union.

For closure, it is a bit tricky. We start with the definition of closure

\begin{equation*}
\begin{aligned}
x \in \closure(A)
&\iff \forall F \text{ closed} \text{ and } A \subseteq F: x \in F
\\&\iff \forall F \text{ closed}: A\subseteq F \implies x \in F
\\&\iff \forall U \in \mathcal{T}: A\subseteq (X - U) \implies x \in (X-U) \quad\text{by definition of closed}
\\&\iff \forall U \in \mathcal{T}: U \cap A = \varnothing \implies x \notin U
\\&\iff \forall U \in \mathcal{T}: x \in U \implies U \cap A \neq \varnothing \quad \text{by contrapositive}
\end{aligned}
\end{equation*}

Therefore we have proven the desired result.
\end{proof}

Using this alternative definition of closure, we may sometimes want to exclude the possibility of \(U \cap A = \{x\}\).
For this reason, we define limit points as

\begin{equation*}
\limit(A) = \left\{x \in X \ \middle| \ \forall U \in \mathcal{T}: x \in U \implies \exists z \neq x: z \in U \cap A\right\}
\end{equation*}

We also define the boundary of \(A\) as \(\closure(A) - \interior(A)\).

\begin{admonition-tip}[{}]
We can alternatively use basis elements when defining the closure, interior and limit points.

\begin{proof}[{}]
Let \(\mathcal{B}\) be a basis for \(X\).

The proof for interior is trivial.

For closure let \(\closure_{\mathcal{B}}(A)\) define the set obtained by using the basis elements in the second definition
of closure.
Then, \(\closure(A) \subseteq \closure_{\mathcal{B}}(A)\) as \(\mathcal{B} \subseteq \mathcal{T}\) (and hence there are fewer restrictions).
Now, suppose that \(x \in \closure_{\mathcal{B}}(A)\) and consider arbitrary \(U \in \mathcal{T}\) with
\(x \in U\). We need to show that \(U \cap A \neq \varnothing\).
Since \(\mathcal{B}\) is a basis, there exists \(B \in \mathcal{B}\) such that \(x \in B \subseteq U\).
Therefore, \(U \cap A \supseteq U \cap B \neq \varnothing\).

An analogous proof may be conducted for \(\limit(A)\).
\end{proof}
\end{admonition-tip}

\section{Properties}
\label{develop--math6620:ROOT:page--interior-closure.adoc---properties}

\begin{proposition}[{}]
Let \(A, B \subseteq X\). Then, we have the following properties of interior and closure

\begin{enumerate}[label=\arabic*)]
\item \(\interior(A) \subseteq A \subseteq \closure(A)\)
\item \(X - \interior(A) = \closure(X-A)\) and \(X- \closure(A) = \interior(X-A)\)
\item \(\interior(A) \cup \interior(B) \subseteq \interior(A\cup B)\) and \(\closure(A) \cap \closure(B) \supseteq \closure(A\cap B)\)
\item \(\interior(A) \cap \interior(B) = \interior(A\cap B)\) and \(\closure(A) \cup \closure(B) =\closure(A\cup B)\)
\end{enumerate}

The first property was proven above.
\end{proposition}

\begin{proof}[{Property 2}]
We prove \(X - \interior(A) = \closure(X-A)\) directly with an implication chain.

\begin{equation*}
\begin{aligned}
x\in X- \interior(A)
&\iff \neg \left[\exists U \in \mathcal{T}: x\in U \subseteq A\right]
\\&\iff \forall U \in \mathcal{T}: \neg \left[x\in U \subseteq A\right]
\\&\iff \forall U \in \mathcal{T}: x \notin U \text { or } U \not\subseteq A
\\&\iff \forall U \in \mathcal{T}: x \notin U \text { or } U \cap (X-A) \neq \varnothing
\\&\iff \forall U \in \mathcal{T}: x \in U \implies U \cap (X-A) \neq \varnothing \quad\text{since } p\to q \equiv \neg p \lor q
\\&\iff x \in \closure(X-A)
\end{aligned}
\end{equation*}

Therefore it follows that

\begin{equation*}
X - \closure(A) = X - \closure (X - (X-A)) = X - (X - \interior(X-A)) = \interior(X-A)
\end{equation*}
\end{proof}

For properties 3 and 4, we only prove the part with interior. Property 2 may be used to determine the parts with closure.

\begin{proof}[{Property 3}]
\begin{equation*}
\begin{aligned}
x \in \interior(A)
\implies \left[\exists U \in \mathcal{T}: x \in U \subseteq A \subseteq A\cup B\right]
\implies x \in \interior(A\cup B)
\end{aligned}
\end{equation*}

Due to symmetry \(\interior(B) \subseteq \interior(A\cup B)\) as well.
\end{proof}

\begin{proof}[{Property 4}]
\begin{equation*}
\begin{aligned}
\interior(A) \cup \interior(B)
&=
\left(\bigcup_{U\text{ is open and } U \subseteq A} U\right)
\cap \left(\bigcup_{V\text{ is open and } V \subseteq B} U\right)
\\&=
\bigcup_{U, V\text{ are open and } U \subseteq A \text{ and } V \subseteq B} U \cap V
\quad\text{since intersections distribute over unions}
\\&=
\bigcup_{W\text{ is open and } W \subseteq A \cap B} W
\\&= \interior(A \cap B)
\end{aligned}
\end{equation*}

We need to justify the third equality. We take it from either side

\begin{itemize}
\item If \(x \in \bigcup \left\{U \cap V \ \middle| \ U, V\text{ are open and } U \subseteq A \text{ and } V \subseteq B\right\}\),
    then there exists a \(W = U \cap V\) which is open and \(x \in W \subseteq A\cap B\)
    and hence \(x\) is in \(\bigcup\left\{ W \ \middle| \ W\text{ is open and } W \subseteq A \cap B\right\}\).
\item If \(x \in \bigcup\left\{ W \ \middle| \ W\text{ is open and } W \subseteq A \cap B\right\}\),
    then \(W \subseteq A\) and \(W \subseteq B\).
    Therefore \(x \in \bigcup \left\{U \cap V \ \middle| \ U, V\text{ are open and } U \subseteq A \text{ and } V \subseteq B\right\}\)
\end{itemize}
\end{proof}

\subsection{Closure on subspace}
\label{develop--math6620:ROOT:page--interior-closure.adoc---closure-on-subspace}

\begin{theorem}[{}]
Suppose that \(Y \subseteq X\) and \(Y\) has a subspace topology. Then for each \(A \subseteq Y\)

\begin{equation*}
\closure_Y(A) = \closure_X(A) \cap Y
\end{equation*}

\begin{admonition-warning}[{}]
The same is not true for interiors. For example, take \(A = Y\) where \(Y\) is not open.
Then \(\interior_Y(A) = Y\) is a strict superset of \(\interior_X(A)\).
\end{admonition-warning}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
\closure_Y(A)
&= \bigcap \left\{F' \ \middle| \ F' \text{ is closed in } Y \text{ and } A \subseteq F'\right\}
\\&= \bigcap \left\{Y-U' \ \middle| \ U' \text{ is open in } Y \text{ and } A \subseteq (Y-U')\right\}
\\&= \bigcap \left\{Y-(U \cap Y) \ \middle| \ U \text{ is open in } X \text{ and } A \subseteq (Y-(U \cap Y))\right\}
\\&= \bigcap \left\{Y-((X-F) \cap Y) \ \middle| \ F \text{ is closed in } X \text{ and } A \subseteq (Y-((X-F) \cap Y))\right\}
\end{aligned}
\end{equation*}

Notice that \(Y - ((X-F) \cap Y) = Y \cap F\) since

\begin{equation*}
\begin{aligned}
Y - ((X-F) \cap Y)
&= Y \cap (X - ((X-F) \cap Y))
\\&= Y \cap ((X - (X-F)) \cup (X - Y)) \quad\text{by De Morgan's law}
\\&= Y \cap (F \cup (X - Y))
\\&= (Y \cap F) \cup  (Y\cap (X-Y))
\\&= (Y \cap F) \cup \varnothing
\\&= Y \cap F
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
\closure_Y(A)
&= \bigcap \left\{Y-((X-F) \cap Y) \ \middle| \ F \text{ is closed in } X \text{ and } A \subseteq (Y-((X-F) \cap Y))\right\}
\\&= \bigcap \left\{Y \cap F \ \middle| \ F \text{ is closed in } X \text{ and } A \subseteq Y\cap F\right\}
\\&= Y \cap \bigcap \left\{F \ \middle| \ F \text{ is closed in } X \text{ and } A \subseteq Y\cap F\right\}
\\&= Y \cap \bigcap \left\{F \ \middle| \ F \text{ is closed in } X \text{ and } A \subseteq F\right\} \quad\text{since } A \subseteq Y
\\&= Y \cap \closure_X(A)
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\subsection{Closure as a set with limit points}
\label{develop--math6620:ROOT:page--interior-closure.adoc---closure-as-a-set-with-limit-points}

\begin{theorem}[{}]
Let \(A \subseteq X\). Then

\begin{equation*}
\closure(A) = \limit(A) \cup A
\end{equation*}

Therefore, we may often refer to the closure as a set with its limit points.

\begin{proof}[{}]
We use an implication chain

\begin{equation*}
\begin{aligned}
x \in \closure(A)
&\iff \forall U \in \mathcal{T}: x \in U \implies U \cap A \neq \varnothing
\\&\iff \forall U \in \mathcal{T}: x \in U \implies (U \cap A = \{x\}) \text{ or } \left[\exists z \in U\cap A: z \neq x\right]
\\&\iff \forall U \in \mathcal{T}: x \in U \implies x \in U \cap A \text{ or } \left[\exists z \in U\cap A: z \neq x\right]
\\&\iff \forall U \in \mathcal{T}: \left[x \in U \implies x \in U \cap A\right] \text{ or } \left[x \in U\implies\exists z \in U\cap A: z \neq x\right]
\\&\iff \left[ \forall U \in \mathcal{T}: x \in U \implies x \in U \cap A\right]
    \text{ or } \left[\forall U \in \mathcal{T}: x \in U \implies \exists z \in U\cap A: z \neq x\right]
\\&\iff x\in A \text{ or } x\in\limit(A)
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{corollary}[{}]
Let \(A \subseteq X\). Then,
\(A\) is closed iff \(\limit(A) \subseteq A\)
\end{corollary}

\section{Convergence}
\label{develop--math6620:ROOT:page--interior-closure.adoc---convergence}

Like metric spaces, it is also possible to define limits of sequences in topologies.

Let \(X\) be a topological space and consider a sequence
\(\{x_n\}\) of \(X\).
Then, we say that \(\{x_n\}\) \emph{converges} to \(x \in X\) if

\begin{equation*}
\forall U \in \mathcal{T}: x\in U \implies \exists N \geq 1: \forall n \geq N: x_n \in U
\end{equation*}

If we consider the indiscrete topology, then there are only two open sets, \(X\) and \(\varnothing\).
This implies that the element at which a sequence converges is not unique since \(x \in U \implies U = X\).
This is an issue since we expect that convergent sequences converge to a unique value.

The problem of non-unique convergence is mostly due to the inability of some topologies to `distinguish'
between two elements.
A naive way to solve this issue is to add the additional requirement that for any two elements,
there exists an open set which contains one but not the other. However, as shown below, this is not sufficient.

Consider the cofinite topology on an infinite \(X\). By definition for each \(x,y \in X\), \(X - \{y\}\)
is an open set. Therefore the cofininte topology satisfies the above condition.
However, any sequence \(\{x_n\}\) with (eventually) distinct elements converges to all values.
Consider arbitrary \(x \in \mathbb{R}\) and open neighbourhood \(U\) of \(x\).
Let \(N\) be the largest such that \(x_{N} \notin U\); if all of the \(x_n \in U\), take \(N=1\).
Therefore, \(\forall n \geq N+1\), \(x_n \in U\) and the sequence converges to \(x\).

Therefore, we need a stronger condition.
Instead, let us look at what is required for a sequence not to converge at a point.
Let \(x, y \in X\) and suppose that \(\{x_n\}\) converges to \(x\) but does not converge to \(y\).
Let us expand what is means to not converge.

\begin{equation*}
\exists V \in \mathcal{T}: y \in V \text{ and }\forall N\geq 1: \exists n \geq N: x_n \notin V
\end{equation*}

Equivalently, there exists infinitely many \(x_n\) which are not in \(V\).
Therefore, in order to ensure that the sequence does not converge to \(y\),
it would be sufficient to find a disjoint neighbourhoods \(U\) and \(V\)
about \(x\) and \(y\).
This property is called Hausdorff and is discussed in \myautoref[{}]{develop--math6620:ROOT:page--separation.adoc}.
Since \(x \in U\), then there exists \(N\) beyond which all \(x_n \in U\).
Also, since \(U\) and \(V\) are disjoint, it follows that all \(x_n\) beyond \(N\)
are not in \(V\). Therefore, the sequence does not converge to \(y\).

\subsection{Limit points vs limits of sequences}
\label{develop--math6620:ROOT:page--interior-closure.adoc---limit-points-vs-limits-of-sequences}

It may be clear to see that if \(\vec{x}\) is the limit of a sequence \(\{x_n\} \subseteq A\), then
either \(\vec{x} \in A\), or \(\vec{x} \in \limit(A)\).
However, the converse is not true.
That is, there exists \(\vec{x} \in \limit(A)\) which are not the limits of any sequences of \(A\).

Consider, the co-countable topology on \(\mathbb{R}\) and the set \(A = \mathbb{R} - \{0\}\).
The only facts which we use are that

\begin{itemize}
\item \(\mathbb{R}\) and \(A\) are uncountable.
\item Any open \(U \subseteq \mathbb{R}\) is uncountable.
\end{itemize}

Now, we claim that \(\closure(A) = \mathbb{R}\) but \(\{0\}\)
is not the limit of any sequence of \(A\).
Notice that \(0 \in \closure(A)\) since for any open \(U\) containing \(0\),
there must be \(U \cap A \neq \varnothing\) (otherwise it would imply that \(U\) is finite).
Next, consider arbitrary sequence \(\{x_n\}\subseteq A\).
Then the set \(A - \{x_n\}\) is is open; however, it never intersects with the sequence.
This shows that \(\{x_n\}\) does not converge; in particular, it does not converge to \(0\).

\begin{admonition-remark}[{}]
This is very interesting.
\href{https://www.youtube.com/watch?v=SyD4p8\_y8Kw}{How can you be a limit point but not a limit of a sequence?}
\end{admonition-remark}

\section{Examples}
\label{develop--math6620:ROOT:page--interior-closure.adoc---examples}

\subsection{Intervals and \(\mathbb{R}_l\)}
\label{develop--math6620:ROOT:page--interior-closure.adoc---intervals-and-latex-backslash-latex-backslashmathbb-latex-openbracer-latex-closebrace-latex-underscorel-latex-backslash}

Find the interior and closure of the following sets with respect to \(\mathbb{R}_l\).

\begin{enumerate}[label=\arabic*)]
\item \(A = (0,1)\)
\item \(A = [0,1)\)
\item \(A = (0,1]\)
\item \(A = [0,1]\)
\end{enumerate}

As done in class, we would find the interior and closure using only the initial definitions of interior and closure.

First, we find the interiors of each of the sets. Recall that \([a,b)\) is a basis for \(\mathbb{R}_l\)
and any open set (including the interior) may be written as the union of basis elements.
Therefore, any \(\interior(A) = \bigcup_{U \subseteq A}\bigcup_{[a,b) \subseteq U} [a,b)\) which may be collapsed into a single union.
Then

\begin{equation*}
\interior((0,1))
= \bigcup_{[a,b) \subseteq (0,1)} [ a,b)
= \bigcup_{0 < a < b \leq 1} [a,b)
= (0, 1)
\end{equation*}

\begin{equation*}
\interior([ 0,1))
= \bigcup_{[a,b) \subseteq [0,1)} [a,b)
= \bigcup_{0 \leq a < b \leq 1} [a,b)
= [0, 1)
\end{equation*}

\begin{equation*}
\interior(( 0,1])
= \bigcup_{[a,b) \subseteq (0,1]} [a,b)
= \bigcup_{0 < a < b \leq 1} [a,b)
= (0, 1)
\end{equation*}

\begin{equation*}
\interior([ 0,1])
= \bigcup_{[a,b) \subseteq [0,1]} [a,b)
= \bigcup_{0 \leq a < b \leq 1} [a,b)
= [0, 1)
\end{equation*}

Next, for closure, let us examine what the closed sets look like.
Consider arbitrary closed set \(F\) which corresponds with open set \(U\).
Then

\begin{equation*}
F = X - \bigcup [a,b)
= \bigcap \left(X - [a,b)\right)
= \bigcap \left((-\infty, a) \cup [b,\infty)\right)
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
\closure((0,1))
&= \bigcap_{(0,1) \subseteq (-\infty, a) \cup [b,\infty)} \left((-\infty, a) \cup [b,\infty)\right)
\\&= \bigcap_{(0,1) \subseteq (-\infty, a)} (-\infty, a) \ \cap  \bigcap_{(0,1) \subseteq [b,\infty)} [b, \infty)
\\&= \bigcap_{1 \leq a} (-\infty, a) \ \cap  \bigcap_{b < 0} [b, \infty)
\\&= (-\infty, 1) \cap [0, \infty)
\\&= [0,1)
\end{aligned}
\end{equation*}

\begin{equation*}
\closure([0,1))
= \bigcap_{1 \leq a} (-\infty, a) \ \cap  \bigcap_{b \leq 0} [b, \infty)
= (-\infty, 1) \cap [0, \infty)
= [0,1)
\end{equation*}

\begin{equation*}
\closure((0,1])
= \bigcap_{1 < a} (-\infty, a) \ \cap  \bigcap_{b \leq 0} [b, \infty)
= (-\infty, 1] \cap [0, \infty)
= [0,1]
\end{equation*}

\begin{equation*}
\closure([0,1])
= \bigcap_{1 < a} (-\infty, a) \ \cap  \bigcap_{b \leq 0} [b, \infty)
= (-\infty, 1] \cap [0, \infty)
= [0,1]
\end{equation*}

\begin{admonition-remark}[{}]
We can actually think of the \((-\infty, a)\) and \([b,\infty)\) as a sort of subbasis for the closed sets.
\end{admonition-remark}
\end{document}
