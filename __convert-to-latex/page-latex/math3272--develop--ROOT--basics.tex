\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Group Basics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
\def\normalsubgroup{\trianglelefteq}
\DeclareMathOperator{\ord}{ord}

% Title omitted
\begin{admonition-note}[{}]
Most of the information presented in this page are only stated but not proven.
For missing proofs see the \myautoref[{old site}]{develop--math2272:ROOT:page--index.adoc}.
\end{admonition-note}

Let \(G\) be a set and \(\star\) be a function which acts on \(G\).
Then \((G, \star)\) is a \emph{group} iff the following properties hold

\begin{description}
\item[Closure] \(\forall a,b \in G: a \star b \in G\)
\item[Associative] \(\forall a,b,c\in G: (a\star b)\star c = a\star(b\star c)\)
\item[Identity] \(\exists e \in G: \forall a \in G: a\star x = x\star a = a\)
\item[Inverse] \(\forall a \in G: \exists a^{-1} \in G: a\star a^{-1} = a^{-1}\star a = e\)
\end{description}

Also, if \(\star\) is commutative, we call \((G, \star)\) an \emph{abelian group}.
Additionally, we may sometimes refer to \(G\) as a group if the function
is implied (usually by stating it before).

Furthermore, we may omit writing
the group operation when it is implied or deemed unnecessary. That is we write
\(ab\) instead of \(a\star b\).

\section{Order}
\label{develop--math3272:ROOT:page--basics.adoc---order}

The term order, may be used in two contexts,

\begin{description}
\item[Group] The order of a finite group, denoted \(|G|\) or \(\ord(G)\) is defined
    as the number of elements in \(G\).
\item[Element] The order of an element \(a \in G\), denoted \(\ord(a)\) is the minimum
    positive integer \(n\) such that \(a^n = e\). If no such \(n\) exists,
    we say that \(a\) has infinite order.
\end{description}

Additionally, the order of am element divides the order of the group.

\section{Subgroups}
\label{develop--math3272:ROOT:page--basics.adoc---subgroups}

Let \((G, \star)\) be a group and \(S \subseteq G\). Then, \(S\)
is a \emph{subgroup} of \(G\) iff \((S, \star)\) also forms a group.
In such a case we use the notation \(S \leq G\).

We may easily check this through one of the following tests

\begin{description}
\item[Standard] Since associativity is from the operation, there is no need to test it.
    We require that \(S\neq \varnothing\) and closure and inverse hold.
\item[One Step] \(S\) is a subgroup iff \(S \neq \varnothing\) and \(\forall a,b \in S: a\star b^{-1} \in S\)
\item[Finite subgroup test] Finite \(S\) is a subgroup iff \(S\star S \subseteq S\).
\end{description}

\subsection{Product subgroups}
\label{develop--math3272:ROOT:page--basics.adoc---product-subgroups}

Let \(S\) and \(K\) be subgroups of \(G\) then the following is true

\begin{equation*}
SK \leq G \iff SK = KS
\end{equation*}

\begin{example}[{Proof}]
Suppose that \(SK \leq G\) and consider \(s \in S\) and \(k\in K\).
Then \(ks = (ek)(se) \in SK\)
since \(e\) is unique. Hence \(KS \subseteq SK\). On the other hand,
since \(SK\) is a group, there exists \((sk)^{-1} = s'k' \in SK\) and hence
\(sk = (s'k')^{-1} = (k')^{-1}(s')^{-1} \in KS\). Therefore, \(SK \subseteq KS\)
and we are done.

On the other hand, suppose that \(SK = KS\) and consider \(s_1k_1, s_2k_2 \in SK\).
Then,

\begin{equation*}
(s_1k_1)(s_2k_2)^{-1} = s_1k_1k_2^{-1}s_2^{-1} = s_1 s_3 k_3 \in SK
\end{equation*}

where \(s_3k_3 = (k_1k_2)^{-1}s_2^{-1}\). Therefore by the one step subgroup test
\(SK \leq G\).
\end{example}

\subsection{Generated subgroups}
\label{develop--math3272:ROOT:page--basics.adoc---generated-subgroups}

Let \(S \subseteq G\). Then, we define the \emph{subgroup generated by \(S\)}
as the smallest subgroup of \(G\) which contains \(S\) and we denote it
\(\abrack{S}\). By ``smallest'', we mean that if \(S \subseteq S' \leq G\)
then \(\abrack{S} \subseteq S'\).

If \(S\) is finite membered, we sometimes use the notation \(\abrack{s_1, \ldots s_n}\)
where \(S = \{s_1, \ldots s_n\}\).

Furthermore, if a subgroup \(K\) can be generated by a single element,
we call \(K\) \emph{cyclic}.

\subsection{Centralizer}
\label{develop--math3272:ROOT:page--basics.adoc---centralizer}

Let \(S \subseteq G\). Then we define the \emph{centralizer} of \(S\) to be
the set

\begin{equation*}
C(S) = \{g \in G: \forall s \in S: gs = sg\}
\end{equation*}

Then, \(C(S) \leq G\).

\section{Homomorphisms}
\label{develop--math3272:ROOT:page--basics.adoc---homomorphisms}

Let \((G, \star)\) and \((H,\circ)\) be groups and \(f: G\to H\). Then,
we call \(f\) a \emph{homomorphism} if

\begin{equation*}
\forall a,b \in G: f(a\star b) = f(a) \circ f(b)
\end{equation*}

That is, \(f\) preserves the group structure. Furthermore, it preserves

\begin{description}
\item[Identity] \(f(e_G) = e_H\)
\item[Inverse] \(f(a^{-1}) = f(a)^{-1}\)
\end{description}

Additionally, we may further classify \(f\) as follows

\begin{description}
\item[Endomorphism] if \(H = G\).
\item[Isomorphism] if \(f\) is bijective.
\item[Automorphism] if \(f\) is both an endomorphism and isomorphism.
\end{description}

\subsection{Isomorphisms and Isomorphism Classes}
\label{develop--math3272:ROOT:page--basics.adoc---isomorphisms-and-isomorphism-classes}

We say that \(G\) is \emph{isomorphic} to \(H\) if there exists
an isomorphism between \(G\) and \(H\) and we write \(G \cong H\).
Then, \(\cong\) has the following properties

\begin{description}
\item[Reflexive] \(G \cong G\)
\item[Symmetric] \(G \cong H \implies H \cong G\)
\item[Transitive] \(G \cong H \land H \cong K \implies G \cong K\)
\end{description}

Although these seem like all that is necessary for an equivalence relation, it actually
falls short since the class of groups is not a set.

\begin{admonition-remark}[{}]
The reason why it is not a set is technical. I am not totally sure,
hopefully it is answered in \myautoref[{set theory}]{develop--math3274:ROOT:page--index.adoc}.
\end{admonition-remark}

\subsection{Images and Pre-images}
\label{develop--math3272:ROOT:page--basics.adoc---images-and-pre-images}

Let \(R\leq G\) and \(S\leq H\) then, \(f(R) \leq H\) and
\(f^{-1}(S) \leq G\). That is, the image of a subgroup is also a subgroup
and the preimage of a subgroup is also a subgroup.

\begin{example}[{Proof of pre-image}]
\begin{admonition-remark}[{}]
This is only here since I didn't find the proof in the old notes.
\end{admonition-remark}

Firstly, since \(S\leq H\), \(f(e_G) = e_H \in S\).
Next, let \(a,b \in f^{-1}(S)\), then since \(f(b) \in S\),
\(f(b)^{-1} \in S\) and hence

\begin{equation*}
f(ab^{-1}) = f(a)f(b)^{-1} \in S \implies ab^{-1} \in f^{-1}(S)
\end{equation*}
\end{example}

In particular, we call \(\ker(f) = f^{-1}(\{e_H\})\) the \emph{kernel of \(f\)}
and it is a normal subgroup of \(G\).

\section{Cosets}
\label{develop--math3272:ROOT:page--basics.adoc---cosets}

Let \(G\) be a group with subgroup \(H\). Then, if \(g \in G\), we
define the following

\begin{description}
\item[Left Coset] We define \(gH\) to be a \emph{left coset} of \(H\)
\item[Right Coset] We define \(Hg\) to be a \emph{right coset} of \(H\)
\item[Coset] If \(G\) is abelian, then \(gH=Hg\) is simply called a \emph{coset} of \(H\).
\end{description}

Additionally, if \(g_1, g_2\in G\), the following holds

\begin{equation*}
g_1H = g_2H
\iff Hg_1^{-1} = Hg_2^{-1}
\iff g_1H \subseteq g_2H
\iff g_1 \in g_2H
\iff g_2^{-1} g_1 \in H
\end{equation*}

and the analogous set of statements hold for right cosets.

Additionally, we may define a equivalence relation on \(G\), where
\(a\sim b\) iff \(aH = bH\) (we may also do something similar for right cosets).
We commonly refer to this relation as \emph{congruence modulo \(H\)}. Hence,
the set of cosets partition the group.

\subsection{Index and Lagrange's Theorem}
\label{develop--math3272:ROOT:page--basics.adoc---index-and-lagranges-theorem}

For any subgroup \(S\), there are an equal number of right and left cosets.
More specifically, there exists a bijection between left and right cosets. We
call this number the \emph{index of \(S\)} and denote it as \([G:H]\).
Then \emph{lagrange's theorem} asserts that

\begin{equation*}
[G: H] = \frac{|G|}{|H|} \in \mathbb{N}
\end{equation*}

In particular, the order of a subgroup always divides the order of the group.

\subsection{Normal subgroups}
\label{develop--math3272:ROOT:page--basics.adoc---normal-subgroups}

We call \(H\) \emph{normal} if all of its right and cosets are equal
and we denote it as \(H \normalsubgroup G\).
That is, for all \(g \in G\), \(gH = Hg\).
Then, \(H\) is normal iff

\begin{equation*}
\forall g \in G: \forall h \in H : g^{-1} h g \in H
\end{equation*}

\subsection{Quotient Groups}
\label{develop--math3272:ROOT:page--basics.adoc---quotient-groups}

If \(N \normalsubgroup G\), then we can define \(G/ N\)
as the set of cosets of \(N\). Then, we can define a group operation
on \(G/N\) as follows

\begin{equation*}
Na \cdot Nb = N(ab)
\end{equation*}

This operation is well defined and we call this group the \emph{quotient group}
of \(G\) by \(N\).

Notice that this operation is the same as our standard product operation.

\begin{equation*}
Na \cdot Nb
= \{n_1 a n_2 b: n_1, n_2 \in N\}
= \{n_1 n_2'a b: n_1, n_2' \in N\}
= \{n a b: n \in N\}
= N(ab)
\end{equation*}

since we can find \(n_2' \in N\) such that \(an_2 = n_2'a\) since \(an_2 \in aN = Na\).
\end{document}
