\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Fisher Information}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Suppose a population exhibits some parameter \(\theta\). Then, for each sample
we take, there possibly exists some information about \(\theta\) which
allows us to estimate it. Additionally, with larger samples should contain
more information about \(\theta\) allowing for better estimates.
\emph{Fisher information} is one measure of the amount of information a sample contains
about a parameter.

Firstly, lets define the fisher information for a single observation. Suppose that
\(X\) comes from a distribution with pdf \(f\) which depends on \(\theta\).
Then, we define \(\lambda(x, \theta) = \log(f(x, \theta))\) and

\begin{equation*}
I(\theta) = E(\lambda'(X, \theta)^2)
\end{equation*}

where \(\lambda^{(n)} = \frac{\partial^n \lambda}{\partial \theta^n}\).

Furthermore if \(\vec{X} = (X_1, \ldots X_n)\) is an iid random sample,
we define

\begin{equation*}
I_n(\theta) = E(\lambda_n'(\vec{X}, \theta)^2) = \sum_{i=1}^n E(\lambda'(X_i, \theta)^2) = nI(\theta)
\end{equation*}

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
I_n(\theta)
&= E(\lambda_n'(\vec{X}, \theta)^2)
\\&= E\left(\left( \frac{\partial }{\partial \theta}\ln(f(\vec{X}, \theta)) \right)^2\right)
\\&= E\left(\left( \frac{\partial }{\partial \theta}\ln\left(\prod_{i=1}^n f(X_i, \theta)\right) \right)^2\right)
\\&= E\left(\left( \frac{\partial }{\partial \theta}\sum_{i=1}^n\ln\left( f(X_i, \theta)\right) \right)^2\right)
\\&= E\left(\left( \sum_{i=1}^n \frac{\partial }{\partial \theta}\ln\left( f(X_i, \theta)\right) \right)^2\right)
\\&= E\left(\left( \sum_{i=1}^n \lambda'(X_i, \theta) \right)^2\right)
\\&= E\left( \sum_{i\neq j} \lambda'(X_i, \theta) \lambda'(X_j, \theta) + \sum_{i=1}^n \lambda'(X_i, \theta)^2\right)
\\&= \sum_{i\neq j} E(\lambda'(X_i, \theta)) E(\lambda'(X_j, \theta)) + \sum_{i=1}^n E(\lambda'(X_i, \theta)^2)
\\&= \sum_{i=1}^n E(\lambda'(X_i, \theta)^2)
\\&= nI(\theta)
\end{aligned}
\end{equation*}

We show that \(E(\lambda'(X, \theta)) = 0\) \myautoref[{here}]{develop--math3465:statistics:page--fisher-information.adoc---expectation-of-first-derivative-log-likelihood}.
\end{example}

\section{Equivalent formulations for fisher information}
\label{develop--math3465:statistics:page--fisher-information.adoc---equivalent-formulations-for-fisher-information}

If our pdf of our variable is ``nice'', we can show that
the following are equivalent formulations for fisher information
of a random variable

\begin{equation*}
I(\theta)  = Var(\lambda'(X, \theta)) = -E(\lambda'' (X, \theta))
\end{equation*}

\begin{example}[{Proof}]
Notice that

\begin{equation*}
I(\theta) = E(\lambda'(X, \theta)^2) - E(\lambda'(X, \theta))^2 = Var(\lambda'(X, \theta))
\end{equation*}

because of \myautoref[{this result}]{develop--math3465:statistics:page--fisher-information.adoc---expectation-of-first-derivative-log-likelihood}.
Then, for the second equality,

\begin{equation*}
\begin{aligned}
- E(\lambda'' (X, \theta))
&= E\left(\frac{f(X, \theta)f''(X, \theta) - f'(X, \theta)^2}{f(X, \theta)^2}\right)
\\&= E\left(\lambda'(X, \theta)^2 - \frac{f''(X, \theta)}{f(X, \theta)}\right)
\\&= I(\theta) - E\left(\frac{f''(X, \theta)}{f(X, \theta)}\right)
\\&= I(\theta) - \int_{\mathbb{R}} \frac{f''(x, \theta)}{f(x, \theta)} f(x, \theta) \ dx
\\&= I(\theta) - \int_{\mathbb{R}} f''(x, \theta) \ dx
\\&= I(\theta) - \int_{\mathbb{R}} \frac{\partial^2}{\partial \theta^2}f(x, \theta) \ dx
\\&= I(\theta) - \frac{\partial^2}{\partial \theta^2}\int_{\mathbb{R}} f(x, \theta) \ dx
\\&= I(\theta) - \frac{\partial^2}{\partial \theta^2} 1
\\&= I(\theta)
\end{aligned}
\end{equation*}
\end{example}

Furthermore, for a random sample,

\begin{equation*}
I_n(\theta) = \sum_{i=1}^n Var(\lambda'(X_i, \theta)) = -\sum_{i=1}^n E(\lambda'' (X_i, \theta))
\end{equation*}

\subsection{Expected value of differential of log likelihood}
\label{develop--math3465:statistics:page--fisher-information.adoc---expectation-of-first-derivative-log-likelihood}

\begin{equation*}
E(\lambda'(X, \theta)) = 0
\end{equation*}

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
E(\lambda'(X, \theta))
&= E\left(\frac{f'(X, \theta)}{f(X, \theta)}\right)
\\&= \int_{\mathbb{R}} \frac{f'(x, \theta)}{f(x, \theta)} f(x, \theta)\ dx
\\&= \int_{\mathbb{R}} f'(x, \theta)\ dx
\\&= \int_{\mathbb{R}}\frac{\partial }{\partial \theta} f(x, \theta)\ dx
\\&= \frac{\partial }{\partial \theta} \int_{\mathbb{R}} f(x, \theta)\ dx
\\&= \frac{\partial }{\partial \theta} 1
\\&= 0
\end{aligned}
\end{equation*}
\end{example}

\section{Information Inequality}
\label{develop--math3465:statistics:page--fisher-information.adoc---information-inequality}

Let \(\vec{X} = (X_1, \ldots X_n)\) comprise a random sample
of a distribution with pdf \(f(X_i, \theta)\) and let \(T\)
be an unbiased estimator for \(m(\theta)\). Then,

\begin{equation*}
Var(T) \geq \frac{m'(\theta)^2}{nI(\theta)}
\end{equation*}

\begin{example}[{Proof}]
This result follows the
\myautoref[{cauchy-schwartz inequaility}]{develop--math3273:inner-product-space:page--index.adoc---cauchy-schwartz-inequality}
since the set of random variables together with the covariance function forms an inner product
space. [\myautoref[{see here}]{develop--math2274:ROOT:page--joint-random-variables.adoc---covariance-as-an-inner-product}].
Then, if we consider random variables \(T\) and \(\lambda'(\vec{X}, \theta)\), by the
cauchy-schwartz inequality, it follows that

\begin{equation*}
\begin{aligned}
&Var(T)Var(\lambda'(\vec{X}, \theta)) \geq Cov(T, \lambda'(\vec{X}, \theta))^2
\\& \implies Var(T)
    \geq \frac{Cov(T, \lambda'(\vec{X}, \theta))^2}{Var(\lambda'(\vec{X}, \theta))}
= \frac{Cov(T, \lambda'(\vec{X}, \theta))^2}{nI(\theta)}
\end{aligned}
\end{equation*}

Therefore, it would suffice for us to show that

\begin{equation*}
Cov(T, \lambda'(\vec{X}, \theta))
= E(T\lambda'(\vec{X}, \theta)) - E(T)E(\lambda'(\vec{X},\theta))
= e(t\lambda'(\vec{x}, \theta))
= m'(\theta)
\end{equation*}

since \(E(\lambda'(\vec{X}, \theta)) = 0\). [\myautoref[{see here}]{develop--math3465:statistics:page--fisher-information.adoc---expectation-of-first-derivative-log-likelihood}]

Now, since \(T\) is a statistic from \(\vec{X}\), we can write it in its function form
\(T(\vec{X})\) to make it clear that the joint distribution can be formulated just in terms of \(x\).
Then,

\begin{equation*}
\begin{aligned}
E(T\lambda'(\vec{X}, \theta))
&= \int_{\mathbb{R}^n}T(\vec{x})\frac{f'(\vec{x}, \theta)}{f(\vec{x}, \theta)}f(\vec{x}, \theta) \ d\vec{x}
\\&= \int_{\mathbb{R}^n}T(\vec{x})f'(\vec{x}, \theta) \ d\vec{x}
\\&= \frac{\partial }{\partial \theta}\int_{\mathbb{R}^n}T(\vec{x})f(\vec{x}, \theta) \ d\vec{x}
\\&= \frac{\partial }{\partial \theta}E(T(\vec{X}))
\\&= \frac{\partial }{\partial \theta} m(\theta)
\\&= m'(\theta)
\end{aligned}
\end{equation*}

and we are done.
\end{example}

In particular, when \(m(\theta) = \theta\), we obtain

\begin{equation*}
Var(T) \geq \frac{1}{nI(\theta)}
\end{equation*}

which is the \emph{Cramer-Rao Lower bound}. Therefore, we aim to find an estimator
which attains this lower bound, in which case, they would be one of the best
unbiased estimators for \(\theta\). If we attain such an estimator,
we call it a \emph{minimum variance, unbiased estimator (MVUE) for \(\theta\)}

\begin{admonition-caution}[{}]
This lower bound is only for unbiased estimators of \(\theta\). That is, there may exists
biased estimators with smaller variance. For example, \(T=5\) has variance \(0\).
\end{admonition-caution}

\section{Rao-Blackwell Theorem}
\label{develop--math3465:statistics:page--fisher-information.adoc---rao-blackwell-theorem}

Suppose \(\vec{X} = (X_1, \ldots X_n)\) comprise a sample of a distribution with parameter
\(\theta\). Then, if \(T\) is an unbiased estimator for \(m(\theta)\) and \(U\)
is sufficient for \(\theta\), we can define a random variable \(W = E(T| U)\)
such that

\begin{itemize}
\item \(W\) is sufficient for \(m(\theta)\)
\item \(W\) is an unbiased estimator for \(m(\theta)\)
\item \(Var(W) \leq Var(T)\)
\end{itemize}

Furthermore, \(W = T\) if \(T\) is sufficient for \(m(\theta)\) (hence, this construction
is idempotent). Therefore, there
is no non-practical reason to not use a sufficient estimator for \(\theta\).

\begin{admonition-warning}[{}]
The proof of the first result is missing. Until then, please do not use it.
\end{admonition-warning}

\begin{example}[{Proof}]
By the law of
\myautoref[{iterated expectation}]{develop--math3278:ROOT:page--conditional-expectation.adoc---law-of-iterated-expectation}.

\begin{equation*}
E(W) = E_U(E(T|U)) = E(T) = m(\theta)
\end{equation*}

and by using the law of
\myautoref[{iterated variance}]{develop--math3278:ROOT:page--conditional-expectation.adoc---law-of-iterated-variance}.

\begin{equation*}
\begin{aligned}
&Var(T) = Var_U(E(T|U)) + E_U(Var(T|U)) = Var(W) +  E_U(Var(T|U))
||& \implies Var(W) = Var(T) - E_U(Var(T|U)) \leq Var(T)
\end{aligned}
\end{equation*}

Now, notice that \(Var(T|U) \geq 0\) and hence its expected value must also
be non-negative. Furthermore, notice that \(Var(T|U) = 0\) iff \(U\)
completely determines \(T\). One instance of this is when \(U\)
determines \(\theta\) which in turn determines \(m(\theta)\) which determines
\(T\). That is, we get equality when \(T\) is determined by \(m(\theta)\).
\end{example}

\begin{admonition-remark}[{}]
Non-practical. Very-important that we note that computing \(W\)
is itself a challenge.
\end{admonition-remark}

\section{Lehmann-scheffe Theorem}
\label{develop--math3465:statistics:page--fisher-information.adoc---lehmann-scheffe-theorem}

If \(U\) is complete sufficient statistic for \(\theta\)
and \(W=g(U)\) is an unbiased estimator for \(m(\theta)\),
then W is the unique minimal variance unbiased estimator for \(m(\theta)\)

\subsection{Corollary: Rao-Blackwellization yeilds unique MVUE}
\label{develop--math3465:statistics:page--fisher-information.adoc---corollary-rao-blackwellization-yeilds-unique-mvue}

If \(U\) is a complete sufficient statistic for \(\theta\)
and \(T\) is an unbiased estimator for \(m(\theta)\)
then \(W = E(T|U)\) is the unique MVUE for \(m(\theta)\).
\end{document}
