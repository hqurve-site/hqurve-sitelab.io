\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Calculus}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Ln}{Ln}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\Res}{Res}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(D\) be a domain. Then complex variable \(w\) is said to be a function of complex variables
of \(z\) if for all \(z \in D\), there correspond one or more definite values of \(w\). In
such a case, we write

\begin{equation*}
w = f(z)
\end{equation*}

Furthermore, if we write \(z = x+iy\) where \(x,y \in \mathbb{R}\), we can write

\begin{equation*}
w = u(x,y) + iv(x,y)
\end{equation*}

where \(u\) and \(v\) are real valued functions.

\begin{description}
\item[Uniform functions] \(w\) is \emph{uniform} or \emph{singled valued} if for each \(z \in D\),
    there exists one corresponding value for \(w\).
\item[Multi-valued function] \(w\) is \emph{many valued} or \emph{multi-valued} if there are some
    \(z \in D\) each of which correspond to two or more values of \(w\).
\end{description}

\begin{admonition-remark}[{}]
\begin{verbatim}
{\textgreater} function\newline
{\textgreater} multi-valued
\end{verbatim}

Pick one
\end{admonition-remark}

\section{Limits}
\label{develop--math3275:basics:page--limits.adoc---limits}

Let \(f\) be a function over domain \(D\) be a domain and consider limit point \(z_0\) of \(D\). Then
if \(f\) is singled valued in a deleted neighbourhood of \(z_0\) we define the following

\begin{custom-quotation}[{}][{}]
\(L \in \mathbb{C}\) is the limit of \(f\) at \(z_0\) if for all \(\varepsilon > 0\),
there exists \(\delta > 0\) such that \(0 < |z - z_0| < \delta\) implies that \(|f(z) - L| < \varepsilon\)
and we write

\begin{equation*}
\lim_{z\to z_0} f(z) = L
\end{equation*}
\end{custom-quotation}

\subsection{Properties}
\label{develop--math3275:basics:page--limits.adoc---properties}

Let \(f\) and \(g\) be functions over domain \(D\) and suppose that

\begin{equation*}
\lim_{z\to z_0} f(z) = A \quad\text{and}\quad \lim_{z\to z_0} g(z) = B
\end{equation*}

Then

\begin{itemize}
\item \(\displaystyle \lim_{z\to z_0} (f(z) + g(z)) = \left(\lim_{z\to z_0} f(z)\right) + \left(\lim_{z\to z_0} g(z)\right) = A + B\)
\item \(\displaystyle \lim_{z\to z_0} f(z)g(z) = \left(\lim_{z\to z_0} f(z)\right)\left(\lim_{z\to z_0} g(z)\right) = AB\)
\item \(\displaystyle \lim_{z\to z_0} \frac{f(z)}{g(z)} = \frac{\lim_{z\to z_0} f(z)}{\lim_{z\to z_0} g(z)} = \frac{A}{B}\) if \(B \neq 0\)
\item \(\displaystyle \lim_{z\to z_0} \overline{f(z)} = \overline{\lim_{z\to z_0} f(z)} = \overline{A}\). This follows immediately from the
    fact that \(|\overline{w}| = |w|\). Additionally, it follows that
    
    \begin{itemize}
    \item \(\displaystyle \lim_{z\to z_0} \Re(f(z)) = \Re\left(\lim_{z\to z_0} f(z)\right) = \Re(A)\) since \(\Re(w) = \frac{w + \overline{w}}{2}\)
    \item \(\displaystyle \lim_{z\to z_0} \Im(f(z)) = \Im\left(\lim_{z\to z_0} f(z)\right) = \Im(A)\) since \(\Im(w) = \frac{w - \overline{w}}{2i}\)
    \end{itemize}
    
    Therefore, the limit of \(f\) at \(z_0\) exists iff \(\Im(f)\) and \(\Re(f)\) both have limits at
    \(z_0\).
\end{itemize}

\begin{admonition-caution}[{}]
Although these rules look like they are ``separating'' limits, they are actually combining them. For example,
we are not separating the limit of \(f + g\) but rather combining the limit of \(f\) and the limit of \(g\).
This is most apparent when the limit of one of \(f\) and \(g\) does not exist but the limit of \(f+g\) does exist.
\end{admonition-caution}

\subsection{Directed Limits}
\label{develop--math3275:basics:page--limits.adoc---directed-limits}

Let \(f\) be a function over domain \(D\) and suppose that

\begin{equation*}
\lim_{z\to z_0} f(z) = A
\end{equation*}

Then if \(E\) is a subdomain of \(D\) which also has limit point \(z_0\),
the limit of the new function \(g\) over this subdomain at \(z_0\) is also \(A\).
That is

\begin{equation*}
\lim_{z\to z_0} g(z) = A
\end{equation*}

Utilizing this, we could form the concept of \emph{directed limits}. That is, if we have
an arc \(w\) parameterized by \(t \in \mathbb{R}\) which contains limit point \(z_0\)
as \(t\to t_0\), the limit of \(f(w(t))\) as \(t\to t_0\) is also \(A\).

Therefore, if we can find two paths which produce different limits, we know that the limit
of \(f\) at \(z_0\) does not exist.

\section{Continuity}
\label{develop--math3275:basics:page--limits.adoc---continuity}

Let \(f\) be a function over closed bounded domain \(D\) and consider \(z_0 \in D\). Then

\begin{custom-quotation}[{}][{}]
\(f\) is \emph{continuous} at \(z= z_0\) if for all \(\varepsilon > 0\), there exists
\(\delta > 0\) such that \(|z-z_0| < \delta\) implies \(|f(z) - f(z_0)| < \varepsilon\)
\end{custom-quotation}

Then it immediately follows that \(f\) is continuous at \(z_0\)
iff \(\lim_{z\to z_0} f(z) = f(z_0)\).

Additionally, since the limit of real and imaginary parts exists iff the limit exists, we get that
\(f\) is continuous at \(z_0\) iff its real and imaginary components are both continuous at \(z_0\).

\begin{description}
\item[Continuity on a domain] \(f\) is continuous in \(D\) iff it is continuous
    at every point in \(D\).
\end{description}

\subsection{Uniform continuity}
\label{develop--math3275:basics:page--limits.adoc---uniform-continuity}

\begin{custom-quotation}[{}][{}]
\(f\) is \emph{uniformly continuous} on \(D\) if for all \(\varepsilon > 0\), there exists
\(\delta > 0\) such that \(|z_1-z_2| < \delta\) implies \(|f(z_1) - f(z_2)| < \varepsilon\)
for all pairs \((z_1, z_2) \in D^2\).
\end{custom-quotation}

Note that continuity is a necessary but insufficient condition for uniform continuity. However,
if \(D\) is compact, \(f\) is continuous on domain \(D\) iff it is uniformly continuous.

\section{Differentiation}
\label{develop--math3275:basics:page--limits.adoc---differentiation}

Let \(f\) be a function over domain \(D\) and consider \(z_0 \in D\). Then

\begin{custom-quotation}[{}][{}]
\(f\) is differentiable at \(z_0\) if the limit

\begin{equation*}
\lim_{\Delta z \to 0} \frac{f(z_0 + \Delta z) - f(z_0)}{\Delta z}
\end{equation*}

exists. In such a case, the value of this limit is denoted \(f'(z_0)\)
or \(\left.\frac{df}{dz}\right|_{z = z_0}\).
\end{custom-quotation}

Then, continuity is necessary but insufficient for differentiability.

\begin{example}[{Proof of necessity}]
\begin{admonition-important}[{}]
Proof taken from lecture notes
\end{admonition-important}

Let \(f\) be differentiable at \(z_0\) then \(f'(z_0) = \lim_{z\to z_0}\frac{f(z) - f(z_0)}{z - z_0}\) exists.
Then,

\begin{equation*}
\begin{aligned}
\lim_{z\to z_0} (f(z) - f(z_0))
&= \lim_{z\to z_0} \left(\frac{f(z) - f(z_0)}{z-z_0} (z-z_0)\right)
\\&= \left(\lim_{z\to z_0} \frac{f(z) - f(z_0)}{z-z_0}\right)\lim_{z\to z_0}  (z-z_0)
\\&= f'(z_0) (z_0 - z_0)
\\&= 0
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
\lim_{z \to z_0} f(z)
= \lim_{z \to z_0} (f(z) - f(z_0)) + \lim_{z\to z_0} f(z_0)
= 0 + f(z_0)
= f(z_0)
\end{equation*}
\end{example}

\begin{example}[{Example of insufficiency}]
\begin{admonition-important}[{}]
This example is taken from the lecture notes
\end{admonition-important}

\begin{admonition-remark}[{}]
This is example truly baffling and not what I would have guessed
\end{admonition-remark}

Consider \(f(z) = \overline{z}\). Then clearly \(f\) is (uniformly) continuous on \(\mathbb{C}\)
since the identity function is continuous. However, notice that for any \(z_0 \in \mathbb{Z}\)

\begin{equation*}
\lim_{\Delta z\to 0} \frac{\overline{\Delta z + z_0} - \overline{z_0}}{\Delta z}
= \lim_{\Delta z\to 0} \frac{\overline{\Delta z}}{\Delta z}
\end{equation*}

We would now write \(\Delta z = x + yi\) and notice that \(\Delta z \to 0\) iff
\((x,y) \to (0,0)\). Then

\begin{equation*}
\begin{aligned}
\lim_{\Delta z\to 0} \frac{\overline{\Delta z + z_0} - \overline{z_0}}{\Delta z}
&= \lim_{(x,y) \to (0,0)} \frac{x - yi}{x+yi}
\\&= \lim_{(x,y) \to (0,0)} \frac{x^2 - y^2 - 2xyi}{x^2 + y^2}
\\&= \lim_{(x,y) \to (0,0)} \left[\frac{x^2 - y^2}{x^2 +y^2} - 2i \frac{xy}{x^2 + y^2}\right]
\end{aligned}
\end{equation*}

And notice that in fact neither the real nor imaginary components of the above limit exists. Consider
the path along the line \((x,y) = (at,bt)\). We get

\begin{equation*}
\lim_{t \to 0} \left[\frac{a^2t^2 - b^2t^2}{a^2t^2 + b^2t^2} - 2i \frac{abt^2}{a^2t^2 + b^2t^2}\right]
= \lim_{t \to 0} \left[\frac{a^2 - b^2}{a^2 + b^2} - 2i \frac{ab}{a^2 + b^2}\right]
= \frac{a^2 - b^2}{a^2 + b^2} - 2i \frac{ab}{a^2 + b^2}
\end{equation*}

and notice that both the real and imaginary components depend on the values of \((a,b)\).
\end{example}
\end{document}
