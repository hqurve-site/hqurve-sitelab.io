\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Writing Guide}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

{} \def\paren#1{\left({#1}\right)} \def\brack#1{\left[{#1}\right]} \def\cbrack#1{\left\{{#1}\right\}} \def\abs#1{\left|{#1}\right|} \DeclareMathOperator{\sgn}{sgn}
% Title omitted
\begin{admonition-note}[{}]
The writing guide is stored in the playbook repository and injected at runtime.
\end{admonition-note}

\section{antora.yaml file}
\label{develop--main:ROOT:page--writing-guide.adoc---antora-yaml-file}

\begin{literal}[{}])
name: math6620
title: Topology
version: develop
nav:
- modules/ROOT/nav.adoc
asciidoc:
  attributes:
    sectnums: ''
    stem: latexmath
    page-latex-prelude-prefix: |
      \DeclareMathOperator{\sign}\{sign}
      \DeclareMathOperator{\interior}\{Int}
      \DeclareMathOperator{\closure}\{Cl}
      \DeclareMathOperator{\limit}\{Lim}

book:
  title: Book title
  single-chapter: false
  layout:
  - { src: ROOT:index.adoc, title-level: omit, content-level: 2 }
  - { src: ROOT:topological-spaces.adoc, title-level: 1, content-level: 2 }
  - { src: ROOT:basis.adoc, level: 2 }
  - { src: ROOT:interior-closure.adoc, level: 2 }
  - { src: ROOT:maps.adoc, level: 2 }
  - { src: ROOT:special-topologies.adoc, level: 2 }
  - { title: "Appendix", title-level: 1, type: appendix }
  - { src: appendix:counting-finite.adoc, level: 2 }
\end{literal}

In asciidoc,

\begin{itemize}
\item \texttt{sectnums: ''} entry indicates that we want our sections to have numbers \url{https://docs.asciidoctor.org/asciidoc/latest/sections/numbers/\#numlevels}.
\item \texttt{stem: latexmath} entry indicates that we want to process stem blocks as latex.
\item \texttt{page-latex-prelude-prefix} is a custom attribute. These lines of latex code are placed before any latex.
    It bloats the source code but makes it easier to write.
\end{itemize}

In \texttt{book} (a custom key)

\begin{itemize}
\item \texttt{title} is the book title (in raw latex)
\item \texttt{single-chapter} specifies whether the content is a single chapter (default false).
    If it is a single chapter, the article class is used instead.
    This is used if the component is not very big.
    This option does not change the levels (2 is still Section).
\item \texttt{layout} is a list of dictionaries containing
    
    \begin{itemize}
    \item \texttt{src}: the location from which to load the content. NOTE: this is not an xref. The only format is \texttt{module:file.adoc}.
    \item \texttt{title} and \texttt{title-level}: the text to place as the title at a given level. This overwrites the title in \texttt{src} if present.
        This text must be valid latex and is placed verbatim in the resulting output (modulo any yaml escaping).
        If \texttt{title-level} is \texttt{omit}, the title is omitted.
    \item \texttt{content-level}: the level at which to place content. Not required if there is no \texttt{src} attribute.
    \item \texttt{level}: the default value of \texttt{title-level} and \texttt{content-level}. Defaults to 1
    \end{itemize}
\end{itemize}

Levels within the document are the number of \texttt{=} (excluding the first).
The corresponding \texttt{title\_level} or \texttt{content\_level} is added to the respective levels.
In the output, the highest level is 1 corresponds to chapters.

\begin{listing}[{}]
// title has an internal level of 0
= My title

// internal level of 1
== my section 1

// internal level of 2
=== my section 2
\end{listing}

\section{Asciidoc common things}
\label{develop--main:ROOT:page--writing-guide.adoc---asciidoc-common-things}

Please see \url{https://docs.asciidoctor.org/asciidoc/latest}.

``Double quotes''

`Single quotes'

\emph{Emphasized}.

Newlinebreak\newline
Following line

\subsection{Lists}
\label{develop--main:ROOT:page--writing-guide.adoc---lists}

\begin{description}
\item[Description List] Hi, this is a description List
    
    \begin{itemize}
    \item Level 2 item
        
        \begin{itemize}
        \item Level 3 item
        \end{itemize}
    \item Level 2 again
    \end{itemize}
\item[Second item in list] 
\item[Third item] Dont forget the \texttt{\{empty\}} tag if you have nothing in the list (see above)
    
    \begin{enumerate}[label=\alph*)]
    \item Numbering level 2
        
        \begin{enumerate}[label=\roman*)]
        \item Numbering level 3
            
            \begin{enumerate}[label=\arabic*)]
            \item Numbering level 1
            \end{enumerate}
        \end{enumerate}
    \end{enumerate}
\end{description}

\subsection{Codeblocks / monospace}
\label{develop--main:ROOT:page--writing-guide.adoc---codeblocks-monospace}

These are done with listing or literal blocks enclosed with \texttt{----} or \texttt{....} respectively. Listing blocks are preferred
and the language can be specified using a leading \texttt{[source,yaml]} for example.

Inline source code may be created using enclosed \texttt{`} as in markdown.

\begin{example}[{}]
\texttt{mycoolfunction(arg1, arg2)}.

\begin{listing}[{}]
Listing block example & // ensure ampersand is parsed properly
\end{listing}

\begin{literal}[{}])
Literal block example
\end{literal}

\begin{listing}[{}]
Markdown-like code block example
\end{listing}
\end{example}

\subsection{Strike through text}
\label{develop--main:ROOT:page--writing-guide.adoc---strike-through-text}

Use

\begin{listing}[{}]
[.line-through]#your text here#
\end{listing}

\begin{example}[{}]
\strikethrough{Strike through text}
\end{example}

\subsection{Urls}
\label{develop--main:ROOT:page--writing-guide.adoc---urls}

\href{https://en.wikipedia.org/wiki/Localization\_(commutative\_algebra)}{[wiki article]}

\href{https://en.wikipedia.org/wiki/Localization\_(commutative\_algebra)}{[cool text \(x^2\)]}

\subsection{Emoji}
\label{develop--main:ROOT:page--writing-guide.adoc---emoji}

Emoji wave \emoji{👋}

\subsection{Latex}
\label{develop--main:ROOT:page--writing-guide.adoc---latex}

To use latex equations, simply use \texttt{stem:[Your equation here]} or

\begin{listing}[{}]
[stem]
++++
% Your equation here
++++
\end{listing}

Be aware that inline equations must escape \texttt{]} using a backslash.

\begin{example}[{}]
\(x^2\), \(\mathcal{C}[0,1]\), \(\begin{bmatrix}1 & 0 \\ 0 & 1\end{bmatrix}\).

\begin{equation*}
\exp(x) = \sum_{n=0}^\infty \frac{x^n}{n!}
\end{equation*}

\begin{equation*}
\begin{aligned}
\forall x \geq 0: x &= (x+1) - 1
\\&= |x+1|-1
\end{aligned}
\end{equation*}
\end{example}

\subsubsection{Defining macros}
\label{develop--main:ROOT:page--writing-guide.adoc---defining-macros}

There are also asciidoc attributes \texttt{page-latex-prelude} and \texttt{page-latex-prelude-prefix}.
These should contain definitions used in equations. For example, you may want to define a new macro \texttt{{\textbackslash}mycoolmacro}.
It is important that you realize that these are asciidoc attributes.

\begin{itemize}
\item You must escape
    strings \texttt{\{[a-zA-Z0-9:-\_]\}} with a leading backslash otherwise, it would attempt to look for a variable to perform the substitution.
    This is required since we sometimes use mathjax.
\item Although they are attributes, please do not rely on other asciidoc attributes since these are not handled during parsing.
\end{itemize}

The code in these definitions should not have issues with being called multiple times since I just dump them (using an extension)
at the start of each stem block. Depending on the renderer (mathjax vs katex), these blocks may be parsed in a chain or independently.
So, use \texttt{{\textbackslash}def} instead of \texttt{{\textbackslash}newcommand}.

\begin{admonition-important}[{}]
These attributes are only checked for at the document. All others are skipped
\end{admonition-important}

\begin{example}[{}]
\(\sgn (-12312) = -1\)

\begin{equation*}
\brack{x^2}\paren{x^2}
\end{equation*}
\end{example}

\subsubsection{Numbering}
\label{develop--main:ROOT:page--writing-guide.adoc---numbering}

Since we are targeting 3 platforms (mathjax, katex and latex) and katex does not handle equation numbering,
we need to implement numbering manually.
For this reason, when labelling equations, use \texttt{{\textbackslash}numberandlabel\{label\}}. You can use eqref as usual

\begin{example}[{Example}]
\begin{listing}[{}]
[stem]
++++
\numberandlabel{binomial2}
(x+1)^2 = x^2+2x+1
++++

Reference to stem:[\eqref{binomial2}]
\end{listing}

\begin{equation*}
\numberandlabel{binomial2}
(x+1)^2 = x^2+2x+1
\end{equation*}

Reference to \(\eqref{binomial2}\)
\end{example}

\subsection{Admonitions}
\label{develop--main:ROOT:page--writing-guide.adoc---admonitions}

Either start a line with \texttt{TYPE:} or use

\begin{listing}[{}]
[TYPE]
====
====
\end{listing}

Types include

\begin{description}
\item[Default] Caution, important, note, tip, warning
\item[Custom] idea, remark, thought
\end{description}

\begin{admonition-important}[{}]
Please ensure that there is a newline before and after the admonition
\end{admonition-important}

\begin{example}[{}]
\begin{admonition-caution}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-caution}

\begin{admonition-important}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-important}

\begin{admonition-note}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-note}

\begin{admonition-tip}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-tip}

\begin{admonition-warning}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-warning}

\begin{admonition-caution}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-caution}

\begin{admonition-important}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-important}

\begin{admonition-note}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-note}

\begin{admonition-tip}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-tip}

\begin{admonition-warning}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-warning}

Custom ones:

\begin{admonition-idea}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-idea}

\begin{admonition-remark}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-remark}

\begin{admonition-thought}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-thought}

\begin{admonition-todo}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-todo}

\begin{admonition-idea}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-idea}

\begin{admonition-remark}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-remark}

\begin{admonition-thought}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-thought}

\begin{admonition-todo}[{}]
Complex content \strikethrough{Hi} and \emph{emph}
\end{admonition-todo}
\end{example}

\subsection{Tables}
\label{develop--main:ROOT:page--writing-guide.adoc---tables}

Tables are a bit complex. Please see \url{https://docs.asciidoctor.org/asciidoc/latest/tables/build-a-basic-table/}

\begin{table}[H]\centering
\caption{My awesome title}

\begin{tblr}{colspec={|[\thicktableline]Q[c,f]|Q[r,m]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{head,1} & {head,2} & {head,3} & {head,4} \\
\hline[\thicktableline]
{1,1} & {1,2} & {1,3} & {1,4} \\
\hline
{2,1} & \SetCell[r=2, c=2]{}{(2,2) -- (3,3)} &  & {(2,4)} \\
\hline
{(3,1)} &  &  & {(3,4)} \\
\hline
{4,1} & {4,2} & {4,3} & {4,4} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{table}[H]\centering
\caption{My awesome title}

\begin{tblr}{colspec={|[\thicktableline]Q[c,f]|Q[r,m]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{head,1} & {head,2} & {head,3} & {\(x^2\)} \\
\hline[\thicktableline]
{1,1} & {1,2} & {1,3} & {\emph{emph}} \\
\hline
{2,1} & \SetCell[r=2, c=2]{}{(2,2) -- (3,3)} &  & {(2,4)} \\
\hline
{(3,1)} &  &  & {(3,4)} \\
\hline
{4,1} & {4,2} & {4,3} & {4,4} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{table}[H]\centering
\caption{My awesome \strikethrough{title}}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{head,1} & {\(x^2\)} \\
\hline[\thicktableline]
{1,1} & {\emph{text}} \\
\hline
{2,1} & {\emph{text}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{} & {Volterra} & {Fredholm} \\
\hline[\thicktableline]
{1st Kind} & {\(f(t) = \int_a^t K(t,s) u(s) \ ds\)} & {\(f(t) = \int_a^b K(t,s) u(s) \ ds\)} \\
\hline
{2nd Kind} & {\(f(t) = g(t)u(t) + \int_a^t K(t,s) u(s) \ ds\)} & {\(f(t) = g(t)u(t) + \int_a^b K(t,s) u(s) \ ds\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{} & {Volterra} & {Fredholm} \\
\hline[\thicktableline]
{1st Kind} & {\(f(t) = \int_a^t K(t,s) u(s) \ ds\)} & {\(f(t) = \int_a^b K(t,s) u(s) \ ds\)} \\
\hline
{2nd Kind} & {\(f(t) = g(t)u(t) + \int_a^t K(t,s) u(s) \ ds\)} & {\(f(t) = g(t)u(t) + \int_a^b K(t,s) u(s) \ ds\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\subsubsection{Customizations}
\label{develop--main:ROOT:page--writing-guide.adoc---customizations}

\begin{itemize}
\item Use the \texttt{autoscale=true} parameter to autoscale tables to page width
    
    \begin{table}[H]\centering
    \begin{adjustbox}{width=\linewidth}
    \begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[l,h]|Q[c,m]|Q[c,m]|Q[l,m]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    \SetRow{font=\bfseries}{Name} & {Requirements} & {Distribution} & {Confidence Interval} & {Notes} \\
    \hline[\thicktableline]
    {\(Z\)} & {\begin{itemize}
    \item \(\overline{X} \sim N\left(\mu, \frac{\sigma^2}{n}\right)\)
    \item The value of \(\sigma^2\) is known
    \end{itemize}} & {\(\displaystyle Z = \frac{\overline{X}-\mu}{\sigma/\sqrt{n}}\sim N(0,1)\)} & {\(\displaystyle \overline{x} \pm z_{\alpha/2}\frac{\sigma}{n}\)} & {\begin{itemize}
    \item \(P(Z > z_{\alpha/2}) = \frac{\alpha}{2}\)
    \end{itemize}} \\
    \hline
    {\(T\)} & {\begin{itemize}
    \item \(\overline{X} \sim N\left(\mu, \frac{\sigma^2}{n}\right)\)
    \item \(\frac{(n-1)S^2}{\sigma^2}\sim \chi^2_{n-1}\)
    \item \(\overline{X} \perp S^2\)
    \end{itemize}} & {\(\displaystyle T = \frac{\overline{X}-\mu}{S/\sqrt{n}}\sim T_{n-1}\)} & {\(\displaystyle \overline{x} \pm t_{\alpha/2}\frac{s}{n}\)} & {\begin{itemize}
    \item \(P(T > t_{\alpha/2}) = \frac{\alpha}{2}\)
    \end{itemize}} \\
    \hline[\thicktableline]
    \end{tblr}
    \end{adjustbox}
    \end{table}
\item Use the \texttt{mode=nested} parameter to have nested tables. Also, dont forget to set the new separator.
    For example \texttt{separator="!", mode=nested}
    
    \begin{table}[H]\centering
    \begin{adjustbox}{width=\linewidth}
    \begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[l,h]|Q[c,m]|Q[c,m]|Q[l,m]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    \SetRow{font=\bfseries}{Name} & {Requirements} & {Distribution} & {Confidence Interval} & {Notes} \\
    \hline[\thicktableline]
    {\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {Small table (1,1)} & {Small table (1,2)} \\
    \hline
    {Small table (2,1)} & {Small table (2,2)} \\
    \hline[\thicktableline]
    \end{tblr}} & {\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {Small table (1,1)} & {Small table (1,2)} \\
    \hline
    {Small table (2,1)} & {Small table (2,2)} \\
    \hline[\thicktableline]
    \end{tblr}} & {\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {Small table (1,1)} & {Small table (1,2)} \\
    \hline
    {Small table (2,1)} & {Small table (2,2)} \\
    \hline[\thicktableline]
    \end{tblr}} & {\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {Small table (1,1)} & {Small table (1,2)} \\
    \hline
    {Small table (2,1)} & {Small table (2,2)} \\
    \hline[\thicktableline]
    \end{tblr}} & {\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {Small table (1,1)} & {Small table (1,2)} \\
    \hline
    {Small table (2,1)} & {Small table (2,2)} \\
    \hline[\thicktableline]
    \end{tblr}} \\
    \hline[\thicktableline]
    \end{tblr}
    \end{adjustbox}
    \end{table}
\end{itemize}

\subsection{Multicol}
\label{develop--main:ROOT:page--writing-guide.adoc---multicol}

Asciidoc does not support multicol. So, we have to use tables.
This works fine in html.
But latex (the unsanitized macro mess that it is) does not work well with some environments in tables.
So instead, we add \texttt{mode=multicol} to tables to use the \texttt{multicol} environment instead.
Note that this only works if the table has exactly one row. Anything more than this is outside the scope of multicol

\subsection{Figures}
\label{develop--main:ROOT:page--writing-guide.adoc---figures}

\subsubsection{Groups of figures}
\label{develop--main:ROOT:page--writing-guide.adoc---groups-of-figures}

Asciidoc has no native support for groups of figures.
This stems from HTML. So, the usual way to implement groups of figures is to put
them in a table.
To let the latex converter know that you are doing this,

\begin{itemize}
\item Use the \texttt{mode=figure} parameter on the table. This is just used to change the caption (if present)
\item Use the \texttt{subfigure\_width} parameter on each image for each the size of the subfigure environment (relative to \texttt{{\textbackslash}textwidth})
    due to bug in latex tabularray \url{https://github.com/lvjr/tabularray/issues/316}
\end{itemize}

\subsection{Quotations}
\label{develop--main:ROOT:page--writing-guide.adoc---quotations}

\begin{listing}[{}]
[quote, "Author name stem:[x^2]", "\"`Characterizations`\"" ]
____
The property that, for an i.i.d. sample, the sample mean and the sample variance are
independent, is a characterization of the normal distribution: for no other distribution
such a property holds. "`Quotes in quote`"
____
\end{listing}

\begin{custom-quotation}[{Author name \(x^2\)}][{``Characterizations''}]
The property that, for an i.i.d. sample, the sample mean and the sample variance are
independent, is a characterization of the normal distribution: for no other distribution
such a property holds. ``Quotes in quote''
\end{custom-quotation}

\begin{custom-quotation}[{Patel, J. K., \& Read, C. B. (1982)}][{Handbook of the normal distribution, p. 81 in the 1st 1982 edition, in chapter ``Characterizations''}]
The property that, for an i.i.d. sample, the sample mean and the sample variance are
independent, is a characterization of the normal distribution: for no other distribution
such a property holds.
\end{custom-quotation}

\begin{custom-quotation}[{Author title only}][{}]
The property that, for an i.i.d. sample, the sample mean and the sample variance are
independent, is a characterization of the normal distribution: for no other distribution
such a property holds.
\end{custom-quotation}

\subsection{Proofs}
\label{develop--main:ROOT:page--writing-guide.adoc---proofs}

For proofs, we use example blocks. The additional benefit of using example blocks is that we can easily make them collapsible.
For a proof use

\begin{listing}[{}]
.Proof title
[.proof%collapsible]
====
Your proof here
====
\end{listing}

The \texttt{\%collapsible} and \texttt{.Proof title} are optional. The default proof title is simply \texttt{Proof}.
At the end of the proof, a box is added. (this may be changed in the future)

Normal text

Normal text

\begin{proof}[{}]
without title
\end{proof}

\begin{proof}[{with title}]
hi
\end{proof}

\begin{proof}[{}]
We prove that \(x=x\)

\begin{equation*}
x=(x-1) + 1 = x
\end{equation*}
\end{proof}

\begin{proof}[{with title}]
We prove that \(x=x\)

\begin{equation*}
x=(x-1) + 1 = x
\end{equation*}

End line
\end{proof}

\begin{proof}[{}]
Multi

Line

Proof

How

Is

This

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{proof}

Multi

Line

Normal text

How

Is

This

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

\subsection{Theorems}
\label{develop--main:ROOT:page--writing-guide.adoc---theorems}

Theorems, lemmas, corollaries and propositions all use sidebars.
Use

\begin{listing}[{}]
.Lemma title
[.lemma]
****
****
\end{listing}

hi hi

\begin{theorem}[{}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{theorem}

\begin{theorem}[{Theorem with title}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{theorem}

\begin{lemma}[{}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{lemma}

\begin{lemma}[{Lemma with title}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{lemma}

\begin{corollary}[{}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{corollary}

\begin{corollary}[{Corollary with title}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{corollary}

\begin{proposition}[{}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{proposition}

\begin{proposition}[{Proposition with title}]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{proposition}

\section{Bug tests}
\label{develop--main:ROOT:page--writing-guide.adoc---bug-tests}

This part contains checks for implementation bugs which should be fixed.

\begin{itemize}
\item There was a bug in the latex converter code which caused multiple quotations in a line to be incorrectly formatted.
    
    ``One'', ``Two''.
    
    This was caused because of the way how asciidoctor converts inline quoted. It was converted as follows
    
    \begin{listing}[{}]
    "`One`", "`Two`"
    \texttt{One'', }Two''
    \end{listing}
    
    I have no idea why this happened, my guess is that asciidoctor does multiple passes.
    I fixed it by encoding the backticks and quotation marks until the end.
\item There is a bug which is causing \texttt{{\textbackslash}end\{document\}} to be inserted????
    
    \begin{equation*}
    a_{ij} = \begin{cases}
        1 \quad &\text{if the $i$'th block contains the $j$'th variety}\\
        0 \quad &\text{otherwise}
    \end{cases}
    \end{equation*}
    
    The above code is producing
    
    \begin{listing}[{}]
    \begin{equation*}
    a_{ij} = \begin{cases}
        1 \quad &\text{if the $i
    \end{document}
    th block contains the $j
    \end{document}
    th variety}\\
        0 \quad &\text{otherwise}
    \end{cases}
    \end{equation*}
    \end{listing}
    
    The problem was with javascript's replace function: \url{https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global\_Objects/String/replace}
    which has a massive footgun.
    The second parameter, even though it is a string, has some special semantics.
    Instead, we just use a function for the second parameter as done in \url{https://stackoverflow.com/questions/75140284/how-to-use-javascript-replace-without-triggering-the-special-replacement-patte}
\end{itemize}
\end{document}
