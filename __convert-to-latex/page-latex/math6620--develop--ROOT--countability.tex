\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Countability Axioms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \((X, \mathcal{T})\) be a topological space
and \(x \in X\).
Then, we call \(\mathcal{B} \subseteq \mathcal{T}\)
a \emph{countable basis} at \(x\) if \(\mathcal{B}\) is countable and for each open \(U \in \mathcal{T}\),
there exists \(B \in \mathcal{B}\) with \(x \in B \subseteq U\).

\begin{admonition-note}[{}]
In previous sections, we could have defined a pointwise basis similarly by omitting the ``countable'' part.
In such a case usual basis is a union of bases at each point.
\end{admonition-note}

We define the following two properties

\begin{description}
\item[First countable] \(X\) is \emph{first countable} iff there exists a countable basis at each \(x \in X\).
\item[Second countable] \(X\) is \emph{second countable} iff \(X\) has a basis which countable.
\end{description}

Note that all metric spaces are automatically first countable by considering the balls of radius \(\frac{1}{n}\),
but not all are second countable. We give a few examples.

\begin{example}[{\(\mathbb{R}\) is second countable}]
Consider

\begin{equation*}
\mathcal{B} = \left\{B\left(a,b\right) \ | \ a, b \in \mathbb{Q}, \ a < b\right\}
\end{equation*}

Then, \(\mathcal{B}\) is a countable basis for \(\mathbb{R}\).

Note that this also implies that \(\mathbb{R}^n\) and \(\mathbb{R}^\omega\) (with product topology)
are also second countable.
\end{example}

\begin{example}[{\(\mathbb{R}^\omega\) with the uniform topology is not second countable}]
We would use a counting argument. Consider the set \(\mathbb{Z}^\omega \subseteq \mathbb{R}^\omega\)
and the open balls of radius \(\frac12\) around each \(\vec{x} \in \mathbb{Z}^\omega\).
These balls are disjoint.
Then, any basis should contain open sets which are contained in exactly one of the aforementioned balls
and hence any basis would have cardinality at least \(|\mathbb{Z}^\omega| = \omega^\omega = |\mathbb{R}|\).
Therefore there is no countable basis for the uniform topology on \(\mathbb{R}^\omega\).
\end{example}

\begin{theorem}[{Limits and continuity}]
Let \(X\) be first countable. Then

\begin{enumerate}[label=\arabic*)]
\item \(\forall x \in X\) and \(A \subseteq X\),
    
    \begin{equation*}
    x \in \closure A \iff \exists \{a_n\} \subseteq A: a_n \to x
    \end{equation*}
\item \(f: X\to Y\) is continuous iff
    
    \begin{equation*}
    \forall x_n \to x: f(x_n) \to f(x)
    \end{equation*}
\end{enumerate}

\begin{proof}[{}]
First suppose that \(x \in \closure A\). Consider the countable basis \(B_1, B_2, \ldots\) at \(x\).
We may assume that this countable basis is nested by redefining \(B_n' = \bigcap_{i=1}^n B_i\) if necessary.
Choose \(a_n \in B_n\). Then, for any open neighbourhood \(U\) about \(x\), there exists
a \(N\) such that \(x \in B_N \subseteq U\) and hence for each \(n\geq N\), \(a_n \in B_n \subseteq B_N \subseteq U\).
Therefore \(a_n \to x\). The converse statement is true for all metric spaces and easy to see.

Now, suppose that \(f: X \to Y\) is continuous and \(x_n \to x\). Let \(B_1, \ldots B_n\)
be a countable nested basis at \(x\). Consider arbitrary open \(V \subseteq Y\) about \(f(x)\).
Then, \(f^{-1}(V)\) is open. Since \(x_n \to x\), there exists \(N \geq 0\)
such that for each \(n \geq N\), \(x_n \in f^{-1}(V)\) and hence \(f(x_n) \in V\).
Therefore \(f(x_n) \to f(x)\).

Conversely, suppose for each \(x_n \to x\), \(f(x_n) \to f(x)\).
Consider \(A \subseteq X\). Then, \(x \in \closure(A)\) implies
that there exists \(\{a_n\} \subseteq A\) which converges to \(x\) (by first statement).
This implies that \(f(a_n)\to f(x)\) and \(f(x) \in \closure f(A)\).
Therefore \(f(\closure(A))\subseteq \closure f(A)\) and \(f\)
is continuous.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(X\) be a first/second countable topological space. Let \(A\subseteq X\) be a subspace.
Then \(A\) is first/second countable (respectively).

\begin{admonition-note}[{}]
This is simple to prove and results directly from the definition of open
sets in \(A\).
\end{admonition-note}
\end{theorem}

\begin{theorem}[{}]
Let \(\{X_i\}_{i=1}^\infty\) be a collection of topological spaces.
Then

\begin{enumerate}[label=\arabic*)]
\item \(X=\prod_{i=1}^\infty X_i\) is first countable iff each of the \(X_i\) are first countable.
\item \(X=\prod_{i=1}^\infty X_i\) is second countable iff each of the \(X_i\) are second countable.
\end{enumerate}

\begin{admonition-note}[{}]
The same is true for finite products as well.
\end{admonition-note}

\begin{proof}[{}]
We prove the forward directions quickly. Recall that \(\pi_n: \prod_{i=1}^\infty X_i \to X_n\)
are continuous and open maps.

\begin{itemize}
\item Suppose \(X\) is first countable and consider \(x_n \in X_n\).
    Then, \(\vec{y}\) defined by \(y_n=x_n\) and \(0\) otherwise has a countable basis \(B_1, B_2, \ldots\).
    We claim that the \(\pi_n(B_i)\) are a countable basis at \(x_n\).
    Consider arbitrary open \(U\subseteq X_n\). Then, since \(\pi_n\) is continuous,
    \(\pi_n^{-1}(U)\) is open and there is a \(\vec{y} \in B_m \subseteq \pi_n^{-1}(U)\).
    Therefore \(x_n \in \pi_n(B_m) \subseteq U\) as desired.
\item Suppose \(X\) is second countable and let \(B_1, B_2, \ldots\)
    be a countable basis. Then, we claim that the \(\pi_n(B_i)\) are a countable basis for \(X_n\).
    Consider arbitrary open \(U \subseteq X_n\). Then, since \(\pi_n\) is continuous,
    \(\pi_n^{-1}(U)\) is open and the union of \(B_i\). Therefore \(U\)
    is the union of \(\pi_n(B_i)\) as desired.
\end{itemize}

Now, suppose that each of the \(X_i\) are first countable. Consider \(\vec{x} \in X\).
Let \(\{B_j^i\}_{j=1}^\infty \subseteq X_i\) be a countable basis about \(x_i \in X_i\) for each \(i\).
Now, consider

\begin{equation*}
\mathcal{U} = \bigcup_{n=1}^\infty \left\{U_{j_1}^1\times U_{j_2}^2 \times \cdots \times U_{j_n}^n\times X_{n+1}\times \cdots \right\}
\end{equation*}

Then, \(\mathcal{U}\) is countable since

\begin{equation*}
|\mathcal{U}| = \sum_{n=1}^\infty |\mathbb{N}^n| = \sum_{n=1}^\infty |\mathbb{N}| = |\mathbb{N}\times \mathbb{N}| = |\mathbb{N}|
\end{equation*}

Also, it is clear that \(\mathcal{U}\) is a basis at \(\vec{x}\) since for any
\(U^* = U_1\times U_2\times \cdots U_n \times X_{n+1}\times \cdots \), we can find \(U^i_{j_i} \subseteq U_i\)
and hence \(\vec{x} \in  U^1_{i_1} \times \cdots U^n_{i_n}\times X_{n+1} \times \cdots \subseteq U^*\) as desired.

Also, if each of the \(X_i\) are second countable, it is clear from the above construction that \(X\)
is also second countable.
\end{proof}
\end{theorem}

\begin{proposition}[{}]
Let \(X\) be first countable and there exists
countable \(A\) such that \(\closure A = X\).
Then \(X\) is second countable.

\begin{admonition-warning}[{}]
It is not clear if this result is true.
\end{admonition-warning}
\end{proposition}

\begin{theorem}[{}]
Let \(X\) be a second countable topology. Then

\begin{enumerate}[label=\arabic*)]
\item Every open cover has a countable subcover
\item There exists a countable \(A \subseteq X\) such that \(\closure A = X\)
\end{enumerate}

\begin{proof}[{}]
Let \(B_1, B_2, \ldots B_n, \ldots\) be a countable basis for \(X\).

Let \(\mathcal{A}\) be a open cover of \(X\).
For each \(i\) choose \(A_i \in \mathcal{A}\)  such that \(B_i \subseteq A_i\) if possible.
Note that multiple choices may be possible. In such a case, any choice is suitable.
We must show that the \(A_i\) are covers \(X\).
For each \(x\), \(x\) is covered by some \(A \in \mathcal{A}\)
and there is a \(B_i\) such that \(x\in B_i \subseteq A\). Therefore, there is
an \(A_i\) which contains \(B_i\) and contains \(x\).
Therefore the \(A_i\) form a countable subcover.

For each \(i\) choose arbitrary \(x_i \in B_i\). We claim that \(\closure \{x_i\} = X\).
Let \(x \in X\) and consider a nested countable cover \(C_j\) about \(x\).
Then, for each \(j\), there is a \(B_i \subseteq C_j\) and hence \(x_i \in C_j\).
Define \(c_j = x_i\). Then, we want to show that the \(c_j \to x\).
This is clear since for each open \(U\) containing \(x\), there is a \(C_j \subseteq U\) and by the nested
property of the \(C_j\), we have that \(c_j \to x\). Therefore \(x \in \closure \{x_i\}\)
for each \(x \in X\) and we have obtained our countable dense subset.
\end{proof}
\end{theorem}
\end{document}
