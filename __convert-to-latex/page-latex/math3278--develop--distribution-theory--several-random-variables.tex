\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Distribution of Several Random Variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

% Title omitted
Let \(\vec{X} = (X_1\ldots X_n)\) be a vector of random variables defined
on the same probability space \((S, F, P)\). Then, the \emph{joint distribution function}
is defiend as

\begin{equation*}
F(\vec{x}) = F(x_1\ldots x_n) = P(X_1 \leq x_1, \ldots X_n \leq x_n)
\end{equation*}

\begin{admonition-warning}[{}]
The ideas presented in class were mainly for 2 variables. So the extensions I made
were the ones which made the most sense to me. Please take caution when using them and if
what I have said is wrong, please inform me.
\end{admonition-warning}

\section{Joint Probability Density Function}
\label{develop--math3278:distribution-theory:page--several-random-variables.adoc---joint-probability-density-function}

If \(\vec{X}\) is a vector of absolutely continuous random varaibles, then there exists
a \emph{joint pdf} of \(\vec{X}\) with the following properties

\begin{enumerate}[label=\arabic*)]
\item \(\forall \vec{x} \in \mathbb{R}^n: f(\vec{x}) \geq 0\)
\item \(\int_{\mathbb{R}^n} f(\vec{x})\ dA = 1\)
\item For any region \(A \in \mathbb{R}^n\), \(P(\vec{X} \in A) = \int_A f(\vec{x})\ dA\)
\end{enumerate}

\section{Marginal Density Function}
\label{develop--math3278:distribution-theory:page--several-random-variables.adoc---marginal-density-function}

Let \(\vec{X} = (X,Y)\) consist of continuous random variables with joint density \(f\).
Then, we define the marginal density of \(X\) as follows

\begin{equation*}
f_X(x) = \int_{\mathbb{R}} f(\vec{x}) dy
\end{equation*}

The marginal density of \(Y\) is defined similarly. Also, notice that \(f_X\)
is a valid pdf for variable \(X\).

\begin{admonition-note}[{}]
This idea can be extended to more than two variables where the integral is a \(k\)-integral
over the \(k\) variables which you want to "eliminate". For example

\begin{equation*}
f_{X_1, X_2}(x_1, x_2) = \iiint_{\mathbb{R_3}} f(x_1,x_2,x_3,x_4,x_5) \ dx_3 \ dx_4\ dx_5
\end{equation*}
\end{admonition-note}

\section{Conditional Density}
\label{develop--math3278:distribution-theory:page--several-random-variables.adoc---conditional-density}

Let \(\vec{X} = (X,Y)\) consist of continuous random variables with joint density \(f\).
Then, we define the conditional density of \(Y\) given \(x\) as

\begin{equation*}
f_{Y|X=x} = \frac{f(x,y)}{f_X(x)}
\end{equation*}

where \(f_X(x) > 0\). Then \(f_{Y|X=x}\) is a valid pdf for variable \(Y\).

\begin{admonition-note}[{}]
Again, this idea can be extended to more than two variables where the condition could be placed
on one or more variables. For example

\begin{equation*}
f_{X_1,X_2,X_3|X_4=x_4,X_5=x_5}(X_1,X_2,X_3)
= \frac{f(x_1,x_2,x_3,x_4,x_5)}{f_{X_4,X_5}(x_4,x_5)}
\end{equation*}
\end{admonition-note}

\section{Independence}
\label{develop--math3278:distribution-theory:page--several-random-variables.adoc---independence}

Let \(\vec{X} = (X,Y)\) consist of continuous random variables with joint density \(f\).
Then, \(X\) and \(Y\) are independent if

\begin{equation*}
\forall (x,y) \in \mathbb{R}^2: f(x,y) = f_X(x)f_Y(y)
\end{equation*}

This could be equivalently stated using the following condition

\begin{equation*}
\exists f_1, f_2: \mathbb{R}\to \mathbb{R}: \forall (x,y) \in \mathbb{R}^2: f(x,y) = f_1(x)f_2(y)
\end{equation*}

That is \(f\) could be factorized into two univariate functions. Also, note that the support must
be rectangular (a box).

\begin{admonition-note}[{}]
In order to extend this, we would say that \(X_1\ldots X_n\) are independent if
each subset is independent.
\end{admonition-note}

\section{Expectation}
\label{develop--math3278:distribution-theory:page--several-random-variables.adoc---expectation}

Let \(\vec{X}\) consist of \(n\) continuous random variables and \(k:\mathbb{R}^n\to \mathbb{R}^m\).
Then the expected value of \(k(\vec{X})\) is defined as

\begin{equation*}
E[k(\vec{X})] = \int_{\mathbb{R}^n} k(\vec{x})f(\vec{x}) \ dA
\end{equation*}

Then \(E\) is a linear transformation in from vector space \((\mathbb{R}^n\to \mathbb{R}^m, \mathbb{R})\)
to \(\mathbb{R}^m\).

\subsection{Linear operations}
\label{develop--math3278:distribution-theory:page--several-random-variables.adoc---linear-operations}

Let \(k \in \mathcal{L}(\mathbb{R}^n \to \mathbb{R}^m)\), then we would refer to it by its matrix.
Then, notice that

\begin{equation*}
E[k(\vec{X})]
= \int_{\mathbb{R}^n} k\vec{x}f(\vec{x}) \ dA
= k\int_{\mathbb{R}^n} \vec{x}f(\vec{x}) \ dA
= kE[\vec{X}]
\end{equation*}

Since the integral is defined elementwise and for each index we get an integral
of linear combination of the components of \(\vec{x}\) which would be
same as the linear combination of each of the integrals. Also, notice
that an analogous result is found for right multiplication of matrices.
\end{document}
