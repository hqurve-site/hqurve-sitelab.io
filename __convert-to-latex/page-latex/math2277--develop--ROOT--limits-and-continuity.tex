\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Limits and Continuity}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Limits}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---limits}

Let \(A \subseteq \mathbb{R}\),
\(f\colon A \to \mathbb{R}\) and suppose that
\(a \in A'\). Then, we say that \(f(x)\) approaches
the limit \(L \in \mathbb{R}\) as \(x\) approaches
\(a\) iff

\begin{equation*}
\begin{aligned}
    \forall \varepsilon > 0: \exists \delta > 0: \forall x \in A: 0 < |x-a| < \delta \implies |f(x) - L| < \varepsilon\end{aligned}
\end{equation*}

and is written symbolically as \(\lim_{x \to a} f(x) = L\).
Additionally, since
\(|(f(x) - L) - 0|=|f(x) - L| = ||f(x) - L| - 0|\), we see
that

\begin{equation*}
\lim_{x\to a} f(x) = L \iff \lim_{x \to a} f(x) - L = 0 \iff \lim_{x \to a} |f(x) - L| = 0
\end{equation*}

Note, that the above definition could be written equivalently as

\begin{equation*}
\lim_{x \to a} f(x) = L \iff
    \forall \varepsilon >0: \exists \delta > 0: \forall x \in N^*(a, \delta): f(x) \in N(L, \varepsilon)
\end{equation*}

which immediately reveals why we require that \(a\) be a limit
point of \(A\).

\subsection{Uniqueness of the limit}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---uniqueness-of-the-limit}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A \to \mathbb{R}\) and \(a \in A'\), then

\begin{equation*}
\lim_{x \to a} f(x) = L_1 \text{ and } \lim_{x \to a} f(x) = L_2
    \implies L_1 = L_2
\end{equation*}

or in other words, the limit is well defined or the limit is unique if
it exists.

\begin{example}[{Proof}]
Suppose \(\lim_{x \to a} f(x) = L_1\) and
\(\lim_{x \to a} f(x) = L_2\) and consider
\(\varepsilon > 0\), then since
\(\frac{\varepsilon}{2} > 0\),
\(\exists \delta_1, \delta_2\) such that

\begin{equation*}
x \in N^*(x, \delta_1) \implies |f(x) -L_1| < \tfrac{\varepsilon}{2}
\end{equation*}

\begin{equation*}
x \in N^*(x, \delta_2) \implies |f(x) -L_2| < \tfrac{\varepsilon}{2}
\end{equation*}

Then, by letting \(\delta = \min(\delta_1, \delta_2)\) we get
that \(N^*(x, \delta) \subseteq N^*(x, \delta_1)\) and
\(N^*(x, \delta_2)\) hence

\begin{equation*}
x \in N^*(\delta)
        \implies |L_1 - L_2|
            \leq |f(x) - L_1| + |f(x) - L_2|
            < \tfrac{\varepsilon}{2} + \tfrac{\varepsilon}{2} = \varepsilon
\end{equation*}

Then, since \(\varepsilon\) was arbitrary,
\(L_1 - L_2 = 0\) and we are done.

 ◻
\end{example}

\subsection{Nice properties}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---nice-properties}

Let \(A \subseteq \mathbb{R}\) and
\(f, g\colon A \to \mathbb{R}\) and \(a \in A'\)
such that

\begin{equation*}
\lim_{x \to a} f(x) = L \text{ and } \lim_{x\to a} g(x) = M
\end{equation*}

then

\begin{itemize}
\item \(\lim_{x \to a} (f(x) + g(x)) = L + M\)
\item \(\lim_{x \to a} kf(x) = kL\) where \(k \in \mathbb{R}\)
\item \(\lim_{x \to a} f(x)g(x) = LM\)
\item \(\lim_{x \to a} \tfrac{1}{f(x)} = \tfrac{1}{L}\) if \(L \neq 0\)
\end{itemize}

\begin{example}[{Proof}]
The proof of the first three are omitted as I refuse to rewrite a proof
I did numerous times in Math1151 (calc 2).

For the forth one one, consider \(\epsilon > 0\), Notice that
since \(L \neq 0\), \(\tfrac{|L|}{2} > 0\) and then
\(\exists \delta_1 > 0\) such that

\begin{equation*}
\forall x \in A: x \in N^*(a, \delta_1)
        \implies f(x) \in N\left(L, \frac{|L|}{2}\right)
        \implies |f(x)| > \frac{|L|}{2} > 0
        \implies \frac{1}{|f(x)|} < \frac{2}{|L|}
\end{equation*}

also, since \(\frac{|L|^2\epsilon}{2} > 0\),
\(\exists \delta_2 > 0\) such that

\begin{equation*}
\forall x \in A: x \in N^*(a, \delta_1)
        \implies |f(x) - f(a)| < \frac{|L|^2\epsilon}{2}
\end{equation*}

now, let \(\delta = \min(\delta_1, \delta_2) > 0\) and

\begin{equation*}
\forall x \in A: x \in N^*(a, \delta) \implies
        \left|\frac{1}{(f(a))} - \frac{1}{L}\right|
        = \frac{|f(x) - L|}{|f(x)| |L|}
        < \left(\frac{|L|^2\epsilon}{2}\right)\left(\frac{2}{|L|}\right) \left(\frac{1}{|L|}\right)
        = \epsilon
\end{equation*}

and we are done.

 ◻
\end{example}

\subsection{Preservation of inequalities}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---preservation-of-inequalities}

Let \(A \subseteq \mathbb{R}\) and
\(f, g\colon A \to \mathbb{R}\) and \(a \in A'\).
Then if \(f(x) \leq g(x)\) \(\forall x \in A\) and
limits of \(f\) and \(g\) both exists at
\(a\),

\begin{equation*}
\lim_{x \to a} f(x) \leq \lim_{x \to a} g(x)
\end{equation*}

That is, the limit preserves inequalities.

\begin{description}
\item[Note] Strict inequalities are not preserved as equality may occur. For example
    consider \(f(x) = \frac{x}{2}\) and \(g(x) = x\) on
    \((0, \infty)\) with limit point \(0\).
\end{description}

\begin{example}[{Proof}]
Instead of proving this fact directly, we would consider a function
\(h:A \to \mathbb{R}\) such that
\(h(x) \geq 0\ \forall x \in A\) and
\(\lim_{x \to a} f(x)\) exists and is equal to
\(L\). Now, if \(L < 0\), we get that

\begin{equation*}
\exists \varepsilon = -L > 0: \forall \delta > 0: \exists x \in N^*(x, \delta): |f(x) -L| = f(x) - L \geq -L =\varepsilon
\end{equation*}

Note that the above statement is the negation of the limit definition
and hence \(L \not< 0\).

Now, back to our initial statement, if \(f(x) \leq g(x)\),
then \(g(x) - f(x) \geq 0\). It then follows that

\begin{equation*}
\lim_{x \to a}(g(x) - f(x)) \geq 0 \implies \lim_{x \to a} f(x) \leq \lim_{x \to a} g(x)
\end{equation*}

and we are done.

 ◻
\end{example}

\subsection{Squeeze/Sandwich Theorem}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---squeezesandwich-theorem}

Let \(A \subseteq \mathbb{R}\) and
\(f, g, h\colon A \to \mathbb{R}\) and \(a \in A'\).
Then, if \(f(x) \leq g(x) \leq h(x)\ \forall x \in A\) and
\(\lim_{x \to a} f(x) = \lim_{x\to a} h(x) = L\) then

\begin{equation*}
\lim_{x \to a} g(x) = L
\end{equation*}

\begin{example}[{Proof}]
Despite all of its hype, its proof is quite simple.

\begin{equation*}
\begin{aligned}
        &\lim_{x \to a} f(x) = \lim_{x\to a} h(x) = L\\
        &\implies \forall \varepsilon > 0: \exists \delta_1, \delta_2 > 0:
            \left[\forall x \in N^*(x, \delta_1): f(x) \in N(L, \varepsilon)\right]
            \ \land \
            \left[\forall x \in N^*(x, \delta_2): h(x) \in N(L, \varepsilon)\right]
            \\
        &\implies \forall \varepsilon > 0: \exists \delta = \min(\delta_1, \delta_2) > 0:
            \left[\forall x \in N^*(x, \delta): f(x), h(x) \in N(L, \varepsilon)\right]
            \\
        &\implies \forall \varepsilon > 0: \exists \delta = \min(\delta_1, \delta_2) > 0:
            \forall x \in N^*(x, \delta): g(x) \in N(L, \varepsilon)
    \end{aligned}
\end{equation*}

since \(N(L, \varepsilon)\) is an interval and hence
connected.

 ◻
\end{example}

\subsection{Nice theorem}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---nice-theorem}

Let \(A \subseteq \mathbb{R}\) and
\(f, g \colon A \to \mathbb{R}\) and \(a \in A'\).
Then, if \(f\) is bounded and
\(\lim_{x\to a} g(x) = 0\),
\(\lim_{x\to a} f(x)g(x) = 0\).

\begin{example}[{Proof}]
Since \(f\) is bounded, \(\exists M > 0\) such that
\(-M\) is a lower bound and \(M\) is an upper bound
for \(f(A)\). Furthermore,
\(M \geq |f(x)| \ \forall x \in A\).

Now, let \(\varepsilon > 0\), then
\(\frac{\varepsilon}{M} > 0\) and
\(\exists \delta > 0\) such that

\begin{equation*}
\forall x \in A: x \in N^*(a, \delta)
        \implies |g(x)| < \frac{\varepsilon}{M}
        \implies |f(x)g(x)| \leq M|g(x)| < \varepsilon
\end{equation*}

and we are done.

 ◻
\end{example}

\section{Left and Right sided limits}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---left-and-right-sided-limits}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A \to \mathbb{R}\). Then, we define the left and
right hand limits as follows.

\begin{itemize}
\item Let \(a \in (A \cap (-\infty, a))'\) then
    \(L \in \mathbb{R}\) is the \emph{left-hand limit} of
    \(f\) at \(a\) iff
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists \varepsilon > 0:  \forall x \in A: -\delta < x-a < 0 \implies |f(x) - L| < \varepsilon
    \end{equation*}
    
    and we write \(f(x-) = \lim_{x\to a^-} f(x) = L\)
\item Let \(a \in (A \cap (a, \infty))'\) then
    \(L \in \mathbb{R}\) is the \emph{right-hand limit} of
    \(f\) at \(a\) iff
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists \varepsilon > 0:  \forall x \in A: 0 < x -a < \delta \implies |f(x) - L| < \varepsilon
    \end{equation*}
    
    and we write \(f(x+) = \lim_{x\to a^+} f(x) = L\)
\end{itemize}

Note, both of these one-sided limits could be considered be considered
as limits of the function \(f\) with a constrained domain.
That is, if we let
\(g\colon (A \cap (-\infty, a))\to \mathbb{R}\) and
\(h\colon (A\cap (a, \infty)) \to \mathbb{R}\)

\begin{equation*}
\lim_{x \to a} g(x) = f(a-)
    \quad\text{and}\quad
    \lim_{x \to a} h(x) = f(a+)
\end{equation*}

granted that they exist. As a result, all the properties for standard
limits also hold for one-sided limits.

\subsection{Relation to general limit}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---relation-to-general-limit}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A \to \mathbb{R}\) and
\(a \in (A \cap (a, \infty))' \cap (A \cap (-\infty, a))'\).
Then

\begin{equation*}
\lim_{x \to a}f (x) = L \iff f(a+) = f(a-) = L
\end{equation*}

\begin{example}[{Proof}]
The forward direction is quite simple. Let \(\varepsilon > 0\)
then, \(\exists \delta > 0\) such that

\begin{equation*}
\begin{aligned}
        &\forall x \in A: x \in N^*(a, \delta) \implies |f(x) - L| < \varepsilon\\
        &\implies \forall x \in A: (- \delta < x-a < 0 \ \lor \ 0 < x-a < \delta) \implies |f(x) - L| < \varepsilon\\
        &\implies \forall x \in A: \left[ - \delta < x-a < 0 \implies |f(x) - L| < \varepsilon\right] \ \land \ \left[0 < x-a < \delta \implies |f(x) - L| < \varepsilon\right]\\
        &\implies \left[\forall x \in A:  - \delta < x-a < 0 \implies |f(x) - L| < \varepsilon\right]\\
        &\quad\quad\ \land \  \left[\forall x \in A: 0 < x-a < \delta \implies |f(x) - L| < \varepsilon\right]
    \end{aligned}
\end{equation*}

and hence \(f(a+) = f(a-) = L\).

Now, consider the reverse direction. Let \(\varepsilon > 0\)
then \(\exists \delta_1, \delta_2 > 0\) such that

\begin{equation*}
\forall x \in A: - \delta_1 < x-a < 0 \implies |f(x) - L| < \varepsilon
\end{equation*}

and

\begin{equation*}
\forall x \in A: 0 < x-a < \delta_2 \implies |f(x) - L| < \varepsilon
\end{equation*}

Then, let \(\delta = \min(\delta_1, \delta_2)\) and consider
arbitrary \(x \in A\) then

\begin{equation*}
\begin{aligned}
        x \in N^*(a, \delta)
        &\implies 0 < x -a < \delta \ \lor \ -\delta < x-a < 0\\
        &\implies 0 < x -a < \delta_1 \ \lor \ -\delta_2 < x-a < 0\\
        &\implies |f(x) - L|< \varepsilon \ \lor \ |f(x) - L| < \varepsilon\\
        &\implies |f(x) - L|< \varepsilon
    \end{aligned}
\end{equation*}

and hence \(\lim_{x \to a} f(x) = L\).

 ◻
\end{example}

\section{Continuity}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---continuity}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A \to \mathbb{R}\) and \(a \in A\). Then
\(f\) is \emph{continuous} at \(a\) if

\begin{equation*}
\forall \varepsilon > 0: \exists \delta > 0: \forall x \in A: x \in N(a, \delta) \implies f(x) \in N(f(a), \varepsilon)
\end{equation*}

Note that the \(a\) need not be a limit point, it can even be
a isolated.

\subsection{Isolated points and relation to limits}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---isolated-points-and-relation-to-limits}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A \to \mathbb{R}\) and \(a \in A\). Then,
we have one of the following

\begin{enumerate}[label=\roman*)]
\item If \(a\) is an isolated point, \(f\) is immediately continuous at \(a\)
\item If \(a \in A'\), \(f\) is continuous at \(a\) iff \(\lim_{x\to a} f(x) = f(a)\)
\end{enumerate}

\begin{example}[{Proof}]
Each part will be proven separately

\begin{description}
\item[Part (i)] If \(a\) is an isolated point,
    \(\exists \delta > 0\) such that
    \(\forall x \in A: x \notin N^*(a, \delta)\) and hence
    
    \begin{equation*}
    \forall x \in A: x \in N(a, \delta) \implies x = a \implies |f(x) - f(a)| = 0 < \varepsilon,\ \forall \varepsilon > 0
    \end{equation*}
\item[Part (ii)] Let \(a \in A'\)
    
    \begin{equation*}
    \begin{aligned}
            &f \text{ is continuous at } a\\
            &\iff \forall \varepsilon > 0: \exists \delta > 0: x \in N(a, \delta) \implies f(x) \in N(f(a), \varepsilon)\\
            &\iff \forall \varepsilon > 0: \exists \delta > 0: x \in N^*(a, \delta) \implies f(x) \in N(f(a), \varepsilon)\\
            &\iff \lim_{x \to a} f(x) = f(a)
        \end{aligned}
    \end{equation*}
    
    Note that the second reverse implication is true since
    \(x = a \implies f(x) = f(a) \in N(f(a), \varepsilon)\).
\end{description}

 ◻
\end{example}

\subsection{Nice properties}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---nice-properties-2}

Let \(A \subseteq \mathbb{R}\) and
\(f, g\colon A \to \mathbb{R}\) and \(a \in A\). If
\(f\) and \(g\) are continuous at \(a\),
then

\begin{enumerate}[label=\roman*)]
\item \(f+ g\) is continuous at \(a\).
\item \(kf\) is continuous at \(a\) where
    \(k \in \mathbb{R}\).
\item \(fg\) (multiplication) is continuous at \(a\).
\item \(\frac{f}{g}\) is continuous at \(a\) if
    \(g(a) \neq 0\).
\end{enumerate}

\begin{example}[{Proof}]
The proof of these are omitted because I refuse to rewrite proofs I did
numerous times in Math1151 (calc 2). Also, the proof if these properties
are very similar to the analogous properties of limits.

 ◻
\end{example}

\subsection{Polynomials and Rational functions}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---polynomials-and-rational-functions}

A \emph{rational function} is a function \(R\) which can be written
as the ratio of two polynomial functions. That is, if \(P\)
and \(Q\) are polynomials, then we define the rational
function \(R\) as

\begin{equation*}
R(x) = \frac{P(x)}{Q(x)}
\end{equation*}

with domain being the set of points where \(Q(x) \neq 0\).
From this definition, it is clear that polynomial functions are also
rationals with \(Q\) being the constant function
\(1\).

Then, we have the following theorems

\begin{enumerate}[label=\roman*)]
\item Every polynomial is continuous on \(\mathbb{R}\).
\item Every rational function is continuous on its domain.
\end{enumerate}

\begin{example}[{Proof}]
Firstly, note that the constant function \(f(x) = 1\) and
\(f(x) = x\) are both continuous on \(\mathbb{R}\).
Then, the result follows from the nice properties above.

 ◻
\end{example}

\subsection{Composition of functions}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---composition-of-functions}

Let \(A, B \subseteq \mathbb{R}\) and
\(f\colon A\to \mathbb{R}\) and
\(g\colon B \to \mathbb{R}\) and
\(f(A) \subseteq B\). Then, if \(f\) is continuous
at \(a \in A\) and \(g\) is continuous at
\(f(a) \in B\), then
\(g \circ f\colon A\to \mathbb{R}\) is continuous at
\(a\).

\begin{example}[{Proof}]
Consider \(\epsilon > 0\), then
\(\exists \delta_1 > 0\) such that

\begin{equation*}
\forall y \in B: y \in N(f(a), \delta_1) \implies g(y) \in N(g(f(a)), \epsilon)
\end{equation*}

and since \(\delta_1 > 0\), \(\exists \delta > 0\)
such that

\begin{equation*}
\forall x \in A: x \in N(a, \delta) \implies f(x) \in N(f(a), \delta_1) \implies g(f(x)) \in N(g(f(a)), \epsilon)
\end{equation*}

 ◻
\end{example}

\subsection{Equivalent definition for continuity}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---equivalent-definition-for-continuity}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A \to \mathbb{R}\). Then \(f\) is
continuous iff \(\forall\) open sets
\(G \subseteq \mathbb{R}\), \(\exists\) an open set
\(H\) such that \(H \cap A = f^{-1}(G)\).

As a result, if \(A\) is a open set, the pre-image of any open
set \(G\), with respect to \(f\), is also open.

\begin{example}[{Proof}]
Firstly consider an arbitrary \(a \in f^{-1}(G)\). Then since
\(G\) is open and \(f(a) \in G\),
\(\exists \varepsilon_a > 0\) such that
\(N(f(a) , \varepsilon_a) \subseteq G\). Next since
\(G\) is continuous, \(\exists \delta_a > 0\) such
that
\(f(A \cap N(a, \delta_a)) \subseteq N(f(a), \varepsilon_a) \subseteq G\).
We now define the following family

\begin{equation*}
\mathcal{A} = \left\{N(a, \delta_a): a \in f^{-1}(G)\right\}
\end{equation*}

and \(H = \bigcup_{a \in f^{-1}(G)} N(a, \delta_a)\). Then,
since each neighbourhood is open, \(H\) is also open. Also,
note that \(f^{-1}(G) \subseteq H \cap A\) and

\begin{equation*}
H \cap A = \bigcup_{a \in f^{-1}(G)} A \cap N(a, \delta_a) \subseteq f^{-1}(G)
\end{equation*}

since the image of each \(A \cap N(a, \delta_a)\) is a subset
of \(G\). Therefore, we have found a open set \(H\)
satisfying \(H \cap A = f^{-1}(G)\).

Conversely, consider an arbitrary \(a \in A\) and
\(\varepsilon > 0\) and
\(G = N(f(a), \varepsilon)\). Then
\(\exists H \subseteq \mathbb{R}\) that is open and
\(H \cap A = f^{-1}(G)\). Then notice that
\(a \in H\) since \(a \in f^{-1}(G)\) and hence
\(\exists \delta> 0\) such that
\(N(a, \delta) \subseteq H\) and hence

\begin{equation*}
\forall x \in A: x \in N(a,\delta) \implies x \in H \implies f(x) \in G = N(f(a), \varepsilon)
\end{equation*}

and we are done.

 ◻
\end{example}

\section{Uniform continuity}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---uniform-continuity}

Let \(A \subseteq \mathbb{R}\) and
\(f \colon A \to \mathbb{R}\). Then \(f\) is
\emph{uniformly continuous} if

\begin{equation*}
\forall \varepsilon > 0: \exists \delta> 0: \forall x, y \in A: |x - y| < \delta \implies |f(x) - f(y)| < \varepsilon
\end{equation*}

Note that uniform continuity implies continuity by fixing
\(y\) however, the converse of this statement is not
necessarily true.

\subsection{Compact sets}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---compact-sets}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A\to \mathbb{R}\). Then if \(A\) is
compact and \(f\) is continuous, \(f\) is uniformly
continuous on \(A\).

\begin{example}[{Proof}]
Look at Theorem 4.6 in Lay’s book.

 ◻
\end{example}

\section{Discontinuities}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---discontinuities}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A\to \mathbb{R}\) and \(a \in A\). Then,
if \(f\) is not continuous at \(a\), it is said that
\(f\) is \emph{discontinuous} at \(a\) or that
\(f\) has a \emph{discontinuity} at \(a\). Note that in
this case \(a \in A'\), as \(f\) is continuous at
all isolated points, and hence we can speak of the limit of
\(f\) as \(x \to a\).

Then, since \(a \in A'\) and \(f\) is continuous iff
\(\lim_{x \to a} f(x) = f(a)\), we classify discontinuities as
follows

\begin{itemize}
\item Removable discontinuity: \(\lim_{x \to a} f(x)\) exists but
    is not equal to \(f(a)\).
\item Essential discontinuity: \(\lim_{x\to a} f(x)\) does not
    exist. In this case, if \(a\) is approachable from both the
    left and right, there are a further two possibilities
    
    \begin{itemize}
    \item Jump discontinuity: both \(f(a-)\) and \(f(a+)\)
        exist but are not equal.
    \item Essential discontinuity?: one of \(f(a-)\) of
        \(f(a+)\) does not exist.
    \end{itemize}
\end{itemize}

\section{Monotonicity}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---monotonicity}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A\to \mathbb{R}\), then \(f\) is

\begin{enumerate}[label=\arabic*)]
\item \emph{(monotonically) increasing} if
    
    \begin{equation*}
    \forall x,y \in A: x < y \implies f(x) \leq f(y)
    \end{equation*}
    
    further, \(f\) is \emph{strictly increasing} if
    
    \begin{equation*}
    \forall x,y \in A: x < y \implies f(x) < f(y)
    \end{equation*}
\item \emph{(monotonically) decreasing} if
    
    \begin{equation*}
    \forall x,y \in A: x < y \implies f(x) \geq f(y)
    \end{equation*}
    
    further, \(f\) is \emph{strictly decreasing} if
    
    \begin{equation*}
    \forall x,y \in A: x < y \implies f(x) > f(y)
    \end{equation*}
\end{enumerate}

Furthermore, if \(f\) is either increasing or decreasing, its
called a \emph{monotonic (or monotone)} function and if \(f\) is
either strictly increasing or strictly decreasing, its called a
\emph{strictly monotonic (or strictly monotone)} function.

\subsection{Limits at interior points}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---limits-at-interior-points}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A\to \mathbb{R}\) be a monotonic function and
\(a \in \mathbb{R}\). Then if
\(a \in (A\cap (-\infty, a))'\), \(f(a+)\) exists
and if \(a \cap (A \cap (a, \infty))\) and \(f(a+)\)
exists. Specifically

\begin{itemize}
\item If \(f\) is increasing,
    
    \begin{equation*}
    f(a-) = \sup\{f(x) \in \mathbb{R}: x \in A \text{ and } x < a\} = \sup(f(A \cap (-\infty, a)))
    \end{equation*}
    
    and
    
    \begin{equation*}
    f(a+) = \inf\{f(x) \in \mathbb{R}: x \in A \text{ and } x > a\} = \inf(f(A \cap (a, \infty)))
    \end{equation*}
\item If \(f\) is decreasing,
    
    \begin{equation*}
    f(a-) = \inf\{f(x) \in \mathbb{R}: x \in A \text{ and } x < a\} = \inf(f(A \cap (-\infty, a)))
    \end{equation*}
    
    and
    
    \begin{equation*}
    f(a+) = \sup\{f(x) \in \mathbb{R}: x \in A \text{ and } x > a\} = \sup(f(A \cap (a, \infty)))
    \end{equation*}
\end{itemize}

This immediately implies that if \(f\) is discontinuous at
\(a \in A\), it must a jump discontinuity and \(a\)
must be approachable from both the left and right.

\begin{example}[{Proof}]
We will only prove the result when
\(a \in (A\cap (-\infty, a))'\) and \(f\) is
increasing as the other cases are quite similar. Also, note that this
proof assumes that \(\sup(f(A \cap (-\infty, a)))\) or more
specifically, \(f(A\cap (-\infty, a))\) has an upper bound
which can be confirmed if
\((A \cap [a,\infty)) \neq \varnothing\).

Firstly, let \(u = \sup(f(A \cap (-\infty, a)))\) and consider
arbitrary \(\varepsilon > 0\), then by the characterization of
supremum, \(\exists f(x_1) \in f(A \cap (-\infty, a))\) such
that \(f(x) > u - \varepsilon\). We then let
\(\delta = a - x_1 > 0\) since
\(x_1 \in (-\infty, a)\). Now, by the monotonicity of
\(f\) and since \(u\) is an upper bound for
\(f(A \cap (-\infty, a))\),

\begin{equation*}
\begin{aligned}
        \forall x \in (A \cap (-\infty, a)):
        x > x_1
        &\implies u \geq f(x) \geq f(x_1) > u - \varepsilon\\
        &\implies 0 \geq f(x) -u > - \varepsilon\\
        &\implies |f(x) - u| < \varepsilon
    \end{aligned}
\end{equation*}

Furthermore,

\begin{equation*}
\forall x \in A: -\delta = x_1 - a < x-a < 0 \implies |f(x) - u| < \varepsilon
\end{equation*}

and we are done since the choice of \(\varepsilon\) was
arbitrary.

 ◻
\end{example}

\subsection{Countability of discontinuities}
\label{develop--math2277:ROOT:page--limits-and-continuity.adoc---countability-of-discontinuities}

Let \(A \subseteq \mathbb{R}\) and
\(f\colon A\to \mathbb{R}\) be a monotonic function. Then,
\(f\) has countably many discontinuities.

\begin{example}[{Proof}]
We will prove that the number of discontinuities in
\((a, b) \cap A\) is countable and it would follow that the
total number of discontinuities in \(A\) is countable since
\(A\) can be written as the union of countably many such sets
\((a, b) \cap A\).

 ◻
\end{example}
\end{document}
