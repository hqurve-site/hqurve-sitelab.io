\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Fourier Series}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Basic Ideas}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---basic-ideas}

Ideas required for the rest of this section

\subsection{Periodic}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---periodic}

A function \(f\) of a single variable \(x\) is \emph{periodic} if \(\exists T \neq 0\)
such that

\begin{equation*}
f(x+T) = f(x)
\end{equation*}

In such a case, \(T\) is called the \emph{period} of \(f(x)\).

Also, note that this immediately implies that all integer multiples of \(T\)
are also periods of \(f\).

\subsection{Piecewise continuous}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---piecewise-continuous}

A function is \emph{piecewise continuous} if it is continuous on all but a finite
number of points where, at these points, the left and right hand limits
both exist but may be distinct.

\subsection{Integral Properties}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---integral-properties}

If \(f\) is a periodic function with period \(T\),

\begin{equation*}
\int_{\alpha}^\beta f(x) \ dx = \int_{\alpha + T}^{\beta + T} f(x) \ dx
\end{equation*}

and

\begin{equation*}
\int_0^T f(x) \ dx = \int_\alpha^{\alpha + T} f(x) \ dx
\end{equation*}

\subsection{Some integral results}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---some-integral-results}

Let, \(m, n \in \mathbb{Z}\) then,

\begin{equation*}
\int_{c - \pi}^{c + \pi} \sin(nx) \ dx
= \int_{c - \pi}^{c + \pi} \cos(nx) \ dx
= 0
\end{equation*}

\begin{equation*}
\int_{c - \pi}^{c + \pi} \sin^2(nx) \ dx
= \int_{c - \pi}^{c + \pi} \cos^2(nx) \ dx
= \pi
\end{equation*}

\begin{equation*}
\int_{c - \pi}^{c + \pi} \sin(nx)\cos(mx) \ dx
= 0
\end{equation*}

\begin{equation*}
\int_{c - \pi}^{c + \pi} \cos(mx)\cos(nx) \ dx
=\int_{c - \pi}^{c + \pi} \sin(mx)\sin(nx) \ dx
= 0
\quad \text{ if $m\neq n$}
\end{equation*}

\begin{example}[{Proofs}]
\begin{example}[{Pair 1}]
\begin{equation*}
\begin{aligned}
\int_{c - \pi}^{c + \pi} \sin(nx) \ dx
&= \left.\frac{-1}{n}\cos(nx)\right|_{c-\pi}^{c+\pi}
\\&= \frac{-1}{n}\left[\cos(nc+n\pi) - \cos(nc-n\pi)\right]
\\&= \frac{-1}{n}\left[\cos(nc+n\pi) - \cos(nc-n\pi + 2n\pi)\right]
\\&= 0
\end{aligned}
\end{equation*}

\begin{equation*}
\begin{aligned}
\int_{c - \pi}^{c + \pi} \cos(nx) \ dx
&= \left.\frac{1}{n}\sin(nx)\right|_{c-\pi}^{c+\pi}
\\&= \frac{1}{n}\left[\sin(nc+n\pi) - \sin(nc-n\pi)\right]
\\&= \frac{1}{n}\left[\sin(nc+n\pi) - \sin(nc-n\pi + 2n\pi)\right]
\\&= 0
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Pair 2}]
\begin{equation*}
\begin{aligned}
\int_{c - \pi}^{c + \pi} \sin^2(nx) \ dx
&= \int_{c - \pi}^{c + \pi} \frac{1-\cos(2nx)}{2} \ dx
\\&= \left. \frac{x-\frac{1}{2n}\sin(2nx)}{2} \right|_{c - \pi}^{c + \pi}
\\&=
   \frac{(c+ \pi)-\frac{1}{2n}\sin(2nc + 2n\pi)}{2}
   - \frac{(c- \pi)-\frac{1}{2n}\sin(2nc - 2n\pi)}{2}
\\&=
   \frac{(c+ \pi)-\frac{1}{2n}\sin(2nc)}{2}
   - \frac{(c- \pi)-\frac{1}{2n}\sin(2nc)}{2}
\\&= \pi
\end{aligned}
\end{equation*}

And, by using the above result,

\begin{equation*}
\begin{aligned}
\int_{c - \pi}^{c + \pi} \cos^2(nx) \ dx
&= \int_{c - \pi}^{c + \pi} 1-\sin^2(nx) \ dx
\\&= (2\pi) - \pi
\\&= \pi
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Result 3}]
\begin{equation*}
\begin{aligned}
I
&=\int_{c - \pi}^{c + \pi} \sin(nx)\cos(mx) \ dx
\\&= \int_{c - \pi}^{c + \pi} \frac{1}{m}\sin(nx)\frac{d}{dx}\sin(mx) \ dx
\\&= \left.\frac{1}{m}\sin(nx)\sin(mx)\right|_{c - \pi}^{c + \pi} - \frac{n}{m}\int_{c - \pi}^{c + \pi}\cos(nx)\sin(mx) \ dx
\\&= \frac{1}{n}\sin(nc+n\pi)\sin(mc+m\pi) - \frac{1}{n}\sin(nc - n\pi)\sin(mc-m\pi)
\\&\hspace{5em}- \frac{n}{m}\int_{c - \pi}^{c + \pi}\cos(nx)\sin(mx) \ dx
\\&= \frac{1}{n}\sin(nc+n\pi)\sin(mc+m\pi) - \frac{1}{n}\sin(nc + n\pi)\sin(mc+m\pi)
\\&\hspace{5em}- \frac{n}{m}\int_{c - \pi}^{c + \pi}\cos(nx)\sin(mx) \ dx
\\&= - \frac{n}{m}\int_{c - \pi}^{c + \pi}\cos(nx)\sin(mx) \ dx
\\&= \frac{n}{m^2}\int_{c - \pi}^{c + \pi}\cos(nx)\frac{d}{dx}\cos(mx) \ dx
\\&= \left.\frac{n}{m^2}\cos(nx)\cos(mx) \right|_{c - \pi}^{c + \pi} + \frac{n^2}{m^2}\int_{c-\pi}^{c+pi} \sin(nx)\cos(mx)\ dx
\\&= \frac{n}{m^2}\cos(nc +n\pi)\cos(mc + m\pi) - \frac{n}{m^2}\cos(nc -n\pi)\cos(mc - m\pi)
\\&\hspace{6em}+ \frac{n^2}{m^2}\int_{c-\pi}^{c+pi} \sin(nx)\cos(mx)\ dx
\\&= \frac{n}{m^2}\cos(nc +n\pi)\cos(mc + m\pi) + \frac{n}{m^2}\cos(nc -n\pi)\cos(mc + m\pi)+ \frac{n^2}{m^2} I
\\&= \frac{n^2}{m^2} I
\end{aligned}
\end{equation*}

If \(m\neq n\) we get that \(I =0\) immediately. Otherwise if \(m =n\), notice that
after integrating by parts once, we get that \(I = -I\) and hence \(I = 0\).
\end{example}

\begin{example}[{Pair 4}]
Let

\begin{equation*}
I = \int_{c - \pi}^{c + \pi} \cos(mx)\cos(nx) \ dx\\
J = \int_{c - \pi}^{c + \pi} \sin(mx)\sin(nx) \ dx
\end{equation*}

Then,

\begin{equation*}
\begin{aligned}
I &= \int_{c - \pi}^{c + \pi} \cos(mx)\cos(nx) \ dx
\\&= \int_{c - \pi}^{c + \pi} \frac{1}{n}\cos(mx)\frac{d}{dx}\sin(nx) \ dx
\\&= \left.\frac{1}{n}\cos(mx)\sin(nx)\right|_{c - \pi}^{c + \pi}
    - \int_{c - \pi}^{c + \pi} \frac{-m}{n}\sin(mx)\sin(nx) \ dx
\\&= \frac{1}{n}\cos(mc + m\pi)\sin(nc +n\pi)
    - \frac{1}{n}\cos(mc - m\pi)\sin(nc -n\pi)
    + \frac{m}{n} J
\\&= \frac{1}{n}\cos(mc + m\pi)\sin(nc +n\pi)
    - \frac{1}{n}\cos(mc - m\pi + 2m\pi)\sin(nc -n\pi + 2m\pi)
    + \frac{m}{n} J
\\& = \frac{m}{n}J
\end{aligned}
\end{equation*}

Similarly

\begin{equation*}
\begin{aligned}
J &= \int_{c - \pi}^{c + \pi} \sin(mx)\sin(nx) \ dx
\\&= \int_{c - \pi}^{c + \pi} \frac{-1}{n}\sin(mx)\frac{d}{dx}\cos(nx) \ dx
\\&= \left.\frac{-1}{n}\sin(mx)\cos(nx)\right|_{c - \pi}^{c + \pi}
    - \int_{c - \pi}^{c + \pi} \frac{-m}{n}\cos(mx)\cos(nx) \ dx
\\&= \frac{-1}{n}\sin(mc + m\pi)\cos(nc +n\pi)
    - \frac{-1}{n}\sin(mc - m\pi)\cos(nc -n\pi)
    + \frac{m}{n} I
\\&= \frac{1}{n}\sin(mc + m\pi)\cos(nc +n\pi)
    - \frac{1}{n}\sin(mc - m\pi + 2m\pi)\cos(nc -n\pi + 2m\pi)
    + \frac{m}{n} I
\\& = \frac{m}{n}I
\end{aligned}
\end{equation*}

Therefore, we get that \(I = \left(\frac{m}{n}\right)^2 I\)
and since \(m\neq n\), \(I = J = 0\).
\end{example}
\end{example}

\section{Forier series}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---forier-series}

Consider a function, \(f\) of a single variable \(x\) with period \(2\pi\)
which is expressed as a series (at each continuous point) in the form

\begin{equation*}
\frac{a_0}{2} + \sum_{n=1}^\infty\left[ a_n \cos(nx) + b_n \sin(nx)\right]
\end{equation*}

This series is called a (trigonometric) \emph{Fourier series} and \(a_0\), \(a_n\)
and \(b_n\) are called \emph{Fourier coefficients}

\begin{admonition-note}[{}]
Different sources define the constant term differently.
We will be using \(\frac{a_0}{2}\) as the constant term.
\end{admonition-note}

\begin{admonition-caution}[{}]
The above expression is not always equal to \(f\) and hence should not be equated.
\end{admonition-caution}

\subsection{Dirichlet's Conditions}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---dirichlets-conditions}

A function, \(f\), can only be expressed as a Fourier series, once it satisfies
the following conditions

\begin{enumerate}[label=\arabic*)]
\item \(f\) is defined and single valued
\item \(f\) is piecewise continuous within a periodic interval
\item \(f'\)  is peiecewise continuous within a periodic interval
\end{enumerate}

\begin{admonition-note}[{}]
These conditions are sufficient but not necessary
\end{admonition-note}

\subsection{Euler's Formula}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---eulers-formula}

Let \(f\) be a function which satisfies Dirichlet's Condtions
and \(c\) be a real constant, then the fourier coeffiecents of \(f\) are given by

\begin{equation*}
a_0 = \frac1\pi \int_{c-\pi}^{c+\pi} f(x) \ dx
\end{equation*}

\begin{equation*}
a_n = \frac1\pi \int_{c-\pi}^{c+\pi} f(x)\cos(nx) \ dx
\end{equation*}

\begin{equation*}
b_n = \frac1\pi \int_{c-\pi}^{c+\pi} f(x)\sin(nx) \ dx
\end{equation*}

\begin{admonition-tip}[{}]
The constant \(c\) may take any value however, it is usually chosen such that
there are either no disconinuities within the interval \((c-\pi, c+\pi)\) is 0, or
the function \(f\) is "simplest".
\end{admonition-tip}

\begin{example}[{Derivation}]
\begin{admonition-caution}[{}]
This is merely a derivation but not a proof because ... this shows that the coefficients
must be this but doesn't show that these coefficients work. (Though I guess since \(f\)
satisfies Dirichlet's conditions, maybe it can be considered a proof).
\end{admonition-caution}

\begin{admonition-note}[{}]
Throughout this derivation, results from \myautoref[{above}]{develop--math3402:ROOT:page--fourier-series/index.adoc---some-integral-results} are used.
\end{admonition-note}

Firstly, we integrate \(f\) between the bounds \((c+\pi, c-\pi)\) to get

\begin{equation*}
\begin{aligned}
\int_{c-\pi}^{c+\pi} f(x) \ dx
&= \frac{a_0}{2} \int_{c - \pi}^{c + \pi} dx
    + \sum_{n=1}^\infty \left[
        a_n \int_{c-\pi}^{c+\pi} \cos(nx) \ dx
        + b_n\int_{c-\pi}^{c+\pi} \sin(nx) \ dx
    \right]
\\&= a_0 \pi + \sum_{n=1}^\infty [ 0 + 0 ]
\\&= a_0 \pi
\end{aligned}
\end{equation*}

Hence, \(a_0 = \frac1\pi \int_{c-\pi}^{c+\pi} f(x) \ dx\).

Next, we multiply \(f\) by \(\cos(mx)\) for \(m \in \mathbb{Z}^+\)
and integrate over the same bounds

\begin{equation*}
\begin{aligned}
\int_{c-\pi}^{c+\pi}& f(x) \cos(mx) \ dx
\\&= \frac{a_0}{2} \int_{c - \pi}^{c + \pi} \cos(mx) dx
    \\&\hspace{3em}+ \sum_{n=1}^\infty \left[
        a_n \int_{c-\pi}^{c+\pi} \cos(nx)\cos(mx) \ dx
        + b_n\int_{c-\pi}^{c+\pi} \sin(nx)\cos(mx) \ dx
    \right]
\\&= 0 + \sum_{n=1}^\infty \left[ a_n \int_{c-\pi}^{c+\pi} \cos(nx)\cos(mx) \ dx + 0 \right]
\\&= 0 +  a_m \int_{c-\pi}^{c+\pi} \cos^2(mx) \ dx + 0
\\&= a_m \pi
\end{aligned}
\end{equation*}

Hence, \(a_m = \frac1\pi \int_{c-\pi}^{c+\pi} f(x)\cos(mx) \ dx\).

Finally, we multiply \(f\) by \(\sin(mx)\) instead

\begin{equation*}
\begin{aligned}
\int_{c-\pi}^{c+\pi}& f(x) \sin(mx) \ dx
\\&= \frac{a_0}{2} \int_{c - \pi}^{c + \pi} \sin(mx) dx
    \\&\hspace{3em}+ \sum_{n=1}^\infty \left[
        a_n \int_{c-\pi}^{c+\pi} \cos(nx)\sin(mx) \ dx
        + b_n\int_{c-\pi}^{c+\pi} \sin(nx)\sin(mx) \ dx
    \right]
\\&= 0 + \sum_{n=1}^\infty \left[ 0 + b_n \int_{c-\pi}^{c+\pi} \sin(nx)\sin(mx) \ dx \right]
\\&= 0 +  b_m \int_{c-\pi}^{c+\pi} \sin^2(mx) \ dx + 0
\\&= b_m \pi
\end{aligned}
\end{equation*}

Hence, \(b_m = \frac1\pi \int_{c-\pi}^{c+\pi} f(x)\sin(mx) \ dx\).
\end{example}

\subsection{Value of Fourier Series}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---value-of-fourier-series}

Let \(f\) be a periodic piecewise continuous function on interval \(I\)
with left and right derivatives at each point on the interval. Then at each point
\(x_0\), if \(f\) is

\begin{itemize}
\item continuous, the fourier series evaluated at \(x_0\) is equal to \(f(x_0)\)
\item otherwise, the fourier series assumes the average of the left and right hand limits of \(f\) at \(x_0\).
\end{itemize}

\begin{admonition-note}[{}]
The second case is always true since when \(f\) is continuous, the average is simple the function at
\(f\).
\end{admonition-note}

\subsection{Fourier Series of even and odd funtions}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---fourier-series-of-even-and-odd-funtions}

Let \(f\) be a function with period \(2\pi\) then the followin table holds

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(f\) is even/odd} & {name} & {\(a_0\)} & {\(a_n\)} & {\(b_n\)} \\
\hline[\thicktableline]
{even} & {Fourier cosine series} & {\(\frac{2}{\pi}\int_0^\pi f(x)\ dx\)} & {\(\frac{2}{\pi}\int_0^\pi f(x)\cos(nx)\ dx\)} & {\(0\)} \\
\hline
{odd} & {Fourier sine series} & {\(0\)} & {\(0\)} & {\(\frac{2}{\pi}\int_0^\pi f(x)\sin(nx)\ dx\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\subsection{Half range Series}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---half-range-series}

Consider function \(f\) of period \(2\pi\) only defined over
half of its period. In such a case, we need to make an assumption
about the other half of the function. We either

\begin{itemize}
\item Assume \(f\) is even
\item Assume \(f\) is odd
\item other
\end{itemize}

Regardless of what assumtion we make, the fourier series representation
of \(f\) is only valid on the period in which \(f\)
is defined.

\section{Smooth}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---smooth}

We say a function \(f\) is \emph{smooth} if both \(f\) and \(f'\)
are continuous. That is, a function is smooth if it is continuously differentiable

\begin{admonition-caution}[{}]
In other courses, \emph{smooth} means that a function is infinitly differentiable.
\end{admonition-caution}

Additionally, we say that \(f\) is \emph{piecewise smooth}
if it can be made up of a finite number of smooth functions, each separated
by jump disconinuities.

\subsection{Theorem: Piecewise smooth functions have continuous fourier series}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---theorem-piecewise-smooth-functions-have-continuous-fourier-series}

If \(f\) is piecewise smooth, then its fourier series is continuous

\subsection{Theorem: Differentiating fourier series of function with piecewise smooth derivative}
\label{develop--math3402:ROOT:page--fourier-series/index.adoc---theorem-differentiating-fourier-series-of-function-with-piecewise-smooth-derivative}

If \(f\) has piecewise smooth derivative \(f'\), then the fourier series
of \(f'\) is simply given by the term by term differential of the fourier series of \(f\).
That is, the fourier series of \(f'\) is given by

\begin{equation*}
\sum_{n=1}^\infty [-na_n \sin(nx) + nb_n \cos(nx)]
\end{equation*}

where \(a_n\) and \(b_n\) are the fourier coefficients of \(f\).
\end{document}
