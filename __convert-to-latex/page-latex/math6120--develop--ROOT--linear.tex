\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Linear differential equations of higher order}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Consider the linear differential equation of order \(n\)

\begin{equation*}
a_0(t)x^{(n)} + a_1(t)x^{(n-1)} + \cdots a_{n-1}(t)x^{(1)} + a_n(t)x = g(t)
\end{equation*}

with \(a_0(t) \neq 0\). We say that the above is in \emph{standard form} of \(a_0(t) = 1\).
A linear differential equation of order \(n\) requires \(n\) initial conditions
for it to be well posed

\begin{equation*}
x(t_0) = \alpha_0, \quad x'(t_0) = \alpha_1, \quad\ldots\quad x^{(n-1)} = \alpha_{n-1}
\end{equation*}

We may equivalently write the above equation using the \emph{differential operator}
\(D\) with

\begin{equation*}
D = \frac{d}{dt}
\quad\text{and}\quad
D^{n}x = \frac{d}{dt} (D^{n-1}x)
\end{equation*}

Then, \(D\) is a linear operator and we may rewrite the differential equation
as follows

\begin{equation*}
\begin{aligned}
&L = a_0(t)D^n + a_1(t)D^{n-1} + \cdots a_{n-1}(t)D + a_n(t)
\\&\implies L(x)(t) \equiv a_0(t)x^{(n)} + a_1(t)x^{(n-1)} + \cdots a_{n-1}(t)x^{(1)} + a_n(t)x
\end{aligned}
\end{equation*}

By applying the technique \myautoref[{to convert higher order IVPs to a system of differential equations}]{develop--math6120:ROOT:page--systems-of-solutions.adoc---higher-order-differential-equations},
we obtain the following theorem.

\begin{theorem}[{Existence and uniquness}]
If the functions \(a_1(t), \ldots a_n(t)\) and \(g(t)\) are continuous on the closed interval
\(I\) containing \(t_0\).
Then the following IVP has a unique solution

\begin{equation*}
\begin{aligned}
&x^{(n)} + a_1(t)x^{(n-1)} + \cdots a_{n-1}(t)x^{(1)} + a_n(t)x = g(t)
\\& x(t_0) = \alpha_0, \quad x'(t_0) = \alpha_1, \quad\ldots\quad x^{(n-1)} = \alpha_{n-1}
\end{aligned}
\end{equation*}

\begin{proof}[{}]
Define \(\vec{x}=(x_1, x_2, \ldots x_n)\) as done in \myautoref[{}]{develop--math6120:ROOT:page--systems-of-solutions.adoc---higher-order-differential-equations}
and \(\vec{\alpha}= (\alpha_0, \alpha_1, \ldots \alpha_{n-1})\). Then the following system of differential
equations has the same solutions as the IVP

\begin{equation*}
\vec{x}' = \vec{f}(t, \vec{x})
,\quad \vec{x}(t_0) = \vec{\alpha}
\end{equation*}

where

\begin{equation*}
\begin{aligned}
f_1(t, \vec{x}) &= x_2\\
f_2(t, \vec{x}) &= x_3\\
&\vdots\\
f_n(t, \vec{x}) &= g(t) - a_1(t)x_n - a_2(t)x_{n-1} - \cdots - a_n(t)x_1\\
\end{aligned}
\end{equation*}

Now, we show that \(\vec{f}\) is Lipschitz.
Notice that for each \(x_i\)

\begin{equation*}
\|\vec{f}_{x_{i}}(t, \vec{x}) \| = \begin{cases}
|a_n(1)| &\quad\text{if } i=1\\
\max(1, |a_{n+1-i}(t)|) &\quad\text{if } i=2, \ldots n\\
\end{cases}
\end{equation*}

Since the \(a_i(t)\) are bounded, we have that \(\|\vec{f}_{x_{i}}(t, \vec{x}) \|\)
is bounded for each choice of \(x_i\) and hence \(\vec{f}\) is Lipschitz.

Therefore, the IVP has a unique solution on \(I\).
\end{proof}
\end{theorem}

\begin{theorem}[{Superposition principle / Fundamental theorem for the Homogeneous Linear ODE}]
If \(x_1\) and \(x_2\) are solutions to the following homogeneous linear ODE,

\begin{equation*}
L(x)(t) = 0
\end{equation*}

then \(c_1x_1 + c_2x_2\) is also a solution to the above ODE for any constants
\(c_1\) and \(c_2\).

This follows from the fact that \(L\) is a linear operator and the kernel
of a linear operator is a subspace.
\end{theorem}

\section{Linear dependence and Wronskian}
\label{develop--math6120:ROOT:page--linear.adoc---linear-dependence-and-wronskian}

We say that functions \(x_1, \ldots x_n: I \to \mathbb{R}\) are \emph{linearly dependent}
if there exists non-trivial constants \((c_1, c_2,\ldots c_n)\) such that

\begin{equation*}
\forall t \in I: c_1x_1(t) + c_2x_2(t) + \cdots + c_nx_n(t) = 0
\end{equation*}

A set of functions which are not linearly dependent are called \emph{linearly independent}.

\begin{admonition-note}[{}]
This is the same definition of linear dependence when considering the vector
space of functions \(I\to \mathbb{R}\) under the usual operations.
\end{admonition-note}

We additionally define the \emph{Wronksian} of \((n-1)\) times differentiable functions
\(x_1, \ldots x_n: I \to \mathbb{R}\) to be

\begin{equation*}
W(x_1, x_2, \ldots x_n)(t) = \begin{vmatrix}
x_1(t) & x_2(t) & \cdots & x_n(t)\\
x_1'(t) & x_2'(t) & \cdots & x_n'(t)\\
\vdots & & \ddots & \vdots \\
x_1^{(n-1)}(t) & x_2^{(n-1)}(t) & \cdots & x_n^{(n-1)}(t)\\
\end{vmatrix}
\end{equation*}

If the \(x_i\) are linearly dependent, then clearly the above determinant is zero.
We state the contrapositive of this statement in the following statement

\begin{theorem}[{}]
If the Wronskian of \(x_1, \ldots x_n\) is non-zero at at least one point of
the interval \(I\), then the functions \(x_1, \ldots x_n\) are linearly independent
on \(I\).

\begin{proof}[{}]
Let \(c_1, \ldots c_n\) be constants such that the linear combination
of \(x_1, \ldots x_n\) is zero. We aim to show that each \(c_i = 0\).

We may determine the following equations for all \(t \in I\)
by differentiating the first \(n-1\) times.

\begin{equation*}
\begin{aligned}
c_1x_1(t) + c_2x_2(t) + \cdots c_nx_n(t) &= 0\\
c_1x_1'(t) + c_2x_2'(t) + \cdots c_nx_n'(t) &= 0\\
&\vdots \\
c_1x_1^{(n-1)}(t) + c_2x_2^{(n-1)}(t) + \cdots c_nx_n^{(n-1)}(t) &= 0\\
\end{aligned}
\end{equation*}

Let \(t_0 \in I\) be a point at which the Wronskian is non-zero.
Then,

\begin{equation*}
\left.\begin{aligned}
c_1x_1(t_0) + c_2x_2(t_0) + \cdots c_nx_n(t_0) &= 0\\
c_1x_1'(t_0) + c_2x_2'(t_0) + \cdots c_nx_n'(t_0) &= 0\\
&\vdots \\
c_1x_1^{(n-1)}(t_0) + c_2x_2^{(n-1)}(t_0) + \cdots c_nx_n^{(n-1)}(t_0) &= 0\\
\end{aligned}
\right\}
\quad\sim\quad
\begin{bmatrix}
x_1(t_0) & x_2(t_0) & \cdots & x_n(t_0)\\
x_1'(t_0) & x_2'(t_0) & \cdots & x_n'(t_0)\\
&\vdots \\
x_1^{(n-1)}(t_0) & x_2^{(n-1)}(t_0) & \cdots & x_n^{(n-1)}(t_0)\\
\end{bmatrix}
\begin{bmatrix}
c_1 \\ c_2 \\ \vdots \\ c_n
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0 \\ \vdots \\ 0
\end{bmatrix}
\end{equation*}

Since the determinant of the above matrix is non-zero (since it is equal to the Wronskian),
the above equation has a unique solution \(c_1=c_2=\cdots =c_n = 0\).
Therefore \(x_1, \ldots x_n\) are linearly independent.
\end{proof}
\end{theorem}

Note that the converse is not necessarily true since \(x_1(t) = t|t|\) and \(x_2(t)=t^2\)
are linearly independent on \(\mathbb{R}\) but

\begin{equation*}
W[x_1, x_2](t) = \begin{vmatrix}
t|t| & t^2\\
2|t| & 2t\\
\end{vmatrix}
= 0
\end{equation*}

However, the converse is true in the case where \(x_1, \ldots x_n\) are solutions to a homogeneous linear IVP.
We study this in the next section.

\section{General solutions}
\label{develop--math6120:ROOT:page--linear.adoc---general-solutions}

Let \(x_1, \ldots x_n\) be linear independent solutions to the following homogeneous linear ODE
with order \(n\)

\begin{equation*}
a_0(t)x^{(n)} + a_1(t)x^{(n-1)} + \cdots a_{n-1}(t)x^{(1)} + a_n(t)x = 0
\end{equation*}

where the \(a_i(t)\) are continuous and \(a_0(t) \neq 0\) in \(I\).
Then, the linear combination

\begin{equation*}
x = c_1x_1 + c_2x_2 + \cdots + c_n x_n
\end{equation*}

is called the \emph{general solution} where \(c_1, \ldots c_n\) are arbitrary constants
(usually determined by initial conditions).
Note that this definition has the implication that all solutions are of this form.
We are yet to prove this to be true.

\begin{theorem}[{}]
Let \(x_1, \ldots x_n\) be solutions of the homogeneous linear ODE with order \(n\)
and continuous coefficients.
Then \(W[x_1, \ldots x_n](t) \neq 0\) on \(I\)
iff the \(x_1, \ldots x_n\) are linearly independent.

\begin{proof}[{}]
First suppose that \(W[x_1, \ldots x_n](t) \neq 0\) on \(I\) then, from
a previous theorem, we know that \(x_1, \ldots x_n\) are linearly independent.

Conversely suppose that there exists a point \(t_0 \in I\) such that
\(W[x_1, \ldots x_n](t_0) = 0\).
Then, there exists non-trivial \(c_1, \ldots c_n\) such that

\begin{equation*}
\begin{aligned}
c_1x_1(t_0) + c_2x_2(t_0) + \cdots c_nx_n(t_0) &= 0\\
c_1x_1'(t_0) + c_2x_2'(t_0) + \cdots c_nx_n'(t_0) &= 0\\
&\vdots \\
c_1x_1^{(n-1)}(t_0) + c_2x_2^{(n-1)}(t_0) + \cdots c_nx_n^{(n-1)}(t_0) &= 0\\
\end{aligned}
\end{equation*}

Define \(x = c_1x_1 + c_2x_2 + \cdots c_nx_n\). Then,

\begin{equation*}
x(t_0) = x'(t_0) = \cdots = x^{(n-1)}(t_0) = 0
\end{equation*}

Therefore \(x\) is a solution to an linear IVP with continuous coefficients.
However, such an IVP has a unique solution.
Since the zero solution also satisfies the IVP, \(x(t)= 0\) on \(I\)
and hence \(x_1, \ldots x_n\) are linearly independent.
\end{proof}
\end{theorem}

\begin{corollary}[{}]
Let \(x_1, \ldots x_n\) be solutions of the homogeneous linear ODE with order \(n\)
and continuous coefficients.
Then either

\begin{itemize}
\item \(W[x_1, \ldots x_n](t) \neq 0\) on \(I\)
\item \(W[x_1, \ldots x_n](t) = 0\) on \(I\)
\end{itemize}
\end{corollary}

\begin{theorem}[{Form of solutions}]
Let \(x_1, \ldots x_n\) be linearly independent solutions of the
homogeneous linear ODE with order \(n\)
and continuous coefficients.
Then, any solution \(x\) of this ODE has the form

\begin{equation*}
x = c_1x_1 + \cdots c_2x_2 + \cdots c_nx_n
\end{equation*}

for some constants \(c_1, \ldots c_n\).
Therefore \(x_1, \ldots x_n\) form a basis for the solution set of the ODE (ie the integral curves).

\begin{proof}[{}]
By the superposition principle, we already know that the span of \(x_1, \ldots x_n\)
is a subset of the solution set. We still need to prove that all solutions
lie in this span.

Now, consider arbitrary solution \(x(t)\). Then, define

\begin{equation*}
x(t_0)=\alpha_1, \ x(t_0) = \alpha_2, \ \cdots \ x^{(n-1)}(t_0) = \alpha_n
\end{equation*}

for some fixed \(t_0 \in I\).
Consider the following equation

\begin{equation*}
\begin{bmatrix}
x_1(t_0) & x_2(t_0) & \cdots & x_n(t_0)\\
x_1'(t_0) & x_2'(t_0) & \cdots & x_n'(t_0)\\
&\vdots \\
x_1^{(n-1)}(t_0) & x_2^{(n-1)}(t_0) & \cdots & x_n^{(n-1)}(t_0)\\
\end{bmatrix}
\begin{bmatrix}
c_1 \\ c_2 \\ \vdots \\ c_n
\end{bmatrix}
=
\begin{bmatrix}
\alpha_1 \\ \alpha_2 \\ \vdots \\ \alpha_n
\end{bmatrix}
\end{equation*}

Since \(W[x_1, \ldots x_n](t_0)\neq 0\), the above has a unique solution
for \(c_1, \ldots c_n\). Define

\begin{equation*}
y(t) = c_1x_1(t) + c_2x(t) + \cdots c_nx_n(t)
\end{equation*}

Then, \(y\) satisfies the homogeneous linear ODE with initial conditions

\begin{equation*}
y(t_0)=\alpha_1, \ y(t_0) = \alpha_2, \ \cdots \ y^{(n-1)}(t_0) = \alpha_n
\end{equation*}

However, since this IVP has a unique solution, we have that \(x=y\) as desired.
\end{proof}
\end{theorem}

\subsection{Non-homogenous}
\label{develop--math6120:ROOT:page--linear.adoc---non-homogenous}

Consider the non-homogenous linear ODE

\begin{equation*}
a_0(t)x^{(n)} + a_1(t)x^{(n-1)} + \cdots a_{n-1}(t)x^{(1)} + a_n(t)x = g(t)
\end{equation*}

where the \(a_i(t)\) and \(g(t)\) are continuous, and \(a_0(t) \neq 0\) in \(I\).
Then, we can determine the general solution to this ODE using the following theorem

\begin{theorem}[{Form of non-homogenous solutions}]
Let \(x_p\) be a solution to the non-homogenous ODE
and \(x_1, \ldots x_n\) be linearly independent solutions to the homogeneous version
of this ODE.
Then \(x\) is a solution to the non-homogenous ODE iff
there exists constants \(c_1, \ldots c_n\) such that

\begin{equation*}
x = c_1x_1 + c_2x_2 + \cdots c_nx_n + x_p
\end{equation*}

\begin{proof}[{}]
It is not hard to see that any \(x\) of this form is a solution.

So now, suppose that \(x\) is a solution.
Then, \(x-x_p\) is a solution to the homogeneous ODE and hence
there exists \(c_1, \ldots c_n\) such that

\begin{equation*}
x-x_p = c_1x_1 + c_2x_2 + \cdots c_nx_n
\end{equation*}

By rearranging, we get the desired result.
\end{proof}
\end{theorem}

We call \(x_h = c_1x_1 + c_2x_2 + \cdots c_nx_n\) the \emph{homogeneous} or \emph{complementary solution}
and we call \(x_p\) a \emph{particular solution} or \emph{particular integral}.

\section{Two useful formulae}
\label{develop--math6120:ROOT:page--linear.adoc---two-useful-formulae}

\begin{theorem}[{}]
Consider the equation

\begin{equation*}
L(x) = a_0(t)x'' + a_1(t)x' + a_2(t)x = 0
\end{equation*}

where \(a_0, a_1, a_2\) are continuous on \(I\) and \(a_0(t) \neq 0\) on \(I\).

If \(u, v\) are twice differentiable functions on \(I\), then

\begin{equation*}
uL(v)-vL(u) = a_0\frac{d}{dt}W[u,v] +a_1(t)W[u,v]
\end{equation*}

In particular if \(L(u) = L(v) = 0\), then

\begin{equation*}
a_0(t)\frac{d}{dt}W[u,v] +a_1(t)W[u,v] = 0
\end{equation*}

\begin{proof}[{}]
Just substitute and you get the result
\end{proof}
\end{theorem}

\begin{theorem}[{Abel's Formula}]
If \(u,v\) are solutions of \(L(x) = 0\) defined in the previous theorem,
then

\begin{equation*}
W[u,v] = k\exp\left[-\int\frac{a_1(t)}{a_0(t)} \ dt\right]
\end{equation*}

where \(k\) is a constant.

\begin{proof}[{}]
Just solve the linear first order ODE.
\end{proof}
\end{theorem}

The above two theorems may be used to solve non-homogenous second order ODEs using
only two independent solutions of the homogeneous ODE.

Suppose that \(x_1\) and \(x_2\) are two linearly independent solutions
of the homogeneous ode and \(x\) be a solution of the non-homogenous ode with initial
conditions \(x(t_0) = x'(t_0) = 0\).
Then, from the first theorem,

\begin{equation*}
\frac{d}{dt}W[x_1, x] + \frac{a_1(t)}{a_0(t)}W[x_1, x] = \frac{1}{a_0(t)}[x_1L(x) - xL(x_1)] = x_1\frac{g(t)}{a_0(t)}
\end{equation*}

We may solve for \(W[x_1, x]\) since it is a linear first order ODE as follows

\begin{equation*}
\begin{aligned}
W[x_1, x]
&= W[x_1, x] - W[x_1,x](t_0)
\\&= \exp\left[-\int_{t_0}^t\frac{a_1(s)}{a_0(s)}\ ds\right]
\int_{t_0}^t \frac{x_1(s) g(s)}{a_0(s)} \exp\left[\int_{t_0}^s\frac{a_1(u)}{a_0(u)}\ du\right] \ ds
\\&= W[x_1, x_2] \int_{t_0}^t \frac{x_1(s) g(s)}{a_0(s) W[x_1, x_2](s)} \ ds
\end{aligned}
\end{equation*}

where the last step uses Abel's formula.
Likewise we determine that

\begin{equation*}
W[x_2, x]
= W[x_1, x_2] \int_{t_0}^t \frac{x_2(s) g(s)}{a_0(s) W[x_1, x_2](s)} \ ds
\end{equation*}

By subtracting both, we get that

\begin{equation*}
\begin{aligned}
x(t)W[x_1, x_2]
&= x(x_1x_2' - x_2x_1')
\\&= x_2 (xx_1' - x'x_1) - x_1(xx_2'-x'x_2)
\\&= x_2(t)W[x_1, x] - x_1(t)W[x_2, x]
\\&= W[x_1, x_2] \int_{t_0}^t \frac{[x_2(t)x_1(s) - x_1(t)x_2(s)] g(s)}{a_0(s) W[x_1, x_2](s)} \ ds
\end{aligned}
\end{equation*}

and hence

\begin{equation*}
x(t) = \int_{t_0}^t \frac{[x_2(t)x_1(s) - x_1(t)x_2(s)] g(s)}{a_0(s) W[x_1, x_2](s)} \ ds
\end{equation*}

\begin{admonition-caution}[{}]
There is \(x_2(t)\) and \(x_1(t)\) inside the integral.
\end{admonition-caution}

\section{Techniques for solving}
\label{develop--math6120:ROOT:page--linear.adoc---techniques-for-solving}

\subsection{Variation of parameters}
\label{develop--math6120:ROOT:page--linear.adoc---variation-of-parameters}

Variation of parameters allows us to determine a particular solution if we already
know the complementary solution.

Let \(x_1, \ldots x_n\) be linearly independent solutions of the homogeneous ODE.
Then, the particular equation is given by

\begin{equation*}
x_p(t) = c_1(t)x_1(t) + c_2(t)x_2(t) + \cdots c_n(t)x_n(t)
\end{equation*}

where \(c_1, \ldots c_n\) are functions with the added condition that

\begin{equation*}
\sum_{i=1}^n c_i'(t)x_i^{(k)}(t) = 0
\end{equation*}

for each \(k = 0, \ldots n-2\).
These added conditions allow us to determine that

\begin{equation*}
x_p^{(k)}(t) = \sum_{i=1}^n c_i(t)x_i^{(k)}(t)
\end{equation*}

for each \(k=0,\ldots n-1\).

By substituting the choice for \(x_p\) into the \(n\)'th order linear ODE, we obtain that

\begin{equation*}
\begin{aligned}
g(t)
&= \sum_{k=0}^n a_{n-k}(t)x_p^{(k)}
\\&= a_0(t)x_p^{(n)}(t) + \sum_{k=0}^{n-1} a_{n-k}(t)x_p^{(k)}
\\&= a_0(t)x_p^{(n)}(t) + \sum_{k=0}^{n-1} \sum_{i=1}^n a_{n-k}(t)c_i(t)x_i^{(k)}(t)
\\&= a_0(t)x_p^{(n)}(t) +  \sum_{i=1}^n c_i(t)\sum_{k=0}^{n-1} a_{n-k}(t)x_i^{(k)}(t)
\\&= a_0(t)x_p^{(n)}(t) +  \sum_{i=1}^n c_i(t) (-a_0x_i^{(n)}(t))
\\&\hspace{3em}\quad\text{ since the }x_i\text{ are solutions to the homogenous ode}
\\&= a_0(t)\sum_{i=1}^n \left(c_i'(t)x_i^{(n-1)}(t) + c_i(t)x_i^{(n)}(t)\right) +  \sum_{i=1}^n c_i(t) (-a_0x_i^{(n)}(t))
\\&= a_0(t)\sum_{i=1}^n c_i'(t)x_i^{(n-1)}(t)
\end{aligned}
\end{equation*}

Therefore, we obtain the following system for the \(c_i\)

\begin{equation*}
\begin{bmatrix}
x_1(t) & x_2(t) & \cdots & x_n(t) \\
x_1'(t) & x_2'(t) & \cdots & x_n'(t) \\
\vdots & & \ddots &\vdots\\
x_1^{(n-1)}(t) & x_2^{(n-1)}(t) & \cdots & x_n^{(n-1)}(t) \\
\end{bmatrix}
\begin{bmatrix}
c_1'(t) \\ c_2'(t) \\ \vdots \\ c_n'(t)
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0 \\ \vdots \\ \frac{g(t)}{a_0(t)}
\end{bmatrix}
\end{equation*}

Since the \(x_1, \ldots x_n\) are linearly independent, the Wronskian is non-zero
and hence we can easily solve for \(c_1', \ldots c_n'\).
By using Cramer's rule,

\begin{equation*}
c_i(t) = \int \frac{W_i[x_1, \ldots x_n]}{W[x_1, \ldots x_n]} \ dt
\end{equation*}

where \(W_i\) is the usual matrix of derivatives with the \(i\)'th column replaced with
the vector \(\begin{bmatrix}0 & 0 & \cdots & \frac{g(t)}{a_0(t)} \end{bmatrix}^T\).

\subsection{Constant coefficients}
\label{develop--math6120:ROOT:page--linear.adoc---constant-coefficients}

If the homogeneous linear ODE has constant coefficients, we substitute \(e^{\lambda t}\)
and solve for \(\lambda\). Let \((\lambda_1, m_1), \ldots (\lambda_k, m_k)\)
be the set of roots with corresponding multiplicities. Then, the general solution is

\begin{equation*}
\sum_{i=1}^k (a_{m_i-1}t^{m_i-1} + \cdots + a_0)e^{\lambda_i t}
\end{equation*}

For non-homogeneous ODE's the particular solution may have one of the following common forms

\begin{description}
\item[Expontential] If the \(g(t)\) is the sum of exponentials,
    try \(x_p\) to be the linear combination of exponentials of the same form.
    Note if any of the \(e^{\lambda_i t}\) appears in \(g(t)\),
    use \(t^{m_i}e^{\lambda_i t}\) instead.
\item[Sine and cosine] Sine and cosine are special cases of exponentials. Use the same logic.
\item[Polynomials] Try a polynomial of the same degree of \(g(t)\)
\end{description}

If \(g(t)\) is a linear combination of the above, solve for each separately and then simply add them.

\subsection{Euler equations}
\label{develop--math6120:ROOT:page--linear.adoc---euler-equations}

If \(a_i(t) = t^{n-i}\), try \(t^r\) as a solution to the homogeneous.
After plugging into the equation, we get

\begin{equation*}
q(r)t^r = 0
\end{equation*}

If we have repeated roots \(r_1\), this suggests that \(q'(r_1) = 0\).
So, differentiating with respect to \(r\) yeilds

\begin{equation*}
[q'(r) + q(r)\ln t]t^{r-1} = 0
\end{equation*}

Since \(q'(r_1) = q(r_1) = 0\), this suggests that \(t^{r_1}\ln t\) is also a solution.
\end{document}
