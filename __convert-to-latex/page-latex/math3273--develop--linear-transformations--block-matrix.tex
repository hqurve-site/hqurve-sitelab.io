\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Block matricies}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Now, instead of thinking of a matrix as a representation of a linear transformation, think
of it as just an array of elements. In particular, let these elements be simply smaller matrcies
(not necessarily all having the same dimensions). Then, we claim that multiplication of such
matricies coincide with multiplication of matricies without division. That is,

\begin{equation*}
\Phi = (A_{ij}) \quad\text{and}\quad \Psi = (B_{jk})
\end{equation*}

where \(A_{ij}\) is an \(m_i \times n_j\) matrix and \(B_{jk}\)
is an \(n_j\times t_k\) matrix (notice that its the same \(n_j\)) and the
dimensions of \(\Phi\) and \(\Psi\) are fit for multiplication (\(\Phi\) has \(N\) columns and \(\Psi\) has \(N\) rows). Then,

\begin{equation*}
\Phi \Psi = \left(\sum_{j} A_{ij}B_{jk}\right)
\end{equation*}

\begin{example}[{Proof}]
The proof of this is rather mechanical. Consider the matrix \((\Phi\Psi)_{ik}\), then on one hand,
its values come from multiplying the relevant row(s) of \(\Phi\) by columns in \(\Psi\). Then, we
would only focus on the multiplication of the \(i\)'th row of block matricies of \(\Phi\) by the
\(k\)'th column of block matricies of \(\Psi\). Then,

\begin{equation*}
c_{xz}
= \sum_{j=1}^{N} \sum_{y=1}^{n_j} (A_{ij})_{xy} (B_{jk})_{yz}
= \sum_{j=1}^{N} (A_{ij}B_{jk})_{xz}
\end{equation*}

and we are done.
\end{example}

\section{Understanding matrix multiplication using block matrices}
\label{develop--math3273:linear-transformations:page--block-matrix.adoc---understanding-matrix-multiplication-using-block-matrices}

In order to have a better understanding of matrix multiplication, we would
use block matricies. Let \(A\) and \(B\) have dimensions
\(a\times n\) and \(n\times b\) respectively. Then,

\begin{equation*}
AB =
\begin{pmatrix}
A_1 \\\hline A_2 \\\hline \vdots \\\hline A_a
\end{pmatrix}
\left(\begin{array}{c|c|c|c}
B_1 & B_2 & \cdots & B_b
\end{array}\right)
=
\left(\begin{array}{c|c|c|c}
A_1 B_1 & A_1B_2 & \cdots & A_1B_b\\\hline
A_2 B_1 & A_2B_2 & \cdots & A_2B_b\\\hline
\vdots & \vdots & \ddots & \vdots\\\hline
A_a B_1 & A_aB_2 & \cdots & A_aB_b
\end{array}\right)
\end{equation*}

where each of the \(A_i\) are row vectors and \(B_j\) column vectors. Then,
\(A_iB_j\) is just a \(1\times 1\) matrix who's value is essentially the dot product
of \(A_i\) and \(B_j\).

Alternatively,

\begin{equation*}
AB =
\left(\begin{array}{c|c|c|c}
A_1 & A_2 & \cdots & A_n
\end{array}\right)
\begin{pmatrix}
B_1 \\\hline B_2 \\\hline \vdots \\\hline B_n
\end{pmatrix}
= A_1 B_1 + A_2 B_2 + \cdots A_nB_n
\end{equation*}

where each of the \(A_i\) are column vectors and each of the \(B_j\) row vectors.
Then each of the \(A_iB_i\) is a \(a\times b\) matrix.

\begin{admonition-remark}[{}]
This second way of viewing matrix muliplication never dawned on me and
at the time of writing, I'm still kind of purplexed.
\end{admonition-remark}

Finally, a recursive way to view matrix mutliplication by a vector as

\begin{equation*}
AB =
\left(\begin{array}{c|c}
A & \vec{v}\\\hline
\vec{w}^T & x
\end{array}\right)
\begin{pmatrix}
\vec{u} \\\hline
b
\end{pmatrix}
=
\begin{pmatrix}
A \vec{u} + y \vec{v}\\\hline
\vec{w}^T\vec{u} + xy
\end{pmatrix}
\end{equation*}

where \(A\) is a \((a-1)\times (b-1)\) matrix and \(v\) and \(w\) are
vectors with \((b-1)\) entries.

\section{Theorem: determinant of upper triangular block matrix}
\label{develop--math3273:linear-transformations:page--block-matrix.adoc---theorem-determinant-of-upper-triangular-block-matrix}

Let \(M = \begin{pmatrix}A & B \\ 0 & C\end{pmatrix}\), then

\begin{equation*}
\det(M) = \det\begin{pmatrix}A & B \\ 0 & C\end{pmatrix} = \det(A)\det(C)
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
This is the same as the proof done in class
\end{admonition-note}

We have two cases, in the first when \(\det(A) = 0\), \(\exists \vec{x} \neq \varnothing\) such that \(A \vec{x} = \vec{0}\)
and hence

\begin{equation*}
\begin{pmatrix}A & B \\ 0 & C\end{pmatrix}\begin{pmatrix}\vec{x} \\ \vec{0}\end{pmatrix}
=\begin{pmatrix}A\vec{x} + B\vec{0} \\ 0\vec{x} & C\vec{0}\end{pmatrix}
=\begin{pmatrix}\vec{0} \\ \vec{0}\end{pmatrix}
\end{equation*}

and hence \(M\) is non-invertible and \(\det(M) = 0 = \det(A)\det(C)\).

Next, if \(\det(A) \neq 0\), \(A\) is invertible and

\begin{equation*}
M = \begin{pmatrix}A & B \\ 0 & C\end{pmatrix}
=\begin{pmatrix}A & 0 \\ 0 & I\end{pmatrix} \begin{pmatrix}I & A^{-1}B \\ 0 & C\end{pmatrix}
\end{equation*}

and hence

\begin{equation*}
\det(M)
= \det\begin{pmatrix}A & B \\ 0 & C\end{pmatrix}
= \det\begin{pmatrix}A & 0 \\ 0 & I\end{pmatrix} \det\begin{pmatrix}I & A^{-1}B \\ 0 & C\end{pmatrix}
= \det(A) \det(C)
\end{equation*}

and we are done.
\end{example}

\section{Understanding block matricies}
\label{develop--math3273:linear-transformations:page--block-matrix.adoc---understanding-block-matricies}

Let \(V\) be a vector space where \(\mathcal{A} = \mathcal{B}\sqcup \mathcal{C}\) is a basis. Additionally, let
\(span(\mathcal{B}) = U\), \(span(\mathcal{C}) = W\) and \(\dim(U) = m\), \(\dim(W) = n\) and \(T:V\to V\).
Then, consider the block matrix

\begin{equation*}
{}_\mathcal{A}[T]_\mathcal{A} = \left(\begin{array}{c|c} M_{11} & M_{21} \\\hline M_{21} & M_{22}\end{array}\right)
\end{equation*}

where \(M_{11}\) is an \(m\times m\) matrix (the dimensions of the rest follow). Now, consider arbirary
\(\vec{v} \in V\), which can be uniquely written as \(\vec{v} = \vec{u} + \vec{w}\) where \(\vec{u} \in U\)
and \(\vec{w} \in W\). Similarly, we can split \(T(\vec{u}) = T_1(\vec{u}) + T_2(\vec{u})\) and
\(T(\vec{w}) = T_1(\vec{w}) + T_2(\vec{w})\) where \(T_1: V \to U\) and \(T_2: V \to W\). Then, we can see that

\begin{equation*}
\begin{array}{cc}
M_{11}[\vec{u}]_\mathcal{B} = [T_1(\vec{u})]_\mathcal{B}
& M_{21}[\vec{w}]_\mathcal{C} = [T_1(\vec{w})]_\mathcal{B}
\\
M_{12}[\vec{u}]_\mathcal{B} = [T_2(\vec{u})]_\mathcal{C}
& M_{22}[\vec{w}]_\mathcal{C} = [T_2(\vec{u})]_\mathcal{C}
\end{array}
\end{equation*}

\begin{admonition-note}[{}]
\(T_1\) and \(T_2\) are unique since the matrix representation is unqiue and can be reversed to obtain
the linear transformation.
\end{admonition-note}

By writing \(T\) like this, it becomes apparent what each
quadent of the block matrix represents. Namely, it shows how
elements from each subspace \(U\) and \(V\) are mapped.

Additionally, note that if \(M_{12}\) and \(M_{21}\) are
both zero, \(T\) preserves the "separation/independence" of \(U\) and \(V\).

\begin{admonition-note}[{}]
This argument can be directly extended for larger block matricies and even if input
and output bases dont match. (In hindsight, I should have let them been different bases)
\end{admonition-note}

\section{Diagonal Block matrix}
\label{develop--math3273:linear-transformations:page--block-matrix.adoc---diagonal-block-matrix}

Let \(\{A_i\}\) be a finite sequence of square matrices where \(A_i\) is a \(n_i\times n_i\) matrix.
Then, we denote

\begin{equation*}
A_1 \oplus A_2\oplus \cdots \oplus A_n =
\begin{pmatrix}
A_1 & 0 & \cdots & 0\\
0 & A_2 & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \cdots & A_n
\end{pmatrix}
\end{equation*}

\begin{description}
\item[Is this notation a coincidence?] No, it isnt. As stated in the \myautoref[{previous section}]{develop--math3273:linear-transformations:page--block-matrix.adoc--independent-subspaces}, we can split
    the vector space into a direct product such that the linear transformation on each of the subspaces is
    independent and maps back onto itself.
\end{description}
\end{document}
