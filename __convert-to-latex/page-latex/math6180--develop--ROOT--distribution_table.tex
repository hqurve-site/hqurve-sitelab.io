\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Summary of Distributions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\section{Discrete}
\label{develop--math6180:ROOT:page--distribution-table.adoc---discrete}

\begin{table}[H]\centering
\begin{adjustbox}{width=\linewidth}
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Name} & {PMF (\(P(X=x)\))} & {Support} & {\(E[X]\)} & {\(Var[X]\)} & {\(E[e^{tX}]\)} \\
\hline[\thicktableline]
{\(Bernoulli(p)\)} & {\(p^x(1-p)^{1-x}\)} & {\(x=0,1\)} & {\(p\)} & {\(p(1-p)\)} & {\((1-p) + pe^t\)} \\
\hline
{\(Binomial(n,p)\)} & {\(p^x(1-p)^{n-x}\)} & {\(x=0,\ldots n\)} & {\(np\)} & {\(np(1-p)\)} & {\([(1-p) + pe^t]^n\)} \\
\hline
{\(Multinomial(n,p_1,\ldots p_k)\)} & {\(\binom{n}{x_1,\ldots x_k} \prod_{i=1}^k p_i^{x_i}\)} & {\(\sum_{i=1}^k x_i = n\), \(x_i \geq 0\)} & {\(E[X_i] = np_i\)} & {\(Var[X_i] = np_i(1-p_i)\)\newline
\(Cov[X_i,X_j] = -np_ip_j\)} & {\(\left[\sum_{i=1}^k p_ie^{t_i}\right]^n\)} \\
\hline
{(Exclusive) \(Geometric(p)\)} & {\((1-p)^{x}p\)} & {\(x=0,1,2\ldots\)} & {\(\frac{1-p}{p}\)} & {\(\frac{1-p}{p^2}\)} & {\(\frac{p}{1-(1-p)e^t}\)} \\
\hline
{(Inclusive) \(Geometric(p)\)} & {\((1-p)^{x-1}p\)} & {\(x=1,2\ldots\)} & {\(\frac{1}{p}\)} & {\(\frac{1-p}{p^2}\)} & {\(\frac{pe^t}{1-(1-p)e^t}\)} \\
\hline
{\(NegativeBinomial(k,p)\)} & {\(\binom{k+x-1}{x}p^k(1-p)^x\)} & {\(x=0,1,2\ldots\)} & {\(\frac{k(1-p)}{p}\)} & {\(\frac{k(1-p)}{p^2}\)} & {\(\left(\frac{p}{1-(1-p)e^t}\right)^k\)} \\
\hline
{\(Poisson(\lambda)\)} & {\(\frac{e^{-\lambda}\lambda^x}{x!}\)} & {\(x=0,1,2\ldots\)} & {\(\lambda\)} & {\(\lambda\)} & {\(\exp(\lambda(e^t-1))\)} \\
\hline
{\(Hypergeometric(N,M,n)\)} & {\(\frac{\binom{M}{x}\binom{N-M}{n-x}}{\binom{N}{n}}\)} & {\(x=\max(0,n+M-N),\ldots \min(n,M)\)} & {\(n\frac{M}{N}\)} & {\(n\frac{M}{N}\left(1-\frac{M}{N}\right)\frac{N-n}{N-1}\)} & {No simple closed form} \\
\hline[\thicktableline]
\end{tblr}
\end{adjustbox}
\end{table}

\section{Continuous}
\label{develop--math6180:ROOT:page--distribution-table.adoc---continuous}

\begin{table}[H]\centering
\begin{adjustbox}{width=\linewidth}
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Name} & {PDF} & {Support} & {\(E[X]\)} & {\(Var[X]\)} & {\(E[e^{tX}]\)} \\
\hline[\thicktableline]
{\(Uniform[a,b]\)} & {\(\frac{1}{b-a}\)} & {\(a \leq x \leq b\)} & {\(\frac{a+b}{2}\)} & {\(\frac{(b-a)^2}{12}\)} & {\(\frac{e^{tb}-e^{ta}}{t(b-a)}\) and \(1\) when \(t=0\)} \\
\hline
{\(Exponential(\lambda)\)} & {\(\lambda e^{-\lambda x}\)} & {\(x \geq 0\)} & {\(\frac{1}{\lambda}\)} & {\(\frac{1}{\lambda^2}\)} & {\(\frac{\lambda}{\lambda - t}\) for \(t < \lambda\)} \\
\hline
{\(Gamma(\alpha, \beta)\)} & {\(\frac{\beta^\alpha}{\Gamma(\alpha)} x^{\alpha-1}e^{-\beta x}\)} & {\(x \geq 0\)} & {\(\frac{\alpha}{\beta}\)} & {\(\frac{\alpha}{\beta^2}\)} & {\(\left(\frac{\beta}{\beta-t}\right)^\alpha\) for \(t < \beta\)} \\
\hline
{\(Normal(\mu, \sigma^2)\)} & {\(\frac{1}{\sqrt{2\pi \sigma^2}} \exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right)\)} & {\(x \in \mathbb{R}\)} & {\(\mu\)} & {\(\sigma^2\)} & {\(\exp\left(\mu t + \frac{\sigma^2 t^2}{2}\right)\)} \\
\hline[\thicktableline]
\end{tblr}
\end{adjustbox}
\end{table}
\end{document}
