\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Sylow Theorems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
\def\normalsubgroup{\trianglelefteq}
\DeclareMathOperator{\ord}{ord}

% Title omitted
Let \(G\) be a finite group of order \(n = p^k m\)
where \(p\) is a prime and \(p \nmid m\).
Then, if \(H \leq G\) and \(|H| = p^k\),
we call \(H\) a \emph{Sylow \(p\)-subgroup} of \(G\).

\section{Theorem I}
\label{develop--math3272:sylow:page--theorems.adoc---theorem-i}

Let \(|G| = p^km\). Then \(G\) has a subgroup
of order \(p^k\). In particular Sylow \(p\)-subgroups
exist.

\begin{example}[{Proof}]
We would do this by strong induction on the order of \(G\). In particular,
we let

\begin{equation*}
P(n): \text{If } |G| = n \text{ and } p^k | n \text{ then } \exists K \leq G: |K| = p^k
\end{equation*}

Now, \(P(1)\) is trivially true. So for our inductive step,
suppose \(P(m)\) is true for all \(m < n\).
Next, if \(k=0\), the statement is also
true by taking \(K = \{e\}\). So, consider \(|G| = n\) where \(p^k | n\)
and \(k \geq 1\). So, consider the class equation,

\begin{equation*}
|G| = |Z(G)| + \sum_{x \in I} [G: G_x]
\end{equation*}

It would be nice of all the indices were divisible by \(p\).
So, suppose that there exists a \(x \in G \backslash Z(G)\) such that \([G:G_x]\) is
not divisible by \(p\). Then since \(|G| = [G:G_x]|G_x|\),
we get that \(p^k\) divides \(|G_x|\). Then since \(G_x\) is a strict
subset of \(G\), since \(x \notin Z(G)\), we get that \(|G_x| < |G| = n\)
hence \(G_x\) has a subgroup of order \(p^k\) by induction.

On the other hand, if \(p| [G:G_x]\) for all \(x \in I\),
we get that \(p\) must divide \(|Z(G)|\). Then, by \myautoref[{Lemma 1}]{develop--math3272:sylow:page--theorems.adoc---lemma-1},
there exists \(x \in Z(G)\) such that \(\ord(x) = p\). Then, since
\(x \in Z(G)\), \(H = \abrack{x} \normalsubgroup G\).
Then since \(p^{k-1}\) divides \(|G/H|\) and \(|G/H| < |G| = n\),
we get that \(G/H\) has a subgroup \(K/H\) of order \(p^{k-1}\) by our inductive hypothesis.
Note that this subgroup has form \(K/H\) by the
\myautoref[{Lattice Isomorphism Theorem}]{develop--math3272:ROOT:page--isomorphism-theorems.adoc---lattice-isomorphism-theorem}.
Then \(|K| = |K/H||H| = p^k\) as desired.
\end{example}

\subsection{Lemma 1}
\label{develop--math3272:sylow:page--theorems.adoc---lemma-1}

If \(G\) is an abelian group who's order is divisible
by \(p\), then \(G\) has an element of order \(p\).

\begin{example}[{Proof}]
We prove this by induction on the order of \(G\).
The base case is simple since when \(|G| = p\)
we simply take the generator of \(G\).

Next, if we have a group of order \(n\) where \(p | n\),
take an element \(x \in G\) where \(d = \ord(x)\). Then,
if \(p | d\) we simply take \(x^{d/p}\).

Otherwise, suppose that \(p \nmid d\). Then,
\(\gcd(p, d) = 1\) and hence \(p | \frac{n}{d}\).
Then if \(H = \abrack{x}\), \(H \normalsubgroup G\)
since \(G\) is abelian and hence \(|G/H| = \frac{n}{d} < n\)
which has an element \(yH\) of order \(p\) by our inductive hypothesis.

Consider \(y^d\), then \(y^d \notin H\) otherwise it would imply
that \(y = y^{ap + bd} = y^{ap}y^{bd} = (y^p)^a(y^d)^b \in H\) since \(y^p \in H\) (since \((yH)^p = H\))
where \(ap + bd = 1\) by Bezout's Lemma. Therefore, \(y^d \neq e\) however,
\((y^d)^p = (y^p)^d = e\) since \(y^p \in H\) and \(|H| = d\). Therefore
\(\ord(y^d) = p\) as desired.

In both cases, we get an element of order \(p\) and hence we have concluded our inductive step.
\end{example}

\section{Theorem II}
\label{develop--math3272:sylow:page--theorems.adoc---theorem-ii}

Let \(G\) be a finite group with Sylow \(p\)-subgroups,
\(P\) and \(Q\). Then, \(P\) and \(Q\) are conjugates.
That is,

\begin{equation*}
\exists g \in G: Q = gPg^{-1}
\end{equation*}

To see a proof of this, apply lemma 2

\subsection{Lemma 2}
\label{develop--math3272:sylow:page--theorems.adoc---lemma-2}

Let \(H \leq G\) be a \(p\)-group and \(P\) be a Sylow
\(p\)-subgroup of \(G\), then

\begin{equation*}
\exists g \in G: H \leq gPg^{-1}
\end{equation*}

\begin{example}[{Proof}]
Since the proof given in class does not directly give any hint as to its
inspiration (as admitted by Dr Tweedle), we would `derive' the proof.

Suppose that we already have this \(g \in G\), then

\begin{equation*}
\begin{aligned}
H \leq gPg^{-1}
&\iff \forall h \in H: h \in gPg^{-1}
\\&\iff \forall h \in H: hg \in gP
\\&\iff \forall h \in H: hgP = gP
\end{aligned}
\end{equation*}

Recall that \(x \in gP \implies xP = gP\) since \(x \in xP\) and cosets are disjoint or equal.
Therefore, we see that if \(H\) is acting on the left cosets of \(P\),
we want to show that there is coset which is stabilized by the entirety of \(H\).
We then look at the class equation. In particular, since \(H\) is a \(p\)-group

\begin{equation*}
|G/P| \equiv |Z| \mod p
\end{equation*}

Now, since \(|G/P| = m\) which is not divisible by \(p\), it implies that
\(|Z| \not\equiv 0 \mod p\) and hence \(Z \neq \varnothing\) and we are done.
\end{example}

\section{Theorem III}
\label{develop--math3272:sylow:page--theorems.adoc---theorem-iii}

Let \(G\) be a finite group of order \(p^km\) with \(n_p\) Sylow \(p\)-subgroups.
Then,

\begin{itemize}
\item \(n_p \equiv 1 \mod p\)
\item \(n_p | m\)
\end{itemize}

\begin{example}[{Proof}]
Firstly, by letting \(X\) be the set of Sylow \(p\)-subgroups,
we see that \(G\) acts on \(X\) by conjugation and by
the orbit stabilizer theorem,

\begin{equation*}
n_p = |X| = \frac{|G|}{|N_G(P)|} = [G:N_G(P)]
\end{equation*}

by theorem II. Furthermore, by lemma 3

\begin{equation*}
n_p[N_G(P):P] = [G:N_G(P)][N_G(P):P] = [G:P] = m
\end{equation*}

and hence we have proven the second result.

Now, by lemma 3, we get that

\begin{equation*}
n_p[N_G(P):P] = [G:P] \equiv [N_G(P):P] \mod p
\end{equation*}

and hence we have proven the first result since \(\mathbb{Z}/p\mathbb{Z}\)
forms a field and \([N_G(P):P] | m\) which implies
that

\begin{equation*}
\gcd([N_G(P):P], p) \mid \gcd(m, p) = 1
\end{equation*}
\end{example}

\subsection{Lemma 3}
\label{develop--math3272:sylow:page--theorems.adoc---lemma-3}

If \(H \leq G\) is a \(p\)-group, then

\begin{itemize}
\item the number of subgroups conjugate to \(H\) is \([G:N_G(H)]\)
\item \([G:H] \equiv [N_G(H): H] \mod p\)
\end{itemize}

\begin{example}[{Proof}]
Let \(X\) be the set of conjugate subgroups conjugate to \(H\), then
\(G\) acts on \(X\) via conjugation and by the orbit stabilizer theorem

\begin{equation*}
|X| |G_H| = |G| \implies |X| = [G:G_H] = [G:N_G(H)]
\end{equation*}

since \(X\) is the orbit of \(H\) by definition and

\begin{equation*}
g \in G_H \iff gHg^{-1} = H \iff g \in N_G(H)
\end{equation*}

Next, let \(Y\) be the set of left cosets of \(H\), then
\(H\) acts on \(Y\) by left multiplication and by the class equation

\begin{equation*}
|Y| \equiv |Z| \mod p
\end{equation*}

where

\begin{equation*}
\begin{aligned}
gH \in Z
&\iff \forall h\in H: hgH = gH
\\&\iff \forall h\in H: g^{-1}hg \in H
\\&\iff g^{-1}Hg \leq H
\\&\iff g^{-1}Hg = H
\\&\iff g \in N_G(H)
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
[G:H] = |Y| \equiv |Z| = [N_G(H):H] \mod p
\end{equation*}
\end{example}
\end{document}
