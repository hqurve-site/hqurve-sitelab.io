\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Simple Random Sample}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In a Simple Random Sample, all elements have the same probability of being selected.
Let \(N\) be the population size and \(n\) be the sample size.

We focus on

\begin{description}
\item[SRSWOR] In a Simple Random Sample Without Replacement, all samples have the same probability
    of being selected \(p(t) = 1/\binom{N}{n}\)
\item[SRSWR] In a Simple Random Sample With Replacement, the i'th sample value has equal probability
    to be any of the population elements. So, each sample has probability \(p(t) = 1/N^n\)
    of being selected.
\end{description}

Note that if the population is infinite, there is no difference between these two methods.

Also let \(Y_i\) be the characteristic of interest for the \(i\)'th element of the population.
Let \(y_i\) be the characteristic of interest for the \(i\)'th element of the sample.
Let \(a_i\) be the number of times that the \(i\)'th population element is in the sample.
Also define

\begin{description}
\item[Mean] 
    
    \begin{equation*}
    \overbar{Y} = \frac{1}{N} \sum_{i=1}^N Y_i
    \quad\text{and}\quad
    \overbar{y} = \frac{1}{n}\sum_{i=1}^n y_i
    \end{equation*}
\item[Variance] 
    
    \begin{equation*}
    \begin{array}{cc}
    S^2 = \frac{1}{N-1}\sum_{i=1}^N (Y_i-\overbar{Y})^2
    &
    s^2 = \frac{1}{n-1}\sum_{i=1}^n (y_i-\overbar{y})^2
    \\
    \sigma^2 = \frac{1}{N}\sum_{i=1}^N (Y_i-\overbar{Y})^2
    &
    \hat{\sigma}^2 = \frac{1}{n}\sum_{i=1}^n (y_i-\overbar{y})^2
    \end{array}
    \end{equation*}
\end{description}

Terminology

\begin{description}
\item[Finite population corection] \(\frac{N-n}{N}\)
\item[Sampling fraction] \(\frac{n}{N}\)
\end{description}

\section{Distribution of the \(a_i\)}
\label{develop--stat6130:ROOT:page--simple-random.adoc---distribution-of-the-latex-backslasha-latex-underscorei-latex-backslash}

We define \(a_i\) to be the number of times that the \(i\)'th population element
is in the sample.
These \(a_i\) help simplify some calculations
and allow results to be shared.

The first thing to note is that in SRSWOR, the \(a_i\) are Bernoulli
random variables;
however, in SRSWR, each \(a_i\) can take any value from \(1,\ldots n\).
In both cases the \(a_i\) are not independent.

\subsection{Without replacement}
\label{develop--stat6130:ROOT:page--simple-random.adoc---without-replacement}

We observe that the \(a_i\) is Bernoulli

\begin{itemize}
\item \(E[a_i] = P(a_i=1) = \frac{n}{N}\)
\item \(Var[a_i] = P(a_i=1) = \frac{n(N-n)}{N^2}\)
\item \(E[a_ia_j]=P(a_i=1, a_j=1) = \frac{n(n-1)}{N(N-1)}\) for \(i \neq j\)
\item \(Cov(a_i, a_j) = -\frac{n(N-n)}{N^2(N-1)}\) for \(i\neq j\)
\end{itemize}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
P(a_i=1)
&= \frac{\#\text{ of ways given that i'th element is chosen}}{\#\text{ number of samples}}
\\&= \frac{\binom{N-1}{n-1}}{\binom{N}{n}}
\\&= \frac{\frac{(N-1)!}{(n-1)!(N-n)!}}{\frac{N!}{n!(N-n)!}}
\\&= \frac{(N-1)!}{N!}\frac{n!}{(n-1)!}
\\&= \frac{n}{N}
\end{aligned}
\end{equation*}

Next, for \(i\neq j\)

\begin{equation*}
\begin{aligned}
P(a_i=1, a_j=1)
&= \frac{\#\text{ of ways given that i'th and j'th elements are chosen}}{\#\text{ number of samples}}
\\&= \frac{\binom{N-2}{n-2}}{\binom{N}{n}}
\\&= \frac{\frac{(N-2)!}{(n-2)!(N-n)!}}{\frac{N!}{n!(N-n)!}}
\\&= \frac{(N-2)!}{N!}\frac{n!}{(n-2)!}
\\&= \frac{n(n-1)}{N(N-1)}
\end{aligned}
\end{equation*}

Next

\begin{equation*}
\begin{aligned}
Cov(a_i,a_j)
&= E[a_ia_j] - E[a_i]E[a_j]
\\&= P(a_ia_j=1) - P(a_i=1)P(a_j=1)
\\&= \frac{n(n-1)}{N(N-1)} - \frac{n^2}{N^2}
\\&= \frac{Nn(n-1) - n^2(N-1)}{N^2(N-1)}
\\&= \frac{-Nn +n^2}{N^2(N-1)}
\\&= -\frac{n(N-n)}{N^2(N-1)}
\end{aligned}
\end{equation*}
\end{proof}

\subsection{With Replacement}
\label{develop--stat6130:ROOT:page--simple-random.adoc---with-replacement}

We observe that for the \(a_i\)

\begin{itemize}
\item the marginal distribution is \(Binomial\left(n, \frac{1}{N}\right)\)
    
    \begin{itemize}
    \item \(E[a_i] = \frac{n}{N}\)
    \item \(Var[a_i] = \frac{n(N-1)}{N^2}\)
    \end{itemize}
\item their joint distribution is \(Multinomial\left(n, \vec{1}_N \frac{1}{N}\right)\)
    
    \begin{itemize}
    \item \(E[a_ia_j] = \frac{n(n-1)}{N^2}\)
    \item \(Cov[a_i,a_j] = -\frac{n}{N^2}\)
    \end{itemize}
\end{itemize}

\begin{proof}[{}]
Note that each \(y_i\) can be any of the \(Y_1, \ldots Y_N\) with equal probability \(\frac{1}{N}\).
Since we have \(y_1, \ldots y_n\) and the \(a_i\) are counting
how many are the \(i\)'th population element,
this is clearly a multinomial distribution.

Recall that the marginal distribution of the multinomial is binomial.
So, we get the desired expectation and variance

Next, recall that the mgf of a multinomial distribution is

\begin{equation*}
M(t_1, \ldots t_N) = \left(p_1e^{t_1} + \cdots p_Ne^{t_N}\right)^n = \frac{1}{N^n}\left(e^{t_1}+\cdots e^{t_N}\right)^n
\end{equation*}

So,

\begin{equation*}
E[a_ia_j] = M_{ij}(0) = \frac{n(n-1)}{N^n}(N)^{n-2} = \frac{n(n-1)}{N^2}
\end{equation*}

Therefore

\begin{equation*}
Cov(a_i, a_j) = E[a_ia_j]- E[a_i]E[a_j] = \frac{n(n-1)}{N^2} - \frac{n^2}{N^2} = -\frac{n}{N^2}
\end{equation*}
\end{proof}

\section{Means}
\label{develop--stat6130:ROOT:page--simple-random.adoc---means}

For both SRSWR and SRSWOR the sample mean is an unbiased estimator of the population mean.

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
E[\overbar{y}]
&= E\left[\frac{1}{n}\sum_{i=1}^n y_i\right]
\\&= \frac{1}{n}\sum_{i=1}^n E\left[ y_i\right]
\\&= \frac{1}{n}\sum_{i=1}^n \sum_{j=1}^N Y_iP(Y_i)
\\&= \frac{1}{n}\sum_{i=1}^n \sum_{j=1}^N Y_i \frac{1}{N}
\\&= \frac{1}{n}\sum_{i=1}^n \overbar{Y}
\\&= \overbar{Y}
\end{aligned}
\end{equation*}
\end{proof}

We can also compute the variance of the sample mean in SRSWR and SRSWOR.

\begin{equation*}
Var(\overbar{y}_{WOR}) = \frac{N-n}{Nn}S^2
\quad\text{and}\quad
Var(\overbar{y}_{WR}) = \frac{N-1}{Nn}S^2
\end{equation*}

\begin{proof}[{SRSWOR}]
\begin{equation*}
\begin{aligned}
Var(\overbar{y})
&= Var\left(\frac{1}{n}\sum_{i=1}^N a_iY_i\right)
\\&= \frac{1}{n^2} \sum_{i,j} Y_iY_j Cov(a_i, a_j)
\\&= \frac{1}{n^2} \left[\sum_{i=1}^N Y_i^2 Var(a_i) + \sum_{i\neq j} Y_iY_j Cov(a_i, a_j)\right]
\\&= \frac{1}{n^2} \left[\sum_{i=1}^N Y_i^2 \frac{n(N-n)}{N^2} - \sum_{i\neq j} Y_iY_j \frac{n(N-n)}{N^2(N-1)} \right]
\\&= \frac{1}{n^2} \left[\frac{n(N-n)}{N^2}\sum_{i=1}^N Y_i^2  - \frac{n(N-n)}{N^2(N-1)}\sum_{i\neq j} Y_iY_j  \right]
\\&= \frac{N-n}{nN^2} \left[\sum_{i=1}^N Y_i^2  - \frac{1}{N-1}\sum_{i\neq j} Y_iY_j  \right]
\\&= \frac{N-n}{nN^2} \left[\sum_{i=1}^N Y_i^2  - \frac{1}{N-1}\left(\left(\sum_{i=1}^NY_i\right)^2 - \sum_{i=1}^N Y_i^2\right)  \right]
\\&= \frac{N-n}{nN^2} \left[\sum_{i=1}^N Y_i^2  - \frac{1}{N-1}\left(N^2\overbar{Y}^2 - \sum_{i=1}^N Y_i^2\right)  \right]
\\&= \frac{N-n}{nN^2} \left[\frac{N}{N-1}\sum_{i=1}^N Y_i^2  - \frac{N^2}{N-1}\overbar{Y}^2  \right]
\\&= \frac{N-n}{nN}\frac{1}{N-1} \left[\sum_{i=1}^N Y_i^2  - N\overbar{Y}^2  \right]
\\&= \frac{N-n}{nN} S^2
\end{aligned}
\end{equation*}
\end{proof}

\begin{proof}[{SRSWR}]
\begin{equation*}
\begin{aligned}
Var(\overbar{y})
&= Var\left(\frac{1}{n}\sum_{i=1}^N a_iY_i\right)
\\&= \frac{1}{n^2} \sum_{i,j} Y_iY_j Cov(a_i, a_j)
\\&= \frac{1}{n^2} \left[\sum_{i=1}^N Y_i^2 Var(a_i) + \sum_{i\neq j} Y_iY_j Cov(a_i, a_j)\right]
\\&= \frac{1}{n^2} \left[\sum_{i=1}^N Y_i^2 \frac{n(N-1)}{N^2} - \sum_{i\neq j} Y_iY_j \frac{n}{N^2} \right]
\\&= \frac{1}{n^2} \left[\frac{n(N-1)}{N^2}\sum_{i=1}^N Y_i^2  - \frac{n}{N^2}\sum_{i\neq j} Y_iY_j  \right]
\\&= \frac{1}{nN^2} \left[(N-1)\sum_{i=1}^N Y_i^2  - \sum_{i\neq j} Y_iY_j  \right]
\\&= \frac{1}{nN^2} \left[(N-1)\sum_{i=1}^N Y_i^2  - \left(\left(\sum_{i=1}^NY_i\right)^2 - \sum_{i=1}^N Y_i^2\right)  \right]
\\&= \frac{1}{nN^2} \left[(N-1)\sum_{i=1}^N Y_i^2  - \left(N^2\overbar{Y}^2 - \sum_{i=1}^N Y_i^2\right)  \right]
\\&= \frac{1}{nN^2} \left[N\sum_{i=1}^N Y_i^2  - N^2\overbar{Y}^2  \right]
\\&= \frac{1}{nN} \left[\sum_{i=1}^N Y_i^2  - N\overbar{Y}^2  \right]
\\&= \frac{1}{nN} (N-1)S^2
\end{aligned}
\end{equation*}
\end{proof}

\section{Variance}
\label{develop--stat6130:ROOT:page--simple-random.adoc---variance}

For SRSWR

\begin{equation*}
E[s^2] = \sigma^2
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
E[s^2]
&= \frac{1}{n-1}\left[\sum_{i=1}^n E[y_i^2] - nE[\overbar{y}^2]\right]
\\&= \frac{1}{n-1}\left[\sum_{i=1}^n (Var[y_i] + E[y_i]^2) - n(Var[\overbar{y}] +E[\overbar{y}]^2)\right]
\\&= \frac{1}{n-1}\left[\sum_{i=1}^n (\sigma^2 + \overbar{Y}^2) - n\left(\frac{N-n}{Nn}S^2 + \overbar{Y}^2\right)\right]
\\&= \frac{1}{n-1}\left[\sum_{i=1}^n \left(\frac{N-1}{N}S^2 + \overbar{Y}^2\right) - n\left(\frac{N-n}{Nn}S^2 + \overbar{Y}^2\right)\right]
\\&= \frac{1}{n-1}\left[\frac{n(N-1)}{N}S^2 + n\overbar{Y}^2 - \left(\frac{N-n}{N}S^2 - n\overbar{Y}^2\right)\right]
\\&= \frac{1}{n-1}\left[\frac{nN - N)}{N}S^2\right]
\\&= S^2
\end{aligned}
\end{equation*}
\end{proof}

For SRSWOR

\begin{equation*}
E[s^2] = \sigma^2
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
E[s^2]
&= \frac{1}{n-1}\left[\sum_{i=1}^n E[y_i^2] - nE[\overbar{y}^2]\right]
\\&= \frac{1}{n-1}\left[\sum_{i=1}^n (Var[y_i] + E[y_i]^2) - n(Var[\overbar{y}] +E[\overbar{y}]^2)\right]
\\&= \frac{1}{n-1}\left[\sum_{i=1}^n (\sigma^2 + \overbar{Y}^2) - n\left(\frac{N-1}{Nn}S^2 + \overbar{Y}^2\right)\right]
\\&= \frac{1}{n-1}\left[\sum_{i=1}^n \left(\frac{N-1}{N}S^2 + \overbar{Y}^2\right) - n\left(\frac{N-1}{Nn}S^2 + \overbar{Y}^2\right)\right]
\\&= \frac{1}{n-1}\left[\frac{n(N-1)}{N}S^2 + n\overbar{Y}^2 - \left(\frac{N-1}{N}S^2 - n\overbar{Y}^2\right)\right]
\\&= \frac{1}{n-1}\left[\frac{(n-1)(N-1)}{N}S^2\right]
\\&= \frac{1}{n-1}\left[(n-1)\sigma^2\right]
\\&= \sigma^2
\end{aligned}
\end{equation*}
\end{proof}

\begin{corollary}[{Unbiased estimator of sample mean variance (SRSWOR)}]
Given a SRSWOR, an unbiased estimator of \(Var(\overbar{y})\) is

\begin{equation*}
\hat{Var}(\overbar{y}) = \frac{1-f}{n}s^2
\end{equation*}

\begin{proof}[{}]
Recall \(Var(\overbar{y}) = \frac{N-n}{Nn}S^2\).

Then

\begin{equation*}
E\left[\hat{Var}(\overbar{y})\right] = E\left[\frac{1-f}{n}s^2\right] = \frac{1-f}{n}S^2 = \frac{1 - \frac{n}{N}}{n}S^2 = \frac{N-n}{nN}S^2
\end{equation*}
\end{proof}
\end{corollary}

\begin{corollary}[{Unbiased estimator of sample mean variance (SRSWR)}]
Given a SRSWR, an unbiased estimator of \(Var(\overbar{y})\) is

\begin{equation*}
\hat{Var}(\overbar{y}) = \frac{1}{n}s^2
\end{equation*}

\begin{proof}[{}]
Recall \(Var(\overbar{y}) = \frac{N-1}{Nn}S^2\).

Then

\begin{equation*}
E\left[\hat{Var}(\overbar{y})\right]
= E\left[\frac{1}{n}s^2\right]
= \frac{1}{n}E\left[s^2\right]
= \frac{1}{n}\sigma^2
= \frac{1}{n}\frac{N-1}{N} S^2
= \frac{N-1}{Nn} S^2
\end{equation*}
\end{proof}
\end{corollary}

\section{Sample Size Determination (SRSWOR)}
\label{develop--stat6130:ROOT:page--simple-random.adoc---sample-size-determination-srswor}

Choosing a sample size is always an important question, unless you are determined to do a census.
The choice of sample size depends on your desired constraint.

Some of these methods require estimates of \(S^2\).
This can be obtained using

\begin{itemize}
\item Pilot survey
\item Literature Review
\end{itemize}

The following table contains various methods of sample size determination

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Prespecified} & {Constraint} & {Solution} & {Notes} \\
\hline[\thicktableline]
{Variance} & {\(Var(\overbar{y}) \leq V\)} & {\(\displaystyle n\geq \frac{n_l}{1-\frac{n_l}{N}}\)} & {\begin{itemize}
\item \(\displaystyle n_l = \frac{S^2}{V}\)
\item As \(N\to \infty\), \(n\to n_l\)
\end{itemize}} \\
\hline
{Estimation Error} & {\(P(|\overbar{y} - \overbar{Y}| \leq e) = 1-\alpha\)} & {\(\displaystyle n = \frac{\left(\frac{z_{\alpha/2}S}{e}\right)^2}{1+ \frac{1}{N}\left(\frac{z_{\alpha/2}S}{e}\right)^2}\)} & {} \\
\hline
{Width} & {\(2z_{\alpha/2}\sqrt{Var(\overbar{y})} \leq W\)} & {\(\displaystyle n \geq \frac{\frac{4z_{\alpha/2}^2S^2}{W^2}}{1+ \frac{1}{N}\left(\frac{4z_{\alpha/2}^2S^2}{W^2}\right)}\)} & {} \\
\hline
{Coefficient of variation} & {\(c.v.(\overbar{y}) \leq C_0 \)} & {\(\displaystyle n \geq \frac{C^2/C_0^2}{1 + \frac{1}{N}\left(C_2/C_0^2\right)}\)} & {\begin{itemize}
\item \(c.v.(\overbar{y}) = \frac{\sqrt{Var(\overbar{y})}}{\overbar{Y}}\)
\item \(C = \frac{S}{\overbar{Y}}\)
\end{itemize}} \\
\hline
{Relative Error} & {\(P\left(\left|\frac{\overbar{y} - \overbar{Y}}{\overbar{Y}}\right| \leq R \right) = 1-\alpha\)} & {\(\displaystyle n = \frac{\left(\frac{z_{\alpha/2}C}{R}\right)^2}{1+ \frac{1}{N}\left(\frac{z_{\alpha/2}C}{R}\right)^2}\)} & {\begin{itemize}
\item \(C = \frac{S}{\overbar{Y}}\)
\end{itemize}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{proof}[{Prespecified variance}]
\begin{equation*}
\begin{aligned}
Var(\overbar{y}) &\leq V
\\ \frac{N-n}{Nn}S^2 &\leq V
\\ \frac{N-n}{Nn} &\leq \frac{V}{S^2} = \frac{1}{n_l}
\\ \frac{1}{n} - \frac{1}{N} &\leq \frac{1}{n_l}
\\ \frac{1}{n} &\leq \frac{1}{n_l} + \frac{1}{N}
\\ n &\geq \frac{1}{\frac{1}{n_l} + \frac{1}{N}} = \frac{n_l}{1-\frac{n_l}{N}}
\end{aligned}
\end{equation*}
\end{proof}

\section{Auxiliary Variable}
\label{develop--stat6130:ROOT:page--simple-random.adoc---auxiliary-variable}

Suppose we have variable of interest \(X\) which is hard/expensive to directly observe.
Instead, we can observe an auxiliary variable \(Y\) which may be ``closely related''
to \(X\).
In such a setup, we can instead have data \((x,y)\) where \(y\) is observed but
\(x\) is unobserved.
In this case, we may have the natural question of how good the estimators based on
\(y\) are at estimating properties of \(X\).

\begin{theorem}[{Covariance of sample means}]
Consider paired sample data \((x_1, y_1), \cdots (x_n, y_n)\).
Then,

\begin{itemize}
\item If the sample is from SRSWOR
    
    \begin{equation*}
    Cov(\overbar{y}, \overbar{x}) = \left(\frac{1-f}{n}\right)S_{xy}
    \end{equation*}
\item If the sample is from SRSWR
    
    \begin{equation*}
    Cov(\overbar{y}, \overbar{x}) = \left(\frac{N-1}{nN}\right)S_{xy}
    \end{equation*}
\end{itemize}

where \(S_{xy} = \frac{1}{N-1}\sum_{i=1}^N(Y_i-\overbar{Y})(X_i-\overbar{X}) = \frac{1}{N-1}\left(\sum_{i=1}^N X_iY_i - N\overbar{X}\overbar{Y}\right)\).

\begin{admonition-note}[{}]
We use notation \(S_{xy}\) instead of \(S_{XY}\) since we are estimating
the correlation between a single pair \((x,y)\).
\end{admonition-note}
\end{theorem}

\begin{proof}[{SRSWOR}]
Throughout this proof we use \(a_i\) since it allows us to indicate whether an entire pair is present or missing.

\begin{equation*}
\begin{aligned}
&Cov(\overbar{y}, \overbar{x})
\\&= Cov\left(\frac{1}{n}\sum_{i=1}^Na_iX_i, \frac{1}{n}\sum_{i=1}^Na_iY_i\right)
\\&= \frac{1}{n^2}\sum_{i=1}^N\sum_{j=1}^NX_iY_jCov(a_i, a_j)
\\&= \frac{1}{n^2}\left[\sum_{i=1}^NX_iY_iCov(a_i, a_i) + \sum_{i\neq j}X_iY_jCov(a_i, a_j)\right]
\\&= \frac{1}{n^2}\left[
    \sum_{i=1}^NX_iY_i \frac{n}{N}\left(1-\frac{n}{N}\right)
    + \sum_{i\neq j}X_iY_j\left(-\frac{n}{N}\right)\left(1-\frac{n}{N}\right)\left(\frac{1}{N-1}\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N}\left(1-\frac{n}{N}\right)\left[
    \sum_{i=1}^NX_iY_i
    -\frac{1}{N-1} \sum_{i\neq j}X_iY_j
\right]
\\&= \frac{1}{n^2}\frac{n}{N}\left(1-\frac{n}{N}\right)\left[
    \sum_{i=1}^NX_iY_i
    -\frac{1}{N-1} \left(\left(\sum_{i=1}^N X_i\right)\left(\sum_{i=1}^N Y_i\right) - \sum_{i=1}^NX_iY_i\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N}\left(1-\frac{n}{N}\right)\left[
    \frac{N}{N-1}\sum_{i=1}^NX_iY_i
    -\frac{1}{N-1} \left(\sum_{i=1}^N X_i\right)\left(\sum_{i=1}^N Y_i\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N}\left(1-\frac{n}{N}\right)\frac{N}{N-1}\left[
    \sum_{i=1}^NX_iY_i
    -\frac{1}{N} \left(\sum_{i=1}^N X_i\right)\left(\sum_{i=1}^N Y_i\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N}\left(1-\frac{n}{N}\right)\frac{N}{N-1}\left[
    \sum_{i=1}^NX_iY_i
    -N\overbar{X}\overbar{Y}
\right]
\\&= \frac{1}{n^2}\frac{n}{N}\left(1-\frac{n}{N}\right)N S_{xy}
\\&= \frac{1}{n}\left(1-\frac{n}{N}\right) S_{xy}
\\&= \frac{1-f}{n} S_{xy}
\end{aligned}
\end{equation*}
\end{proof}

\begin{proof}[{SRSWOR}]
Throughout this proof we use \(a_i\) since it allows us to indicate whether an entire pair is present or missing.

\begin{equation*}
\begin{aligned}
&Cov(\overbar{y}, \overbar{x})
\\&= Cov\left(\frac{1}{n}\sum_{i=1}^Na_iX_i, \frac{1}{n}\sum_{i=1}^Na_iY_i\right)
\\&= \frac{1}{n^2}\sum_{i=1}^N\sum_{j=1}^NX_iY_jCov(a_i, a_j)
\\&= \frac{1}{n^2}\left[\sum_{i=1}^NX_iY_iCov(a_i, a_i) + \sum_{i\neq j}X_iY_jCov(a_i, a_j)\right]
\\&= \frac{1}{n^2}\left[
    \sum_{i=1}^NX_iY_i \frac{n(N-1)}{N^2}
    + \sum_{i\neq j}X_iY_j\left(-\frac{n}{N^2}\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N^2}\left[
    (N-1)\sum_{i=1}^NX_iY_i
    - \sum_{i\neq j}X_iY_j
\right]
\\&= \frac{1}{n^2}\frac{n}{N^2}\left[
    (N-1)\sum_{i=1}^NX_iY_i
    - \left(\left(\sum_{i=1}^N X_i\right)\left(\sum_{i=1}^N Y_i\right) - \sum_{i=1}^NX_iY_i\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N^2}\left[
    N\sum_{i=1}^NX_iY_i
    - \left(\sum_{i=1}^N X_i\right)\left(\sum_{i=1}^N Y_i\right)
\right]
\\&= \frac{1}{n^2}\frac{n}{N^2}\left[
    N\sum_{i=1}^NX_iY_i
    - N^2 \overbar{X}\overbar{Y}
\right]
\\&= \frac{1}{n^2}\frac{n}{N^2}N\left[
    \sum_{i=1}^NX_iY_i
    - N \overbar{X}\overbar{Y}
\right]
\\&= \frac{1}{n^2}\frac{n}{N^2}N(N-1)S_{xy}
\\&= \frac{N-1}{nN}S_{xy}
\end{aligned}
\end{equation*}
\end{proof}

\section{Proportions}
\label{develop--stat6130:ROOT:page--simple-random.adoc---proportions}

Consider a qualitative characteristic based on a population divided into
two mutually exclusive classes \(C\) and \(C^*\).

Let \(A\) be the number of units with characteristic \(C\).
Then, there are \(N-C\) units with characteristic \(C^*\).

The proportion of units in \(C\) is \(P = \frac{A}{N}\)
and the proportion of units in \(C^*\) is \(Q = 1-\frac{A}{N}\).

If we assign \(Y_i=1\) if unit \(i\) belongs to \(C\) and
\(Y_i=0\) if the unit \(i\) belongs to \(C^*\),
then

\begin{equation*}
\sum_{i=1}^n Y_i = A
\quad\text{and}\quad
\overbar{Y} = \frac{1}{N}\sum_{i=1}^N Y_i = \frac{A}{N} = P
\end{equation*}

Therefore the proportion is simply a mean of indicator values.
Hence, we can use already established sampling properties of the mean
to analyze the proportion.

Also, note that

\begin{equation*}
S^2
= \frac{1}{N-1}\left[\sum_{i=1}^N Y_i^2 - N\overbar{Y}^2\right]
= \frac{1}{N-1}\left[A - NP^2\right]
= \frac{1}{N-1}\left[NP - NP^2\right]
= \frac{N}{N-1}PQ
\end{equation*}

\subsection{Sample proportions}
\label{develop--stat6130:ROOT:page--simple-random.adoc---sample-proportions}

Let

\begin{equation*}
p = \frac{a}{n} = \frac{1}{n} \sum_{i=1}^n y_i
\end{equation*}

Then \(s^2 = \frac{n}{n-1}pq\) where \(q=1-p\).

From previous analysis

\begin{equation*}
E[p] = E[\overbar{y}] = \overbar{Y} = P
\end{equation*}

Also

\begin{description}
\item[SRSWOR] 
    
    \begin{equation*}
    Var(p) = Var(\overbar{y}) = \frac{N-n}{Nn}S^2 = \frac{N-n}{Nn} \frac{N}{N-1}PQ = \frac{N-n}{(N-1)} \frac{PQ}{n}
    \end{equation*}
    
    And an unbiased estimator of the sample proportion variance is
    
    \begin{equation*}
    \hat{Var}(p) = \hat{Var}(\overbar{y})
    = \frac{N-n}{Nn}s^2
    = \frac{N-n}{Nn} \frac{n}{n-1}pq
    \approx \frac{N-n}{Nn} pq
    \end{equation*}
    
    if \(\frac{n}{n-1}\approx 1\)
\item[SRSWR] 
    
    \begin{equation*}
    Var(p) = Var(\overbar{y}) = \frac{N-1}{Nn}S^2 = \frac{N-1}{Nn} \frac{N}{N-1}PQ = \frac{PQ}{n}
    \end{equation*}
    
    And an unbiased estimator of the sample proportion variance is
    
    \begin{equation*}
    \hat{Var}(p) = \hat{Var}(\overbar{y})
    = \frac{1}{n}s^2
    = \frac{1}{n} \frac{n}{n-1}pq
    = \frac{pq}{n-1}
    \end{equation*}
\end{description}

\subsection{Confidence intervals}
\label{develop--stat6130:ROOT:page--simple-random.adoc---confidence-intervals}

Assuming that \(n\) is relatively large,
a confidence interval for \(P\) is given by

\begin{equation*}
p\pm z_{\alpha/2}\sqrt{\hat{Var}(p)}
\end{equation*}

\subsection{Counts}
\label{develop--stat6130:ROOT:page--simple-random.adoc---counts}

Just like proportions, we can model counts using means.
Then

\begin{equation*}
\hat{A} = Np = \frac{N}{n}a
\end{equation*}

Then,

\begin{equation*}
Var(\hat{A}) = N^2Var(p)
\quad\text{and}\quad
\hat{Var}(\hat{A}) = N^2\hat{Var}(p)
\end{equation*}
\end{document}
