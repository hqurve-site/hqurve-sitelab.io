\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Vector functions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
\begin{admonition-note}[{}]
In this section, the functions output real values/vectors.
We may also have functions which output complex values.
\end{admonition-note}

\section{Vector-valued (of a real variable)}
\label{develop--math6120:appendix:page--vector-functions.adoc---vector-valued-of-a-real-variable}

Let \(I \subseteq \mathbb{R}\) be an interval. Then, a mapping \(\vec{\phi} \to \mathbb{R}^n\)
is said to be a \emph{vector valued function of (a real variable)}.
In such a case, we can find \emph{components} \(\phi_1, \ldots \phi_n: I \to \mathbb{R}\) such
that

\begin{equation*}
\forall t \in I: \vec{\phi}(t) = \left(\phi_1(t), \ldots, \phi_n(t)\right)
\end{equation*}

Such a function is continuous iff each of the components are continuous
(this is a topological result).
Differentiation and integration of vector-valued functions are done component-wise.

An important property of integration is that

\begin{equation*}
\left\|\int_c^d \vec{\phi}(t) \ dt \right\| \leq \int_c^d \|\vec{\phi}(t)\| \ dt
\end{equation*}

This property holds for any norm.

\begin{admonition-caution}[{}]
Citation needed.
\end{admonition-caution}

\section{Vector-valued function (of a vector variable)}
\label{develop--math6120:appendix:page--vector-functions.adoc---vector-valued-function-of-a-vector-variable}

Let \(I \subseteq \mathbb{R}\) be an interval and \(E \subseteq \mathbb{R}^n\)
and \(I \times E = D\) (where \(D \subseteq \mathbb{R}^{n+1}\)).
We represent elements in \(D\) by \((t, x_1, \ldots x_n)\)
or \((t, \vec{x})\).

A mapping \(\vec{f}: D \to \mathbb{R}^n\) is said to be a \emph{vector-valued
function (of a vector variable)}. In such a case,
there exists \(f_1, \ldots f_n: D \to \mathbb{R}\) such that

\begin{equation*}
\forall (t,\vec{x}) \in D: \vec{f}(t,\vec{x})
= (f_1(t,\vec{x}), \ldots, f_n(t,\vec{x}))
\end{equation*}

We say that \(\vec{f}\) is continuous if each of its components
\(f_i\) are continuous.

\begin{admonition-thought}[{}]
Is this continuity defined component-wise or is it actual continuity?
For example is \(f_1(t,x_1) = \frac{tx_1}{t^2+x_1^2}\) if \((t,x_1) \neq (0,0)\) otherwise \(0\)
considered continuous?
\end{admonition-thought}

Integration and differentiation are done component-wise.
We perform integration and differentiation wrt \(t\) unless stated otherwise.

\begin{admonition-note}[{}]
The relationship between the dimensionality of the domain and codomain is for our purposes
(well posed systems of differential equations).
In general, the dimension of the domain and codomain may be independent.
\end{admonition-note}

\section{Matrix-valued function (of a single variable)}
\label{develop--math6120:appendix:page--vector-functions.adoc---matrix-valued-function-of-a-single-variable}

Matrix function properties are defined similarly to column vector functions.
That is convergence, continuity, differentiation and integration are defined pointwise.

One nice property is that of norms.
If we use the taxicab norm for matrices and vectors, we obtain that

\begin{equation*}
\|AB\| \leq \|A\|\ \|B\|
\quad\text{and}\quad
\|A\vec{x}\| \leq \|A\|\ \|\vec{x}\|
\end{equation*}

Additionally, we define the exponential function of square matrices as

\begin{equation*}
e^A = E + \sum_{i=1}^\infty \frac{A^n}{n!}
\end{equation*}

where \(E\) is the identity matrix. Note that the above series converges since
square matrices form a Banach space (ie complete). Additionally, if \(A\)
and \(B\) commute, we obtain that

\begin{equation*}
e^{A+B} = e^Ae^B
\end{equation*}

\subsection{Determinants}
\label{develop--math6120:appendix:page--vector-functions.adoc---determinants}

Recall that a determinant can be defined on any commutative ring.
So, we don't need to care about existence when the matrix entries are functions.
The following is a nice result on the derivative of a determinant.

\begin{lemma}[{Derivative of determinant}]
Let \(\phi_{ij}\) be \(n\) differentiable functions
which are entries in the matrix

\begin{equation*}
\Phi(t) = \begin{bmatrix}
\phi_{11}(t) & \phi_{12}(t) & \cdots & \phi_{1n}(t)\\
\phi_{21}(t) & \phi_{22}(t) & \cdots & \phi_{2n}(t)\\
\vdots & & \ddots & \\
\phi_{n1}(t) & \phi_{n2}(t) & \cdots & \phi_{nn}(t)\\
\end{bmatrix}
\end{equation*}

Then

\begin{equation*}
\frac{d}{dt}\det\Phi
=
\begin{vmatrix}
\phi_{11}' & \phi_{12}' & \cdots & \phi_{1n}'\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
+
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21}' & \phi_{22}' & \cdots & \phi_{2n}'\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
+\cdots+
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1}' & \phi_{n2}' & \cdots & \phi_{nn}'\\
\end{vmatrix}
\end{equation*}

\begin{proof}[{}]
Our aim is to use the multivariate chain rule to determine the determinant.
Consider the determinant of a matrix as a multivariate function of \(u_{ij}\).
Then, we can compute the derivative of the determinant relative to each \(u_{ij}\).
as

\begin{equation*}
\begin{aligned}
\frac{\partial \det}{\partial u_{ij}}
&= \lim_{h\to 0}
\frac{1}{h}\left(
\begin{vmatrix}
u_{11} & \cdots & u_{12} & \cdots & u_{1n}\\
\vdots& \\
u_{i1} & \cdots & u_{ij} + h & \cdots & u_{in}\\
\vdots& \\
u_{n1} & \cdots & u_{nj} & \cdots & u_{nn}\\
\end{vmatrix}
-
\begin{vmatrix}
u_{11} & \cdots & u_{12} & \cdots & u_{1n}\\
\vdots& \\
u_{i1} & \cdots & u_{ij} & \cdots & u_{in}\\
\vdots& \\
u_{n1} & \cdots & u_{nj} & \cdots & u_{nn}\\
\end{vmatrix}
\right)
\\&= \lim_{h\to 0}
\frac{1}{h}
\begin{vmatrix}
u_{11} & \cdots & u_{12} & \cdots & u_{1n}\\
\vdots& \\
0 & \cdots & h & \cdots & 0\\
\vdots& \\
u_{n1} & \cdots & u_{nj} & \cdots & u_{nn}\\
\end{vmatrix}
\quad\text{from linearity in each row}
\\&=
\begin{vmatrix}
u_{11} & \cdots & u_{12} & \cdots & u_{1n}\\
\vdots& \\
0 & \cdots & 1 & \cdots & 0\\
\vdots& \\
u_{n1} & \cdots & u_{nj} & \cdots & u_{nn}\\
\end{vmatrix}
\end{aligned}
\end{equation*}

Therefore, by letting the \(u_{ij} = \phi_{ij}(t)\),
the derivative of \(\Phi(t)\) is given by

\begin{equation*}
\frac{d}{dt}\det\Phi(t)
= \sum_{i,j=1}^n \frac{\partial \det}{\partial u_{ij}} \frac{d u_{ij}}{d t}
= \sum_{i,j=1}^n \phi_{ij}'\frac{\partial \det}{\partial u_{ij}}
= \sum_{i=1}^n\sum_{j=1}^n \phi_{ij}'\frac{\partial \det}{\partial u_{ij}}
\end{equation*}

Focus on one fixed \(i\). Then

\begin{equation*}
\begin{aligned}
\sum_{j=1}^n \phi_{ij}'\frac{\partial \det}{\partial u_{ij}}
&= \sum_{j=1}^n \phi_{ij}'
\begin{vmatrix}
u_{11} & \cdots & u_{12} & \cdots & u_{1n}\\
\vdots& \\
0 & \cdots & 1 & \cdots & 0\\
\vdots& \\
u_{n1} & \cdots & u_{nj} & \cdots & u_{nn}\\
\end{vmatrix}
\\&=
\begin{vmatrix}
u_{11} & \cdots & u_{12} & \cdots & u_{1n}\\
\vdots& \\
\phi_{i1}' & \cdots & \phi_{ij}' & \cdots & \phi_{in}'\\
\vdots& \\
u_{n1} & \cdots & u_{nj} & \cdots & u_{nn}\\
\end{vmatrix}
=
\begin{vmatrix}
\phi_{11} & \cdots & \phi_{12} & \cdots & \phi_{1n}\\
\vdots& \\
\phi_{i1}' & \cdots & \phi_{ij}' & \cdots & \phi_{in}'\\
\vdots& \\
\phi_{n1} & \cdots & \phi_{nj} & \cdots & \phi_{nn}\\
\end{vmatrix}
\end{aligned}
\end{equation*}

Therefore, we get the desired result.
\end{proof}
\end{lemma}
\end{document}
