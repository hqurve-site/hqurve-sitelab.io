\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Boundary Value Problems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Boundary conditions are classified as known values of the solution / derivatives at more that one point.
These points need not be the endpoints of the intervals.
A differential equation together with its boundary conditions is called a \emph{boundary value problem}.
Consider the second order differential equation

\begin{equation*}
Lx = a_0(t)x'' + a_1(t) x' + a_2(t) x = g(t)
\end{equation*}

where \(a_0(t) \neq 0\) and \(a_0, a_1, a_2, g\) are continuous.
There are several important forms of boundary conditions:

\begin{description}
\item[Dirichlet / First Kind] \(x(a) = \xi\), \(x(b)=\eta\)
\item[Neumann / Second Kind] \(x'(a) = \xi\), \(x'(b) = \eta\)
\item[Robin / Third Kind / Mixed / Repeated] \(\alpha_1x(a) + \alpha_2x'(a) = \xi\), \(\beta_1x(b) + \beta_2x'(b) = \eta\)
\item[Periodic] \(x(a) = x(b), x'(a)=x'(b)\)
\end{description}

Boundary conditions are usually written terms of linear forms \(B_1\)
and \(B_2\) where

\begin{equation*}
B_1[x] = \alpha_1x(a) + \alpha_2x'(a) + \alpha_3x(b) + \alpha_4x(b) = \xi
\quad\text{and}\quad
B_2[x] = \beta_1x(a) + \beta_2x'(a) + \beta_3x(b) + \beta_4x(b) = \eta
\end{equation*}

where \(B_1\) and \(B_2\) are not identically zero.
There are several classifications for these linear forms

\begin{description}
\item[Homogeneous] If \(B_1[x] = 0\) and \(B_2[x] = 0\), the forms are called homogeneous;
    otherwise, they are called non-homogeneous.
\item[Linear independence] Linear (in)dependence is defined using the coefficients of
    the linear forms.
\end{description}

In the case where the boundary conditions are homogeneous, we call the problem
\(Lx = 0\) a \emph{linear homogenous BVP} and \(Lx = g(t)\) a \emph{linear non-homogenous BVP}.

A linear BVP (homogeneous or non-homogeneous) is called
a \emph{regular boundary value probelm} if \(a\) and \(b\)
are both finite and \(a_0(t) \neq 0\) on \([a,b]\).
Otherwise, the problem is called \emph{singular}.

A boundary value problem which is not linear is called a \emph{non-linear boundary value problem}.
This may arise to do either

\begin{itemize}
\item The differential equation is nonlinear
\item The boundary conditions are not homogeneous
\end{itemize}

It is important to also note that boundary value problems
are much ``harder'' than initial value problems.
There is no guarantee of existence nor uniqueness of solutions.
For example \(x'' + x = 0\) with boundary conditions

\begin{itemize}
\item \(x(0) = 1\) and \(x\left(\frac{\pi}{2}\right)=1\) has a unique solution
\item \(x(0)=1\) and \(x(\pi) = 1\) has no solutions
\item \(x(0) = 1\) and \(x(2\pi)=1\) has infinitely many solutions
\end{itemize}

\section{Sturm-Liouville Problem}
\label{develop--math6120:ROOT:page--boundary-value-problems.adoc---sturm-liouville-problem}

The Sturm-Liouville probelm is a class of BVPs that have many applications.
Of particular interest is the idea of orthogonal functions and series
expansions.

A linear differential equation is called a \emph{Sturm-Liouville differential equation}
if it is of the form

\begin{equation*}
(px')' + qx + \lambda rx =0
\end{equation*}

where \(p,q,r\) are continuous real-valued functions on interval \(I = [a,b]\),
\(p'\) exists and is continuous on \(I\) and \(\lambda\) is a parameter.

\begin{itemize}
\item If we let \(L=\frac{d}{dt}\left(p(t)\frac{d}{dt}\right) + q(t)\),
    the equation may be written as
    
    \begin{equation*}
    Lx + \lambda rx = 0
    \end{equation*}
    
    and \(L\) is called the \emph{S-L operator}.
\item The function \(r(t)\) is called the \emph{weight function} or \emph{density function}
\end{itemize}

The \emph{S-L boundary value problem} is defined as \(Lx + \lambda rx =0\)
together with the mixed boundary conditions

\begin{equation*}
B_1[x] = \alpha_1x(a)+ \alpha_2x'(a) = 0
\quad\text{and}\quad
B_2[x] = \beta_1(b)+ \beta_2x'(b) = 0
\end{equation*}

The value of \(\lambda\) is not specified in the equation and we seek
to find values of \(\lambda\) for which a non-trivial solution exists.
This is called the \emph{Sturm-Liouville Eigenvalue problem} (S-L EVP).
The values of \(\lambda\) for which a non-trivial solution exists
are called \emph{eigenvalues} and the corresponding solutions
\(\phi_\lambda\) are called \emph{eigen functions}.

\begin{admonition-tip}[{}]
We call these values eigenvalues and eigenfunctions since
we can rewrite the differential equation as \(Lx = -\lambda r x\).
\end{admonition-tip}

\begin{itemize}
\item A SL-EVP is called \emph{regular} if \(p > 0\) and \(r > 0\) on \([a,b]\).
    
    \begin{itemize}
    \item If \(p(a) = p(b)\) and the boundary conditions are periodic,
        we call the problem a \emph{periodic SL-EVP}.
    \end{itemize}
\item A SL-EVP is called \emph{singular} if \(p > 0\) on \((a,b)\),
    \(p(a)=p(b)=0\) and \(r\geq 0\).
\end{itemize}

\begin{theorem}[{}]
Let \(a,b\) be finite real functions and \(p,q,r\) be real valued functions
defined on \([ a,b]\) and \(p'\) exist and be continuous on \([ a,b]\).
Then, the number of eigenvalues to the SL-BVP is countably infinite.
\end{theorem}

\begin{admonition-remark}[{}]
The notes state that there are ``countably many eigenvalues''.
It was not clear what this meant since when taken literally, this is equivalent
to stating that there is an eigenvalue. Other interpretations
may conclude that this means that there are at least countably infinite
eigenvalues or at most countably infinite eigenvalues.

A Textbook on Ordinary Differential Equations by Ahmad and Ambrosetti
clarifies this in theorem 9.2.2. They assert that the differential
equation \((px')' + \lambda r t\) (with the necessary boundary conditions)
has eigenvalues which can be ordered such that

\begin{equation*}
0 < \lambda_1 < \lambda_2 < \cdots \to \infty
\end{equation*}

if \(r(t) > 0\).

Although it is not stated, this implies that there are exactly countably infinite many
eigenvalues.

For now, we assume that this previously stated theorem means.
Note that this agrees with the statement in the
\href{https://en.wikipedia.org/wiki/Sturm\%E2\%80\%93Liouville\_theory\#Main\_results}{wikipedia article}.
\end{admonition-remark}

We say that two functions \(\phi_1\) and \(\phi_2\) defined
on interval \([ a,b]\) are \emph{orthogonal} with respect to weight function
\(r(t)\) iff

\begin{equation*}
\int_a^b r(t)\phi_1(t)\phi_2(t) \ dt = 0
\end{equation*}

\begin{theorem}[{Orthogonal Property}]
Let \(\phi_1, \phi_2\) be two eigenfunctions with associated distinct
eigenvalues \(\lambda_1\) and \(\lambda_2\).
Then, \(\phi_1\) and \(\phi_2\) are orthogonal wrt weight function
\(r\) iff

\begin{equation*}
p(b)W(\phi_1, \phi_2)(b) - p(a)W(\phi_1, \phi_2)(a)
= \left[p(t)W(\phi_1, \phi_2)\right]_a^b = 0
\end{equation*}

where \(W(\phi_1, \phi_2)\) is the Wronskian.

\begin{proof}[{}]
Note that since \(\phi_1\) and \(\phi_2\) are solutions, we have that

\begin{equation*}
\begin{aligned}
(p\phi_1')' + q\phi_1 + \lambda_1r\phi_1 &= 0\\
(p\phi_2')' + q\phi_2 + \lambda_2r\phi_2 &= 0\\
\end{aligned}
\end{equation*}

and hence

\begin{equation*}
\begin{aligned}
0
&=\phi_1(p\phi_2')' - \phi_2(p\phi_1')' + (\lambda_2 - \lambda_1)r\phi_1\phi_2
\\&= \frac{d}{dt}\left[\phi_1(p\phi_2') - \phi_2(p\phi_1')\right] + (\lambda_2 - \lambda_1)r\phi_1\phi_2
\\&= \frac{d}{dt}\left[p W(\phi_1, \phi_2)\right] + (\lambda_2 - \lambda_1)r\phi_1\phi_2
\end{aligned}
\end{equation*}

by rearranging and integrating, we obtain that

\begin{equation*}
\int_a^b r(t)\phi_1(t)\phi_2(t) \ dt
= \frac{1}{\lambda_1-\lambda_2}\left[pW(\phi_1, \phi_2)\right]_a^b
\end{equation*}

since \(\lambda_1\neq \lambda_2\), we have the desired result.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(\phi_1\), \(\phi_2\) be two solutions with distinct eigenvalues
to the SL-BVP with continuous
\(p, q, r, p'\) and finite \(a,b\).
Then, \(\phi_1\) and \(\phi_2\) are orthogonal wrt the weight function \(r\).

\begin{proof}[{}]
It is sufficient to prove that

\begin{equation*}
[pW(\phi_1, \phi_2)]_a^b =0
\end{equation*}

From the boundary conditions we have that

\begin{equation*}
\begin{aligned}
\alpha_1 \phi_1(a) + \alpha_2\phi_1'(a) &= 0,\\
\alpha_1 \phi_2(a) + \alpha_2\phi_2'(a) &= 0,\\
\end{aligned}
\quad
\begin{aligned}
\beta_1 \phi_1(b) + \beta_2\phi_1'(b) &= 0,\\
\beta_1 \phi_2(b) + \beta_2\phi_2'(b) &= 0,\\
\end{aligned}
\end{equation*}

and hence

\begin{equation*}
\begin{bmatrix}
\phi_1(a) & \phi_1'(a)\\
\phi_2(a) & \phi_2'(a)\\
\end{bmatrix}
\begin{bmatrix}\alpha_1 \\ \alpha_2\end{bmatrix}
=
\begin{bmatrix}0 \\ 0\end{bmatrix}
,\quad\text{and}\quad
\begin{bmatrix}
\phi_1(b) & \phi_1'(b)\\
\phi_2(b) & \phi_2'(b)\\
\end{bmatrix}
\begin{bmatrix}\beta_1 \\ \beta_2\end{bmatrix}
=
\begin{bmatrix}0 \\ 0\end{bmatrix}
\end{equation*}

Since \((\alpha_1, \alpha_2) \neq (0,0)\) and \((\beta_1, \beta_2)\neq (0,0)\),
we have that the above matrices are singular and hence
their determinants are zero and hence
\(W(\phi_1, \phi_2)(a) = W(\phi_1, \phi_2)(b) = 0\).
Therefore

\begin{equation*}
[p(t)W(\phi_1, \phi_2)]_a^b
= p(b)W(\phi_1, \phi_2)(b) - p(a)W(\phi_1, \phi_2)(a)
= 0
\end{equation*}

and hence \(\phi_1\) and \(\phi_2\) are orthogonal with respect to \(r(t)\).
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Consider the SL-BVP with continuous
\(p, q, r, p'\) and finite \(a,b\).
Then all the eigenvalues are real if \(r \neq 0\) on \([ a , b ]\).

\begin{proof}[{}]
Let \(\lambda = c+id\) be an eigenvalue with eigenfunction
\(\phi(t) = m(t) + in(t)\).
Then,

\begin{equation*}
\begin{aligned}
0
&=(p\phi')'+q\phi+\lambda r\phi
\\&=(p m' + ipn')'+qm + iqn + r (cm-dn) + ir(cn + dm)
\\&= [(p m')' + (q + r c)m - rdn] + i [(pn')' + (q + rc)n + rdm]
\end{aligned}
\end{equation*}

Then, both real and imaginary parts are equal to zero and hence
by considering \(m\Im - n\Re\)

\begin{equation*}
\frac{d}{dt}[p(mn'-m'n)] = m(pn')' - n(pm')' = -rdm^2 - rdn^2 = -dr(m^2+n^2)
\end{equation*}

and hence

\begin{equation*}
-d\int_a^b [m(t)^2 + n(t)^2] r(t) \ dt
= [p(mn'-m'n)]_a^b = 0
\end{equation*}

since \(m,n\) satisfy the boundary conditions.
Now, since \(r \neq 0\) and continuous, it is either strictly positive or strictly
negative. In both cases we obtain that \(\int_a^b [m(t)^2 + n(t)^2]r(t) \ dt \neq 0\)
since \(m(t)^2 + n(t)^2\) is non zero at least on some small interval
(since \(\phi\) is not identically zero).
Therefore, \(d=0\) and \(\lambda\) is real.
\end{proof}
\end{theorem}

\begin{theorem}[{Eigenfunction expansion}]
Consider the SL-BVP with continuous
\(p, q, r, p'\) and finite \(a,b\). Also let \(r \neq 0\).

Let \(g(t)\) be a piecewise continuous function defined on \([ a, b ]\)
which satisfies the boundary conditions
(not necessarily a solution to the differential equation).
Then, there exists constants \(c_1, c_2, \ldots \) such that

\begin{equation*}
g(t) = c_1\phi_1(t) + c_2\phi_2(t) + \cdots
\end{equation*}

where

\begin{equation*}
c_n = \frac{ \int_a^b r(t)g(t)\phi_n(t) \ dt }{ \int_a^b r(t)\phi_n^2(t) \ dt}
\end{equation*}
\end{theorem}

Note that this theorem asserts several common series expansions.
Primarily Fourier series with the differential equation
\(x'' + \lambda x=0\) and periodic boundary conditions.

\section{Green's function}
\label{develop--math6120:ROOT:page--boundary-value-problems.adoc---greens-function}

Consider the second order non-homogenous linear differential equation

\begin{equation*}
L(x) = a_0(t) x'' + a_1(t)x' + a_2(t)x = g(t)
\end{equation*}

where \(a_0, a_1, a_2, g\) are continuous and \(a_0(t) \neq 0\)
on \([ a, b ]\).
Also consider the mixed boundary conditions

\begin{equation*}
\begin{aligned}
B_1[x] = \alpha_1x(a) + \alpha_2x'(a) &= 0\\
B_2[x] = \beta_1x(b) + \beta_2x'(b) &= 0\\
\end{aligned}
\end{equation*}

A function \(G(t,s)\) on \([ a, b ]\times[ a, b ]\)
is called a Green's function for \(L(x) = 0\)
if

\begin{equation*}
G(t,s) = \begin{cases}
G_1(t,s), \quad&\text{if} \ a\leq t \leq s\\
G_2(t,s), \quad&\text{if} \ s\leq t \leq b
\end{cases}
\end{equation*}

where
\(G_1, G_2\) satisfy the following

\begin{enumerate}[label=\arabic*)]
\item \(G_1\) satisfies \(L(G_1) = 0\) for \(a\leq t < s\) and satisfies the
    boundary condition at \(t=a\). Note this also implies
    
    \begin{itemize}
    \item \(G_1\) is continuous as a function on \(t\) on \(a \leq t \leq s\)
    \item \(\frac{\partial G_1}{\partial t}\) exists and is continuous on \(a \leq t < s\) and is discontinuous at \(t=s\)
    \end{itemize}
\item \(G_2\) satisfies \(L(G_2) = 0\) for \(t < s \leq b\) and satisfies the
    boundary condition at \(t=b\). Note this also implies
    
    \begin{itemize}
    \item \(G_2\) is continuous as a function on \(t\) on \(s \leq t \leq b\)
    \item \(\frac{\partial G_2}{\partial t}\) exists and is continuous on \(s < t \leq b\) and is discontinuous at \(t=s\)
    \end{itemize}
\item \(G(t,s)\) is continuous at \(t=s\)
\item \(\frac{\partial G}{\partial t}\) has a jump discontinuity at \(t=s\) and
    
    \begin{equation*}
    \left[\frac{\partial G_1}{\partial t} - \frac{\partial G_2}{\partial t}\right]_{t=s}
    = - \frac{1}{a_0(s)}
    \end{equation*}
\end{enumerate}

Since the greens function is a solution of \(L(x)=0\), it can be expressed
as a linear combination of a basis for the solution \(L(x)=0\).
Therefore to find the Green's function, we write \(G(t,s)\) as a linear
combination of the basis of the solution and impose the stated conditions.

\begin{proposition}[{Form of Green's function}]
Let \(x_1, x_2\) be linearly independent solutions to \(L(x)=0\).
Then, the following is a Green's function

\begin{equation*}
G(t,s) = \begin{cases}
\frac{x_1(t)x_2(s)}{a_0(s)W(s)}, \quad&\text{if } a\leq t\leq s\\
\frac{x_2(t)x_1(s)}{a_0(s)W(s)}, \quad&\text{if } s\leq t\leq b\\
\end{cases}
\end{equation*}

where \(W(s)\) is the Wronskian of \(x_1\) and \(x_2\).

\begin{proof}[{}]
We would prove that the above is the only Green's function of the form

\begin{equation*}
G(t,s) = \begin{cases}
c_1(s)x_1(t), \quad&\text{if } a\leq t\leq s\\
c_2(s)x_2(t), \quad&\text{if } s\leq t\leq b\\
\end{cases}
\end{equation*}

From continuity at \(t=s\), we get that

\begin{equation*}
c_1(s)x_1(s) = c_2(s)x_2(s)
\end{equation*}

and from the jump discontinuity

\begin{equation*}
c_1(s)x_1'(s) -c_2(s)x_2'(s) = -\frac{1}{a_0(s)}
\end{equation*}

by solving the system for \(c_1\) and \(c_2\) we get the desired form.
\end{proof}

\begin{admonition-remark}[{}]
I think this result also requires that \(x_1\) satisfies the boundary
condition at \(t=a\) and \(x_2\) satisfies the boundary condition at \(t=b\).
\end{admonition-remark}
\end{proposition}

\begin{theorem}[{}]
Consider the second order differential equation

\begin{equation*}
L(x) = a_0(t) x'' + a_1(t)x' + a_2(t) x = g(t)
\end{equation*}

Let \(t_0\) be fixed and \(t\) be any point in \([ a, b ]\) with \(t > t_0\).
Let \(x_1(t)\) and \(x_2(t)\) be two linearly independent solutions
to the above differential equation and let

\begin{equation*}
G(t,s) = \frac{1}{a_0(s) W(x_1, x_2)(s)}\begin{vmatrix}
x_1(s) & x_2(s)\\
x_1(t) & x_2(t)\\
\end{vmatrix}
\end{equation*}

Then, the following is a particular solution to the above differential equation

\begin{equation*}
x_p(t) = \int_{t_0}^t G(t,s) \ g(s) \ ds
\end{equation*}

with \(x_p(t_0)=x_p'(t_0)=0\)

\begin{admonition-caution}[{}]
The notes also specify that \(G(t,s)\) is a Green's function.
But this does not make sense since the derivative of the above function
is continuous with respect to \(t\).
\end{admonition-caution}

\begin{proof}[{}]
By applying variation of parameters, we want a solution of the form

\begin{equation*}
x_p(t) = u_1(t)x_1(t) + u_2(t)x_2(t)
\end{equation*}

Then

\begin{equation*}
u_1(t)
= \int_{t_0}^t \frac{\begin{vmatrix}0 & x_2(s)\\ \frac{g(s)}{a_0(s)} & x_2'(s)\end{vmatrix}}{W(x_1, x_2)(s)} \ ds
= \int_{t_0}^t \frac{-x_2(s)g(s)}{a_0(s)W(x_1, x_2)(s)} \ ds
\end{equation*}

and

\begin{equation*}
u_2(t)
= \int_{t_0}^t \frac{\begin{vmatrix}x_1(s) & 0\\ x_1'(s) & \frac{g(s)}{a_0(s)}\end{vmatrix}}{W(x_1, x_2)(s)} \ ds
= \int_{t_0}^t \frac{x_1(s)g(s)}{a_0(s)W(x_1, x_2)(s)} \ ds
\end{equation*}

Therefore

\begin{equation*}
\begin{aligned}
x_p(t)
&= u_1(t)x_1(t) + u_2(t)x_2(t)
\\
&= \int_{t_0}^t \frac{g(s) [-x_2(s)x_1(t) + x_1(s)x_2(t)]}{a_0(s)W(x_1, x_2)(s)} \ ds
\\
&= \int_{t_0}^t \frac{g(s) \begin{vmatrix}x_1(s) & x_2(s)\\ x_1(t) & x_2(t)\end{vmatrix}}{a_0(s)W(x_1, x_2)(s)} \ ds
\\
&= \int_{t_0}^t G(t,s)g(s) \ ds
\end{aligned}
\end{equation*}

as desired.

Note that \(x_p(t_0)=0\) and

\begin{equation*}
x_p'(t) = G(t,t)g(t) + \int_{t_0}^tG_t(t,s)g(s)\ ds
\end{equation*}

and hence \(x_p'(t_0)=0\) since \(G(t_0, t_0)=0\) from definition of \(G(t,s)\).
\end{proof}
\end{theorem}

\begin{theorem}[{}]
If \(G(t,s)\) is a Green's function for the linear homogeneous boundary value problem

\begin{equation*}
L(x) = 0, \ B_1[x] = B_2[x]= 0
\end{equation*}

Then there exists a unique solution to the non-homogeneous BVP

\begin{equation*}
L(x) = g(t), \ B_1[x] = B_2[x]= 0
\end{equation*}

given by

\begin{equation*}
x(t) = \int_a^b G(t,s) g(s) \ ds
\end{equation*}

\begin{proof}[{}]
Suppose \(x(t)\) has the desired form.
Then we may write it as

\begin{equation*}
x(t) = \int_a^t G_2(t,s) g(s) \ ds + \int_t^b G_1(t,s) g(s) \ ds
\end{equation*}

Note that

\begin{equation*}
\begin{aligned}
x'(t)
&= G_2(t,t)g(t) - G_1(t,t)g(t) + \int_a^t \frac{\partial G_2}{\partial x}(t,s) g(s) \ ds + \int_t^b \frac{\partial G_1}{\partial x}(t,s) \ g(s) \ ds
\\&= \int_a^t \frac{\partial G_2}{\partial x}(t,s) g(s) \ ds + \int_t^b \frac{\partial G_1}{\partial x}(t,s) \ g(s) \ ds
\end{aligned}
\end{equation*}

since \(G\) is continuous at \(t=s\).
And

\begin{equation*}
\begin{aligned}
x''(t)
&= \frac{\partial G_2}{\partial x}(t,t)g(t) - \frac{\partial G_1}{\partial x}(t,t)g(t)
+ \int_a^t \frac{\partial^2 G_2}{\partial x^2}(t,s) g(s) \ ds + \int_t^b \frac{\partial^2 G_1}{\partial x^2}(t,s) \ g(s) \ ds
\\&= \frac{g(t)}{a_0(t)}
+ \int_a^t \frac{\partial^2 G_2}{\partial x^2}(t,s) g(s) \ ds + \int_t^b \frac{\partial^2 G_1}{\partial x^2}(t,s) \ g(s) \ ds
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
L[x] = a_0(t)x''(t) + a_1(t)x'(t) + a_2x(t) = g(t)
\end{equation*}

as desired.
Also, for each boundary condition,

\begin{equation*}
B_i[x] =\int_a^b B_i[G](s) g(s) \ ds = \int_a^b 0 g(s) \ ds = 0
\end{equation*}

Therefore \(x\) satisfies the BVP.

\begin{admonition-caution}[{}]
I am not sure how to prove uniqueness. The general
idea is to prove that the solution \(x(t)\) satisfies
\(x(t) = \int_a^b G(t,s) g(s) \ ds\).
\end{admonition-caution}
\end{proof}
\end{theorem}
\end{document}
