\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Kermack-McKendric SIR Epidemic Model}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
This is a compartmental model where there are three groups

\begin{description}
\item[Susceptible (S)] Individuals who are healthy but can potentially
    contract the disease
\item[Infected (I)] Individuals who are sick with the disease and are assumed
    to also be infectious
\item[Recovered/Removed (R)] Individuals who can no longer be infected by the disease
    and are not infectious. This can either be interpreted as dead
    or simply recovered
\end{description}

Let \(N\) be the population size.
Then,

\begin{equation*}
N = S(t) + I(t) + R(t)
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/epidemic-SIR}
\caption{Flow diagram for SIR model}
\end{figure}

\section{Assumptions}
\label{develop--math6192:ROOT:page--epidemic-models/sir.adoc---assumptions}

We assume that

\begin{itemize}
\item The total population size, \(N\), is constant. No births and no deaths
\item All infected individuals are infectious
\item A susceptible individual (from class \(S\)) becomes
    infected when in contact with an infectious individual with probability \(p\).
\item The number of contacts made by one infectious individual is directly
    proportional to the total population size \(N\) with
    per capita \textbf{contact rate} \(c\).
\item The probability of an infected individual to ``recover'' in unit time is \(\alpha\)
\item All recovered individuals are completely immune and cannot become infected again.
\end{itemize}

\section{Governing equation}
\label{develop--math6192:ROOT:page--epidemic-models/sir.adoc---governing-equation}

Since the number of contacts made by an infected individual per unit time is \(cN\),
and the probability that contact is made with a susceptible individual is \(\frac{S}{N}\),
the number of contacts with susceptible individuals is \(cS\).
Also, since the probability of becoming infected is \(p\), the number of susceptible individuals
which become infected by the infected individual is \(cpS\).
We let \(\beta = pc\) be the \textbf{transmittion rate constant}.
Therefore, we have that

\begin{equation*}
incidence = (cpS)I = \beta IS = \lambda S
\end{equation*}

where \(\lambda(t) = \beta I\) is called the \textbf{force of infection}.
Now, since the infection is the only process causing the susceptible class
to change, we have that

\begin{equation*}
\frac{dS}{dt} = -incidence = -\beta IS
\end{equation*}

Next, since the probability of recovery is \(\alpha\), we must have

\begin{equation*}
\frac{dR}{dt} = \alpha I
\end{equation*}

Finally, we can easily see that the infected class changes according to

\begin{equation*}
\frac{dI}{dt} = \beta IS - \alpha I
\end{equation*}

So, our governing equations are

\begin{equation*}
\begin{aligned}
S' &= -\beta IS\\
I' &= \beta IS -\alpha I\\
R' &= \alpha I\\
\end{aligned}
\end{equation*}

Subject to some initial conditions \(S(0)=S_0\), \(I(0)=I_0\) and \(R(0)=0\).
Note that the above operator must satisfy the Lipschitz condition
since the above is continuously differentiable with bounded derivatives
since \(0 \leq S,I,R\leq N\).
Therefore, we have existence and uniqueness of solutions and the problem
is well posed.

Also, note that there is an epidemic iff \(I'(0) > 0\) since \(I_\infty = 0\).
This occurs iff \(\beta S(0) - \alpha > 0\)

\section{Limiting behaviour}
\label{develop--math6192:ROOT:page--epidemic-models/sir.adoc---limiting-behaviour}

We are interested in the limiting behaviour of the compartments.
For example, we may want to know

\begin{itemize}
\item Whether the disease remains endemic \(\lim_{t\to \infty} I(t) = I_{\infty} > 0\)
\item The number of individuals which remain susceptible \(\lim_{t\to \infty} S(t) = S_{\infty}\)
\end{itemize}

\begin{proposition}[{Susceptible class remains positive}]
In the SIR model

\begin{equation*}
S(t) = S(0) e^{-(\beta/\alpha) R(t)}
\end{equation*}

and hence \(S_{\infty} > 0\)

\begin{proof}[{}]
Note that

\begin{equation*}
\frac{S'}{R'} = \frac{-\beta I S}{\alpha I} = -\frac{\beta}{\alpha} S
\implies \frac{S'}{S} = -\frac{\beta}{\alpha} R
\implies S(t) = S(0)e^{-(\beta/\alpha) R(t)}
\end{equation*}

since \(R(0)=0\).
Also, note

\begin{equation*}
S(t)
= S(0)e^{-(\beta/\alpha) R(t)}
\geq S(0)e^{-(\beta/\alpha) N}
> 0
\end{equation*}

Therefore, we have a positive lower bound for \(S(t)\) and \(S_\infty > 0\).
\end{proof}
\end{proposition}

\begin{proposition}[{Infected class tends to zero}]
\begin{equation*}
\lim_{t\to\infty} I(t) =0
\end{equation*}

and hence the SIR model cannot be used to model endemic diseases.

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
S_\infty - S_0
&= \int_0^\infty S'(t) \ dt
\\&= \int_0^\infty -\beta I(t) S(t) \ dt
\\&= -\beta \int_0^\infty I(t) S(t) \ dt
\\&\geq -\beta  \int_0^\infty I(t) S_\infty \ dt
\\&= -\beta S_\infty \int_0^\infty I(t) \ dt
\end{aligned}
\end{equation*}

since \(S_\infty \leq S(t)\).
Since the integral on the right is finite and \(I(t) \geq 0\),
we must have that \(I(t) \to 0\).
\end{proof}
\end{proposition}

\section{Estimating parameters}
\label{develop--math6192:ROOT:page--epidemic-models/sir.adoc---estimating-parameters}

Consider the following

\begin{equation*}
\frac{I'}{S'} = \frac{\beta IS - \alpha I}{-\beta IS} = -1 + \frac{\alpha}{\beta S}
\implies I' = \left(-1 + \frac{\alpha}{\beta S}\right) S'
\end{equation*}

Therefore,

\begin{equation*}
I = -S + \frac{\alpha}{\beta} \ln S + \left(I_0 + S_0 - \frac{\alpha}{\beta}S_0\right)
\end{equation*}

Now, by using the limiting behaviour, we must have that

\begin{equation*}
0 = I_\infty
= -S_\infty + \frac{\alpha}{\beta} \ln S\infty + \left(I_0 + S_0 - \frac{\alpha}{\beta}S_0\right)
\end{equation*}

and hence

\begin{equation*}
\frac{\beta}{\alpha} = \frac{\ln S_0 - \ln S_\infty}{I_0 + S_0 -S_\infty}
\end{equation*}

Next, we can estimate \(\alpha\) by using the fact that we assume
that a patient recovers with probability \(\alpha\) per unit item.
Therefore, the distribution of the patient recovery time
is an exponential distribution with mean \(\frac{1}{\alpha}\).
Therefore, we can estimate \(\alpha\) using the reciprocal of the mean recovery time.

Finally, note that \(I\) attains a maximum
at \(S(t) = \frac{\alpha}{\beta}\) with value

\begin{equation*}
I_{\max} = -\frac{\alpha}{\beta} + \frac{\alpha}{\beta} \ln \frac\alpha\beta
+ I_0 + S_0 - \frac\alpha\beta \ln S_0
\end{equation*}

\begin{admonition-important}[{}]
This maximum only occurs if \(S_0 > \frac\alpha\beta\).
Well ... technically a maximum still occurs when \(S_0 < \frac\alpha\beta\)
but it is when \(t=0\); so, it is not interesting.
\end{admonition-important}

\section{Basic repoduction number}
\label{develop--math6192:ROOT:page--epidemic-models/sir.adoc---basic-repoduction-number}

Note that we often needed to consider the sign of \(S_0 - \frac{\alpha}{\beta}\).
This suggests that we define

\begin{equation*}
R_0 = \frac{\beta S(0)}{\alpha}
\end{equation*}

We found that \(R_0\) is the threshold value for an epidemic and

\begin{itemize}
\item If \(R_0 < 1\), there is no epidemic
\item If \(R_0 > 1\), there is an epidemic
\end{itemize}

We call \(R_0\) the \textbf{basic repoduction number}
and it represents the number of secondary infections caused by
a single infected individual being introduced into a susceptible population.

\begin{admonition-caution}[{}]
\(R_0\) is not \(R(0)=0\).
\end{admonition-caution}
\end{document}
