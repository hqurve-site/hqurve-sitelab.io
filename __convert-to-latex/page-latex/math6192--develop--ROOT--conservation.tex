\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Conservation of energy}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Newton's second law states that

\begin{equation*}
\numberandlabel{eq:newton2}
\vec{F} = \frac{d\vec{\rho}}{dt}
\end{equation*}

where \(\vec{\rho} = m\frac{d\vec{r}}{dt}\) is the momentum.

\section{Particles}
\label{develop--math6192:ROOT:page--conservation.adoc---particles}

Suppose that the system consists of particles (\(i\in I\))
with (fixed) masses \(m_i\). We will look at the forces between particles
and their respective changes in momentums.

\subsection{Zero net-force}
\label{develop--math6192:ROOT:page--conservation.adoc---zero-net-force}

Newton's second \(\eqref{eq:newton2}\) law states
that the change of momentum applied to \(i\)
is the sum of forces imparted on \(i\) by each other \(j\in I\).
That is

\begin{equation*}
\frac{d\vec{\rho_i}}{dt} = \sum_{j \in I}\vec{F_{ji}}
\end{equation*}

So, the total change in momentum in a closed system is therefore

\begin{equation*}
\sum_{i \in I} \frac{d\vec{\rho_i}}{dt} = \sum_{i,j \in I} \vec{F_{ji}} = 0
\end{equation*}

since each by Newton's third law \(\vec{F_{ij}} = - \vec{F_{ji}}\).

\subsection{One big particle}
\label{develop--math6192:ROOT:page--conservation.adoc---one-big-particle}

Now, define \(\vec{P} = \sum_{i\in I} \vec{\rho_i}\).
Since \(\frac{d\vec{P}}{dt} = 0\),

\begin{equation*}
\vec{P} = \sum_{i\in I} m_i\vec{v_i} = \text{constant} = M\vec{V}
\end{equation*}

where we define

\begin{equation*}
M = \sum_{i \in I} m_i,
\quad\text{and}\quad
\vec{V}
= \frac{\vec{P}}{M}
= \frac{\sum_{i\in I}m_i \vec{v_i}}{\sum_{i\in I} m_i}
\end{equation*}

We refer to \(\vec{V}\) as the \emph{moving frame velocity}
and we may consider the system as one ``big'' particle.
If we look at the momentum relative to this big particle, it is not hard to see that
it is zero. Hence the moving frame velocity is constant.

Additionally, we may define

\begin{equation*}
\vec{R} = \frac{\sum_{i\in I} m_i \vec{r_i}}{\sum_{i \in I}m_i}
\end{equation*}

as the \emph{center of mass} of the particles. It can be proven that \(\frac{d\vec{R}}{dt} = \vec{V}\)
(ie our definition of \(\vec{R}\) is consistent with our definition of \(V\)).

\subsection{Work done}
\label{develop--math6192:ROOT:page--conservation.adoc---work-done}

\begin{admonition-note}[{}]
\(\vec{v}^2 = \vec{v}\cdot\vec{v}\) (for convenience)
\end{admonition-note}

We define the work done as

\begin{equation*}
\int_{\text{point 1}}^{\text{point 2}} \vec{F}\cdot d\vec{r}
\end{equation*}

Since \(\vec{F} = m\frac{d\vec{v}}{dt}\), we have that

\begin{equation*}
\begin{aligned}
\int_{\text{point 1}}^{\text{point 2}} \vec{F}\cdot d\vec{r}
&=\int_{\text{point 1}}^{\text{point 2}} m\frac{d\vec{v}}{dt}\cdot d\vec{r}
\\&=\int_{\text{point 1}}^{\text{point 2}} m\frac{d\vec{v}}{dt}\cdot \frac{d\vec{r}}{dt} \ dt
\\&=\int_{\text{point 1}}^{\text{point 2}} m\frac{d\vec{r}}{dt}\cdot \frac{d\vec{v}}{dt} \ dt
\\&=\int_{\text{point 1}}^{\text{point 2}} m\vec{v}\cdot \frac{d\vec{v}}{dt} \ dt
\\&= \frac{m\vec{v}^2_{\text{point 2}}}{2} - \frac{m\vec{v}^2_{\text{point 1}}}{2}
\end{aligned}
\end{equation*}

Since we define kinetic energy as \(\frac{m\vec{v}^2}{2}\),
we see that the work done is just the change in kinetic energy between the initial and final points.

Also, it is important to notice that the work only depends on the initial and final positions.
In such a case, \(\vec{F}\) is conservative with \(\vec{F} = - \nabla U\) for some potential field \(U\)
and hence \(\vec{F}\) is irrotational (\(\nabla \times \vec{F} = \vec{0}\)).
We also call \(\vec{F}\) a \emph{potential force}.
Furthermore, by Stokes' theorem, we have that

\begin{equation*}
W = \oint \vec{F}\cdot d\vec{r} = 0 = \iint_S \nabla \times \vec{F} \ dS
\end{equation*}

and hence we can obtain that

\begin{equation*}
U_{\text{point 1}} + E_{\text{point 1}}
=
U_{\text{point 2}} + E_{\text{point 2}}
\end{equation*}

Ie, the principle of conservation of energy for a closed potential system.

\section{Example 1: Escape Velocity}
\label{develop--math6192:ROOT:page--conservation.adoc---example-1-escape-velocity}

Consider a two body system which interacts with Gravitational forces.
We have a planet of mass \(M\) and a (possibly orbiting) object of mass \(m\).
First, we consider the work required to change the radius of the orbit
from \(r_1\) to \(r_2\).
Below \(\vec{r}\) represents the displacement of the object relative to the planet.

\begin{equation*}
W
= \int_{r_1}^{r_2} \vec{F}\cdot d\vec{r}
= \int_{r_1}^{r_2} \frac{GMm}{r^3} (-\vec{r})\cdot d\vec{r}
= \int_{r_1}^{r_2} -\frac{GMm}{r^2}\cdot dr
= GMm \left(\frac{1}{r_2} - \frac{1}{r_1}\right)
\end{equation*}

So, we can define the potential energy as \(U = -\frac{GMm}{r}\).

So the total energy at any point in time at radius \(R\) is

\begin{equation*}
\text{Total energy} = \frac{m\vec{v}^2}{2} - \frac{GMm}{R} = \text{constant}
\end{equation*}

\subsection{Orbit velocity}
\label{develop--math6192:ROOT:page--conservation.adoc---orbit-velocity}

Suppose we are orbiting in a circle with radius \(R\). At what velocity should the object be moving?

When in orbit, the gravitational force should cancel the
\href{https://en.wikipedia.org/wiki/Centripetal\_force}{centripetal force}
so that there is no effective force on the object.
So,

\begin{equation*}
\frac{GMm}{R^2} = \frac{mv^2}{R}
\end{equation*}

So, we obtain that

\begin{equation*}
v = \sqrt{\frac{GM}{R}}
\end{equation*}

\subsection{Deriving orbit velocity in a different way.}
\label{develop--math6192:ROOT:page--conservation.adoc---deriving-orbit-velocity-in-a-different-way}

Instead of using the centripetal force, we can derive the orbit velocity using concepts we already know.
Let \(\Delta t = \delta\) be a small time step so that

\begin{itemize}
\item the forces acting on the object are constant. This is valid since the gravitational force is a continuous function
    of position.
\item the changes in position are additive. This is valid since the derivative approximates a linear function.
\end{itemize}

Let \(\vec{r}\) be the original displacement of the object and \(\vec{v}\) be the velocity of the object.
Since the object is in orbit \(\vec{v}\perp \vec{r}\). Also, we know that the acceleration due to gravity
on the object is \(\frac{\vec{F}}{m} = -\frac{GM}{r^3}\vec{r}\).
So, after \(\Delta t = \delta\), the new position of the object is given by

\begin{equation*}
\vec{r} + \delta\vec{v} - \frac{\delta^2}{2}\frac{GM}{r^3}\vec{r}
\end{equation*}

We want that this new position is the same distance from the planet as \(\vec{r}\). So,

\begin{equation*}
\begin{aligned}
&\|\vec{r}\|^2 = \left\|\vec{r} + \delta\vec{v} - \frac{\delta^2}{2}\frac{GM}{r^3}\vec{r}\right\|^2
\\&\implies r^2
= r^2 + \delta^2v^2 + \frac{\delta^4}{4}\left(\frac{GM}{r^2}\right)^2
+ 2\vec{r}\vec{v} - \delta^2\frac{GM}{r^3}r^2 - \delta^3\frac{GM}{r^3}\vec{v}\vec{r}
\\&\implies 0
=  \delta^2v^2 + \frac{\delta^4}{4}\left(\frac{GM}{r^2}\right)^2-\delta^2\frac{GM}{r}
\\&\implies 0
=  v^2 + \frac{\delta^2}{4}\left(\frac{GM}{r^2}\right)^2- \frac{GM}{r}
\end{aligned}
\end{equation*}

So, as \(\delta \to 0\), we have the desired result.

\begin{admonition-note}[{}]
\(R = r = \|\vec{r}\|\)
\end{admonition-note}

\subsection{Escape velocity}
\label{develop--math6192:ROOT:page--conservation.adoc---escape-velocity}

For escape velocity, we can simply compute the minimum energy required at the initial.
Consider the energy at the final position (ie \(R\to \infty\)).
Ideally, we should have no extra kinetic energy and it is clear that the potential energy tends to zero.
So, by the principle of conservation of energy, we have that

\begin{equation*}
\frac{m\vec{v}_0^2}{2} - \frac{GMm}{R} = \text{Initial energy} = \text{Limiting energy} = 0
\end{equation*}

and hence we obtain that the minimum initial energy is given by

\begin{equation*}
v_0 = \sqrt{\frac{2GM}{R}} = \sqrt{2R\frac{GM}{R^2}} = \sqrt{2Rg}
\end{equation*}

where \(g\) is the acceleration due to gravity at radius \(R\).
If we start at the ground, we can use the common value for \(g=9.81m/s^2\).

\section{Example 2: Oscillation Period of a Pendulum}
\label{develop--math6192:ROOT:page--conservation.adoc---example-2-oscillation-period-of-a-pendulum}

Consider a pendulum of length \(L\) and mass \(m\) at angle \(\theta\)
to the vertical. We want to find the period of oscillation.

The velocity of the mass can be simply given by \(v = L\frac{d\theta}{dt}\)
by using the arc length formula. So the kinetic energy is given by

\begin{equation*}
E_k = \frac{m\left(L\frac{d\theta}{dt}\right)}{2} = \frac{mL^2 \left(\frac{d\theta}{dt}\right)}{2}
\end{equation*}

We also assume that the force due to gravity which acts on the mass is constant and the magnitude acceleration
is given by \(g\). So the gravitational potential energy relative to the pivot of the pendulum
is given by

\begin{equation*}
U = -mgL\cos\theta
\end{equation*}

Let us consider the pendulum at the point of release (\(\theta_0\)).
At this point, all of the energy is potential.
So, by the principle of conservation of energy

\begin{equation*}
-mgL\cos\theta_0 = -mgL\cos\theta + \frac{mL^2}{2} \left(\frac{d\theta}{dt}\right)^2
\end{equation*}

after rearranging, we obtain that

\begin{equation*}
\int \frac{d\theta}{2\frac{g}{L}(\cos\theta - \cos\theta_0)} = \int dt = t+C
\end{equation*}

By letting the period of oscillation be \(T\), we have that

\begin{equation*}
T = 4\int_0^{\theta_0} \frac{d\theta}{\sqrt{2\frac{g}{L}(\cos\theta - \cos\theta_0)}}
\end{equation*}

since we can split the swing into 4 parts which must each take the same amount of time

\begin{itemize}
\item fall from \(\theta_0\) to \(0\)
\item rise from \(0\) to \(-\theta_0\)
\item fall from \(-\theta_0\) to \(0\)
\item rise from \(0\) to \(\theta_0\)
\end{itemize}

By rearranging, we obtain

\begin{equation*}
T = 4\sqrt{\frac{L}{g}} K \left(\sin \left(\frac{\theta_0}{2}\right)\right)
\quad\text{where}\quad
K(s) = \int_0^{\pi/2} \frac{d\xi}{\sqrt{1-s^2\sin^2\xi}}
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
T
&= 4\sqrt{\frac{L}{g}}\int_0^{\theta_0} \frac{d\theta}{\sqrt{2(\cos\theta - \cos\theta_0)}}
\\&= 4\sqrt{\frac{L}{g}}\int_0^{\theta_0} \frac{d\theta}{\sqrt{4\sin^2 \frac{\theta_0}{2} - 4\sin^2\frac{\theta}{2}}}
\quad\text{by double angle formulae}
\\&= 4\sqrt{\frac{L}{g}}\int_0^{\theta_0} \frac{d\theta}{\sqrt{4\sin^2 \frac{\theta_0}{2} - 4\sin^2\frac{\theta}{2}}}
\end{aligned}
\end{equation*}

By using the substitution \( s\sin \xi = \sin\frac{\theta}{2} \)
where \(s = \sin\frac{\theta_0}{2}\), we have that

\begin{equation*}
s \cos\xi = \frac{1}{2}\frac{d\theta}{d\xi} \cos\frac{\theta}{2}
= \frac{1}{2}\frac{d\theta}{d\xi}\sqrt{1-\sin^2\frac{\theta}{2}}
= \frac{1}{2}\frac{d\theta}{d\xi}\sqrt{1-s^2\sin^2\xi}
\end{equation*}

and hence

\begin{equation*}
\begin{aligned}
T
&= 4\sqrt{\frac{L}{g}}\int_0^{\pi/2}  \frac{2\frac{s\cos\xi}{1-s^2\sin^2\xi} \ d\xi}{\sqrt{4s^2 - 4s^2\sin^2\xi}}
\\&= 4\sqrt{\frac{L}{g}}\int_0^{\pi/2}  \frac{2\frac{s\cos\xi}{1-s^2\sin^2\xi} \ d\xi}{2s\cos\xi}
\\&= 4\sqrt{\frac{L}{g}}\int_0^{\pi/2}  \frac{d\xi}{1-s^2\sin^2\xi}
\end{aligned}
\end{equation*}
\end{proof}

\section{Example 3: Spring collision}
\label{develop--math6192:ROOT:page--conservation.adoc---example-3-spring-collision}

We have mass \(m_1\) which is attached to a spring (with spring constant \(k\)) and a surface (initially at rest).
We also have mass \(m_2\) which is moving with velocity \(\vec{v}\).
If the second mass hits the surface and continues with the same velocity as the surface,
what is the deformation of the spring.

Note that we have many assumptions

\begin{itemize}
\item We assume that the combined body is moving with the same velocity
\item We assume that there is no oscillation of the spring
\item We assume that the spring does not deflect
\item All interactions are happening in the same one-dimensional space.
\item Many more
\end{itemize}

First we determine the combined velocity \(V\) by using conservation of momentum

\begin{equation*}
m_2v = m_1V + m_2V \quad\implies \quad V = \frac{m_2}{m_1+m_2}v
\end{equation*}

and by conservation of energy

\begin{equation*}
\frac{m_2v^2}{2} = \frac{m_1}{2} V^2 + \frac{m_2}{2}V^2 + k\frac{(\Delta x)^2}{2}
\end{equation*}

So, we obtain that

\begin{equation*}
k\frac{(\Delta x)^2}{2}
= \frac{m_2v^2}{2} - \frac{m_1+m_2}{2} \left(\frac{m_2}{m_1+m_2}v\right)^2
= \frac{m_2v^2}{2} - \frac{m_2v^2}{2}\frac{1}{m_1+m_2}
= \frac{m_1m_2v^2}{2(m_1+m_2)}
\end{equation*}

By rearranging, we obtain the desired deformation

\section{Example 4: Oscillating mass}
\label{develop--math6192:ROOT:page--conservation.adoc---example-4-oscillating-mass}

\begin{admonition-note}[{}]
In this example, there is no conservation of momentum
\end{admonition-note}

Consider a mass \(m\) attached to a spring.
Determine the period of oscillation.

The total energy in the system is given by

\begin{equation*}
E = \frac{mv^2}{2} + U(x)
= \frac{m}{2} \left(\frac{dx}{dt}\right)^2 + U(x)
\end{equation*}

where \(U(x)\) is the potential energy at displacement \(x\).

\begin{admonition-tip}[{}]
Typically \(U(x) = -\frac{kx^2}{2}\) where \(k\) is the spring constant.
\end{admonition-tip}

So, since the energy is constant, we can rearrange to obtain an implicit form for position

\begin{equation*}
\int \frac{dx}{\sqrt{\frac{2}{M}(E-U(x))}} = \int dt = t + C
\end{equation*}

So, the period of oscillation is given by

\begin{equation*}
T = 2\int_{x_{\min}}^{x_{\max}} \frac{dx}{\sqrt{\frac{2}{M}(E-U(x))}}
\end{equation*}

where \(x_{\min}\) and \(x_{\max}\) are determined using the set of
\(x\) values for which \(E > U(x)\). If such an interval
does not exist (eg unbounded), we do not have periodic behaviour

\subsection{Potential well}
\label{develop--math6192:ROOT:page--conservation.adoc---potential-well}

The potential well of a system is a region surrounding the global minimum of potential energy
such that any potential energy is not able to be converted to another form.
This prevents the global minimum from being achieved.

\section{Example 5: collision of two bodies}
\label{develop--math6192:ROOT:page--conservation.adoc---example-5-collision-of-two-bodies}

Suppose we have three balls of equal mass \(m\) on a flat surface.
The first ball has a velocity of \(\vec{v}\)
and hits the two other balls which have exiting velocities of \(\vec{v}_1\)
and \(\vec{v}_2\).
We assume that the first ball has no velocity after collision.

\begin{admonition-note}[{}]
Instead of assuming that the first ball does not move after collision, we can
just use two balls instead of three.
\end{admonition-note}

First, let us look at the momentum and kinetic energy in the system.
There is no need to look at the potential energy since it is unchanged.

\begin{equation*}
\begin{aligned}
&m\vec{v} = m\vec{v}_1 + m \vec{v}_2
\implies \vec{v} = \vec{v}_1 + \vec{v}_2
\\
&\frac{1}{2}m\vec{v}^2 = \frac{1}{2}m\vec{v}_1^2 + \frac12 m \vec{v}_2^2
\implies \vec{v}^2 = \vec{v}_1^2 + \vec{v}_2^2
\end{aligned}
\end{equation*}

These two properties imply that \(\vec{v}_1\cdot\vec{v}_2 = 0\) (ie orthogonal).

\section{Coordinate change for two body system}
\label{develop--math6192:ROOT:page--conservation.adoc---coordinate-change-for-two-body-system}

Example 6: Planetary motion
The total potential energy (due to gravity) in a system only depends on
the distances between the masses. So, consider a two body system
with masses \(m_1\) at \(\vec{r}_1\) and \(m_2\) at \(\vec{r}_2\).
We will convert displacements relative to the center of mass.
The center of mass is given by

\begin{equation*}
\vec{R} = \frac{m_1}{m_1+m_2}\vec{r}_1 + \frac{m_2}{m_1+m_2}\vec{r}_2
\end{equation*}

So, the relative displacements of the two masses are

\begin{equation*}
\begin{aligned}
&\tilde{\vec{r}}_1 = \vec{r}_1 - \vec{R}
= \frac{m_2}{m_1+m_2}(\vec{r}_1-\vec{r}_2)
= \frac{m_2}{m_1+m_2}\vec{r}
\\
&\tilde{\vec{r}}_2 = \vec{r}_2 - \vec{R}
= \frac{m_1}{m_1+m_2}(\vec{r}_2-\vec{r}_1)
= \frac{-m_1}{m_1+m_2}\vec{r}
\end{aligned}
\end{equation*}

where \(\vec{r} = \vec{r}_1 - \vec{r}_2\).
So, using these transformed coordinates, we can determine the relative velocities
and the total kinetic energy as

\begin{equation*}
T
= \frac{1}{2} \left(\frac{m_1m_2}{m_1+m_2}\right)\left(\frac{d\vec{r}}{dt}\right)^2
= \frac{1}{2} M\left(\frac{d\vec{r}}{dt}\right)^2
\end{equation*}

where \(M=\frac{m_1m_2}{m_1+m_2}\) is called the \emph{reduced mass}.
So, if the potential energy function is given by \(U(\vec{r})\),
the total energy is given by

\begin{equation*}
E = \frac{1}{2}M \left(\frac{d\vec{r}}{dt}\right)^2 + U(\vec{r})
\end{equation*}

\subsection{Application to planetary motion}
\label{develop--math6192:ROOT:page--conservation.adoc---application-to-planetary-motion}

Recall that for planar motion,
\(\vec{r}\times \vec{p} = M\) is constant
since \(\vec{p} = m \frac{d\vec{r}}{dt}\) and \(\frac{d\vec{p}}{dt}\) is
collinear with \(\vec{r}\) (due to the direction of the gravitational force).

\begin{admonition-tip}[{}]
Recall that that \(\frac{d\vec{r}}{dt} = \frac{dr}{dt}\vec{e}_r +r \vec{e}_\theta\frac{d\theta}{dt}\).
\end{admonition-tip}

Next, the angular component of velocity is given by \(r\frac{d\theta}{dt}\)
and hence the angular component of momentum of a body with mass \(m\)
is given by \(mr\frac{d\theta}{dt}\).
Therefore,

\begin{equation*}
M = \vec{r}\times\vec{p}
= r(\text{orthogonal component of }\vec{\rho})
= mr^2\frac{d\theta}{dt}
\implies
\frac{d\theta}{dt} = \frac{M}{mr^2}
\end{equation*}

So, the total energy is given by

\begin{equation*}
E
= \frac{1}{2}m\left[\left(\frac{dr}{dt}\right)^2 + r^2\left(\frac{d\theta}{dt}\right)^2\right] + U(r)
= \frac{1}{2}m\left[\left(\frac{dr}{dt}\right)^2 + r^2\left(\frac{M}{mr^2}\right)^2\right] + U(r)
= \frac{1}{2}m\left(\frac{dr}{dt}\right)^2 + U_{eff}(r)
\end{equation*}

where \(U_{eff}(r) = \frac{M^2}{2mr^2} + U(r)\) is the effective potential energy.
So, by rearranging we get that

\begin{equation*}
\int \frac{dr}{\sqrt{\frac{2}{m}(E-U_{eff}(r))}} = \int dt = t + C
\end{equation*}

which is an implicit form for \(r(t)\).
Also, we have that

\begin{equation*}
\theta = \int \frac{M}{mr^2} \ dt
\end{equation*}

Therefore, we have have a form for both \(\vec{r}(t)\).

If we use \(U(r) = -\frac{GMm}{r}\) (gravitational potential energy),
we get that

\begin{equation*}
U_{eff} = -\frac{GMm}{r} + \frac{M^2}{2mr^2}
\end{equation*}

From the implicit form for \(r(t)\), we require that \(E > U_{eff}\).
We first need to understand the behaviour of \(U_{eff}\);
it will be nice if it is the same for all sets of parameters \(G,M,m\).
Note

\begin{equation*}
U_{eff}
= -Gm \frac{M}{r} + \frac{1}{2m}\left(\frac{M}{r}\right)^2
= \frac{M}{r}\left(\frac{1}{2m}\frac{M}{r} - Gm\right)
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/planetary-potential-well}
\caption{Illustration of potential well \(U_{eff}(r)\)}
\end{figure}

As we see it is a parabolic shape with variable \(\frac{1}{r}\),
and attains a negative minimum. Also, there is only one root.
So, it is initial positive (for small \(r\)),
then becomes and remains negative as \(r\to \infty\) (but \(U_{eff} \to 0^-\)).
So,

\begin{itemize}
\item If \(E \geq 0\), then \(E > U_{eff}\) for an unbounded interval in \(r\)
    and there is no periodic behaviour
\item If \(E < 0\), then there exists minimum and maximum values for \(r\)
    and hence there is periodic behaviour.
\end{itemize}
\end{document}
