\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Taylor's Theorem}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Taylor's Theorem gives an approximation of a continuous function \(f\) about a number \(c\) by a
polynomial \(P\) of degree \(n\). Additionally, it is possible to determine the minimum value of \(n\)
required in order to achieve a desired accuracy in the approximation of \(f\).

\begin{description}
\item[Taylor Polynomial] Let \(f:(a, b) \to \mathbb{R}\) be a \(n\)-times differentiable function. Then, the
    \emph{Taylor polynomial} of degree \(n\) of \(f\) at \(c \in (a, b)\) is
\end{description}

\begin{equation*}
P_n(x) =
\sum_{k=0}^n \frac{f^{(k)}(c)}{k!}(x-c)^k
\end{equation*}

\section{Taylor's Theorem}
\label{develop--math3277:differentiation:page--taylor.adoc---taylors-theorem}

Let \(f:(a,b)\to \mathbb{R}\) be a \((n+1)\)-times differentiable function and \(c \in (a, b)\).
Then, for every \(x \in (a,b)\), there exists \(\xi\) between \(c\) and \(x\) (note that
\(c\) could be larger or smaller than \(x\)) such that

\begin{equation*}
f(x) = P_n(x) + R_n(x)
\end{equation*}

where

\begin{equation*}
P_n(x) =
\sum_{k=0}^n \frac{f^{(k)}(c)}{k!}(x-c)^k
\quad\text{and}\quad
R_n(x) = \frac{f^{(n+1)}(\xi)}{(n+1)!}(x-c)^{n+1}
\end{equation*}

We call \(R_n\) the error/remainder in the approximation of \(f\) by \(P_n\).

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
The usual proof (the one presented in class) which is presented
lacks reasoning and is often hard to understand. I find the proof presented
here more organic (well, as this is the thought process I went through while developing it).
\end{admonition-note}

Recall our aim. We want to show that there is \(\xi\) between \(x\) and \(c\)
such that

\begin{equation*}
R_n(x) = \frac{f^{(n+1)}(\xi)}{(n+1)!}(x-c)^{n+1} = f(x) - P_n(x)
\end{equation*}

where \(x\) and \(c\) are fixed. We would now make the right
a function of \(c\) and call it \(g\). That is, we define \(g\)
as follows

\begin{equation*}
g(t)
= f(x) - \sum_{k=0}^{n} \frac{f^{(k)}(t)}{k!} (x-t)^k
= f(x) - f(t) - \sum_{k=1}^{n} \frac{f^{(k)}(t)}{k!} (x-t)^k
\end{equation*}

Then, immediately \(g(x) = 0\) and, by construction, \(g(c) = f(x) - P_n(x)\). Also, \(g\) is differentiable and notice that

\begin{equation*}
\begin{aligned}
g'(t)
&= -f'(t) - \sum_{k=1}^n \left[\frac{f^{(k+1)}(t)}{k!} (x-t)^k - \frac{f^{(k)}(t)}{(k-1)!} (x-t)^{k-1}\right]
\\& = -f'(t) - \sum_{k=1}^n \frac{f^{(k+1)}(t)}{k!} (x-t)^k + \sum_{k=0}^{n-1} \frac{f^{(k+1)}(t)}{k!} (x-t)^{k}
\\& = -f'(t) - \frac{f^{(n+1)}(t)}{n!} (x-t)^n + f'(t)
\\& = - \frac{f^{(n+1)}(t)}{n!} (x-t)^n
\end{aligned}
\end{equation*}

Now, if we directly apply the mean value theorem, we would get a term in \((x-\xi)^n\) which we do not
want. Instead we would apply the \myautoref[{Cauchy Mean Value Theorem}]{develop--math3277:differentiation:page--extreme-points.adoc---cauchy-mean-value-theorem}
with second function
\(t \mapsto (x-t)^{n+1}\) to get that there exists a value \(\xi\) between \(x\) and \(c\)
such that

\begin{equation*}
g'(\xi)\left[(x-c)^{n+1} - (x-x)^{n+1}\right] = -(n+1)(x-\xi)^n[g(c) - g(x)]
\end{equation*}

Then, we get that

\begin{equation*}
\begin{aligned}
\frac{f^{(n+1)}(\xi)}{(n+1)!} (x-c)^{n+1}
&= \frac{ - g'(\xi)}{(n+1)(x-\xi)^n}(x-c)^{n+1}
\\&= g(c) - g(x)
\\&= g(c)
\\&= f(x) - P_n(x)
\end{aligned}
\end{equation*}

and we are done.
\end{example}
\end{document}
