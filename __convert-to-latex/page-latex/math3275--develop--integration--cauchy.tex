\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cauchy's Integral Theorems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Ln}{Ln}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\Res}{Res}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-remark}[{}]
Lo siento a otra mathematiciones

\begin{admonition-thought}[{}]
My condolences to people who speak spanish
\end{admonition-thought}
\end{admonition-remark}

Throughout this section, all closed contours are simply closed Jordan arcs unless otherwise stated.
Also, we would denote the interior of a closed contour \(C\), defined by the Jordan curve theorem, as \(I(C)\).

\section{Cauchy-Goursat Integral Theorem}
\label{develop--math3275:integration:page--cauchy.adoc---cauchy-goursat-integral-theorem}

Let \(C\) be a closed contour and \(f(z)\) be analytic on \(C \cup I(C)\) then

\begin{equation*}
\oint_C f(z) \ dz = 0
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-theorem}
\end{figure}

This theorem was initially formulated by Cauchy with the additional restriction
that \(f'(z)\) was continuous. However, Goursat was later able to obtain the
same result without requiring this restriction.

\begin{example}[{Proof with differential continuity}]
Let \(f(z) = u + iv\). Then

\begin{equation*}
\oint_C f(z) \ dz = \oint_C (u+iv)(dx + i dy) = \oint_C u dx - v dy + i\oint_C v dx + u dy
\end{equation*}

Then, by \myautoref[{Green's theorem}]{develop--math2270:ROOT:page--line-integrals.adoc---greens-theorem} since \(f'(z)\) is continuous
and hence the partial derivatives of \(u\) and \(v\) are also continuous, we get that

\begin{equation*}
\oint_C f(z) \ dz = \iint_{I(C)} (-v_x - u_y) dA + i \iint_{I(C)} (u_x - v_y) dA = 0
\end{equation*}

with additional help from the \myautoref[{}]{develop--math3275:analytic:page--cr-equations.adoc}.
\end{example}

\subsection{Indepedence of path}
\label{develop--math3275:integration:page--cauchy.adoc---indepedence-of-path}

If \(I(C)\) contains two paths \(C_1\) and \(C_2\) with the same endpoints (and orientation),

\begin{equation*}
\int_{C_1} f(z) \ dz = \int_{C_2} f(z) \ dz
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/independent-path}
\end{figure}

\begin{example}[{Proof}]
Notice that \(C_1 \cup (-C_2)\) form a closed contour and since they lie in \(I(C)\),
\(f(z)\) is also analytic in \((C_1 \cup (-C_2)) \cup I(C_1 \cup (-C_2))\)
and hence

\begin{equation*}
0 = \oint_{C_1 \cup (-C_2)} f(z) \ dz = \oint_{C_1} f(z) \ dz - \oint_{C_2} f(z) \ dz
\end{equation*}

and we are done.
\end{example}

\section{Multiply Connected regions}
\label{develop--math3275:integration:page--cauchy.adoc---multiply-connected-regions}

Let \(C\) and \(C_1, \ldots C_n\) be closed contours such that \(I(C)\) contains all \(C_i\)
and each of the \(C_i\) are disjoint. Then if \(f(z)\) is analytic
in the region between them,

\begin{equation*}
\oint_C f(z) \ dz = \oint_{C_1} f(z) \ dz + \oint_{C_2} f(z) \ dz + \cdots + \oint_{C_n} f(z) \ dz
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-theorem-multiple}
\end{figure}

\begin{example}[{Proof}]
We can the result by creating a cross cuts from \(C\) to
the \(C_i\).

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-theorem-multiple-cuts}
\end{figure}

Hence obtaining the following integral

\begin{equation*}
\begin{aligned}
0
&= \oint_{C \cup (-B_1) \cup (-C_1) \cup (B_1) \cup \cdots (B_1) \cup (-C_n) \cup (B_n)} f(z) \ dz
\\&= \oint_{C} f(z) \ dz - \oint_{C_1} f(z) \ dz - \cdots - \oint_{C_n} f(z) \ dz
\end{aligned}
\end{equation*}

since the new path encloses the same region and the cross cuts cancel each of themselves out.
Hence, the result follows.
\end{example}

\begin{admonition-tip}[{}]
Using this result when \(n=1\) allows us to ``reshape'' the contour over which we need
to integrate.
\end{admonition-tip}

\section{Integral Formula}
\label{develop--math3275:integration:page--cauchy.adoc---integral-formula}

Let \(C\) be a closed contour and \(f(z)\) be analytic on \(C \cup I(C)\)
and \(a \in I(C)\). Then

\begin{equation*}
f(a) = \frac{1}{2\pi i}\oint_C \frac{f(z)}{z-a} \ dz
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-integral}
\end{figure}

\begin{example}[{Proof}]
Firstly, let \(\varepsilon > 0\). Then, since \(f\) is analytic,
it is continuous at \(z = a\) and hence, there exists
\(\delta > 0\) such that

\begin{equation*}
\forall z: |z - a| < \delta \implies |f(z) - f(a)| < \varepsilon
\end{equation*}

Then let \(0 < \rho < \delta\) be such that the circle \(C_1\), defined by \(|z-a| = \rho\),
lies in \(I(C)\).

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-integral-small}
\end{figure}

Then since \(\frac{f(z)}{z-a}\) has only possibly has a pole at \(z = a\), it is analytic on
the area between \(C\) and \(C_1\). Then

\begin{equation*}
\oint_{C} \frac{f(z)}{z-a} \ dz  = \oint_{C_1} \frac{f(z)}{z-a} \ dz = \oint_{C_1} \frac{f(z) - f(a)}{z-a} \ dz + \oint_{C_1} \frac{f(a)}{z-a} \ dz
\end{equation*}

We would now focus on each of the integrals separately. Then,
by using the parameterization \(z-a = \rho e^{i\theta}\)

\begin{equation*}
\oint_{C_1} \frac{f(z) - f(a)}{z-a} \ dz
= \int_0^{2\pi} \frac{f(a + \rho e^{i\theta}) - f(a)}{\rho e^{i\theta}} i\rho e^{i\theta} \ d\theta
= i\int_0^{2\pi} f(a + \rho e^{i\theta}) - f(a) \ d\theta
\end{equation*}

Then we get that

\begin{equation*}
\begin{aligned}
\left|\oint_{C_1} \frac{f(z) - f(a)}{z-a} \ dz\right|
&= \left| \int_0^{2\pi} f(a + \rho e^{i\theta}) - f(a) \ d\theta \right|
\\&\leq \int_0^{2\pi} |f(a + \rho e^{i\theta}) - f(a)| \ d\theta
\\&\leq \int_0^{2\pi} \varepsilon \ d\theta
\\&= 2\pi \varepsilon
\end{aligned}
\end{equation*}

Therefore the integral must be zero since it lies in every circle centered at the origin.
Hence,

\begin{equation*}
\oint_{C} \frac{f(z)}{z-a} \ dz
= \oint_{C_1} \frac{f(a)}{z-a} \ dz
= \int_0^{2\pi} \frac{f(a)}{\rho e^{i\pi}} i\rho e^{i\theta} \ d\theta
= i f(a)\int_0^{2\pi} \ d\theta
= 2\pi i f(a)
\end{equation*}

and we are done.
\end{example}

\begin{admonition-tip}[{}]
This formula can be combined with the multiple connected
region theorem to simplify integrals.
\end{admonition-tip}

\subsection{Cauchy's multiple integral formula}
\label{develop--math3275:integration:page--cauchy.adoc---cauchys-multiple-integral-formula}

If \(f(z)\) is analytic in-between contours \(C\) and \(D\) and \(a\) lies in that region,

\begin{equation*}
f(a) = \frac{1}{2\pi i}\oint_{C} \frac{f(z)}{z-a} \ dz - \frac{1}{2\pi i}\oint_{D} \frac{f(z)}{z-a} \ dz
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-integral-multiple}
\end{figure}

\begin{admonition-note}[{}]
This is a corollary of the integral formula along with the multiple region theorem.
\end{admonition-note}

\subsection{Gauss Mean Value Theorem}
\label{develop--math3275:integration:page--cauchy.adoc---gauss-mean-value-theorem}

If \(f(z)\) is analytic in some domain \(D\) and
the circular region \(|z - a| \leq \rho\) is contained in \(D\),
then

\begin{equation*}
f(a) = \frac{1}{2\pi} \int_0^{2\pi} f(a + \rho e^{i\theta}) \ d\theta
\end{equation*}

This follows immediately from a change of variables \(z = \rho e^{i\theta}\) on the integral
theorem.

\subsection{Posisson's Integral Formula}
\label{develop--math3275:integration:page--cauchy.adoc---posissons-integral-formula}

Let \(f(z)\) be analytic in \(C \cup I(C)\) where
\(C\) is the circle \(|z| = R\). Then if
\(r < R\) and \(z = re^{i\theta}\) then

\begin{equation*}
f(re^{i\theta}) = \frac{1}{2\pi} \int_0^{2\pi} \frac{R^2 - r}{R^2 - 2Rr\cos(\theta-\phi) + r^2} f(Re^{i\phi}) \ d\phi
\end{equation*}

\begin{example}[{Proof}]
Let \(a = re^{i\theta}\). Then by the integral theorem

\begin{equation*}
f(re^{i\theta})
= \frac{1}{2\pi i }\oint_C \frac{f(z)}{z-re^{i\theta}} \ dz
\end{equation*}

Also, since \(\frac{R^2}{\overbar{a}}\) is the inverse point of \(a\),
it lies outside of \(C\) and hence

\begin{equation*}
0
= \oint_{C} \frac{f(z)}{z-\frac{R^2}{\overbar{a}}} \ dz
= \oint_{C} \frac{f(z)}{z-\frac{R^2}{r}e^{i\theta}} \ dz
\end{equation*}

since it remains analytic since it has no poles. Therefore

\begin{equation*}
\begin{aligned}
f(re^{i\theta})
&= \frac{1}{2\pi i }\oint_C \frac{f(z)}{z-re^{i\theta}} \ dz - \frac{1}{2\pi i}\oint_{C} \frac{f(z)}{z-\frac{R^2}{r}e^{i\theta}} \ dz
\\&= \frac{1}{2\pi i }\oint_C \frac{f(z)}{z-re^{i\theta}} - \frac{f(z)}{z-\frac{R^2}{r}e^{i\theta}} \ dz
\\&= \frac{1}{2\pi i }\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        Re^{i\phi}-\frac{R^2}{r}e^{i\theta} - Re^{i\phi}+re^{i\theta}
    }{
        (Re^{i\phi}-re^{i\theta})\left(Re^{i\phi}-\frac{R^2}{r}e^{i\theta}\right)
    } iRe^{i\phi} d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        re^{i\theta} - \frac{R^2}{r}e^{i\theta}
    }{
        (Re^{i\phi}-re^{i\theta})\left(Re^{i\phi}-\frac{R^2}{r}e^{i\theta}\right)
    } Re^{i\phi} d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        r^2 - R^2
    }{
        (Re^{i\phi}-re^{i\theta})(re^{-i\theta}-Re^{-i\phi})
    } d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        r^2 - R^2
    }{
        Rre^{i(\phi-\theta)} - R^2 - r^2 + Rre^{i(\theta-\phi)}
    } d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        r^2 - R^2
    }{
        2Rr\cos(\theta - \phi) - R^2 - r^2
    } d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        R^2 - r^2
    }{
        R^2 - 2Rr\cos(\theta - \phi) + r^2
    } d\phi
\end{aligned}
\end{equation*}
\end{example}

\section{Derivatives}
\label{develop--math3275:integration:page--cauchy.adoc---derivatives}

If \(f(z)\) is analytic on \(C \cup I(C)\) then

\begin{equation*}
f'(a) = \frac{1}{2\pi i }\oint_C \frac{f(z)}{(z-a)^2} \ dz
\end{equation*}

and in general

\begin{equation*}
f^{(n)}(a) = \frac{n!}{2\pi i }\oint_C \frac{f(z)}{(z-a)^{n+1}} \ dz
\end{equation*}

Both of these are a result of from the \myautoref[{following lemma}]{develop--math3275:integration:page--cauchy.adoc---lemma-1}
since \myautoref[{Cauchy's integral formula}]{develop--math3275:integration:page--cauchy.adoc---integral-formula} holds and \(f(z)\)
is analytic and hence bounded on \(C\).

\subsection{Lemma 1}
\label{develop--math3275:integration:page--cauchy.adoc---lemma-1}

Let \(C\) be a closed contour on which \(g(z)\) is bounded.
Also let

\begin{equation*}
f(z) = \oint_C \frac{g(w)}{(w - z)^n} \ dw
\end{equation*}

for all \(z \in I(C)\) for some \(n \in \mathbb{Z}^+\). Then,
\(f(z)\) is analytic on \(I(C)\) and

\begin{equation*}
f'(z) = n \oint_C \frac{g(w)}{(w-z)^{n+1}} \ dw
\end{equation*}

for all points \(z \in I(C)\). This then implies that \(f\)
is infinitely differentiable.

\begin{example}[{Proof}]
Firstly, focus on some \(z=a \in I(C)\) and let \(d > 0\) be such that
\(|z-a| > d\) for all \(z \in C\).
Then let \(\varepsilon > 0\) and \(0<|h| < \delta\) where \(0<\delta < \frac{d}{2}\)

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/cauchy-integral-lemma-1}
\end{figure}

Then, we get that

\begin{equation*}
\frac{f(a + h) - f(a)}{h} - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
= \oint_C g(w)h\frac{\mu_n(w,h,a)}{(w-a -h)^n(w-a)^{n+1}} \ dw
\end{equation*}

where \(\mu_n\) is some multinomial in \(w\), \(h\) and \(a\).

\begin{example}[{When \(n=1\)}]
Because of the manipulations done when \(n \geq 2\), we would conduct
this case separately.

\begin{equation*}
\begin{aligned}
&\frac{f(a + h) - f(a)}{h} - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \frac{1}{h}\left[ \oint_{C} \frac{g(w)}{(w-a -h)} \ dw - \oint_C \frac{g(w)}{(w-a)} \ dw \right] - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \frac{1}{h}\oint_C g(w)\frac{(w-a) - (w-a-h)}{(w-a -h)(w-a)} \ dw - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \oint_C g(w)\frac{1}{(w-a -h)(w-a)} \ dw - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \oint_C g(w)\frac{(w-a) - (w-a-h)}{(w-a -h)(w-a)^2} \ dw
\\&= \oint_C g(w)\frac{h}{(w-a -h)(w-a)^2} \ dw
\\&= \oint_C g(w)h\frac{\mu_1(w,h,a)}{(w-a -h)(w-a)^{2}} \ dw
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{When \(n \geq 2\)}]
\begin{equation*}
\begin{aligned}
&\frac{f(a + h) - f(a)}{h} - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\\&= \frac{1}{h}\left[ \oint_{C} \frac{g(w)}{(w-a -h)^n} \ dw - \oint_C \frac{g(w)}{(w-a)^n} \ dw \right] - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\\&= \frac{1}{h}\oint_C g(w)\frac{(w-a)^n - (w-a-h)^n}{(w-a -h)^n(w-a)^n} \ dw - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)[(w-a)^n - (w-a-h)^n] - n(w-a-h)^n}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \frac{1}{h}(w-a)(w-a-h)^n - n(w-a-h)^n}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \left[\frac{1}{h}(w-a) +n\right](w-a-h)^n}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=0}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \left[\frac{1}{h}(w-a) +n\right](w-a)^n - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=1}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{-n(w-a)^n - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=1}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{-n(w-a)^n - \left[\frac{1}{h}(w-a) +n\right]n(-h)(w-a)^{n-1} - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=2}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{n^2h(w-a)^{n-1} - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=2}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)h\frac{n^2(w-a)^{n-1} - \left[(w-a) +nh\right]\sum_{k=2}^n\binom{n}{k}(-h)^{k-2}(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)h\frac{\mu_n(w,h,a)}{(w-a -h)^n(w-a)^{n+1}} \ dw
\end{aligned}
\end{equation*}
\end{example}

Then we see that,

\begin{equation*}
|w-a-h| \geq |w-a| - |h| > d - \frac{d}{2} = \frac{d}{2}
\end{equation*}

Also, let \(|g(w)| < M\) and \(|\mu_n(w,h,a)| < N_n\). Then,

\begin{equation*}
\begin{aligned}
&\left|\frac{f(a + h) - f(a)}{h} - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw\right|
\\&= \left|\oint_C g(w)h\frac{\mu_n(w,h,a)}{(w-a -h)^n(w-a)^{n+1}} \ dw\right|
\\&\leq \oint_C |g(w)||h|\frac{|\mu_n(w,h,a)|}{|w-a -h|^n|w-a|^{n+1}} \ |dw|
\\&\leq \oint_C |h|\frac{MN_n}{\left(\frac{d}{2}\right)^n d^{n+1}} \ |dw|
\\&= \frac{2^nMN_n}{d^{2n+1}}|h|\oint_C \ |dw|
\\&= \frac{2^nMN_n L}{d^{2n+1}}|h|
\\&= \frac{2^nMN_n L}{d^{2n+1}}\delta
\end{aligned}
\end{equation*}

where \(L\) is the length of \(C\). Then, we could have fixed \(\delta\)
to be a value such that the above was less than \(\varepsilon\) and hence we get that

\begin{equation*}
f'(a) = \lim_{h\to 0} \frac{f(a + h) - f(a)}{h} = \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\end{equation*}
\end{example}

\subsection{Liouville's Theorem: Bounded entire functions}
\label{develop--math3275:integration:page--cauchy.adoc---liouvilles-theorem-bounded-entire-functions}

If \(f(z)\) is a bounded entire function, then \(f(z)\) is constant.

\begin{example}[{Proof}]
Focus on some \(z = a\) and let \(r > 0\) and \(C\) be a circle
of radius \(r\) centered at \(a\). Then,

\begin{equation*}
f'(a) = \frac{1}{2\pi i}\oint_C \frac{f(z)}{(z-a)^2} \ dz
\end{equation*}

Let \(|f(z)| < M\), then

\begin{equation*}
|f'(a)|
= \frac{1}{2\pi}\left|\oint_C \frac{f(z)}{(z-a)^2} \ dz\right|
\leq frac{1}{2\pi}\oint_C \frac{|f(z)|}{|z-a|^2} \ |dz|
\leq frac{1}{2\pi}\oint_C \frac{M}{r^2} \ |dz|
= frac{1}{2\pi} \frac{2\pi r M }{r^2}
= \frac{M}{r}
\end{equation*}

and hence for each \(\varepsilon > 0\), \(\exists r = \frac{M}{\varepsilon}\)
such that \(|f'(a)| < \varepsilon\). Therefore,
\(f'(a) = 0\) and hence \(f(z)\) is constant.
\end{example}

\section{Morera's Theorem}
\label{develop--math3275:integration:page--cauchy.adoc---moreras-theorem}

Let \(f(z)\) be defined on domain \(D\). Then if

\begin{itemize}
\item \(f(z)\) is continuous inside all closed contours \(C\) inside \(D\)
\item \(\oint_C f(z) \ dz = 0\) for all simple closed curves \(C\) inside \(D\).
\end{itemize}

Then, \(f(z)\) is analytic in \(D\).

\begin{admonition-note}[{}]
The second condition can equivalently stated as the integral is
independent of path inside \(D\).
\end{admonition-note}

\begin{admonition-remark}[{}]
I'm not totally sure, but I do believe that this page has already proven this
result. Notice that all the above proofs (barring the first) only required that
the integral of a closed curve as \(0\) and the function was continuous. Note
that the requirement of continuity was only used by the integral formula. Then
since the derivative formula holds, we get that \(f(z)\) must be analytic.

... I must be missing something
\end{admonition-remark}
\end{document}
