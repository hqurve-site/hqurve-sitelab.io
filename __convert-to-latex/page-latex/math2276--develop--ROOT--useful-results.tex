\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Useful results}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
In this section, several useful results will be discussed since they
don’t fit into any one of the previous sections.

\section{Allocating objects to classes}
\label{develop--math2276:ROOT:page--useful-results.adoc---allocating-objects-to-classes}

Suppose given a set of \(r\) classes and \(n\)
identical objects, each of the objects must be allocated to one class.
How many ways of doing this are there? Well as previously discussed,
there are

\begin{equation*}
\binom{n+r-1}{r-1} = \binom{n+r-1}{n}
\end{equation*}

ways of doing this. However, what happens if constraints on the number
of objects in each class. In order simplify the constraints, the problem
would be transformed into the following diophantine equation

\begin{equation*}
x_1 + x_2 + \ldots + x_r = n
\end{equation*}

where constraints will be put onto each of the \(x_i\). It is
easily seen that each solution of this equation corresponds to one
allocation of the \(n\) objects.

\subsection{Result for positive \(x_i\)}
\label{develop--math2276:ROOT:page--useful-results.adoc---result-for-positive-latex-backslashx-latex-underscorei-latex-backslash}

For further investigation, this case is very important. Given that each
of the \(x_i\) must be positive, how many ways are there to
solve this equation? In order to solve this, we will again transform the
problem as follows.

Consider a line of \(n\) balls and \(r-1\) dividers
where each of the dividers can be placed into the \(n-1\)
spaces between the balls. If only one divider can be placed in each
space, we partition the balls into \(r\) sets each containing
at least one ball. Note now, that each of these partitions corresponds
to exactly one solution of the equation we want to solve. Therefore
there are

\begin{equation*}
\binom{n-1}{r-1}
\end{equation*}

solutions to the equation.

\subsection{Result for \(x_i\) bounded from below}
\label{develop--math2276:ROOT:page--useful-results.adoc---result-for-latex-backslashx-latex-underscorei-latex-backslash-bounded-from-below}

How many ways are there to select \(r\) integers (ordered)
such that their sum is \(n\) and each of the
\(x_i > a_i\)? That is how many solutions are there to the
following equation

\begin{equation*}
x_1 + x_2 +\ldots x_r = n \quad \quad \text{such that } x_i > a_i \;\forall i=1,2,\ldots, r
\end{equation*}

In order to solve this general case, a transformation is required. So we
define

\begin{equation*}
y_i = x_i - a_i
\end{equation*}

and now the equation becomes

\begin{equation*}
y_1 + y_2 + \ldots y_r = n - (a_1 + a_2 + \ldots a_r)
\end{equation*}

Note now that each of the \(y_i\) is positive. Therefore we
can use the previous result for positive values and we find that the
total number of solutions is

\begin{equation*}
\binom{n - (a_1 + a_2 + \ldots a_r) -1}{r-1}
\end{equation*}

\begin{admonition-note}[{}]
The two previous cases both work with this formula

\begin{itemize}
\item when \(a_i = 0\), that is \(x_i > 0\) the result
    reduces to \(\binom{n - 1}{r-1}\).
\item when \(a_i = -1\), that is \(x_i \geq\) the result
    reduces to \(\binom{n+r-1}{r-1}\).
\end{itemize}
\end{admonition-note}

\subsection{Result for \(x_i\) bounded from above}
\label{develop--math2276:ROOT:page--useful-results.adoc---result-for-latex-backslashx-latex-underscorei-latex-backslash-bounded-from-above}

Suppose, some of the \(x_i\) are bounded from above. No need
to worry because theres a simple tool to use, the principle of
inclusion-exclusion. That is, firstly count the number of solutions
without the upper bound and then remove those which don’t satisfy the
upper bound. Suppose \(t\) of the \(x_i\) are
bounded by above each by \(x_i \leq b_i\). Then the number of
valid solutions is given by

\begin{equation*}
N - \sum_i N(i) + \sum_{i < j} N(i, j) - \ldots + (-1)^t N(i, j, \ldots t)
\end{equation*}

where

\begin{itemize}
\item \(N\) is the number of solutions without the upper bound.
\item \(N(i)\) is the number of solutions where
    \(x_i > b_i\) instead of \(x_i > a_i\).
\item \(N(i,j)\) is the number of solutions where
    \(x_i > b_i\) instead of \(x_i > a_i\) and
    \(n_j > b_j\) instead of \(x_j > a_j\).
    \(\vdots\)
\end{itemize}

Therefore the solution reduces to a problem where it is only bounded
below (at any one step).

\begin{example}[{Example}]
for sake of understanding. Consider the number of ways of solving the
following equation

\begin{equation*}
x_1 + x_2 + x_3 = 15 \quad \quad \text{where } 0 < x_1 \leq 5, \; 2 < x_2 \leq 10, \; 0 \leq x_3
\end{equation*}

Firstly, the equation is transformed to get the following

\begin{equation*}
y_1 + y_2 + y_3 = 15 - 0 - 2 -(-1) = 14 \quad \quad \text{where } 0 < y_1 \leq 5, \; 0 < y_2 \leq 8 \; 0 < y_3
\end{equation*}

Then

\begin{itemize}
\item \(N = \binom{14-1}{3-1} = \binom{13}{2} = 78\)
\item \(N(1) = \binom{14 - 5 - 1}{3-1} = \binom{8}{2} = 28\)
\item \(N(2) = \binom{14 - 8 - 1}{3-1} = \binom{5}{2} = 10\)
\item \(N(1,2) = \binom{14 - (5 + 8) - 1}{3-1} = \binom{0}{2} = 0\)
\end{itemize}

Therefore, by the principle of inclusion-exclusion, the total number of
solutions is

\begin{equation*}
N - N(1) - N(2) + N(1,2) = 78 - 28 - 10 + 0 = 40
\end{equation*}
\end{example}

\section{Special matrix inversion}
\label{develop--math2276:ROOT:page--useful-results.adoc---special-matrix-inversion}

Consider the following matrix

\begin{equation*}
A =
\begin{bmatrix}
    1 & 0 & 0 & 0\\
    1 & 1 & 0 & 0\\
    1 & 2 & 1 & 0\\
    1 & 3 & 3 & 0\\
\end{bmatrix}
\end{equation*}

where each of the rows is a row from pascal’s triangle (padded with
zeros on the right).\newline
Notice that the inverse of this matrix is

\begin{equation*}
A^{-1} =
\begin{bmatrix}
    1 & 0 & 0 & 0\\
    -1 & 1 & 0 & 0\\
    1 & -2 & 1 & 0\\
    -1 & 3 & -3 & 0\\
\end{bmatrix}
\end{equation*}

This is very interesting. Lets formalize this.\newline
Given a \((n+1) \times (n+1)\) matrix \(A\), with
rows and columns labeled \(0, 1,\ldots n\), defined by
\(a_{ij} = \binom{i}{j}\). Also let \(B\) is defined
by \(b_{ij} = (-1)^{i+j}\binom{i}{j}\). Then \(B\)
is the inverse of \(A\).

\begin{admonition-note}[{}]
Before we get into this proof we take \(\binom{n}{k} = 0\) if
\(k > n\). Note that this also makes sense since there is no
way to choose \(k\) objects from \(n\) if
\(k > n\).
\end{admonition-note}

\begin{example}[{Proof}]
Consider the \((i, j)\)’th entry of \(BA\).
The formula for this is

\begin{equation*}
\sum_k b_{ik}a_{kj} = \sum_k (-1)^{i+k} \binom{i}{k} \binom{k}{j}
\end{equation*}

There are three cases

\begin{itemize}
\item \(i = j\): Then the sum is zero for all \(k\)
    except when \(k = i\). Hence the value is
    \((-1)^{2i} \binom{i}{i}^2 = 1\). Therefore, the diagonals
    consists of \(1\)s.
\item \(i > j\): Then the sum is
    
    \begin{equation*}
    \begin{aligned}
                        \sum_k (-1)^{i+k} \binom{i}{k} \binom{k}{j}
                        &= \sum_{j \leq k \leq i} (-1)^{i+k} \binom{i}{k} \binom{k}{j} \\
                        &= \sum_{j \leq k \leq i} (-1)^{i+k} \frac{i!}{k!(i-k)!} \frac{k!}{j!(k-j)!} \\
                        &= \sum_{j \leq k \leq i} (-1)^{i+k} \frac{i!}{(i-k)!j!(k-j)!} \\
                        &= \sum_{j \leq k \leq i} (-1)^{i+k} \frac{i!}{(i-j)!j!} \frac{(i-j)!}{(i-k)!(k-j)!} \\
                        &= \sum_{j \leq k \leq i} (-1)^{i+k} \binom{i}{j} \binom{i-j}{k-j} \\
                        &= \binom{i}{j} \sum_{j \leq k \leq i} (-1)^{i+k}  \binom{i-j}{k-j} \\
                        &= \binom{i}{j} (-1)^{i + j}\sum_{0 \leq k-j \leq i-j} (-1)^{k-j}  \binom{i-j}{k-j}\\
                        &= \binom{i}{j} (-1)^{i + j} (1 - 1)^{i-j}
                        &= 0
                    \end{aligned}
    \end{equation*}
\item \(i < j\): Note the sum is zero for all \(k\)
    since either \(k > i\) or \(k < j\).
\end{itemize}

Therefore, the matrix \(BA = I\) and by using the same
argument we can get \(AB = I\). Therefore, \(B\) is
the inverse of \(A\). ◻
\end{example}

\subsection{Usage}
\label{develop--math2276:ROOT:page--useful-results.adoc---usage}

Consider the two sequences \(\{a_n\}\) and
\(\{b_n\}\) related by

\begin{equation*}
a_n = \sum_{k=0}^n \binom{n}{k} b_k
\end{equation*}

then the sequences follow the linear system

\begin{equation*}
\begin{bmatrix}
    a_0 \\ a_1 \\\vdots\\ a_n
\end{bmatrix}
=
A
\begin{bmatrix}
    b_0 \\ b_1 \\\vdots\\ b_n
\end{bmatrix}
\end{equation*}

therefore

\begin{equation*}
\begin{bmatrix}
    b_0 \\ b_1 \\\vdots\\ b_n
\end{bmatrix}
 BA
\begin{bmatrix}
    b_0 \\ b_1 \\\vdots\\ b_n
\end{bmatrix}
 B
\begin{bmatrix}
    a_0 \\ a_1 \\\vdots\\ a_n
\end{bmatrix}
\end{equation*}

and hence

\begin{equation*}
b_n = \sum_{k=0}^n (-1)^{n+k}\binom{n}{k} a_k
\end{equation*}

\section{Derangements}
\label{develop--math2276:ROOT:page--useful-results.adoc---derangements}

A derangement is simply a permutation with no fixed points. That is a
permutation \(\pi : S \rightarrow S\) such that
\(\forall i \in S: \pi(i) \neq i\). Now knowing this, how many
derangements \(d_n\) are there on a finite set with
\(n\) elements.

\subsection{Recursive approach}
\label{develop--math2276:ROOT:page--useful-results.adoc---recursive-approach}

Firstly, note that we have two base cases

\begin{equation*}
d_1 = 0 \quad \text{and} \quad d_2 = 1
\end{equation*}

both of which are easy to verify.\newline
Now, consider the \(n\)’th element, there are two cases

\begin{itemize}
\item \(n\) swaps with some other element \(i\). Then
    the remaining \(n-2\) elements need to be deranged. In this
    case, there are \((n-1)d_{n-2}\) total derangements.
\item \(n\) does not swap with some other element \(i\).
    In this case there are again \(n-1\) cases for
    \(i\). However, \(n\) can no longer be placed at
    position \(i\). Not to worry, note that if we place
    \(n\) at \(i\), we can treat it along with the
    \(n-2\) other elements as there own set none of which can be
    assigned to their current position. Therefore in this case, there are
    \((n-1)d_{n-1}\) total derangements.
\end{itemize}

Therefore, our recurrence relation is as follows

\begin{equation*}
d_n = (n-1)d_{n-1} + (n-1)d_{n-2} = (n-1)(d_{n-1} + d_{n-2})
\end{equation*}

Note that this is not a linear system. However, a magical trick will be
used

\begin{equation*}
\begin{aligned}
    d_n - nd_{n-1}
    &= -d_{n-1} + (n-1)d_{n-2}\\
    &= -(d_{n-1} - (n-1)d_{n-2})\\
    &= (-1)^2(d_{n-2} - (n-2)d_{n-3})\\
    &\vdots\\
    &= (-1)^k(d_{n-k} - (n-k)d_{n-k-1})\\
    &= (-1)^{n-2}(d_{2} - 2d_{1})\\
    &= (-1)^{n}\\\end{aligned}
\end{equation*}

and another magic trick

\begin{equation*}
\frac{d_n}{n!} - \frac{d_{n-1}}{(n-1)!} = \frac{(-1)^n}{n!}
\end{equation*}

And finally sum over all \(m=2 \vdots n\)

\begin{equation*}
\sum_{m=2}^n \frac{d_m}{m!} - \frac{d_{m-1}}{(m-1)!}
 \frac{d_m}{m!} - \frac{d_{1}}{1!}
 \sum_{m=2}^n \frac{(-1)^m}{m!}
 \sum_{m=0}^n \frac{(-1)^m}{m!}
\end{equation*}

Finally, since \(d_1 = 0\)

\begin{equation*}
d_n = n!\sum_{m=0}^n \frac{(-1)^m}{m!}
\end{equation*}

\begin{admonition-note}[{}]
Consider the proportion of derangements to total permutations as
\(n\) tends to infinity. This is surprising, this proportion
tends to \(e^{-1} \approx 0.368\). Surprising huh? Very
unexpected that it doesn’t approach 0.
\end{admonition-note}

\subsection{Proof with matrix inversion}
\label{develop--math2276:ROOT:page--useful-results.adoc---proof-with-matrix-inversion}

Consider building the set of permutations. For all
\(k= 0\ldots n\), take \(k\) of the elements in
\(n\) and derange the elements. Clearly, this covers all
permutations once. Therefore

\begin{equation*}
n! = \sum_{k=0}^n \binom{n}{k} d_k
\end{equation*}

Hmmmm, doesn’t this seem familiar? Of course it does, this is the
special matrix inversion. Therefore, we can easily solve for
\(d_n\) as

\begin{equation*}
\begin{aligned}
    d_n
    &= \sum_{k=0}^n (-1)^{n+k} \binom{n}{k} k!\\
    &= \sum_{k=0}^n (-1)^{n-k} \frac{n!}{(n-k)!}\\
    &= \sum_{k=0}^n (-1)^{k} \frac{n!}{k!} \quad \text{by reversing the order of summation}\\
    &= n!\sum_{k=0}^n \frac{(-1)^k}{k!} \quad \text{by reversing the order of summation}\\\end{aligned}
\end{equation*}

as previously proved.

\subsection{Using the principle of inclusion-exclusion}
\label{develop--math2276:ROOT:page--useful-results.adoc---using-the-principle-of-inclusion-exclusion}

As previously stated, the number of derangements is the number
permutations with no fixed points. This problem lends itself directly to
the inverted principle of inclusion-exclusion where we consider none of
the elements being a fixed point. For each \(k=0\ldots n\) we
can choose \(k\) elements to be fix points and count the
number the of permutations of the remaining \(n-k\) elements.
Therefore, the number of permutations which have no fixed points

\begin{equation*}
\begin{aligned}
    d_n
    &= \sum_{k=0}^n (-1)^k \binom{n}{k} (n-k)!\\
    &= \sum_{k=0}^n (-1)^k \frac{n!}{k!}\\
    &= n!\sum_{k=0}^n \frac{(-1)^k}{k!}\end{aligned}
\end{equation*}

We get the same result and much quicker.

\section{Catalan numbers}
\label{develop--math2276:ROOT:page--useful-results.adoc---catalan-numbers}

Catalan numbers, \(\{C_n\}\) are defined by the following
recurrence relation

\begin{equation*}
C_{n+1} = \sum_{k=0}^n C_k C_{n-k} = C_0C_n + C_1C_{n-1} + \cdots + C_nC_0
\end{equation*}

and boundary condition \(C_0 = 1\).

\subsection{Closed form}
\label{develop--math2276:ROOT:page--useful-results.adoc---closed-form}

Consider the following function

\begin{equation*}
A(x) = \sum_{i=0}^{\infty} C_i x^i
\end{equation*}

Notice that

\begin{equation*}
\begin{aligned}
    A(x) - 1
    &= \sum_{i=1}^{\infty} C_i x^i\\
    &= x\sum_{i=0}^{\infty} C_{i+1} x^i\\
    &= x\sum_{i=0}^{\infty} \sum_{k=0}^{i} C_{k}C_{i-k} x^i\\
    &= x\sum_{i=0}^{\infty} \sum_{k=0}^{i} C_{k}C_{i-k} x^{k}x^{i-k}\\
    &= x\sum_{k=0}^{\infty} \sum_{i-k=0}^{\infty} C_{k}C_{i-k} x^{k}x^{i-k}\\
    &= x\sum_{k=0}^{\infty} \sum_{j=0}^{\infty} C_{k}C_{j} x^{k}x^{j}\\
    &= x \left(\sum_{k=0}^{\infty} C_k x^{k}\right) \left(\sum_{j=0}^{\infty} C_j x^{j}\right)\\
    &= x A(x)^2\end{aligned}
\end{equation*}

Therefore
\(\displaystyle A(x) = \frac{1 \pm \sqrt{1 - 4x}}{2x}\). We
take the negative solution since we want that
\(A(0) = \lim_{x\rightarrow 0} A(x) = 1\). Then

\begin{equation*}
\begin{aligned}
    A(x)
    &= \frac{1 - \sqrt{1 - 4x}}{2x}\\
    &= \frac{1}{2x}\left[1 - \sqrt{1-4x}\right]\\
    &= \frac{1}{2x}\left[1 - \left( 1
            - \frac{1}{2}\frac{4x}{1!}
            - \frac{1}{2}\frac{1}{2}\frac{(4x)^2}{2!}
            - \frac{1}{2}\frac{1}{2}\frac{3}{2}\frac{(4x)^3}{3!}
            - \frac{1}{2}\frac{1}{2}\frac{3}{2}\frac{5}{2}\frac{(4x)^4}{4!}
            - \ldots
        \right)\right]\\
    &= \frac{1}{2x}\left[
            \frac{1}{2}\frac{4x}{1!}
            + \frac{1}{2}\frac{1}{2}\frac{(4x)^2}{2!}
            + \frac{1}{2}\frac{1}{2}\frac{3}{2}\frac{(4x)^3}{3!}
            + \frac{1}{2}\frac{1}{2}\frac{3}{2}\frac{5}{2}\frac{(4x)^4}{4!}
            + \ldots
        \right]\\
    &=  1
            + \frac{1}{2}\frac{(4x)^1}{2!}
            + \frac{1}{2}\frac{3}{2}\frac{(4x)^2}{3!}
            + \frac{1}{2}\frac{3}{2}\frac{5}{2}\frac{(4x)^3}{4!}
            + \ldots\end{aligned}
\end{equation*}

And hence the coefficient of \(x^n\) is

\begin{equation*}
C_n = 4^n\frac{1\cdot 3 \cdot 5\cdots (2n-1)}{2^n (n+1)!}
    = 2^n\frac{(2n-1)!!}{(n+1)!}
    = \frac{2n!}{n!(n+1)!}
    = \frac{1}{n+1}\binom{2n}{n}
\end{equation*}

\subsection{Polygon triangulation}
\label{develop--math2276:ROOT:page--useful-results.adoc---polygon-triangulation}

A convex polygon with \(n+2\) sides can be split into triangle
in exactly \(C_n\) ways.

\begin{example}[{Proof}]
Clearly \(C_1 = 1\) and we define
\(C_0=1\). (Notice that \(C_1\) follows also follows
from the recurrence relation).

Now consider a polygon with \(n+3\) sides and focus on
vertices \(1\) and \(n+3\). Then the set of all
triangulations can be partitioned based on which triangle the first and
last points form. Suppose that they join to make a triangle with the
\(k+2\) point (where \(1 \leq k \leq n-1\)). Then,
the initial polygon is split into triangle \((1, k+2, n+3)\)
and polygons \(A(1, 2, \ldots k+2)\) and
\(B(k+2, k+3, \ldots, n+3)\) which have \(k+2\) and
\(n-k+2\) sides respectively. Then the number of
triangulations when point \(k+2\) is chosen is
\(C_kC_{n-k}\).

Now consider the case that \(k=0\) that is triangle
\((1, 2, n+3)\) is formed. Then a polygon with
\(n+2\) sides is left over and the total number of
triangulations is \(C_n = C_0C_n\). Similarly, when
\(k = n\) we get \(C_n = C_nC_0\) triangulations.

By adding over all the possible selections of the triangle we get the
desired formula. ◻
\end{example}

\subsection{Up-right paths such that \(up \leq right\)}
\label{develop--math2276:ROOT:page--useful-results.adoc---up-right-paths-such-that-latex-backslashup-latex-backslashleq-right-latex-backslash}

This section has been omitted.
\end{document}
