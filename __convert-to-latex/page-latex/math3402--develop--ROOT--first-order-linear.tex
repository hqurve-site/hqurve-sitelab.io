\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{First order Linear}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
The general form of a first order linear PDE of a function \(u\) of two variables,
\(x\) and \(y\) is

\begin{equation*}
a(x,y) \frac{\partial u}{\partial x}
+ b(x,y) \frac{\partial u}{\partial y}
+ c(x,y)u
= f(x, y)
\end{equation*}

\section{Method of characteristics}
\label{develop--math3402:ROOT:page--first-order-linear.adoc---method-of-characteristics}

When \(a\) and \(b\) are constants and the function \(c\) is \(0\),
we get the following equation

\begin{equation*}
a \frac{\partial u}{\partial x}
+ b \frac{\partial u}{\partial y}
= f(x, y)
\end{equation*}

which has a general solution of

\begin{equation*}
u = \int f(a\xi + b\eta, b\xi - a\eta) \ d\xi + g(\eta)
\quad\text{where}\
\begin{pmatrix} x \\ y \end{pmatrix}
=
\begin{pmatrix} a & b \\ b & -a \end{pmatrix}
\begin{pmatrix} \xi \\ \eta \end{pmatrix}
\end{equation*}

\(g(\eta)\) is an arbitrary function of \(\eta\)
and \(a^2 + b^2 \neq 0\). Alternatively, this solution could be written
as

\begin{equation*}
u = \int \frac{f\left(\frac{a\xi + b\eta}{a^2+b^2}, \frac{b\xi - a\eta}{a^2 + b^2}\right)}{a^2 + b^2} \ d\xi + g(\eta)
\quad\text{where}\
\begin{pmatrix} \xi \\ \eta \end{pmatrix}
=
\begin{pmatrix} a & b \\ b & -a \end{pmatrix}
\begin{pmatrix} x \\ y \end{pmatrix}
\end{equation*}

\begin{admonition-note}[{}]
The factor of \(a^2+b^2\) arises since \(\xi\) has been scaled
by factor \(a^2+b^2\) since the original equation
\(
\begin{pmatrix} \xi \\ \eta \end{pmatrix}
=
\frac{1}{a^2+b^2}
\begin{pmatrix} a & b \\ b & -a \end{pmatrix}
\begin{pmatrix} x \\ y \end{pmatrix}
\)
\end{admonition-note}

\begin{example}[{Proof}]
Firstly, recognize that the equation bears strong resemblance to the
chain rule. Namely, if \(x\) and \(y\) are both a function of \(\xi\) (yet to determined)
where \(\frac{\partial x}{\partial \xi} = a\) and \(\frac{\partial y}{\partial \xi} = b\),
we see that

\begin{equation*}
\frac{\partial u}{\partial \xi}
=
\frac{\partial u}{\partial x} \frac{\partial x}{\partial \xi}
+  \frac{\partial u}{\partial y} \frac{\partial y}{\partial \xi}
=
a\frac{\partial u}{\partial x}
+ b \frac{\partial u}{\partial y}
= f(x,y)
\end{equation*}

Now, in order to fully define \((x, y)\), we need another variable. Let it be \(\eta\)
such that \((\eta,\xi)\) is an orthoganal basis (integration/differentiation acts weird if bases aren't orthoganal).
Then, from our initial defintion of \(\xi\), we get

\begin{equation*}
x = a\xi + f_1(\eta) \quad\text{and}\quad y=b\xi + f_2(\eta)
\end{equation*}

Since \((\eta, \xi)\) is a basis, \(f_1,f_2\) are linear functions and hence
\(f_1(\eta) = \kappa_1\eta\) and \(f_2(\eta) = \kappa_2\eta\). And since \(\eta\) and
\(\xi\) are orthoganal,

\begin{equation*}
\begin{pmatrix}a \\ b\end{pmatrix}
\cdot
\begin{pmatrix}\kappa_1 \\ \kappa_2\end{pmatrix}
= a\kappa_1 + b\kappa_2 = 0
\end{equation*}

For convenience we set \(\kappa_1 = b\) and \(\kappa_2 =-a\). Now, by substituting
our functions for \(x\) and \(y\), we get that

\begin{equation*}
\frac{\partial u}{\partial \xi}  = f(x,y) = f(a\xi + b\eta, b\xi - a\eta)
\end{equation*}

and by integrating both sides, we get the general solution for \(u\) to be

\begin{equation*}
u = \int f(a\xi + b\eta, b\xi - a\eta) \ d\xi + g(\eta)
\end{equation*}

where

\begin{equation*}
\begin{pmatrix} x \\ y \end{pmatrix}
=
\begin{pmatrix} a & b \\ b & -a \end{pmatrix}
\begin{pmatrix} \xi \\ \eta \end{pmatrix}
\end{equation*}

which is invertible iff \(a^2 + b^2 \neq 0\).
\end{example}

\section{Characteristic Line}
\label{develop--math3402:ROOT:page--first-order-linear.adoc---characteristic-line}

Note that the equation can be more compactly written as

\begin{equation*}
(a \hat\imath + b\hat\jmath)\cdot \nabla u = f(x, y)
\end{equation*}

From this, we can see that if \(f=0\), \(\nabla u \perp (a\hat\imath + b\hat\jmath)\). Therefore,
if we have a solution \(u_0\), we can add any \(u_c\) such that
\(\nabla u_c \perp (a\hat\imath + b\hat\jmath)\). We call any
line parallel to this vector, the \emph{characteristic line}.
\end{document}
