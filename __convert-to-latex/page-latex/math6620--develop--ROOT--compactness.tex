\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Compactness}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(X\) be a topological space.
An \emph{open cover} of \(X\) is a collection of open sets \(\{U_\alpha\}_{\alpha \in I}\)
such that \(X = \bigcup U_{\alpha}\).
It is also notable that if \(A\subseteq X\) has the subspace topology,
the collection of open sets \(\{U_\alpha\}_{\alpha \in I}\) is an open cover of \(A\)
iff \(A \subseteq \bigcup U_\alpha\).

We say that the open cover \(\{U_\alpha\}_{\alpha \in I}\) of \(X\)
has a \emph{finite subcover} if there exists a finite subset \(J \subseteq I\)
such that \(X = \bigcup_{\alpha \in J} U_\alpha\).
We call \(X\) \emph{compact} if all of its open covers have a finite subcover.

Due to the symmetry of closed and open sets, we may rephrase compact
using closed sets.

Let \(\mathcal{C}\) be a collection of subsets of \(X\).
We say that \(\mathcal{C}\) has the \emph{finite intersection property}
if for any \(n \geq 1\) and \(C_1, C_2, \ldots C_n \in \mathcal{C}\),
\(C_1 \cap C_2 \cap \cdots C_n \neq \varnothing\).

\begin{theorem}[{Equivalent definitions of compact}]
Let \(X\) be a topological space.
Then, the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(X\) is compact
\item For all collections of closed subsets \(\mathcal{C}\) which satisfy the finite intersection property,
    \(\bigcap_{C \in \mathcal{C}} C \neq \varnothing\)
\item For all collections of subsets, \(\mathcal{C}\) which satisfy the finite intersection property,
    \(\bigcap_{C \in \mathcal{C}} \closure{C} \neq \varnothing\)
\end{enumerate}

\begin{proof}[{}]
We prove that \((1)\) is equivalent to the contrapositive of \((2)\).
Notice that by DeMorgan's Laws,

\begin{equation*}
\bigcap_{C \in \mathcal{C}} C = \varnothing
\iff X = X - \bigcap_{C \in \mathcal{C}} C = \bigcup_{C \in \mathcal{C}} (X-C)
\end{equation*}

Now,

\begin{itemize}
\item If \(X\) is compact and \(\mathcal{C}\) is a collection of closed subsets whose intersection is empty,
    then the \(X-C\) form an open cover of \(X\) and hence has a finite subcover.
    Therefore, there exists \(C_1, \ldots C_n \in \mathcal{C}\) such that
    
    \begin{equation*}
    \bigcup_{i=1}^n (X-C_i) = X \implies \bigcap_{i=1}^n C_i = \varnothing
    Therefore the stem:[\mathcal{C}] does not satisfy the finite intersection property.
    \end{equation*}
\item If \((2)\) is true and \(\mathcal{U}\) is an open cover of \(X\),
    \(\bigcap \{X - U \ | \ U \in \mathcal{U}_\alpha\} = \varnothing\)
    and hence there exists \(U_1, \ldots U_n\) such that
    
    \begin{equation*}
    \bigcap_{i=1}^n (X-U_i) = \varnothing \implies \bigcup_{i=1}^n U_i = X
    \end{equation*}
    
    and hence \(X\) is compact.
\end{itemize}

Now, we need to prove equivalence with \((3)\).
Note that \((2)\) readily implies \((3)\) since \(\closure(A) \supseteq A\) and \(\closure(A)\) is closed.
Conversely if \((3)\) is true, \((2)\) is also true since the closure of a closed set is itself.

Therefore, we have proven the desired equivalences
\end{proof}
\end{theorem}

These equivalences are helpful when it is easier to work in terms of closed sets.

\section{Common looking results}
\label{develop--math6620:ROOT:page--compactness.adoc---common-looking-results}

\begin{proposition}[{Closed subsets}]
Let \(X\) be compact and \(A \subseteq X\) be closed.
Then, \(A\) is also compact.

\begin{proof}[{}]
Let \(\{U_\alpha\}_{\alpha \in I}\) be a cover for \(A\).
Then, since \(X-A\) is open, \(\{U_{\alpha}\}_{\alpha \in I} \cup\{X-A\}\)
is a collection of open sets which covers \(X\).
Therefore, there exists a finite subcover
\(\{U_{\alpha_1}, \ldots U_{\alpha_n}, (X-A)\}\).
Note that the presence of \(X-A\) in the finite subcover does not affect whether it is finite.
Now, since \(A \cap (X-A) = \varnothing\), we must have that

\begin{equation*}
A \subseteq U_{\alpha_1} \cup \cdots U_{\alpha_n}
\end{equation*}

Therefore our cover has a finite subcover and since the choice of cover was arbitrary,
\(A\) is compact.
\end{proof}
\end{proposition}

\begin{theorem}[{Hausdorff spaces}]
Let \(X\) be Hausdorff and \(A \subseteq X\) be compact.
Then,

\begin{enumerate}[label=\arabic*)]
\item for each \(x \in X-A\), there exists open \(U, V \subseteq X\)
    such that
    
    \begin{equation*}
    x \in U, \quad A \subseteq V, \quad U \cap V = \varnothing
    \end{equation*}
\item \(A\) is closed in \(X\).
\end{enumerate}

\begin{proof}[{}]
We prove the first statement first.
Focus on some fixed \(x \in X-A\).
Then, since \(X\) is Hausdorff, for each \(y \in A\), there exists open \(U_y, V_y\)
such that \(x\in U_y\) and \(y\in V_y\).
Therefore, the \(V_y\) form an open cover of \(A\)
and by compactness, there exists a finite subcover
\(\{V_y\}\) for some \(y_1, \ldots y_n \in A\).
Let \(V = \bigcup_{i=1}^n V_{y_i}\) and \(U = \bigcap_{i=1}^n U_{y_i}\).
Then, both are open sets and, \(A \subseteq V\) and \(x \in U\).
Also

\begin{equation*}
V\cap U
= \bigcup_{i=1}^n V_{y_i} \cap U
= \bigcup_{i=1}^n \left(V_{y_i} \cap \bigcap_{j=1}^n U_{y_j} \right)
= \varnothing
\end{equation*}

since \(V_{y_i} \cap U_{y_i} \varnothing\).
Therefore we have found the desired \(U\) and \(V\).

For the second statement, for each \(x \in X-A\), we can find open \(U\)
such that \(x \in U \subseteq X-A\) (from the first statement).
Therefore \(X-A\) is open and \(A\) is closed as desired.
\end{proof}
\end{theorem}

\section{Nice Theorems}
\label{develop--math6620:ROOT:page--compactness.adoc---nice-theorems}

\begin{theorem}[{Continuous images}]
Let \(X\) be compact and \(f: X\to Y\) be continuous.
Then, \(f(X)\) is compact.

\begin{proof}[{}]
Let \(\{V_\alpha\}_{\alpha \in I}\)  form an open cover of \(f(X)\).
Then, \(\{f^{-1}(V_\alpha)\}_{\alpha \in I}\) forms an open cover of \(X\)
since for each \(x\), \(f(x)\) is in at least one of the \(V_\alpha\).
By compactness of \(X\), there exists a finite subcover
\(\{f^{-1}(V_{\alpha_1}), \ldots f^{-1}(V_{\alpha_n})\}\)
of \(X\).

We want to show that \(\{V_{\alpha_1}, \ldots V_{\alpha_n}\}\) forms a subcover of \(f(X)\).
Consider arbitrary \(f(x) \in f(X)\). Then, \(x \in f^{-1}(V_{\alpha_i})\)
for some \(i\) and hence \(f(x) \in V_{\alpha_i}\) for some \(i\).
Therefore we have found a finite subcover.

Since the choice of cover was arbitrary, \(f(X)\) is compact.
\end{proof}
\end{theorem}

\begin{theorem}[{Continuous bijections are homeomorphisms}]
Let \(X\) be compact and \(Y\) be Hausdorff.
Let \(f:X\to Y\) be continuous and bijective.
Then, \(f\) is a homeomorphism.

\begin{proof}[{}]
For \(f\) to be a homeomorphism, we need only show that \(f^{-1}\)
is continuous; ie \(f\) is an open/closed map.

Consider arbitrary closed \(U \subseteq X\).
Then \(U\) is also compact and by continuity of \(f\),
\(f(U)\) is also compact.
Since \(Y\) is Hausdorff and \(f(U) \subseteq Y\) is compact,
we have that \(f(U)\) is closed.
Since \(f\) is a closed map, \(f^{-1}\) is continuous
and \(f\) is a homeomorphism as desired.
\end{proof}
\end{theorem}

\begin{theorem}[{Uniform Continuity Theorem}]
Let \(f:X \to Y\) be a continuous where \((X, d_X)\) is a compact metric space
and \((Y, d_Y)\) is a metric space (not necessarily compact).
Then, \(f\) is uniformly continuous. That is

\begin{equation*}
\forall \varepsilon > 0: \exists \delta > 0: \forall x_1, x_2 \in X: d_X(x_1, x_2) < \delta
\implies d_Y(f(x_1), f(x_2)) < \varepsilon
\end{equation*}

\begin{proof}[{}]
Consider the open cover \(\left\{f^{-1}\left(B\left(y, \frac{\varepsilon}{2}\right)\right) \ | \ y \in Y\right\}\)
of \(X\).
Then, by the Lebesgue number lemma, there exists a \(\delta > 0\)
such that any \(C \subseteq X\) with diameter less than \(\delta\),
\(C\) is contained in one of the \(f^{-1}\left(B\left(y, \frac{\varepsilon}{2}\right)\right)\).

So, consider \(x_1, x_2\) with \(d_X(x_1, x_2) < \delta\).
Then, the diameter of \(\{x_1, x_2\}\) is also less than \(\delta\)
and there exists a \(y\) such that
\(\{x_1, x_2\} \subseteq f^{-1}\left(B\left(y, \frac{\varepsilon}{2}\right)\right)\)
and hence

\begin{equation*}
f(x_1) , f(x_2) \in B\left(y, \frac{\varepsilon}{2}\right)
\implies d_Y(f(x_1), f(x_2)) \leq d_Y(f(x_1), y) + d_Y(y, f(x_2)) < \varepsilon
\end{equation*}
\end{proof}
\end{theorem}

\begin{theorem}[{Uncountable}]
Let \(X\) be a Hausdorff and compact topological space.
Then, if no singleton is open, \(X\) is uncountable.

\begin{admonition-note}[{}]
The condition that no singleton is open may be equivalently be stated
as \(X\) has no isolated points, or all points in \(X\) are limit points.
\end{admonition-note}

\begin{proof}[{}]
Note that if \(X\) is finite, it must have the discrete topology and hence
all points are isolated. So, \(X\) is infinite.

Suppose BWOC that \(X\) is countable and let \(X = \{x_1, x_2, \ldots\}\).
For convenience, we also define an ordering on \(X\) according to the aforementioned
list. Note that \(X\) is well ordered since it can be embedded into \(\mathbb{N}\).

First define \(y_1 = x_1\).
Then since \(\{x_1\}\) is closed, it is also compact
and there exists an non-empty open \(U_1\) such that \(x_1 \notin \closure U_1\).
For example, we can construct \(U_1\) to contain \(x_2\).
Since \(x_2\) is a limit point, \(U_1 \cap X = U_1\) must have infinitely many elements.

Next, define \(y_2 = \min U_1\). Since \(\{x \in X \ | \ x \leq y_2\}\) is
finite, it is closed (and hence compact),
there exists non-empty open \(U_2\) such that \(\{x \in X \ | \ x \leq y_2\}\cap \closure U_2 = \varnothing\)
and \(U_2 \subseteq U_1\).

We build a sequence of \(y_n\) and \(U_n\) inductively.
Suppose that we have \(y_1 \leq y_2 \leq \cdots y_n\)
and \(U_1 \supseteq U_2 \supseteq \cdots U_n\)
with

\begin{itemize}
\item \(\{x \in X \ | \ x \leq y_k\} \notin \closure U_k\)
\item \(y_k = \min U_{k-1}\) for \(k=2\ldots n\)
\end{itemize}

Then, we can define \(y_{n+1} = \min U_n\).
Since \(\{x \in X \ | \ x \leq y_{n+1}\}\) is finite, it is closed and hence compact.
Therefore we can determine disjoint, non-empty, open \(V, W\) such that \(\{x \in X \ | \ x \leq y_{n+1}\}\subseteq V\).
We can therefore take \(U_{n+1} = W \cap U_n\) and hence \(\closure(U_{n+1}) \subseteq \closure W \subseteq X-V \subseteq X - \{x \in X \ | \ x \leq y_{n+1}\}\).

Therefore we have a constructed a sequence of nested \(U_n\).
Therefore any finite intersection of \(\closure U_n\) is non-empty.
However, since for each \(N > 0\), there exists \(U_n\) with \(x_1, \ldots x_N \notin \closure U_n\),

\begin{equation*}
\bigcap \closure U_n = \varnothing
\end{equation*}

This contradicts the fact that \(X\) is compact and hence our assumption that \(X\) as countable was incorrect.
\end{proof}
\end{theorem}

\section{Other types of compactness}
\label{develop--math6620:ROOT:page--compactness.adoc---other-types-of-compactness}

Let \(X\) be a topological space. Then, we define the following
types of compactness

\begin{description}
\item[Limit-point compact] Each infinite subset of \(X\) has a limit point.
\item[Sequentially compact] Each sequence has a convergent subsequence.
\end{description}

\begin{theorem}[{}]
Let \(X\) be a compact topological space. Then, it is limit-point compact.

\begin{proof}[{}]
Suppose BWOC that \(A \subseteq X\) has no limit points.
Then \(A\) is closed and hence compact.
Also, for each \(x \in A\), there exists open \(U_x\)
such that \(A \cap U_x = \{x\}\).
Therefore the \(U_x\) form an open cover for \(A\).
However, there is no finite subcover since each \(U_x\) contains at
most one element of \(A\).
Therefore our assumption that \(A\) has no limit points is false.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(X\) be a limit-point compact metric space.
Then, \(X\) is sequentially compact.

\begin{proof}[{}]
Let \(\{x_n\}\) be a sequence and \(A = \{x \in X \ | \ \exists n: x_n = x\}\).
Then, if \(A\) is finite, by the pigeon hole principle, there exists \(x \in X\)
which appears infinitely many times in the sequence. Hence we have found a convergent subsequence.
Otherwise, suppose that \(A\) is infinite.
Then, since \(X\) is limit-point compact, there exists \(x \in \limit A\).

For each \(k \geq 1\), define \(x_{n_k}\) to be such that

\begin{equation*}
n_k = \min \left\{n \geq 1 \ \middle| \ n > n_{k-1}, x_n \in A \cap B\left(x, \frac{1}{k}\right)\right\}
\end{equation*}

Then, \(\{x_{n_k}\}_{k=1}^\infty\) is a convergent subsequence of \(\{x_n\}\).
\end{proof}
\end{theorem}

\subsection{Metric spaces}
\label{develop--math6620:ROOT:page--compactness.adoc---metric-spaces}

When dealing with metric spaces, all three types of compactness are equivalent.
However, we must be careful in any proof proceeding the proof of equivalence.

For convince, we define two properties for metric space \(X\).

\begin{description}
\item[Completeness] \(X\) is complete iff each cauchy sequence is convergent
\item[Totally bounded] For each \(\varepsilon > 0\), \(X\) can be covered by finitely
    many balls with radius \(\varepsilon\).
\end{description}

If \(X\) is sequentially compact, these two properties hold.

\begin{lemma}[{}]
Let \(X\) be a sequentially compact metric space.
Then, \(X\) is complete.

\begin{proof}[{}]
Let \(\{x_n\}_{n=1}^\infty\) be Cauchy. Then, since
\(X\) is sequentially compact, there exists a convergent subsequence \(\{x_{n_k}\}_{k=1}^\infty\)
which converges to some \(x\).

Let \(\varepsilon > 0\). By the convergence of the subsequence, there exists \(N_1 > 0\) such that
for each \(k \geq N_1\), \(d(x_{n_k}, x) < \frac{\varepsilon}{2}\).
Also, since \(\{x_n\}_{n=1}^\infty\) is cauchy, there exists \(N_2 > 0\) such that
for each \(i,j \geq N_2\), \(d(x_i, x_j) < \frac{\varepsilon}{2}\).

Let \(N = \max(n_{N_1}, N_2)\). Then, for each \(n \geq N\),

\begin{equation*}
d(x_n, x) \leq d(x_n, x_{n_N}) + d(x_{n_N}, x) < \varepsilon
\end{equation*}

since \(n_N \geq N_2\) and \(n_N \geq n_{N_1}\) by the monotonicity of \(n_1, n_2, \ldots\).
Therefore we have proven that \(\{x_n\}_{n=1}^\infty\) converges to \(x\).
\end{proof}
\end{lemma}

\begin{lemma}[{}]
Let \(X\) be a sequentially compact metric space.
Then, \(X\) is totally bounded. That is, for each \(\varepsilon > 0\),
there exists \(n\geq 1\) and \(x_1, \ldots x_n \in X\)
such that

\begin{equation*}
X = \bigcup_{i=1}^n B(x_i, \varepsilon)
\end{equation*}

\begin{proof}[{}]
Suppose that this is impossible.
Then, we can inductively develop an infinite sequence of \(\{x_i\}\)

\begin{equation*}
x_{k+1} \notin \bigcup_{i=1}^k B(x_1, \varepsilon)
\end{equation*}

Now, since this is an infinite sequence, by sequential compactness, there is a convergent
subsequence. This subsequence satisfies the same above property and hence we may assume (for convenience)
that \(\{x_i\}\) is converges to \(x \in X\).
Then, there exists an \(N > 0\) such that for each \(i \geq N\), \(x_i \in B\left(x, \frac{\varepsilon}{2}\right)\).
Then, \(d(x_{N+1}, x_N) < \varepsilon\) which implies that \(x_{N+1} \in B(x_N, \varepsilon)\).
Contradiction.
Therefore there exists \(n \geq 1\) and \(x_1, \ldots x_n \in X\) such that
\(\{B(x_i, \varepsilon)\}_{i=1}^n\) is a cover for \(X\).
\end{proof}
\end{lemma}

We can now prove the equivalence of the three types of compactness (for metric spaces).

\begin{theorem}[{}]
Let \(X\) be a metric space. Then, the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(X\) is compact
\item \(X\) is limit point compact
\item \(X\) is sequentially compact
\end{enumerate}

We have already proved \((1) \implies (2) \implies (3)\) for metric spaces.
So, we need only prove \((3) \implies (1)\).

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof is very similar to the proof of compactness of \(\mathbb{R}\).
\end{admonition-note}

Let \(X\) be a sequentially compact metric space.

Now, let \(\mathcal{A}\) be a collection of closed subsets of \(X\) satisfying the finite intersection property.
Let \(\mathcal{A}_0 = \mathcal{A}\). We would inductively determine a sequence
of collections of subsets satisfying FIP \(\{\mathcal{A}_i\}_{i=1}^\infty\) and accompanying
\(\{y_i\}_{i=1}^\infty\) such that

\begin{equation*}
\mathcal{A}_{k} = \left\{B\left(y_k, \frac{1}{k}\right) \cap A \ | \ A \in \mathcal{A}_{k-1}\right\}
\end{equation*}

for \(k=1,2,\ldots\). Suppose that we have \(\mathcal{A}_{n-1}\) satisfying the FIP.
Then, since \(X\) is totally bounded,
there exists \(x_1, \ldots x_N \in X\) such that \(B\left(x_i, \frac{1}{n}\right)\)
covers \(X\).
We need to show that at least one of the \(x_i\) are suitable for choice as \(y_n\).
If for each \(i\) there exists \(A^{i}_{1}, \ldots A^{i}_{m_i} \in \mathcal{A}_{n-1}\) with
\(A^i_{1}\cap \cdots A^{i}_{m_i} \cap B\left(x_i, \frac{1}{n}\right) = \varnothing\), we have
that

\begin{equation*}
\bigcap_{i=1}^N \bigcap_{j=1}^{m_i} A_i
\subseteq \bigcap_{i=1}^N \left(X - B\left(x_i, \frac{1}{n}\right)\right)
= X - \bigcup_{i=1}^N B\left(x_i, \frac{1}{n}\right)
= X-  X = \varnothing
\end{equation*}

and hence the FIP of \(\mathcal{A}_{n-1}\) is invalidated.
Therefore, there exist \(y_n=x_i\) such that \(\mathcal{A}_{n}\) satisfies the FIP.
Therefore, by induction, we have proven the existence of the sequences of \(\{\mathcal{A}_i\}_{i=1}^\infty\)
and \(\{y_i\}_{i=1}^\infty\).

Note that for each \(0 \leq n \leq m\), \(B\left(y_n, \frac{1}{n}\right) \cap B\left(y_m, \frac{1}{m}\right)\neq \varnothing\)
which implies there exists \(x\) in each of the balls and

\begin{equation*}
d(y_n, y_m) \leq d(x, y_n) + d(x, y_m) < \frac{1}{n} + \frac{1}{m} \leq \frac{2}{n}
\end{equation*}

and hence \(\{y_n\}\) is cauchy and hence converges to some \(y \in X\).

We claim that \(y \in A\) for each \(A \in \mathcal{A}\).
Consider any fixed \(A \in \mathcal{A}\) and \(U = B\left(y, \varepsilon\right)\).
Then, there exists \(m \geq 0\) such that \(y_m \in U\)
and \(B\left(y_m, \frac{1}{m}\right) \subseteq U\).
We may, for example choose \(m\) by first ensuring that \(d(y_m, y) < \frac{\varepsilon}{2}\) and then ensuring that \(\frac{1}{m} \leq \frac{\varepsilon}{2}\).
Then

\begin{equation*}
\varnothing \neq A\cap \bigcap_{i=1}^m B\left(y_i, \frac{1}{i}\right)
\subseteq A \cap B\left(y_m, \frac{1}{m}\right)
\subseteq A \cap U
\end{equation*}

and \(A\cap U \neq \varnothing\). Then, since the choice of \(\varepsilon > 0\) was arbitrary,
\(y \in \closure A = A\) for each \(A \in \mathcal{A}\).
Therefore \(\bigcap_{A \in \mathcal{A}} A \neq \varnothing\)
and \(X\) is compact.
\end{proof}
\end{theorem}

\section{Real numbers}
\label{develop--math6620:ROOT:page--compactness.adoc---real-numbers}

From previous courses, we stated that a subset of real numbers is compact iff it is closed and bounded.
Here we prove that the definition of compact in the real numbers is the same as the actual definition of compactness.

\begin{lemma}[{Nested closed subsets}]
Let \(\{C_i\}_{i=1}^\infty\) be a collection of closed subsets of \([0,1]\) such that

\begin{equation*}
[0,1] \supseteq C_1 \supseteq C_2 \supseteq \cdots \supseteq C_n \supseteq \cdots
\end{equation*}

and \(\operatorname{diam} C_n = \max(C_n) - \min(C_n) \to 0\) .
Then, there exists \(a \in [0,1]\) such that

\begin{equation*}
\bigcap_{i=1}^\infty C_n = \{a\}
\end{equation*}

\begin{proof}[{}]
Let \(a_n = \min C_n\) and \(b_n = \max C_n\);
these exist since the \(C_n\) are closed and bounded.
Then,

\begin{equation*}
0 \leq a_1 \leq a_2 \cdots \cdots \leq a_n \cdots \leq b_n \leq \cdots \leq b_2 \leq b_1 \leq 1
\end{equation*}

Now, let \(a_* = \sup a_n\).
Notice that for each \(n\), \(a_* \in C_n\) since

\begin{equation*}
a_* \in \limit \{a_m \ | \ m \geq n\} \subseteq C_n
\end{equation*}

Therefore

\begin{equation*}
a_* \in \bigcap_{i=1}^\infty C_n
\end{equation*}

Finally, we want to show that for each \(x \in \bigcap_{i=1}^\infty C_n\), \(x = a_*\).
Notice that \(a_n \leq a_*, x \leq b_n\) for each \(n\)
and since \(b_n - a_n \to 0\), \(|x - a_*| \to 0\).
Therefore, we have that

\begin{equation*}
\bigcap_{i=1}^\infty C_n = \{a_*\}
\end{equation*}
\end{proof}
\end{lemma}

\begin{lemma}[{}]
\([a,b] \subseteq \mathbb{R}\) is compact.

\begin{admonition-note}[{}]
This can be generalized to any ordered \(Y\) satisfying the least
upper bound property.
\end{admonition-note}

\begin{proof}[{}]
We would prove that \([0,1]\) is compact for convenience.
Since \([0,1]\) is homeomorphic to \([a,b]\) by
\(x \to (b-a)x + a\), this would suffice.

For fun, we would do this proof using collections of closed sets.
It is almost a direct translation of the standard proof.

Let \(\mathcal{C}\) be a collection of closed subsets of \([0,1]\)
which satisfy the finite intersection property.
Then, at least one of the following two sets also satisfies the finite intersection property

\begin{equation*}
\left\{C \cap \left[0, \frac12\right] \ \middle| \ C \in \mathcal{C}\right\}
, \quad\left\{C \cap \left[\frac12, 1\right] \ \middle| \ C \in \mathcal{C}\right\}
\end{equation*}

Otherwise there would exist two finite subcollections of \(\mathcal{C}\) whose intersection lies in
\(\left( \frac12, 1\right]\) and \(\left[0, \frac12\right)\) respectively.
Let \(\mathcal{C}_1\) be one of the above which satisfies the finite intersection property (prefer the first collection if both satisfy the FIP).
We want to build a sequence of \(\mathcal{C}_n\) inductively.

Let \(\mathcal{C}_0 = \mathcal{C}\) and \(\mathcal{C}_1, \ldots \mathcal{C}_n\) and
\(p_0 = 0\) and \(p_1, \ldots p_n \in \mathbb{Z}\) be such that for \(k=1, \ldots n\)

\begin{equation*}
\mathcal{C}_k = \left\{C\cap \left[\frac{p_k}{2^k}, \frac{p_k+1}{2^k}\right] \ \middle| \ C \in \mathcal{C}_{k-1}\right\}
\end{equation*}

and each \(\mathcal{C}_k\) satisfies the finite intersection property.
Then, it is possible to construct a \(\mathcal{C}_{n+1}\) and \(p_{n+1}\) using the argument which was used in the base case.
Therefore, we have a sequence of \(\left\{\mathcal{C}_n\right\}_{n=0}^\infty\) and \(\left\{p_n\right\}_{n=0}^\infty\) which satifies
the above.

Now, since the \(\left[\frac{p_n}{2^n}, \frac{p_n+1}{2^n}\right]\) form a collection
of nested closed subsets whose diameter tends to \(0\), there exists a single \(x\) which is contained in each interval.
We also want to show that \(x\) is in each of the \(C\in\mathcal{C}_k\).

Consider the neighbourhood \(U = B\left(x, 2^{1-n}\right)\) and notice that \(\left[\frac{p_n}{2^n}, \frac{p_n+1}{2^n}\right] \subseteq U\).
If any of the \(C\cap U = \varnothing\), we would have that \(\mathcal{C}_n\) contains an empty set.
Therefore \(B(x, 2^{1-n}) \cap C \neq \varnothing\) for each \(n \geq 1\) and since \(C\) is closed,
\(x \in C\) for each \(C \in \mathcal{C}\).
Therefore, we have shown that

\begin{equation*}
x \in \bigcap_{C \in \mathcal{C}} C
\end{equation*}

and the intersection is non-empty.
\end{proof}
\end{lemma}

\begin{theorem}[{Heine-Borel Theorem}]
Let \(A \subseteq \mathbb{R}^n\). Then the following are equivalent

\begin{itemize}
\item \(A\) is compact
\item \(A\) is closed and bounded
\end{itemize}

\begin{proof}[{}]
First if \(A\) is closed and bounded, \(A\) is the subset of some \([a,b]^n\) which is compact.
Therefore \(A\) is also compact.

Conversely, if \(A\) is compact, it must be closed since \(\mathbb{R}^n\) is Hausdorff.
Also, it must be bounded otherwise the balls \(B(\vec{0}, n)\) would not have a finite subcover.
\end{proof}
\end{theorem}

\begin{theorem}[{Extreme Value Theorem}]
Let \(X\) be compact and \(f: X\to \mathbb{R}\) be continuous.
Then, there exists \(c, d \in X\) such that

\begin{equation*}
\forall x \in X: f(c) \leq f(x) \leq f(d)
\end{equation*}

\begin{admonition-note}[{}]
This can be generalized to any \(Y\) with the order topology.
\end{admonition-note}

\begin{proof}[{}]
Since \(X\) is compact and \(f\) is continuous,
\(f(X)\) is also compact. Therefore it is closed and bounded.
Notice that \(\sup f(X)\) exists and must be contained in \(f(X)\)
since it is a limit point. Therefore there exists \(d \in X\)
such that \(f(d) = \max f(X)\). We can likewise construct \(c\).
\end{proof}
\end{theorem}

\section{Distances}
\label{develop--math6620:ROOT:page--compactness.adoc---distances}

Let \((X, d)\)  be a metric space and for each non-empty subset
\(A \subseteq X\), we define

\begin{equation*}
d(x, A) = \inf\{d(x, a) \ | \ a \in A\}
\end{equation*}

Then, firstly, \(d\) is a continuous function in \(x\) for any
fixed \(A\).

\begin{proof}[{}]
Consider arbitrary \(x \in X\) and \(\varepsilon > 0\).
Let \(\delta = \varepsilon\).
Now, if \(y \in B(x, \delta)\), we want to show that
\(d(y, A) \in B(d(x, A), \varepsilon)\).

\begin{equation*}
d(y, A) = \inf \{d(y,a) \ | \ a \in A\}
\leq d(x,y) + \inf \{d(x,a) \ | \ a \in A\}
= d(x,y) + d(x,A)
< \delta + d(x,A)
\end{equation*}

Therefore \(d(x, A) - d(y,A) < \delta\).
Similarly, \(d(y,A) - d(x,A) < \delta\).
Therefore, we have the desired result and \(x \to d(x,A)\)
is continuous.
\end{proof}

Recall that the diameter of \(A\) is defined to be

\begin{equation*}
\sup \{d(x,y) \ | \ x,y \in A\}
\end{equation*}

\begin{lemma}[{Lebesgue number lemma}]
Let \((X, d)\) be a compact metric space and
\(\mathcal{A}\) be an open cover.
Then, there exists a \(\delta > 0\)
such that for each \(C \subseteq X\) with diameter less than \(\delta\),
there exists \(A \in \mathcal{A}\) with \(C \subseteq A\).
This \(\delta\) is called the \emph{Lebesgue number} of \(\mathcal{A}\).

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof comes from Munkres. The proof presented
in class relies on the fact that \(X\) is sequentially compact
and does a proof by contradiction.
\end{admonition-note}

We could only look at a finite subcover of \(\mathcal{A}\) since \(X\)
is compact. Let \(A_1, \ldots A_n\) constitute this finite subcover.
Define \(f:X\to \mathbb{R}\) by

\begin{equation*}
f(x) = \frac{1}{n}\sum_{i=1}^n d(x, X - A_i)
\end{equation*}

This function is continuous since it is the sum of continuous functions.
Also, note that the \(d(x, X-A_i)\) term is the ``distance'' \(x\) is from the outside of \(A_i\).
So, if any of the \(d(x, X-A_i) > r\), we know that \(x\) can be enclosed
by a ball of radius \(r\) which is contained entirely in \(A_i\);
hence any set with diameter less than \(r\) which contains \(x\) must also be contained in \(A_i\).
So our aim is to show that there is a \(r\) such that for each \(x\), one of
the \(d(x, X-A_i) > r\).

First notice that there is no \(x\) such that \(f(x) = 0\).
This is true since \(x\) is in at least one \(A_i\) and there exists
an open ball around \(x\) which is contained in \(A_i\).

Also, since \(X\) is compact, \(f(X)\) is also compact.
Since \(0 \notin f(X)\) and \(f(X)\) is closed,
there must be a \(\delta > 0\)
such that \((-\delta, \delta) \cap f(X) = \varnothing\).
We claim \(\delta\) is the number we have been looking for.

Let \(C\) be a subset of \(X\) with diameter strictly less than \(\delta\)
and consider arbitrary \(x \in C\).
Since \(f(x) > \delta\), there exists and \(A_i\) with \(d(x, X-A_i) > \delta\)
and hence \(C \subseteq B(x, \delta) \subseteq A_i\) as desired.
\end{proof}
\end{lemma}

Intuitively, the Lebesgue number lemma asserts that
the subsets in an open cover overlap and they overlap with some minimum distance.
\end{document}
