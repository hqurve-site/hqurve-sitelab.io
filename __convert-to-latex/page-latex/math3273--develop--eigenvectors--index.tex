\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Eigenvectors}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Let \(V\) be a vector space over
field \(\mathbb{F}\) and let \(T \in \mathcal{L}(V)\).
Then we define \((\vec{v}, \lambda) \in (V, \mathbb{F})\) an \emph{eigenpair}
of \(A\) iff

\begin{equation*}
T(\vec{v}) = \lambda\vec{v}
\end{equation*}

where \(\vec{v} \neq \vec{0}\)
Additionally, we say that \(\vec{v}\) is an \emph{eigenvector} and \(\lambda\)
is an \emph{eigenvalue}. Additionally, we call \(\ker(T-\lambda I)\) the \emph{eigenspace}
since it consists of all eigenvectors with eigenvalue \(\lambda\).

\section{Characteristic Polynomial}
\label{develop--math3273:eigenvectors:page--index.adoc---characteristic-polynomial}

Let \(z \in \mathbb{F}\), then the \emph{characteristic polynomial} of \(A \in \mathbb{F}^{n\times n}\) is

\begin{equation*}
p_A(z) = \det(z I - A)
\end{equation*}

Then firstly notice that if \(A\) and \(B\) are similar, they share the same characteristic polynomial

\begin{example}[{Proof}]
Let \(A = P B P^{-1}\), then

\begin{equation*}
\begin{aligned}
p_A(z) = \det(z I - A)
&= \det(zPP^{-1} - PBP^{-1})
\\&= \det[P(z I - B)P^{-1}]
\\&= \det(P)\det(z I - B)\det(P^{-1})
\\&= \det(P)p_B(z) \frac{1}{\det(P)}
\\&= p_B(z)
\end{aligned}
\end{equation*}
\end{example}

Furthermore, since similar matrices have the same underlying linear transformation,
this implies that the characteristic polynomial of a linear transformation is
uniquely defined and independent of the chosen basis. Hence, we can instead talk of the characteristic polynomial
of a linear transformation, if necessary.

Next, notice that \(\lambda\) is a root of \(p_A\) iff \(\lambda\) is an eigenvalue.

\begin{example}[{Proof}]
Direct proof

\begin{equation*}
\begin{aligned}
\lambda \text{ is a root of } p_A
&\iff \ker(\lambda I - A) \neq \varnothing
\\&\iff \exists \vec{v} \in \ker(\lambda I - A): \vec{v} \neq \vec{0}
\\&\iff \exists \vec{v} \neq \vec{0}: (\lambda I - A)\vec{v} = \vec{0}
\\&\iff \exists \vec{v} \neq \vec{0}: A\vec{v} = \lambda \vec{v}
\\&\iff \lambda \text{ is an eigenvalue}
\end{aligned}
\end{equation*}
\end{example}

From this we can discuss multiplicities. The \emph{algebraic multiplicity} of eigenvalue
\(\lambda\) of \(A\), is the multiplicity of the root \(\lambda\) in \(p_A\).
That is, the largest power of \((z-\lambda)\) which divides \(p_A(z)\). Then we immediately
find that the sum of algebraic multiplicities is \(n\) since \(\mathbb{F}\) is a field.

Also, notice that if we write

\begin{equation*}
p_A(z)  = a_0 + a_1 z + \cdots + a_{n-1}z^{n-1} + z^n
\end{equation*}

then \(a_0 = p_A(0) = det(A)\) and \(-a_{n-1}\) which is the sum
of the entries on the diagonal of \(A\) (also known as the \href{https://en.wikipedia.org/wiki/Trace\_(linear\_algebra)}{trace})

\subsection{Block matrices}
\label{develop--math3273:eigenvectors:page--index.adoc---block-matrices}

If \(M = \begin{pmatrix}A & B \\ 0 & C\end{pmatrix}\),
then

\begin{equation*}
p_M(z) = p_A(z) p_C(z)
\end{equation*}

This follows from
\myautoref[{this property}]{develop--math3273:linear-transformations:page--block-matrix.adoc---theorem-determinant-of-upper-triangular-block-matrix}.

\section{Diagonalizable}
\label{develop--math3273:eigenvectors:page--index.adoc---diagonalizable}

Let \(A \in \mathcal{C}^{n\times n}\).
Then we define the following

\begin{description}
\item[Diagonalizable] \(A\) is \emph{diagonalizable} if there exists an invertible matrix \(P\) such that
    \(PAP^{-1}\) is a diagonal matrix.
\item[Orthogonally Diagonalizable] \(A\) is \emph{orthogonally diagonalizable} if there exists an orthogonal matrix \(Q\) such that
    \(QAQ^T\) is a diagonal matrix.
\item[Unitarily Diagonalizable] \(A\) is \emph{unitarily diagonalizable} if there exists an unitary matrix \(U\) such that
    \(UAU^*\) is a diagonal matrix.
\end{description}

Then we have the following theorem. \(A\) is diagonalizable iff there exists a basis
for \(\mathbb{F}^n\) consisting of eigenvectors of \(A\).

\begin{example}[{Proof}]
Firstly, we rewrite the equation as \(AP = PD\).

Now, suppose that such a \(P\) exists and consider the \(r\)'th column in the product \(AP=PD\). On the right
hand we get \(\lambda_r\vec{p}_r \) while on right we get \(A\vec{p}_r\). Therefore the columns of \(P\)
are eigenvectors. Furthermore, since \(P\) is full rank, these columns span the space \(\mathbb{F}^n\) and hence
form a basis.

Conversely, suppose that we have a basis consisting of eigenvectors. Then we can let \(P\)
be the matrix whose columns are populated by the eigenvectors and \(D\) be \(\lambda_r\). Then
\(P\) is full rank and hence we get the desired result.
\end{example}

\section{Unitarily similar with upper triangular matrix}
\label{develop--math3273:eigenvectors:page--index.adoc---unitarily-similar-with-upper-triangular-matrix}

Let \(A \in \mathbb{C}^{n\times n}\) be a matrix with eigenvalues \(\lambda_1, \ldots \lambda_n\) (including multiplicity).
Also, let \(\vec{v}\) be the unit eigenvector corresponding to \(\lambda_1\). Then, there exists
unitary matrix \(U\) and upper triangular \(T\) such that

\begin{equation*}
A = U T U^*
\end{equation*}

and the first column of \(U\) is \(\vec{v}\) and

\begin{equation*}
T = \begin{pmatrix}
\lambda_1 & \cdots    &        & \\
          & \lambda_2 & \cdots & \\
          &           & \ddots & \vdots \\
          &           &        & \lambda_n
\end{pmatrix}
\end{equation*}

\begin{admonition-note}[{}]
A similar result holds for \(A \in \mathbb{R}^{n\times n}\)
which has all of its eigenvalues. That is \(A\) is orthogonally similar
to a upper triangular matrix with the same properties. The proof is the same.
\end{admonition-note}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
This the same proof presented in class
\end{admonition-note}

We would do this by induction. Where the statement is that if \(A \in \mathbb{C}^{k\times k}\)
with eigenvalues \(\lambda_1, \ldots \lambda_n\) and \((\vec{v}, \lambda_1)\)
is an eigenpair, then the desired statement holds.

\discretefloatingtitle{Base case \(n=1\)}

This is trivially true.

\discretefloatingtitle{Inductive Step}

Let \(U\) be a unitary matrix with first column being \(\vec{v}\).
One way to generate this matrix is to use the
\myautoref[{previously discussed method}]{develop--math3273:inner-product-space:page--orthogonal-projections.adoc---cool}.
We then let \(U = \begin{pmatrix} \vec{v} & U' \end{pmatrix}\) and notice that

\begin{equation*}
\begin{aligned}
U^* A U
&= \begin{pmatrix} \vec{v}^* \\ (U')^*\end{pmatrix}
    A
    \begin{pmatrix} \vec{v} & U' \end{pmatrix}
\\&= \begin{pmatrix} \vec{v}^* \\ (U')^*\end{pmatrix}
    \begin{pmatrix} A\vec{v} & A U' \end{pmatrix}
\\&= \begin{pmatrix} \vec{v}^* \\ (U')^*\end{pmatrix}
    \begin{pmatrix} \lambda_1 \vec{v} & A U' \end{pmatrix}
\\&= \begin{pmatrix}
    \lambda_1 \vec{v}^* \vec{v} & \vec{v}^* A U' \\
    \lambda_1 (U')^* \vec{v} & (U')^* A U'
    \end{pmatrix}
\\&= \begin{pmatrix}
    \lambda_1 & \vec{v}^* A U' \\
    \vec{0} & (U')^* A U'
    \end{pmatrix}
\end{aligned}
\end{equation*}

Since the columns of \(U\) are an orthonormal basis. Next, since \(A\)
is similar to the final matrix, it shares the same characteristic polynomial. In particular

\begin{equation*}
p_A(z) = (z-\lambda_1)\det(z I - (U')^* A U')
\end{equation*}

Then the roots of \(\det(z I - (U')^* A U')\) are \(\lambda_2,\ldots \lambda_n\)
and hence the eigenvalues of \((U')^* A U'\) are \(\lambda_2, \ldots \lambda_n\).
Now we have that

\begin{equation*}
(U')^* A U' = V T' V^*
\end{equation*}

where \(T\) is upper diagonal where the diagonal entries consist of eigenvalues
\(\lambda_2,\ldots \lambda_n\). Therefore

\begin{equation*}
\begin{aligned}
\begin{pmatrix}1 & \vec{0}^*\\ \vec{0} & V^*\end{pmatrix}
U^* A U
\begin{pmatrix}1 & \vec{0}^*\\ \vec{0} & V\end{pmatrix}
&=
\begin{pmatrix}1 & \vec{0}^*\\ \vec{0} & V^*\end{pmatrix}
\begin{pmatrix}
    \lambda_1 & \vec{v}^* A U' \\
    \vec{0} & (U')^* A U'
    \end{pmatrix}
\begin{pmatrix}1 & \vec{0}^*\\ \vec{0} & V\end{pmatrix}
\\&=
\begin{pmatrix}1 & \vec{0}^*\\ \vec{0} & V^*\end{pmatrix}
\begin{pmatrix}
    \lambda_1 & \vec{v}^* A U' V\\
    \vec{0} & V T' V^* V
    \end{pmatrix}
\\&=
\begin{pmatrix}
    \lambda_1 & V^* \vec{v}^* A U' V\\
    \vec{0} & V^* V T' V^* V
    \end{pmatrix}
\\&=
\begin{pmatrix}
    \lambda_1 & V^* \vec{v}^* A U' V\\
    \vec{0} & T'
    \end{pmatrix}
\end{aligned}
\end{equation*}

Then since \(U\begin{pmatrix}1 & \vec{0}^*\\ \vec{0} & V\end{pmatrix}\)
is unitary, we are done.
\end{example}

\section{Normal Matrices}
\label{develop--math3273:eigenvectors:page--index.adoc--normal-matrices}

The matrix \(A\in \mathbb{C}^{n\times n}\) is called \emph{normal} iff

\begin{equation*}
A^* A = A A^*
\end{equation*}

Furthermore, the following are equivalent

\begin{itemize}
\item \(A\) is normal
\item \(A\) is unitarily diagonalizable
\item \(\mathbb{C}^n\) has an orthonormal basis consisting of eigenvectors for \(A\).
\end{itemize}

\begin{admonition-important}[{}]
If we limit \(A\) to be real with all its eigenvalues,
then the same results hold with the addition that \(A\) is orthogonally diagonalizable.
\end{admonition-important}

Even further, if \(A\) is hermitian, it is unitarily diagonalizable and has all
real eigenvalues. Even even further, \(A\) is real symmetric iff it is orthogonally diagonalizable.

\begin{example}[{Proof}]
In a similar manner to that described
\myautoref[{previously}]{develop--math3273:eigenvectors:page--index.adoc---diagonalizable}, \(A\)
is unitarily diagonalizable iff there is an orthonormal basis of \(\mathbb{C}^n\)
consisting of eigenvalues of \(A\).

Also, if \(A\) is unitarily diagonalizable we get that it is normal since

\begin{equation*}
AA^* = (U D U^*)(U D U^*)^* = (UDU^*)(UD^*U^*) = UDD^*U^*
\end{equation*}

\begin{equation*}
A^*A = (U D U^*)^*(U D U^*) = (UD^*U^*)(UDU^*) = UD^*DU^*
\end{equation*}

and notice that each entry on the diagonal of \(DD^*\) is \(\lambda\overline{\lambda} = \|\lambda\|^2\)
we get that \(D^*D = DD^*\) and hence \(A\) is normal.

Conversely, suppose that \(A\) is normal. Then \(A\) is unitarily similar
to upper triangular matrix \(T\) whose diagonal consists of the eigenvalues of \(A\)
 [\myautoref[{read more}]{develop--math3273:eigenvectors:page--index.adoc---unitarily-similar-with-upper-triangular-matrix}]. That is
\(A = UTU^*\) where \(U\) is unitary. Then

\begin{equation*}
UT^*TU^*
= A^*A
= AA^*
= UTT^* U^*
\end{equation*}

and hence \(T\) is normal as well. We want to show that \(T\)
is diagonal. Let \(TT^* = T^* T = (a_{ij})\) and \(T=(t_{ij})\).
Then \(T^* = (\overline{t_{ji}})\).
Consider the \(ii\) entry in \(TT^*\), we get

\begin{equation*}
a_{ii}
= \sum_{k=1}^n (T)_{ik}(T^*)_{ki}
= \sum_{k=1}^n t_{ik}\overline{t_{ik}}
= \sum_{k=i}^n \|t_{ik}\|^2
\end{equation*}

On the other hand, the \(ii\) entry in \(T^*T\) is

\begin{equation*}
a_{ii}
= \sum_{k=1}^n (T^*)_{ik}(T)_{ki}
= \sum_{k=1}^n \overline{t_{ki}}t_{ki}
= \sum_{k=1}^i \|t_{ki}\|^2
\end{equation*}

Therefore

\begin{equation*}
\sum_{k=i}^n \|t_{ik}\|^2 = a_{ii} = \sum_{k=1}^i \|t_{ki}\|^2
\end{equation*}

What does this formula mean? It says that the sum of the squared magnitudes of the
entries to the right of the diagonal is equal to the sum of the squared magnitudes of the
entries above the diagonal.
What we would do is show that
all entries right of the diagonal must be zero.

\begin{itemize}
\item When \(i=1\), the only entry in the first column is \(t_{11}\) and hence the
    right hand side is \(t_{11}\). Hence all the terms in the left sum must be zero except
    \(t_{11}\). Therefore the first row consists only of \(t_{11}\).
\item Suppose for all rows \(j < i\), \(t_{jj}\) is the only entry. Then in the right
    sum, we get only \(t_{ii}\) since all the entries above it belong to a row \(j < i\).
    Hence all the terms in the left sum must be zero except \(t_{ii}\) and hence
    the \(i\)'th row consists only of \(t_{ii}\).
\end{itemize}

Therefore, \(T\) is diagonal and we are done.
\end{example}

\begin{example}[{Proof for hermitian and symmetric matrices}]
Let \(A\) be hermitian, then clearly it is normal since

\begin{equation*}
AA^* = A^2 = A^*A
\end{equation*}

Then, it is unitarily diagonalizable with \(A = UDU^*\) and notice that

\begin{equation*}
UDU^* = A = A^* = UD^* U^*
\end{equation*}

Hence \(D = D^*\) and all the entries (eigenvalues) are real.

Now, when \(A\) is real, it is automatically hermitian and hence all of its eigenvalues
are real. Now, we could find that \(A\) is orthogonally similar to a upper triangular
matrix and hence \(A\) is orthogonally diagonalizable. Conversely, if \(A\)
is orthogonally diagonalizable

\begin{equation*}
A = UDU^T = UD^TU^T = A^T
\end{equation*}
\end{example}
\end{document}
