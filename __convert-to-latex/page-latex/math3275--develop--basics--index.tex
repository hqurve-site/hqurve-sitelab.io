\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Basic Definitions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Ln}{Ln}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\Res}{Res}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
As its name implies, the complex plane is the focus of this course.
Hence, the definitions are focused mainly on the complex plane. However,
some of these concepts could be easily generalized.

\begin{description}
\item[Point Set] Any collection of points in the complex plane.
\end{description}

\section{Topological Properties}
\label{develop--math3275:basics:page--index.adoc---topological-properties}

The definitions presented below are the specialization of the more general
metric space
(\myautoref[{discussed here}]{develop--math3277:topology:page--index.adoc}) where the set is the
set of complex numbers and with metric \(d(z_1,z_2) = |z_1 - z_2|\) (the modulus function).

\begin{description}
\item[Neighbourhood] A \emph{\(\delta\) neighbourhood} of a point \(z_0\) is the set of points \(\{z: |z-z_0| < \delta\}\).
    \(z_0\) is refered to as the \emph{center}.
    
    \begin{description}
    \item[Deleted Neighbourhood] A \emph{deleted \(\delta\) neighbourhood} of a point \(z_0\) is the set of points \(\{z: 0 < |z-z_0| < \delta\}\)
    \end{description}
\item[Limit point] \(z_0\) is a \emph{limit point} of point set \(S\) iff all deleted neighbourhoods of \(z_0\) contain
    a point in \(S\).
    
    \begin{admonition-note}[{}]
    \(z_0\) need not be a member of \(S\)
    \end{admonition-note}
\item[Closed] Point set \(S\) is \emph{closed} iff all limit points of \(S\) belong to \(S\).
\item[Bounded] Point set \(S\) is \emph{bounded} iff \(\exists k \in \mathbb{R}^+\) such that
    \(|z| < k\) for all \(z \in S\). If \(S\) is not bounded, we say it is \emph{unbounded}.
\item[Compact] Point set \(S\) is \emph{compact} iff it is both closed and bounded.
\item[Interior, Exterior and Boundary Points] Let \(S\) be a set and consider \(z_0 \in \mathbb{C}\)
    
    \begin{description}
    \item[Interior Point] \(z_0\) is an \emph{interior point} of \(S\) if there exists a neighbourhood centered
        about \(z_0\) which contains only contains points of \(S\).
    \item[Boundary Point] \(z_0\) is a \emph{boundary point} of \(S\) if all neighbourhoods of \(z_0\)
        contain both points in \(S\) and not in \(S\).
    \item[Exterior Point] \(z_0\) is an \emph{exterior point} if there exists a neighbourhood of \(z_0\) which only
        contains points not in \(S\).
    \end{description}
    
    \begin{admonition-note}[{}]
    \(z_0\) is exactly one of the three above classifications.
    \end{admonition-note}
\item[Open] Point set \(S\) is \emph{open} if it consists only of interior points.
\item[Connected] Open set \(S\) is \emph{connected} iff any two of its points can be joined
    by a path consisting of straight line segments whose points all lie in \(S\).
    
    \begin{description}
    \item[Simply Connected] Connected region \(S\) is \emph{simply connected} if each jordan curve which lies within \(S\)
        can be shrunk to a point without ever passing through a point not in \(S\). That is,
        \(S\) contains no ``gaps''.
    \item[Multiply Connected] Connected region \(S\) is \emph{multiply connected} if it is not simply connected.
    \item[Cross cuts] Lines drawn along a multiply connected region to make it simply connected. It does
        this by removing all points which are on the drawn line. Then if a multiply
        connected region has a finite number of ``gaps'' a finite number of cross cuts can be
        used to make it multiply connected.
    \end{description}
\item[Open Domain] Point set \(S\) is an \emph{open domain} (or \emph{open region}) iff it is both open and connected.
\item[Closure] The \emph{closure} of point set \(S\) is the union of the set \(S\) and its limit points.
    
    \begin{admonition-note}[{}]
    The closure of a closed set is itself.
    \end{admonition-note}
    
    \begin{admonition-note}[{}]
    The closure of a set is closed. This implies that the closure of the closure is simply the closure.
    \end{admonition-note}
\item[Closed Region] The closure of an open domain is a \emph{closed region}.
\item[Region (or domain)] Open domain \(S\) unioned with a subset of its limit points.
    
    \begin{admonition-note}[{}]
    Since in \(\mathbb{C}\) all interior points are limit points, all points in a domain are limit points.
    \end{admonition-note}
\end{description}

\section{Curves}
\label{develop--math3275:basics:page--index.adoc---curves}

\begin{description}
\item[Continuous Arc] If \(x(t)\) and \(y(t)\) are real and continuous functions defined on
    interval \(I\), then the set of points \(\{z = x(t) + iy(t): t \in I\}\) is called a \emph{continous arc}
\item[Multiple point] The point \(z_1\) is a \emph{multiple point} of arc \(z=x(t) + iy(t)\) if there exists more
    than one value of \(t \in I\)  which generate point \(z_1\). In particular, if there exists exactly two values
    of \(t\), \(z_1\) is a \emph{double point}.
\item[Jordan arc] The continuous arc \(z = x(t) + iy(t)\) is called a \emph{jordan arc} iff it contains no double points.
    That is, it is injective on the closed interval \(I\).
\item[Simply closed Jordan curve] A continuous arc is called a \emph{simply closed jordan curve} iff it has exactly one
    double point which occurs at the endpoints of its interval.
\end{description}

\begin{theorem}[{Jordan Curve Theorem}]
A simply closed jordan curve divides the plane into two domains. One domain is bounded and it is called the
\emph{interior} while the other is unbounded and called the \emph{exterior}.
\end{theorem}

\begin{admonition-note}[{}]
Although this may seem like a trivial result, it is difficult to prove. [\href{https://en.wikipedia.org/wiki/Jordan\_curve\_theorem}{wiki article}]
\end{admonition-note}
\end{document}
