\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cauchy Residue Theorem}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Ln}{Ln}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\Res}{Res}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-tip}[{}]
See \myautoref[{}]{develop--math3275:integration:page--useful.adoc}.
\end{admonition-tip}

Let \(C\) be a simple closed contour and \(f(z)\) be analytic
on \(C \cup I(C)\) except at a finite number of points.
Then,

\begin{equation*}
\oint_C f(z) \ dz = 2\pi i \sum_{j} R_{a_j}(f)
\end{equation*}

Where \(\{a_j\}\) is the set of points within \(I(C)\) at which \(f(z)\)
fails to be analytic. Notice that this formula is simply
our \myautoref[{Cauchy Integral Theorem for multiply connected regions}]{develop--math3275:integration:page--cauchy.adoc---multiply-connected-regions}
combined with our definition of residues.

\begin{admonition-thought}[{}]
How long until you think I have to amend the above statement?
\end{admonition-thought}

Throughout this section, we would explore various classifications of
integrals for which this formula is helpful.

\section{Integrals of trig functions over \([0, 2\pi]\)}
\label{develop--math3275:residue:page--residue-theorem.adoc--case-1}

Consider the following integral

\begin{equation*}
\int_0^{2\pi} f(\cos\theta, \sin\theta) \ d\theta
\end{equation*}

Then, by letting \(z = e^{i\theta}\) and \(C: |z| = 1\), we get that

\begin{equation*}
\int_0^{2\pi} f(\cos\theta, \sin\theta) \ d\theta
= \oint_C f\left(\frac{z + \frac{1}{z}}{2}, \frac{z - \frac{1}{z}}{2i}\right)\frac{1}{iz} \ dz
\end{equation*}

Let \(g(z)\) be the integrand. Then we may apply the residue theorem to \(g(z)\)
about the unit circle.

\section{Integrals of analytic functions on real axis}
\label{develop--math3275:residue:page--residue-theorem.adoc--case-2}

Consider the following integral,

\begin{equation*}
\int_{-\infty}^\infty f(x) \ dx
\end{equation*}

where \(f(z)\) has no poles or residues on the real axis.
Then let \(R > 0\) and

\begin{itemize}
\item \(L: z = x, -R \leq x \leq R\)
\item \(\Gamma: z = Re^{i\theta}, 0 \leq \theta \leq \pi\)
\end{itemize}

and \(C = \Gamma \cup L\).

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/residue/cases/case2}
\end{figure}

Then,

\begin{equation*}
\oint_C f(z) \ dz = \int_\Gamma f(z) \ dz + \int_{-R}^R f(x) \ dx
\end{equation*}

Then, by taking limits and rearranging we get that

\begin{equation*}
\int_{-\infty}^\infty f(x) \ dx = \lim_{R\to \infty} \left[ \oint_C f(z) \ dz - \int_\Gamma f(z) \ dz\right]
\end{equation*}

In order to compute the first integral, we need to consider all the poles in the upper half complex
plane and take \(R\) such that \(C\) encompasses them all, then evaluate
the integral. On the other hand if the set of poles is not finite, we would need to find the limit instead.

In order to compute the second integral, we typically have to use manual methods. However, notice
that if \(|f(z)| \leq \frac{M}{R^k}\) on \(\Gamma\) for some \(M > 0\)
and \(k > 1\) where \(M\) does not depend on \(R\), then we get that

\begin{equation*}
\left|\int_\Gamma f(z) \ dz\right|
= \left|\int_0^\pi f(Re^{i\theta}) Rie^{i\theta}\ d\theta\right|
\leq \int_0^\pi |f(Re^{i\theta})| R \ d\theta
\leq \int_0^\pi MR^{1-k} \ d\theta
= \pi MR^{1-k} \rightarrow 0
\end{equation*}

was \(R \to \infty\) since \(1-k < 0\). Therefore, the second integral would tend to zero.

\begin{admonition-tip}[{}]
Although we used the semi-circle in the upper half plane, we could have instead used the semi-circle
in the lower half plane. However, we would need to be mindful that we are taking a clockwise integral.
\end{admonition-tip}

\section{Integrals with trigs over the real line}
\label{develop--math3275:residue:page--residue-theorem.adoc--case-3}

Suppose we have either of the following two integrals

\begin{equation*}
\int_{-\infty}^\infty f(x) \cos m x \ dx
\quad\text{or}\quad
\int_{-\infty}^\infty f(x) \sin m x \ dx
\end{equation*}

where \(m > 0\), then would look at

\begin{equation*}
\int_{-\infty}^\infty f(x) e^{im x} \ dx
\end{equation*}

instead, and take the real or imaginary component as necessary.
In order to evaluate this integral, we would use the \myautoref[{case II form}]{develop--math3275:residue:page--residue-theorem.adoc--case-2}.

\section{Integrals with singularities over the real line}
\label{develop--math3275:residue:page--residue-theorem.adoc--case-4}

Consider the integral

\begin{equation*}
\int_{-\infty}^\infty f(x) \ dx
\end{equation*}

where \(f(z)\) has singularities on the real axis.
Then in such a case we would skirt around the singularities and take
limits.

Consider
the case where \(a\) is the only singularity on the real axis.
Then, we let \(R > a\) and \(0 < \varepsilon < (R-a)\)
and consider the following contour

\begin{itemize}
\item \(L_1: z=x\), \(-R \leq x \leq a - \varepsilon\).
\item \(\Gamma_1: z = a + \varepsilon e^{i\theta}\), \(0 \leq \theta \leq \pi\) (clockwise)
\item \(L_2: z=x\), \(a + \varepsilon \leq x \leq R\).
\item \(\Gamma_2: z = R e^{i\theta}\), \(0 \leq \theta \leq \pi\)
    and \(C = L_1 \cup \Gamma_1 \cup L_2 \cup \Gamma_2\).
\end{itemize}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/residue/cases/case4}
\end{figure}

Then,

\begin{equation*}
\oint_C f(z) \ dz =
\int_{L_1} f(z) \ dz
+ \int_{\Gamma_1} f(z) \ dz
+ \int_{L_2} f(z) \ dz
+ \int_{\Gamma_2} f(z) \ dz
\end{equation*}

Then, we take limits \(R \to \infty\) and \(\varepsilon \to 0\),
we see that

\begin{equation*}
\begin{aligned}
\int_{-\infty}^\infty f(x) \ dx
= \lim_{R \to \infty, \varepsilon \to 0}\left[\int_{L_1} f(z) \ dz + \int_{L_2} f(z) \ dz \right]
= \lim_{R \to \infty, \varepsilon \to 0}\left[\oint_C f(z) \ dz - \int_{\Gamma_1} f(z) \ dz + \int_{\Gamma_2} f(z) \ dz \right]
\end{aligned}
\end{equation*}

One might now question the validity of this result. I am
unable to determine why it is valid however, notice
that the choice of \(\Gamma_1\) around \(a\) does
not affect the final result. Consider if \(\Gamma_1\)
went below \(a\), let \(\Gamma_1'\) and \(C'\) be the two affected
paths. Then,

\begin{equation*}
\int_{\Gamma_1'} f(z) \ dz
= \oint_{(-\Gamma_1) \cup \Gamma_1'} f(z) \ dz
+ \int_{\Gamma_1} f(z) \ dz
= R_a(f) + \int_{\Gamma_1} f(z) \ dz
\end{equation*}

and

\begin{equation*}
\begin{aligned}
\oint_{C'} f(z) \ dz
&= \oint_{L_1 \cup \Gamma_1' \cup L_2 \cup \Gamma_2} f(z) \ dz
\\&= \int_{L_1 \cup L_2 \cup \Gamma_2} f(z) \ dz
    + \int_{\Gamma_1'} f(z) \ dz
\\&= \int_{L_1 \cup L_2 \cup \Gamma_2} f(z) \ dz
    + R_a(f) + \int_{\Gamma_1} f(z) \ dz
\\&= \oint_{C} f(z) \ dz + R_a(f)
\end{aligned}
\end{equation*}

Therefore, the final integral does not change value.

\section{Multi-valued functions}
\label{develop--math3275:residue:page--residue-theorem.adoc--case-5}

If the function we would like to integrate is mutlivalued, we must be very careful.
Our general approach is to make a branch cut and ensure that no contour
crosses that cut.

\subsection{Keyhole contour}
\label{develop--math3275:residue:page--residue-theorem.adoc---keyhole-contour}

If we wish to evaluate

\begin{equation*}
\int_0^\infty f(x) \ dx
\end{equation*}

where \(f(z)\) is multivalued and has branch points at \(z= 0\) and \(z=\infty\)
a popular contour we can use is that of the keyhole contour.

\begin{itemize}
\item \(\Gamma_1: z= \varepsilon e^{i\theta}: 0 \leq \theta \leq 2\pi\) (clockwise)
\item \(L_1: z= x: \varepsilon \leq x \leq R\)
\item \(\Gamma_2: z= Re^{i\theta}: 0 \leq \theta \leq 2\pi\)
\item \(L_2: z= xe^{2\pi i}: \varepsilon \leq x \leq R\) (opposite direction)
\end{itemize}

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/residue/cases/case5-keyhole}
\end{figure}

where we have a branch cut along the positive \(x\) axis.
\end{document}
