\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Todo}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{2021-10-21}
\label{develop--main:ROOT:page--todo.adoc---2021-10-21}

\begin{itemize}
\item Take all open tabs and organize what you want to do
\item Read proof for fundamental theorem of algebra \url{https://en.wikipedia.org/wiki/Fundamental\_theorem\_of\_algebra}
\item Put definition of gcd for polynomials into \myautoref[{cool page}]{develop--main:ROOT:page--cool.adoc}. Namely, let \(a,b \in S\) where \(S\) is an euclidean domain,
    then
    
    \begin{equation*}
    I = \{ax + by | x,y\in S\}
    \end{equation*}
    
    is principal where its generator is the gcd.
\item Make prettier checklist when not interactive. Apparently, I need to simply adjust the \texttt{:before} selector. I might use an image.
\item Text wrap (kerning) looks terrible.
\item Fix custom admonitions. Currently, paragraphs do not properly render stem blocks while blocks dont seem to render complex blocks.
    
    Example:
    
    \begin{admonition-remark}[{}]
    \(x=2\)
    \end{admonition-remark}
    
    \begin{admonition-idea}[{}]
    \begin{admonition-thought}[{}]
    Cool
    \end{admonition-thought}
    \end{admonition-idea}
    
    \begin{admonition-idea}[{}]
    \(x=3\)
    \end{admonition-idea}
    
    \begin{admonition-note}[{}]
    \begin{admonition-warning}[{}]
    Cool
    \end{admonition-warning}
    \end{admonition-note}
\end{itemize}

\subsection{Old stuff (hopefully gets done one day)}
\label{develop--main:ROOT:page--todo.adoc---old-stuff-hopefully-gets-done-one-day}

\begin{itemize}
\item Read inner product space \url{https://en.wikipedia.org/wiki/Inner\_product\_space}
\item Read more on euclidean domain, namely go through the notes from last term (math2272). Also
    read \url{https://en.wikipedia.org/wiki/Euclidean\_domain}
\end{itemize}

\subsubsection{Category Theory}
\label{develop--main:ROOT:page--todo.adoc---category-theory}

\begin{itemize}
\item Category theory
    
    \begin{itemize}
    \item \url{http://web.stanford.edu/~truax/notes/Category\_Theory.pdf} (this is the main one to read)
    \item \url{https://plato.stanford.edu/entries/category-theory/}
    \item Look at how homomorphisms preserve logical positive statements. I dont remember where I saw this claim, but I belvie its true.
        Found it: \url{https://math.stackexchange.com/questions/3711643/why-is-it-the-group-morphisms-that-matter}
    \end{itemize}
\end{itemize}

\subsubsection{Real Analysis}
\label{develop--main:ROOT:page--todo.adoc---real-analysis}

\begin{itemize}
\item Chain rule: what happens if the denominator is zero?
\item \url{https://math.stackexchange.com/questions/399722/metric-assuming-the-value-infinity\#399759}
\item What is this about? \url{https://www.whitman.edu/mathematics/calculus\_online/section14.05.html}
\end{itemize}

\subsubsection{PDES}
\label{develop--main:ROOT:page--todo.adoc---pdes}

\begin{itemize}
\item Convergence of fourier series? (proof or its false)
\item The uniqueness of solution for second order PDEs, namely d'Alembert's solution.
    
    \begin{itemize}
    \item Maximum principle \& Energy
        
        \begin{itemize}
        \item \url{https://math.stackexchange.com/questions/157703/energy-method-for-elliptic-pde}
        \end{itemize}
    \item \url{https://math.stackexchange.com/questions/1456273/uniqueness-of-the-solution-of-a-pde}
    \item Seems kind of cool: \url{http://www.math.toronto.edu/ivrii/PDE-textbook/contents.html}
    \end{itemize}
\end{itemize}

\subsubsection{Number theory}
\label{develop--main:ROOT:page--todo.adoc---number-theory}

\begin{itemize}
\item \url{https://en.wikipedia.org/wiki/Lagrange\%27s\_theorem\_(number\_theory})
\item Cool: \url{https://en.wikipedia.org/wiki/Formula\_for\_primes\#Formulas\_based\_on\_Wilson's\_theorem}
\item \url{https://en.wikipedia.org/wiki/P-adic\_number}
\item \url{https://en.wikipedia.org/wiki/Hensel\%27s\_lemma\#Hensel\_lifting}
\end{itemize}

\subsubsection{Probability}
\label{develop--main:ROOT:page--todo.adoc---probability}

\begin{itemize}
\item \url{https://stats.stackexchange.com/questions/320936/independence-of-sample-mean-and-sample-variance-in-binomial-distribution}
    I may have done this already, but im unsure.
\end{itemize}

\subsubsection{Font:}
\label{develop--main:ROOT:page--todo.adoc---font}

\begin{itemize}
\item Font forge
\item I need a handwritten font (how I usually write)
\item Font for equations/diagrams.
\end{itemize}

\subsubsection{System}
\label{develop--main:ROOT:page--todo.adoc---system}

\begin{itemize}
\item move to nvim-cmp
\item Issues with nvim-tree-lua
\item Automatically load microphone and apply echo cancel with null sink and loopback\newline
    Partial: echo cancel and nullsinks are auto loaded, however I still have to change profile
    and route manually.
\item Limit mpv memory usage. (This is a hackfix to prevent fork bombs from thumbnail script)
    
    \begin{itemize}
    \item \strikethrough{[ ] Cgroups?} Ended up using \texttt{systemd-run}
    \end{itemize}
\item Wrap websites in electron easily
    
    \begin{itemize}
    \item What configuration does electron use when I call \texttt{electron \url{https://google.com}} ?
    \end{itemize}
\end{itemize}

\subsubsection{Other}
\label{develop--main:ROOT:page--todo.adoc---other}

\begin{itemize}
\item Zoneminder
\item Paneer \url{https://www.whiskaffair.com/achari-paneer-paneer/}
\item Silverblue \& libostree (just curious)
\end{itemize}

\section{2021-10-22}
\label{develop--main:ROOT:page--todo.adoc---2021-10-22}

\begin{itemize}
\item Cool: \url{http://math.ucsd.edu/~nwallach/m100b-pid.pdf}
\end{itemize}

\section{2022-02-18}
\label{develop--main:ROOT:page--todo.adoc---2022-02-18}

\begin{itemize}
\item Create page/site dedicated to probability. This page would house
    important results. For example, distributions and results surrounding them (MLEs, mgfs, tests)
    would be housed here. Currently these things are stored in probability I site however, many
    things should not go there.
\end{itemize}
\end{document}
