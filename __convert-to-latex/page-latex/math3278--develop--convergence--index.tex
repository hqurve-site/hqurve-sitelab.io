\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Convergence}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

% Title omitted
\section{Concentration inequalities}
\label{develop--math3278:convergence:page--index.adoc---concentration-inequalities}

\subsection{Markov's Inequality}
\label{develop--math3278:convergence:page--index.adoc---markovs-inequality}

Let \(X\) be a non-negative random variable and \(\alpha > 0\), then

\begin{equation*}
P(X > \alpha) \leq \frac{E(X)}{\alpha}
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
We prove this using the integral, however this result holds in general
\end{admonition-note}

\begin{equation*}
\begin{aligned}
P(X > \alpha)
= \int_{\alpha}^\infty f(x) \ dx
= \frac1{\alpha}\int_{\alpha}^\infty \alpha f(x) \ dx
\leq \frac1{\alpha}\int_{\alpha}^\infty x f(x) \ dx
\leq \frac1{\alpha}\int_{0}^\infty x f(x) \ dx
= \frac1{\alpha}E(X)
\end{aligned}
\end{equation*}
\end{example}

\subsection{Chebyshev's Inequality}
\label{develop--math3278:convergence:page--index.adoc---chebyshevs-inequality}

Let \(X\) be a random variable with finite mean \(\mu\) and variance \(\sigma^2\). Then
for all positive \(k\),

\begin{equation*}
P(|X-\mu| > k\sigma) \leq \frac{1}{k^2}
\end{equation*}

\begin{example}[{Proof}]
We utilize \myautoref[{Markov's Inequality}]{develop--math3278:convergence:page--index.adoc---markovs-inequality} to get that

\begin{equation*}
P(|X-\mu| > k\sigma)
=P(|X-\mu|^2 > k^2\sigma^2)
\leq \frac{E(|X-\mu|^2)}{k^2\sigma^2}
= \frac{Var(X)}{k^2\sigma^2}
= \frac{\sigma^2}{k^2\sigma^2}
= \frac{1}{k^2}
\end{equation*}
\end{example}

\section{Convergence in Distribution}
\label{develop--math3278:convergence:page--index.adoc---convergence-in-distribution}

Let \(\{X_n\}\) be a sequence of random variables each with respective distribution
\(F_n\) and let \(X\) be a random variable with distribution function \(F\).
Then, \(\{X_n\}\) \emph{converges in distribution} to \(X\) if

\begin{equation*}
\forall x \in \mathbb{R}: \lim_{n\to \infty} F_n(x) = F(x)
\end{equation*}

and we write

\begin{equation*}
X_n \xrightarrow[]{d} X
\end{equation*}

\section{Convergence in probability}
\label{develop--math3278:convergence:page--index.adoc---convergence-in-probability}

Let \(\{X_n\}\) be a sequence of random variables and \(X\) be a random variable.
Then, \(\{X_n\}\) \emph{converges in probability} to \(X\) if \(\forall \varepsilon > 0\)

\begin{equation*}
\lim_{n\to \infty} P(|X_n - X| > \varepsilon ) = 0
\end{equation*}

and we write

\begin{equation*}
X_n \xrightarrow[]{p} X
\end{equation*}

\begin{admonition-note}[{}]
Convergence in probability is stronger than \myautoref[{convergence in distribution}]{develop--math3278:convergence:page--index.adoc---convergence-in-distribution}.
\end{admonition-note}

\begin{admonition-remark}[{}]
I am unsure how this definition works as I have only seen examples in which \(X\) is a constant.
However, I belive you find the joint distribution of \((X_i, X)\) for each \(i\)
and show that the above limit holds.
\end{admonition-remark}

\section{Almost sure convergence}
\label{develop--math3278:convergence:page--index.adoc---almost-sure-convergence}

Let \(\{X_n\}\) be a aseqnec of random variables and \(X\)
be a random varibale. Then, we define

\begin{equation*}
E = \left\{\omega\in S : \ \lim_{n\to\infty}X_n(\omega) = X(\omega)\right\}
\end{equation*}

where \(S\) is our sample space. Then, if

\begin{equation*}
P(E) = 1
\end{equation*}

we say that \(\{X_n\}\) \emph{converges almost surely} to \(X\) and write

\begin{equation*}
X_n \xrightarrow[]{a.s.} X
\end{equation*}

\begin{admonition-note}[{}]
Convergence almost surely is stronger than \myautoref[{convergence in probability}]{develop--math3278:convergence:page--index.adoc---convergence-in-probability}
\end{admonition-note}
\end{document}
