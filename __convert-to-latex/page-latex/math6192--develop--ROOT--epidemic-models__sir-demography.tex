\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{SIR Model with Demography}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
We modify the classical SIR model to include changes in population
due to births and deaths.
We assume

\begin{itemize}
\item All new individuals are added to the susceptible class with constant rate \(\Gamma\)
\item Deaths occur with equal rates in all three compartments with constant
    per capita rate \(\mu\).
\end{itemize}

So, our new equations are

\begin{equation*}
\begin{aligned}
S' &= \Gamma -\beta IS - \mu S\\
I' &= \beta IS -\alpha I - \mu I\\
R' &= \alpha I - \mu R\\
\end{aligned}
\end{equation*}

and hence \(N = S+I+R\) follows the simplified logistic model

\begin{equation*}
N' = \Gamma - \mu N
\end{equation*}

Since we know the solution for \(N\), we can easily recover \(R\)
using only \(S\) and \(I\) (whose equations are independent of \(R\)).

\section{Dimensionless}
\label{develop--math6192:ROOT:page--epidemic-models/sir-demography.adoc---dimensionless}

We perform two transformations:

\begin{itemize}
\item We use \(\tau = (\alpha + \mu) t\)
\item We scale the equations by factor \(\frac{\mu}{\Gamma}\).
    By doing this, we are instead modelling the size of each class relative to
    the limiting population size.
\end{itemize}

Let

\begin{equation*}
\begin{aligned}
x(\tau)
&= \frac\mu\Gamma \tilde{S}(\tau)
= \frac\mu\Gamma S\left(\frac\tau{\alpha + \mu}\right)
= \frac\mu\Gamma S(t)
\\
y(\tau)
&= \frac\mu\Gamma \tilde{I}(\tau)
= \frac\mu\Gamma I\left(\frac\tau{\alpha + \mu}\right)
= \frac\mu\Gamma I(t)
\end{aligned}
\end{equation*}

So, we get that

\begin{equation*}
\begin{aligned}
S' &= \Gamma -\beta IS - \mu S\\
(\alpha+\mu)\frac\Gamma\mu x'
=\frac\Gamma\mu\frac{dx}{dt}
&= \Gamma - \beta \frac{\Gamma^2}{\mu^2}xy - \Gamma x\\
x' &= \frac{\mu}{\alpha + \mu} - \frac{\Gamma\beta}{\mu(\alpha + \mu)}xy - \frac{\mu}{\alpha + \mu}x\\
x' &= p(1-x) - R_0 xy = f(x,y)\\
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
I' &= \beta IS -\alpha I - \mu I\\
(\alpha+\mu)\frac\Gamma\mu y'
=\frac\Gamma\mu\frac{dy}{dt}
&= \beta \frac{\Gamma^2}{\mu^2}xy -\frac{\alpha \Gamma}{\mu} y- \Gamma y\\
y' &= \frac{\Gamma\beta}{\mu(\alpha + \mu)}xy - \frac{\alpha}{\alpha + \mu} y - \frac{\mu}{\alpha + \mu}y\\
y' &= R_0xy -y = g(x,y)\\
\end{aligned}
\end{equation*}

where \(p = \frac{\mu}{\alpha + \mu}\) and \(R_0 = \frac{\beta \Gamma}{\mu(\alpha+\mu)}\).
We are yet to show that \(R_0\) is a suitable definition
for a basic reproduction number.

Recall that the basic reproduction number is the number of secondary infections
due to a single infected individual.
Consider the limiting population size of \(\frac{\Gamma}{\mu}\).
If we have one infected individual, the incidence rate is \(\beta IS = \beta \frac{\Gamma}{\mu}\).
Also, since \(\alpha + \mu\) is the rate of leaving the infectious class, the single individual
would stay an average time of \(\frac{1}{\alpha + \mu}\) in the infectious class.
Therefore we get our definition of \(R_0\) by multiplying the incidence rate and the average
time in the infectious class.

\section{Phase-plane analysis and linearization}
\label{develop--math6192:ROOT:page--epidemic-models/sir-demography.adoc---phase-plane-analysis-and-linearization}

Notice that there are only two equilibria

\begin{description}
\item[Disease-free] \(I_1=(x,y)=(1,0)\)
\item[Endemic] \(I_2 = \left(\frac{1}{R_0}, p\left(1-\frac{1}{R_0}\right)\right)\)
\end{description}

Note that the endemic equilibrium only exists if \(R_0 > 0\).

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-math6192/ROOT/SIR-demographic-phase-plane}
\caption{Phase plane plot showing the direction field and orbits \((x,y)\) with different initial conditions for the dimensionless SIR model with \(p=0.5\) and \(R_0=2\). Plot obtained numerically using Sagemath.}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-math6192/ROOT/SIR-demographic-phase-plane-sink}
\caption{Phase plane plot showing the direction field and orbits \((x,y)\) with different initial conditions for the dimensionless SIR model with \(p=0.75\) and \(R_0=5\). Plot obtained numerically using Sagemath.}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-math6192/ROOT/SIR-demographic-phase-plane-disease-free}
\caption{Phase plane plot showing the direction field and orbits \((x,y)\) with different initial conditions for the dimensionless SIR model with \(p=0.75\) and \(R_0=0.5\). Plot obtained numerically using Sagemath.}
\end{figure}

When linearizing the system about a particular equilibrium,
note that we only need to consider the first order partial derivatives.
This is precisely the jacobian.

\begin{admonition-important}[{}]
This approach does not work when doing linear-stability analysis in general
since we may often have relationships between derivatives.
\end{admonition-important}

\subsection{Disease-free linearization}
\label{develop--math6192:ROOT:page--epidemic-models/sir-demography.adoc---disease-free-linearization}

\begin{equation*}
\begin{bmatrix}
    x'\\ y'
\end{bmatrix}
= \begin{bmatrix}
f_x & f_y\\
g_x & g_y\\
\end{bmatrix}_{I_1}
\begin{bmatrix}
    x\\ y
\end{bmatrix}
= \begin{bmatrix}
-p-R_0y & -R_0x\\
R_0y & R_0x-1\\
\end{bmatrix}_{(x,y)=(1,0)}
\begin{bmatrix}
    x\\ y
\end{bmatrix}
= \begin{bmatrix}
-p & -R_0\\
0 & R_0-1\\
\end{bmatrix}
\begin{bmatrix}
    x\\ y
\end{bmatrix}
\end{equation*}

Therefore there are two eigenvalues: \(-p\) and \(R_0-1\).
Since \(p>0\), the first eigenvalue is always negative.
So, the stability of the equilibrium depends on \(R_0\)

\begin{itemize}
\item If \(R_0 > 1\), we have that the disease free state is unstable (since it is a saddle)
\item If \(R_0 < 1\), the disease free state is stable.
\end{itemize}

Note that this also agrees with our desired behaviour of the basic reproduction number.

\subsection{Endemic equilibrium}
\label{develop--math6192:ROOT:page--epidemic-models/sir-demography.adoc---endemic-equilibrium}

\begin{equation*}
\begin{bmatrix}
    x'\\ y'
\end{bmatrix}
= \begin{bmatrix}
-p-R_0y & -R_0x\\
R_0y & R_0x-1\\
\end{bmatrix}_{(x,y)=\left(\frac{1}{R_0}, p\left(1-\frac{1}{R_0}\right)\right)}
\begin{bmatrix}
    x\\ y
\end{bmatrix}
= \begin{bmatrix}
-pR_0 & -1\\
p(R_0 - 1) & 0
\end{bmatrix}
\begin{bmatrix}
    x\\ y
\end{bmatrix}
\end{equation*}

This system has characteristic polynomial

\begin{equation*}
\begin{vmatrix}
-pR_0-\lambda & -1\\
p(R_0 - 1) & -\lambda
\end{vmatrix}
= \lambda^2 + pR_0\lambda + p(R_0-1)
\end{equation*}

Then, from the Routh-Hurwitz criteria, the system is stable iff
\(pR_0 > 0\) and

\begin{equation*}
0 < \begin{vmatrix}
pR_0 & 0 \\
1 & p(R_0-1)
\end{vmatrix}
= p^2R_0(R_0-1)
\end{equation*}

which is true since the endemic equilibrium exists.
Therefore, when the endemic equilibrium exists, it is always stable.
Furthermore, by examining the roots

\begin{equation*}
\lambda_{1,2}
= \frac{-pR_0 \pm \sqrt{p^2R_0^2 -4pR_0+4p}}{2}
= \frac{-pR_0}{2} \pm \sqrt{\left(\frac{pR_0}{2}-1\right)^2 + (p-1)}
\end{equation*}

We can either have

\begin{itemize}
\item a stable node (sink) if \(\left(\frac{pR_0}{2}-1\right)^2 + (p-1) > 0\)
\item a stable spiral if \(\left(\frac{pR_0}{2}-1\right)^2 + (p-1) < 0\)
\end{itemize}
\end{document}
