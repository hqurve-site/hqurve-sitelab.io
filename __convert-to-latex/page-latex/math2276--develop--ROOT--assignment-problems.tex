\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Assignment problems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Given two sets \(A\) and \(B\) with predefined
notions of allowed pairs and preference of pairings, is it possible to
pair off each member of \(A\) with a different member of
\(B\)? This is the essence of assignment problems.

\section{Hall’s Marriage Theorem}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---halls-marriage-theorem}

Given a set of men and a set of women, where each man makes a list of
women he is willing to marry. Then each can be married off to a woman on
his list if and only if for each value of \(k\), any
\(k\) lists contain in their union at least \(k\)
names.

\subsection{Alternative formulations}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---alternative-formulations}

This theorem can be restated in multiple different ways. Here are a few

\begin{itemize}
\item The sets \(A_1, \ldots, A_n\) possess a system of distinct
    representatives if and only if, for all \(k=1, \ldots, n\),
    any \(k\) \(A_i\)’s contain at least \(k\)
    elements in their union.
\item 
\end{itemize}

\begin{example}[{Proof}]
This proof will be constructive. And it will show that if
\(r < n\) men have been paired off, \(r+1\) men can
be paired off.

Suppose \(r < n\) men have be paired off. Now follow
the following algorithm to increase \(r\) to
\(r+1\).

Conduct the following process to find an unmarried woman
\(B_s\).

\begin{enumerate}[label=\arabic*)]
\item Set \(n = 0\)
\item Take an unmarried man, \(A_0\), then there is a lady,
    \(B_1\) on his list.
\item If \(B_{n+1}\) is unmarried stop.
\item Otherwise, increment \(n\) by \(1\).
\item Let \(A_n\) be the husband of \(B_n\).
\item Consider the set of men \(\{A_0, \ldots, A_n\}\), then there
    exists a lady, \(B_{n+1}\), on their collective lists which
    not already labeled.
\item Go to step 3.
\end{enumerate}

Notice that for all \(i=1, \ldots, s\), lady \(B_i\)
is on one man \(A_j\)’s list where \(j < i\).

Now we will redo some of the marriages using the following process.

\begin{enumerate}[label=\arabic*)]
\item Set \(n = s\)
\item We marry \(B_n\) to a man \(A_j\), who’s list shes
    on, where \(j < i\).
\item Now we are unconcerned with men \(A_j\) up to
    \(A_{s-1}\) since they are all now married.
\item If \(j = 0\), we have finished.
\item Otherwise, by marrying \(B_n\) to \(A_j\), we have
    freed lady \(B_j\).
\item Set \(n = j\)
\item Go to step 2
\end{enumerate}

We are now done since \(A_0\) is now married and all
previously married men are still married (although some of their
marriages may have changed).

Therefore, we now have \(r+1\) married men and we can repeat
this process until there are no more unmarried men. ◻
\end{example}

\subsection{Application to latin squares}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---application-to-latin-squares}

An \(n \times n\) \emph{Latin square} is an
\(n \times n\) matrix satisfying

\begin{enumerate}[label=\arabic*)]
\item Each row contains each of the numbers \(1, \ldots, n\)
    exactly once
\item Each column contains each of \(1, \ldots, n\) exactly once
\end{enumerate}

Latin squares are simple to create if we have no entries already
defined. However, the problem becomes more difficult if some of the rows
are already defined.

We will consider how to create a \(n\times n\)Latin square
with \(r\) of its rows already defined. In fact these
intermediary structures are called \emph{Latin rectangles} which is simply an
\(r \times n\) matrix (or its transpose) where
\(r\leq n\) and the second condition is relaxed a little. That
is

\begin{enumerate}[label=\arabic*)]
\item Each row contains each of the numbers \(1, \ldots, n\)
    exactly one
\item Each column contains \(r\) distinct numbers (no repetitions
    in the rows)
\end{enumerate}

From this it is simple to see that an \(n\times n\) latin
rectangle is in fact a latin square. This proof, like many before, will
show how to create an \((r + 1) \times n\) latin rectangle
from a \(r\times n\) one (with \(r < n\)).

\begin{example}[{Proof}]
Since an \((r+1)\)’th row must be added, we consider
all the numbers that can be placed in each of the \(n\)
columns in the row. Let \(S_j\) for
\(j= 1, 2, \ldots, n\) be the set of numbers from
\(1\) to \(n\) which have not yet been placed in the
\(j\)’th column. Therefore, the problem reduces to finding a
system of distinct representatives for each of the \(S_j\).
Moreover, we need to show that \(S_1, S_2, \ldots, S_n\)
satisfy Hall’s condition.

Consider a subset of \(k\) of the \(S_j\). Notice
that each of the \(k\) \(S_j\)’s contains
\(n-r\) numbers therefore, there are \(k(n-r)\)
numbers among them (not including repetitions). Also, notice that each
number occurs exactly \(n-r\) times in all of the
\(S_j\). Therefore, since each of the \(k\) sets
contain \(k(n-r)\) numbers with no number repeated more than
\(n-r\) times, the \(k\) sets must contain at least
\(k\) distinct elements among them. Hence, Hall’s condition is
satisfied. ◻
\end{example}

\section{Optimal assignment problem}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---optimal-assignment-problem}

Suppose there are \(n\) candidates for \(n\) jobs
where each of the candidates have an unsuitability rating for each the
\(n\) jobs. In general, it is not possible to assign each
candidate the job they are most suited for nor assign each job the most
suitable candidate. The question now arises as to how to minimize the
total unsuitability.

Firstly, we can restate this problem in terms of an
\(n \times n\) matrix where each of the \(n\)
columns represent a candidate, \(n\) rows represent a job and
the \((i, j)\)’th entry contains the unsuitability of the
\(j\)’th candidate for the \(i\)’th job. Now the
problem is to select \(n\) entries from the matrix so that

\begin{enumerate}[label=\arabic*)]
\item no two selections are from the same row
\item no two selections are from the same column
\end{enumerate}

while minimizing their total.

Notice that we can add or subtract some value \(k\) from each
row or column without changing the set of optimal selections (yes there
may be multiple). From this we can reduce each of the rows by their
respective minimums and each of the resultant columns also by their new
minimums. This helps in creating a selection since we now just have to
choose zeros from the matrix (since this would obviously optimal
selection).

A set of \(k \leq n\) zeros, no two in the same row or column,
is called \emph{independent zeros} and the objective is to select
\(n\) independent zeros. However, the apparent zeros depend on
how the rows and columns were reduced and hence there may not be
\(n\) independent zeros we can immediately choose. In order to
combat this we will consider the following procedure.

Consider the minimum number of lines required to cover all the zeros in
the matrix. This number is equal to the number of independent zeros by
the König-Egerváry max-min theorem. Now, let \(m\) be the
smallest uncovered number (note \(m > 0\)). Then add
\(m\) to all of the crossed rows and subtract \(m\)
from all the uncrossed columns. Again, like before, this does not change
the set of optimal selections. Furthermore, it has the following effect

\begin{itemize}
\item subtracts \(m\) from all uncrossed numbers
\item leaves numbers crossed once unchanged. Note that crossed rows and
    uncrossed columns have a net zero change while uncrossed rows and
    crossed columns have no change.
\item adds \(m\) to all numbers crossed twice.
\end{itemize}

Hence, all entries are still non-negative. We then reduce the rows and
columns and try to get \(n\) independent zeros again. We claim
that this procedure must finish after a finite number of steps.

\begin{example}[{Proof}]
Suppose \(h=a+b < n\) lines are required to cover the
matrix, comprising of \(a\) rows and \(b\) columns.
Then the net change in the total of the entries is

\begin{equation*}
m(a\times b) - m(n-a)(n-b) = m\left(ab - n^2 + an + bn -ab\right) = mn(a+b - n) \leq -mn < 0
\end{equation*}

Therefore, after each round of the procedure, the total of all entries
reduces by at least \(mn\). Then after \(k\) steps
of the procedure, the total of all entries reduces by at least
\(kmn\). Notice, that this reduction tents to infinity as
\(k\) tends to infinity while the score is always
non-negative. Therefore, the procedure must complete after a finite
number of steps. ◻
\end{example}

\subsection{König-Egerváry max-min theorem}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---könig-egerváry-max-min-theorem}

Let \(A\) be any \(m\times n\) matrix. Then the
maximum number of independent zeros which can be found in
\(A\) is equal to the minimum number of lines (rows or
columns) which together cover all the zeros of A.

\begin{example}[{Proof}]
This proof will first show that the maximum number of
independent zeros is less than the minimum number of lines then show the
converse. Together, these two imply that the maximum number of
independent zeros is equal to the minimum number of lines.

Suppose the maximum number of independent zeros is \(k\). Then
it is obvious that no set of fewer than \(k\) lines can cover
all the zeros. This is because one line can cover at most one of the
independent zeros. Therefore

\begin{equation*}
\text{maximum number of independent zeros} \;\leq\;\text{minimum number of lines covering zeros}
\end{equation*}

Suppose the minimum set of lines, \(h\), consists of
\(a\) rows and \(b\) columns
(\(h = a+b\)). Then, by changing the order of the rows and
columns we can say that without loss of generality the first
\(a\) rows and \(b\) columns contain of all the
zeros in \(A\). Furthermore, we will consider the form of
\(A\) as follows

\begin{equation*}
A = \left(\begin{array}{c | c}
            W & X\\
            \hline
            Y & Z
        \end{array}
        \right)
\end{equation*}

Where \(Z\) contains no zeros and \(W\) is
\(a\times b\), \(X\) is \(a\times (n-b)\),
\(Y\) is \((m-a)\times b\) and \(Z\) is
\((m-a)\times(n-b)\).

Now we will show that \(X\) contains \(a\)
independent zeros and hence similarly \(Y\) would contain
\(b\) independent zeros.

Consider the \(a\) rows of \(X\) and the associated
zeros. We will label the columns of \(X\) \(1\) to
\((n-b)\). Now each row has associated with it a set of zeros
which can be identified by the column index. Therefore, the question of
finding the independent zeros can be reformulated to finding distinct
representatives (columns) for each of the \(a\) rows.

Suppose, by way of contradiction, there is not a system of distinct
representatives, that is Hall’s criteria is violated. Therefore there
exists a set of \(k\) rows of \(X\) where all of
their zeros lie in \(t < k\) columns. Then we can replace the
\(k\) rows with these \(t\) columns. This new
selection of rows and columns still covers all the zeros and
additionally is less than the initial number of rows and columns,
\(h\). Therefore, the minimality is invalidated and there is a
system of distinct representatives for the \(a\) rows and
hence there are \(a\) independent zeros in \(X\).

Similarly, there are \(b\) independent zeros in
\(Y\) and therefore

\begin{equation*}
\text{maximum number of independent zeros} \;\geq a + b = \;\text{minimum number of lines covering zeros}
\end{equation*}

 ◻
\end{example}

\subsubsection{Side note}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---side-note}

The König-Egerváry max-min theorem is equivalent to Hall’s marriage
theorem.

\section{Optimal assignments in an ordered set (ft. David Gale)}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---optimal-assignments-in-an-ordered-set-ft-david-gale}

Given a list of jobs (ordered by decreasing precedence), each with a set
of suitable workers, is it possible to find an optimal set of
\emph{assignable jobs}? This problem, at first glance, is very similar to
Hall’s marriage theorem, however, there are two key differences; not all
jobs need to be assigned and jobs have a certain priority. Furthermore,
it should be explained as to what ''optimal'' means. We say that an
assignable set \(\{a_1, a_2, \ldots, a_n\}\) is \emph{optimal} if
for any other assignable set \(\{b_1, b_2, \ldots, b_m\}\)
then both the following hold

\begin{itemize}
\item \(n \geq m\)
\item \(a_i \leq b_i\) \(\forall i \leq m\)
\end{itemize}

Again, we will construct such a optimal solution however, we firstly
need the following property.

\begin{admonition-note}[{}]
for this subsection, sets would be ''ordered'' in decreasing
precedence.
\end{admonition-note}

\subsection{Exchange property}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---exchange-property}

The property to be described is not unique to this problem and forms
part of the general notion of \emph{matroids} (please read more on this). The
\emph{exchange property states} that for any two assignable sets
\(\{a_1, a_2, \ldots, a_n\}\) and
\(\{b_1, b_2, \ldots, b_{n+1}\}\) (not necessarily disjoint),
there exists \(b_k\) such that
\(\{a_1, a_2, \ldots, a_n, b_k\}\) (not in order) is
assignable.

\begin{example}[{Proof}]
We will prove this by induction on \(n\).

Consider the two assignable sets \(\{a\}\) and
\(\{b_1, b_2\}\); we have two cases. Firstly, if
\(a\) is one of \(b_1\) and \(b_2\) then
clearly \(\{b_1, b_2\}\) is our extended set. Otherwise, at
least one of \(\{b_1, b_2\}\) can be done by a worker not
doing job \(a\). We can choose this job to extend our set.

Assume true for some \(k-1\). Now, consider the two assignable
sets \(\{a_1, a_2, \ldots, a_k\}\) and
\(\{b_1, b_2, \ldots, b_{k+1}\}\). Since the latter has more
elements, there exists a job \(b_j\) which is not done by any
of the jobs \(a_i\). If \(b_j\) is not one of the
\(a_i\)’s we can immediately extend the set. Otherwise,
suppose it is \(a_k\), then we temporarily remove it from both
sets to get two sets with \(k-1\) and \(k\) elements
respectively. By our inductive hypothesis, there exists an element
\(b_h\) which can extend the new first set. Therefore, we add
\(b_h\) to the new set and restore element \(a_k\),
therefore, we have extended the set. ◻
\end{example}

\subsection{Construction}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---construction}

We construct a solution in a very similar way to that in Hall’s theorem.
Furthermore, we will traverse the jobs in order of decreasing
precedence.

Firstly, add the first job to the assignable set. At the
\(r\)’th stage attempt to assign the \(r\)’th job to
worker using the algorithm described in Hall’s theorem. If the algorithm
succeeds at the job to the assignable set and proceed to the
\(r+1\)’th job. However, it it fails, the job will be rejected
and will not be assigned and proceed to the \(r+1\)’th job.
Continue this process until the end of the list is reached.

\subsection{Validity}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---validity}

We now need to show that the construction above indeed produces the
optimal assignable set. Let \(\{a_1, a_2, \ldots, a_n\}\) be
the set created by construction and
\(\{b_1, b_2, \ldots, b_m\}\) another assignable set. We have
two conditions to satisfy

\begin{itemize}
\item Suppose that \(m > n\). Then we can limit the second set to
    \(\{b_1, b_2, \ldots, b_{n+1}\}\) and by the exchange
    property, there exists a job \(b_k\) which can be used to
    extend our constructed set. However, this implies that the construction
    would have rejected \(b_k\) which is impossible. Therefore
    \(n \geq m\).
\item Suppose there is an \(i\) such that \(b_i < a_i\).
    Then if we consider the sets \(\{a_1, a_2, \ldots, a_{i-1}\}\)
    and \(\{b_1, b_2, \ldots, b_i\}\) there is a \(b_k\)
    which extends the first set to
    \(\{a_1, a_2, \ldots, a_{i-1}, b_k\}\) (which is assignable).
    However, \(b_k \leq b_i\) which implies that it was rejected
    during construction. This is impossible since at whatever stage of the
    construction of \(\{a_1, a_2, \ldots, a_{i-1}\}\)
    \(b_k\) appeared, it would have been accepted since all
    subsets of an assignable set are assignable. This is a contradiction and
    therefore \(a_i \leq b_i\) for each \(i\).
\end{itemize}

\section{Stable Marriage problem}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---stable-marriage-problem}

Given a set of \(n\) men and \(n\) women each with
their own preferences of opposite sex, it is possible to produce a
stable assignment. In this situation, all \(n!\) matchings are
allowed, however, we are only looking at the stable ones. That is, a
matching is called \emph{stable} if there is no unmatched pair which both
prefer each other to their currently assigned partner (that is
infidelity is mutual). Otherwise, the matching is called \emph{unstable} and
the offending pair(s) is likewise called an \emph{unstable pair}.

Immediately, this problem is very similar to Hall’s marriage theorem (by
its very nature) however, in this case,

\begin{itemize}
\item The preferences of both men and women are taken into account (ie
    symmetric)
\item Each person ranks all members of the opposite sex
\end{itemize}

Furthermore, the question is not about if a matching exists, but instead
whether or not a stable matching exists.

\subsection{Gale-Shapley algorithm}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---gale-shapley-algorithm}

The process consists of rounds

\begin{itemize}
\item In the first round
    
    \begin{enumerate}[label=\arabic*)]
    \item Each man proposes to the woman he prefers most.
    \item Each suited woman replies ''maybe'' to her suitor which she prefers
        most and ''no'' to her other suitors. At this point she the suitor she
        prefers most are ''engaged'' and the men she said no to are ''rejected''
        and remain ''unengaged.''
    \end{enumerate}
\item In each subsequent round
    
    \begin{enumerate}[label=\arabic*)]
    \item Each unengaged man proposes to his next most preferred woman (who has
        not yet been rejected by)
    \item Each suited woman replies ''maybe'' to her suitor which prefers most
        and ''no'' to her other suitors. Note that she may have been previously
        engaged, in which case she may either ''reject'' or keep based on if she
        is suited by a more preferred man.
    \end{enumerate}
\item This process is repeated until everyone is engaged.
\end{itemize}

It is not immediately apparent that this process completes and even
produces a matching. However, there are two key points to consider

\begin{itemize}
\item Each time a woman ''rejects'' a man, the man’s list is shortened and
    due to the finite nature of the lists the process must at some point
    end.
\item No woman can reject all men and hence when the algorithm terminates,
    each woman must have been proposed to at least once and is hence
    ''engaged''.
\end{itemize}

Furthermore, we can see that this algorithm produces a stable matching
since if it didn’t, there would be a pair who both prefer each other
over their current partners. However, this is impossible since the man
would have had to been rejected by her.

\begin{admonition-note}[{}]
\begin{itemize}
\item The algorithm above ensures that the men each get their best possible
    woman (this will be proved later). Conversely, if the algorithm was
    carried out with the women proposing, the women would have each gotten
    their best possible man. These two cases represent extremes of the set
    of possible stable matchings. Furthermore, if the same matching is
    obtained (using the algorithm) in both cases, the stable matching is
    unique.
\item The order in which the men propose does not matter (produces the same
    matching). So you can do the proposals one at a time to simplify things.
\end{itemize}
\end{admonition-note}

\subsubsection{Quality of solution}
\label{develop--math2276:ROOT:page--assignment-problems.adoc---quality-of-solution}

As alluded to before, this algorithm assigns each man to their best
possible woman. This will be instead stated as follows

If a man is rejected by a woman during the algorithm, then in no stable
matching is the man matched to that same woman.

\begin{example}[{Proof}]
Suppose the algorithm produces a matching where each
\(X_i\) is matched to \(Y_i\) (an order will be
established later).

Suppose there exists a stable matching which matches \(X_0\)
to \(Y_1\) which previously rejected \(X_0\). This
implies that \(Y_1\) finds \(X_1\) more preferable
to \(X_0\) (otherwise the algorithm produced an unstable
matching). Furthermore, \(X_1\) must be now matched to some
\(Y_2\) which is also more preferable to \(Y_1\)
otherwise the second matching would be unstable.

We can now continue this process inductively as follows. Given that
\(X_i\) is matched to some \(Y_{i+1}\) which is more
preferable to \(Y_i\). This implies that \(Y_{i+1}\)
finds \(X_{i+1}\) more preferable to \(X_i\)
(otherwise the algorithm produced an unstable matching). Next there are
one of two cases, \(X_{i+1}\) is matched to \(Y_0\)
or \(X_{i+1}\) is matched to \(Y_{i+2}\). We will
look at the former case later. For now, suppose that
\(X_{i+1}\) is matched to \(Y_{i+2}\), then he must
find \(Y_{i+2}\) more preferable to \(Y_{i+1}\)
otherwise this second matching would be unstable.

Notice that at some point, say \(i+1=k\), such that
\(X_k\) is matched to \(Y_0\) since the number of
men (and women) is \(n\). Now at this point, \(X_k\)
finds \(Y_0\) more preferable than \(Y_k\) while
\(Y_0\) must find \(X_0\) more preferable to
\(X_k\) (for the second and algorithm matching to both be
stable). In summary, we see that from the men \(X_0\) to
\(X_k\) prefer their new partners to their old ones while
women \(Y_0\) to \(Y_k\) prefer their old partners
to their new ones. This does affect the stability of either matching but
instead brings into question how the algorithm didn’t produce this
result.

In fact, in the algorithm, \(X_0\) must have proposed to
\(Y_1\) who in turn rejected him for \(X_1\). And
each \(X_i\) must have proposed to \(Y_{i+1}\) who
in turn rejected him for \(X_{i+1}\) (we consider
\(0 = k+1\) for the sake of brevity). This produces a ''cycle
of rejections'' which must start at some point. For the sake of
argument, we can say that it started at \(X_i\), who got
rejected first. That is \(X_i\) proposed to
\(Y_{i+1}\) who rejected him for \(X_{i+1}\) who she
prefers. However, \(X_{i+1}\) would have had to be rejected by
\(Y_{i+2}\), who he prefers, in order to propose and be
matched to \(Y_{i+1}\). This of course contradicts the fact
that \(X_i\) was rejected first and no such cycle exists.

Therefore, There is no stable matching which matches \(X_0\)
to \(Y_1\), who previously rejected him. ◻
\end{example}
\end{document}
