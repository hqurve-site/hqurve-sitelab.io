\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Basis}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(\mathcal{B}\) be a collection of subsets of \(X \neq \varnothing\) such that

\begin{enumerate}[label=\arabic*)]
\item \(X = \bigcup_{B \in \mathcal{B}} B\)
\item If \(B_1, B_2 \in \mathcal{B}\), then
    
    \begin{equation*}
    \forall x \in B_1 \cap B_2: \exists B_3 \in \mathcal{B}: x \in B_3 \subseteq B_1\cap B_2
    \end{equation*}
    
    then, we call \(\mathcal{B}\) a basis for a topology on \(X\).
    For each topology, there is a trivial basis \(\mathcal{B} = \mathcal{T}\).
\end{enumerate}

\begin{admonition-tip}[{}]
According to Dr Tweedle, in future topics, the fact that a countable basis exists may be useful.
\end{admonition-tip}

The concept of basis is a way to generalize the idea of open balls in a metric space generating a topology.
In fact, as shown below, the open balls in a metric space form a basis for a topology.

\begin{admonition-important}[{}]
There need not exists \(B_3 = B_1\cap B_2\). In one dimensional spaces, such a set happens to exist,
but, in general, the intersection need not represent another element of a basis.
This is best illustrated by \myautoref[{}]{develop--math6620:ROOT:page--basis.adoc--metric-balls}
\end{admonition-important}

\section{Generated topology}
\label{develop--math6620:ROOT:page--basis.adoc---generated-topology}

Let \(X\) be a non-empty set with basis \(\mathcal{B}\).
We define the topology \emph{generated} by \(\mathcal{B}\) as

\begin{equation*}
\mathcal{T} = \left\{U \subseteq X \ \middle| \ \forall x \in U: \exists B \in \mathcal{B}: x \in B \subseteq U\right\}
\end{equation*}

From the above, it is clear that the open sets in a generated topology are simply the unions of basis elements.

\begin{proof}[{This set is a topology}]
We have that \(X \in \mathcal{T}\) by property 1, and \(\varnothing \in \mathcal{T}\) \myautoref[{vacuously}]{develop--main:opinions:page--vacuous.adoc}.

Now, consider \(\{U_\alpha\}_{\alpha \in I} \subseteq \mathcal{T}\). Then \(\bigcup_{\alpha \in I} U_\alpha \in \mathcal{T}\)
since \(\bigcup_{\alpha \in I} U_\alpha \subseteq X\) and

\begin{equation*}
\begin{aligned}
&\forall x \in \bigcup_{\alpha \in I} U_\alpha:
    \exists U_\alpha \in \{U_\alpha\}_{\alpha \in I}: x \in U_\alpha
    \quad\text{by definition of union}
\\&\implies\quad \forall x \in \bigcup_{\alpha \in I} U_\alpha:
    \exists B \in \mathcal{B}: x \in B \subseteq U_\alpha \subseteq \bigcup_{\alpha \in I} U_\alpha
    \quad\text{since } U_\alpha \in \mathcal{T}
\end{aligned}
\end{equation*}

Next, consider \(U_1, U_2 \in \mathcal{T}\), Then \(U_1\cap U_2 \in \mathcal{T}\)
since \(U_1 \cap U_2 \subseteq X\) and

\begin{equation*}
\begin{aligned}
&\forall x \in U_1 \cap U_2:
    \left[\exists B_1 \in \mathcal{B}: x \in B_1 \subseteq U_1 \right]
    \text{ and }
    \left[\exists B_2 \in \mathcal{B}: x \in B_2 \subseteq U_2 \right]
    \quad\text{since }U_1, U_2 \in \mathcal{T}
\\&\forall x \in U_1 \cap U_2:
    \exists B_3 \in \mathcal{B}: x \in B_3 \subseteq B_1\cap B_2 \subseteq U_1\cap U_2
    \quad\text{by property 2 of basis}
\end{aligned}
\end{equation*}

Therefore, \(\mathcal{T}\) is a topology on \(X\).
\end{proof}

We can also determine a necessary and sufficient condition for a set to be a basis.

\begin{proposition}[{}]
Let \((X, \mathcal{T})\) be a topology and suppose that \(\mathcal{C} \subseteq \mathcal{T}\).
Then the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(\mathcal{C}\) is a basis for \(\mathcal{T}\)
\item \(\forall U \in \mathcal{T}: \forall x \in U: \exists C \in \mathcal{C}: x \in C \subseteq U\).
    (ie each open set is generated by some element in \(\mathcal{C}\)).
\end{enumerate}

\begin{proof}[{}]
Note that the second statement follows from definition of basis.

Now, assume that the second is true. We have to prove that \(\mathcal{C}\) is a basis
and it generates \(\mathcal{T}\).

We show that \(\bigcup \mathcal{C} = X\) as follows. Define \(C_x \in \mathcal{C}\)
to be such that \(x \in C_x \subseteq X\) (since \(X\) is an open set).
Then,

\begin{equation*}
X \subseteq \bigcup_{x \in X} \{x\} \subseteq \bigcup_{x \in X} C_x \subseteq \bigcup \mathcal{C}
\subseteq \bigcup \mathcal{T} \subseteq X
\end{equation*}

Next, consider \(C_1, C_2 \in \mathcal{C}\) and arbitrary \(x \in C_1 \cap C_2\).
Since \(C_1 \cap C_2\) is open, there exists \(C_3 \in \mathcal{C}\) such that \(x \in C_3 \subseteq (C_1\cap C_2)\).
Therefore we conclude that \(\mathcal{C}\) is a basis.

Let \(\mathcal{T}'\) be the topology that \(\mathcal{C}\) generates.
By the second statement, we see that each \(U \in \mathcal{T}\) may be written as a union of elements
in \(\mathcal{C}\). Therefore, it follows that \(\mathcal{T} \subseteq \mathcal{T}'\).
Conversely, \(\mathcal{T}' \subseteq \mathcal{T}\) since \(\mathcal{C} \subseteq \mathcal{T}\)
and the elements in \(\mathcal{T}'\) are the unions of elements in \(\mathcal{C}\).

Therefore, \(\mathcal{C}\) is a basis for \(\mathcal{T}\).
\end{proof}
\end{proposition}

\subsection{Example with metric spaces}
\label{develop--math6620:ROOT:page--basis.adoc---example-with-metric-spaces}

We claim that the open balls of a metric space form a basis.

\begin{proof}[{}]
Let \((X, d)\) be a metric space. Then, we claim that the following set forms a basis

\begin{equation*}
\mathcal{B} = \{B(x, \varepsilon) \ | \ x \in X,\ \varepsilon > 0\}
\end{equation*}

The first property is satisfied since

\begin{equation*}
\forall x \in X: x \in B(x, 1) \subseteq \bigcup \mathcal{B}
\end{equation*}

For the second property, consider \(B(a, \varepsilon), B(b, \delta) \in \mathcal{B}\) and
arbitrary \(c \in B(a, \varepsilon) \cap B(b, \delta)\).
Let \(r = \min(\varepsilon - d(a,c), \delta - d(b,c))\). Note that \(r > 0\) since these are open balls.
Also, \(B(c, r) \subseteq B(a,\varepsilon)\) since

\begin{equation*}
\forall x \in B(c, r): d(x, a) \leq d(x,c) +d(a,c) < r + d(a,c) \leq (\varepsilon - d(a,c)) + d(a,c) = \varepsilon \implies x \in B(a,\varepsilon)
\end{equation*}

We can likewise prove that \(B(c, r) \subseteq B(b,\delta)\).
Therefore, we have found \(B(c, r) \in \mathcal{B}\) such that \(c \in B(c,r) \subseteq B(a, \varepsilon) \cap B(b, \delta)\).
Since the choice of \(c\) was arbitrary, we have proven the second property.

Therefore, \(\mathcal{B}\) is a basis.
\end{proof}

\begin{figure}[H]\centering
\includegraphics[width=0.4\linewidth]{images/develop-math6620/ROOT/metric_balls}
\caption{Intersection of open balls in metric space}
\label{develop--math6620:ROOT:page--basis.adoc--metric-balls}
\end{figure}

\section{Comparing Topologies}
\label{develop--math6620:ROOT:page--basis.adoc--comparing-topologies}

\begin{theorem}[{}]
Let \(\mathcal{B}\) and \(\mathcal{B}'\) be bases on \(X\) which generate topologies \(\mathcal{T}\) and \(\mathcal{T}'\)
respectively.
Then, the following statements are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(\mathcal{T}'\) is finer than \(\mathcal{T}\) (ie \(\mathcal{T} \subseteq \mathcal{T}'\))
\item \(\forall x \in X: \forall B \in \mathcal{B}: x \in B \implies \exists B' \in \mathcal{B}': x \in B' \subseteq \mathcal{B}\)
\item The basis elements of \(\mathcal{B}\) are open sets in \(\mathcal{T}'\) (ie \(\mathcal{B} \subseteq \mathcal{T}'\))
\end{enumerate}
\end{theorem}

\begin{proof}[{Equivalence of 1 and 3}]
The proof of \((1) \implies (3)\) is trivial since \(\mathcal{B} \subseteq \mathcal{T} \subseteq \mathcal{T}'\).

For the converse, consider arbitrary \(U \in \mathcal{T}\)
and define \(B_x \in \mathcal{B}\) to be such that \(x \in B_x \subseteq U\).
Such a basis element exists by definition of a basis.
Then,

\begin{equation*}
\begin{aligned}
U = \bigcup_{x \in U} B_x \in \mathcal{T}'
\end{aligned}
\end{equation*}

since \(\mathcal{T}'\) is closed under arbitrary unions.
Thus we have proven (1).
\end{proof}

\begin{proof}[{Equivalence of 2 and 3}]
First suppose that (3) is true and consider arbitrary \(x \in X\) and \(B \in \mathcal{B}\) where
\(x \in \mathcal{B}\).
From (3), \(B\) is an open set in \(\mathcal{T}'\) and hence there exists \(B' \in \mathcal{B}'\) such
that \(x \in B' \subseteq B\). Thus we have proven (2).

Conversely, suppose that (2) is true and consider arbitrary \(B \in \mathcal{B}\).
For each \(x \in B\) define \(B'_x \in \mathcal{B}'\) to be such that
\(x \in B'_x \subseteq B\). Such an basis element exists by (2).
Then,

\begin{equation*}
\begin{aligned}
B = \bigcup_{x \in U} B'_x \in \mathcal{T}'
\end{aligned}
\end{equation*}

since \(\mathcal{T}'\) is closed under arbitrary unions.
Thus we have proven (3).
\end{proof}

This theorem may be used to easily prove when two topologies are the same by just comparing their basis elements.

\subsection{Example usage}
\label{develop--math6620:ROOT:page--basis.adoc---example-usage}

Consider the topologies generated by \((a,b)\) and \((a,b]\) in the real numbers \(T\) and \(T'\).
Notice that

\begin{itemize}
\item \((a,b) \in T'\) by considering the union of \(\{(a,c] \ | \ a<c<b\}\).
    Therefore, \(T \subseteq T'\)
\item \((0, 1] \notin T\) since there does not exist \((a,b)\) such that \(1 \in (a,b) \subseteq (0,1]\)
\end{itemize}

Therefore, we see that \(T'\) is strictly finer than \(T\).

\section{Subbasis}
\label{develop--math6620:ROOT:page--basis.adoc---subbasis}

Let \(X\) be a non-empty set and \(\mathcal{S} \subseteq \mathcal{P}(X)\) such that

\begin{equation*}
\bigcup_{S \in \mathcal{S}} S = X
\end{equation*}

Then, we call \(\mathcal{S}\) a subbasis of \(X\).

\begin{admonition-note}[{}]
The subbasis is not attached to any basis or topology. It is just a subset.
\end{admonition-note}

\subsection{Creating a basis using a subbasis}
\label{develop--math6620:ROOT:page--basis.adoc---creating-a-basis-using-a-subbasis}

Let \(X\) be a set with subbasis \(\mathcal{S}\).
Then, we claim that

\begin{equation*}
\mathcal{B} = \{S_1 \cap \cdots \cap S_n \ |  \ n \geq 1,\ S_1, \ldots S_n \in \mathcal{S}\}
\end{equation*}

forms a basis for \(X\).

\begin{admonition-important}[{}]
In class, the restriction \(n \geq 0\) was used. However, I don't want to deal with empty intersections.
It is either \(X\) or \(\varnothing\). In either case, we do not need \(\mathcal{B}\) to contain this
element for it to be a basis
\end{admonition-important}

\begin{proof}[{}]
Clearly \(\bigcup\mathcal{B} = X\) since \(\mathcal{S} \subseteq \mathcal{B}\).
Also, \(\mathcal{B}\) is closed under intersection by definition.
\end{proof}
\end{document}
