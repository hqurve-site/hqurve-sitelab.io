\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Logistic Regression}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In logistic regression, we model the probability of a binary variable using one or more covariates.
There are three equivalent forms

\begin{description}
\item[logit (log odds)] \(\displaystyle \mathrm{logit} \pi = \ln \frac{\pi}{1-\pi}= \beta_0 + \beta_1X\)
\item[odds] \(\displaystyle \mathrm{odds} = \frac{\pi}{1-\pi} = e^{\beta_0 + \beta_1X}\)
\item[probability] \(\displaystyle \pi = \frac{e^{\beta_0 + \beta_1X}}{1 + e^{\beta_0 + \beta_1X}}\)
\end{description}

Covariates are usually encoded as follows

\begin{description}
\item[Continuous] Nothing special is done. Possibly, the data may be transformed to obtain a normal distribution
\item[Binary] Either 0/1 or \(\pm1\). We typically use 0/1
\item[Categorical] Suppose we have levels \(1\ldots k\). Then, we let one of these levels
    be the reference (eg 1) and we define \(x_i\) (\(i=2,\ldots k\)) to indicate whether
    the level is \(i\) using either the 0/1 or \(\pm\) encoding.
\end{description}

The maximum likelihood estimation method is used to estimate the parameters of this model.

\begin{example}[{Likelihood calculation}]
Suppose we would like the model the probability of a disease given some exposure.
Then, we may have the below contingency table. The values in parentheses
are associated probabilities. Note that for each level of exposure, we have a binary random variable
with probability of success (disease) given by \(\displaystyle \pi = \frac{e^{\alpha + \beta_1 x_1}}{1 + e^{\alpha + \beta_1 x_1}}\).

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {\(E\)} & {\(\sim E\)} \\
\hline
{\(D\)} & {21 \(\displaystyle \left(\frac{e^{\alpha+\beta_1}}{1 + e^{\alpha+\beta_1}}\right)\)} & {22 \(\displaystyle \left(\frac{e^{\alpha}}{1 + e^{\alpha}}\right)\)} \\
\hline
{\(\sim D\)} & {6 \(\displaystyle \left(\frac{1}{1 + e^{\alpha+\beta_1}}\right)\)} & {51 \(\displaystyle \left(\frac{1}{1 + e^{\alpha}}\right)\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

So, the likelihood function is

\begin{equation*}
L(\alpha, \beta) =
\left(\frac{e^{\alpha+\beta_1}}{1 + e^{\alpha+\beta_1}}\right)^{21}
\left(\frac{1}{1 + e^{\alpha+\beta_1}}\right)^{6}
\left(\frac{e^{\alpha}}{1 + e^{\alpha}}\right)^{22}
\left(\frac{1}{1 + e^{\alpha}}\right)^{51}
\end{equation*}
\end{example}

The guiding principle behind logistic regression is the linear relationship between
covariates and the logit function.
All assumptions follow directly from this.

After modelling the probability of the binary variable, we need to determine
a threshold to use.
The choice of threshold depends on required sensitivity or specificity of the model.
This analysis is further discussed further during model validation.

\section{Alterations}
\label{develop--stat6160:ROOT:page--logistic.adoc---alterations}

\subsection{Multinomial (Polytomous)}
\label{develop--stat6160:ROOT:page--logistic.adoc---multinomial-polytomous}

Suppose instead of a binary variable, we have a categorical variable with more than \(k>2\) levels.
In such a case, we cannot use the logistic model directly.
Instead, we use \(k-1\) equations and take \(k\)'th level as a reference

\begin{equation*}
\begin{aligned}
\log \frac{P(D=1)}{P(D=k)} &= \alpha_1 + \beta_{i1}x_1 + \cdots
\\
\log \frac{P(D=2)}{P(D=k)} &= \alpha_2 + \beta_{21}x_1 + \cdots
\\
&\vdots
\\
\log \frac{P(D=k-1)}{P(D=k)} &= \alpha_{k-1} + \beta_{(k-1)1}x_1 + \cdots
\end{aligned}
\end{equation*}

The terms \(\frac{P(D=i)}{P(D=k)}\) are called \textbf{log-like} since they are not the true odds.

We can additionally rearrange the above to obtain expressions for the individual probabilities
\(P(D=i)\) for \(i=1, \ldots k\).

\begin{example}[{Probabilities when \(k=3\)}]
Suppose we have that

\begin{equation*}
\log \frac{\pi_1}{\pi_3} = L_1
\quad\text{and}\quad
\log \frac{\pi_2}{\pi_3} = L_2
\end{equation*}

Then

\begin{equation*}
\begin{aligned}
\pi_1 &= \frac{e^{L_1}}{e^{L_1} + e_{L_2} + 1}
\\
\pi_2 &= \frac{2}{e^{L_1} + e_{L_2} + 1}
\\
\pi_3 &= \frac{1}{e^{L_1} + e_{L_2} + 1}
\end{aligned}
\end{equation*}

\begin{admonition-remark}[{}]
This resembles the softmax function
\end{admonition-remark}
\end{example}

We can additionally prove that the model is symmetric to the order of levels of the dependent variable
despite taking the \(k\)'th level as the reference.

\begin{proposition}[{Equivalence to symmetric design}]
Consider the model

\begin{equation*}
\begin{aligned}
\log P(D=1) &= \alpha_1 + \beta_{i1}x_1 + \cdots
\\
\log P(D=2) &= \alpha_2 + \beta_{21}x_1 + \cdots
\\
&\vdots
\\
\log P(D=k-1) &= \alpha_{k-1} + \beta_{(k-1)1}x_1 + \cdots
\\
\log P(D=k) &= \alpha_{k} + \beta_{k1}x_1 + \cdots
\end{aligned}
\end{equation*}

where \(\sum_i a_i = \sum_i \beta_{ij} = 0\).
Then, this model is equivalent to the originally proposed model.

\begin{proof}[{}]
It is clear that the model implies the original model
by subtracting \(\log P(D=k)\) from each of \(\log P(D=i)\).
Therefore, given the symmetric model, we can derive the original model.

To see the reverse, let

\begin{equation*}
\begin{aligned}
\alpha_k' &= \frac{1}{k} \left[-\alpha_1 - \alpha_2 \cdots - \alpha_{k-1}\right]
\\
\alpha_i' &= \alpha_i + \alpha_k' \quad i=1,\ldots (k-1)
\end{aligned}
\end{equation*}

and likewise for the \(\beta_{kj}\).
Now, we have that

\begin{equation*}
P(D=i) = \alpha_i' + \beta_{i1}'x_i
\end{equation*}

Note that the sum of the coefficients (over \(i\)) is zero since

\begin{equation*}
\alpha_1' + \cdots \alpha_{k-1}' + \alpha_k'
= (\alpha_1 + \cdots \alpha_{k-1}) + k\alpha_k' = 0
\end{equation*}

Therefore, given the original equation, we can get the symmetric equation.
\end{proof}
\end{proposition}

\subsection{Ordinal}
\label{develop--stat6160:ROOT:page--logistic.adoc---ordinal}

To model ordinal data, we instead model

\begin{equation*}
\mathrm{logit} P(Y\leq i) = \alpha_i + \beta_1x_1 + \cdots
\end{equation*}

\section{Interpretation}
\label{develop--stat6160:ROOT:page--logistic.adoc---interpretation}

Since logistic regression models the log-odds of a event,
odds-ratios are used to interpret the model.

\begin{equation*}
OR = \frac{odds(Y| x_{11}, x_{21}, \ldots)}{odds(Y| x_{12}, x_{22}, \ldots)}
= \frac{
e^{\alpha + \beta_1x_{11} + \beta_2x_{21} + \cdots}
}
{
e^{\alpha + \beta_1x_{12} + \beta_2x_{22} + \cdots}
}
\end{equation*}

Typically however, we care about the effect of a single variable \(x_i\).
In the case when there are no interactions between covariates,
the odds ratio would reduce to \(e^{\beta_i(x_{i1} - x_{i2})}\).
We may sometimes just look at \(e^{\beta_i}\) which would be equal to

\begin{itemize}
\item the odds ratio for for one unit increase in \(x_i\)
\item the odds ratio for variable \(i\) (if the covariate is binary)
\end{itemize}

Note that in both cases, this \(e^{\beta_i}\) is the odds ratio while \textbf{accounting for other covariates}
as long as the model is a multiple logistic regression model.

We can additionally conduct statistical analysis on the coefficients of a logistic regression model.
We can assume that \(\beta_i\) is approximately normal and as a result produce a Wald-test

\begin{equation*}
Z = \frac{\hat{\beta}_i}{SE(\hat{\beta}_i)} \sim N(0,1)
\end{equation*}

and the associated confidence intervals.
Also, sometimes, we may test \(Z^2\sim \chi^2_1\) instead.

\section{Model selection and goodness of fit}
\label{develop--stat6160:ROOT:page--logistic.adoc---model-selection-and-goodness-of-fit}

When we have multiple covariates, it is important to consider
whether or not all covariates (and interactions) are necessary for the model.
Therefore, it is important to choose the best model.
Typically the backward or forward elimination method is used.

\begin{description}
\item[Forward elimination] Start with no covariates and add one at a time (most significant first).
\item[Backward elimination] Start with all covariates and remove one at a time (least significant first).
\end{description}

\begin{admonition-note}[{}]
Forward elimination is preferred when there are many covariates.
CAUTION: This can lead to over-fitting if we have many variables and too small of a simple size.
\end{admonition-note}

At each stage of the elimination process, we compare two models:
the ``reduced'' model and the ``full'' model.
These models are compared using the likelihood ratio test statistic

\begin{equation*}
\chi^2 = -2\left[L(\mathrm{reduced}) - L(\mathrm{full})\right]
\sim \chi^2_p
\end{equation*}

where \(L(\mathrm{model})\) is the log likelihood of model
and \(p\) is the difference in the number of parameters of the two models.

\subsection{Hosmer-Lemeshow goodness of fit}
\label{develop--stat6160:ROOT:page--logistic.adoc---hosmer-lemeshow-goodness-of-fit}

The Hosmer-Lemeshow goodness of fit procedure can be used to assess a logistic regression model.
The procedure is conducted as follows

\begin{enumerate}[label=\arabic*)]
\item Fit the model and associate each observation with the estimated probability
\item Cluster observations based on estimated probability.  For example,
    we can cluster by percentile or ranges. Let \(n_j\) be the number of observations in \(j\)'th cluster.
\item Entry \((i, j)\) contains the number of observations with probabilities in the \(j\)'th cluster
    but whose true value is \(i\) (0 or 1). Let \(o_j\) be the number of observations with 1 in the \(j\)'th cluster
\item For the \(j\)'th cluster, we compute \(\hat{\overbar{\pi}}_j\) to be the average probability
    for observations in the cluster.
    Note that we expect that \(o_j \sim Bin(n_j, \hat{\overbar{\pi}}_j)\).
\item The expected values for row \(1\) of the \(j\)'th cluster is \(n_j\hat{\overbar{\pi}}_j\)
    and the expected values for row \(0\) are \(n_j(1-\hat{\overbar{\pi}}_j)\)
\end{enumerate}

Then, we define

\begin{equation*}
\hat{C} = \sum_{j=1}^g \frac{(o_j-n_j\hat{\overbar{\pi}}_j)^2}{n_j\hat{\overbar{\pi}}_j(1-\hat{\overbar{\pi}}_j)} \approx\chi^2_{g-2}
\end{equation*}

\subsection{Receiver Operating Character (ROC) Curve}
\label{develop--stat6160:ROOT:page--logistic.adoc---receiver-operating-character-roc-curve}

Once we have a logistic regression model, we may want to use the model for prediction.
For example, we may have a model that predicts presence of a disease.
However, the model only gives you a probability of whether you have the disease.
We would like to collapse this probability to a simple definitive statement.
We need a \textbf{cut-off} probability, above which we assign positive and below which we assign negative.

The choice of cut-off probability is governed by the desired sensitivity (probability of correctly identifying positive)
and specificity (probability of correctly identifying negative) of the model.
If we choose a cut-off probability that is

\begin{itemize}
\item too small, we assign everything as positive. Therefore, the sensitivity would be approximately 1 while the specificity is 0
\item too low, we assign everything as negative. Therefore, the sensitivity would be approximately 0 while the specificity is 1
\end{itemize}

We can illustrate the different sensitivities and specificities on a \textbf{receiver operating characteristic} (ROC) curve.
The x-axis is the false positive rate (1-specificity; \(P(Y=1|X=0)\)) and the y-axis is the true positive rate (sensitivity; \(P(Y=1|X=1)\)).
Typically, this curve is convex and the area underneath represents the \textbf{concordance index}.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6160/ROOT/roc-curve}
\caption{Estimated ROC Curve from \url{https://en.wikipedia.org/wiki/File:Roccurves.png}}
\end{figure}

Note that we always want the true positive rate to be greater than the false positive rate.
We typically want the area under the curve to be at least 0.7. If it is less than 0.5,
the test is worse than a random guess.

To estimate the ROC curve, we can plot the true positive rate and false positive rate for different cutoff values.

\subsection{Implementation in R}
\label{develop--stat6160:ROOT:page--logistic.adoc---implementation-in-r}

Suppose we want to model how well a disease is controlled using two different
treatments

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {Controlled} & {Not controlled} \\
\hline
{Surgery} & {21} & {2} \\
\hline
{Radiation} & {15} & {3} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

We can use the code below to conduct this analysis.
Note that each row corresponds to a different treatment
and the columns are (treatment, outcome1, outcome2).

\begin{listing}[{}]
# Code from https://online.stat.psu.edu/stat504/book/export/html/842
count = cbind(
controlled = c(21, 15),
notcontrolled = c(2,3)
)
treatment = factor(c(1,0))
result = glm(count~treatment, family="binomial")
summary(result)
\end{listing}

Alternatively

\begin{listing}[{}]
df = data.frame(
   controlled = c(21, 15),
   notcontrolled = c(2,3),
   treatment = factor(c(1,0))
)
df
result = glm(cbind(controlled, notcontrolled)~treatment, data=df, family=binomial("logit"))
summary(result)
\end{listing}
\end{document}
