\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Binomial Theorem}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
The numbers \(\binom{n}{r}\) are known as binomial
coefficients. This is due to fact that the coefficient of
\(x^r\) in \((1+x)^n\) is precisely equal to
\(\binom{n}{r}\). In fact

\begin{equation*}
(1+x)^n = \sum_{r=0}^n \binom{n}{r} x^r
\end{equation*}

But why is this? Well perhaps the simplest way to see it is by counting
how many times \(x^r\) appears in the expansion of
\((1+x)^n\). This can be done by choosing \(r\) of
the brackets containing \(1+x\) to contribute an
\(x\). Alternatively, we could choose \(n-r\) of the
brackets to choose a \(1\) from. Here we see our first
identity

\begin{equation*}
\binom{n}{r} = \binom{n}{n-r}
\end{equation*}

In this section, we will explore various properties of binomial
coefficients

\section{Proper binomial theorem}
\label{develop--math2276:ROOT:page--binomial-theorem.adoc---proper-binomial-theorem}

In fact, the binomial theorem states that

\begin{equation*}
(x+y)^n = \sum_{r=0}^n \binom{n}{r} x^r y^{n-r}
\end{equation*}

This is a more general case than the previous statement, however, its
proof is basically identical.

\section{Pascal’s Triangle}
\label{develop--math2276:ROOT:page--binomial-theorem.adoc---pascals-triangle}

Pascal’s triangle is a structure made entirely of binomial coefficients.
Additionally, it is a useful tool when examining binomial coefficients.
Heres the first \(5\) rows of it

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[r,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(n=0\)} & {} & {} & {} & {} & {} & {1} & {} & {} & {} & {} & {} \\
\hline
{\(n=1\)} & {} & {} & {} & {} & {1} & {} & {1} & {} & {} & {} & {} \\
\hline
{\(n=2\)} & {} & {} & {} & {1} & {} & {2} & {} & {1} & {} & {} & {} \\
\hline
{\(n=3\)} & {} & {} & {1} & {} & {3} & {} & {3} & {} & {1} & {} & {} \\
\hline
{\(n=4\)} & {} & {1} & {} & {4} & {} & {6} & {} & {4} & {} & {1} & {} \\
\hline
{\(n=5\)} & {1} & {} & {5} & {} & {10} & {} & {10} & {} & {5} & {} & {1} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

As seen, the \(n\)’th row is made up of the values
\(\binom{n}{0}, \binom{n}{1}, \ldots, \binom{n}{1}\).
Additionally, each entry in the computed by summing the two values above
it (taking missing values as zero). This clearly matches the following
property

\begin{equation*}
\binom{n}{r} = \binom{n-1}{r-1} + \binom{n-1}{r}
\end{equation*}

\begin{admonition-note}[{}]
This identity can be derived arithmetically or via the following
combinatorial argument. Consider how to construct the choices of
\(r\) objects from \(n\), focusing on the first
object. If the first object is in the selection we have
\(\binom{n-1}{r-1}\) ways to select the remaining objects
otherwise there are \(\binom{n-1}{r}\) ways to select the
remaining objects.
\end{admonition-note}

\section{Analysis of Binomial Theorem}
\label{develop--math2276:ROOT:page--binomial-theorem.adoc---analysis-of-binomial-theorem}

The question arises of whether the binomial theorem also applies to
negative as well as other real values of n and if it does, to what
extent. In other words, is it possible to expand \((1+x)^n\)
for all real values of \(n\)?

Firstly, it should be noted that the factorial function is only defined
on non-negative integers, so its no good trying to extend that. However,
we can consider the following for non-negative integers

\begin{equation*}
(1+x)^n = \sum_{r=0}^{n} \binom{n}{r}x^r = 1 + nx + \frac{n(n-1)}{1(2)} + \frac{n(n-1)(n-2)}{1(2)(3)} + \cdots
\end{equation*}

Firstly, notice that the sum on the left can truly be considered to be
infinite since, for non-negative integers, there will exist a point
where all following terms contain a zero in the numerators product. On
the other hand, this never happens for
\(n \notin \mathbb{Z}^+ \cup \{0\}\).

So does this formula work although it is just an extended formula? Does
it reflect the actual behaviour of the function? Yes, of course it does,
since it is simply the taylor series expansion. Perhaps now it would be
fair to state that the following is an alternative definition for
binomial coefficients for all \(n\)

\begin{equation*}
\binom{n}{r} = \frac{n(n-1)(n-2) \cdots (n+r-1)}{1(2)(3)\cdots (r)} = \frac{\prod_{i=0}^{r-1} (n-i)}{r!}
\end{equation*}

Note that although we discussed this formula as an alternative
definition, it fits perfectly with the definition that the binomial
coefficient \(\binom{n}{r}\) is the coefficient of
\(x^r\) in the expansion of \((1+x)^n\).

\subsection{Special case when \(n\) is a negative integer}
\label{develop--math2276:ROOT:page--binomial-theorem.adoc---special-case-when-latex-backslashn-latex-backslash-is-a-negative-integer}

Although the factorial is not defined for negative integers, we can try
to deduce a simplified formula as followed

\begin{equation*}
\binom{-n}{r}= \frac{\prod_{i=0}^{r-1} (-n-i)}{r!} = (-1)^r \frac{\prod_{i=0}^{r-1} (n+i)}{r!} = (-1)^r \frac{(n+r-1)!}{(n-1)!r!} = (-1)^r \binom{n+r-1}{r}
\end{equation*}

From this the following expansion is immediate

\begin{equation*}
(1+x)^{-n} = \sum_{r=0}^{\infty} (-1)^r \binom{n+r-1}{r} x^r
\end{equation*}

and can be simplified if we replace \(x\) with
\(-x\)

\begin{equation*}
(1-x)^{-n} = \sum_{r=0}^{\infty} \binom{n+r-1}{r} x^r
\end{equation*}

\section{Generalization: Multinomial theorem}
\label{develop--math2276:ROOT:page--binomial-theorem.adoc---generalization-multinomial-theorem}

The binomial theorem is in fact a special case of the more general
multinomial theorem. The multinomial theorem states

\begin{equation*}
(x_1 + x_2 + \ldots + x_m)^n
    = \sum_{k_1 + k_2 +\ldots + k_m = n}
        \binom{n}{k_1, k_2, \ldots, k_m} \prod_{t=1}^m x_t^{k_t}
\end{equation*}

where

\begin{equation*}
\binom{n}{k_1, k_2, \ldots, k_m}  = \frac{n!}{k_1! k_2! \ldots k_m!}
\end{equation*}

and is called the multinomial coefficient.

\begin{admonition-note}[{}]
The multinomial coefficient is equivalent to the number of ways to order
non-distinct objects.
\end{admonition-note}
\end{document}
