\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Power Series}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A \emph{power series expansion} of a function \(y(x)\) about a
point \(x - x_0\) is a sum

\begin{equation*}
y(x) = \sum_{i=0}^\infty a_i (x-x_0)^i
\end{equation*}

where the \(a_i\) are constants. Note that such a series may
not necessarily converge and hence we must consider the set of points on
which it is valid, also called the \emph{domain of validity}.

\begin{admonition-note}[{}]
A power series need not have non-negative coefficients of
\((x-x_0)\) but we will speak of this later.
\end{admonition-note}

\section{Classification of points}
\label{develop--math2271:ROOT:page--power-series.adoc---classification-of-points}

In this section, we will only focus on second order homogeneous linear
differential equations however methods can be extended to higher orders.
Consider the second order homogeneous linear equation

\begin{equation*}
y'' + p(x)y' + q(x) y = 0
\end{equation*}

Then the point \(x=x_0\) is called

\begin{itemize}
\item \emph{ordinary} if \(p(x_0)\) and \(q(x_0)\) are both
    finite
\item \emph{singular} if \(p(x)\) or \(q(x)\) have a pole at
    \(x=x_0\). Additionally \(x=x_0\) can be further
    classified
    
    \begin{itemize}
    \item \emph{regular} if \(p(x)(x-x_0)\) and
        \(q(x)(x-x_0)^2\) are both finite as \(x \to x_0\)
    \item \emph{irregular} otherwise.
    \end{itemize}
\end{itemize}

\section{Solutions about ordinary points}
\label{develop--math2271:ROOT:page--power-series.adoc---solutions-about-ordinary-points}

Let \(x=x_0\) be an ordinary point of a linear second order
homogeneous ODE. Then, there exists a unique power series solution of
this ODE about the point \(x=x_0\) of the form

\begin{equation*}
y(x) = \sum_{n=0}^\infty a_n (x-x_0)^n
\end{equation*}

with two arbitrary constants and with domain of validity

\begin{equation*}
|x-x_0| < R
\end{equation*}

where \(R\) is the distance between \(x_0\) and the
nearest singular point. If there is no singular point, the solution is
valid for all real numbers.

\section{Method of Frobenius: Solutions about regular singular points}
\label{develop--math2271:ROOT:page--power-series.adoc---method-of-frobenius-solutions-about-regular-singular-points}

Let \(x=x_0\) be a regular singular point of a linear second
order homogeneous ODE. Then, the equation has at least one \emph{Frobenius
series solution} about that point of the form

\begin{equation*}
y(x) = \sum_{n=0}^\infty a_n (x-x_0)^{n+c}
\end{equation*}

where \(c\) is a constant to be determined using the \emph{indicial
equation}. The solution is valid for \(|x-x_0| < R\) where
\(R\) is the distance between \(x=x_0\) and the
nearest singular point. Note that if \(c < 0\), the series
does not converge at \(x=x_0\) and this point must be removed
from the domain of validity.

Also, note that there may only be one Frobenius series solution, so the
other linearly independent solutions would have to be generated using
another method.

\subsection{The indicial equation}
\label{develop--math2271:ROOT:page--power-series.adoc---the-indicial-equation}

The indicial equation is the coefficient of the lowest power of
\((x-x_0)\) when the series solution is substituted into the
ODE. Note that since we have second order differentials, this equation
would be a polynomial of degree \(2\) in \(c\) whose
roots we will use to generate the Frobenius series solutions.

Let \(c_1\) and \(c_2\) be the roots where
\(c_1 \geq c_2\), then we have the following cases

\begin{itemize}
\item Case 1: \(c_1-c_2 \notin \mathbb{Z}\). In this case, there
    exists two Frobenius series solutions
\item Case 2: \(c_1-c_2 = 0\). In this case, there is only one
    Frobenius series solution and the second linearly independent solution
    must be found using another method such as reduction of order.
\item \(c_1 - c_2 \in \mathbb{Z}^+\). Then, we have two cases
    
    \begin{itemize}
    \item Case 3a: The larger root \(c_1\) gives a solution but
        \(c_2\) does not. In this case, the second linearly
        independent solution will have to be found using another method.
    \item Case 3b: the smaller root \(c_2\) generates two linearly
        independent Frobenius series solutions.
    \end{itemize}
\end{itemize}

\section{Bessel’s Equation}
\label{develop--math2271:ROOT:page--power-series.adoc---bessels-equation}

A second order ODE of the form

\begin{equation*}
x^2y'' + xy' + (x^2 - \alpha^2)y = 0 \quad\text{where }\alpha \geq 0
\end{equation*}

is commonly referred to as \emph{Bessel’s Equation} of order n. Notice that
\(x=0\) is a regular singular point and hence, we can assume a
Frobenius series form

\begin{equation*}
y = \sum_{n=0}^\infty a_n x^{n+c}
\end{equation*}

and upon substitution into the equation, we obtain

\begin{equation*}
\begin{aligned}
    0
    &= x^2 \sum_{n=0}^\infty (n+c)(n+c-1)a_nx^{n+c-2} +
        x\sum_{n=0}^\infty (n+c)a_nx^{n+c-1} +
        (x^2-\alpha^2) \sum_{n=0}^\infty a_n x^{n+c}\\
    &= \sum_{n=0}^\infty (n+c)(n+c-1)a_nx^{n+c} +
        \sum_{n=0}^\infty (n+c)a_nx^{n+c} +
        \sum_{n=0}^\infty a_n x^{n+c+2}
        - \sum_{n=0}^{\infty} \alpha^2a_n x^{n+c}\\
    &= \left[c(c-1)a_0x^c + (c+1)(c)x^{c+1} + \sum_{n=2}^\infty (n+c)(n+c-1)a_nx^{n+c}\right]
        \\&\hspace{4em} + \left[c a_0 x^c + (c+1)x^{c+1} + \sum_{n=2}^\infty (n+c)a_nx^{n+c}\right]
        + \sum_{n=2}^\infty a_{n-2} x^{n+c}
        \\&\hspace{4em}- \left[\alpha^2a_0x^c + \alpha^2a_1x^{c+1} + \sum_{n=2}^{\infty} \alpha^2a_n x^{n+c}\right]\\
    &= (c(c-1) + c - \alpha^2)a_0x^c + ((c+1)c + (c+1) - \alpha^2)a_1x^{c+1}
      \\&\hspace{4em}+ \sum_{n=2}^\infty \left[(n+c)(n+c-1)a_n + (n+c)a_n + a_{n-2} - \alpha^2a_n\right]x^{n+c}\\
    &= (c^2 - \alpha^2)a_0x^c + ((c+1)^2 - \alpha^2)a_1x^{c+1}
    + \sum_{n=2}^\infty \left[((n+c)^2 - \alpha^2)a_n + a_{n-2}\right]x^{n+c}\end{aligned}
\end{equation*}

Then, from our indicial equation, we get that
\(c = \pm \alpha^2\). Also,

\begin{equation*}
(n+c)^2 -\alpha^2 = (n\pm \alpha) - \alpha^2 = (n + \alpha \pm \alpha)(n-\alpha \pm\alpha)
    = n(n\pm 2\alpha)
\end{equation*}

which may be zero when
\(n - 2\alpha = 0 \iff \alpha = \frac{n}{2}\).

\section{Legendre equation}
\label{develop--math2271:ROOT:page--power-series.adoc---legendre-equation}

The equation

\begin{equation*}
(1-x^2)y'' - 2xy' + \alpha(\alpha+1)y = 0 \quad\text{where } \alpha \in \mathbb{R}
\end{equation*}

is called \emph{Legendre’s equation}.
\end{document}
