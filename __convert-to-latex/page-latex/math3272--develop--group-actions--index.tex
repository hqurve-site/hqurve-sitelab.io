\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Group Actions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
\def\normalsubgroup{\trianglelefteq}
\DeclareMathOperator{\ord}{ord}

% Title omitted
Let \(G\) be a group and \(X\). Then the operation
\(\alpha: G\times X \to X\) is a \emph{group action}
if the two properties hold

\begin{description}
\item[Identity] \(\alpha(e,x) = x\)
\item[Compatibility] \(\alpha(g,\alpha(h,x)) = \alpha(gh,x)\)
\end{description}

Then we say \(G\) \emph{acts on} \(X\). Additionally, as typical,
we may omit explicit mention of \(\alpha\) if
it is understood and we may use some infix notation (or implied action)
instead of function notation.

\begin{admonition-note}[{}]
What is defined here is a \emph{left group action}. For a right group action,
the composition of \(g\) and \(h\) is reversed in compatibility.
This leads to little meaningful difference and left actions are used
as they are natural especially when speaking of groups of functions.
\end{admonition-note}

\section{Orbits and Stabalizers}
\label{develop--math3272:group-actions:page--index.adoc---orbits-and-stabalizers}

Let \(G\) act on \(X\) and \(x \in X\). Then we define the following

\begin{description}
\item[Orbit of \(x\)] \([X] = \{g x : g \in G\} \subseteq X\)
\item[Stabilizer of \(x\)] \(G_x = \{g \in G : gx = x\} \leq G\)
\end{description}

Furthermore, we may define an equivalence relation on \(X\) where two elements
are related iff they belong to the same orbit. Hence, the orbits of \(X\)
partition \(X\).

\begin{example}[{Proof of subgroup of stabalizer}]
Clearly, \(e \in G_x\) by definition of group actions. Now, suppose
that \(g,h \in G_x\), then \(x = e x = h^{-1}h x = h^{-1} x\) and
\(gh x = g x =x \). Therefore we are done.
\end{example}

We also have the following interesting property

\begin{equation*}
G_{gx} = gG_xg^{-1}
\end{equation*}

\begin{example}[{Proof}]
\begin{equation*}
h \in G_{gx}
\iff hgx = gx
\iff g^{-1}hg x = x
\iff g^{-1}hg \in G_x
\iff h \in gG_xg^{-1}
\end{equation*}
\end{example}

\subsection{Orbit stabilzer theorem}
\label{develop--math3272:group-actions:page--index.adoc---orbit-stabilzer-theorem}

Let \(x \in X\), then we claim there is a bijective correspondence

\begin{equation*}
[x] \longleftrightarrow G/G_x
\end{equation*}

where the right is the set of left cosets of \(G_x\) and not
the actual quotient group as \(G_x\) may not be normal.
This correspondence is defined by

\begin{equation*}
y \longleftrightarrow \{h \in G: hx = y\} = gG_x
\end{equation*}

where \(y = gx\). Then we claim that this mapping is well defined and bijective.

\begin{example}[{Proof}]
Firstly, let \(y \in [x]\) and notice that
if \(gx = y\)

\begin{equation*}
h \in gG_x
\iff g^{-1}h \in G_x
\iff g^{-1}h x = x
\iff hx = gx = y
\end{equation*}

Therefore \(gG_x = \{h \in G: hx = y\}\). Furthermore, we have
shown that it is independent of the choice of \(g\).

Conversely, if we consider \(gG_x\), there exists an element,
namely \(gx\), such that \(\forall h \in gG_x\), \(hx = gx\).
Therefore, the reverse mapping is also well defined.

Now, clearly the forward mapping is injective since

\begin{equation*}
\{h \in G: hx = y_1\} = \{h \in G: hx = y_2\} \implies y_1 = y_2
\end{equation*}

Additionally, it is surjective since for each \(gG_x\),
there exists an element \(y = gx\) such that all elements
map \(x\) to.

Therefore we have a bijection and we are done.
\end{example}

Using this relation, we see that if \([x]\) is finite,
\([G:G_x]\) is also finite and vice versa furthermore, both
are equal. Then together with Lagrange's theorem, we see
that

\begin{equation*}
|[x]|\ |G_x| = |G|
\end{equation*}

This is the \emph{orbit-stabalizer theorem}.

\section{Correspondence with the symmetric group}
\label{develop--math3272:group-actions:page--index.adoc---correspondence-with-the-symmetric-group}

Let \(G\) act on \(X\). Then, we can think of
each element \(g \in G\) as a permutation on \(X\)
since it must be bijective since \(g^{-1} \in G\)
and the compatibility of the operation. Therefore,
there is a homomorphism \(\Phi: G \to S_X\)
defined by

\begin{equation*}
\Phi(g) = \{x \mapsto \alpha(g, x)\}
\end{equation*}

Now, notice that the signature of a group action is \((G\times X) \times X\)
while the signature of the above homomorphism is \(G \times (X \times X)\).
There is indeed a bijective correspondence as follows

\begin{equation*}
\left\{\begin{array}{c}\text{group actions} \\ \alpha: G \times X \to X\end{array}\right\}
\longleftrightarrow
\left\{\begin{array}{c}\text{homomorphisms from } G \text{ to } S_X \\ \Phi: G\to S_X\end{array}\right\}
\end{equation*}

defined by

\begin{equation*}
\begin{aligned}
\alpha &\longrightarrow \Phi_\alpha &&\quad\text{where}\ \Phi_\alpha(g) = \{x \mapsto \alpha(g,x)\}\\
\alpha_\Phi &\longleftarrow \Phi &&\quad\text{where}\ \alpha_\Phi(g,x) = \Phi(g)(x)\
\end{aligned}
\end{equation*}

Then we claim that \(\alpha\) are inverse functions and map group actions to homomorphisms
and vice versa.

\begin{example}[{Proof}]
Firstly, note that \(\Phi_\alpha\) is a homomorphism since

\begin{equation*}
\begin{aligned}
\Phi_\alpha(g)\circ\Phi_\alpha(h)
&= \{x \mapsto \alpha(g,x)\}\circ \{x\mapsto \alpha(h,x)\}
\\&= \{x\mapsto \alpha(g,\alpha(h,x))\}
\\&= \{x\mapsto \alpha(gh,x)\}
\\&= \Phi_\alpha(gh)
\end{aligned}
\end{equation*}

Next, \(\alpha_\Phi\) is a group action since

\begin{equation*}
\alpha_\Phi(e,x) = \Phi(e)(x) = \{x\mapsto x\}(x) = x
\end{equation*}

since \(\Phi\) is a homomorphism it preserves identity. Next,

\begin{equation*}
\begin{aligned}
\alpha_\Phi(g,\alpha_\Phi(h,x))
&= \alpha_\Phi(g,\Phi(h)(x))
\\&= \Phi(g)(\Phi(h)(x))
\\&= (\Phi(g)\circ \Phi(h))(x)
\\&= \Phi(gh)(x)
\\&= \alpha_\Phi(gh, x)
\end{aligned}
\end{equation*}

Therefore \(\alpha_\Phi\) is a group action. Next,
we need to show that \(\alpha = \alpha_{\Phi_\alpha}\)

\begin{equation*}
\alpha_{\Phi_\alpha}(g,x)
= \Phi_\alpha(g)(x)
= \{x\mapsto \alpha(g,x)\}(x)
= \alpha(g,x)
\end{equation*}

Conversely, we need to show that \(\Phi_{\alpha_\Phi} = \Phi\)

\begin{equation*}
\Phi_{\alpha_\Phi}(g)
= \{x\mapsto \alpha_\Phi(g,x)\}
= \{x\mapsto \Phi(g)(x)\}
= \Phi(g)
\end{equation*}

Therefore these two functions are indeed inverses of each other
and we are done.
\end{example}

\section{Class Equation}
\label{develop--math3272:group-actions:page--index.adoc---class-equation}

Let \(G\) act on \(X\) and define

\begin{itemize}
\item \(Z =  \{x \in X: |[x]| = 1 \}\)
\item \(I\) be a set of representatives of all orbits which contain more than one element
\end{itemize}

Then,

\begin{equation*}
|X|
= |Z| + \sum_{x\in I} |[x]|
= |Z| + \sum_{x\in I} [G:G_x]
\end{equation*}

\subsection{Class equation for conjugacy}
\label{develop--math3272:group-actions:page--index.adoc---class-equation-for-conjugacy}

In the special case when \(X = G\) and \(G\) is acting with conjugacy, the class
equation resolves to

\begin{equation*}
|G| = |Z(G)| + \sum_{a \in I}[G:C_G(a)]
\end{equation*}
\end{document}
