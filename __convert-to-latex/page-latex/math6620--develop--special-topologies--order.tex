\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Order topology}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \((X, <)\) be a linearly ordered set (sometimes also called a
\href{https://en.wikipedia.org/wiki/Total\_order\#Strict\_and\_non-strict\_total\_orders}{strict total order})
with \(|X| \geq 2\).
Then, we we claim that the following forms a basis for \(X\)

\begin{equation*}
\begin{aligned}
\mathcal{B} &= \{(a,b) \ |\ a < b\}\\
            &\quad \cup\ \left\{{[}a_0, b) \ \middle|\ b \in X \text{ and } \exists a_0 = \min X \right\}\\
            &\quad \cup\ \left\{ {(}a, b_0] \ \middle|\ a \in X \text{ and } \exists b_0 = \max X \right\}
\end{aligned}
\end{equation*}

where \((a,b)\), \([a_0, b)\) and \((a, b_0]\) are defined as usual.
We call the generated topology the \emph{order topology}.
It is clear that this topology must be Hausdorff.

\begin{admonition-note}[{}]
If the minimum does not exist, we omit the second set (likewise for the maximum and third set)
\end{admonition-note}

\begin{proof}[{Basis}]
First, we need to prove that \(\bigcup \mathcal{B} = X\).
Consider arbitrary \(x \in X\). There are three cases

\begin{itemize}
\item If \(x\) is neither the maximum nor minimum, there exists \(a,b \in X\) such that \(a < x< b\)
\item If \(x\) is the minimum, then any \([x, b)\) contains \(x\)
\item If \(x\) is the maximum, then any \((a, x]\) contains \(x\)
\end{itemize}

Therefore, \(\bigcup \mathcal{B} = X\).
Next notice that the intersection of any two of the intervals is also an interval (or the empty set).
Therefore, the second property of basis is satisfied by using \(B_3\) equal to the intersection.
\end{proof}

\section{Example with \(\mathbb{R}^2\)}
\label{develop--math6620:special-topologies:page--order.adoc---example-with-latex-backslash-latex-backslashmathbb-latex-openbracer-latex-closebrace-latex-caret2-latex-backslash}

Consider \(\mathbb{R}^2\) with the lexicographical (aka dictionary) order as defined below

\begin{equation*}
(a_1, b_1) < (a_2, b_2)
\quad\iff\quad
a_1 < a_2
\text{ or } (a_1= a_2 \text{ and } b_1 < b_2)
\end{equation*}

Consider the interval \(\Big((0, 0), \ (1,1)\Big)\). Then, this interval is

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math6620/special-topologies/lexiographical-r2}
\end{figure}

\begin{admonition-remark}[{}]
It is unfortunate that the notation for intervals and points (tuples) are the same.
\end{admonition-remark}

\begin{proposition}[{}]
Let \(\mathcal{T}\) be the topology.
Then, we claim that \(\mathcal{T}\) is the product topology of the discrete topology on \(\mathbb{R}\)
and the normal topology on \(\mathbb{R}\).
Since a basis for the product topology is given by

\begin{equation*}
\left\{\{x\} \times (a,b) \ \middle| \ x,a,b \in \mathbb{R} \text{ and } a < b\right\}
\end{equation*}

We have a clear understanding of the topology generated in this case.

\begin{proof}[{}]
To prove that the topologies are equal consider the following

\begin{itemize}
\item Notice that each \(\{x\}\times (a,b)\) in the basis of the product topology is in the order topology
    since \(\{x\} \times (a,b) = \Big((x,a), (x,b)\Big)\)
\item Consider \(\Big((a_1, b_1), \ (a_2, b_2)\Big)\) in the basis of the order topology.
    
    \begin{itemize}
    \item If \(a_1=a_2\), \(\Big((a_1, b_1), \ (a_2, b_2)\Big) = \{a_1\}\times(b_1,b_2)\) which is in the product topology
    \item If \(a_1 < a_2\)
        
        \begin{equation*}
        \begin{aligned}
        &\Big((a_1, b_1), \ (a_2, b_2)\Big)
        \\&=
        \left(\{a_1\}\times (b_1, \infty)\right)
        \cup \left(\bigcup_{a_1 < x< a_2} \{x\}\times\mathbb{R} \right)
        \cup \left(\{a_2\} \times (-\infty, b_2)\right)
        \end{aligned}
        \end{equation*}
        
        which is the union of elements in the product topology.
    \end{itemize}
\end{itemize}

Therefore, both topologies must be the same since they contain each other's basis.
\end{proof}
\end{proposition}

\begin{corollary}[{}]
The lexicographical topology on \(\mathbb{R}^2\) is metrizable with metric

\begin{equation*}
d((a_1, b_1),(a_2, b_2)) = \begin{cases}
1, &\quad\text{if } a_1 \neq a_2\\
\min(|b_1-b_2|,1), &\quad\text{otherwise}
\end{cases}
\end{equation*}

\begin{proof}[{}]
Note that \(d\) satisfies the triangle inequality since
for any \((a_1, b_1), (a_2, b_2), (a_3, b_3)\),

\begin{equation*}
d((a_1, b_1), (a_2, b_2))
\leq
d((a_1, b_1), (a_3, b_3))
+ d((a_3, b_3), (a_2, b_2))
\end{equation*}

since

\begin{itemize}
\item if \(a_1 \neq a_2\), either \(a_1 \neq a_3\) or \(a_2 \neq a_3\).
    Therefore the left side is 1 and the right side is at least 1
\item if \(a_1 = a_2\) and \(a_1 \neq a_3\), the left side is at most 1 and the right
    side is 2
\item if \(a_1=a_2=a_3\), the inequation is equivalent to
    
    \begin{equation*}
    \min(|b_1 -b_2|, 1)
    \leq
    \min(|b_1 -b_3|, 1)
    + \min(|b_3 -b_2|, 1)
    \end{equation*}
\end{itemize}

Therefore \(d\) is a metric.

Also, the balls generated by \(d\) are either
\(\mathbb{R}^2\) or \(\{x\}\times (y-\varepsilon, y+\varepsilon)\)
with \(\varepsilon < 1\).
The non-trivial balls are also basis elements of the order topology.
Conversely, the basis elements of the order topology are the unions
of non-trivial balls.
Therefore the topology generated by \(d\) is the order topology.
\end{proof}
\end{corollary}
\end{document}
