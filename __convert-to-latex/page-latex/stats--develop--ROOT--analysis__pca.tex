\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Principal Component Analysis}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
A \textbf{principal component analysis} (PCA) is concerned with explaining the variance–covariance
structure of a set of variables through a few \textbf{linear} combinations of the variables.

The objectives include

\begin{itemize}
\item Dimensionality reduction
\item Interpretability and visualization
\item Noise Reduction
\item Multicollinearity handling
\item Avoiding overfitting
\end{itemize}

Suppose that we have \(n\) observations, each with \(p\) variables.
Also, assume that the data is centered (zero mean); in the case that it isn't,
the mean can be subtracted and then later readded.
Often, for high dimensional data, the structure of the covariance matrix
can be explained by \(k\) (\(<p\)) components.
PCA seeks to find these components while trying to preserve the original structure
of the data.
PCA in itself is not often the main goal, but rather a necessary step for data analysis.

\section{Finding principal components}
\label{develop--stats:ROOT:page--analysis/pca.adoc---finding-principal-components}

In PCA, \textbf{linear combinations} of the variables are used to find \textbf{principal components}
which \textbf{maximize} the variance captured.
Let the \(i\)'th principal component have linear combination

\begin{equation*}
Y_i = \vec{a}_i^T\vec{X} = a_{i1}X_1 + a_{i2}X_2 + \cdots + a_{ip}X_p
\end{equation*}

Then
\(Var(Y_i) = \vec{a}_i^T\mat{\Sigma}\vec{a}_i\)
and
\(Cov(Y_i, Y_j) = \vec{a}_i^T\mat{\Sigma}\vec{a}_k\).
We determine the \(\vec{a}_i\) as follows

\begin{description}
\item[First principal component] Maximize \(Var(\vec{a}_1^T\vec{X}) = \vec{a}_1^T\mat{\Sigma}\vec{a}_1\)
    subject to \(\vec{a}_1^T\vec{a}_1 = 1\)
\item[Second principal component] Maximize \(Var(\vec{a}_2^T\vec{X}) = \vec{a}_2^T\mat{\Sigma}\vec{a}_2\)
    subject to \(\vec{a}_2^T\vec{a}_2 = 1\) and \(\vec{a}_1^T\mat{\Sigma}\vec{a}_2 = 0\)
\item[i'th principal component] Maximize \(Var(\vec{a}_i^T\vec{X}) = \vec{a}_i^T\mat{\Sigma}\vec{a}_i\)
    subject to \(\vec{a}_i^T\vec{a}_i = 1\) and \(\vec{a}_i^T\mat{\Sigma}\vec{a}_k=0\)
    for \(k=1,\ldots i-1\).
\end{description}

That is, we find one principal component at a time
while ensuring

\begin{itemize}
\item Earlier principal components capture more variance than later ones.
\item The principal components are uncorrelated
\item The scale is preserved
\end{itemize}

The approach is very good for explaining why we choose the specific
components. However, it is very hard to implement/perform
as it consists of many constrained maximization problems.
Instead, we can use the following result

\begin{theorem}[{}]
Let \(\mat{\Sigma}\) be the covariance matrix associated with the data.
Let the normalized eigenpairs of \(\mat{\Sigma}\) be \((\lambda_1, \vec{e}_1), \ldots (\lambda_p, \vec{e}_p)\)
where \(\lambda_1 \geq \lambda_2 \geq \cdots \geq \lambda_p\).
Then, the \(i\)'th principal component has direction \(\vec{e}_i\)
and variance \(\lambda_i\).

\begin{admonition-note}[{}]
If any of the \(\lambda_i\) are equal, the order of the principal components is not unique.
\end{admonition-note}
\end{theorem}

The above allows us to compute the principal components in a simple manner.
Also, it allows us to easily analyze properties each principal component.
One important property is the amount of variability captured by each principal component.
From the above theorem, we see that the i'th principal component captures

\begin{equation*}
\frac{\lambda_i}{\lambda_1 + \lambda_2 + \cdots + \lambda_k} = \frac{\lambda_i}{\tr\mat{\Sigma}}
\end{equation*}

of the variance.

\section{Implementation in R}
\label{develop--stats:ROOT:page--analysis/pca.adoc---implementation-in-r}

\begin{listing}[{}]
# data with rows as observations and columns as variables
data <- ....

# perfom pca and scale the data
res <- prcomp(data, scale=TRUE)

# plot a scree plot
plot(res, type='l')

# calculate the proportion variance explained by each variable
prop_var_explained = res$sdevˆ2 / sum(res$sdevˆ2)
# calculate total proportion variance explained by the first i components
cumsum(prop_var_explained)

# produce a biplot
biplot(res)
\end{listing}

To choose the number of principal components to use the following methods

\begin{description}
\item[Scree plot] Plot the variance of each of the principal components.
    Choose the number at which there is an \textbf{elbow} in the plot. This occurs
    if there is no significant change between the two consecutive principal components
\item[Large Eigenvalues] If the data is not normalized,
    we can choose principal components which have eigenvalues greater than 1
\item[Large cumulative variance] Choose the number of principal components
    such that a large proportion of the variance is explained
\end{description}

\section{Biplots}
\label{develop--stats:ROOT:page--analysis/pca.adoc---biplots}

A biplot plots the variables in terms of the first two principal components.
To plot, we can plot the columns of \(\mat{E}^T\) (the inverse of the matrix with columns of principal components)
as well as \(\mat{E}^T(\vec{x}- \overbar{\vec{x}})\).

If the first two principal components explain a large proportion of the variance,
the biplot allows us to similarity between observations.
\end{document}
