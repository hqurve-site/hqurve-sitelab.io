\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Completely Random Design}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% life is a lie
\def\bigchi{\chi}
\DeclareMathOperator{\SST}{SST}
\DeclareMathOperator{\SSE}{SSE}
\DeclareMathOperator{\SSTr}{SSTr}
\DeclareMathOperator{\SSB}{SSB}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\MSTr}{MSTr}
\DeclareMathOperator{\MSB}{MSB}

\def\bm#1{\mathbf #1}

% Title omitted
A completely random design consists of \(k\) \emph{treatments} each
of which are allocated \(n\) random \emph{observations} of one
factor. That is, given \(N = nk\) possible situations, each
treatment is randomly allocated \(n\) of them. This design
allows us to utilize statistical theory established prior since the
randomization ensures that there is no systematic or patterned variation
in the observations.

This design seeks to test whether all of the \(k\) treatment
means \(\mu_i\) are the same.

\section{Note}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---note}

the approach in this section uses the \emph{fixed effects model} instead of
the \emph{random effects model}. That is, the \(k\) treatments are
selected beforehand instead of choosing from a larger pool. This choice
would limit us to only make conclusions about the selected
\(k\) treatments and not the larger pool of treatments as a
whole.

\section{Approach and neccessity of Analysis of Variance (ANOVA)}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---approach-and-neccessity-of-analysis-of-variance-anova}

As would be emphasized later, this section seeks to produce a test for
the following hypothesis

H\textsubscript{0}: \(\mu_1 = \mu_2 \cdots = \mu_k\).
H\textsubscript{1}: not all of the \(\mu_i\) are equal

The simple approach would be to simply test if each pair
\((\mu_i, \mu_j)\) are equal. Suppose that each of these are
conducted at a \(\alpha\) level of significance. Then,
assuming each pair is independent

\begin{equation*}
P(\text{type I error})
    = 1- (1-\alpha)^{\binom{k}{2}}
\end{equation*}

Notice that \((1-\alpha)^{\binom{k}{2}}\) tends to
\(0\) rather quickly by the nature of
\(\binom{k}{2}\) and hence the probability of a type I error
increase. There are two ways to counteract this,

\begin{enumerate}[label=\arabic*)]
\item Accept the high probability of committing a type I error
\item Reduce the probability of committing type I error for each pairwise
    test which in return causes type II errors to be more likely.
\end{enumerate}

Both cases are undesirable.

Instead, we use a wholistic approach to testing if all of the
\(\mu_i\) are equal. Notice that if each of the
\(\mu_i\) are equal, the variation of the entire
\(N=nk\) observations should mirror the variation within each
of the \(k\) treatments. The analysis of variance (ANOVA),
does this but instead uses a more rigorous approach which will be
discussed later.

\section{The models}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---the-models}

We will denote the \(j\)’th observation for the
\(i\)’th treatment as \(y_{ij}\). Then, we can write
each \(y_{ij}\) from the \(i\)th treatment as

\begin{equation*}
y_{ij} = \mu_i + \varepsilon_{ij}
\end{equation*}

where \(\varepsilon_{ij}\) is due to random error. This model
is called the \emph{means model}. We assume that the
\(\varepsilon_{ij}\) are independent and identically
distributed with \(\varepsilon_{ij} \sim N(0, \sigma^2)\).

Additionally, we denote \(\mu\) to be the mean of the
treatment means. That is
\(\mu = \frac{1}{k}\sum_{i=1}^k \mu_i\). Then, we can denote
\(\alpha_i = \mu - \mu_i\) and rewrite \(y_{ij}\) as

\begin{equation*}
y_{ij} = \mu +\alpha_i + \varepsilon_{ij}
\end{equation*}

This model is called the \emph{means model} and would be the basis of our
investigation. Furthermore, we can obtain that each of the
\(y_{ij}\) are independent and

\begin{equation*}
y_{ij} \sim N(\mu + \alpha_i, \sigma^2)
\end{equation*}

with each of the observations within one treatment being iid.

At this point, our hypotheses can be simplified as

H\textsubscript{0}: \(\alpha_i = 0\ \forall i = 1\ldots k\).
H\textsubscript{1}: at least one \(\alpha_i \neq 0\)

\subsection{Note}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---note-2}

Both of these models are examples of linear models.

\subsection{Some common notations}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---some-common-notations}

Due to the large use of indices, the following summation conventions are
used

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(\displaystyle y_{i.} = \sum_{j=1}^n y_{ij}\).} & {\(\displaystyle \overbar{y}_{i.} = \frac{1}{n}\sum_{j=1}^n y_{ij}\).} \\
\hline
{\(\displaystyle y_{..} = \sum_{i=1}^k\sum_{j=1}^n y_{ij}\).} & {\(\displaystyle \overbar{y}_{..} = \frac{1}{N}\sum_{i=1}^k\sum_{j=1}^n y_{ij}\).} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\section{Variations}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---variations}

We define the following

\begin{itemize}
\item \(\displaystyle \text{Total variation} = \SST= \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2\)
\item \(\displaystyle \text{Random variation} = \SSE= \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2\)
\item \(\displaystyle \text{Variation due to treaments} = \SSTr= n\sum_{i=1}^k (\overbar{y}_{i.} - \overbar{y}_{..})^2\)
\end{itemize}

\subsection{Important result}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---important-result}

We have the following result

\begin{equation*}
\SST= \SSTr+ \SSE
\end{equation*}

\begin{example}[{Proof}]
We will prove the result for each treatment then sum all of the
treatments. That is, we first need to prove

\begin{equation*}
\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2
        = n(\overbar{y}_{i.} - \overbar{y}_{..})^2 + \sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
\end{equation*}

Note that this result is quite similar to that which relates MSE as the
sum of variance and bias squared. Regardless, we will prove this result
directly.

\begin{equation*}
\begin{aligned}
        &\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2 \\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.} + \overbar{y}_{i.} - \overbar{y}_{..})^2 \\
        &=\sum_{j=1}^n \left[(y_{ij} - \overbar{y}_{i.})^2
            + (\overbar{y}_{i.} - \overbar{y}_{..})^2
            + 2(y_{ij} - \overbar{y}_{i.})(\overbar{y}_{i.} - \overbar{y}_{..})\right]\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
            + \sum_{j=1}^n(\overbar{y}_{i.} - \overbar{y}_{..})^2
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^n(y_{ij} - \overbar{y}_{i.})\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\left[\sum_{j=1}^ny_{ij} - n\overbar{y}_{i.}\right]\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\left[y_{i.} - n\overbar{y}_{i.}\right]\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2
    \end{aligned}
\end{equation*}

Now, if we sum both sides of the equation for \(i=1\ldots k\),
we get the desired result of

\begin{equation*}
\sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2
        =
            n\sum_{i=1}^k (\overbar{y}_{i.} - \overbar{y}_{..})^2
            + \sum_{i=1}^k \sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
\end{equation*}

 ◻
\end{example}

\subsection{Distributions}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---distributions}

Given our means model and the null hypothesis, we have the following
results

\begin{equation*}
\frac{\SSTr}{\sigma^2} \sim \bigchi_{(k-1)}^2
    \quad\text{and}\quad
    \frac{\SSE}{\sigma^2} \sim \bigchi_{N-k}^2
\end{equation*}

its important to note that \(N-k = k(n-1)\) which gives a hint
as to how prove the result.

\begin{example}[{Proof}]
Each result will be proven separately.

==== Result (i)

Now, by our model, each of the \(y_{ij}\) are independent and
hence, their treatment means are independent. Therefore, each of their
treatment means are independent with distribution
\(N\left(\mu + \alpha_i, \tfrac{\sigma^2}{n}\right)\). Now,
under \(H_0\), we have \(\alpha_i = 0\) and hence
each of the
\(\overbar{y}_{i.}\)
are iid with distribution
\(N\left(\mu, \tfrac{\sigma^2}{n}\right)\). Therefore, the
sample variance of the
\(\overbar{y}_{i.}\)
are distributed such that

\begin{equation*}
\frac{(k-1) \frac{1}{k-1}\sum_{i=1}^k(\overbar{y}_{i.} - \overbar{y}_{..})^2}{\sigma^2/n}
        = \frac{n\sum_{i=1}^k(\overbar{y}_{i.} - \overbar{y}_{..})^2}{\sigma^2}
        = \frac{\SSTr}{\sigma^2} \sim \bigchi_{(k-1)}
\end{equation*}

==== Result (ii)

Let \(s_i\) be the sample variance for the \(i\)’th
treatment. Then
\(\tfrac{(n-1)s_i^2}{\sigma^2} \sim \bigchi_{(n-1)}\)
for each treatment. Now, since the \(y_{ij}\) are independent,
their sample variances are also independent. Therefore,

\begin{equation*}
\sum_{i=1}^k \frac{(n-1)s_i^2}{\sigma^2}
        = \sum_{i=1}^k \frac{\sum_{j=1}^n(y_{ij} - \overbar{y}_{i.})^2}{\sigma^2}
        = \frac{\SSE}{\sigma^2}
        \sim \bigchi_{k(n-1)} = \bigchi_{(N-k)}
\end{equation*}

 ◻
\end{example}

\section{The test}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---the-test}

In order to determine whether each of the \(\alpha_i\) are
zero, we consider the distribution of

\begin{equation*}
F = \frac{\left.\frac{\SSTr}{\sigma^2}\right/(k-1)}{\left.\frac{\SSE}{\sigma^2}\right/(N-k)} \sim F_{(k-1), (N-k)}
\end{equation*}

and by letting
\(\tfrac{\SSTr}{k-1} = \MSTr\)
and
\(\tfrac{\SSE}{N-k} = \MSE\)
we get that

\begin{equation*}
F = \frac{\MSTr}{\MSE} \sim F_{(k-1), (N-k)}
\end{equation*}

We then produce the following table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Source of variation} & {Sum of Squares (SS)} & {Degrees of freedom (df)} & {Mean square (MS)} & {F-statistic} \\
\hline[\thicktableline]
{Treatments} & {\(\SSTr\)} & {\(k-1\)} & {\(\MSTr= \frac{\SSTr}{k-1}\)} & {\(F = \frac{\MSTr}{\MSE}\)} \\
\hline
{Random variation} & {\(\SSE\)} & {\(N-k\)} & {\(\MSE= \frac{\SSE}{N-k}\)} & {} \\
\hline
{Total} & {\(\SST\)} & {\(N-1\)} & {} & {} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

At which point we perform a right tailed test on the F-statistic. We use
the following R code to produce the test.

\begin{listing}[{}]
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the independent and dependent variables
# each row contains a pair (independent, dependent)

is.factor(data$independent) #ensure that independent data is a a factor
data$independent <- as.factor(data$independent) #convert if necessary

result <- lm(dependent ~ independent, data=data)
anova(result)
\end{listing}

\subsection{Tukey’s HSD Test}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---tukeys-hsd-test}

The ANOVA test is a omnibus test, in that it only tells us whether our
results are significant overall and not exactly where the differences
lie. Tukey’s HSD post-hoc analysis produces adjusted probabilities for
each pair of treatments which allow us to determine which treatments may
have caused the ANOVA test to produce significant findings.

\begin{listing}[{}]
result <- lm(dependent ~ independent, data=data)

result2 <- aov(result)

summary(result2) #produces similar information to anova(result)

TukeyHSD(result2)
\end{listing}

\subsubsection{Note}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---note-3}

this test can only be conducted if the ANOVA test produces significant
findings.

\section{Model Adequacy Checking}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---model-adequacy-checking}

While performing the ANOVA test, there were several assumptions made.
This section shows methods which can be used to test whether the data
collected satisfies those assumptions and hence also satisfies the
model.

\subsection{Normality}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---normality}

The simplest method is to use a histogram as follows

\begin{listing}[{}]
hist(result$res, main="Histogram of residuals", xlab="residuals")
\end{listing}

However, this is can only be used if the sample size is large. As an
alternative, we can use a Q-Q plot to see if the data matches what would
theoretically be obtained if the data was exactly normal.

\begin{listing}[{}]
# normality probability plot
qqnorm(result$res)
# 45-degree line (y=x)
qqline(result$res)
\end{listing}

\subsection{Independence}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---independence}

This can only be validated during the collection phase.

\subsection{Constant variance}
\label{develop--math2275:ROOT:page--block-designs/completely-random-design.adoc---constant-variance}

We can conduct Bartlett’s test with the null hypothesis begin that the
treatment variances are equal and alternative hypothesis being that at
least one is different using

\begin{listing}[{}]
bartlett.test(dependent ~ independent, data=data)
\end{listing}

Alternatively, we can use box plots, however this only works if the
number of observations \(n\) is not small

\begin{listing}[{}]
boxplot(dependent ~ independent, xlab="independent", ylab="dependent", data=data)
\end{listing}

Finally, we can use a scatter plot to plot the residuals against the
fitted values. If the data is randomly distributed, the plot should be a
random cloud and should not show any widening or shrinking.

\begin{listing}[{}]
plot(result$fit, result$res)
\end{listing}
\end{document}
