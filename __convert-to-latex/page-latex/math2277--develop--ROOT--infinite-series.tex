\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Infinite Series}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(\{a_n\}\) be a sequence of real numbers. Then, we define
the sequence \(\{s_n\}\) of \emph{parital sums} by

\begin{equation*}
s_n = \sum_{k=1}^n a_k
\end{equation*}

Then, we say that the \emph{infinite series} \(\sum_{n=1}^\infty\)
is convergent if \(\{s_n\}\) is convergent and we define

\begin{equation*}
s = \sum_{n=1}^\infty :=\lim_{n\to \infty} \sum_{k=1}^n a_k = \lim_{n\to \infty} s_n
\end{equation*}

where \(s\) is the \emph{sum of the series}. If
\(\sum_{n=1}^\infty a_n\) is not convergent, we say that the
series is \emph{divergent}.

\section{Note on starting index}
\label{develop--math2277:ROOT:page--infinite-series.adoc---note-on-starting-index}

Although we initially defined the infinite series as starting from index
\(n=1\), we can start at any integer as it would only fintely
affect the series. That is, if a series is convergent, it remains
convergent if finitely many terms are added or removed.

\section{Linearity}
\label{develop--math2277:ROOT:page--infinite-series.adoc---linearity}

The infinite sum is a linear operator, that is if
\(\sum_{n=1}^\infty a_n = a\) and
\(\sum_{n=1}^\infty b_n = b\) then

\begin{equation*}
\sum_{n=1}^\infty (a_n + b_n) = a + b
\end{equation*}

and

\begin{equation*}
\sum_{n=1}^\infty ka_n = ka
\end{equation*}

where \(k \in \mathbb{R}\).

\section{Cauchy criterion for convergence}
\label{develop--math2277:ROOT:page--infinite-series.adoc---cauchy-criterion-for-convergence}

Let \(\{a_n\}\) be a sequence of real numbers and consider the
sequence of partial sums \(\{s_n\}\). Then, recall that a real
sequence is convergent iff it is cauchy convergent. Then,
\(\sum_{n=1}^\infty a_n\) converges iff

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall m, n \in \mathbb{N}:
    n > m > N \implies |s_n - s_m| = |a_{m+1} + \cdots + a_{n}| < \varepsilon
\end{equation*}

\section{Absolute Convergence}
\label{develop--math2277:ROOT:page--infinite-series.adoc---absolute-convergence}

Let \(\{a_n\}\) be a sequence of real numbers. Then, the
series \(\sum_{n=1}^\infty a_n\) is said to be \emph{absolutely
convergent} if the series \(\sum_{n=1}^\infty |a_n|\)
converges. Then, by the triangle inequality,

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall m, n \in \mathbb{N}:
    n > m > N \implies |a_{m+1} + \cdots + a_n| \leq |a_{m+1}| +\cdots + |a_{n}| < \varepsilon
\end{equation*}

we see that absolute convergence implies convergence; that is, absolute
convergence is a special kind of convergence. Additionally, we call
convergent series which are not absolutely convergent, \emph{conditionally
convergent}.

\subsection{Side note:}
\label{develop--math2277:ROOT:page--infinite-series.adoc---side-note}

I believe that the term 'conditional' is used\textsuperscript{[citation needed]} since
the value to which a series consisting of the same terms terms depends
on the arrangement of said terms. In fact by the Riemann series theorem,
the terms of a conditionally convergent sequence can be rearranged in
such a manner, to converge to any real number, positive or negative
infinity, or diverge. This is unlike absolutely convergent series which
converge to the same value regardless of the order of its terms. From
this, we see that an absolutely convergent sequence is simply the sum of
a set while a conditionally convergent sequence is dependent on its
sequence of partial sums.

\subsection{Side note 2:}
\label{develop--math2277:ROOT:page--infinite-series.adoc---side-note-2}

I just remembered how integrals (and integral transforms) are defined,
they require absolute convergence. Usually it is phrased as the integral
of the negatives and positives, both converge and the integral of the
function itself is simply the difference. In a similar manner, we could
consider an absolutely convergent series as the difference of the
series(es) of positive and negative terms provided that they both
converge.

\section{Tests for convergence/divergence}
\label{develop--math2277:ROOT:page--infinite-series.adoc---tests-for-convergencedivergence}

\subsection{Limit of terms is zero if convergent}
\label{develop--math2277:ROOT:page--infinite-series.adoc---limit-of-terms-is-zero-if-convergent}

Let \(\{a_n\}\) be a real sequence such that
\(\sum_{n=1}^\infty a_n\) converges. Then immediately, by the
cauchy criterion, \(\{a_n\}\) must tend to zero by taking
\(m = n-1\).

\subsection{Comparision test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---comparision-test}

Let \(\{a_n\}\) and \(\{b_n\}\) be two non-negative
sequences where \(\forall n \in \mathbb{N}: a_n \leq b_n\).
Then

\begin{itemize}
\item If \(\sum_{n=1}^\infty b_n\) is convergent, then
    \(\sum_{n=1}^\infty a_n\) is also convergent.
\item If \(\sum_{n=1}^\infty a_n\) is divergent, then
    \(\sum_{n=1}^\infty b_n\) is also divergent.
\end{itemize}

\begin{example}[{Proof}]
Firstly, note that the second statement is simply the contrapositive of
the first, so only the first statement will be proven.

Let \(\{s_n\}\) and \(\{t_n\}\) be the sequence of
partial sums of \(\{a_n\}\) and \(\{b_n\}\)
respectively. Then notice that both \(\{s_n\}\) and
\(\{t_n\}\) are monotonically increasing and
\(\forall n \in \mathbb{N}: s_n \leq t_n\). Then, if
\(\{t_n\}\) (or equivalently
\(\sum_{n=1}^\infty b_n\)) converges, then by the monotone
convergence theorem, there exists an upper bound \(M\) for
\(\{t_n\}\). This upper bound must also be an upper bound for
\(\{s_n\}\) and the by monotone convergence theorem we are
done.

 ◻
\end{example}

\subsection{Limit comparision test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---limit-comparision-test}

Let \(\{a_n\}\) and \(\{b_n\}\) be two positive
sequences such that \(\{a_n / b_n\}\) converges to a positive
number. Then, either both \(\sum_{n=1}^\infty a_n\) and
\(\sum_{n=1}^\infty b_n\) converge or both diverge.

\begin{example}[{Proof}]
Note that since \(\{a_n / b_n\}\) converges to a positive
value, the sequence of recipricals \(\{b_n / a_n\}\) also
converge to a positve value. Then, we only need to show that
\(\sum_{n=1}^\infty a_n\) converges implies that
\(\sum_{n=1}^\infty b_n\) also converges.

Consider arbitary \(\varepsilon > 0\) and let
\(\lim_{n\to \infty} \frac{b_n}{a_n} = L\). Then, consider a
yet to determined constants
\(\varepsilon_1, \varepsilon_2 > 0\) and note that

\begin{equation*}
\exists N_1 \in \mathbb{N}: \forall n \in \mathbb{N}:
        n > N_1
        \implies \frac{b_n}{a_n} \in N(L, \varepsilon_1)
        \implies b_n < a_n(L +\varepsilon_1)
\end{equation*}

Next, by the cauchy converge criterion,

\begin{equation*}
\exists N_2 \in \mathbb{N}: \forall m, n \in \mathbb{N}:
        n > m > N_2
        \implies \sum_{k=m}^n a_k < \varepsilon_2
\end{equation*}

Now, let \(N = \min(N_1, N_2)\) and we get that

\begin{equation*}
\forall m, n \in \mathbb{N}:
        n > m > N
        \implies \sum_{k=m}^n b_k < \sum_{k=n}^m a_k (L + \varepsilon_1)
        < (L + \varepsilon_1)\varepsilon_2
\end{equation*}

All that remains is to set \(\varepsilon_1\) and
\(\varepsilon_2\) such that
\((L +\varepsilon_1)\varepsilon_2 \leq \varepsilon\). Note
that this choice is arbitary but \(\varepsilon_1 = 1\) and
\(\varepsilon_2 = \frac{\varepsilon}{L + 1}\) would work.

 ◻
\end{example}

\subsubsection{Note 1:}
\label{develop--math2277:ROOT:page--infinite-series.adoc---note-1}

After doing some online research, I noted that I could have stopped
immeditately at \(b_n < a_n(L + \varepsilon_1)\) and applied
the comparision test.

\subsubsection{Note 2:}
\label{develop--math2277:ROOT:page--infinite-series.adoc---note-2}

I am yet to find another source for this, but the restrictions of this
test are too strict. All that is required is that
\(\{a_n/b_n\}\) is bounded between two positive numbers (for
all \(n\) greater than some \(N \in \mathbb{N}\),
there is no need for the the sequence to converge. Additionally, using
this version of the test, the statement is equivalent to normal
comparision test (albiet with finite constants which do not affect the
outcome).

\subsection{Ratio test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---ratio-test}

Let \(\{a_n\}\) be a sequence of non-zero terms and

\begin{equation*}
L = \lim_{n \to \infty} \left| \frac{a_{n+1}}{a_n} \right|
\end{equation*}

Then, if

\begin{itemize}
\item \(L < 1\), \(\sum_{n=1}^\infty a_n\) converges
    absolutely.
\item \(L > 1\), \(\sum_{n=1}^\infty a_n\) diverges.
\item \(L = 1\), the test gives no information about the
    convergence or divergence of \(\sum_{n=1}^\infty a_n\).
\end{itemize}

\begin{example}[{Proof}]
In a similar manner to note 2 of the limit comparision test, we will be
using bounds instead of limits. The wikipedia article states it in terms
of limit inferior and limit superior, but the methods used here are
equivalent. There is no need to find the actual infimum or supremum of
the sequence, but all that is required is the existence of bounds which
satisfy the requirements. That is, all that is required is to find a
\(N \in \mathbb{N}\) such that for all \(n > N\),
\(\left|\frac{a_{n+1}}{a_n}\right|\) lies above or below the
required boundary. That being said, in order to show that the equivalent
case of \(L=1\), the actual supremum/infimum must be used.

Consider the sequence \(\{|a_{n+1}/a_{n}|\}\) with limit
inferior \(r\) and limit supremum \(s\). Then

\begin{itemize}
\item \(s < 1\) (or equivalent when \(L < 1\)). Then,
    let \(v = \frac{1+s}{2} < 1\), and since
    \(\varepsilon = v - s > 0\),
    \(\exists N \in \mathbb{N}\) such that
    
    \begin{equation*}
    \forall n \in \mathbb{N}: n > N \implies \left|\frac{a_{n+1}}{a_n}\right| < v \implies |a_{n+1}| < v|a_n|
    \end{equation*}
    
    Now, by induction
    
    \begin{equation*}
    \forall n \in \mathbb{N}: n > N \implies |a_{n+1}| \leq v |a_{n+1 - 1}| \leq v^2 |a_{n+1 - 2}| \leq \cdots \leq v^{n-N}|a_{N+1}|
    \end{equation*}
    
    Note that I left in the case of equality for when \(n = N+1\).
    Now, consider the series,
    
    \begin{equation*}
    \begin{aligned}
                    \sum_{k=1}^\infty |a_k|
                    &= \sum_{k=1}^N |a_k| + \sum_{k=N+1}^\infty |a_{k}|
                  \\&= \sum_{k=1}^N |a_k| + \sum_{k=N}^\infty |a_{k+1}|
                  \\&\leq \sum_{k=1}^N |a_k| + \sum_{k=N}^\infty |a_{N+1}|v^{n-N}
                  \\&= \sum_{k=1}^N |a_k| + |a_{N+1}|\sum_{k=0}^\infty v^k
                \end{aligned}
    \end{equation*}
    
    Now, since \(v < 1\), the geometric series converges and by
    the comparision test, the original sequence is absolutely convergent.
\item \(r > 1\) (or equivalently when \(L > 1\)). Then,
    by letting \(u = \frac{r+1}{2} > 1\), since
    \(\varepsilon = r - u > 0\),
    \(\exists N \in \mathbb{N}\) such that
    
    \begin{equation*}
    \forall n \in \mathbb{N}: n > N \implies \left|\frac{a_{n+1}}{a_n}\right| > u \implies |a_{n+1}| > u|a_n|
    \end{equation*}
    
    and by a similar calculations, we would get that
    
    \begin{equation*}
    \forall n \in \mathbb{N}: n > N \implies |a_{n+1}| \geq u^{n-N} |a_{N+1}|
    \end{equation*}
    
    now since \(a_{N+1} \neq 0\) and \(u > 1\), it is
    clear that \(\lim_{n\to \infty}|a_{n+1}| = \infty\).
    Therefore, \(\lim_{n\to \infty} a_{n+1} \neq 0\) and in fact
    either tends to \(\pm \infty\) or doesnt exist. Then the
    series does not converge.
\item For the case when \(L = 1\), the equivalent cases are
    \(r = 1\) and \(s = 1\). I am yet to come up with
    examples which do not produce a limit, but only produce the relevant
    limit superior or limit inferior. Below are three examples,
    
    \begin{itemize}
    \item \(\sum_{n=1}^\infty 1\) diverges to infinity.
    \item \(\sum_{n=1}^\infty (-1)^n\) diverges.
    \item \(\sum_{n=1}^\infty \frac{(-1)^n}{n}\) converges
        conditionally
    \item \(\sum_{n=1}^\infty \frac{1}{n^2}\) converges absolutely.
        
        Note that in general, any rational function would produce a ratio limit
        of \(1\).
    \end{itemize}
\end{itemize}

 ◻
\end{example}

If not evident, the ratio test works by approximating the sequence
\(\{a_n\}\) by an exponential one \(\{\beta L^n\}\).

\subsection{n’th root test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---nth-root-test}

Let \(\{a_n\}\) be a real sequence and

\begin{equation*}
L = \lim_{n\to \infty} |a_n|^{\frac{1}{n}}
    \lim_{n\to \infty} \sqrt[n]{|a_n|}
\end{equation*}

Then if

\begin{itemize}
\item \(L < 1\), the series \(\sum_{n = 1}^\infty\)
    converges absolutely.
\item \(L > 1\), the series \(\sum_{n = 1}^\infty\)
    diverges.
\item \(L=1\), no information could be obtained about the series.
\end{itemize}

Furthermore, all conclusions yeilded by the ratio test are also yeilded
by the n’th root test (but not vice-versa).

\begin{example}[{Proof}]
Similar to the ratio test, the limit suprerior and limit inferior can be
used instead of just the limit. Let \(r\) and \(s\)
be the limit inferior and limit suprerior of
\(\{|a_n|^{\frac{1}{n}}|\}\) respectively. Then

\begin{itemize}
\item If \(s < 1\), let \(v = \frac{1+s}{2}\). Then,
    \(\varepsilon = v-s > 0\) and
    \(\exists N \in \mathbb{N}\) such that
    
    \begin{equation*}
    \forall n \in \mathbb{N}: n > N \implies |a_{n}|^{\frac{1}{n}} < v \implies |a_n| < v^n
    \end{equation*}
    
    Then, by the comparision test, \(\sum_{n=N+1}^\infty |a_n|\)
    is convergent. Hence, the original series is absolutely convergent.
\item If \(r > 1\), let \(u = \frac{1+s}{2}\). Then,
    \(\varepsilon = r-u > 0\) and
    \(\exists N \in \mathbb{N}\) such that
    
    \begin{equation*}
    \forall n \in \mathbb{N}: n > N \implies |a_{n}|^{\frac{1}{n}} > u \implies |a_n| > u^n
    \end{equation*}
    
    Then, the \(\lim_{n\to \infty} |a_n| = \infty\) and hence
    \(\{a_n\}\) does not converge to \(0\). Therefore,
    the oriiginal series is divergent
\item If \(L=1\). The same examples given in the ratio test can be
    used here.
\end{itemize}

To see why the ratio test is weaker, we first show that conclusions in
the ratio test imply conclusions for the n’th root test. Only the limit
superior would be shown but the same arguement can be used for the limit
inferior.

Let
\(L = \lim_{n\to \infty} \sup \left|\frac{a_{n+1}}{a_n}\right|\)
where \(L < 1\) and consider \(\varepsilon > 0\)
such that \(L + \varepsilon < 1\). Then from the proof for the
limit comparision test, \(\exists N \in \mathbb{N}\) such that

\begin{equation*}
\forall n \in \mathbb{N}: n > N \implies
        |a_{n+1}| < |a_{N+1}|(L +\varepsilon)^{N-n}
\end{equation*}

and by reindexing

\begin{equation*}
\forall k \in \mathbb{N}: |a_{k+N+1}| < |a_{N+1}|(L + \varepsilon)^k
        \implies |a_{k+N+1}|^{\frac{1}{k}} < |a_{N+1}|^{\frac{1}{k}}(L + \varepsilon)
\end{equation*}

Note that the limit of \(|a_{N+1}|^{\frac{1}{k}}\) is
\(1\) and hence the limit supremum is at most
\(L + \varepsilon\) which is less than \(1\). Hence,
the condition for the n’th root test is satisifed.

See example 2.10 in Lay’s book to see where the ratio test is
inconclusive while the root test works. Additionally, another example is
\(a_n = 3^{-n +(-1)^n}\) which was obtained from
\url{https://math.stackexchange.com/q/1708595}.

 ◻
\end{example}

\subsubsection{Remark}
\label{develop--math2277:ROOT:page--infinite-series.adoc---remark}

Theorem 2.8 of Lay’s book lays out a more general condition that in fact
only the limit suprerior needs to exceed \(1\) for the series
to diverge. I am not going to modify my proof here, see Lay’s book for
the statement and proof. Furthermore, the wikipedia article states that
if the limit suprerior decreases to \(1\), the series is also
divergent.

\subsection{Alternating series test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---alternating-series-test}

Let \(\{a_n\}\) be a non-negative sequence of montonically
decreasing terms which tend to \(0\). Then, the series

\begin{equation*}
\sum_{n=1}^\infty (-1)^n a_n
\end{equation*}

is convergent. Intuitively, this works since series always alternates
upwards and downwards getting closer and closer (due to the decrasing
nature of the terms). This together with the fact the terms tending to
\(0\) causes the difference in partial sums to tend to
\(0\) and also causes the series to converge. Note that this
is an outline of one of the actual proofs.

To see a complete proof, see the article at
\url{https://en.wikipedia.org/wiki/Alternating\_series\_test} which also gives a
counterexample to show the importance of montonic condition.

\subsection{Integral test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---integral-test}

Let \(\{a_n\}\) be a sequence where there exists a
non-negative (monotonically) decreasing function
\(f: \mathbb{R} \to \mathbb{R}\) such that
\(a_n = f(n)\). Then

\begin{equation*}
\sum_{n=1}^\infty a_n \text{ converges}
    \iff
    \int_1^\infty f(x) \ dx \text{ converges}
\end{equation*}

Note that the lower bounds of both operations need not be
\(1\).

\begin{example}[{Proof}]
Firstly, note that

\begin{equation*}
\forall n \in \mathbb{N}: f(n) \geq \int_n^{n+1} f(x) \ dx \geq f(n+1) \geq 0
\end{equation*}

Since \(n < x < n+1 \implies f(n) \geq f(x) \geq f(n+1)\) and
the preservation of inequalities using integrals. Next, notice that the
following relationship,

\begin{equation*}
\int_1^\infty f(x) \ dx = \sum_{n=1}^\infty \int_n^{n+1} f(x) \ dx
\end{equation*}

which is the sum of the sequence
\(\left\{b_n =\int_n^{n+1} f(x) \ dx\right\}\). Then, we will
show both directions seperately.

\begin{itemize}
\item if \(\sum_{n=1}^\infty f(n)\) is convergent, then by the
    comparision test, the sum of \(\left\{b_n\right\}\) converges.
    That is the integral converges.
\item if the intergral converges, the sume of \(\{b_n\}\)
    converges and by the comparision test, the sum of
    \(\{f(n+1)\}\) converges. Therefore, since this series only
    differs by one term, the sum of \(\{f(n)\}\) also converges.
\end{itemize}

From the above inequalities, it is possible to base bounds on both the
integral and summation.

 ◻
\end{example}

\section{Some common series}
\label{develop--math2277:ROOT:page--infinite-series.adoc---some-common-series}

\subsection{Harmonic series}
\label{develop--math2277:ROOT:page--infinite-series.adoc---harmonic-series}

The \emph{harmonic series} is defined by

\begin{equation*}
\sum_{n=1}^\infty \frac1{n}
\end{equation*}

is divergent. This can be shown using the cauchy onvergence criterion
and partial sums \(S_{n}\) and \(S_{2n}\) which will
have a difference greater than
\(n\frac{1}{2n} = \frac{1}{2}\).

\subsection{p-series}
\label{develop--math2277:ROOT:page--infinite-series.adoc---p-series}

A p-series is defined by

\begin{equation*}
\sum_{n=1}^\infty \frac{1}{n^p}
\end{equation*}

which converges iff \(p > 1\) where
\(p \in \mathbb{R}\). This can be easily shown using the
intergal test for when \(p \neq 1\) and the harmonic series
for when \(p=1\).

\subsection{Geometric series}
\label{develop--math2277:ROOT:page--infinite-series.adoc---geometric-series}

A geometric series is one of the form

\begin{equation*}
\sum_{n=1}^\infty ar^n
\end{equation*}

which converges iff \(|r| < 1\) where
\(a, r \in \mathbb{R}\). This can be easily shown by writing
the partial sum as \(\frac{a(r^{n+1} - 1)}{r-1}\) when
\(|r| \neq 1\).

\section{Power series}
\label{develop--math2277:ROOT:page--infinite-series.adoc---power-series}

Let \(\{a_n\}\) be a sequence of real numbers and
\(x_0 \in \mathbb{R}\). The series

\begin{equation*}
\sum_{n=0}^\infty a_n(x-x_0)^n
\end{equation*}

is called a \emph{power series} and \(a_n\) is alled the
\(n\)\textsuperscript{th} \emph{coefficient} of the series.

\subsection{Radius of convergence using ratio test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---radius-of-convergence-using-ratio-test}

Let \(\{a_n\}\) be a sequence of real numbers and
\(x_0 \in \mathbb{R}\), by the ratio test, the series
converges absolutely if

\begin{equation*}
\lim_{n\to \infty} \left|\frac{a_{n+1}(x-x_0)^{n+1}}{a_n(x-x^0)^n}\right|
    = |x-x_0|\lim_{n\to \infty} \left|\frac{a_{n+1}}{a_n}\right|
    < 1
    \iff
    R :=\lim_{n\to \infty}\left|\frac{a_n}{a_{n+1}}\right| > |x-x_0|
\end{equation*}

Note that this gives no information about the convergence at the
endpoints. Also, we have two cases to consider, if
\(R = \infty\), this is because the original limit tends to
\(0\), hence the power series is absolutely convergent
everywhere. On the otherhand, if \(R = 0\), this is because
the original limit diverged, hence the power series is only convergent
at \(x=x_0\) (trivially) and its value is equal to
\(a_0\).

\subsection{Radius of convergence using n’th root test}
\label{develop--math2277:ROOT:page--infinite-series.adoc---radius-of-convergence-using-nth-root-test}

Let \(\{a_n\}\) be a sequence of real numbers and
\(x_0 \in \mathbb{R}\), by the ratio test, the series
converges absolutely if

\begin{equation*}
\lim_{n\to \infty} \sqrt[n]{|a_n(x-x_0)^n|}
    = |x-x_0|\lim_{n\to \infty} \sqrt[n]{|a_n|} < 1
    \iff
    R :=\lim_{n\to \infty} \sqrt[n]{1/|a_n|} > |x - x_0|
\end{equation*}

and analogous considerations (to those using the ratio test) are given
for the values of \(R\). Additionally, recall that n’th root
test gives more information than the ratio test, hence in general, the
radius of convergence using the n’th root test would be wider.
\end{document}
