\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Differentiation}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
For a function of two or more variables, the variable must be specified
for differentiation of the function. This leads to the concept of
partial derivatives.

\section{Partial derivative}
\label{develop--math2270:ROOT:page--differentiation.adoc---partial-derivative}

The \emph{partial derivative} of \(f(x, y)\) with respect to
\(x\) is

\begin{equation*}
\frac{\partial f}{\partial x} = D_x f = f_x(x, y) = \lim_{\Delta x\rightarrow 0} \frac{f(x + \Delta x, y) - f(x, y)}{\Delta x}
\end{equation*}

Things to notice

\begin{itemize}
\item The partial derivative of \(f(x, y)\) with respect to
    \(y\) is defined similarly
\item If this limit doesn’t exist, we say that the partial derivative does
    not exist
\item The partial derivative for functions of more than two variables are
    defined similarly
\item This formula is very similar to the case for only one variable
\item The partial derivative of \(f(x, y)\) at the point
    \((x_0, y_0)\) is denoted
    \(\left. \frac{\partial f}{\partial x} \right|_{(x_0, y_0)}\)
    or \(f_x(x_0, y_0)\)
\end{itemize}

\section{Differential}
\label{develop--math2270:ROOT:page--differentiation.adoc---differential}

The \emph{increment} of \(f(x, y)\) at \((x_0, y_0)\)
denoted by \(\Delta f(x_0, y_0)\) is defined as

\begin{equation*}
\Delta f(x_0, y_0) = f(x_0, \Delta x, y_0, \Delta y) - f(x_0, y_0)
\end{equation*}

Furthermore, \(f\) is \emph{differentiable} if and only if the
increment of \(f\) can be written as

\begin{equation*}
\Delta f(x_0, y_0) =
    \left( \frac{\partial f}{\partial x} (x_0, y_0) \right)\Delta x
    + \left( \frac{\partial f}{\partial y} (x_0, y_0) \right)\Delta y
    +  \Delta x\, \epsilon_1 +  \Delta y\, \epsilon_2
\end{equation*}

where \(\epsilon_1\) and \(\epsilon_2\) are both
functions of \(\Delta x\) and \(\Delta y\) such that

\begin{equation*}
\lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_1(\Delta x, \Delta y)
    = 0 =
    \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_2(\Delta x, \Delta y)
\end{equation*}

Notice that the existence of the partial derivatives of \(f\)
is not sufficient for \(f\) to be differentiable. Therefore,
more work must be done. We will now show ways to prove and disprove that
a function is differentiable.

The above is sometimes called the fundamental increment lemma.

\begin{theorem}[{}]
If \(f\) is differentiable then \(f\) is continuous.

We will show that this is a sufficient condition however not necessary.
That is \(f\) cannot be proven to be differentiable just by
showing that it is continuous.

\begin{proof}[{}]
This proof will be for two variables however, a similar
argument works in general.

Since \(f\) is differentiable, there exists functions
\(\epsilon_1, \epsilon_2\) of \(\Delta x\) and
\(\Delta y\) such that

\begin{equation*}
\Delta f(x_0, y_0) =
        \left( \frac{\partial f}{\partial x} (x_0, y_0) \right)\Delta x
        + \left( \frac{\partial f}{\partial y} (x_0, y_0) \right)\Delta y
        +  \Delta x\, \epsilon_1 +  \Delta y\, \epsilon_2
\end{equation*}

and
\(\lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_1(\Delta x, \Delta y)
    = 0 =
    \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_2(\Delta x, \Delta y)\).

Therefore,

\begin{equation*}
\lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} f(x_0 + \Delta x, y_0 + \Delta y) - f(x_0, y_0)
        =
        \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \Delta f(x_0, y_0)
        = 0
\end{equation*}

and by defining \(x = x_0 + \Delta x\) and
\(y = y_0 + \Delta y\) we note that as
\((\Delta x, \Delta y)\rightarrow (0, 0)\),
\((x, y)\rightarrow (x_0, y_0)\). Therefore,

\begin{equation*}
\lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} f(x_0 + \Delta x, y_0+ \Delta y) - f(x_0, y_0) =
        \lim_{(x, y)\rightarrow (x_0, y_0)} f(x, y) - f(x_0, y_0) = 0
\end{equation*}

Hence, the result follows.
\end{proof}
\end{theorem}

This theorem can be used to show that a function is not differentiable
through the contrapositive.

\begin{theorem}[{}]
If all the partial derivatives of \(f\) are continuous then \(f\) is continuously differentiable

\begin{proof}[{}]
Let

\begin{equation*}
\begin{aligned}
\epsilon_1(\Delta x, \Delta y) &= \frac{f(x_0 + \Delta x, y_0 + \Delta y) - f(x_0, y_0 + \Delta y)}{\Delta x} - f_x(x_0, y_0)
\\
\epsilon_2(\Delta x, \Delta y) &= \frac{f(x_0, y_0 + \Delta y) - f(x_0, y_0)}{\Delta y} - f_y(x_0, y_0)
\end{aligned}
\end{equation*}

with \(\epsilon_1(0, \Delta y) = 0\) and \(\epsilon_2(\Delta x, 0) = 0\).
Then

\begin{equation*}
\lim_{(\Delta x, \Delta y) \to (0,0)} \epsilon_1(\Delta x, \Delta y) =
\lim_{(\Delta x, \Delta y) \to (0,0)} \epsilon_2(\Delta x, \Delta y)
= 0
\end{equation*}

Since for any \(\varepsilon > 0\), there exists \(\delta > 0\) such that
\(|\Delta x| < \delta\) and \(|\Delta y| < \delta\) implies that

\begin{equation*}
\begin{aligned}
&\left|\frac{f(x_0 + \Delta x, y_0 + \Delta y) - f(x_0, y_0 + \Delta y)}{\Delta x} - f_x(x_0, y_0 + \Delta y)\right| < \frac{\varepsilon}{2}
\quad\text{by existence of partial derivative }f_x(x_0, y_0 + \Delta y)\\
&\left|f_x(x_0, y_0 + \Delta y) - f_x(x_0, y_0)\right| < \frac{\varepsilon}{2}
\quad\text{by continuity of partial derivative }f_x \text{ at }(x_0, y_0)\\
&\left|\frac{f(x_0, y_0 + \Delta y) - f(x_0, y_0)}{\Delta y} - f_y(x_0, y_0)\right| < \frac{\varepsilon}{2}
\quad\text{by existence of partial derivative } f_y(x_0, y_0)\\
\end{aligned}
\end{equation*}

hence by triangle inequality we get the desired limits of \(\epsilon_1\) and \(\epsilon_2\).

Now, notice that

\begin{equation*}
\begin{aligned}
f_x(x_0, y_0)\Delta x + \epsilon_1(\Delta x, \Delta y) \Delta x
+ f_y(x_0, y_0)\Delta y + \epsilon_1(\Delta x, \Delta y) \Delta y
= f(x_0 + \Delta x, y_0 + \Delta y) - f(x_0, y_0)
\end{aligned}
\end{equation*}

as desired
\end{proof}
\end{theorem}

\begin{admonition-note}[{}]
This continuity need only persist on a neighbourhood about \((x_0, y_0)\).
\end{admonition-note}

\subsection{Total differential}
\label{develop--math2270:ROOT:page--differentiation.adoc---total-differential}

If \(f\) is differentiable at \((x, y)\), then the
total differential of \(f\), denoted \(df\), is
defined as

\begin{equation*}
df = \frac{\partial f}{\partial x} dx + \frac{\partial f}{\partial y} dy
\end{equation*}

Note that this formula arises from the fact that both
\(\epsilon_1\) and \(\epsilon_2\) tend to
\(0\) as \((\Delta x, \Delta y)\rightarrow (0, 0)\)
Also, note the general results

\begin{itemize}
\item If \(f(x_1, x_2, \ldots, x_n)\) is constant, then the total
    differential \(df=0\).
\item The expression \(P(x, y) dx + Q(x, y) dy\) is the
    differential of \(f\) iff \(P_y = P_x\), in which
    case, \(P(x, y) dx + Q(x, y) dy\) is referred to as an exact
    differential
\end{itemize}

\subsection{General form in terms of vectors}
\label{develop--math2270:ROOT:page--differentiation.adoc---general-form-in-terms-of-vectors}

A function \(f: D\rightarrow \mathbb{R}\), where
\(D \subseteq \mathbb{R}^n\), is said to be differentiable at
the point \(a \in D\) if there exists a linear function (ie a
matrix) and a function \(\Phi\) from a deleted neighbourhood
of the origin in \(\mathbb{R}^n\) to
\(\mathbb{R}^n\) such that

\begin{enumerate}[label=\arabic*)]
\item \(\lim_{\bm{h}\rightarrow\bm{0}} \Phi(\bm{h}) = \bm{0}\)
\item \(f(\bm{a} + \bm{h}) = f(\bm{a}) + M(\bm{h}) + \Phi(\bm{h})\cdot\bm{h}\)
    for all non-zero vectors \(\bm{h}\) sufficiently close to
    \(\bm{0} \in \mathbb{R}^n\).
\end{enumerate}

When this is the case, the linear function \(M\) is called the
derivative of \(f\) at \(\bm{a}\). Note
\(M\) is total differential.

\section{Chain Rule}
\label{develop--math2270:ROOT:page--differentiation.adoc---chain-rule}

If \(u\) is a differentiable function of \(x\) and
\(y\) defined by \(u = f(x, y)\) and
\(x=F(r, s)\) and \(y = G(r, s)\) and

\begin{equation*}
\frac{\partial x}{\partial r}, \quad \frac{\partial x}{\partial s}, \quad \frac{\partial y}{\partial r}, \quad \frac{\partial y}{\partial s}
\end{equation*}

all exist, then \(u\) is a function of \(r\) and
\(s\) and the following holds

\begin{equation*}
\frac{\partial u}{\partial r} = \frac{\partial u}{\partial x}\frac{\partial x}{\partial r} + \frac{\partial u}{\partial y}\frac{\partial y}{\partial r}
\end{equation*}

\begin{equation*}
\frac{\partial u}{\partial s} = \frac{\partial u}{\partial x}\frac{\partial x}{\partial s} + \frac{\partial u}{\partial y}\frac{\partial y}{\partial s}
\end{equation*}

\subsection{Generalization}
\label{develop--math2270:ROOT:page--differentiation.adoc---generalization}

Given that \(u\) is a differentiable function of the
\(n\) variables \(x_1, x_2, \ldots, x_n\) and that
each of these variables is a function of the \(m\) variables
\(y_1, y_2, \ldots, y_m\) and that each of the partial
derivatives \(\frac{\partial x_i}{\partial y_j}\) exist for
\(i = 1, 2, \ldots, n\) and \(j = 1, 2, \ldots, m\).
Then \(u\) is a function of
\(y_1, y_2, \ldots, y_m\) and

\begin{equation*}
\frac{\partial u}{\partial y_j} = \sum_{i=1}^n \frac{\partial u}{\partial x_i}\frac{\partial x_i}{\partial y_j}
\end{equation*}

for all \(j = 1, 2, \ldots, m\).

\section{Higher order partial derivatives}
\label{develop--math2270:ROOT:page--differentiation.adoc---higher-order-partial-derivatives}

Higher order derivatives are obtained just by repeatedly differentiating
lower order ones. That is

\begin{equation*}
D_{xy} f = D_y(D_x f) = f_{xy} = \frac{\partial^2 f}{\partial y \partial x} = \frac{\partial }{\partial y}\left(\frac{\partial f}{\partial x}\right)
\end{equation*}

which can simply be computed by using the previously established
definitions.

For example

\begin{equation*}
f_{xy}(x_0, y_0) = \lim_{\Delta y \rightarrow 0} \frac{f_x(x_0, y_0 + \Delta y) - f_x(x_0, y_0)}{\Delta y}
\end{equation*}

\begin{theorem}[{Clairaut’s Theorem}]
In general \(f_{xy} \neq f_{yx}\) however, if both are
continuous at a point \((x_0, y_0)\) then
\(f_{xy}(x_0, y_0) = f_{yx}(x_0, y_0)\).
\end{theorem}

\section{Implicit function theorem}
\label{develop--math2270:ROOT:page--differentiation.adoc---implicit-function-theorem}

If \(F(x, y)\) is a continuously differentiable function
defined on a disk containing \((x_0, y_0)\) where
\(F(x_0, y_0)\) = 0 and \(F_y(x_0, y_0) \neq 0\),
then there exists an open set \(U \subset \mathbb{R}^2\)
containing \((x_0, y_0)\) such that there is a unique
continuously differentiable function
\(g: U \rightarrow \mathbb{R}\) where
\(F(x, g(x)) = 0\) and who’s derivative is given by

\begin{equation*}
\frac{d g}{dx} = - \frac{F_x}{F_y}
\end{equation*}

Note:

\begin{itemize}
\item We usually say that \(y\) is a function of \(x\)
    instead of there exists a function \(g\).
\item We get the above formula by applying the chain rule to
    \(F(x, y) = 0\).
\end{itemize}

\subsection{More than two variables}
\label{develop--math2270:ROOT:page--differentiation.adoc---more-than-two-variables}

Let \(F: \mathbb{R}^{n+1} \rightarrow \mathbb{R}\) be a
continuously differentiable function with coordinates
\((\bm{x}, y) \in \mathbb{R}^n \times \mathbb{R}\). Then for
any fixed point \((\bm{x_0}, y)\) with
\(F(\bm{x}, y) =0\) and \(F_y(\bm{x}, y) \neq 0\)
there exists an open set \(U \subset \mathbb{R}^{n+1}\),
containing \((\bm{x_0}, y)\), such that there exists a unique
function \(g: U \rightarrow \mathbb{R}\) where
\(F(\bm{x}, g(\bm{x})) = 0\) and whos partial derivatives are
given by

\begin{equation*}
\frac{d g}{dx_i} = - \left[\frac{\partial F}{\partial y}\right]^{-1} \frac{\partial F}{\partial x_i} = - \frac{F_{x_i}}{F_y}
\end{equation*}

Note again:

\begin{itemize}
\item We usually say that \(y\) is a function of
    \(\bm{x}\) instead of there exists a function \(g\).
\item We get the above formula by applying the chain rule to
    \(F(\bm{x}, y) = 0\) (differentiating wrt \(x_i\)).
\end{itemize}
\end{document}
