\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Primitive Roots}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

{} \def\mod{\mathrm{mod}\ } \DeclareMathOperator\ord{ord}
% Title omitted
Let \(n\) be an integer and consider \(U_n\). Then, \(a \in U_n\)
is a \emph{primitive root} of \(U_n\) if its a generator for \(U_n\). That is,

\begin{equation*}
U_n = \{a^k \ | \ k \in \mathbb{Z}\}
\end{equation*}

alternatively, \(a\) is a primitive root iff one of the three equivalent conditions hold

\begin{itemize}
\item \(\ord_n(a) = d\) and \(\{1, \overline{a}, \overline{a}^2, \ldots \overline{a}^{d-1} \in U_n\}\) are all distinct
\item \(\overline{a}^i = \overline{a}^j \iff i \equiv j (\mod d)\)
\item \(\ord_n(a) = \phi(n) = |U_n|\)
\end{itemize}

where \(\ord_n(a)\) is the order of \(a\) in \(\mathbb{Z}/n\mathbb{Z}\) (note that since \(U_n\)
is a finite group, \(a\) has finite order.

\section{Existance and numerousity of primitive roots}
\label{develop--math2400:modular-arithmetic:page--primitive-roots.adoc---existance-and-numerousity-of-primitive-roots}

Let \(p\) be a prime integer. Then \(U_p\) has exactly \(\phi(p-1)\) primitive roots.

\begin{admonition-remark}[{}]
Initially I saw this proof \href{https://wstein.org/edu/2007/spring/ent/ent-html/node29.html}{in this book}
however, I would give the proof given in class.
\end{admonition-remark}

\begin{example}[{Proof}]
Let

\begin{equation*}
A(m) = \{u \in u_p \ | \ \ord(u) = m\}
\end{equation*}

Then, \(A(m)\) is zero for all \(m\) which doesn't divide \(p-1\) (since the order
of an element divides the order of the group). So we would only consider when \(m | p-1\). Now,
consider the following sum

\begin{equation*}
\begin{aligned}
\sum_{d|m} |A(d) |
&= \sum_{d | m} |\{u \in u_p \ | \ \ord(u) = d\}|
\\&= |\{u \in u_p \ | \ \ord(u) | m\}|
\\&= |\{u \in u_p \ | \ u^m \equiv 1 (\mod p)\}|
\end{aligned}
\end{equation*}

Now, notice that \(u^{p-1} -1\) has exactly distinct \(p-1\) roots, hence any factor of this polynomial
must have the same number of roots as its degree otherwise the quotient would have more roots than its degree.
So since

\begin{equation*}
u^{p-1} -1 = (u^m -1)\sum_{k=0}^{m-1} u^{k\frac{p-1}{m}}
\end{equation*}

\(u^m -1\) has exactly \(m\) distinct roots. Hence

\begin{equation*}
\sum_{d|m} |A(d)| = m
\end{equation*}

and by mobius inversion

\begin{equation*}
|A(m)| = \sum_{d|m} \mu\left(\frac{m}{d}\right) d = \phi(m)
\end{equation*}

See \myautoref[{here}]{develop--math2400:arithmetic-functions:page--mobius-inversion.adoc---sum-of-euler-totient-function}. Hence we are done since \(A(p-1) = \phi(p-1) \)

\begin{admonition-remark}[{}]
Now, its a shame. Only one part of this proof fails when \(p\) is not prime, the number of roots of \(u^m-1\) may not have exactly \(m\).
roots.
\end{admonition-remark}
\end{example}

\subsection{More conditions for primitive roots mod \(p\)}
\label{develop--math2400:modular-arithmetic:page--primitive-roots.adoc---more-conditions-for-primitive-roots-mod-latex-backslashp-latex-backslash}

Let \(\gcd(a,p)=1\). Then, \(a\) is a primitive roots iff each of the following

\begin{itemize}
\item \(\forall q | p-1\ :\forall b \in U_p: b^q \not\equiv a (\mod p)\) where \(q\) is prime.
    
    \begin{itemize}
    \item Notice that if \(a\) is a primitve root and \(b^q \equiv a\), then \(a^\frac{p-1}{q} = b^{p-1} \equiv 1\) (contradiction)
    \item Notice that if \(a\) is not a primitive root, then \(a^{\frac{p-1}{q}} \equiv 1\) for some \(q\) (since the order of \(a\) divides \(p-1\))
        and by the existance of a primitive root, there exists a \(b\) such that \(b^k \equiv a\). Then, \(b^{k\frac{p-1}{q}} \equiv 1\) and \(k\frac{p-1}{q} \equiv 0 (\mod p-1)\)
        implies \(q | k\) and \(\left(b^{\frac{k}{q}}\right)^q \equiv a\).
    \end{itemize}
\item \(\forall q| p-1: a^\frac{p-1}{q} \not\equiv 1 (\mod p)\) where \(q\) is prime
    
    \begin{itemize}
    \item Notice that if \(a\) is a primitve root this leads to a contradiction
    \item Notice that if \(a\) is not a primitive root, since the order of \(a\) divides \(p-1\), \(a^{\frac{p-1}{q}}\) must be \(1\) for at least one \(q\).
    \end{itemize}
\end{itemize}

\begin{admonition-note}[{}]
The second condition gives an immediate way to easily test for primitive roots.
\end{admonition-note}
\end{document}
