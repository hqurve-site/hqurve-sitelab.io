\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Ordering}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
Let \(X\) be a set and \(\preceq\) be a relation on \(X\).
Then we say that \(\preceq\) is a \emph{partial order}
on \(X\) iff \(\preceq\) is reflexive, antisymmetric and transitive.
Then, we say that \((X, \preceq)\) is a \emph{partially ordered set} or \emph{poset}.

Furthermore, if \(\preceq\) is a \emph{total order} (or \emph{chain}) if
\(x \preceq y\) or \(y \preceq x\) for each \(x,y \in X\).

Also, notice that the inverse of \(\preceq\), denoted \(\succeq\),
is also a partial order on \(X\).

Now, using this, we can define a new relation \(\prec\) where \(x \prec y\)
iff \(x \preceq\) and \(x \neq y\). This relation is irreflexive and transitive.
Note that (anti)symmetry is not of note since the antecedent is never
satisfied.

Furthermore if \(x, y \in X\), we define the following

\begin{description}
\item[Predecessor] We say that \(x\) is \emph{smaller than} (or a \emph{predecessor} of) \(y\)
    if \(x \preceq y\) and \(x \neq y\).
\item[Successor] We say that \(x\) is \emph{greater than} (or a \emph{successor} of) \(y\)
    if \(x \succeq y\) and \(x \neq y\).
\end{description}

\begin{admonition-remark}[{}]
Since the inverse of a partial order is also a partial order, the
successor and predecessor are a matter of \href{https://www.youtube.com/watch?v=4BSfWh9uHoY}{perspective}.
\end{admonition-remark}

\section{Least vs Minimal}
\label{develop--math3274:relations:page--order.adoc---least-vs-minimal}

Let \((X, \preceq)\) be a poset. Then, we define the following

\begin{description}
\item[Least Element] \(a\) is a \emph{least} (or \emph{smallest} or \emph{first}) element of \(X\)
    if \(a \preceq x\) for all \(x \in X\).
\item[Greatest Element] \(a\) is a \emph{greatest} (or \emph{largest} or \emph{last}) element of \(X\)
    if \(x \preceq a\) for all \(x \in X\).
\item[Minimal Element] \(a\) is a \emph{minimal} element of \(X\) if there is no other \(x \in X\) strictly smaller
    than \(a\). ie \(x \preceq a \implies a = x\)
\item[Maximal Element] \(a\) is a \emph{maximal} element of \(X\) if there is no other \(x \in X\) strictly greater
    than \(a\). ie \(a \preceq x \implies a = x\)
\end{description}

Now, clearly by antisymmetry of \(\preceq\), a least element is automatically a
minimal element. Likewise of greatest and maximal.

Furthermore, if \(a\) is a least element, it is unique and it is the \emph{only}
minimal element.

\begin{example}[{Proof}]
Let \(a, b\) be least elements of \(X\). Then by antisymmetry, \(a = b\).

Now, suppose that \(c\) is a minimal element. Then, again, by antisymmetry, \(c = a\).
\end{example}

It should be noted that the converse is not true. A unique minimal element does not imply
it is the least element. For example, consider the set \(A \neq \varnothing\) which has no minimal element wrt \(\preceq\)
and define \(B = A \cup t\) where \(t \notin A\) and \(\preceq_1 = \preceq \cup \{(t,t)\}\).
Then, clearly, \(t\) is the unique minimal but since its not comparable with any element in \(A\),
it is not the least.

\section{Bounds}
\label{develop--math3274:relations:page--order.adoc---bounds}

Let \((X, \preceq)\) be a poset with \(E \subseteq X\). Then we define the following

\begin{description}
\item[Lower Bound] \(a \in X\) is a \emph{lower bound} of \(E\) if \(\forall x \in E\), \(a \preceq x\).
\item[Upper Bound] \(a \in X\) is a \emph{upper bound} of \(E\) if \(\forall x \in E\), \(x \preceq a\).
\item[Infimum] Let \(L = \{a \in X: \forall x \in E: a \preceq x\}\) (ie the set of lower bounds).
    Then \(a \in L\) is the \emph{infimum} (or \emph{greatest lower bound}) of \(E\) if
    \(a\) is the greatest element in \(L\).
\item[Supremum] Let \(U = \{a \in X: \forall x \in E: x \preceq a\}\) (ie the set of upper bounds).
    Then \(a \in U\) is the \emph{supremum} (or \emph{least upper bound}) of \(E\) if
    \(a\) is the least element in \(L\).
\end{description}

We now have the following theorem. Let \(E \subseteq X\), then

\begin{itemize}
\item If \(a \in E\) is the least element of \(E\), then \(a\) is a maximal
    element of the lower bounds. Hence \(a \in E\) would be the infimum of \(E\)
    if it is comparable with all other lower bounds of \(E\).
\item If \(a\) is the infimum of \(E\) and \(a \in E\), then \(a\)
    is the least element of \(E\).
\end{itemize}

\begin{example}[{Proof}]
The second follows directly from definition. We would only consider the first.

Let \(a \in E\) be the least element of \(E\). Suppose that there
exists \(b \in L\) (set of lower bounds) such that \(a \preceq b\). Then,
since \(a \in E\), \(b \preceq a\) and by antisymmetry, \(a = b\).
\end{example}

\section{Well ordering}
\label{develop--math3274:relations:page--order.adoc---well-ordering}

Let \((X, \preceq)\) be a poset such that \(\preceq\) is total. Then, we say that
\(\preceq\) is a \emph{well ordering} on \(X\) if every subset
of \(X\) contains a least element.
\end{document}
