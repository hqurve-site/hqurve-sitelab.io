\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Discrete random variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
A random variable is discrete if the masses of its atoms of probability sum to one.
There are several popular families of discrete random variables

\section{Bernoulli}
\label{develop--stats:ROOT:page--discrete-rvs.adoc---bernoulli}

This is the basis of many discrete distributions.
It models a statistical experiment with a binary outcome

\begin{itemize}
\item success (1) occurs with probability \(p\)
\item failure (0) occurs with probability \(1-p\)
\end{itemize}

\(X \sim Bernoulli(p)\)

\begin{description}
\item[pdf] \(P(X=x) = p^x(1-p)^{(1-x)}\), \(x=0,1\)
\item[expectation] \(E[X] = p\)
\item[variance] \(Var[X] = p(1-p)\)
\item[MGF] \(M(t) = (1-p)+pe^t\)
\end{description}

\section{Binomial}
\label{develop--stats:ROOT:page--discrete-rvs.adoc---binomial}

The binomial distribution models the number of success in \(n\) iid \(Bernoulli(p)\)
trials. As such, it is the sum of \(n\) iid \(Bernoulli(p)\) random variables.

\(X \sim Binomial(n,p)\)

\begin{description}
\item[pdf] \(P(X=x) = \binom{n,x} p^x(1-p)^{(1-x)}\), \(x=0,\ldots n\)
\item[expectation] \(E[X] = np\)
\item[variance] \(Var[X] = np(1-p)\)
\item[MGF] \(M(t) = ((1-p)+pe^t)^n\)
\end{description}

\section{Geometric}
\label{develop--stats:ROOT:page--discrete-rvs.adoc---geometric}

The geometric distribution models a sequence of iid \(Bernoulli(p)\)
trials which stop at the first success. We can either count the number
of failures or the total number of trials.
The below formulation counts the number of failures

\(X\sim Geometric(p)\)

\begin{description}
\item[pdf] \(P(X=x) = p(1-p)^x\), \(x=0,\ldots\)
\item[expectation] \(E[X] = \frac{1-p}{p}\)
\item[variance] \(Var[X] = \frac{1-p}{p^2}\)
\item[MGF] \(M(t) = \frac{p}{1-(1-p)e^t}\)
\end{description}

\section{Negative Binomial}
\label{develop--stats:ROOT:page--discrete-rvs.adoc---negative-binomial}

The negative binomial models a sequence of iid \(Bernoulli(p)\) trials
which stop at the \(k\)'th success. As such it is the sum of \(k\)
iid \(Geometric(p)\) trials. The below formulation counts
the number of failures

\(X\sim NegBinomial(k,p)\)

\begin{description}
\item[pdf] \(P(X=x) = \binom{k+x-1}{k-1} p^k(1-p)^x\), \(x=0,\ldots\)
\item[expectation] \(E[X] = \frac{k(1-p)}{p}\)
\item[variance] \(Var[X] = \frac{k(1-p)}{p^2}\)
\item[MGF] \(M(t) = \left(\frac{p}{1-(1-p)e^t}\right)^k\)
\end{description}

\section{Hypergeometric}
\label{develop--stats:ROOT:page--discrete-rvs.adoc---hypergeometric}

The hypergeometric distribution models the process of choosing
\(n\) objects from \(N\) without replacement.
Of the \(N\) objects, \(m\) are success and \(N-m\) are failure.
Although each selection is a Bernoulli trial, they are not independent.

\(X\sim Hypergeometric(N,m,n)\)

\begin{description}
\item[pdf] \(P(X=x) = \frac{\binom{m}{x} \binom{N-m}{n-x}}{\binom{N}{n}}\), \(x=\max(0,n+m-N),\ldots \min(n,m)\)
\item[expectation] \(E[X] = n\frac{m}{N}\)
\item[variance] \(Var[X] = n\frac{m}{N}\left(1-\frac{m}{N}\right)\frac{N-n}{N-1}\)
\item[MGF] No simple closed form
\end{description}

\section{Poisson}
\label{develop--stats:ROOT:page--discrete-rvs.adoc---poisson}

The poisson distribution counts the number of occurrences of an event
within a fixed space (and or time), given some average rate of occurrence, \(\lambda\).

\(X \sim Poisson(\lambda)\)

\begin{description}
\item[pdf] \(P(X-x) = \frac{e^{-\lambda}\lambda^x}{x!}\)
\item[expectation] \(E[X] = \lambda\)
\item[variance] \(Var[X] = \lambda\)
\item[MGF] \(M(t) = \exp(\lambda(e^t - 1))\)
\end{description}
\end{document}
