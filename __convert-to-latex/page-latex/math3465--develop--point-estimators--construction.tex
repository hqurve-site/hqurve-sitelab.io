\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Construction of Estimators}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Although we could ``randomly'' create and guess estimators and then test
their ability, it would be better if we had a systematic way of constructing estimators.

\section{Method of Moments}
\label{develop--math3465:point-estimators:page--construction.adoc---method-of-moments}

Perhaps one of the simplest ways of constructing an estimator is by utilizing the
\emph{method of moments}. That is, if we know that a parameter \(\theta\) can be represented
as a linear combination of \myautoref[{moments}]{develop--math2274:ROOT:page--random/index.adoc---moments},
we could then estimate it by the same
linear combination of sample moments. More concretely, if \(\theta\) is
a parameter of the random variable \(X\) and

\begin{equation*}
\theta = \sum_{r=1}^m a_r\mu_r
\end{equation*}

then we estimate \(\theta\) with

\begin{equation*}
\hat\theta = \sum_{r=1}^n a_rm_r
\end{equation*}

where \(\mu_r = E[X^r]\) and \(m_r = \sum_{i=1}^n \frac{x_i^r}{n}\).
Notice that by doing this, we assume that our sample is ``representative''
of the population. Specifically, if the \(x_i\) occur with the same distribution
as they do in the population, the parameter is accurately determined.

\section{Maximum Likelihood estimator}
\label{develop--math3465:point-estimators:page--construction.adoc---maximum-likelihood-estimator}

Another method which is used to generate moments is that of \emph{maximum likelihood estimators}.
Consider the sample \(\vec{x}\) which follows a distribution
dependent on the vector of parameters \(\vec{\theta}\). Then, we first
determine the probability (or measure of likelihood) of obtaining that sample given some set of
parameters. That is

\begin{equation*}
f(\vec{x} ; \vec{\theta}) = P(\vec{X} = \vec{x} \ | \ \vec{\theta} = \vec{\theta}) \quad\text{ or the joint pdf if continuous}
\end{equation*}

This is called our \emph{likelihood function}.
Then, we find \(\hat\theta\) which maximizes \(f\);
this is our \emph{maximum likelihood estimator}.

\begin{admonition-tip}[{}]
Sometimes it is easier to utilize the \emph{log likelihood function} which is
simply the logarithm composed with the likelihood function.
\end{admonition-tip}

\subsection{Bernoulli}
\label{develop--math3465:point-estimators:page--construction.adoc---bernoulli}

Let \(\vec{X} = (X_1, \ldots X_n)\)  be a vector of iid \(Bernoulli(p)\)
rand variables. Then the MLE of \(p\) is given by

\begin{equation*}
\hat p = \overbar{X}
\end{equation*}

\begin{example}[{Proof}]
By using the \myautoref[{exponential form}]{develop--math3465:statistics:page--index.adoc---exp-bernoulli}, we get that we need
to optimize the following equation

\begin{equation*}
L(p) = n\ln(1-p) + [\ln(p) - \ln(1-p)]\sum_{i=1}^n x_i
\end{equation*}

Then, we get a maximum when

\begin{equation*}
\begin{aligned}
&L'(p)
= -\frac{n}{1-p} + \left[\frac{1}{p} + \frac{1}{1-p}\right]\sum_{i=1}^n x_i
= -\frac{n}{1-p} + \frac{1}{p(1-p)}\sum_{i=1}^n x_i = 0
\\&\iff p = \frac{1}{n} \sum_{i=1}^n x_i
\end{aligned}
\end{equation*}
\end{example}

\subsection{Binomial (with \(n\) known)}
\label{develop--math3465:point-estimators:page--construction.adoc---binomial-with-latex-backslashn-latex-backslash-known}

Let \(\vec{X} = (X_1, \ldots X_N)\)  be a vector of iid \(Binomial(n, p)\)
rand variables. Then the MLE of \(p\) is given by

\begin{equation*}
\hat p = \frac{\sum_{i=1}^N X_i}{Nn}
\end{equation*}

Then notice that we get the same mle as to splitting the binomial into multiple bernoulli
random variables.

\begin{example}[{Proof}]
By using the \myautoref[{exponential form}]{develop--math3465:statistics:page--index.adoc---exp-binomial}, we get that we need
to optimize the following equation

\begin{equation*}
L(p)
= N\ln((1-p)^n) + [\ln(p) - \ln(1-p)]\sum_{i=1}^N x_i
= Nn\ln(1-p) + [\ln(p) - \ln(1-p)]\sum_{i=1}^N x_i
\end{equation*}

Then, we get a maximum when

\begin{equation*}
\begin{aligned}
&L'(p)
= -\frac{Nn}{1-p} + \left[\frac{1}{p} + \frac{1}{1-p}\right]\sum_{i=1}^N x_i
= -\frac{Nn}{1-p} + \frac{1}{p(1-p)}\sum_{i=1}^N x_i = 0
\\&\iff p = \frac{1}{Nn} \sum_{i=1}^B x_i
\end{aligned}
\end{equation*}
\end{example}

\subsection{Multinomial (with \(n\) and \(m\) known)}
\label{develop--math3465:point-estimators:page--construction.adoc---multinomial-with-latex-backslashn-latex-backslash-and-latex-backslashm-latex-backslash-known}

Let \(\vec{X} = (X_1, \ldots X_N)\)  be a vector of iid \(Multinomial(n, \vec{a})\)
rand variables. Then the MLE of \(\vec{a}\) is given by

\begin{equation*}
\hat a_j = \frac{1}{Nn}\sum_{i=1}^N x_{ij}
\end{equation*}

where \(x_{ij}\) be the count of the \(j\)'th item in \(X_i\). In other words,
\(\hat a_j\) is the average sample proportion of the \(j\)'th item.

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
We would not directly use the \myautoref[{exponential form}]{develop--math3465:statistics:page--index.adoc---exp-multinomial}
as it is too cumbersome.
\end{admonition-note}

Firstly, the likelihood function is

\begin{equation*}
f(\vec{x}; \vec{a}) = \prod_{i=1}^N \binom{n}{x_{i1}, \ldots x_{im}}\prod_{j=1}^m a_j^{x_{ij}}
\end{equation*}

and the log likelihood function is

\begin{equation*}
L(\vec{x}; \vec{a})
= \sum_{i=1}^N \left[\ln \left(\binom{n}{x_{i1}, \ldots x_{im}}\right) +  \sum_{j=1}^m x_{ij}\ln(a_j)\right]
= \sum_{i=1}^N \ln \left(\binom{n}{x_{i1}, \ldots x_{im}}\right) + \sum_{j=1}^m\sum_{i=1}^N  x_{ij}\ln(a_j)
\end{equation*}

Now, we use the technique of \myautoref[{Lagrange multipliers}]{develop--math2270:ROOT:page--stationary-points-and-optimization.adoc---lagrange-multipliers}
with constraint \(\sum_{j=1}^m a_j = 1\). That is we need to optimize the equation

\begin{equation*}
F(\vec{a}, \lambda) = \sum_{j=1}^m\sum_{i=1}^N  x_{ij}\ln(a_j) + \lambda\left(\sum_{j=1}^m a_j -1\right)
\end{equation*}

by ignoring constant terms. Now, for each \(j= 1\ldots m\), we require that

\begin{equation*}
\frac{\partial F}{\partial a_j} = \sum_{i=1}^N \frac{x_{ij}}{a_j} + \lambda = 0
\iff a_j = -\frac{1}{\lambda}\sum_{i=1}^N x_{ij}
\end{equation*}

Then, either by taking the partial differential or directly applying the constraint, we get that

\begin{equation*}
1 = \sum_{j=1}^m a_j = -\frac{1}{\lambda} \sum_{j=1}^m \sum_{i=1}^N x_{ij} = \frac{1}{\lambda} Nn \iff \lambda = -Nn
\end{equation*}

since \(\sum_{j=1}^m x_{ij} = n\).
Therefore

\begin{equation*}
a_j = \frac{1}{Nn}\sum_{i=1}^N x_{ij}
\end{equation*}

\begin{admonition-remark}[{}]
I didn't expect Lagrange multipliers to make a comeback.
\end{admonition-remark}
\end{example}

\subsection{Poisson}
\label{develop--math3465:point-estimators:page--construction.adoc---poisson}

Let \(\vec{X} = (X_1, \ldots X_n)\) be a vector of
of iid \(Poisson(\lambda)\) random variables. Then, the MLE
of \(\lambda\) is given by

\begin{equation*}
\hat\lambda = \frac{\sum_{i=1}^n X_i}{n} = \overbar{X}
\end{equation*}

\begin{example}[{Proof}]
Firstly, the likelihood function is simply the product of probabilities of
obtaining the given sample. That is,

\begin{equation*}
f(\vec{x}; \lambda)
= \prod_{i=1}^n P(X = x_i)
= \prod_{i=1}^n \frac{e^{-\lambda} \lambda^{x_i}}{x_i!}
= e^{-n\lambda} \frac{\lambda^{\sum_{i=1}^n x_i}}{\prod_{i=1}^n x_i!}
\end{equation*}

Then, the log likelihood function is

\begin{equation*}
L(\vec{x}; \lambda) = \ln(f(\vec{x}; \lambda)) = -n\lambda + \ln (\lambda) \sum_{i=1}^n x_i - \sum_{i=1}^n \ln (x_i!)
\end{equation*}

which attains a maximum when its derivative is \(0\)

\begin{equation*}
\frac{dL}{d\lambda} = -n + \frac{1}{\lambda} \sum_{i=1}^n x_i = 0
\iff \lambda = \frac{\sum_{i=1}^n x_i}{n}
\end{equation*}
\end{example}

\subsection{Normal}
\label{develop--math3465:point-estimators:page--construction.adoc---normal}

Let \(\vec{X} = (X_1, \ldots X_n)\) be a vector of
of iid \(N(\mu, \sigma^2)\) random variables. Then, the MLE
of \((\mu, \sigma^2)\) is

\begin{equation*}
(\hat\mu, \hat\sigma^2) = \left(\overbar{X}, \frac{n-1}{n}S^2\right)
\end{equation*}

\begin{example}[{Proof}]
Firstly, the likelihood function is simply the product of probabilities of
obtaining the given sample. That is,

\begin{equation*}
f(\vec{x}; \mu, \sigma^2)
= \prod_{i=1}^n f(x_i; \mu, \sigma^2)
= \prod_{i=1}^n \frac{1}{\sqrt{2\pi\sigma^2}} \exp\left\{\frac{-(x_i -\mu)^2}{2\sigma^2}\right\}
= (2\pi\sigma^2)^{-\frac{n}{2}}  \exp\left\{-\frac{1}{2\sigma^2} \sum_{i=1}^n (x_i -\mu)^2\right\}
\end{equation*}

Then, the log likelihood function is

\begin{equation*}
L(\vec{x}; \mu, \sigma^2) = -\frac{n}{2}\left[\ln(2\pi) + \ln(\sigma^2)\right] - \frac{1}{2\sigma^2} \sum_{i=1}^n (x_i -\mu)^2
\end{equation*}

Then, we maximize wrt \(\mu\) by

\begin{equation*}
\frac{\partial L}{\partial \mu} = \frac{1}{\sigma^2} \sum_{i=1}^n (x_i -\mu) = 0
\iff \mu = \frac{1}{n} \sum_{i=1}^n x_i = \overbar{x}
\end{equation*}

and we maximize wrt \(\sigma^2\) by

\begin{equation*}
\begin{aligned}
&\frac{\partial L}{\partial \sigma^2} = -\frac{n}{2\sigma^2} + \frac{1}{2(\sigma^2)^2} \sum_{i=1}^n (x_i -\mu)^2 = 0
\\&\iff n\sigma^2 = \sum_{i=1}^n (x_i -\mu)^2
\\&\iff \sigma^2 = \frac{1}{n}\sum_{i=1}^n (x_i -\mu)^2 = \frac{n-1}{n}S^2
\end{aligned}
\end{equation*}
\end{example}

\begin{admonition-note}[{}]
The estimator for \(\sigma^2\) is a biased estimator for the sample variance. Sometimes,
MLEs do not yield unbiased estimators.
\end{admonition-note}

\subsection{Uniform}
\label{develop--math3465:point-estimators:page--construction.adoc---uniform}

Let \(\vec{X} = (X_1, \ldots X_n)\) be a vector of
of iid \(Uniform[a, b]\) random variables. Then, the MLE
of \((a, b)\) is

\begin{equation*}
(\hat a, \hat b) = (\min \{X_i\}, \max\{X_i\})
\end{equation*}

\begin{example}[{Proof}]
Firstly, the likelihood function is given by

\begin{equation*}
f(\vec{x}; a, b) = \prod_{i=1}^n f(x_i; a, b) = \prod_{i=1}^n \frac{1}{b-a} = \frac{1}{(b-a)^n}
\end{equation*}

Now, the maximum of this function occurs when we minimize \((b-a)\). In fact, we can
both minimize \(b\) and maximize \(a\) simultaneously. By the nature of the
uniform distribution, we require that \(a \leq x_i \leq b\)  for all \(i=1\ldots n\).
Therefore, the minimum possible value for \(b\) is the maximum of the \(x_i\)
while the maximum of \(a\) is the minimum of the \(x_i\)
\end{example}

Notice that if we were looking at a uniform distribution over a open or half open interval, these
estimates would not yield the given sample (since endpoints are excluded).

\begin{admonition-remark}[{}]
This example is truly unbelievable. It seems like a ``wrong solution'' as we would
never expect an observation at one of the endpoints. However, this is not a ``sensible''
set of parameters, they are the most likely.
\end{admonition-remark}

\subsection{Gamma}
\label{develop--math3465:point-estimators:page--construction.adoc---gamma}

Let \(\vec{X} = (X_1, \ldots X_n)\) be a vector of
of iid \(Gamma(\alpha, \beta)\) random variables. Then, the MLE
of \((\alpha, \beta)\) is given by

\begin{equation*}
\psi(\hat\alpha) - \ln(\hat\alpha) = \ln\left(\sqrt[n]{\prod_{i=1}^n X_i}\right) - \ln\left(\frac{1}{n}\sum_{i=1}^n X_i\right)
\end{equation*}

where \(\psi\) is the \href{https://en.wikipedia.org/wiki/Polygamma\_function}{polygamma function}
and \(\hat\beta = \frac{1}{\hat\alpha n}\sum_{i=1}^n X_i\). What is interesting is that these are functions
of the algebraic and geometric means.

\begin{admonition-tip}[{}]
See \href{https://en.wikipedia.org/wiki/Gamma\_distribution\#Maximum\_likelihood\_estimation}{wikipedia article} on how to proceed from here.
\end{admonition-tip}

\begin{example}[{Proof}]
By using the \myautoref[{exponential form}]{develop--math3465:statistics:page--index.adoc---exp-gamma}, we get that we need
to optimize the following equation

\begin{equation*}
\begin{aligned}
L(p)
&= n\ln\left(\frac{1}{\beta^\alpha \Gamma(\alpha)}\right) + (\alpha - 1)\sum_{i=1}^n \ln (x_i) + \frac{1}{\beta}\sum_{i=1}^n x_i
\\&= -n\alpha\ln(\beta) - n\ln(\Gamma(\alpha)) + (\alpha - 1)\sum_{i=1}^n \ln (x_i) - \frac{1}{\beta}\sum_{i=1}^n x_i
\end{aligned}
\end{equation*}

Then, we get a maximum when

\begin{equation*}
\begin{aligned}
&\frac{\partial L}{\partial \alpha}= -n\ln(\beta) -n\frac{\Gamma(\alpha)\psi(\alpha)}{\Gamma(\alpha)} + \sum_{i=1}^n \ln (x_i) = 0
\\& \iff \ln(\beta) + \psi(\alpha) = \frac{1}{n} \sum_{i=1}^n \ln (x_i)
\end{aligned}
\end{equation*}

... which is ugly and

\begin{equation*}
\begin{aligned}
&\frac{\partial L}{\partial \beta}= -n\frac{\alpha}{\beta} + \frac{1}{\beta^2}\sum_{i=1}^n x_i = 0
\\&\iff n\alpha\beta = \sum_{i=1}^n x_i
\\&\iff \beta = \frac{1}{\alpha n}\sum_{i=1}^n x_i
\end{aligned}
\end{equation*}

By substituting back into the previous equation

\begin{equation*}
\begin{aligned}
& -\ln(\alpha) + \ln\left(\frac{1}{n}\sum_{i=1}^n x_i\right) + \psi(\alpha) = \frac{1}{n} \sum_{i=1}^n \ln (x_i)
\\& \iff \psi(\alpha) - \ln(\alpha) = \frac{1}{n} \sum_{i=1}^n \ln (x_i) - \ln\left(\frac{1}{n}\sum_{i=1}^n x_i\right)
\\& \iff \psi(\alpha) - \ln(\alpha) = \ln\left(\sqrt[n]{\prod_{i=1}^n x_i}\right) - \ln\left(\frac{1}{n}\sum_{i=1}^n x_i\right)
\end{aligned}
\end{equation*}
\end{example}

\subsection{Beta}
\label{develop--math3465:point-estimators:page--construction.adoc---beta}

Let \(\vec{X} = (X_1, \ldots X_n)\) be a vector of
of iid \(Beta(\alpha, \beta)\) random variables. Then, the MLE
of \((\alpha, \beta)\) is given by the following equations

\begin{equation*}
\psi(\hat\alpha) - \psi(\hat\alpha + \hat\beta) = \ln\left(\sqrt[n]{ \prod_{i=1}^n x_i}\right)
\end{equation*}

\begin{equation*}
\psi(\hat\beta) - \psi(\hat\alpha + \hat\beta) = \ln\left(\sqrt[n]{ \prod_{i=1}^n (1-x_i)}\right)
\end{equation*}

where \(\psi\) is the \href{https://en.wikipedia.org/wiki/Polygamma\_function}{polygamma function}.

\begin{example}[{Proof}]
By using the \myautoref[{exponential form}]{develop--math3465:statistics:page--index.adoc---exp-beta}, we get that we need
to optimize the following equation

\begin{equation*}
\begin{aligned}
L(p)
&= n\ln\left(\frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha)\Gamma(\beta)}\right) + (\alpha - 1)\sum_{i=1}^n \ln (x_i) + (\beta - 1)\sum_{i=1}^n \ln (1 - x_i)
\\&= n\ln(\Gamma(\alpha + beta)) - n\ln(\Gamma(\alpha)) - n\ln(\Gamma(beta)) + (\alpha - 1)\sum_{i=1}^n \ln (x_i) + (\beta - 1)\sum_{i=1}^n \ln (1 - x_i)
\end{aligned}
\end{equation*}

Then, we get a maximum when

\begin{equation*}
\begin{aligned}
&\frac{\partial L}{\partial \alpha}= n\frac{\Gamma(\alpha + \beta)\psi(\alpha + \beta)}{\Gamma(\alpha + \beta)} - n \frac{\Gamma(\alpha)\psi(\alpha)}{\Gamma(\alpha)}+ \sum_{i=1}^n \ln (x_i) = 0
\\&\iff  \psi(\alpha) - \psi(\alpha + \beta) = \ln\left(\sqrt[n]{ \prod_{i=1}^n x_i}\right)
\end{aligned}
\end{equation*}

and by symmetry

\begin{equation*}
\frac{\partial L}{\partial \beta} = 0 \iff \psi(\beta) - \psi(\alpha + \beta) = \ln\left(\sqrt[n]{ \prod_{i=1}^n (1-x_i)}\right)
\end{equation*}
\end{example}

\subsection{Simple Linear Regression}
\label{develop--math3465:point-estimators:page--construction.adoc---simple-linear-regression}

Suppose we have a set of fixed values, which we denote \(\{X_i\}\), and
for each \(X_i\) we observe a corresponding value for \(Y_i\). Then,
in simple linear regression, we make the following assumptions

\begin{itemize}
\item The \(Y_i\) are independent
\item \(E(Y_i | X_i = x)\) is a linear function of \(x\).
\item The conditional distribution of \(Y_i\) given a value for \(X_i\) is normal
    and has variance which does not depend on \(X_i\).
\end{itemize}

More simply put,

\begin{equation*}
Y_i = \alpha + \beta X_i + \varepsilon_i
\end{equation*}

where the \(\varepsilon_i\) are iid \(N(0, \sigma^2)\). Then, the mle of each of these
parameters is

\begin{equation*}
\hat\alpha = \overbar{Y} - \hat\beta \overbar{X}
\quad\text{and}\quad
\hat\beta = \frac{\sum_{i=1}^n (X_i - \overbar{X})(Y_i - \overbar{Y})}{\sum_{i=1}^n (X_i - \overbar{X})^2}
\quad\text{and}\quad
\hat\sigma^2 = \frac{1}{n}\sum_{i=1}^n (y_i - (\alpha + \beta x_i))^2= \frac{1}{n}\sum_{i=1}^n \varepsilon_i^2
\end{equation*}

Then notice that since \(E[\varepsilon] =0\), \(\hat\sigma^2\) is simply the biased sample variance of
random errors.

\begin{example}[{Proof}]
Firstly, the likelihood function is given by

\begin{equation*}
\begin{aligned}
f(\vec{y} | \vec{x}, \alpha, \beta, \sigma^2)
&= \prod_{i=1}^n \frac{1}{\sqrt{2\pi \sigma^2}} \exp\left\{ \frac{-(y_i - (\alpha + \beta x_i))^2}{2\sigma^2} \right\}
\\&= (2\pi)^{\frac{-n}{2}}\left(\sigma^2\right)^{\frac{-n}{2}} \exp\left\{ \frac{-1}{2\sigma^2} \sum_{i=1}^n (y_i - (\alpha + \beta x_i))^2 \right\}
\end{aligned}
\end{equation*}

since each \(Y_i \sim N(\alpha + \beta X_i, \sigma^2)\). Then, we get the following log likelihood function

\begin{equation*}
L(\vec{y} | \vec{x}, \alpha, \beta, \sigma^2)
= -\frac{n}{2}\ln(2\pi) - \frac{n}{2}\ln (\sigma^2) - \frac{1}{2\sigma^2} \sum_{i=1}^n (y_i - (\alpha + \beta x_i))^2
\end{equation*}

Now, we optimize

\begin{equation*}
\begin{aligned}
&\frac{\partial L}{\partial \sigma^2} = -\frac{n}{2} \frac{1}{\sigma^2} + \frac{1}{2} \frac{1}{(\sigma^2)^2}\sum_{i=1}^n (y_i - (\alpha + \beta x_i))^2 =0
\\&\iff    \sigma^2 = \frac{1}{n}\sum_{i=1}^n (y_i - (\alpha + \beta x_i))^2
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
& \frac{\partial L}{\partial \alpha} = \frac{1}{\sigma^2} \sum_{i=1}^n (y_i - (\alpha + \beta x_i)) =0
\\& \iff   \sum_{i=1}^n y_i - n\alpha - \left(\sum_{i=1}^n x_i\right) \beta = 0
\\& \iff   \sum_{i=1}^n y_i = n\alpha + \left(\sum_{i=1}^n x_i\right) \beta
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
& \frac{\partial L}{\partial \beta} = \frac{1}{\sigma^2} \sum_{i=1}^n x_i(y_i - (\alpha + \beta x_i)) = 0
\\& \iff  \sum_{i=1}^n x_iy_i - \left(\sum_{i=1}^n x_i\right)\alpha - \left(\sum_{i=1}^n x_i^2\right)\beta = 0
\\& \iff  \sum_{i=1}^n x_iy_i = \left(\sum_{i=1}^n x_i\right)\alpha + \left(\sum_{i=1}^n x_i^2\right)\beta
\end{aligned}
\end{equation*}

Then notice, when we convert this to matrix form, we get the same matrix (and hence same solution)
as with the \myautoref[{least squares model}]{develop--math2275:ROOT:page--simple-linear-regression.adoc---least-squares-parameters-matrix}.

\begin{equation*}
\begin{bmatrix}
    \sum_{i=1}^n y_i \\
    \sum_{i=1}^n x_i y_i
\end{bmatrix}
=
\begin{bmatrix}
    n & \sum_{i=1}^n x_i\\
    \sum_{i=1}^n x_i & \sum_{i=1}^n x_i^2
\end{bmatrix}
\begin{bmatrix}
    \alpha \\
    \beta
\end{bmatrix}
\end{equation*}
\end{example}
\end{document}
