\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Inner products}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Definition}
\label{develop--math2273:ROOT:page--inner-products.adoc---definition}

Let \(V\) be a real vector space. An \emph{inner product} on
\(V\) is a function
\(\left\langle \cdot, \cdot \right\rangle: V \times V \rightarrow \mathbb{R}\)
which satisfies the following

\begin{enumerate}[label=\arabic*)]
\item \(\left\langle \underline{u}, \underline{u} \right\rangle \geq 0\),
    \(\forall \underline{u} \in V\) (non-negativity)
\item \(\forall \underline{u} \in V: \left\langle \underline{u}, \underline{u} \right\rangle = 0 \Longleftrightarrow \underline{u} = \underline{0}\)
    (coincidence)
\item \(\left\langle \underline{u}, \underline{v} \right\rangle = \left\langle \underline{v}, \underline{u} \right\rangle\),
    \(\forall \underline{u}, \underline{v} \in V\) (symmetry)
\item \(\left\langle a\underline{u} + b\underline{v}, \underline{w} \right\rangle = a\left\langle \underline{u}, \underline{w} \right\rangle + b\left\langle \underline{v}, \underline{w} \right\rangle\),
    \(\forall \underline{u}, \underline{v}, \underline{w} \in V\)
    (linearity)
\end{enumerate}

Furthermore, the pair
\((V, \left\langle \cdot, \cdot \right\rangle)\) is called an
\emph{inner product space}.

\subsection{Note}
\label{develop--math2273:ROOT:page--inner-products.adoc---note}

\begin{itemize}
\item Condition (ii) can be weakened using (iv) since
    \(0\underline{u} = \underline{0}\).
\item Condition (iv) can be extended to bilinearity using symmetry.
\item Inner products can be defined for different fields not only
    \(\mathbb{R}\). However, their definitions vary slightly. (I
    don’t know if there is a unified version).
\end{itemize}

\subsection{Examples}
\label{develop--math2273:ROOT:page--inner-products.adoc---examples}

\begin{enumerate}[label=\arabic*)]
\item Consider the real vector space \(\mathbb{R}^n\). Then the
    \emph{dot product} or \emph{Euclidean inner product} \(\mathbb{R}^n\)
    denoted \(\underline{x} \cdot \underline{y}\) is in inner
    product where
    
    \begin{equation*}
    \underline{x}\cdot \underline{y} = x_1y_1 + x_2y_2 +\cdots + x_ny_n = \sum_{i=1}^nx_iy_i
    \end{equation*}
    
    and
    \(\underline{x} = (x_1, x_2, \ldots, x_n), \underline{y} = (y_1, y_2, \ldots, y_n) \in \mathbb{R}^n\).
    
    \begin{example}[{Proof}]
    \begin{description}
    \item[Non-negativity] since \(x_i \in \mathbb{R}\)
    \end{description}
    
    \begin{equation*}
    \underline{x} \cdot \underline{x} = x_1^2 + x_2^2 + \cdots +x_n^2 \geq 0
    \end{equation*}
    
    \begin{description}
    \item[Coincidence] since \(x_i \in \mathbb{R}\)
    \end{description}
    
    \begin{equation*}
    \begin{aligned}
                        &\underline{x} \cdot \underline{x} = x_1^2 + x_2^2 + \cdots +x_n^2 = 0\\
                        &\implies x_1^2 = x_2^2 = \cdots = x_n^2\\
                        &\implies x_1 = x_2 = \cdots = x_n\\
                        &\implies \underline{x} = \underline{0}
                    \end{aligned}
    \end{equation*}
    
    \begin{description}
    \item[Symmetry] 
    \end{description}
    
    \begin{equation*}
    \underline{x}\cdot \underline{y}
                            = x_1y_1 + x_2y_2 +\cdots + x_ny_n
                            = y_1x_1 + y_2x_2 +\cdots + y_nx_n
                            = \underline{y}\cdot\underline{x}
    \end{equation*}
    
    \begin{description}
    \item[Linearity] 
    \end{description}
    
    \begin{equation*}
    \begin{aligned}
                            (a\underline{x} + b\underline{y}) \cdot \underline{z}
                            &= (ax_1 + by_1)z_1 + (ax_2 + by_2)z_2 + \cdots + (ax_n + by_n)z_n\\
                            &= a(x_1z_1 + x_2z_2 + \cdots + x_nz_n) + b(y_1z_1 + y_2z_2 + \cdots + y_nz_n)\\
                            &= a(\underline{x} \cdot \underline{z}) + b(\underline{y} \cdot \underline{z})
                        \end{aligned}
    \end{equation*}
    
     ◻
    \end{example}
\item Consider the real vector space \(\mathbb{P}_2\) and distinct
    \(\alpha, \beta, \gamma \in \mathbb{R}\). Then the following
    function is an inner product over \(\mathbb{P}_2\)
    
    \begin{equation*}
    \left\langle p, q \right\rangle = p(\alpha)q(\alpha) + p(\beta)q(\beta) + p(\gamma)q(\gamma)
    \end{equation*}
    
    where \(p, q\in \mathbb{P}_2\). Notice that this can be
    extended to \(\mathbb{P}_n\) by taking \((n+1)\)
    distinct real values instead of \(3\).
    
    \begin{example}[{Proof}]
    \begin{description}
    \item[Non-negativity] 
    \end{description}
    
    \begin{equation*}
    \left\langle p, p \right\rangle
                        = p(\alpha)p(\alpha) + p(\beta)p(\beta) + p(\gamma)p(\gamma)
                        = p(\alpha)^2 + p(\beta)^2 + p(\gamma)^2
                        \geq 0
    \end{equation*}
    
    \begin{description}
    \item[Coincidence] 
    \end{description}
    
    \begin{equation*}
    \begin{aligned}
                        & \left\langle p, p \right\rangle = p(\alpha)^2 + p(\beta)^2 + p(\gamma)^2 = 0\\
                        &\implies  p(\alpha)=0, \quad p(\beta)=0, \quad p(\gamma) = 0\\
                        &\implies  p = \theta \in \mathbb{P}_2
                    \end{aligned}
    \end{equation*}
    
    since \(deg(p) =2\) while it has \(3\) distinct
    roots.
    Symmetry::
    
    \begin{equation*}
    \left\langle p, q \right\rangle
                        = p(\alpha)q(\alpha) + p(\beta)q(\beta) + p(\gamma)q(\gamma)
                        = q(\alpha)p(\alpha) + q(\beta)p(\beta) + q(\gamma)p(\gamma)
                        = \left\langle q, p \right\rangle
    \end{equation*}
    
    \begin{description}
    \item[Linearity] 
    \end{description}
    
    \begin{equation*}
    \begin{aligned}
                        \left\langle ap + bq, r \right\rangle
                        &= (ap + bq)(\alpha)r(\alpha) + (ap + bq)(\beta)r(\beta) + (ap + bq)(\gamma)r(\gamma)\\
                        &= a(p(\alpha)r(\alpha) + p(\beta)r(\beta) + p(\gamma)r(\gamma)) + b(q(\alpha)r(\alpha) + q(\beta)r(\beta) + q(\gamma)r(\gamma))\\
                        &= a\left\langle p, r \right\rangle + b\left\langle q,r \right\rangle
                    \end{aligned}
    \end{equation*}
    
     ◻
    \end{example}
\item Consider the real vector space \(\mathbb{P}_n\) and
    \(R \subseteq \mathbb{R}\) then the following function is an
    inner product over \(\mathbb{P}_n\)
    
    \begin{equation*}
    \left\langle p, q \right\rangle = \int_R p(t)q(t) dt
    \end{equation*}
    
    where \(p, q\in \mathbb{P}_n\). The proof of this is fairly
    trivial and mostly follows from the linearity of the integral and that
    order is preserved under integration.
\end{enumerate}

Side note: I would like to know what this function is called in general.
It looks like a convolution but its not. Perhaps its a special type of
convolution or rather another special case of what a convolution really
is.

Update: upon reading about hilbert spaces, it have come along the same
inner product but no name was mentioned.

\section{Norm and unit vector}
\label{develop--math2273:ROOT:page--inner-products.adoc---norm-and-unit-vector}

Let \(V\) be in inner product space with inner product
\(\left\langle \cdot, \cdot \right\rangle\). Then the \emph{norm}
of \(\underline{u}\in V\) is denoted and defined by

\begin{equation*}
\left\| \underline{u} \right\| = \sqrt{\left\langle \underline{u}, \underline{u} \right\rangle}
\end{equation*}

Additionally, a \emph{unit vector} is any vector
\(\underline{u} \in V\) such that
\(\left\| u \right\| = 1\). Furthermore for any
\(\underline{u} \in V\backslash\{\underline{0}\}\), the \emph{unit
vector in the direction of \(\underline{u}\)} is denoted and
defined by

\begin{equation*}
\hat{\underline{u}} = \frac{1}{\left\| \underline{u} \right\|}\underline{u}
\end{equation*}

\subsection{Examples}
\label{develop--math2273:ROOT:page--inner-products.adoc---examples-2}

\begin{enumerate}[label=\arabic*)]
\item The norm associated with the dot product in \(\mathbb{R}^n\)
    is called the \emph{Euclidean norm} on \(\mathbb{R}^n\)
\end{enumerate}

\subsection{Cauchy-Schwarz Inequality}
\label{develop--math2273:ROOT:page--inner-products.adoc---cauchy-schwarz-inequality}

\begin{equation*}
\left| \left\langle \underline{u}, \underline{v} \right\rangle \right| \leq \left\| \underline{u} \right\|\left\| \underline{v} \right\|.
\end{equation*}

Moreover, equality holds if and only if \(\underline{u}\) and
\(\underline{v}\) are linearly dependent.

\begin{example}[{Proof}]
Clearly, if \(\underline{u} = \underline{0}\) or
\(\underline{v} = \underline{0}\) the result follows. So we
will take
\(\underline{u}, \underline{v} \in V \backslash \{\underline{0}\}\).
Let
\(\lambda = -\frac{\left\langle \underline{u}, \underline{v} \right\rangle}{\left\| \underline{v} \right\|^2} \in \mathbb{R}\)
then

\begin{equation*}
\begin{aligned}
        \left\langle \underline{u} + \lambda\underline{v},\underline{u} + \lambda\underline{v} \right\rangle
        &= \left\langle \underline{u}, \underline{u} \right\rangle
            + \left\langle \underline{u}, \lambda \underline{v} \right\rangle
            + \left\langle \lambda \underline{v}, \underline{u} \right\rangle
            + \left\langle \lambda \underline{v}, \lambda\underline{v} \right\rangle
            \\
        &= \left\| \underline{u} \right\|^2
            + 2 \lambda \left\langle \underline{u}, \underline{v} \right\rangle
            + \lambda^2 \left\| \underline{v} \right\|^2
            \\
        &= \left\| \underline{u} \right\|^2
                - 2\frac{\left\langle \underline{u}, \underline{v} \right\rangle^2}{\left\| v \right\|^2}
                + \frac{\left\langle \underline{u}, \underline{v} \right\rangle^2\left\| \underline{v} \right\|^2}{\left\| v \right\|^4}
            \\
        &= \left\| \underline{u} \right\|^2
                - 2\frac{\left\langle \underline{u}, \underline{v} \right\rangle^2}{\left\| v \right\|^2}
                + \frac{\left\langle \underline{u}, \underline{v} \right\rangle^2}{\left\| v \right\|^2}
            \\
        &= \left\| \underline{u} \right\|^2
                - \frac{\left\langle \underline{u}, \underline{v} \right\rangle^2}{\left\| v \right\|^2}
    \end{aligned}
\end{equation*}

Therefore, by the non-negativity of the inner product (with identical
vectors), we get

\begin{equation*}
0 \leq \left\| \underline{u} \right\|^2 - \frac{\left\langle \underline{u}, \underline{v} \right\rangle^2}{\left\| v \right\|^4}
        \quad\Longleftrightarrow\quad
        \left\langle \underline{u}, \underline{v} \right\rangle^2 \leq \left\| \underline{u} \right\|^2\left\| \underline{v} \right\|^2
        \quad\Longleftrightarrow\quad
        \left| \left\langle \underline{u}, \underline{v} \right\rangle \right| \leq \left\| \underline{u} \right\|\left\| \underline{v} \right\|
\end{equation*}

Furthermore, by the coincidence property,

\begin{equation*}
\left| \left\langle \underline{u}, \underline{v} \right\rangle \right| = \left\| \underline{u} \right\|\left\| \underline{v} \right\|
        \quad\Longleftrightarrow\quad
        \underline{u} + \lambda\underline{v} = \underline{0}
        \quad\Longleftrightarrow\quad
        \text{$\underline{u}$ and $\underline{v}$ are linearly dependent}
\end{equation*}

 ◻
\end{example}

\section{Orthogonality}
\label{develop--math2273:ROOT:page--inner-products.adoc---orthogonality}

Let \(V\) be an inner product space with inner product
\(\left\langle \cdot, \cdot \right\rangle\). Then the angle,
\(\theta \in [0, \pi]\), between two vectors
\(\underline{u}\) and \(\underline{v}\) in
\(V\) is given by

\begin{equation*}
\theta = \arccos\left(
        \frac{
            \left\langle \underline{u}, \underline{v} \right\rangle
        }{
            \left\| \underline{u} \right\|\left\| \underline{v} \right\|
        }
     \right)
\end{equation*}

Furthermore, \(\underline{u}\) and \(\underline{v}\)
are \emph{orthogonal} or \emph{perpendicular}, denoted
\(\underline{u}\perp\underline{v}\) if
\(\left\langle \underline{u}, \underline{v} \right\rangle = 0\)
or \(\theta=\frac{\pi}{2}\).

\subsection{Orthogonal complement}
\label{develop--math2273:ROOT:page--inner-products.adoc---orthogonal-complement}

The \emph{orthogonal component} of a set \(S \subset V\) is the set

\begin{equation*}
S^\perp = \left\{ \underline{u}\in V| \forall \underline{v} \in S: \left\langle \underline{u}, \underline{v} \right\rangle= 0 \right\}
\end{equation*}

Furthermore, note that \(S^\perp\) is a subspace and that
\(\{\underline{0}\}^\perp = V\).

\subsection{Orthogonal and orthonormal sets}
\label{develop--math2273:ROOT:page--inner-products.adoc---orthogonal-and-orthonormal-sets}

A set
\(S = \{\underline{u}_1, \underline{u}_2, \ldots, \underline{u}_n\}\)
is an \emph{orthogonal set} if

\begin{equation*}
\underline{0} \notin S \text{ and }i\neq j \implies \left\langle \underline{u}_i, \underline{u}_j \right\rangle = 0
\end{equation*}

That is, the elements in \(S\) are mutually orthogonal.
Furthermore, \(S\) is a linearly independent set.

\begin{example}[{Proof}]
Let \(a_1, a_2, \ldots, a_n \in \mathbb{R}\) such that

\begin{equation*}
a_1\underline{u}_1 + a_2\underline{u}_2 + \cdots + a_n\underline{u}_n = \underline{0}
\end{equation*}

Then, consider any \(\underline{u}_i \in S\)

\begin{equation*}
\begin{aligned}
        \left\langle \underline{u}_i, a_1\underline{u}_1 + a_2\underline{u}_2 + \cdots + a_n\underline{u}_n \right\rangle
        &= a_1\left\langle \underline{u}_i, \underline{u}_1 \right\rangle
            + a_2\left\langle \underline{u}_i, \underline{u}_2 \right\rangle
            +\cdots
            + a_n\left\langle \underline{u}_i, \underline{u}_n \right\rangle
            \\
        &= a_i\left\langle \underline{u}_i, \underline{u}_i \right\rangle\\
        &= a_i\left\| \underline{u}_i \right\|^2
    \end{aligned}
\end{equation*}

Then since
\(\left\langle \underline{u}_i, \underline{0} \right\rangle = 0\)
and
\(\left\langle \underline{u}_i, \underline{u}_i \right\rangle = \left\| \underline{u}_i \right\|^2 \neq 0\),
\(a_i = 0\).\newline

 ◻
\end{example}

Additionally, if \(\underline{u} \in span(S)\) then

\begin{equation*}
\underline{u} = a_1\underline{u}_1 + a_2\underline{u}_2 + \cdots + a_n\underline{u}_n
\end{equation*}

and by using the formulation in the proof above we get that

\begin{equation*}
\left\langle \underline{u}, \underline{u}_i \right\rangle
    = \left\langle a_1\underline{u}_1 + a_2\underline{u}_2 + \cdots + a_n\underline{u}_n, \underline{u}_i \right\rangle
    = a_i\left\| \underline{u}_i \right\|^2
\end{equation*}

This result provides an easy way to evaluate the coefficients of a
linear combination of a orthogonal set. Furthermore, \(S\) is
\emph{orthonormal}, that is
\(\forall \underline{u} \in S: \left\| \underline{u} \right\| = 1\),
then

\begin{equation*}
\left\langle \underline{u}, \underline{u}_i \right\rangle = a_i\left\| \underline{u}_i \right\|^2 = a_i
\end{equation*}

hence providing solid motivation for using orthonormal sets as bases.

\subsection{Orthogonal bases and the Gram-Schmidt Orthogonalization}
\label{develop--math2273:ROOT:page--inner-products.adoc---orthogonal-bases-and-the-gram-schmidt-orthogonalization}

If
\(R =\{\underline{v}_1, \underline{v}_2, \ldots, \underline{v}_n\} \subset V\)
is linearly independent then there exists an orthogonal set
\(S = \{\underline{u}_1, \underline{u}_2, \ldots, \underline{u}_n\} \subset V\)
such that \(span(R) = span(S)\). This set can be constructed
using the \emph{Gram-Schmidt Orthogonalization Process} defined as followed.

\subsubsection{Step 1:}
\label{develop--math2273:ROOT:page--inner-products.adoc---step-1}

take \(\underline{u}_1 = \underline{v}_1\)

\subsubsection{Step k:}
\label{develop--math2273:ROOT:page--inner-products.adoc---step-k}

for \(k = 2,3,\ldots, n\) take

\begin{equation*}
\begin{aligned}
    \underline{u}_k
    &= \underline{v}_k - \sum_{i=1}^{k-1} Proj_{\underline{u}_i}(\underline{v}_k) \\
    &= \underline{v}_k - \sum_{i=1}^{k-1} \left\langle \underline{v}_k, \hat{\underline{u}_i} \right\rangle\hat{\underline{u}_i}\\
    &= \underline{v}_k - \sum_{i=1}^{k-1} \frac{\left\langle \underline{v}_k, \underline{u}_i \right\rangle}{\left\| \underline{u}_i \right\|^2} \underline{u}_i\end{aligned}
\end{equation*}

Furthermore, an orthonormal basis can be constructed by taking the set
\(\{\hat{\underline{u}_1}, \hat{\underline{u}_2}, \ldots, \hat{\underline{u}_n}\}\)
instead.

\begin{example}[{Proof}]
The proof will be conducted in two steps. Firstly, we need to show that
the set generated is indeed orthogonal. This will be done inductively
using the following sets and proposition. For
\(1 \leq k \leq n\)

\begin{equation*}
S_k = \{\underline{u}_1, \underline{u}_2, \ldots, \underline{u}_k\}
\end{equation*}

where \(S_k\) is an orthogonal. Clearly, this is true for
\(k=1\) (vacuous truth) now assume it is true for some
\(k\geq 1\). Now consider \(S_{k+1}\) and
\(\underline{u}_j\), where \(1 \leq j \leq k\), then

\begin{equation*}
\begin{aligned}
        \left\langle \underline{u}_j, \underline{u}_{k+1} \right\rangle
        &= \left\langle \underline{u}_j,\;\underline{v}_{k+1} - \sum_{i=1}^{k} \frac{\left\langle \underline{v}_{k+1}, \underline{u}_i \right\rangle}{\left\| \underline{u}_i \right\|^2} \underline{u}_i \right\rangle\\
        &= \left\langle \underline{u}_j, \underline{v}_{k+1} \right\rangle - \left\langle \underline{u}_j, \frac{\left\langle \underline{v}_{k+1}, \underline{u}_j \right\rangle}{\left\| \underline{u}_j \right\|^2} \underline{u}_j \right\rangle\\
        &= \left\langle \underline{u}_j, \underline{v}_{k+1} \right\rangle - \frac{\left\langle \underline{v}_{k+1}, \underline{u}_j \right\rangle}{\left\| \underline{u}_j \right\|^2} \left\langle \underline{u}_j, \underline{u}_j \right\rangle\\
        &= \left\langle \underline{u}_j, \underline{v}_{k+1} \right\rangle - \frac{\left\langle \underline{v}_{k+1}, \underline{u}_j \right\rangle}{\left\| \underline{u}_j \right\|^2} \left\| \underline{u}_j \right\|^2\\
        &= \left\langle \underline{u}_j, \underline{v}_{k+1} \right\rangle - \left\langle \underline{v}_{k+1}, \underline{u}_j \right\rangle\\
        &= 0
    \end{aligned}
\end{equation*}

and is hence \(\underline{u}_{k+1}\) is orthogonal to
\(S_k\). However, we need to show that
\(\underline{u}_{k+1} \neq \underline{0}\). Notice that each
of \(\underline{u}_k\) is linear combination of
\(\{\underline{v}_1, \underline{v}_2, \ldots, \underline{v}_k\}\).
Therefore, if \(\underline{u}_{k+1}\) were
\(\underline{0}\) it would invalidate the linear independence
of \(R\) since the coefficients of
\(\underline{v}_{k+1}\) in \(\underline{u}_{k+1}\)
is \(1\). Therefore, the final set, \(S = S_n\), is
orthogonal.

Furthermore, by quick rearrangement of the generating equation, we see
that \(R \subseteq span(S)\). However, since \(S\)
consists of elements in the span of \(R\),
\(span(R) = span(S)\) and we are done.

 ◻
\end{example}
\end{document}
