\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Order completeness of reals}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Fields}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---fields}

A set \(\mathbb{F}\) together with two binary operations
\(+\) and \(\cdot\) on \(\mathbb{F}\) form
a field \((\mathbb{F}, + , \cdot)\) if the following
properties hold

\begin{enumerate}[label=\arabic*)]
\item \(\forall x, y \in \mathbb{F}: x + y \in \mathbb{F}\) and
    \(x \cdot y \in \mathbb{F}\) (closure of
    \(\mathbb{F}\) under \( + \) and \(\cdot\)).
\item \( + \) and \(\cdot\) are both commutative. That is
    \(\forall x, y \in \mathbb{F}: x + y = y + x\) and
    \(x \cdot y = y \cdot x\).
\item \( + \) and \(\cdot\) are both associative. That is
    \(\forall x, y, z \in \mathbb{F}: (x+y)z = x(y+z)\) and
    \((x\cdot y)\cdot z = x \cdot (y \cdot z)\).
\item \(\cdot\) distributes over \( + \). That is
    \(\forall x, y, z \in \mathbb{F}: x \cdot(x + z) = (x \cdot y) + (x \cdot z)\).
\item \(\exists\, 0, 1 \in \mathbb{F}\) which are the identities
    under \(+\) and \(\cdot\) (respectively) in
    \(\mathbb{F}\).
\item \(\forall x \in \mathbb{F}: \exists (-x) \in \mathbb{F}: x + (-x) = 0\).
\item \(\forall x \in \mathbb{F}- \{0\}: \exists x^{-1} \in \mathbb{F}: x \cdot x^{-1} = 1\).
\end{enumerate}

The above definition can be summarized as follows

\begin{itemize}
\item \((\mathbb{F}, +)\) forms an abelian group where
    \(0\) is identity element.
\item \((\mathbb{F} - \{0\}, \cdot)\) forms an abelian group where
    \(1\) is identity element.
\item \(\cdot\) distributes over \(+\).
\end{itemize}

Note however, that the second point requires proof of closure of
\(\cdot\) over \(\mathbb{F} - \{0\}\) (this is
proven below).

\subsection{Convenience convensions}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---convenience-convensions}

Note that for convenience we use the following conventions

\begin{itemize}
\item \(\mathbb{F}\) used in place of
    \((\mathbb{F}, + , \cdot)\) when the operations are
    understood.
\item In general, we call \(+\) field addition and
    \(\cdot\) field multiplication even if they are not the
    standard operations.
\item Let \(n \in \mathbb{N}\). Then, we write/define
    
    \begin{itemize}
    \item \(x-y = x + (-y)\).
    \item \(xy = x\cdot y\).
    \item \(\frac{x}{y} = x\cdot y^{-1}\) where \(y \neq 0\).
    \item \((n+1)x = nx + x\)
    \item \(n = n(1)\). That is, the naturals are imbedded in the field.
    \item \((-n)x = n(-x)\)
    \item \(x^{n+1} = x^nx\)
    \item \(x^0 = 1\) where \(x \neq 0\)
    \item \(x^{-n} = (x^{-1})^n\) where \(x \neq 0\)
    \end{itemize}
\end{itemize}

\subsection{Immediate results}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---immediate-results}

Let \(x,y , z\in \mathbb{F}\), then

\begin{enumerate}[label=\roman*)]
\item \(x = y \implies x + z = y+ z\)
\item \(x = y \implies xz = yz\)
\item \(0x = 0\)
\item \(xy = 0 \implies x = 0 \text{ or } y =0\)
\item \(-x = (-1)x\)
\item \((-x)(-y) = xy\)
\end{enumerate}

\begin{example}[{Proof}]
\begin{admonition-caution}[{}]
I do not know how to prove the first two facts (or if it is even
possible). Regardless, here is a proof of the other ones
\end{admonition-caution}

\begin{description}
\item[Part (iii)] Since \(0\) is the additive identity
    
    \begin{equation*}
    0 = 0+ 0 \implies 0x = (0+0)x = 0x +0x
    \end{equation*}
    
    Then by the uniqueness of the identity in the group
    \((\mathbb{F}, +)\), \(0 = 0x\).
\item[Part (iv)] Assume BWOC that \(xy = 0\) and \(x \neq 0\) and
    \(y \neq 0\). Then,
    
    \begin{equation*}
    1
                = \left(x \cdot \frac{1}{x}\right)\left(y \cdot \frac{1}{y}\right)
                = \left(xy\right)\left( \frac{1}{x} \cdot \frac{1}{y}\right)
                = 0\left( \frac{1}{x} \cdot \frac{1}{y}\right)
                = 0
    \end{equation*}
    
    This is clearly not true since it would imply that
    \(1x = 0x = 0 \neq x\). Note: this actually is okay only when
    \(\mathbb{F}\) is the trivial field \(\{0\}\) but
    the result still holds.
\item[Part (v)] 
\end{description}

\begin{equation*}
x + (-1)x = 1x + (-1)x = (1-1)x = 0x = 0
\end{equation*}

+
Then by the uniqueness of the inverse in the group
\((\mathbb{F}, +)\), \((-1)x = -x\).

\begin{description}
\item[Part (vi)] By part (v) and the associativity and commutativity of
    \(\cdot\) in \(\mathbb{F}\) all we need to show is
    that \((-1)(-1) = 1\). However, again by part (v) and since
    the inverse of the inverse is the element in a group (specifically
    \((\mathbb{F}, +)\))
    
    \begin{equation*}
    (-1)(-1) = -(-1) = 1
    \end{equation*}
\end{description}

 ◻
\end{example}

\section{Ordered field}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---ordered-field}

A field \(\mathbb{F}\) is called an ordered field with respect
to the relation \(<\) if
\(\forall x, y, z \in \mathbb{F}\), the following hold

\begin{enumerate}[label=\arabic*)]
\item Law of trichotomy: exactly one of the following hold.
    
    \begin{equation*}
    x < y, \quad x =y, \quad y < x
    \end{equation*}
\item Transitivity of \(<\)
\item Monotonicity of addition: \(x < y \implies x + z < y +z\)
\item Monotonicity of multiplication: \(x < y\) and
    \(0 < z\) \(\implies xz < yz\)
\end{enumerate}

\subsection{Convenience convensions}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---convenience-convensions-2}

Note that for convenience we use the following conventions

\begin{itemize}
\item \(x < y\) is equivalent to \(y > x\).
\item We say \(x\) is \emph{negative} if \(x < 0\) and
    \emph{positive} if \(x > 0\). Similar conventions hold for 'greater
    than' and 'less than'.
\item We write \(x\) is \emph{greater than or equal} to \(y\)
    as \(x \geq y\). Again, a similar convention is used for 'less
    than or equal to'.
\item We say that \(x\) is \emph{nonnegative} if
    \(x \geq 0\).
\end{itemize}

\subsection{Immediate results}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---immediate-results-2}

Let \(w, x,y , z\in \mathbb{F}\), then

\begin{enumerate}[label=\roman*)]
\item \(w < x\) and \(y < z\)
    \(\implies w + y < x + z\).
\item \(x >0\), \(w < x\) and \(0 < y < z\)
    \(\implies wy < xz\).
\item \(x < y\) and \(z < 0\)
    \(\implies xz > yz\).
\item \(x \neq 0\) \(\implies\) \(x^2 > 0\).
    In particular \(1 > 0\).
\item \(n \in \mathbb{N} \implies n(1) > 0\)
\item \(0 < x < y\) \(\implies\)
    \(0 < \frac{1}{y} < \frac{1}{x}\).
\item \(xy > 0\) \(\implies\) either \(x\) and
    \(y\) are both positive or both negative.
\end{enumerate}

\begin{example}[{Proof}]
This is mostly a sketch of the proof.

\begin{description}
\item[Part (i)] Since \(w < x\), then \(w + y < x + y\) and since
    \(y < z\), \(x+y < x + z\). The result follows by
    transitivity.
\item[Part (ii)] Since \(y > 0\), \(wy < xy\) and since
    \(x > 0\), \(xy < yz\). The result follows by
    transitivity.
\item[Part (iii)] Since \(z < 0\), \(0 = z-z < -z\) and hence
    \(-zx < -zy\) and hence the result follows.
\item[Part (iv)] There are two cases. When \(x > 0\), the result follows by
    part (ii). Otherwise, when \(x < 0\) then by part (iii),
    \(xx = x^2 > 0\).
\item[Part (v)] Since \(1 = 1^2\), \(1 > 0\). Also, if
    \(n > 0\), \(n+1 > 0+ 1 > 0\). Then the result
    follows inductively.
\item[Part (vi)] Since \(x > 0\), \(\frac{1}{x} > 0\) (otherwise
    \(1 = x\frac{1}{x} < 0\)) and similarly
    \(\frac{1}{y} > 0\). Therefore,
    \(1 = x\frac{1}{x} < \frac{y}{x}\) which implies that
    \(\frac{1}{y} < \frac{1}{y}\frac{y}{x} = \frac{1}{x}\).
\item[Part (vii)] Note that if \(x\) or \(y\) are zero,
    \(xy = 0\). So, consider \(x > 0\), then
    \(y\) must be greater than \(0\) otherwise
    \(xy < 0\). On the other hand, if \(x < 0\), then
    \(y < 0\) otherwise \(xy < 0\).
\end{description}

 ◻
\end{example}

\subsection{Equality with zero}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---equality-with-zero}

Let \(\mathbb{F}\) be an ordered field and
\(x \in \mathbb{F}\). Then if \(x \geq 0\) and
\(x \leq \varepsilon\) \(\forall \epsilon > 0\),
\(x = 0\).

\begin{example}[{Proof}]
BWOC, suppose that \(x > 0\). Then, since \(2>0\),
\(\frac{1}{2} > 0\) and
\(\epsilon = \frac{x}{2} >0\). However, \(2x > x\)
implies that \(\epsilon = \frac{x}{2} < x\). This is a
contradiction and the result holds.

 ◻
\end{example}

\section{Bounds}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---bounds}

Let \(A \subset \mathbb{F}\) where
\(A \neq \varnothing\). Then

\begin{itemize}
\item \(u \in \mathbb{F}\) is called an \emph{upper bound} of
    \(A\) if \(u \geq a\) \(\forall a \in A\).
    If such a \(u\) exists then \(A\) is said to be
    \emph{bounded above}.
\item \(l \in \mathbb{F}\) is called a \emph{lower bound} of
    \(A\) if \(l \leq a\) \(\forall a \in A\).
    If such a \(l\) exists then \(A\) is said to be
    \emph{bounded below}.
\item \(A\) is said to be \emph{bounded} if it is both bounded above
    and bounded below.
\end{itemize}

\subsection{Supremum and Infimum}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---supremum-and-infimum}

\begin{itemize}
\item An element \(u \in \mathbb{F}\) is called the \emph{supremum} (or
    least upper bound) of \(A\) if \(u\) is an upper
    bound of \(A\) and \(u \leq u'\) forall other upper
    bounds \(u'\) of \(A\). In this case we write
    \(u = \sup A\).
\item An element \(l \in \mathbb{F}\) is called the \emph{infimum} (or
    greatest lower bound) of \(A\) if \(l\) is an upper
    bound of \(A\) and \(l \geq l'\) forall other upper
    bounds \(l'\) of \(A\). In this case we write
    \(l = \inf A\).
\end{itemize}

Then, we have the following theorem

\begin{equation*}
l = \inf A \iff -l = \sup (-A)\quad\text{where } -A = \{-a: a \in A\}
\end{equation*}

\begin{example}[{Proof}]
For the sake of brevity, we will let \(UB(X)\) and
\(LB(X)\) be the set of upper and lower bounds of
\(X\) (respectively). Then

\begin{equation*}
LB(X) = - UB(-X)
\end{equation*}

This is true since

\begin{equation*}
\begin{aligned}
        l \in LB(X)
        &\iff \forall x \in X: x \geq l\\
        &\iff \forall x \in X: -x \leq -l\\
        &\iff \forall x \in (-X): x \leq -l\\
        &\iff -l \in UB(-X)\\
        &\iff l \in - UB(-X)
    \end{aligned}
\end{equation*}

Additionally, notice that

\begin{equation*}
\begin{aligned}
        &\forall\, l' \in LB(X): l \geq l'\\
        &\iff \forall\, l' \in -UB(-X): l \geq l'\\
        &\iff \forall\, -l' \in UB(-X): l \geq l'\\
        &\iff \forall\, l' \in UB(-X): l \geq -l'\\
        &\iff \forall\, l' \in UB(-X): -l \leq l'
    \end{aligned}
\end{equation*}

Therefore, by combining these two results

\begin{equation*}
\begin{aligned}
        l = \sup A
        &\iff l \in LB(A) \;\land\; \forall\, l' \in LB(A): l \geq l'\\
        &\iff l \in -UB(-A) \;\land\; \forall\, l' \in UB(-A): -l \leq l'\\
        &\iff -l \in UB(-A) \;\land\; \forall\, u' \in UB(-A): -l \leq u'\\
        &\iff -l = \sup (-A)
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\subsection{Characterization of supremum and infimum}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---characterization-of-supremum-and-infimum}

The following statements are equivalent

\begin{enumerate}[label=\roman*)]
\item \(u = \sup A\)
\item \(u\) is an upper bound of \(A\) and
    \(\forall \varepsilon > 0: \exists x \in A: x > u-\varepsilon\)
\end{enumerate}

And in a similar manner the following statements are equivalent

\begin{enumerate}[label=\roman*), start=3]
\item \(l = \inf A\)
\item \(l\) is a lower bound of \(A\) and
    \(\forall \varepsilon > 0: \exists x \in A: x < u+\varepsilon\)
\end{enumerate}

Because of the relationship between suprema and infima,
\((iii)\iff (iv)\) follows from \((i) \iff (ii)\) as
follows

\begin{equation*}
\begin{aligned}
    l = \inf A
    &\iff -l = \sup(-A)\\
    &\iff -l \in UB(-A) \text{ and }\forall \varepsilon > 0: \exists x \in (-A): x > -l-\varepsilon\\
    &\iff l \in LB(A) \text{ and } \forall \varepsilon > 0: \exists x \in A: -x > -l-\varepsilon\\
    &\iff l \in LB(A) \text{ and } \forall \varepsilon > 0: \exists x \in A: x < l +\varepsilon\end{aligned}
\end{equation*}

As for the first part

\begin{example}[{Proof}]
We will prove \((i) \implies (ii)\) by contrapositive. The
contrapositive of \((ii)\) is

\begin{equation*}
u \text{ is not an upper bound of $A$ or} \exists \varepsilon > 0: \forall x \in A: x \leq u - \varepsilon
\end{equation*}

Then, if \(u\) is not an upper bound of \(A\), it is
clearly not the supremum of \(A\). On the other hand, if
\(\exists \epsilon > 0: \forall x \in A: x \leq u - \varepsilon\),
\(u -\varepsilon\) is an upper bound of \(A\) and
since \(u-\varepsilon < u\), \(u\) is not the
supremum of \(A\). Therefore, \((i) \implies (ii)\).

Now, we will prove that \((ii) \implies (i)\). Suppose that
\(u'\) is also an upper bound then, if \(u' < u\),
\(u - u' > 0\). By letting \(\varepsilon = u - u'\)
we get that \(\exists x \in A\) such that
\(x > u - \varepsilon = u - (u-u') = u'\) and hence
\(u'\) is not an upper bound. This is a contradiction and
hence \(u' \geq u\) and it follows that
\(u = \sup A\).

 ◻
\end{example}

\subsection{Maxima and Minima}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---maxima-and-minima}

Let \(A \subseteq \mathbb{F}\) then

\begin{itemize}
\item An element \(u \in A\) is called the \emph{maximum} of
    \(A\) if \(u \geq a\) \(\forall a \in A\).
    In this case we write \(u = \max A\).
\item An element \(l \in A\) is called the \emph{minimum} of
    \(A\) if \(l \leq a\) \(\forall a \in A\).
    In this case we write \(l = \min A\).
\end{itemize}

Furthermore, the following two sets of equivalences hold

\begin{equation*}
\max A \text{ exists}\iff \sup A = \max A \iff \sup A \in A
\end{equation*}

\begin{equation*}
\min A \text{ exists}\iff \inf A = \min A \iff \inf A \in A
\end{equation*}

\begin{example}[{Proof}]
Proof omitted because obvious.

 ◻
\end{example}

\subsection{Sum of suprema and infima}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---sum-of-suprema-and-infima}

Let \(A, B \subseteq \mathbb{F}\) then

\begin{itemize}
\item \(\sup A, \sup B \in \mathbb{F} \implies \sup(A + B) = \sup A + \sup B\)
\item \(\inf A, \inf B \in \mathbb{F} \implies \inf(A + B) = \inf A + \inf B\)
\end{itemize}

\begin{example}[{Proof}]
Again, the second statement follows from the first since
\(\inf A = - \sup(-A)\). Now, notice that
\(\sup A + \sup B\) is an upper bound for \(A + B\)
since

\begin{equation*}
x \in A + B \implies x = a + b \leq \sup A + \sup B
\end{equation*}

Now let \(\varepsilon > 0\) then
\(\frac{\varepsilon}{2} > 0\) and

\begin{equation*}
a \in A  \implies a > \sup A - \frac{\varepsilon}{2}
        \quad \text{and}\quad
        b \in B  \implies b > \sup B - \frac{\varepsilon}{2}
\end{equation*}

then

\begin{equation*}
x \in A + B \implies
        x = a + b  >
            \sup A - \frac{\varepsilon}{2} + \sup A - \frac{\varepsilon}{2}
            = (\sup A + \sup B) - \varepsilon
\end{equation*}

and by the characterization by suprema, the result follows.

 ◻
\end{example}

\subsection{Nice theorem}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---nice-theorem}

Let \(A, B \subseteq \mathbb{F}\) such that
\(\sup A\) and \(\inf B\) exists in
\(\mathbb{F}\) Then

\begin{equation*}
\forall a \in A: \forall b \in B: a \leq b
    \iff
    \sup A \leq \inf B
\end{equation*}

\begin{example}[{Proof}]
We use \(A - B = \{a -b : a \in A, b\in B\}\). Then

\begin{equation*}
\begin{aligned}
        &\forall a \in A: \forall b \in B: a \leq B\\
        &\iff \forall a \in A: \forall b \in B: a -b \leq 0 \\
        &\iff \forall x \in (A -B): x \leq 0 \\
        &\iff 0 \text{ is an upper bound for } A -B\\
        &\iff \sup(A - B ) \leq 0\\
        &\iff \sup(A  + (- B) ) \leq 0\\
        &\iff \sup(A) + \sup (- B) \leq 0\\
        &\iff \sup(A) \leq -\sup (- B) = \inf(B)
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\subsection{Additional nice results}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---additional-nice-results}

Let \(x \in \mathbb{F}\) and
\(A \subseteq \mathbb{F}\) such that \(\sup A\) and
\(\inf A\) exists in \(\mathbb{F}\) (where
applicable). Then

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[l,h] Q[l,h]}, measure=vbox}
{\begin{enumerate}[label=\roman*)]
\item \(\sup(x + A) = x + \sup A\)
\item If \(x > 0\), then \(\sup(xA) = x \sup(A)\)
\item If \(x < 0\), then \(\sup(xA) = x \inf(A)\)
\end{enumerate}} & {\begin{enumerate}[label=\roman*), start=4]
\item \(\inf(x + A) = x + \inf A\)
\item If \(x > 0\), then \(\inf(xA) = x \inf(A)\)
\item If \(x < 0\), then \(\inf(xA) = x \sup(A)\)
\end{enumerate}} \\
\end{tblr}
\end{table}

Note that \((i)\) and \((iv)\) follow from the sum
of suprema and infima. Additionally, \((iii)\),
\((v)\) and \((vi)\) will follow from
\((ii)\) using the correspondence between infima and suprema.

\begin{example}[{Proof}]
As said above, only a proof of \((ii)\) will be given.

Let \(x > 0\) and \(\sup A \in \mathbb{F}\) then
immediately, \(x\sup A\) is an upper bound for
\(\sup(xA)\) since

\begin{equation*}
\forall a \in A: a \leq \sup A
        \iff
        \forall a' = xa \in xA: a' = xa \leq x\sup A
\end{equation*}

by the monotonicity of multiplication. Now, consider
\(\varepsilon > 0\) then since \(x > 0\),
\(\frac{\varepsilon}{x} > 0\). Then by the characterization of
suprema

\begin{equation*}
\frac{\varepsilon}{x} > 0 \implies \exists a \in A: a > \sup A - \frac{\varepsilon}{x}
\end{equation*}

then \(xa > x\sup A - \varepsilon\) and since
\(xa \in xA\)

\begin{equation*}
\exists a' = xa \in xA: a' > x\sup A - \varepsilon
\end{equation*}

and hence the result follows.

 ◻
\end{example}

\section{Completeness}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---completeness}

An ordered field \(\mathbb{F}\) is \emph{complete} if every

\begin{itemize}
\item \(A \subseteq \mathbb{F}\) is bounded above
    \(\implies \sup(A) \in \mathbb{F}\).
\item \(A \subseteq \mathbb{F}\) is bounded below
    \(\implies \inf(A) \in \mathbb{F}\).
\end{itemize}

In, this section we will focus on the completeness of
\(\mathbb{R}\) which is complete either by construction or
axiomatically. In either case, the following theorem holds

There exists a unique ordered field \(\mathbb{R}\) with the
least upper bound property such that
\(\mathbb{Q} \subset \mathbb{R}\).

The existence of this field is guaranteed by Dedekind cuts (one possible
construction) however for uniqueness, I do not know.

\subsection{Archimedean Property in Reals}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---archimedean-property-in-reals}

The set \(\mathbb{N}\) of natural numbers is unbounded above
in \(\mathbb{R}\). The proof of is straightforward by
contradiction. Regardless the following statements are equivalent

\begin{enumerate}[label=\roman*)]
\item The Archimedean property holds.
\item \(\forall z \in \mathbb{R}: \exists n \in \mathbb{N}: n > z\).
\item \(\forall x \in \mathbb{R}^+, y \in \mathbb{R}: \exists n \in \mathbb{N}: nx > y\).
\item \(\forall x \in \mathbb{R}^+: \exists n \in \mathbb{N}: 0 < \tfrac{1}{n} < x\).
\end{enumerate}

\begin{example}[{Proof}]
Note that \((i)\) is logically equivalent to
\((ii)\) by the definition of upper bounds (or rather the
negation). Part \((iii)\) follows from and implies
\((ii)\) since \(\tfrac{y}{x} \in \mathbb{R}\).
Finally, \((iv)\) follows from \((iii)\) by using
\(y = 1\) and implies \((iv)\) since
\(\tfrac{1}{x} \in \mathbb{R}^+\) and all non-positive, values
of \(z\) (iii) trivially holds.

 ◻
\end{example}

\subsection{Denisty of the rationals in the reals}
\label{develop--math2277:ROOT:page--order-completeness-of-reals.adoc---denisty-of-the-rationals-in-the-reals}

If \(x , y \in \mathbb{R}\) such that \(x < y\),
then \(\exists r \in \mathbb{Q}\) such that
\(x < r < y\). It also follows that there are infinitely many
such \(r\).

\begin{example}[{Proof}]
The proof of this can be found under theorem 3.13 in Lay’s book.

 ◻
\end{example}

\begin{description}
\item[Corollary] It follows that \(\exists w \in \mathbb{Q}^c\) such that
    \(x < w < y\) by letting
    \(x\to \tfrac{x}{\sqrt{2}}\) and
    \(y \to \tfrac{y}{\sqrt{2}}\) and taking
    \(w = r\sqrt{2}\). Therefore the irrationals are also dense in
    \(\mathbb{R}\).
\end{description}
\end{document}
