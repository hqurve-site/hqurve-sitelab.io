\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Norms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
the following properties

\begin{description}
\item[Non-negativity] \(\forall \vec{x} \in X: \|\vec{x}\| \geq 0\)
\item[Coincidence] \(\forall \vec{x} \in X: \|\vec{x}\| = 0 \iff \vec{x} = \vec{0}\)
\item[Absolute homogeneity] \(\forall \lambda \in \mathbb{R }: \forall \vec{x} \in X: \|\lambda \vec{x}\| = |\lambda|\,\|\vec{x}\|\)
\item[Triangle inequality] \(\forall \vec{x}, \vec{y} \in X: \|\vec{x} + \vec{y}\| \leq \|\vec{x}\| + \|\vec{y}\|\)
\end{description}

Then, \((X, \|\cdot\|)\) is called a \emph{normed linear (vector) space}.

\begin{admonition-note}[{}]
For any inner product space we can define \(\|\vec{x}\| = \sqrt{\left\langle\vec{x},\vec{x}\right\rangle}\)
which would be a valid norm. Also, note that the inner product space could be hermitian.
\end{admonition-note}

\section{Metric induced by the norm}
\label{develop--math3277:metrics:page--norms.adoc---metric-induced-by-the-norm}

If \((X, \|\cdot\|)\) is a normed vector space, we can define

\begin{equation*}
d(\vec{x}, \vec{y}) = \|\vec{x} - \vec{y}\|
\end{equation*}

which would be a valid metric on \(X\). We then refer to \(d\) as the metric \emph{induced}
by the norm \(\|\cdot\|\).

\section{Norm on \(\mathbb{R}^n\)}
\label{develop--math3277:metrics:page--norms.adoc---norm-on-latex-backslash-latex-backslashmathbb-latex-openbracer-latex-closebrace-latex-caretn-latex-backslash}

Let \(k\) be a positive integer. Then we define
\(\|\cdot\|_k: \mathbb{R}^n \to \mathbb{R}\) by

\begin{equation*}
\|\vec{x}\|_k = \sqrt[k]{\sum_{i=1}^n |x_i|^k}
\end{equation*}

where \(\vec{x} = (x_1,\ldots x_n) \in \mathbb{R}^n\). Additionally,
some of these have special names

\begin{description}
\item[Taxi-cab] We call \(d_1\) the taxi-cab norm
\item[Euclidean norm] We call \(d_2\) the euclidean norm
\end{description}

Furthermore, we would show that \(\{\|\cdot\|_k\}\) is
a decreasing sequence with infimum \(\|\cdot\|_\infty\) defined by

\begin{equation*}
\|\vec{x}\| = \max_{1 \leq i \leq n} |x_i|
\end{equation*}

Notice that \(\|\cdot\|_\infty\) is also a norm.

\begin{example}[{Proof that \(\|\cdot\|_k\) is a norm}]
The first three properties are easy to see. So we would focus on the
triangle inequality. Consider arbitrary \(\vec{x}, \vec{y} \in \mathbb{R}^n\).
Then, by using the
\myautoref[{generalized Cauchy-Schwarz}]{develop--math3277:metrics:page--inequalities.adoc---generalized-cauchy-schwarz-inequality},

\begin{equation*}
\begin{aligned}
\|\vec{x} + \vec{y}\|_l
&= \sqrt[k]{\sum_{i=1}^n |x_i + y_i|^k}
\\&\leq \sqrt[k]{\sum_{i=1}^n (|x_i| + |y_i|)^k}
\\&= \sqrt[k]{\sum_{i=1}^n \sum_{j=0}^k \binom{k}{j} |x_i|^j |y_i|^{k-j}}
\\&= \sqrt[k]{\sum_{j=0}^k \binom{k}{j} \sum_{i=1}^n |x_i|^j |y_i|^{k-j}}
\\&\leq \sqrt[k]{\sum_{j=0}^k \binom{k}{j} \sqrt[k]{\left(\sum_{i=1}^n |x_i|^k\right)^j \left(\sum_{i=1}^n |y_i|^k\right)^{k-j}}}
    \\&\hspace{3em}\text{by generalized Cauchy-Schwarz where}
    \\&\hspace{4em}a_{i1} = \cdots =a_{ij} = |x_i|
    \\&\hspace{4em}a_{i(j+1)} = \cdots =a_{ik} = |y_i|
\\&= \sqrt[k]{\sum_{j=0}^k \binom{k}{j} \left(\sqrt[k]{\sum_{i=1}^n |x_i|^k}\right)^j \left(\sqrt[k]{\sum_{i=1}^n |y_i|^k}\right)^{k-j}}
\\&= \sqrt[k]{\sum_{j=0}^k \binom{k}{j} \left(\|\vec{x}\|_k\right)^j \left(\|\vec{y}\|_k\right)^{k-j}}
\\&= \sqrt[k]{\left(\|\vec{x}\|_k + \|\vec{y}\|_k\right)^{k}}
\\&= \|\vec{x}\|_k + \|\vec{y}\|_k
\end{aligned}
\end{equation*}

Now, suppose that we have equality. Then
for the first equality (\(|a + b| \leq |a| + |b|\)) to hold,
we require that \(x_iy_i \geq 0\) (ie both are either non-negative or non-positive).
Now, for the second equality to hold (generalized cauchy-schwarz), we have to consider
two cases

\begin{itemize}
\item If \(k = 1\), the inequality is actually equality. Hence,
    we only require that \(x_iy_i \geq 0\). This means that \(\vec{x}\) and \(\vec{y}\)
    must both lie in the same \(2^n\)'drant (\(n\) dimensional equivalent of quadrants).
\item If \(k > 1\), we can fix some \(0 < j < k\) and by using the conditions
    for the generalized cauchy-schwarz, we get equality iff \(|x_i| = cb_i\) and \(|y_i| = db_i\)
    for each \(i\). Now if \(c=0\) or \(d = 0\), we get that \(\vec{x} = \vec{0}\)
    or \(\vec{y} = \vec{0}\). Otherwise we could write \(|x_i| = e|y_i|\)
    and since \(x_i\) and \(y_i\) have the same sign for each \(i\), \(x_i = ey_i\)
    and hence \(\vec{x} = e\vec{y}\) where. Hence we get equality iff \(\vec{x}\)
    and \(\vec{y}\) '`point in the same direction''.
\end{itemize}
\end{example}

\begin{example}[{Proof that \(\|\cdot\|_\infty\) is a norm}]
Again, the first three properties clearly hold. So for the triangle inequality
notice that for all \(i= 1,\ldots n\)

\begin{equation*}
|x_i + y_i| \leq |x_i| + |y_i|
\leq \max_{1\leq j \leq n}|x_j| + \max_{1 \leq j \leq n} |y_j|
= \|\vec{x}\|_\infty + \|\vec{y}\|_\infty
\end{equation*}

And hence \(\|\vec{x}\|_\infty + \|\vec{y}\|_\infty\) is an upper bound for
\(|x_i + y_i|\) and

\begin{equation*}
\|\vec{x} + \vec{y}\|_\infty
= \max_{1 \leq i \leq n} |x_i + y_i|
\leq \max_{1 \leq i \leq n} (\|\vec{x}\|_\infty + \|\vec{y}\|_\infty)
= \|\vec{x}\|_\infty + \|\vec{y}\|_\infty
\end{equation*}
\end{example}

\begin{example}[{Proof that \(\{\|\cdot\|_k\}\) is a decreasing sequence}]
By \myautoref[{lemma 2}]{develop--math3277:metrics:page--inequalities.adoc---lemma-2}, this is the case. Furthermore,
equality holds iff at most one of the \(|x_i| \neq 0\). That is
\(\vec{x} = k\vec{e}_i\) for some \(i\).
\end{example}

\begin{example}[{Proof that \(\|\cdot\|_\infty\) is a lower bound of \(\{\|\cdot\|\}\)}]
Consider arbitrary \(\vec{x}\in \mathbb{R}^n\) and WLOG
suppose that \(\|\vec{x}\|_\infty = |x_1|\). Then,

\begin{equation*}
\|\vec{x}\|_k
= \sqrt[k]{\sum_{i=1}^n |x_i|^k}
\geq \sqrt[k]{|x_1|^k}
= |x_1|
= \|\vec{x}\|_\infty
\end{equation*}
\end{example}

\begin{example}[{Proof that \(\{\|\cdot\|_k\}\) converges to \(\|\cdot\|_\infty\)}]
Firstly, consider the following bounds

\begin{equation*}
\|\vec{x}\|_\infty
\leq \|\vec{x}\|_k
= \sqrt[k]{\sum_{i=1}^n |x_i|^k}
\leq \sqrt[k]{\sum_{i=1}^n \|\vec{x}\|_\infty^k}
= \|\vec{x}\|_\infty \sqrt[k]{n}
\end{equation*}

Therefore,

\begin{equation*}
\left|\|\vec{x}\|_k - \|\vec{x}\|_\infty  \right|
\leq \|\vec{x}\|_\infty(\sqrt[k]{n} -1)
\end{equation*}

And notice that \(\sqrt[k]{n} - 1 \to \infty\) as \(k\to \infty\) since
for any \(\varepsilon > 0\),

\begin{equation*}
k > \frac{n-1}{\varepsilon}
\implies n < 1+k\varepsilon < (1+\varepsilon)^k
\implies \sqrt[k]{n} < \varepsilon + 1
\implies \sqrt[k]{n} -1 < \varepsilon
\end{equation*}

Now, if \(\vec{x}\) is fixed, then \(\|\vec{x}\|_k \to \|\vec{x}\|_\infty\)
as \(k\to\infty\). That is \(\{\|\cdot\|\}\) converges pointwise to \(\|\cdot\|_\infty\).
Furthermore if \(\vec{x} \in A \subseteq \mathbb{R}^n\)
where \(\|\cdot\|_\infty\) is bounded on \(A\) (ie \(A\) is subset of a box),
we get that \(\{\|\cdot\|_k\}\)
converges uniformly to \(\|\cdot\|_\infty\) on \(A\).
\end{example}

\section{Norm on \(\mathcal{C}[a,b]\)}
\label{develop--math3277:metrics:page--norms.adoc---norm-on-latex-backslash-latex-backslashmathcal-latex-openbracec-latex-closebraceab-latex-backslash}

Consider the set of continuous function \(\mathcal{C}[a,b]\).
Then the function \(\|\cdot\|_k: \mathcal{C}[a,b] \to \mathbb{R}\)
defined by

\begin{equation*}
\|f\|_k = \sqrt[k]{\int_a^b |f(x)|^k \ dx}
\end{equation*}

and likewise to the norm on \(\mathbb{R}^n\), we define \(\|\cdot\|_\infty\)
as

\begin{equation*}
\|f\|_\infty = \sup_{x\in [a,b]} |f(x)|
\end{equation*}

\section{Norm on \(\mathcal{l}_p\)}
\label{develop--math3277:metrics:page--norms.adoc---norm-on-latex-backslash-latex-backslashmathcal-latex-openbracel-latex-closebrace-latex-underscorep-latex-backslash}

Let \(p \in [1,\infty)\) and define

\begin{equation*}
l_p = \left\{x = \{x_i\}: \sum_{i=1}^\infty |x_i|^p < \infty\right\}
\end{equation*}

and we define \(\|\cdot\|_p: l_p \to \mathbb{R}\) as

\begin{equation*}
\|\{x_i\}\|_p = \sqrt[p]{\sum_{i=1}^\infty |x_i|^p}
\end{equation*}

Furthermore, we define

\begin{equation*}
l_\infty = \{x =\{x_i\}: \sup_{1 \leq i} |x_i| < \infty \}
\end{equation*}

and we define \(\|\cdot\|_\infty: l_\infty \to \mathbb{R}\) as

\begin{equation*}
\|\{x_i\}\|_\infty = \sup_{1 \leq i} |x_i|
\end{equation*}
\end{document}
