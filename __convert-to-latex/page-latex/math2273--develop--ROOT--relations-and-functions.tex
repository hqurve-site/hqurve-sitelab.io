\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Relations and functions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Relation definition}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---relation-definition}

A set \(R\) is a \emph{relation} between two sets \(A\)
and \(B\) iff \(R \subseteq A \times B\). In this
case the set \(A\) is referred to as the \emph{domain} of the
relation while the set \(B\) is called the \emph{codomain} of the
relation. Additionally, if \(A = B\), then the relation
\(R \subseteq A^2\) is said to be a relation on
\(A\).

\begin{admonition-note}[{}]
An element \(x \in A\) is related by \(R\) to
\(y \in B\) (denoted \(xRy\)) iff
\((x, y) \in R\).
\end{admonition-note}

\section{Equivalence relation}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---equivalence-relation}

A relation \(R\) on a set \(A\) is an \emph{equivalence
relation} iff it has the following properties
\(\forall x, y, z \in A\).

\begin{enumerate}[label=\arabic*)]
\item reflexive: \(xRx\)
\item symmetric: \(xRy \implies yRx\)
\item transitive: \(xRy \wedge yRz \implies xRz\)
\end{enumerate}

\section{Equivalence classes}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---equivalence-classes}

If \(R\) is an equivalence relation, the \emph{equivalence class}
(with respect to \(R\)) of \(x \in A\) is defined by

\begin{equation*}
[x] = \{y \in A: yRx\}
\end{equation*}

\subsection{Notes}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---notes}

\begin{itemize}
\item \(x\) belongs to its own equivalence class (due to the
    reflexive property)
\item Two different equivalence classes are disjoint (due to the symmetric
    and transitive property)
\end{itemize}

\section{Function definition}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---function-definition}

A \emph{function} from \(A\) to \(B\) is a non-empty
relation (st \(f \subseteq A \times B\)) that satisfies the
following

\begin{enumerate}[label=\arabic*)]
\item Existence: \(\forall x \in A, \exists y \in B\) such that
    \((x, y) \in f\)
\item Uniqueness: \((x, y) \in f\) and \((x, z) \in f\)
    \(\implies y = z\)
\end{enumerate}

\subsection{Notes}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---notes-2}

\begin{itemize}
\item we write \(f: A \rightarrow B\) to indicate that
    \(f\) has domain \(A\) and codomain \(B\).
\item If \((x, y) \in f\) we say \(f\) maps
    \(x\) onto \(y\), or that the image of
    \(x\) under \(f\) is \(y\). Additionally,
    instead of writing \((x,y) \in f\), we write
    \(f(x) = y\)
\end{itemize}

\section{Image and pre-image}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---image-and-pre-image}

Consider the sets \(A\) and \(B\) and function
\(f: A \rightarrow B\). Then

\begin{itemize}
\item The \emph{image} of \(C \subseteq A\) in \(B\) is given
    by \(f(C) = \{y \in B: y = f(x) \text{ for some } x\in C\}\)
\item The \emph{pre-image} of \(D \subseteq B\) in \(A\) is
    given by \(f^{-1}(D) = \{x \in A: f(x) \in D\}\)
\end{itemize}

\begin{admonition-note}[{}]
If \(C = A\) then \(f(C) = f(A)\) is called
the \emph{range} of the function \(f\) (also denoted
\(Ran f\)).
\end{admonition-note}

\section{Injections, surjections and bijections}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---injections-surjections-and-bijections}

A function \(f: A \rightarrow B\) is said to be

\begin{itemize}
\item injective (one-to-one) if
    
    \begin{equation*}
    \forall x_1, x_2 \in A: x_1 \neq x_2 \implies f(x_1) \neq f(x_2)
    \end{equation*}
    
    \begin{admonition-note}[{}]
    It is sometimes easier to prove the contrapositive
    \(f(x_1) = f(x_2) \implies x_1 = x_2\)
    \end{admonition-note}
\item surjective (onto) if \(f(A) = B\)
    
    \begin{admonition-note}[{}]
    It is usually simpler to prove
    \(\forall y \in B: \exists x\in A\) such that
    \(f(x) = y\)
    \end{admonition-note}
\item bijective if \(f\) is both an injection and surjection
\end{itemize}

\section{Some theorems}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---some-theorems}

Suppose \(f: A \rightarrow B\) and
\(C, C_1, C_2 \subseteq A\). Then

\begin{enumerate}[label=\arabic*)]
\item \(f(\varnothing) = \varnothing\)
\item \(C_1 \subseteq C_2 \implies f(C_1) \subseteq f(C_2)\)
    
    \begin{example}[{Proof}]
    \begin{equation*}
    \begin{aligned}
                    y \in f(C_1)
                    &\implies \exists x \in C_1 \text{ such that } f(x) = y\\
                    &\implies \exists x \in C_2 \text{ such that } f(x) = y\\
                    &\implies y \in f(C_2)
                \end{aligned}
    \end{equation*}
    
     ◻
    \end{example}
\item \(C \subseteq f^{-1}[f(C)]\)
    
    \begin{example}[{Proof}]
    \(x \in C \implies f(x) \in f(C) \iff x \in f^{-1}[f(C)]\) ◻
    
    \begin{admonition-note}[{}]
    The first step is reversible if \(f\) is injective.
    \end{admonition-note}
    \end{example}
\item \(f[f^{-1}(D)] \subseteq D\)
    
    \begin{example}[{Proof}]
    \begin{equation*}
    \begin{aligned}
                    y \in f[f^{-1}(D)]
                    &\implies \exists x \in f^{-1}(D): f(x) = y\\
                    &\implies \exists x \in f^{-1}(D): f(x) = y \wedge f(x) \in D
                            \quad (\text{since } x \in f^{-1}(D) \equiv f(x) \in D)\\
                    &\implies y \in D
                \end{aligned}
    \end{equation*}
    
     ◻
    \end{example}
\item \(f(C_1 \cap C_2) \subseteq f(C_1) \cap f(C_2)\)
    
    \begin{example}[{Proof}]
    \begin{equation*}
    \begin{aligned}
                    y \in f(C_1 \cap C_2)
                    &\iff \exists x \in (C_1 \cap C_2): f(x) = y\\
                    &\iff
                        \left(\exists x \in (C_1 \cap C_2): f(x) = y\right) \wedge
                        \left(\exists x \in (C_1 \cap C_2): f(x) = y\right)\\
                    &\implies
                        \left(\exists x \in C_1: f(x) = y\right) \wedge
                        \left(\exists x \in C_2: f(x) = y\right)\\
                    &\iff y\in f(C_1) \wedge y\in f(C_2)\\
                    &\iff y\in (f(C_1) \cap f(C_2))
                \end{aligned}
    \end{equation*}
    
     ◻
    \end{example}
\item \(f(C_1 \cup C_2) = f(C_1) \cup f(C_2)\)
    
    \begin{example}[{Proof}]
    \begin{equation*}
    \begin{aligned}
                    y \in f(C_1 \cup C_2)
                    &\iff \exists x\in (C_1 \cup C_2): f(x) = y\\
                    &\iff (\exists x \in C_1: f(x) = y) \vee (\exists x \in C_2: f(x) = y)\\
                    &\iff y\in f(C_1) \vee y\in f(C_2)\\
                    &\iff y\in (f(C_1) \cup f(C_2))
                \end{aligned}
    \end{equation*}
    
     ◻
    \end{example}
\item If \(f\) is injective, then
    \(f(C_1 \cap C_2) = f(C_1) \cap f(C_2)\)
    
    \begin{example}[{Proof}]
    Same as in part (v) above, however the one non-reversible
    implication is now reversible. This is because \(x\) is unique
    (due to injectivity) and hence must be present in both \(C_1\)
    and \(C_2\). ◻
    \end{example}
\end{enumerate}

\section{Function equality}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---function-equality}

Two functions \(f: A \rightarrow B\) adn
\(g: A \rightarrow B\) are said to be \emph{equal} (written as
\(f = g\)) if

\begin{equation*}
\forall x \in A: f(x) = g(x)
\end{equation*}

Note: function equality is an equivalence relation. ie, reflexive,
symmetric, transitive.

\section{Function composition}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---function-composition}

If \(f: A \rightarrow B\) and
\(g: B \rightarrow C\), then the composition of
\(f\) and \(g\) (denoted \(g \circ f\) or
just \(gf\)) is defined by

\begin{equation*}
(g \circ f)(x) = g(f(x)) \forall x \in A
\end{equation*}

Alternatively

\begin{equation*}
g \circ f = \{(x, z): x \in A, z \in C, \exists y \in B: (x, y) \in f \wedge (y, z) \in g\}
\end{equation*}

Note: that the composition of functions is not commutative

\subsection{Equivalence and inspection of definitions}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---equivalence-and-inspection-of-definitions}

We shall walk through the process of the evaluation of
\((g \circ f)(x)\).

Consider \(x \in A\). Then there exists a unique
\(y \in B\) such that \((x, y) \in f\) or
\(f(x) = y\). Next there exists a unique \(z \in C\)
such that \((y, z) \in g\) or
\(g(y) = g(f(x)) = z\). Therefore, the two above definitions
are equivalent and well defined.

It should be additionally noted that both in the above proof and the set
builder notation above, the existential quantifier always evaluates once
for a unique \(y\) and \(z\)

\subsection{Function composition and function equality}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---function-composition-and-function-equality}

Consider
\(x\in A \implies g_1(x) = g_2(x) \implies f_1(g_1(x)) = f_2(g_2(x))\)

\subsection{The identity function}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---the-identity-function}

The \emph{identity} function (map) on a set \(A\) is the function
\(I_A: A \rightarrow A\) defined by

\begin{equation*}
I_A(x) = x \quad \forall x \in A
\end{equation*}

As its name suggests, the identity function is the identity for function
composition. That is

\begin{equation*}
f \circ I_A = I_B \circ f = f
\end{equation*}

Note however, the left and right identities are not necessarily the same

\subsection{Some theorems}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---some-theorems-2}

Consider the functions \(f: A \rightarrow B\),
\(g: B \rightarrow C\) and \(h: C \rightarrow D\).
Then the following are true

\begin{itemize}
\item \((f \circ g) \circ h = f \circ (g \circ h)\)
\item If \(f\) and \(g\) are injective, then \(g \circ f\) is injective
    
    \begin{example}[{Proof}]
    \begin{equation*}
    \begin{aligned}
    (f \circ g)(x_1) = (f \circ g)(x_2)
                &\implies f(g(x_1)) = f(g(x_2))
                \\&\implies g(x_1) = g(x_2)
                \\&\implies x_1 = x_2
    \end{aligned}
    \end{equation*}
    \end{example}
\item If \(f\) and \(g\) are surjective, then
    \(g \circ f\) is surjective
    
    \begin{example}[{Proof}]
    \begin{equation*}
    \begin{aligned}
    &[\forall y \in C:  \exists x' \in B \text{ st } g(x') = y]
                \\&\implies
                [\forall y \in C:
                    \exists x' \in B \text{ st } g(x') = y \wedge \exists x \in A \text{ st } f(x) = x'
                ]
                \\&\implies
                [\forall y \in C: \exists x \in A \text{ st } gf(x) = y]
    \end{aligned}
    \end{equation*}
    \end{example}
\item If \(f\) and \(g\) are bijective, then
    \(g \circ f\) is bijective
\end{itemize}

\section{Inverse functions}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---inverse-functions}

Consider a function \(f: A \rightarrow B\). Then the following
statements are equivalent

\begin{enumerate}[label=\roman*)]
\item \(f\) is bijective
\item There exists a function \(f^{-1}: B \rightarrow A\), called
    the \emph{inverse} of \(f\) such that
    
    \begin{equation*}
    f^{-1}\circ f = I_A \text{ and } f \circ f^{-1} = I_B
    \end{equation*}
\end{enumerate}

\begin{example}[{Proof}]
\begin{example}[{(i) \(\rightarrow\) (ii)}]
We will prove this by showing the following relation is a
function then show that it satisfies the necessary conditions

\begin{equation*}
f^{-1} = \{(y, x) : (x, y) \in f \}
\end{equation*}

Note, this is equivalent to
\((x, y) \in f  \iff  (y, x) \in f^{-1}\)

\begin{itemize}
\item Existence:
    \(y \in B \implies \exists x \in A: (x, y) \in f\) (since
    \(f\) is surjective) and hence \((y, x) \in f^{-1}\)
\item Uniqueness: \((y_1, x) \in f^{-1}\) and
    \((y_2, x) \in f^{-1}\) \(\implies\)
    \((x, y_1) \in f\) and \((x, y_2) \in f\)
    \(\implies\) \(y_1 = y_2\) (since \(f\) is
    injective)
\end{itemize}

Now we know \(f^{-1}\) is a function and need to show the
necessary properties.

\begin{itemize}
\item Consider, \(x \in A\). Therefore,
    \(\exists y \in B\) such that \((x,y) \in f\). Also,
    note that \((y, x) \in f^{-1}\). Therefore,
    \(f(x) = y\) and \(f^{-1}(y) = x\) and hence
    \((f^{-1} \circ f)(x) = f^{-1}(f(x)) = f^{-1}(y) = x\)\newline
    Since we considered an arbitrary \(x\),
    \(\forall x \in A: (f^{-1} \circ f)(x) = x = I_A(x)\).\newline
    Therefore, \(f^{-1} \circ f = I_A\)
\item Consider, \(y \in B\). Therefore,
    \(\exists x \in A\) such that \((y, x) \in f^{-1}\).
    Also, note that \((x, y) \in f\). Therefore,
    \(f^{-1}(y) = x\) and \(f(x) = y\) and hence
    \((f \circ f^{-1})(y) = f(f^{-1}(y)) = f(x) = y\)\newline
    Since we considered an arbitrary \(y\),
    \(\forall y \in B: (f^{-1} \circ f)(y) = y = I_B(y)\).\newline
    Therefore, \(f \circ f^{-1} = I_B\)
\end{itemize}

 ◻
\end{example}

\begin{example}[{(ii) \(\rightarrow\) (i)}]
.

\begin{itemize}
\item injectivity: \(f(x_1) = f(x_2)
    \implies I_A(x_1) = f^{-1}(f(x_1)) = f^{-1}(f(x_2)) = I_A(x_2)
    \implies x_1 = x_2\)
\item surjectivity: \(\forall y \in B: \exists x \in A\) (namely
    \(x = f^{-1}(y)\)) such that
    \(f(x) = f(f^{-1}(y)) = I_B(y) = y\)
\end{itemize}

 ◻
\end{example}
\end{example}

\subsection{Uniqueness of the inverse function}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---uniqueness-of-the-inverse-function}

\begin{example}[{Proof}]
Consider two functions \(g_1: A\rightarrow B\) and
\(g_2: A\rightarrow B\) that satisfy (ii).

\begin{equation*}
\begin{aligned}
        \text{Therefore: } f \circ g_1 = I_B = f \circ g_2
        &\implies g_1 \circ (f \circ g_1) = g_1 \circ (f \circ g_2)\\
        &\implies (g_1 \circ f) \circ g_1 = (g_1 \circ f) \circ g_2\\
        &\implies I_B \circ g_1 = I_B \circ g_2\\
        &\implies g_1 = g_2
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\subsection{Inverse of composed function}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---inverse-of-composed-function}

Consider two bijective functions \(f: A\rightarrow B\) and
\(g: B \rightarrow C\). Then \(g \circ f\) is
bijective and

\begin{equation*}
(g \circ f)^{-1} = f^{-1} \circ g^{-1}
\end{equation*}

\begin{example}[{Proof}]
Right inverse

\begin{equation*}
\begin{aligned}
        (g \circ f) \circ f^{-1} \circ g^{-1}
        &= g \circ (f \circ f^{-1}) \circ g^{-1}\\
        &= g \circ I_B \circ g^{-1}\\
        &= g \circ g^{-1}\\
        &= I_C\\
    \end{aligned}
\end{equation*}

Also, left inverse

\begin{equation*}
\begin{aligned}
        (f^{-1} \circ g^{-1}) \circ (g \circ f)
        &= f^{-1} \circ (g^{-1} \circ g) \circ f\\
        &= f^{-1} \circ I_B \circ f\\
        &= f^{-1} \circ f\\
        &= I_A
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\section{Binary operations}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---binary-operations}

A \emph{binary operation} on a set \(A\) is a function from
\(A \times A\) to \(A\).

Note, binary operations are usually denoted by special symbols rather
than letters. Additionally, binary operators are usually written using
infix notation instead of prefix. That is, with the binary operation
\(\oplus: A \times A \rightarrow A\), we write
\(a \oplus b\) instead of \(\oplus(a, b)\).

\section{Exercise}
\label{develop--math2273:ROOT:page--relations-and-functions.adoc---exercise}

Waste of my time (answers are trivial)
\end{document}
