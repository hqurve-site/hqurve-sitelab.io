\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Dedekind cuts}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
Let \(C \subseteq \mathbb{Q}\). Then \(C\) is called a \emph{cut} if

\begin{itemize}
\item \(C \neq \varnothing\) and \(C \neq \mathbb{Q}\)
\item No first element: \(\forall c \in C: \forall a \in \mathbb{Q}: a < c \implies a \in C\)
\item No last element: \(\forall c \in C: \exists b \in C: b > c\)
\end{itemize}

\section{Nice properties}
\label{develop--math3274:numbers:page--dedekind.adoc---nice-properties}

\subsection{Characterization of inclusion}
\label{develop--math3274:numbers:page--dedekind.adoc---characterization-of-inclusion}

Let \(C\) be a cut. Then,

\begin{equation*}
x \in C \iff [ \exists c \in C: x < c ]
\end{equation*}

This is clearly true and may seem insignificant, however, notice that this implies that

\begin{equation*}
C' = \{x \in \mathbb{Q}: \forall c \in C: x \geq c\} =\{x \in \mathbb{Q}: \forall c \in C: x > c\}
\end{equation*}

Notice that the second two sets are equal since if \(x = c\) for any \(c \in C\) there exists
\(c_1 > c \in C\) such that \(x \geq c_1 > c\); which is a contradiction.

Additionally, \(C\) is precisely the set of elements bounded by \(C'\). That is

\begin{equation*}
C = \{x \in \mathbb{Q}: \forall d \in C': c < d\} = C_1
\end{equation*}

Then, we have that \(C \subseteq C_1\). Also,
notice that \(x \in C_1\) implies that \(x \notin C'\);
hence \(C_1 \subseteq \mathbb{Q}\backslash C' = C\). Therefore, we get equality.

\subsection{Stradling the border}
\label{develop--math3274:numbers:page--dedekind.adoc---stradling-the-border}

Let \(C\) be a cut and \(r \in \mathbb{Q}^+ \). Then, there exists \(c \in C\)
such that \(c + r \notin C\).

Furthermore, if \(r > 1\) and \(C\) is positive, then \(\exists c \in C\)
such that \(cr \notin C\). Equivalently, \(C < D\).

\begin{example}[{Proof}]
\begin{example}[{Proof of first}]
Suppose BWOC that the opposite is true. That is,

\begin{equation*}
\forall c \in C: c + r \in C
\end{equation*}

Now, we would show that \(C = \mathbb{Q}\). Consider arbitrary
\(q \in \mathbb{Q}\) and focus on \(c \in C\). If \(q \leq c\), since \(C\) is a cut
\(q \in C\). On the other hand if \(q > c\), then \(\frac{q-c}{r} > 0\).
Let \(\frac{q-c}{r} = \frac{A}{B}\) where \(A\) and \(B\)
are both positive. Also let \(N = AB\).
Then,

\begin{equation*}
c + Nr = c + ABr > c + \frac{A}{B}r = c + (q-c) = q
\end{equation*}

Now, by induction, we know that \(c + Nr \in C\). Then,
since \(C\) is a cut, \(q \in C\). Therefore \(C = \mathbb{Q}\),
contradiction.

\begin{admonition-remark}[{}]
We could have just used the archemedian property, however, I haven't
proven it here as yet.
\end{admonition-remark}
\end{example}

\begin{example}[{Proof of second}]
Suppose BWOC that the opposite is true. That is,

\begin{equation*}
\forall c \in C: cr \in C
\end{equation*}

Then, consider \(c > 0\) and notice that

\begin{equation*}
cr^n = c(1+s)^n = c \sum_{k=0}^n \binom{n}{k}s^n > c (1 + ns)
\end{equation*}

where \(s = r-1 > 0\). Then since \(c \in C\),
\(cr^n \in C\) and is unbounded. Hence, we get a contradiction.
\end{example}
\end{example}

\subsection{Shifting the cut}
\label{develop--math3274:numbers:page--dedekind.adoc---shifting-the-cut}

Let \(C\) be a cut and \(r \in \mathbb{Q}\). Then,

\begin{equation*}
D = \{c + r: c \in C\}
\end{equation*}

is a cut and \(D' = \{c' + r: c \notin C\}\). Furthermore,

\begin{equation*}
D = C + C(r)
\end{equation*}

\begin{example}[{Proof}]
Notice that if we would show that \(D = C + C(r)\), \(D\) is automatically a cut

\begin{example}[{Proof of addition}]
Firstly

\begin{equation*}
C + C(r) = \{c_1 + c_2: c_1 \in C, c_2 < r\}
\end{equation*}

Now, consider \(x = c + r \in D\) and \(c < c_1 \in C\). Then,
\(c - c_1 < 0\) and \(c - c_1 + r < r\). Then

\begin{equation*}
x = c + r = c_1 + (c - c_1) + r = c_1 + (c - c_1 + r) \in C + C(r)
\end{equation*}

Therefore, \(D \subseteq C + C(r)\).

Conversely, we consider \(x = c_1 + c_2 \in C + C(r)\).
Then, \(c_2 < r\) and hence \(c_1 + c_2 - r < c_1\) and hence
\(c_1 + c_2 - r \in C\). Therefore,

\begin{equation*}
x = c_1 + c_2 = (c_1 + c_2 - r) + r \in D
\end{equation*}

Therefore \(C + C(r) \subseteq D\) and we are done.
\end{example}

\begin{example}[{Proof of form of complement}]
We would use characterization of exclusion. Let

\begin{equation*}
E = \{c' + r: c' \notin C\}
\end{equation*}

Now, consider \(c' + r \in E\). Then, since \(c' > c\) for all \(c \in C\),
\(c' + r > d\) for all \(d \in D\) and hence \(c' + r \notin D\).
Therefore, \(E \subseteq D'\)

Conversely, consider \(d' \notin D\), then \(d' > d\) for all
\(d \in D\). That is

\begin{equation*}
\forall c + r \in D: d' > c+r
\end{equation*}

Therefore, \(d' - r > c\) for all \(c \in C\) and hence,
\(d' -r \notin C\). Therefore, \(d' = (d' - r) + r \in E\)
and \(D' \subseteq E\) and we are done.
\end{example}
\end{example}

\subsection{Expanding the cut}
\label{develop--math3274:numbers:page--dedekind.adoc---expanding-the-cut}

Let \(C\) be a cut and \(r \in \mathbb{Q}^+ \).
Then,

\begin{equation*}
D = \{cr: c\in C\}
\end{equation*}

is a cut and \(D' = \{c'r : c \notin C\}\). Furthermore,

\begin{equation*}
D = C \cdot C(r)
\end{equation*}

\begin{example}[{Proof}]
\begin{example}[{Proof of cut}]
Clearly, \(D \neq \varnothing\). Also, \(D \neq \mathbb{Q}\) since
if \(e \notin C\), \(e > c\) for all \(c \in C\)
hence \(er > cr\) for all \(cr \in D\) and hence,
\(er\) is not in \(D\).

Next, if \(x < cr\) we get that \(\frac{x}{r} < c\) and
we get that \(x = \frac{x}{r} r \in D\) since \(C\) is a cut.
Also, since \(C\) is a cut, there exists \(c_1 > c\) where
\(c_1 \in C\) and hence \(c_1r > cr\) and \(c_1r \in D\).

Therefore, \(D\) is a cut.
\end{example}

\begin{example}[{Proof of mutliplication}]
Lets consider the case where \(C > 0\) firstly. Then, consider \(cr \in D\).
If \(cr \leq 0\), we know it lies in \(C \cdot C(r)\).
Otherwise, there exists \(e > c\) where \(e \in C\) and hence, \(\frac{c}{e}r < r\)
and we get that

\begin{equation*}
cr = e \cdot \frac{cr}{e} \in C \cdot C(r)
\end{equation*}

Therefore, \(D \subseteq C \cdot C(r)\).

Otherwise, consider \(x \in C \cdot C(r)\). Then, if \(x \leq 0\),
we again know that it lies in \(D\) since \(0 \in D\). Otherwise,
\(x = c e\) where \(c \in C^+ \) and \(0 < e < r\).
Then, \(\frac{ce}{r} < c\) and hence \(\frac{ce}{r} \in C\)
and

\begin{equation*}
x = ce = \frac{ce}{r}r \in D
\end{equation*}

Therefore \(C \cdot C(r) \subseteq D\) and we are done.

If \(C < 0\), let

\begin{equation*}
E = \{dr: d \in (-C)\}
\end{equation*}

Then, \((-C)\cdot C(r) = E\). Also, notice
that

\begin{equation*}
E + D = \{(c + d)r: c \in C, d \in (-C)\} = \{f r: f \in C(0)\} = \{fr: f < 0\}
\end{equation*}

Then clearly, \(E + D \subseteq C(0)\) also if \(x \in C(0)\),
\(\frac{x}{r}r \in E + D\). Therefore \(E + D = C(0)\).
Then, we get that

\begin{equation*}
C \cdot C(r)
= C \cdot C(r) + (-C)\cdot C(r) + (-E)
= (C + (-C)) \cdot C(r) + (-E)
= C(0) \cdot C(r) + (-E)
= (-E)
= D
\end{equation*}
\end{example}

\begin{example}[{Proof of form of complement}]
We would use characterization of exclusion. Let

\begin{equation*}
E = \{c'r: c' \notin C\}
\end{equation*}

Now, consider \(c'r \in E\). Then, since \(c' > c\) for all \(c \in C\),
\(c'r > cr\) for all \(cr \in D\) and hence \(c'r \notin D\).
Therefore, \(E \subseteq D'\)

Conversely, consider \(d' \notin D\), then \(d' > d\) for all
\(d \in D\). That is

\begin{equation*}
\forall cr \in D: d' > cr
\end{equation*}

Therefore, \(\frac{d'}{r} > c\) for all \(c \in C\) and hence,
\(\frac{d'}{r} \notin C\). Therefore, \(d' = \frac{d'}{r}r \in E\)
and \(D' \subseteq E\) and we are done.
\end{example}
\end{example}

\section{Ordering of cuts}
\label{develop--math3274:numbers:page--dedekind.adoc---ordering-of-cuts}

Let \(R\) be our set of cuts. Note that such a set is guaranteed by the axiom of powers and specification.
Then if \(C \in R\), exactly one of the following hold

\begin{description}
\item[Positive] \(\exists c \in C: c > 0\)
\item[Zero] \(C = C(0)\)
\item[Negative] \(-C\) is positive
\end{description}

Then, we let

\begin{equation*}
R^+ = \{C \in R: C \text{ is positive}\}
\quad\text{and}\quad
R^- = \{C \in R: C \text{ is negative}\}
\end{equation*}

In general if \(C_1\) and \(C_2\) are cuts, then

\begin{equation*}
C_1 < C_2 \iff C_2 - C_1 \text{ is positive} \iff C_1 \subset C_2
\end{equation*}

\section{Operations}
\label{develop--math3274:numbers:page--dedekind.adoc---operations}

\subsection{Postive segment}
\label{develop--math3274:numbers:page--dedekind.adoc---postive-segment}

Let \(C\) be a positive cut. Then we define

\begin{equation*}
C^+ = \{c \in C: c > 0\}
\end{equation*}

Then,

\begin{equation*}
C = \mathbb{Q}^- \cup \{0\} \cup C^+ = C(0) \cup C^+
\end{equation*}

\subsection{Addition}
\label{develop--math3274:numbers:page--dedekind.adoc---addition}

Let \(C_1, C_2\) be cuts. Then

\begin{equation*}
C_1 + C_2 = \{c_1 + c_2: c_1 \in C_1, c_2 \in C_2\}
\end{equation*}

is a cut. Additionally,

\begin{itemize}
\item \(+ \) is commutative and associative
\item \(C(0)\) is the identity for \(+ \)
\end{itemize}

\begin{example}[{Proof}]
Commutativity and associativity follow from the commutativity
and associativity of \(+ \).

\begin{example}[{Proof of cut}]
Clearly, \(C_1 + C_2\) is nonempty. Next, consider \(d_1 \in C_1'\) and \(d_2 \in C_2'\).
Then by characterization of exclusion, \(d_1 > c_1\) and \(d_2 > c_2\) for
all \(c_1 \in C_1\) and \(c_2 \in C_2\). Therefore, \(d_1 + d_2 > c_1 + c_2\)
for all \(c_1, c_2\) and hence \(d_1 + d_2 \notin C_1 + C_2\).

Next, consider \(c < c_1 + c_2\). Then, \(x = c-c_1 < c_2\) and \(x \in C_2\).
Therefore, \(c = c_1 + x \in C_1 + C_2\).

Finally, for each \(c_1 + c_2 \in C_1 + C_2\). There exists,
\(c_1 < d_1 \in C_1\) and \(c_2 < d_2 \in C_2\). Therefore, \(c_1 + c_2 < d_1 + d_2 \in C_1 + C_2\)
and we are done.
\end{example}

\begin{example}[{Proof of identity}]
Consider arbitrary cut \(C\). Then,

\begin{equation*}
C + C(0) = \{c + r: c \in C, r < 0\}
\end{equation*}

Then, for each \(c+ r \in C + C(0)\), \(c + r < c \in C\) and hence
\(c+ r \in C\). Therefore \(C + C(0) \subseteq C\)

Conversely, if \(c \in C\), there exists \(c < d \in C\).
Then \(c - d < 0\) and \(c -d \in C(0)\) and we get that \(c = d + (c-d) \in C + C(0)\).
Therefore, \(C \subseteq C + C(0)\) and we are done.
\end{example}
\end{example}

Additionally, addition preserves ordering. That is
if \(C_1 > D_1\) and \(C_2 > D_2\)

\begin{equation*}
C_1 + C_2 > D_1 + D_2
\end{equation*}

\subsection{Negation (additive inverse)}
\label{develop--math3274:numbers:page--dedekind.adoc---negation-additive-inverse}

Let \(C\) be a cut. Then,

\begin{equation*}
(-C) = \{x \in \mathbb{Q}: \exists a \in C': x < -a \}
\end{equation*}

is a cut and

\begin{equation*}
(-C) + C = C + (-C) = C(0)
\end{equation*}

\begin{example}[{Proof}]
\begin{example}[{Proof of cut}]
Since \(C\) is a cut \(C'\) is non empty and hence \((-C)\)
is also non empty. Next, if \(c \in C\), then
for all \(a \in C'\), \(a > c\) and hence \(-a < -c\).
Therefore \(-c \notin (-C)\).

Next, consider \(x \in (-C)\) where \(x < -a\) and \(a \in C'\). Then if \(y < x\),
\(y < x < -a\) and \(y \in (-C)\). Also, since the rationals
are dense, \(\exists\) \(x < z < -a\) and hence \(z \in (-C)\).
Therefore \((-C)\) is a cut.
\end{example}

\begin{example}[{Proof of inverse}]
\begin{equation*}
C + (-C)
= \{c_1 + c_2: c_1 \in C, c_2 \in (-C)\}
= \{c_1 + c_2: c_1 \in C, \exists a \in C': c_2 < -a\}
\end{equation*}

Now, consider \(c_1 + c_2\) where \(c_2 < -a\). Then, \(a > c_1\)
by characterization of exclusion and hence \(c_1 + c_2 < c_1 - a < c_1 - c_1 = 0\)
and hence \(c_1 + c_2 \in C(0)\). Therefore \(C + (-C) \subseteq C(0)\)

Conversely, consider \(x \in C(0)\). Then \(\frac{-x}{2} > 0\) and \(\exists c_1 \in C\)
such that \(c_1 + \frac{-x}{2} \notin C\). Then, \(-c_1 + x < -c_1 + \frac{x}{2}\)
and hence \(-c_1 + x \in (-C)\). Therefore \(x = c_1 + (-c_1 + x) \in C + (-C)\)
and \(C(0) \subseteq C + (-C)\) and we are done.
\end{example}
\end{example}

Therefore, negation is an involution by considering the additive group. Further, negation must
distribute over addition since addition is commutative. We also define subtraction to be

\begin{equation*}
A - B = A + (-B)
\end{equation*}

\subsection{Absolute value}
\label{develop--math3274:numbers:page--dedekind.adoc---absolute-value}

Let \(C\) be a cut, then we define

\begin{equation*}
|C| = \begin{cases}
C &\quad\text{if } C \geq C(0)\\
-C & \text{if } C < C(0)
\end{cases}
\end{equation*}

Then if \(C \neq C(0)\), we get that \(|C| > C(0)\)

\subsection{Multiplication}
\label{develop--math3274:numbers:page--dedekind.adoc---multiplication}

Let \(C_1\) and \(C_2\) be positive cuts, then we define

\begin{equation*}
C_1 \cdot C_2 = \mathbb{Q}^- \cup \{0\} \cup (C_1^+ \cdot C_2^+)
\end{equation*}

where

\begin{equation*}
C_1^+ \cdot C_2^+ = \{c_1 \cdot c_2: c_1 \in C_1^+, c_2 \in C_2^+\}
\end{equation*}

Then, \(C_1 \cdot C_2\) is a cut.

In general, if \(C_1\) and \(C_2\) are arbitrary cuts, we define

\begin{equation*}
C_1 \cdot C_2 = \begin{cases}
0 &\quad\text{if } C_1 = C(0) \text{ or } C_2 = C(0)\\
|C_1| \cdot |C_2| &\quad\text{if } [ C_1 > C(0) \text{ and } C_2 > C(0) ] \text{ or } [ C_1 < C(0) \text{ and } C_2 < C(0) ]\\
-(|C_1| \cdot |C_2|) &\quad\text{if } [ C_1 > C(0) \text{ and } C_2 < C(0) ] \text{ or } [ C_1 < C(0) \text{ and } C_2 > C(0) ]\\
\end{cases}
\end{equation*}

\begin{itemize}
\item \(\cdot\) is commutative and associative
\item \(C(1)\) is the identity for \(\cdot\)
\item \(\cdot\) distributes over \(+ \)
\end{itemize}

\begin{example}[{Proof}]
\begin{example}[{Proof of cut}]
By definition, it must be non-empty. Also, notice that if \(d_1 \notin C_1\) and \(d_2 \notin C_2\),
\(d_1\) is an upper bound for \(C_1\) and \(d_2\) is an upper bound for \(C_2\)
and hence

\begin{equation*}
\forall c_1 \cdot c_2 \in (C_1^+ \cdot C_2^+): c_1 \cdot c_2 \leq d_1 \cdot d_2
\end{equation*}

Therefore, \(C_1 \cdot C_2 \neq \mathbb{Q}\).

Next, consider arbitrary \(c_1 \cdot c_2 \in (C_1^{\textbackslash}cdot C\_2^)\). Then,
if \(0 < x < c_1 \cdot c_2\), \(0 < q = \frac{x}{c_1 \cdot c_2} < 1\).
Let \(q < q_2 < 1\) and \(q_1 = \frac{q}{q_2}\), then \(0 < q_1 < 1\).
Therefore, \(0 < c_1q_1 < c_1\) and \(0 < c_2q_2 < c_2\)
with \(c_1q_1 \in C_1^+ \) and \(c_2q_2 \in C_2^+ \).
Therefore

\begin{equation*}
x = c_1c_2 \frac{x}{c_1c_2} = c_1c_2q_1q_2 = (c_1q_1) \cdot (c_2 q_2) \in C_1^+ \cdot C_2^+
\end{equation*}

Also, if we let \(c_1 < e_1 \in C_1^+ \) and \(c_2 < e_2 \in C_2^+ \),
we get that

\begin{equation*}
c_1c_2 < e_1e_2 \in C_1^+ \cdot C_2^+
\end{equation*}

and hence we have that \(C_1\cdot C_2\) is a cut.
\end{example}

\begin{example}[{Proof of assiciativity and commutativity}]
By commutativity and associativity of multiplication
in \(\mathbb{Q}\), multiplication of positive cuts
also exhibit these two properties.

To generalize this for all cuts would just be an exercise in checking
all of the cases. I'm not going to do it, but its pretty easy to see,
especially since the result of multiplication explicitly states which
of the forms we need to take if we multiply again.
\end{example}

\begin{example}[{Proof of identity}]
Let \(C\) be a positive cut and consider

\begin{equation*}
C^+ \cdot C(1)^+ = \{c_1 \cdot c_2: c_1 \in C^+, 0 < c_2 < 1\}
\end{equation*}

Consider arbitrary \(c \in C^+ \), then \(\exists d \in C^+ \) such that
\(d > c\). Then \(0 < \frac{c}{d} < 1\) and

\begin{equation*}
c = d \cdot \frac{c}{d}  \in C^+ \cdot C(1)^+
\end{equation*}

Therefore \(C \subseteq C^+ \cdot C(1)^+ \).

Conversely, if \(c_1 \cdot c_2 \in C^+ \cdot C(1)^+ \), we get that
\(c_1 \cdot c_2 < c_1\) and hence \(c_1 \cdot c_2 \in C^+ \)
and \(C^+ \cdot C(1)^+ \subseteq C^+ \) and we are done.

Well.. we have shown that \(C(1)\) is the identity for positive cuts.
It is also clearly the identity for \(C(0)\) while for negative cuts

\begin{equation*}
C \cdot C(1) = -(|C| \cdot C(1)) = -|C| = C
\end{equation*}

Now, we actually done.
\end{example}

\begin{example}[{Proof of distribution over addition}]
\begin{admonition-note}[{}]
We follow much of the same process as Ethan Bloch in
``The Real Numbers and Real Analysis''.
Thank you Mauro ALLEGRANZA for suggesting the book in
\href{https://math.stackexchange.com/questions/1190755/proving-the-distributive-law-of-a-dedekind-cut}{this post}.
\end{admonition-note}

Consider cuts \(A,B,C\). Then, we want to prove that

\begin{equation*}
(A + B) \cdot C = (A\cdot C) + B \cdot C
\end{equation*}

Notice that if any of \(A, B, C\) are zero, the result is trivial.
Then, we would prove the case when all are positive then derive
all other results.

\begin{example}[{Proof when all are positive}]
Notice that \(A+B > C(0)\) and hence

\begin{equation*}
(A+ B)\cdot C
= \mathbb{Q}^- \cup \{0\} \cup ((A+B)^+ \cdot C^+)
\end{equation*}

while

\begin{equation*}
(A \cdot C) + (B \cdot C) =
(\mathbb{Q}^- \cup \{0\}\cup (A^+ \cdot C^+)
+ (\mathbb{Q}^- \cup \{0\}\cup (B^+ \cdot C^+)
\end{equation*}

We would show that both sets are equal. Firstly consider \((a+b)c \in (A+B)^+ \cdot C^+ \).
Then clearly, \((a + b)c \in (A \cdot C) + (B \cdot C)\) since
\(ac \in A \cdot C\) and \(bc\in B \cdot C\) since

\begin{itemize}
\item if \(a \leq 0\), \(ac \leq 0\) and \(ac \in \mathbb{Q}^- \subseteq A \cdot C\)
\item if \(a > 0\), \(ac \in A^+ \cdot C^+ \subseteq A \cdot C\)
\end{itemize}

and likewise for \(bc\). Hence \((A+B)\cdot C \subseteq (A\cdot C) + (B\cdot C)\).

Conversely, consider \(e_1 + e_2 \in (A\cdot C) + (B \cdot C)\).
Note, we only need to show that if both \(e_1\) and \(e_2\) are positive,
\(e_1 + e_2 \in (A+B)\cdot C\) since if either is negative,
there exists a sum consisting of positives which is greater than it.
Now, let \(e_1 = ac_1\) and \(e_2 = bc_2\) where \(a\in A^+ \),
\(b \in B^+ \) and \(c_1, c_2 \in C^+ \). Let \(c = \max(c_1, c_2)\).
Then,

\begin{equation*}
e_1 + e_2 = ac_1 + bc_2 \leq ac + bc = (a+b)c \in (A +C)\cdot C
\end{equation*}

hence we are done.
\end{example}

Now, we have \(7\) other cases. However, we would omit
when \(C\) is negative since the result is easily derived.
Now, we have \(3\) cases to show

If \(A < 0\) and \(B > 0\), we would split into two cases.

\begin{itemize}
\item If \(A +B > 0\), then
    
    \begin{equation*}
    \begin{aligned}
    AC + BC
    &= AC + ((-A) + A + B)C
    \\&= AC + (-A)C + (A+B)C
    \\&= -((-A)C) + AC + (A+B)C
    \\&= (A+B)C
    \end{aligned}
    \end{equation*}
\item If \(A +B < 0\), then
    
    \begin{equation*}
    \begin{aligned}
    (A+B)C
    &= -((-(A+B))C) \quad\text{since } A+B < 0
    \\&= -(((-A) + (-B))C) \quad\text{since negation distributes over addition}
    \\&= -((-A)C + (-B)C) \quad\text{since }(-A) + (-B) > 0 \text{ (previous case)}
    \\&= (-(-A)C) + (-(-B)C)
    \\&= AC + BC
    \end{aligned}
    \end{equation*}
\end{itemize}

Now, the case when \(A > 0\) and \(B < 0\) is very similar and we can
indeed use the previous case along with the commutativity of addition
to obtain this result.

Finally, if both \(A < 0\) and \(B < 0\), then
\(A + B < 0\) and

\begin{equation*}
\begin{aligned}
(A + B)C
&= - ((-(A+B))C)
\\&= - (((-A) + (-B))C)
\\&= - ((-A)C + (-B)C)
\\&= (-(-A)C) + (-(-B)C)
\\&= AC + BC
\end{aligned}
\end{equation*}
\end{example}
\end{example}

\subsection{Reciprical (multiplicative inverse)}
\label{develop--math3274:numbers:page--dedekind.adoc---reciprical-multiplicative-inverse}

If \(C\) is a positive cut, we define

\begin{equation*}
C^{-1} = \mathbb{Q}^- \cup \{0\} \cup (C^+)^{-1}
\end{equation*}

where

\begin{equation*}
(C^+)^{-1}
= \{b \in \mathbb{Q}^+: \exists a \in (\mathbb{Q}^+ \backslash C+): b < a^{-1}\}
= \{b \in \mathbb{Q}^+: \exists a \in C': b < a^{-1}\}
\end{equation*}

Then \(C^{-1}\) is a cut.

In general if \(C\) is a cut, we define

\begin{equation*}
C^{-1} = \begin{cases}
|C|^{-1} & \quad\text{if } C > C(0)\\
-(|C|^{-1}) & \quad\text{if } C < C(0)
\end{cases}
\end{equation*}

Then,

\begin{equation*}
C^{-1} \cdot C = C \cdot C^{-1} = C(1)
\end{equation*}

\begin{example}[{Proof}]
\begin{example}[{Proof of cut}]
Clearly, \(C^{-1} \neq \varnothing\) also if \(c \in C^+ \),
then \(c < a\) for all \(a \in C'\)
and hence \(a^{-1} < c^{-1}\). Therefore,
\(c^{-1} \notin (C^+)^{-1}\) and hence not in
in \(C\).

Next, if \(b \in C^{-1}\) then \(\exists a \in C'\)
such that \(b < a^{-1}\).
Then if \(e < b\), \(e < a^{-1}\)
and hence \(e \in C^{-1}\).
Also, by the density of rationals,
there exists \(b < f < a^{-1}\)
and hence \(f \in C^{-1}\).

Therefore \(C^{-1}\) is a cut.
\end{example}

\begin{example}[{Proof of inverse}]
Firstly suppose that \(C\) is positive. Then

\begin{equation*}
\begin{aligned}
C \cdot C^{-1}
&= \mathbb{Q}^- \cup \{0\} \cup \{c d: c \in C^+, d \in (C^+)^{-1}\}
\\&= \mathbb{Q}^- \cup \{0\} \cup \{c d: c \in C^+, d \in \mathbb{Q}, \exists e \in C': d < e^{-1}\}
\end{aligned}
\end{equation*}

Then, consider \(cd \in (C \cdot C^{-1})^+ \) where \(d < e^{-1}\) and \(e \in C'\).
Then \(c < e\) and hence \(cd < ce^{-1} < ee^{-1} = 1\).
Therefore, \(cd \in C(1)\) and hence \(C \cdot C^{-1} \subseteq C(1)\).

Conversely, consider \(x \in C(1)^+ \); that is \(0 < x < 1\).
Then, there exists \(y \in \mathbb{Q}\) such that \(x < y < 1\)
and hence, \(\frac{1}{y} > 1\). Therefore,
there exists \(c \in C\) such that \(c\frac{1}{y} \notin C\).
Hence,

\begin{equation*}
\frac{x}{c} < \frac{y}{c} = \left(\frac{c}{y}\right)^{-1} \implies \frac{x}{c} \in C^{-1}
\end{equation*}

and hence \(x = c\frac{x}{c} \in C \cdot C^{-1}\). Therefore,
\(C(1) \subseteq C \cdot C^{-1}\) and we are done.

For the case when \(C\) is negative,

\begin{equation*}
C \cdot C^{-1} = C \cdot (-(|C|^{-1})) = |C| \cdot |C|^{-1} = C(1)
\end{equation*}
\end{example}
\end{example}

\section{Rational cuts}
\label{develop--math3274:numbers:page--dedekind.adoc---rational-cuts}

Let \(r \in \mathbb{Q}\), then we define

\begin{equation*}
C(r) = \{c \in \mathbb{Q}: c < r\}
\end{equation*}

Then \(C(r)\) is a \emph{rational} cut. Furthermore, \(C \in R\) is a cut
iff \(C'\) has a least element.

\begin{example}[{Proof}]
\begin{example}[{Proof of cut}]
Firstly, \(C(r) \neq \varnothing\) since \(r-1 \in C(r)\). Also, \(C(r) \neq \mathbb{Q}\)
since \(r \notin C(r)\). Next, consider arbitrary \(c \in C(r)\).

If \(a < c\), then \(a < c < r\) and \(a \in C(r)\).
Also, notice that \(c < \frac{c + r}{2} < r\).

Therefore, \(C(r)\) is a cut.
\end{example}

\begin{example}[{Proof of characterization}]
Consider cut \(C = C(r)\). Then,

\begin{equation*}
C' = \{c \in \mathbb{Q}: c \geq r\}
\end{equation*}

Then, \(r\) is the least element of \(C'\).

Conversely if we have an arbitrary cut \(C\) and \(r\) is the least element of \(C'\),
then we want to show that \(C = C(r)\). Firstly, notice that \(r \notin C\) and hence, if
\(c > r\), \(c \notin C\) otherwise it would imply that \(r \in C\) since \(C\) is a cut.
Therefore, if \(c \in C\), we get that \(c < r\) and hence \(C \subseteq C(r)\).
Conversely, notice that if \(c < r\), \(c \in C\) otherwise it would invalidate the fact
that \(r\) is the lease element in \(C'\). Therefore \(C(r) \subseteq C\) and we are done.
\end{example}
\end{example}

\subsection{Homomorphism}
\label{develop--math3274:numbers:page--dedekind.adoc---homomorphism}

The function \(x \to C(x)\) is an injective
ordered field homomorphism from \((\mathbb{Q}, +, \cdot, \leq)\)
to \((R, +, \cdot, \leq)\). That is,

\begin{itemize}
\item \(C(a + b) = C(a) + C(b)\)
\item \(C(-a) = -C(a)\)
\item \(C(ab) = C(a)C(b)\)
\item \(a \leq b \iff C(a) \leq C(b)\)
\end{itemize}

\begin{example}[{Proof}]
The fact that \(x\mapsto C(x)\) is injective would follow from order preservation.

\begin{example}[{Proof of ordering}]
Suppose that \(a < b\). Then, \(a \in C(b)\) however, \(a \notin C(a)\). Therefore, \(C(a) \neq C(b)\).
Also notice that if \(c \in C(a)\), then \(c < a < b\) and \(c \in C(b)\).
Therefore \(C(a) \subset C(b)\) and \(C(a) < C(b)\).

Conversely, suppose that \(C(a) < C(b)\). Then, if \(a > b\), we get a contradiction,
since \(b \in C(a) \subset C(b)\). Therefore \(a < b\) as expected.
\end{example}

\begin{example}[{Proof of sum}]
Recall that

\begin{equation*}
C(a) + C(b) = \{c_1 + c_2: c_1 \in C(a) , c_2 \in C(b)\} = \{c_1 + c_2: c_1 < a, c_2 < b\}
\end{equation*}

Now, consider \(x \in C(a) + C(b)\), then \(x = c_1 + c_2 < a +b\) and \(x \in C(a+b)\)
and hence \(C(a) + C(b) \subseteq C(a+b)\).

On the other hand, if \(x \in C(a + b)\), then \(x < a + b\) and hence \(0 < a+ b - x\) and

\begin{equation*}
x = \left(a - \frac{a+b-x}{2}\right) + \left(b - \frac{a+b -x}{2}\right)
\end{equation*}

and hence \(x \in C(a) + C(b)\). Therefore \(C(a+b) \subseteq C(a) + C(b)\)
and we are done.
\end{example}

\begin{example}[{Proof of additive inverse}]
This is a given. Notice that

\begin{equation*}
C(0) = C(a-a)= C(a + (-a)) = C(a) + C(-a)
\end{equation*}

Therefore \(C(-a)\) is the additive inverse of \(C(a)\).
\end{example}

\begin{example}[{Proof of product}]
Consider product \(ab\). If \(a = 0\) or \(b =0\), \(ab = 0\). Also,
\(C(a)C(b) = 0\) by definition. Therefore, we only need to consider
when all are non zero.

If \(a > 0\) and \(b > 0\), \(ab > 0\). Then, we need to show that

\begin{equation*}
\{c \in \mathbb{Q}: 0 < ab < c\} = C(ab)^+ = C(a)^+ \cdot C(b)^+ = \{c_1c_2: 0 < c_1 < a, 0 < c_2 < b\}
\end{equation*}

Firstly, consider \(c \in C(ab)^+ \). Then \(0 < \frac{c}{ab} < 1\) and there exists
\(0 < q_1, q_2 < 1\) such that \(q_1 q_2 = q = \frac{c}{ab}\). This can be done by considering \(q < q_2 < 1\)
and notice that \(q_1 = \frac{q}{q_2} < 1\). Therefore, we then take \(c = aq_1 bq_2\)
and notice that \(aq_1 < a\) and \(b q_2 < b\). Therefore \(c \in C(a)^ + \cdot C(b)^ + \)
and \(C(ab) \subseteq C(a)^ + \cdot C(b)^ + \).

On the other hand, if we consider \(c_1 \in C(a)^ + \) and \(c_2 \in C(b)^ + \). Then,
\(0 < c_1c_2 < ab\) and \(c_1c_2 \in C(ab)^ + \). Therefore, we get \(c_1c_2 \in C(ab)^ + \)
and \(C(a)^ + \cdot C(b)^ + \subseteq C(ab)^ + \).

If \(a < 0\) and \(b > 0\), then

\begin{equation*}
C(a)C(b) = - ((-C(a))C(b)) = -(C(-a)C(b)) = - C(-ab) = C(ab)
\end{equation*}

similarly if \(a > 0\) and \(b < 0\).

Finally, if \(a < 0\) and \(b < 0\), then

\begin{equation*}
C(a)C(b) = (-C(a))(-C(b)) = C(-a)C(-b) = C((-a)(-b)) = C(ab)
\end{equation*}

and we are done.
\end{example}
\end{example}
\end{document}
