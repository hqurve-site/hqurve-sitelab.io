\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Systems of Differential Equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
In this page, we discuss systems of differential equations.
Knowledge of solving systems of differential equations is important
since we would later show that any higher order differential equation
can be converted to a system of first order differential equations.
Many of the previously discussed techniques which have a single variable and equation
translate nicely to systems of differential equations.

Before we can speak about a system of differential equations, we must
first understand vector-valued functions which are either of a single real
variable or multiple variables.
Please see \myautoref[{}]{develop--math6120:appendix:page--vector-functions.adoc} for basic terminology for vector functions.

\section{Basics}
\label{develop--math6120:ROOT:page--systems-of-solutions.adoc---basics}

Let \(I\) be an interval of \(\mathbb{R}\) and \(E \subseteq \mathbb{R}^n\).
Let \(D = I \times E \subseteq \mathbb{R}^{n+1}\)
Consider the system of differentiation equations of the form

\begin{equation*}
\left.\begin{aligned}
x_1'(t) &= f_1(t, x_1(t), x_2(t),\ldots x_n(t))\\
x_2'(t) &= f_2(t, x_1(t), x_2(t),\ldots x_n(t))\\
&\vdots\\
x_n'(t) &= f_n(t, x_1(t), x_2(t),\ldots x_n(t))\\
\end{aligned}
\right\}
\end{equation*}

Where each \(f\) has domain \(D \subseteq \mathbb{R}^{n+1}\)
This may be more compactly written as \(\vec{x}'(t) = \vec{f}(t, \vec{x}(t))\).

Then, \(\vec{\phi} = (\phi_1, \phi_2, \ldots \phi_n): I \to \mathbb{R}^n\) is called
a solution of this system iff

\begin{enumerate}[label=\arabic*)]
\item \(\phi_1', \phi_2', \ldots \phi_n'\) exist
\item \((t, \phi_1(t), \phi_2(t), \ldots \phi_n(t)) \in D\) for each \(t \in I\)
\item \(\phi_i'(t) = f_i(t, \phi_1(t), \phi_2(t), \ldots, \phi_n(t))\) for each \(t \in I\)
\end{enumerate}

This may be more compactly written as

\begin{enumerate}[label=\arabic*)]
\item \(\vec{\phi}'\) exists
\item \((t, \vec{\phi}(t)) \in D\) for each \(t \in I\)
\item \(\vec{\phi}'(t) = \vec{f}(t, \vec{\phi})\) for each \(t \in I\)
\end{enumerate}

\begin{admonition-note}[{}]
if we have an IVP, we have the additional condition that \(\vec{\phi}(t_0) = \vec{x}_0\)
\end{admonition-note}

\subsection{Lipschitz condition}
\label{develop--math6120:ROOT:page--systems-of-solutions.adoc---lipschitz-condition}

Let \(\vec{f}: D \to \mathbb{R}^n\). Then, we say that \(f\) is Lipschitz continuous
with constant \(K > 0\)  if

\begin{equation*}
\forall (t, \vec{x}_1), (t, \vec{x}_2) \in D:
\|\vec{f}(t,\vec{x}) - \vec{f}(t, \vec{y})\| \leq K\|\vec{x} - \vec{y}\|
\end{equation*}

\begin{proposition}[{Sufficient condition for Lipschitz}]
If \(\vec{f}_{x_i}\) exists and is bounded in closed region \(R \subseteq D\),
then \(\vec{f}\) satisfies the Lipschitz condition in \(R\)
with constant given by

\begin{equation*}
\sup_{(t, \vec{x}) \in R,\ i\in \{1, \ldots n\}} \|\vec{f}_{x_i}(t, \vec{x})\| = K
\end{equation*}
\end{proposition}

\section{Existence and Uniqueness of solution}
\label{develop--math6120:ROOT:page--systems-of-solutions.adoc---existence-and-uniqueness-of-solution}

\begin{lemma}[{Equivalent integral equation}]
Let \(\vec{f}: D \to \mathbb{R}^n\) be a continuous function.
The function \(\vec{\phi}\) is a solution of the following
IVP on some interval \(I\)

\begin{equation*}
\vec{x}'(t) = \vec{f}(t, \vec{x}(t)), \quad\vec{x}(t_0)= \vec{x}_0
\end{equation*}

iff \(\vec{\phi}\) is a solution of the integral equation

\begin{equation*}
\vec{x}(t) = \vec{x}_0 + \int_{t_0}^t \vec{f}(s, \vec{x}(s)) \ ds
\end{equation*}
\end{lemma}

Picards approximations are defined equivalently as in the univariate case
\(\vec{\phi}_0(t) = \vec{x}_0\) and

\begin{equation*}
\vec{\phi}_{n+1}(t)
= \vec{x}_0 + \int_{t_0}^t \vec{f}(s, \vec{\phi}_n(s)) \ ds
\end{equation*}

\begin{theorem}[{Picards local existence theorem for systems of equations}]
\begin{enumerate}[label=\arabic*)]
\item Let \(D \subseteq \mathbb{R}^{n+1}\) be an open domain and let
    \(\vec{f}: D \to \mathbb{R}^n\) be a vector valued function satisfying the two requirements
    
    \begin{enumerate}[label=\alph*)]
    \item \(\vec{f} \in C(D)\); ie \(\vec{f}\) is continuous
    \item \(\vec{f}\) satisfies Lipschitz condition in \(D\) wrt \(\vec{x}\)
    \end{enumerate}
\item Let \((t_0, \vec{x}_0) \in D\) be an interior point and \(a > 0\) and \(b>0\)
    such that the closed rectangle
    
    \begin{equation*}
    R = \left\{(t,\vec{x}) \ | \ |t-t_0| \leq a,\ \|\vec{x} - \vec{x}_0\| \leq b\right\} \subseteq D
    \end{equation*}
    
    Let \(M>0\) be such that \(\vec{f}\) is bounded by \(M\) in \(R\)
    and \(\alpha = \min\left(a, \frac{b}{M}\right)\).
\end{enumerate}

Then, there exists a unique solution \(\vec{\phi}\) of the IVP

\begin{equation*}
\vec{x}'(t) = f(t, \vec{x}), \quad \vec{x}(t_0) = \vec{x}_0
\end{equation*}

on the interval \(|t-t_0| \leq \alpha\)
\end{theorem}

\begin{theorem}[{Non-local existence theorem}]
Let \(\vec{f}\) be a vector valued function which is satisfies
the Lipschitz condition on a strip \(S\) of the form

\begin{equation*}
S = \left\{(t, \vec{x}) \ | \ |t-t_0| \leq a, \ \|\vec{x}\| < \infty\right\}
\end{equation*}

If \((t_0, \vec{x}_0) \in S\), then Picards successive approximations
exists as continuous functions in \(I = |t-t_0| \leq a\)
and converge to a unique solution of the IVP.
\end{theorem}

\begin{theorem}[{Global existence theorem}]
Let \(\vec{f}\) be a vector valued function which is defined
and continuous on \(\mathbb{R}^{n+1}\).
If \(\vec{f}\) satisfies Lipschitz condition on every strip \(S_a\)

\begin{equation*}
S_a = \left\{(t, \vec{x}) \ | \ |t-t_0| \leq a, \ \|\vec{x}\| < \infty\right\}
\end{equation*}

then the IVP associated with \(\vec{f}\) has a unique solution which exists
for all real \(t\).
\end{theorem}

\begin{theorem}[{Cauchy - Peano Existence Theorem}]
Let \(\vec{f}\) be continuous in the open set \(D \subseteq \mathbb{R}^{n+1}\)
and \(t_0, \vec{x}_0 \in D\). Then, there exists at least one solution
of the following IVP in the neighbourhood of \(t_0\)

\begin{equation*}
\vec{x}' = \vec{f}(t, \vec{x}), \ \vec{x}(t_0) = \vec{x}_0
\end{equation*}
\end{theorem}

\begin{admonition-tip}[{}]
Apparently, the proofs of each of these results are very similar to
the univariate case.
\end{admonition-tip}

\section{Abstract versions}
\label{develop--math6120:ROOT:page--systems-of-solutions.adoc---abstract-versions}

\begin{theorem}[{Abstract version of Picard's Existence and Uniqueness theorem}]
Let \(\vec{f} \in C(D, \mathbb{R}^n)\) and \((t_0, \vec{x}_0) \in D\).
Also let \(I = |t-t_0| \leq a\) and \(\Omega = \{\vec{x} \in \mathbb{R}^n: \| \vec{x} - \vec{x}_0\| \leq b\}\)
be such that \(\vec{f}\) is Lipschitz (in the second argument) in \(V = I \times \Omega \subseteq D\)
with Lipschitz constant \(K> 0\).

Then, there exists \(\alpha > 0\) for which there exists a unique solution of the integral
equation

\begin{equation*}
\vec{x}(t) = \vec{x}_0 = \int_{t_0}^t f(s, \vec{x}(s)) \ ds
\end{equation*}

with domain \(|t-t_0| \leq \alpha\). One such \(\alpha\) is such that

\begin{equation*}
\alpha \leq a, \quad \alpha \leq \frac{b}{M}, \quad \alpha < \frac{1}{K}
\end{equation*}

where \(\vec{f}\) is bounded by \(M\) in \(V\).

\begin{admonition-important}[{}]
Note that \(\alpha\) is \emph{strictly less than} \(\frac{1}{K}\)
\end{admonition-important}

\begin{proof}[{}]
In this proof, we use the Banach fixed-point theorem to show that there is a single fixed point
of the following function

\begin{equation*}
T\vec{x}(t) = \vec{x}_0 + \int_{t_0}^t \vec{f}(t, \vec{x}(s)) \ ds
\end{equation*}

where \(T: X \to X\) and \(X = C(J, \Omega)\) where \(J = \|t-t_0\|\leq \alpha\)
and \(\alpha\) is yet to be specified.
Note that \(\vec{x}(t)\) is a solution of the integral equation iff it is a fixed point of \(T\).
We need to prove that

\begin{itemize}
\item \(X\) is a complete metric space
\item \(T\) is a self-map
\item \(T\) is a contraction; ie Lipschitz with \(K < 1\)
\end{itemize}

Note that \(X\) is a complete metric space since the domain is closed and bounded.
Also notice that \(T\) is a self map since for each
continuous \(\vec{\phi}(t) \in C(X, \mathbb{R}^n)\), \(T\phi\) is continuous and

\begin{equation*}
\|T\vec{\phi}(t)- \vec{x}_0\|
= \left\|\int_{t_0}^t \vec{f}(t, \vec{\phi}(s)) \ ds \right\|
\leq \left|\int_{t_0}^t \left\| \vec{f}(t, \vec{\phi}(s))\right\| \ ds\right|
\leq \left|\int_{t_0}^t M \ ds\right|
\leq M\alpha
\end{equation*}

For this reason, we want that \(\alpha \leq \frac{b}{M}\). Under this condition,
\(T\) is indeed a self-map.

Finally

\begin{equation*}
\begin{aligned}
\|T\vec{\phi}(t) - T\vec{\psi}(t)\|
&= \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \ ds \right\|
\\&\leq \left|\int_{t_0}^t \left\|\vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \right\| \ ds\right|
\\&\leq \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\psi}(s) \right\| \ ds\right|
\\&\leq K\left\|\vec{\phi} - \vec{\psi} \right\| |t-t_0| \quad\text{Note this is the supremum metric between the functions}
\\&\leq K\alpha\left\|\vec{\phi} - \vec{\psi} \right\|
\end{aligned}
\end{equation*}

Therefore, \(\|T\vec{\phi} - T\vec{\psi}\| \leq K\alpha\left\|\vec{\phi} - \vec{\psi} \right\|\).
Also \(T\) is a contraction if \(\alpha < \frac{1}{K}\).

Therefore, the conditions of Banach's fixed-point theorem hold and there exists a unique \(\vec{x}\)
such that \(T\vec{x} = \vec{x}\).
\end{proof}
\end{theorem}

\begin{theorem}[{Abstract version of non-existence theorem}]
Suppose that \(\vec{f}(t, \vec{x})\) satisfies the Lipschitz
condition on \(V = I \times \mathbb{R}^n\) where
\(I = |t-t_0| \leq a\).
Then, there exists a unique solution to the following
integral equation

\begin{equation*}
\vec{x}(t) = \vec{x}_0 = \int_{t_0}^t f(s, \vec{x}(s)) \ ds
\end{equation*}

\begin{proof}[{}]
The same proof as the local existence theorem can be used.
However, we need to somehow avoid limiting the domain
when proving that \(T\) is a contraction.

In the notes, they do this by using a different norm. Namely
\(\|\vec{x}\| = \sup_{t\in I}|\vec{x}(t)|e^{-N|t-t_0|}\) for some \(N > 0\) yet to be specified.
We are not going to prove that this forms a norm; but apparently it does
and the metric induced is complete.
Now, lets prove that \(T\) is a contraction

\begin{equation*}
\begin{aligned}
\|T\vec{\phi}(t) - T\vec{\psi}(t)\|e^{-N|t-t_0|}
&= \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \ ds \right\|e^{-N|t-t_0|}
\\&\leq \left|\int_{t_0}^t \left\|\vec{f}(t, \vec{\phi}(s)) - \vec{f}(t, \vec{\psi}(s)) \right\| \ ds\right|e^{-N|t-t_0|}
\\&\leq \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\psi}(s) \right\| \ ds\right|e^{-N|t-t_0|}
\\&\leq \left|\int_{t_0}^t K\left\|\vec{\phi} - \vec{\psi} \right\|e^{N|s-t_0|} \ ds\right|e^{-N|t-t_0|}
\\&\leq \frac{K}{N}\left\|\vec{\phi} - \vec{\psi} \right\|\left(e^{N|t-t_0|} - 1\right)e^{-N|t-t_0|}
\\&\leq \frac{K}{N}\left\|\vec{\phi} - \vec{\psi} \right\|
\end{aligned}
\end{equation*}

Then, by choosing \(N = 2K\), we get a that \(T\) is a contraction and the result follows.

\begin{admonition-note}[{}]
We could have also done a similar trick for Picard's local existence theorem to remove
the restriction that \(\alpha < \frac{1}{K}\).
\end{admonition-note}
\end{proof}
\end{theorem}

\section{Continuation and dependence on initial conditions}
\label{develop--math6120:ROOT:page--systems-of-solutions.adoc---continuation-and-dependence-on-initial-conditions}

The following theorem allows us to have

\begin{theorem}[{Continuity based on initial conditions}]
Let \(\vec{f}\) be continuous and bounded by some \(M > 0\)
and satisfy Lipschitz condition on connected set \(D\).
And consider the ODE

\begin{equation*}
\vec{x}'(t) = \vec{f}(t, \vec{x})
\end{equation*}

with interval \(I = [a,b]\).

Then for each \(\varepsilon > 0\), there exists a \(\delta > 0\)
such that for each pair of
solutions \(\vec{\phi}\) and \(\vec{\phi}^*\)
corresponding to initial conditions \(\vec{x}(t_0) = \vec{x}_0\)
and \(\vec{x}(t_0^*) = \vec{x}_0^*\).

\begin{equation*}
|t_0 - t_0^*| < \delta \quad\text{and}\quad \|\vec{x}_0 - \vec{x}_0^*\| < \delta
\end{equation*}

implies that

\begin{equation*}
\|\vec{\phi} - \vec{\phi}^*\| < \varepsilon
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
The proof is basically identical to the univariate case.
\end{admonition-note}

Consider the supremum norm. Then for each \(t \in I\)

\begin{equation*}
\begin{aligned}
\|\vec{\phi}(t) - \vec{\phi}^*(t)\|
&= \left\| (\vec{x}_0-\vec{x}_0^*)
+ \int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) \ ds
- \int_{t_0^*}^t \vec{f}(s, \vec{\phi}^*(s)) \ ds
\right\|
\\&= \left\| (\vec{x}_0-\vec{x}_0^*)
+ \int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\phi}^*(s))\ ds
+ \int_{t_0}^{t_0^*} \vec{f}(s, \vec{\phi}(s)) \ ds
\right\|
\\&\leq \left\| \vec{x}_0-\vec{x}_0^*\right\|
+ \left|\int_{t_0}^t \left\|\vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\phi}^*(s))\right\|\ ds\right|
+ \left| \int_{t_0}^{t_0^*} \left\|\vec{f}(s, \vec{\phi}(s))\right\| \ ds\right|
\\&\leq \left\| \vec{x}_0-\vec{x}_0^*\right\|
+ \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\phi}^*(s)\right\|\ ds\right|
+ M| t_0 - t_0^*|
\\&\leq (M+1)\delta
+ \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\phi}^*(s)\right\|\ ds\right|
\end{aligned}
\end{equation*}

By applying Gronwall's inequality, we get

\begin{equation*}
\|\vec{\phi}(t) - \vec{\phi}^*(t)\|
\leq (M+1)\delta e^{\left|\int_{t_0}^t K \ ds\right|}
\leq (M+1)\delta e^{K|b-a|}
\end{equation*}

Therefore we can choose sufficiently small \(\delta\) such that that the above
is less than \(\varepsilon\) and hence we get the desired result.
\end{proof}
\end{theorem}

\begin{admonition-note}[{}]
I split the Approximation and Uniqueness theorem into two parts
\end{admonition-note}

\begin{theorem}[{Approximation Theorem}]
Let \(\vec{f}\) and \(\vec{g}\) be continuous functions defined in
a rectangle

\begin{equation*}
R = \left\{(t, \vec{x}) \ \middle| \ |t-t_0| \leq a, \ \|\vec{x} - \vec{x}_0\| \leq b\right\}
\end{equation*}

and \(\vec{f}\) satisfy the Lipschitz condition with constant \(K\).
Let \(\vec{\phi}\) and \(\vec{\psi}\) be solutions of
the following respective IVPs

\begin{equation*}
\vec{x}'=\vec{f}(t, \vec{x}),\ \vec{x}(t_0) = \vec{x}_0^{(1)}
\end{equation*}

\begin{equation*}
\vec{x}'=\vec{g}(t, \vec{x}),\ \vec{x}(t_0) = \vec{x}_0^{(2)}
\end{equation*}

on some interval \(J\) containing \(t_0\).
Let \(\varepsilon > 0\) and \(\delta > 0\) be such that

\begin{equation*}
\forall (t,\vec{x}) \in R:
\|\vec{f}(t, \vec{x}) - \vec{g}(t, \vec{x})\| \leq \varepsilon
\quad\text{and}\quad
\|\vec{x}_0^{(1)} - \vec{x}_0^{(2)}\| \leq \delta
\end{equation*}

Then,

\begin{equation*}
\forall t \in J: \|\vec{\phi}(t) - \vec{\psi}(t)\|
\leq \delta e^{K|t-t_0|} + \frac{\varepsilon}{K}\left(e^{K|t-t_0|} - 1\right)
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
\|\vec{\phi}(t) - \vec{\psi}(t)\|
&\leq \|\vec{x}_0^{(1)} - \vec{x}_0^{(2)}\|
+ \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{g}(s, \vec{\psi}(s)) \ ds\right\|
\\&\leq \|\vec{x}_0^{(1)} - \vec{x}_0^{(2)}\|
+ \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \ ds\right\|
+ \left\|\int_{t_0}^t \vec{f}(s, \vec{\psi}(s)) - \vec{g}(s, \vec{\psi}(s)) \ ds\right\|
\\&\leq \delta
+ \varepsilon|t-t_0|
+ K\left|\int_{t_0}^t \|\vec{\phi}(s) - \vec{\psi}(s)\| \ ds\right|
\end{aligned}
\end{equation*}

By using the strategy as in \myautoref[{Lecture 4 Question 2}]{develop--math6120:exercises:page--chapter2.adoc--continuation-and-dependence-question2},
we obtain the desired result.

\begin{admonition-tip}[{}]
In case the above link is unreachable, the idea is two cases based on the sign
of \(t-t_0\). Then, let the right hand side be some function \(h(t)\). Then, conduct
the same process as in the proof of Gronwall's inequality.
\end{admonition-tip}
\end{proof}
\end{theorem}

This theorem may be used to approximate solutions to hard initial value problems
by providing an approximation for \(f\) using \(g\).

\begin{corollary}[{Uniqueness}]
Let \(\vec{f}\) satisfy the Lipschitz condition in some rectangle \(R\).
Then the following IVP has at most one solution on any interval containing \(t_0\)

\begin{equation*}
\vec{x} = \vec{f}(t, \vec{x}),\ \vec{x}(t_0) = \vec{x}_0
\end{equation*}
\end{corollary}

\section{Higher order differential equations}
\label{develop--math6120:ROOT:page--systems-of-solutions.adoc---higher-order-differential-equations}

Consider the following IVP

\begin{equation*}
\begin{aligned}
&x^{(n)} = f(t, x, \ldots x^{(n-1)})
\\&x(t_0) = \alpha_1, \quad x'(t_0) = \alpha_2, \quad\ldots\quad x^{(n-1)} = \alpha_{n}
\end{aligned}
\end{equation*}

We have not yet developed any techniques to solve this problem.
However, we can convert it to a system of differential equations by
defining \(\vec{\alpha}_0 = (\alpha_1, \ldots \alpha_n)\) and \(\vec{x} = (x_1, x_2, \ldots x_n)\) as follows

\begin{equation*}
x_1 = x,\quad x_2 = x',\quad \ldots\quad x_{n} = x^{(n-1)}
\end{equation*}

Consider the following system of differential equations

\begin{equation*}
\begin{aligned}
x_1' &= x_2 \quad &x_1(t_0) &= \alpha_1\\
x_2' &= x_3 \quad &x_2(t_0) &= \alpha_2\\
&\vdots\\
x_{n-1}' &= x_{n} \quad &x_{n-1}(t_0) &= \alpha_{n-1}\\
x_n' &= f(t, x_1, x_2, \ldots x_n) \quad &x_n(t_0) &= \alpha_n\\
\end{aligned}
\end{equation*}

Then, it is not hard to see that this system of equations is equivalent to our original IVP and
\(x\) is a solution of the IVP iff \(\vec{x} = (x, x', \ldots x^{(n-1)}\) is a solution of the above system.
Therefore, we can apply the theorems developed for systems of differential equations to higher order differential equations.
\end{document}
