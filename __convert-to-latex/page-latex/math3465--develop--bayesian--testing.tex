\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Hypothesis Testing}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Suppose we have a distribution \(\pi(\theta | \vec{x})\) and
a sequence of decisions \(d_0, d_1, \ldots\). Then,
for each decision, we associate the function
\(L(\theta_i; d_i)\) to be the loss incurred when
\(d_i\) is taken and \(\theta = \theta_i\).
Therefore, our job is to simply make the decision which
minimizes the expected loss

\begin{equation*}
\gamma(d_i| \vec{x}) = E(L(\theta; d_i)) = \int_{\mathbb{R}} L(\theta; d_i) \pi(\theta | \vec{x}) \ d\theta
\end{equation*}

Therefore, the choice of \(d_i\) depends directly on \(\pi(\theta | \vec{x})\)
which depends on \(\vec{x}\). Then, for each \(\vec{x}\)
observed, we can associate a unique \(d_i\) and hence,
we can identify \(d_i\) by the set of \(\vec{x}\)
which when observed results in the choice of \(d_i\).
Therefore, we can associate probabilities of choosing
each of the \(d_i\), namely \(P(choose\ d_i) = P(\vec{x} \in d_i)\).

Conversely, suppose that we specified each \(d_i\) by
a set of \(\vec{x}\) which when observed results in
the choice of \(d_i\). Then, notice we are not guaranteed that
the procedure outlined above results in our original sets of \(\vec{x}\).
The only case in which a guarantee is made is when
our sets are already optimal.

\section{Bayes Test Procedure}
\label{develop--math3465:bayesian:page--testing.adoc---bayes-test-procedure}

As we have shown above, the Bayesian approach is to make a decision
which minimizes the loss given the values \(\vec{x}\) observed.
Then the approach also minimizes the expected loss

\begin{equation*}
E(Loss)
= \int \gamma(\text{optimial }d_i | \vec{x})f(\vec{x}) \ d\vec{x}
= \sum_i \gamma(d_i)P(\text{choose }d_i)
\end{equation*}

by changing the order of summation. Conversely, if we have
a set of \(d_i\) which minimizes the expected loss, then it
must also be optimal for (almost) all \(\vec{x}\).
We call this a \emph{bayes test procedure}.

\begin{admonition-note}[{}]
The reason why we say almost is because of the nature of integrals.
More precisely, the set of \(\vec{x}\) for which the test would
be suboptimal would have measure zero.
\end{admonition-note}

Additionally, notice that we can think of parameter estimation as a
hypothesis test where \(d_i\) states that \(\theta = \theta_i\)
for each \(\theta_i\) in the parameters space.

\section{The standard issue}
\label{develop--math3465:bayesian:page--testing.adoc---the-standard-issue}

Typically, our loss function would have the following form

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{} & {\(d_0\) : Accept \(H_0\)} & {\(d_1\) : Reject \(H_0\)} \\
\hline[\thicktableline]
{\(H_0\) is true (\(\theta \in \Omega_0\))} & {\(0\)} & {\(w_0\)} \\
\hline
{\(H_1\) is true (\(\theta \in \Omega_1\))} & {\(w_1\)} & {\(0\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

where \(\Omega_0\) and \(\Omega_1\) partition the sample space
and \(w_0\) and \(w_1\) are the losses associated with
making Type I and Type II errors respectively.
Then the expected losses are

\begin{equation*}
\gamma(d_0 | \vec{x}) = E(L(\theta; d_0)) = w_1P(\theta \in \Omega_1 | \vec{x})
\quad\text{and}\quad
\gamma(d_1 | \vec{x}) = E(L(\theta; d_1)) = w_0P(\theta \in \Omega_0 | \vec{x})
\end{equation*}

Then, we accept \(H_0\) if \(\gamma(d_0 | \vec{x}) \leq \gamma(d_1 | \vec{x})\)
and reject it otherwise. Equivalently, we reject \(H_0\) if

\begin{equation*}
% w_0 P(\theta \in \Omega_0) \leq w_1 P(\theta \in \Omega_1)
% w_0 P(\theta \in \Omega_0) \leq w_1 [1- P(\theta \in \Omega_0)]
P(\theta \in \Omega_0 | \vec{x}) \leq \frac{w_1}{w_0 + w_1}
\end{equation*}

Furthermore, our expected loss is

\begin{equation*}
\begin{aligned}
E(Loss)
&= \gamma(d_0 | \vec{x})P(\text{choose } d_0) + \gamma(d_1 | \vec{x})P(\text{choose } d_1)
\\&= w_1P(\theta \in \Omega_1 | \vec{x})P(\text{choose } d_0)
    + w_0P(\theta \in \Omega_0 | \vec{x})P(\text{choose } d_1)
\\&= w_1P(\text{choose } d_0 | \theta \in \Omega_1)P(\theta \in \Omega_1)
    + w_0P(\text{choose } d_1 | \theta \in \Omega_0)P(\theta \in \Omega_0)
\\&= w_1\beta(\delta)P(\theta \in \Omega_1)
    + w_0\alpha(\delta)P(\theta \in \Omega_0)
\end{aligned}
\end{equation*}

where \(\alpha(\delta)\)  and \(\beta(\delta)\) are the probabilities
of making type I and II errors respectively.

\subsection{Simple Hypotheses}
\label{develop--math3465:bayesian:page--testing.adoc---simple-hypotheses}

Suppose that our sample space is \(\Omega = \{\theta_0, \theta_1\}\)
such that \(\Omega_i = \{\theta_i\}\) then
the Bayes test procedure, \(\delta\), is the test
which rejects \(H_0\) (chooses \(d_1\))
when

\begin{equation*}
\frac{f(\vec{x} | \theta_1)}{f(\vec{x} | \theta_0)} \geq \frac{a}{b} = \frac{w_0P(\theta = \theta_0)}{w_1P(\theta = \theta_1)}
\end{equation*}

\begin{example}[{Proof}]
Let \(a = w_0P(\theta = \theta_0)\) and \(b = w_1P(\theta = \theta_1)\) and
associate \(d_i\) with the set of \(\vec{x}\) which results \(d_i\)
being chosen. Then,
we want to minimize

\begin{equation*}
\begin{aligned}
E(Loss)
&= a\alpha(\delta) + b\beta(\delta)
\\&= aP(\vec{x} \in d_1 | \theta = \theta_0) + bP(\vec{x} \in d_0 | \theta = \theta_1)
\\&= a\int_{d_1} f(\vec{x} | \theta_0) \ d\vec{x} + b\int_{d_0} f(\vec{x} | \theta_1) \ d\vec{x}
\\&= a\int_{d_1} f(\vec{x} | \theta_0) \ d\vec{x} + b -b\int_{d_1} f(\vec{x} | \theta_1) \ d\vec{x}
\\&= \int_{d_1} af(\vec{x} | \theta_0) - b f(\vec{x} | \theta_1) \ d\vec{x} + b
\end{aligned}
\end{equation*}

Therefore to minimize the above integral, \(d_1\) must consist of \(\vec{x}\) such that

\begin{equation*}
af(\vec{x} | \theta_0) - bf(\vec{x} | \theta_1) \leq 0
\iff \frac{f(\vec{x} | \theta_1)}{f(\vec{x} | \theta_0)} \geq \frac{a}{b} = \frac{w_0P(\theta = \theta_0)}{w_1P(\theta = \theta_1)}
\end{equation*}

That is we reject \(H_0\) when the above inequality holds.
\end{example}

\subsection{Likelihood principle}
\label{develop--math3465:bayesian:page--testing.adoc---likelihood-principle}

The likelihood principle asserts that the decision made on a test should be solely based on the
likelihood ratio of the null and alternative hypotheses.

Bayesian hypothesis testing
follows this principle since \(w_0, w_1\) and the probabilities of \(\theta_0\) and \(\theta_1\)
occurring are all fixed.

However, the frequentist approach does not follow this principle. For example, using
any likelihood ratio test (eg Neyman Pearson), we reject \(H_0\) given a value \(c\)
which depends on our sample size.
\end{document}
