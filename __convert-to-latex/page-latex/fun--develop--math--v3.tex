\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Nearest point approximation}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% complex conjugate
\def\conj#1{\overbar{#1}}

% Title omitted
Previously in \myautoref[{}]{develop--fun:math:page--v2.adoc}, we attempted to find the distance between two lines which eventually
led us to finding the nearest point to a linear space. Although the discussion was general inner
product spaces, it is very matrix heavy and the essence of the argument is hidden due
to a focus on a particular set of vectors instead of the vector space itself. To clarify,
although the following two discussions are equivalent, the second allows is much
more readily understandable

\begin{description}
\item[Previous approach] Given a set of vectors \(\vec{w}_1, \ldots \vec{w}_n\),
    find coordinate vector \(\vec{t}\) such that \(\left\|\sum_{i=1}^n t_i \vec{w}_i - \vec{a}\right\|^2\)
    is minimized.
\item[New approach] Given subspace \(W\) and vector \(\vec{a}\), determine
    \(\vec{w} \in W\) which minimizes \(\left\|\vec{w} - \vec{a}\right\|^2\)
\end{description}

Notice that the first approach is mainly focused on representation. As a result,
we overlooked an important property of \(\vec{w}\). Additionally, in
the second form, any result is easier to apply.

\section{Properties of the nearest point}
\label{develop--fun:math:page--v3.adoc---properties-of-the-nearest-point}

We should be careful as we are yet to prove that there is only one nearest point.
Let \(\vec{a} \in V\) and \(W \leq V\). Then, we say that \(\vec{w} \in W\)
is a \emph{nearest point} of \(\vec{a}\) if

\begin{equation*}
\forall \vec{w}' \in W: \left\|\vec{a} - \vec{w}\right\|^2 \leq \left\|\vec{a} - \vec{w}'\right\|^2
\end{equation*}

Recall that \(\|\vec{x}\|^2 = \abrack{\vec{x}, \vec{x}}\).

\subsection{Characterization of nearest point}
\label{develop--fun:math:page--v3.adoc---characterization-of-nearest-point}

Our first result is that

\begin{equation*}
\vec{w}\ \text{is a nearest point} \iff (\vec{a} - \vec{w}) \perp W
\end{equation*}

\begin{example}[{Proof}]
For the forward direction, let \(\vec{w} \in W\) be a nearest point
of \(\vec{a}\), \(k\in \mathbb{C}\) and \(\vec{u} \in W\). Then,
\(\vec{w} + k\vec{u} \in W\) and hence

\begin{equation*}
\begin{aligned}
    0 &\leq \abrack{\vec{a} - \vec{w} - k\vec{u}, \vec{a} - \vec{w} - k\vec{u}} - \abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}}
    \\&= \abrack{k\vec{u}, k\vec{u}} - \abrack{k\vec{u}, \vec{a} - \vec{w}} - \abrack{\vec{a} - \vec{w}, k\vec{u}}
    \\&= \conj{k}k\abrack{\vec{u}, \vec{u}} - \conj{k}\abrack{\vec{u}, \vec{a} - \vec{w}} - k\abrack{\vec{a} - \vec{w}, \vec{u}}
    \\&= \left(\conj{k} - \frac{\abrack{\vec{a} - \vec{w}, \vec{u}}}{\abrack{\vec{u}, \vec{u}}}\right)\abrack{\vec{u}, \vec{u}}\left(k - \frac{\abrack{\vec{u}, \vec{a} - \vec{w}}}{\abrack{\vec{u}, \vec{u}}}\right)
        - \frac{\abrack{\vec{a} - \vec{w}, \vec{u}}\abrack{\vec{u}, \vec{a} - \vec{w}}}{\abrack{\vec{u}, \vec{u}}}
    \\&= \abrack{\vec{u}, \vec{u}}\left|k - \frac{\abrack{\vec{u}, \vec{a} - \vec{w}}}{\abrack{\vec{u}, \vec{u}}}\right|^2
        - \frac{\left|\abrack{\vec{a} - \vec{w}, \vec{u}}\right|^2}{\abrack{\vec{u}, \vec{u}}}
\end{aligned}
\end{equation*}

Now, since \(k\) is a free variable, we can manipulate it such that the first term becomes zero. Hence, we must have that \(\abrack{\vec{a} - \vec{w}, \vec{u}} = 0\).
Notice that if \(\abrack{\vec{u}, \vec{u}} = 0\), we automatically have this result. Also, since that choice of \(\vec{u}\) was arbitrary,
we have that \((\vec{a} - \vec{w}) \perp W\).

Conversely, if \((\vec{a} - \vec{w}) \perp W\), by use the above equality chain, we have that
\(\forall \vec{u} \in W\)

\begin{equation*}
\begin{aligned}
&\abrack{\vec{a} - \vec{u}, \vec{a} - \vec{u}}
\\&=\abrack{\vec{a} - \vec{w} + \vec{w} - \vec{u}, \vec{a} - \vec{w} + \vec{w} - \vec{u}}
\\&=\abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}} + \abrack{\vec{w} - \vec{u}, \vec{a} - \vec{w}}
    + \abrack{\vec{a} - \vec{w}, \vec{w} - \vec{u}} + \abrack{\vec{w} - \vec{u}, \vec{w} - \vec{u}}
\\&=\abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}} + \abrack{\vec{w} - \vec{u}, \vec{w} - \vec{u}} \quad\text{since } \vec{w} - \vec{u} \in W
\\&\geq \abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}}
\end{aligned}
\end{equation*}

Hence, \(\vec{w}\) is a nearest point as desired.
\end{example}

Intuitively, this makes sense, at least in standard euclidean space. However, it is nice to see that
intuition extends to the general case. Furthermore, it provides a relatively simple test
for the nearest point since we can use any spanning set of vectors.

It is also important to note that this relies on the fact that \(W\) is a linear space. Take for example, a region
with a hole in it. Then, there the difference to the nearest point can often not be orthogonal to the region.

\subsection{Uniqueness of nearest point}
\label{develop--fun:math:page--v3.adoc---uniqueness-of-nearest-point}

Let \(\vec{w}_1\) and \(\vec{w}_2\) be nearest points of \(\vec{a}\);
then, \(\vec{w}_1 = \vec{w}_2\).
This allows us speak of the \emph{the} nearest point and perhaps
inspires us to think of a function which maps vectors to their nearest
points. However, caution is needed as we have not proven that a nearest
point exists.

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
0
&= \abrack{\vec{a} - \vec{w}_1, \vec{a} - \vec{w}_1} - \abrack{\vec{a} - \vec{w}_2, \vec{a} - \vec{w}_2}
\\&= \abrack{\vec{a} - \vec{w}_2 + (\vec{w}_2 - \vec{w}_1), \vec{a} - \vec{w}_2 + (\vec{w}_2 -\vec{w}_1)} - \abrack{\vec{a} - \vec{w}_2, \vec{a} - \vec{w}_2}
\\&= \abrack{\vec{w}_1 - \vec{w}_2, \vec{w}_1 - \vec{w}_2} + \abrack{\vec{a} - \vec{w}_2, \vec{w}_1 - \vec{w}_2} + \abrack{\vec{w}_1 - \vec{w}_2, \vec{a} - \vec{w}_2}
\\&= \abrack{\vec{w}_1 - \vec{w}_2, \vec{w}_1 - \vec{w}_2}
\end{aligned}
\end{equation*}

and hence \(\vec{w}_1 - \vec{w}_2 = \vec{0}\).
\end{example}

\section{Projection onto nearest points}
\label{develop--fun:math:page--v3.adoc---projection-onto-nearest-points}

We claim that the partial function which maps vectors in \(V\) onto their nearest point
in \(W\) is a projection. Furthermore, we claim that the domain of this function is a subspace \(V'\) such that \(W \leq V' \leq V\).
We would denote this partial function \(P_W \in \mathcal{L}(V', W)\).

\begin{example}[{Proof}]
We first see that \(V'\) contains \(W\) since for all \(\vec{w} \in W\)

\begin{itemize}
\item \(\vec{w} \in W\)
\item \(\vec{w} - \vec{w} = \vec{0} \perp W\)
\end{itemize}

Hence \(P_W(\vec{w}) = \vec{w}\). This at the same time proves that \(P_W\) is idempotent.

Next \(V'\) is a linear space and \(P_W\) is linear since if \(P_W(\vec{a}_1) = \vec{w}_1\)
and \(P_W(\vec{a}_2) = \vec{w}_2\),

\begin{itemize}
\item \(\vec{w}_1 + \lambda \vec{w}_2 \in W\)
\item \((\vec{a}_1 - \vec{w}_1), (\vec{a}_2 - \vec{w}_2) \perp W\) implies that
    
    \begin{equation*}
    (\vec{a}_1 + \lambda \vec{a}_2) - (\vec{w}_1 + \lambda \vec{w}_2)) \perp W
    \end{equation*}
\end{itemize}

Hence \(P_W(\vec{a}_1 + \lambda\vec{a}_2) = \vec{w}_1 + \lambda\vec{w}_2 = P_W(\vec{a}_1) + \lambda P_W(\vec{a}_2)\).

\begin{admonition-note}[{}]
Throughout this proof we only needed to prove that there is a nearest point in \(W\) since
we know that it is unique. However, we need to ensure that this nearest point is indeed in \(W\)
otherwise the mapping would not be valid.
\end{admonition-note}
\end{example}

Conversely, by the characterization of nearest points, we see that any orthogonal projection maps points to their nearest points.
Additionally, since we know that \(P_W\) is an orthogonal projection, we can utilize existing strategies to compute
it. Primarily, if \(I = \{\vec{w}_i\}\) forms an orthogonal basis for \(W\), \(P_W\) is simply given by

\begin{equation*}
P_W(\vec{a}) = \sum_{\vec{w}_i \in I} \abrack{\vec{w}_i, \vec{a}} \vec{w}_i
\end{equation*}

We may now have some questions about convergence of the above sum. However, these are implementation details.
\end{document}
