\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cosets and Lagrange’s Theorem}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(G\) be a group, \(H\) be a subgroup of
\(G\) and \(g \in G\). Then, the \emph{left coset} of
\(H\) with representative \(g\) is defined as

\begin{equation*}
gH = \{gh: h \in H\}
\end{equation*}

Similarly, the \emph{right coset} with representative \(g\) is
defined as

\begin{equation*}
Hg = \{hg: h \in H\}
\end{equation*}

Note that if \(G\) is abelian, we simply refer to
\(Hg = gH\) as the \emph{coset} of \(H\) with
representative \(g\).

\section{Nice properties}
\label{develop--math2272:ROOT:page--cosets/index.adoc---nice-properties}

Let \(H\) be a subgroup of \(G\) and
\(g_1, g_2 \in G\), then the following statements are
equivalent where each column is independent

\begin{multicols}{2}
\begin{enumerate}[label=\roman*)]
\item \(g_1 H = g_2H\)
\item \(H g_1^{-1} = H g_2^{-1}\)
\item \(g_1 H \subseteq g_2 H\)
\item \(g_2 \in g_1 H\)
\item \(g_1^{-1}g_2 \in H\)
\end{enumerate}
\columnbreak
\begin{enumerate}[label=\roman*)]
\item \(H g_1 = H g_2\)
\item \(g_1^{-1} H = g_2^{-1} H\)
\item \(H g_1 \subseteq H g_2\)
\item \(g_1 \in H g_2\)
\item \(g_1g_2^{-1} \in H\)
\end{enumerate}\end{multicols}

\begin{admonition-note}[{}]
The second column is equivalent to the first by using
\(g_1^{-1}\) and \(g_2^{-1}\) instead of
\(g_1\) and \(g_2\) respectively and using
\((i)\iff(ii)\) in the first column. Therefore only the first
column would be proven.

\begin{admonition-warning}[{}]
I am not saying that \(g_1H = g_2H\) implies
\(Hg_1 = Hg_2\) but rather that the proofs for both columns
are essentially identical.
\end{admonition-warning}
\end{admonition-note}

\begin{example}[{Proof}]
I’m using the first order I came up with, however, by the end,
everything is equivalent.

\begin{example}[{\((v) \iff (iii)\)}]
\begin{equation*}
\begin{aligned}
        g_1^{-1}g_2 \in H
        &\iff \exists h \in H: g_1^{-1}g_2 =h \\
        &\iff \forall h_1 \in H: \exists h_2 \in H: g_1^{-1}g_2 =h_1h_2^{-1}= h \text{ by letting }h_2 = h^{-1}h_1 \\
        &\iff \forall h_1 \in H: \exists h_2 \in H: g_1h_1 = g_2h_2\\
        &\iff \forall h_1 \in H: g_1h_1 \in g_2H\\
        & g_1H \in g_2H
    \end{aligned}
\end{equation*}
\end{example}

\begin{example}[{\((v) \iff (iv)\):}]
Note

\begin{equation*}
g_1^{-1}g_2 \in H
        \iff \exists h \in H: g_1^{-1}g_2 \in h
        \iff \exists h \in H: g_2 \in g_1h
        \iff g_2 \in g_1H
\end{equation*}
\end{example}

\begin{example}[{\((i) \iff (iii)\):}]
Firstly, clearly, \((i) \implies (iii)\). Conversely, since
\((v) \iff (iii)\)

\begin{equation*}
g_1H \subseteq g_2H \implies g_1^{-1}g_2 \in H \implies g_2^{-1}g_1 \in H \implies g_2H \subseteq g_1H
\end{equation*}

Therefore, \(g_1H = g_2H\).
\end{example}

\begin{example}[{\((i) \iff (v)\):}]
Follow carefully

\begin{equation*}
\begin{aligned}
        Hg_1^{-1} \subseteq Hg_2^{-1}
        &\iff \forall h_1 \in H: h_1g_1^{-1} \in Hg_2^{-1}\\
        &\iff \forall h_1 \in H: \exists h_2 \in H: h_1g_1^{-1} = h_2g_2^{-1}\\
        &\iff \forall h_1 \in H: \exists h_2 \in H: g_1^{-1}g_2 = h_1^{-1}h_2\\
        &\iff \exists h \in H: g_1^{-1}g_2 = h\\
        &\iff g_1^{-1}g_2 \in H
    \end{aligned}
\end{equation*}

Therefore,
\(Hg_1^{-1} = Hg_2^{-1} \implies g_1^{-1}g_2 \in H\).
Conversely,
\(g_1^{-1}g_2 \in H \implies Hg_1^{-1} \subseteq Hg_2^{-1}\)
and

\begin{equation*}
g_1^{-1}g_2 \in H
        \implies g_2^{-1}g_1 \in H
        \implies Hg_2^{-1} \subseteq Hg_1^{-1}
\end{equation*}

and hence the result follows.
\end{example}

 ◻
\end{example}

\section{Equal Cosets}
\label{develop--math2272:ROOT:page--cosets/index.adoc---equal-cosets}

Let \(G\) be a group with subgroup \(H\) and
\(g \in G\) then

\begin{equation*}
gH = Hg = H \iff g \in H
\end{equation*}

This result follows directly from the closure of subgroups.

\section{Partitioning}
\label{develop--math2272:ROOT:page--cosets/index.adoc---partitioning}

Let \(G\) be a group with subgroup \(H\) then the
left cosets of \(H\) partition \(G\). Additionally,
the right cosets of \(H\) also partition \(G\).

\begin{example}[{Proof}]
Only result for left cosets will be proven since the process is similar
for right cosets.

Clearly, the union of set of left cosets is \(G\) since
\(\forall g \in G: g \in gH\). Additionally, suppose that
\(g_1H\) and \(g_2H\) have elements in common. Let
one such element be \(x\). Then
\(x \in g_1H \implies xH = g_1H\) and similarly,
\(x \in g_2H \implies xH=g_2H\). Therefore,
\(g_1H = xH = g_2H\) and hence two left cosets are either
disjoint or equal. Hence, the result follows.

 ◻
\end{example}

\section{Cardinality of a coset}
\label{develop--math2272:ROOT:page--cosets/index.adoc---cardinality-of-a-coset}

Let \(G\) be a group with subgroup \(H\) and
\(a \in G\) then

\begin{equation*}
|H| = |aH| = |Ha|
\end{equation*}

\begin{example}[{Proof}]
On the proof that \(|H| = |aH|\) will be provided as
\(|H| = |Ha|\) is almost identical.

We defined \(f(h) = ah\) then, clearly, \(f\) is
surjective. Furthermore, by the cancellation property of groups

\begin{equation*}
f(h_1) = f(h_2) \implies ah_1 = bh_2 \implies h_1 = h_2
\end{equation*}

therefore \(f\) is also injective and the result follows.

 ◻
\end{example}

\section{Index of a subgroup}
\label{develop--math2272:ROOT:page--cosets/index.adoc---index-of-a-subgroup}

Let \(G\) be a group with subgroup \(H\). Then the
\emph{index} of \(H\) is the number of left cosets of
\(H\). We denote the index as \([G: H\)].

Additionally, the number of left cosets is equal to the number of right
cosets and hence, the index will also be equal to the number of right
cosets of \(H\).

\begin{example}[{Proof}]
Note, the proof given below is quite convoluted (unnecessarily so),
so it might be best to reference the proof of theorem 6.8 in Judson.

We will prove that the cardinality of left cosets is equal to that of
the right cosets by defining a bijection between the two.

Let \(a \in G\). Then, we define
\(Left_a = \{x: aH = xH\}\) and
\(Right_a = \{x: Ha = Hx\}\). Then, \(Left_a\) and
\(Right_a\) can be thought of as equivalence classes of
\(a\) and are hence well defined.

Additionally, we define \(Left = \{Left_a: a \in G\}\) and
\(Right = \{Right_a: a \in G\}\). Then, these two sets have
immediate bijections with the left and right cosets of \(H\)
respectively. Now, we define the function
\(f: Left \to Right\) as

\begin{equation*}
f(Left_a) = Right_{a^{-1}}
\end{equation*}

Firstly, note that the function is well defined since

\begin{equation*}
\begin{aligned}
        Left_a = Left_b \\
        &\iff aH = bH\\
        &\iff Ha^{-1} = Hb^{-1}\\
        & \iff Right_{a^{-1}} = Right_{b^{-1}}
    \end{aligned}
\end{equation*}

Note, the jump from line 2 to 3 was proven before. Also, equivalence
classes are equal iff any of their elements are related. Additionally,
this immediately proves that \(f\) is injective. Furthermore,
\(f\) is surjective since

\begin{equation*}
\forall Right_b \in Right: f(Left_{b^{-1}}) = Right_{(b^{-1})^{-1}} = Right_b
\end{equation*}

Therefore, \(f\) is a bijection from \(Left\) to
\(Right\) and it follows \(|Left| = |Right|\).

 ◻
\end{example}

\section{Lagrange’s Theorem}
\label{develop--math2272:ROOT:page--cosets/index.adoc---lagranges-theorem}

Let \(G\) be a finite group with subgroup \(H\).
Then

\begin{equation*}
[G:H] = \frac{|G|}{|H|} \in \mathbb{N}
\end{equation*}

That is the order of a subgroup always divides the order of its parent
group.

\begin{example}[{Proof}]
The proof of this is quite straight forward. Let
\([G:H\) = k] and \(a_1H \ldots a_kH\) be the left
cosets of \(H\). Then since the left cosets are disjoint

\begin{equation*}
|G|
        = \left|\bigcup_{i=1}^k a_i H\right|
        = \bigcup_{i=1}^k |a_i H|
        = \bigcup_{i=1}^k |H|
        = k|H|
        = [G:H]|H|
\end{equation*}

and the result follows.

 ◻
\end{example}

\subsection{Corollaries}
\label{develop--math2272:ROOT:page--cosets/index.adoc---corollaries}

Let \(G\) be a group and \(g \in G\) then

\begin{enumerate}[label=\roman*)]
\item the order of an element \(g\) divides \(G\).
\item \(g^{|G|} = e\) (the identity in \(G\)).
\item if \(G\) has prime order, then \(G\) is cyclic.
\end{enumerate}

\begin{example}[{Proof}]
Note that the order of \(g\) is equal to the order of the
group generated by \(g\). Then by Lagrange’s theorem
\(|gp(g)|\) divides \(|G|\) since
\(gp(g)\) is a subgroup of \(G\).

This follows immediately from part (i) since \(g^k = e\) iff
the order of \(g\) divides \(k\).

Suppose \(|G|\) is prime then \(|G| \geq 2\) and
\(\exists x \in G: x \neq e\). Then the order of
\(x\) is not \(1\) and divides \(|G|\).
Since \(|G|\) is prime the order of \(x\) is equal
to \(|G|\). Therefore, since \(G\) has an element
with order equal to the order of the group, \(G\) is cyclic.

 ◻
\end{example}

\section{Congruence}
\label{develop--math2272:ROOT:page--cosets/index.adoc---congruence}

Let \(G\) be a group with subgroup \(H\). Then
\emph{\(a\) is congruent to \(b\) modulo \(H\)}
iff \(ab^{1} \in H\). This is written as

\begin{equation*}
a \equiv b \ (\mathrm{mod}\ H) \iff ab^{-1} \in H
\end{equation*}

Note that this relation is an equivalence relation since

\begin{itemize}
\item \(aa^{-1} = e \in H \implies a \equiv a \ (\mathrm{mod}\ H)\)
\item \(a \equiv b \ (\mathrm{mod}\ H) \implies ab^{-1} \in H \implies ba^{-1} \in H \implies b\equiv a \ (\mathrm{mod}\ H)\)
\item \(a \equiv b \ (\mathrm{mod}\ H)\ \land\ b \equiv c \ (\mathrm{mod}\ H)\)
\end{itemize}

\begin{equation*}
\begin{aligned}
&\implies ab^{-1} \in H\ \land\ bc^{-1} \in H
\\&\implies ab^{-1}bc^{-1} = ac^{-1} \in H
\\&\implies a\equiv c\ (\mathrm{mod}\ H)
\end{aligned}
\end{equation*}
\end{document}
