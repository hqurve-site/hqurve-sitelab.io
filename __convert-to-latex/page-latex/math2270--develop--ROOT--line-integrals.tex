\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Line Integrals}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
\section{Definition}
\label{develop--math2270:ROOT:page--line-integrals.adoc---definition}

The line integral of a vector field \(\bm{F}\) over a curve
\(C\) is

\begin{equation*}
\int_C \bm{F}\bm{\cdot}\bm{dr}
    = \int_C F_1 dx_1 + F_2 dx_2 + \cdots + F_n dx_n
\end{equation*}

\subsection{Properties}
\label{develop--math2270:ROOT:page--line-integrals.adoc---properties}

As with the normal integral, the line integral is both linear and
additive over their region. That is

\begin{equation*}
\int_C (\alpha\bm{F}_1 + \beta\bm{F}_2)\bm{\cdot}\bm{dr}
    = \alpha\int_C \bm{F}_1\bm{\cdot}\bm{dr} + \beta\int_C \bm{F}_2\bm{\cdot}\bm{dr}
\end{equation*}

and if \(C_1\) and \(C_2\) are disjoint regions
(intersection has measure \(0\)) and
\(C = C_1 \cup C_2\) then

\begin{equation*}
\int_{C} \bm{F}\bm{\cdot}\bm{dr}
    = \int_{C_1} \bm{F}\bm{\cdot}\bm{dr} + \int_{C_2} \bm{F}\bm{\cdot}\bm{dr}
\end{equation*}

Furthermore, the curve in the reverse direction is \(-C\) and
its integral is negated. That is

\begin{equation*}
\int_{-C} \bm{F}\bm{\cdot}\bm{dr} = -\int_{C} \bm{F}\bm{\cdot}\bm{dr}
\end{equation*}

\subsection{Closed curves}
\label{develop--math2270:ROOT:page--line-integrals.adoc---closed-curves}

If \(C\) is a closed curve (endpoints are the same), the
integral is instead written as

\begin{equation*}
\oint_C \bm{F} \bm{\cdot}\bm{dr}
\end{equation*}

Note that positive orientation in the plane refers to counterclockwise
motion.

\subsection{Parameterized computation}
\label{develop--math2270:ROOT:page--line-integrals.adoc---parameterized-computation}

One way to compute the line integral is to parameterize the curve with
respect to some real number \(t\). Then

\begin{equation*}
\int_C \bm{F} \bm{\cdot}\bm{dr}
    = \int_a^b \bm{F}(\bm{r}(t)) \bm{\cdot}\nabla(\bm{r}(t)) dt
\end{equation*}

where \(\nabla \bm{r}(t)\) is the gradient.\newline
Note that \(\nabla (\bm{r}(t))\) is also sometimes written as
\(\bm{r}'(t)\)

\section{Independence of path}
\label{develop--math2270:ROOT:page--line-integrals.adoc---independence-of-path}

A line integral is said to be independent of path over region
\(D\) if

\begin{equation*}
\int_{C_1} \bm{F} \bm{\cdot}\bm{dr} = \int_{C_2} \bm{F} \bm{\cdot}\bm{dr}
\end{equation*}

whenever \(C_1\) and \(C_2\) (in \(D\))
have the same end points. In other words, the integral only depends on
its endpoints. This statement has various equivalents outlined below.

\subsection{Closed Paths}
\label{develop--math2270:ROOT:page--line-integrals.adoc---closed-paths}

If the integral is independent of path, then the integral of a closed
path is \(0\). Consider any closed path composed of paths
\(C_1\) and \(C_2\). Then

\begin{equation*}
\int_{C_1 \cup C_2} \bm{F} \bm{\cdot}\bm{dr}
    = \int_{C_1} \bm{F} \bm{\cdot}\bm{dr} + \int_{C_2} \bm{F} \bm{\cdot}\bm{dr}
    = \int_{C_1} \bm{F} \bm{\cdot}\bm{dr} - \int_{-C_2} \bm{F} \bm{\cdot}\bm{dr}
    = 0
\end{equation*}

since curves \(C_1\) and \(-C_2\) have the same
endpoints. Conversely, if integral of all closed paths is
\(0\) consider two curves \(C_1\) and
\(-C_2\) with the same endpoints. Then
\(C_1 \cup C_2\) forms a closed path and by the above
equation, the integral is independent of path.

\subsection{Conservative Fields}
\label{develop--math2270:ROOT:page--line-integrals.adoc---conservative-fields}

Consider the smooth curve \(C\) defined by
\(\bm{r}(t)\) from \(a \leq t \leq b\). Then if
\(\bm{F} = \nabla \phi\) (conservative)

\begin{equation*}
\int_C \bm{F} \bm{\cdot}\bm{dr}
    = \int_a^b \nabla \phi(\bm{r}(t))  \bm{\cdot}\bm{r}'(t) dt
    = \int_a^b \frac{d}{dt} \phi(\bm{r}(t)) dt
    =\phi (\bm{r}(b)) - \phi (\bm{r}(a))
\end{equation*}

by the chain rule (multivariable one) and fundamental theorem of
calculus. Note that the curve can also be piecewise smooth; that is not
smooth over region with measure 0. Additionally, the converse is also
true under the added condition that \(D\) is open and
connected. To show this fact, consider the function

\begin{equation*}
\phi(\bm{x}) = \int_{\bm{x}_0}^{\bm{x}} \bm{F} \bm{\cdot}\bm{dr}
\end{equation*}

where \(\bm{x}_0\) is an arbitrary point in \(D\).
Now all that is required is to show that the component \(F_i\)
is equal to \(\frac{\partial \phi}{\partial x_i}\). Since
\(D\) is open, there exists a ball around
\(\bm{x}\); choose a point \(\bm{x}_1\) in this ball
with all components excluding \(x_i\) equal to that of
\(\bm{x}\). Then

\begin{equation*}
\phi(\bm{x}) = \int_{\bm{x}_0}^{\bm{x}_1} \bm{F} \bm{\cdot}\bm{dr} + \int_{\bm{x}_1}^{\bm{x}} \bm{F} \bm{\cdot}\bm{dr}
\end{equation*}

Notice that the first integral is independent of \(x_i\).
Also, consider the straight line path from \(\bm{x}_1\) to
\(\bm{x}\) then all components but \(x_i\) in the
second integral are constant; so \(\bm{dr}\) has zeros for all
components except \(x_i\). Then

\begin{equation*}
\frac{\partial \phi(\bm{x})}{\partial x_i} = 0 + \frac{\partial }{\partial x_i}\int_{\bm{x}_1}^{\bm{x}} F_i\, dx_i = F_i(\bm{x})
\end{equation*}

The above procedure can be repeated for all components and therefore
\(\bm{F}\) is conservative.

\section{Greens Theorem}
\label{develop--math2270:ROOT:page--line-integrals.adoc---greens-theorem}

Greens theorem relates the integral of a curved curve \(C\) in
the plane \(\mathbb{R}^2\) to the region, \(D\),
which it encloses by the following equation

\begin{equation*}
\oint_C P\, dx + Q\, dy = \iint_D \frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y}\, dA
\end{equation*}

where \(P\) and \(Q\) are continuously
differentiable in \(D\). Or alternatively,

\begin{equation*}
\oint_C \bm{F} \bm{\cdot}\bm{dr} = \iint_D \nabla \times \bm{F}\, dA = \iint_D \bm{d}(\bm{F}\bm{\cdot}\bm{dx})
\end{equation*}

\subsection{Proof for simple regions}
\label{develop--math2270:ROOT:page--line-integrals.adoc---proof-for-simple-regions}

If \(D\) is can be written as both a type \(I\) and
type \(II\) region, then

\begin{equation*}
\begin{aligned}
{1}
    & D = \{(x,y) | a\leq x \leq b, f_1(x) \leq y \leq f_2(x)\}\\
    & D = \{(x,y) | c\leq y \leq d, g_1(y) \leq x \leq g_2(y)\}\end{aligned}
\end{equation*}

\subsubsection{Part 1}
\label{develop--math2270:ROOT:page--line-integrals.adoc---part-1}

\begin{equation*}
\begin{aligned}
    \iint_D \frac{\partial P}{\partial y}\,dA
    &= \int_a^b \int_{f_1(x)}^{f_2(x)} \frac{\partial P}{\partial y} \, dy\, dx\\
    &= \int_a^b P(x, f_2(x)) - P(x, f_1(x)) \, dx\\
    &= \int_{-C} P \, dx\\
    &= -\int_C P \, dx\end{aligned}
\end{equation*}

The send to last equivalence follows by considering the cycle

\begin{equation*}
(a, f_1(a))
    \rightarrow (b, f_1(b))
    \rightarrow (b, f_2(b))
    \rightarrow (a, f_2(a))
    \rightarrow (a, f_1(a))
\end{equation*}

Note that integral along the path where \(a\) and
\(b\) are constant is \(0\).

\subsubsection{Part 2}
\label{develop--math2270:ROOT:page--line-integrals.adoc---part-2}

\begin{equation*}
\begin{aligned}
    \iint_D \frac{\partial Q}{\partial x}\,dA
    &= \int_c^d \int_{g_1(y)}^{g_2(y)} \frac{\partial Q}{\partial x} \, dx\, dy\\
    &= \int_c^d Q(y, g_2(y)) - Q(y, g_1(y)) \, dy\\
    &= \int_C Q \, dy\end{aligned}
\end{equation*}

The last equivalence follows by considering the cycle

\begin{equation*}
(c, g_1(c))
    \rightarrow (c, g_2(c))
    \rightarrow (d, g_2(d))
    \rightarrow (d, g_1(d))
    \rightarrow (c, g_1(c))
\end{equation*}

\subsubsection{Conclusion}
\label{develop--math2270:ROOT:page--line-integrals.adoc---conclusion}

By combining the two above parts, the result is obtained.

\subsection{Extension to finite union of simple regions}
\label{develop--math2270:ROOT:page--line-integrals.adoc---extension-to-finite-union-of-simple-regions}

Consider the region \(D\) which can be split into two regions
which greens theorem holds. Let the regions be \(D_1\) and
\(D_2\) with curves boundary \(C_1 \cup C_3\) and
\(C_2 \cup -C_3\) respectfully where \(C_3\) is the
curve along which \(D_1\) and \(D_2\) meet. Then

\begin{equation*}
\begin{aligned}
    \int_C \bm{F} \bm{\cdot}\bm{dr}
    &= \int_{C_1\cup C_2} \bm{F} \bm{\cdot}\bm{dr}\\
    &= \int_{C_1\cup C_2} \bm{F} \bm{\cdot}\bm{dr} + \int_{C_3} \bm{F} \bm{\cdot}\bm{dr} + \int_{-C_3} \bm{F} \bm{\cdot}\bm{dr}\\
    &= \int_{C_1\cup C_3} \bm{F} \bm{\cdot}\bm{dr} + \int_{C_2 \cup -C_3} \bm{F} \bm{\cdot}\bm{dr}\\
    &= \iint_{D_1} \nabla \times \bm{F}\, dA + \iint_{D_2} \nabla \times \bm{F}\, dA\\
    &= \iint_{D} \nabla \times \bm{F}\, dA\end{aligned}
\end{equation*}
\end{document}
