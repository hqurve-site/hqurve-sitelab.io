\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Multivariate normal}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Suppose that the random sample \(\vec{X}_1, \ldots \vec{X}_n\) is multivariate
is \(p\) variables.

\section{Hotellings T\textsuperscript{2} test}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---hotellings-t-latex-backslashtextsuperscript-latex-openbrace2-latex-closebrace-test}

We can either assume that the sample comes from a multivariate normal distribution,
or we can use the central limit theorem, provided that \(n-p\) is large
and the population mean and variance are both finite.

Let \(\overbar{\vec{X}}\)  be the sample
mean
and \(\mat{S}\) be the (unbiased) sample
covariance matrix.
Then,

\begin{equation*}
T^2 = n(\overbar{\vec{X}}-\vec{\mu})^T
\mat{S}^{-1}(\overbar{\vec{X}}-\vec{\mu})
\sim  \frac{(n-1)p}{n-p}F_{p,n-p}
\end{equation*}

\begin{proposition}[{Propterties of T\textsuperscript{2} statistic}]
The \(T^2\) statistic defined above is

\begin{itemize}
\item Invariant to transformations of the form \(\vec{Z} = \mat{A}\vec{X} + \vec{b}\)
    where \(\mat{A}\) is an invertible square matrix.
        This is known to as measurement equivariance.
\item Sensitive to departures from normality
\item The likelihood ratio test statistic
\item The uniformly most powerful test
\item The union-intersection test statistic
\end{itemize}
\end{proposition}

\begin{proposition}[{Equivalent formulation of T\textsuperscript{2} statistic}]
See Equation 5-15 of Applied Multivariate Statistics (6th)

\begin{equation*}
\begin{aligned}
T^2
&= \frac{(n-1)|\hat{\mat{\Sigma}}_0|}{|\hat{\mat{\Sigma}}|} - (n-1)
\\&= \frac{(n-1)\left|\sum_{i=1}^n (\vec{x}_i - \vec{\mu})(\vec{x}_i - \vec{\mu})^T\right|}
{\left|\sum_{i=1}^n (\vec{x}_i - \overbar{\vec{x}})(\vec{x}_i - \overbar{\vec{x}})^T\right|} - (n-1)
\end{aligned}
\end{equation*}
\end{proposition}

\begin{proposition}[{Limiting distribution of T\textsuperscript{2}}]
If \(n-p\) is large, then

\begin{equation*}
T^2 \sim \chi^2_p
\end{equation*}
\end{proposition}

\begin{admonition-tip}[{}]
The \texttt{ICSNP} and \texttt{mvtnorm} packages are useful for conducting the Hotellings \(T^2\) test.
\end{admonition-tip}

\section{Two sample tests}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---two-sample-tests}

The two sample tests are very similar to the univariate case.
In these tests, the null hypothesis is that difference of the means of two sets
\(\vec{X}_i\) and \(\vec{Y}_i\) is some known value.
Let the \(\vec{X}_i\) have \(n\) observations and the \(\vec{Y}_i\) have
\(m\) observations.

\begin{description}
\item[Paired] If the data is paired, we can simply take their differences
    \(\vec{D}_i = \vec{X}_i-\vec{Y}_i\) and perform the Hotellings \(T^2\) test.
\end{description}

\begin{description}
\item[Two sample pooled] 
    
    \begin{itemize}
    \item The \(\vec{X}_i\) and \(\vec{Y}_i\) are independent
    \item The population covariance matrices is unknown
    \item The population covariance matrices are equal
        
        The test statistic is
        
        \begin{equation*}
        T^2 =
        \frac{1}{1/n + 1/m}
        \left[(\overbar{\vec{X}} - \overbar{\vec{Y}}) - (\vec{\mu}_X-\vec{\mu_Y})\right]^T
        \mat{S}_p^{-1}
        \left[(\overbar{\vec{X}} - \overbar{\vec{Y}}) - (\vec{\mu}_X-\vec{\mu_Y})\right]
        \sim
        \frac{(n+m-2)p}{(n+m-1 -p)}F_{p, n+m-1-p}
        \end{equation*}
        
        where
        
        \begin{equation*}
        \mat{S}_p = \frac{(n-1)\mat{S}_X + (m-1)\mat{S}_Y}{n+m-2}
        \end{equation*}
        
        and \(\mat{S}_X\) and \(\mat{S}_Y\) are the unbiased sample
        covariance matrices for the two samples.
        
        \begin{admonition-note}[{}]
        The coefficient vector for the linear
        combination \textbf{most responsible} for rejection is proportional
        to
        \end{admonition-note}
        
        \begin{equation*}
        \mat{S}_p^{-1}
        \left[(\overbar{\vec{X}} - \overbar{\vec{Y}}) - (\vec{\mu}_X-\vec{\mu_Y})\right]
        \end{equation*}
    \end{itemize}
\item[Welch's Equivalent] 
    
    \begin{itemize}
    \item The \(\vec{X}_i\) and \(\vec{Y}_i\) are independent
    \item The population covariance matrices is unknown
    \item The population covariance matrices are not equal
        
        The test statistic is
        
        \begin{equation*}
        T^2 =
        \left[(\overbar{\vec{X}} - \overbar{\vec{Y}}) - (\vec{\mu}_X-\vec{\mu_Y})\right]^T
        \left(\frac{\mat{S}_X}{n} + \frac{\mat{S}_Y}{m}\right)^{-1}
        \left[(\overbar{\vec{X}} - \overbar{\vec{Y}}) - (\vec{\mu}_X-\vec{\mu_Y})\right]
        \sim
        \frac{\nu p}{\nu -p +1}F_{p, \nu - p +1}
        \end{equation*}
        
        where
        
        \begin{equation*}
        \mat{S} = \frac{\mat{S}_X}{n} + \frac{\mat{S}_Y}{m}
        \quad\text{and}\quad
        \nu = \frac{p(1+p)}{
        \sum_{i=1}^2 \frac{1}{n_i^3} \left\{
            \operatorname{tr}\left[(\mat{S}_i\mat{S}^{-1})^2\right]
            + \left(\operatorname{tr}\left[\mat{S}_i\mat{S}^{-1}\right]\right)^2
        \right\}
        }
        \end{equation*}
        
        and \(\mat{S}_X\) and \(\mat{S}_Y\) are the unbiased sample
        covariance matrices for the two samples.
        
        \begin{admonition-note}[{}]
        If the sample size is large, the distribution tends to \(\chi^2_p\).
        \end{admonition-note}
    \end{itemize}
\end{description}

\section{Confidence intervals}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---confidence-intervals}

\begin{admonition-note}[{}]
See section 5.4 of Applied Multivariate Statistics (6th)
\end{admonition-note}

Confidence intervals in multiple dimensions are a bit difficult.
We will focus on the confidence interval for the population mean.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/multivariate-normal/confidence-intervals}
\caption{Illustration of different confidence intervals for population mean}
\end{figure}

\subsection{Confidence ellipsoid}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---confidence-ellipsoid}

The first most obvious choice of confidence interval for \(\vec{\mu}\) is the \textbf{confidence ellipsoid}.
A \(100(1-\alpha)\%\) confidence region for population mean is all \(\vec{\mu}\)
such that

\begin{equation*}
n(\overbar{\vec{x}} - \vec{\mu})^T\mat{S}^{-1}(\overbar{\vec{x}} - \vec{\mu}) \leq \frac{p(n-1)}{n-p}F_{p,n-p}(\alpha)
\end{equation*}

Let \((\lambda_i, \vec{e}_i)\) be the normalized eigenpairs of \(\mat{S}\).
Then, the confidence region defined above is an ellipsoid centered at \(\overbar{\vec{x}}\) with
semi-axes

\begin{equation*}
\pm \sqrt{\lambda_i}\sqrt{\frac{p(n-1)}{n(n-p)}F_{p,n-p}(\alpha)} \vec{e}_i
\end{equation*}

\subsection{Simultaneous Confidence intervals}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---simultaneous-confidence-intervals}

Let \(\vec{a}\) be any vector. Then, a \(100(1-\alpha)\%\) confidence
interval for \(\vec{a}^T\vec{\mu}\) is

\begin{equation*}
\left(
\vec{a}^T\overbar{\vec{X}}- \sqrt{\frac{p(n-1)}{n(n-p)}F_{p,n-p}(\alpha)\vec{a}^T\mat{S}\vec{a}}
,\quad
\vec{a}^T\overbar{\vec{X}}+ \sqrt{\frac{p(n-1)}{n(n-p)}F_{p,n-p}(\alpha)\vec{a}^T\mat{S}\vec{a}}
\right)
\end{equation*}

By setting the \(\vec{a}\) to be the standard basis vectors in the above,
we get the \textbf{\(T^2\) simultaneous} confidence intervals.

\subsection{One-at-a-time}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---one-at-a-time}

The \textbf{one-at-a-time} intervals are simply made by producing each
of the univariate confidence intervals.

\begin{equation*}
\overbar{x}_i - t_{n-1}(\alpha/2)\sqrt{\frac{s_{ii}}{n}}
\leq \mu_i \leq
\overbar{x}_i + t_{n-1}(\alpha/2)\sqrt{\frac{s_{ii}}{n}}
\end{equation*}

for \(i=1,\ldots p\).

\subsection{Bonferroni}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---bonferroni}

The \textbf{Bonferroni} confidence intervals are defined as

\begin{equation*}
\overbar{x}_i - t_{n-1}\left(\frac{\alpha}{2p}\right)\sqrt{\frac{s_{ii}}{n}}
\leq \mu_i \leq
\overbar{x}_i + t_{n-1}\left(\frac{\alpha}{2p}\right)\sqrt{\frac{s_{ii}}{n}}
\end{equation*}

for \(i=1,\ldots p\).

Note that these are very similar to the one-at-a-time confidence intervals
but with a smaller range.

\section{Profile Analysis}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---profile-analysis}

Suppose we have a multivariate random variable \(\vec{X}\)
where the entries are measured in similar units (scale and offset).
Often such random vectors \(\vec{X}\) may come from \textbf{repeated measures}
and we may be interested in how the means \(\mu_i\) evolve.
If we plot \((i, \mu_i)\), this is the \textbf{profile} of \(\vec{X}\).

Now, suppose that we have \(2\) populations, we can plot each
of their respective profiles and ask the following questions:

\begin{itemize}
\item Are the profiles parallel?
\item Are the profiles coincident?
\item Are the profiles flat/level?
\end{itemize}

Each of these questions can be rephrased as multivariate hypothesis tests
using contrast matrices.

\subsection{Parallel}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---parallel}

To test if two profiles are \textbf{parallel}, we can test
if \(\mu_{1i} - \mu_{1j}\) is equal to
\(\mu_{2i} - \mu_{2j}\) for all \(i,j\).
There are multiple (equivalent) null hypotheses which can
be used to test this:

\begin{itemize}
\item \(H_0\): \(\mu_{11}- \mu_{1i} = \mu_{21}-\mu_{2i}\) for \(i=2,\ldots p\).
    Let
    
    \begin{equation*}
    \mat{C} = \begin{bmatrix}
    1 & -1 \\
    1 & & -1\\
    \vdots & & & \ddots\\
    1 & & & & -1\\
    \end{bmatrix}
    \end{equation*}
\item \(H_0\): \(\mu_{1i}- \mu_{1(i-1)} = \mu_{2i}-\mu_{2(i-1)}\) for \(i=2,\ldots p\).
    Let
    
    \begin{equation*}
    \mat{C} = \begin{bmatrix}
    1 & -1 \\
      & 1& -1\\
      &  &  \ddots & \ddots\\
      &  &    & 1 & -1\\
    \end{bmatrix}
    \end{equation*}
\end{itemize}

where \(\mat{C}\) is a \((p-1)\times p\) matrix.
So, we test

\begin{itemize}
\item \(H_0: \mat{C}\vec{\mu}_1 = \mat{C}\vec{\mu}_2\)
\item \(H_1: \mat{C}\vec{\mu}_1 \neq \mat{C}\vec{\mu}_2\)
\end{itemize}

We then test using the test statistic

\begin{equation*}
T^2 = \frac{1}{1/n_1 + 1/n_2}
\left(\vec{\overbar{X}}_1 - \vec{\overbar{X}}_2\right)^T\mat{C}^T
(\mat{C}\mat{S}_p\mat{C}^T)^{-1}
\mat{C}\left(\vec{\overbar{X}}_1 - \vec{\overbar{X}}_2\right)
\sim
\frac{(n_1 + n_2 - 2)(p-1)}{n_1 + n_2 - p} F_{p-1, n_1 +n_2 -p}
\end{equation*}

where \(\mat{S}_p\) is the pooled sample covariance matrix.

\begin{admonition-note}[{}]
This is the same test statistic obtained by first transforming the data
and then performing the two sample T\textsuperscript{2}-test.
\end{admonition-note}

\begin{admonition-note}[{}]
Both tests are equivalent since the T\textsuperscript{2} statistic is invariant
to transformations of \(\vec{Z} = \mat{A}\mat{C}\vec{X}\) where
\(\mat{A} \in \mathbb{R}^{(p-1)\times(p-1)}\) is the invertible
matrix which transforms between the two contrast matrices.
\end{admonition-note}

\subsection{Coincident given parallel}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---coincident-given-parallel}

To test if two profiles are \textbf{coincident} given that they are parallel, we can test
if \(\sum_i \mu_{1i} = \sum_i \mu_{2i}\).
So, we test

\begin{itemize}
\item \(H_0: \vec{1}^T\vec{\mu}_1 = \vec{1}^T\vec{\mu}_2\)
\item \(H_1: \vec{1}^T\vec{\mu}_1 \neq \vec{1}^T\vec{\mu}_2\)
\end{itemize}

We then test using the test statistic

\begin{equation*}
T^2 =
\frac{\left(\vec{1}^T(\vec{\overbar{x}}_1 - \vec{\overbar{x}})\right)^2}{
\left(\frac{1}{n_1} + \frac{1}{n_2}\right)\vec{1}^T\mat{S}_p\vec{1}
}
\sim F_{1, n_1+n_2-2}
\end{equation*}

where \(\mat{S}_p\) is the pooled sample covariance matrix.

\begin{admonition-note}[{}]
This is derived from the two sample T\textsuperscript{2} test.
\end{admonition-note}

\subsection{Flat/level given coincident}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---flatlevel-given-coincident}

To test if the profiles are \textbf{flat/level} given that they are coincident, we can test if
if \(\mu_{i} = \mu{j}\) for all \(i,j\).
There are multiple (equivalent) null hypotheses which can
be used to test this:

\begin{itemize}
\item \(H_0\): \(\mu_{1}- \mu_{i} = 0\) for \(i=2,\ldots p\).
    Let
    
    \begin{equation*}
    \mat{C} = \begin{bmatrix}
    1 & -1 \\
    1 & & -1\\
    \vdots & & & \ddots\\
    1 & & & & -1\\
    \end{bmatrix}
    \end{equation*}
\item \(H_0\): \(\mu_{i}- \mu_{i-1} = 0\) for \(i=2,\ldots p\).
    Let
    
    \begin{equation*}
    \mat{C} = \begin{bmatrix}
    1 & -1 \\
      & 1& -1\\
      &  &  \ddots & \ddots\\
      &  &    & 1 & -1\\
    \end{bmatrix}
    \end{equation*}
\end{itemize}

where \(\mat{C}\) is a \((p-1)\times p\) matrix.
So, we test

\begin{itemize}
\item \(H_0: \mat{C}\vec{\mu} = \vec{0}\)
\item \(H_1: \mat{C}\vec{\mu} \neq \vec{0}\)
\end{itemize}

We then test using the test statistic

\begin{equation*}
T^2 = (n_1 + n_2)\vec{\overbar{X}}\mat{C}^T
(\mat{C}\mat{S}\mat{C}^T)^{-1}
\mat{C}\vec{\overbar{X}}
\sim
\frac{(n_1 +n_2-1)(p-1)}{n +n_2- (p-1)} F_{p-1, n_1 + n_2- (p-1)}
\end{equation*}

where \(\vec{\overbar{X}}\) is the overall sample mean
and \(\vec{S}\) is the overall covariance matrix.

\subsection{Implementation in R}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---implementation-in-r}

In order to perform profile analysis in \texttt{R} we use the \texttt{profileR}
library. Note that profile analysis
can be performed with more than two groups, and results in a MANOVA
of reduced dimension.
The library handles all of this for us

\begin{listing}[{}]
library(profileR)

# dataframe with columns: treatment, x1, .. xp
data

# find treatment means and plot curves
treatment.means <- aggregate(. ~ treatment, data, mean)
profileplot(
    treatment.means[,2:(p+1)],
    person.id=treatment.means$treatment,
    standardize=FALSE
)

# perform profile analysis
mod <- pbg(data[,2:(p+1)], group=$data$treatment, original.names=TRUE)
summary(mod)
\end{listing}

\section{Growth curves}
\label{develop--stats:ROOT:page--hypothesis-tests/multivariate-normal.adoc---growth-curves}

\begin{admonition-todo}[{}]

\end{admonition-todo}
\end{document}
