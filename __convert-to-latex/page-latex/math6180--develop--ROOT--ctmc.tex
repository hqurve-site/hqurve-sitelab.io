\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Continuous Time Markov Chains}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
The difference between a (stationary) continuous time markov chain (CTMC) and a DTMC is that the time spent
at each state is no longer a constant fixed number.
In this section, we study CTMCs.

\section{Definition}
\label{develop--math6180:ROOT:page--ctmc.adoc---definition}

Consider the collection of random variables \(\{X(t): t\geq 0\}\).
This collection is called a \textbf{continuous time Markov chain} (CTMC) if it satisfies
the \textbf{Markovian property}

\begin{equation*}
P(X(t+s) = j \ | \ X(s) = i, \ X(u) = x(u), 0 \leq u < s)
= P(X(t+s) = j \ | \ X(s) = i)
\end{equation*}

That is, the probability that we are in state \(j\) at time
\(t+s\) only depends on the latest known state, \(X(s) = i\).

We say that the CTMC has \textbf{stationary} transition probabilities
if \(P(X(t+s) = j \ | \ X(s) = i)\) does not depend on \(s\).
We will assume this for the remainder of the section.

\begin{proposition}[{Time spent at each state is exponentially distributed}]
Suppose that \(X(0) = i\).
Let \(T_i\) be the time spent at state \(i\).
Then, \(T_i\) is an exponential random variable.
We denote the rate of this exponential random variable to be \(\nu_i\)

\begin{proof}[{}]
Note that \(T_i\) is a memoryless continuous random variable since

\begin{equation*}
\begin{aligned}
P(T_i > t + s \ | \ X(0) = i)
&= P(X(u) = i \text{ for } 0 < u \leq t+s \ | \ X(0) = i)
\\&= P(X(u) = i \text{ for } t < u \leq t+s \ | \ X(u) = i \text{ for } 0 \leq u \leq t)P(X(u) = i \text{ for } 0 < u \leq t \ | \ X(0) = i)
\\&= P(X(u) = i \text{ for } t < u \leq t+s \ | \ X(u) = i \text{ for } 0 \leq u \leq t)P(T_i \geq t \ | \ X(0) = i)
\\&= P(X(u) = i \text{ for } t < u \leq t+s \ | \ X(t) = i)P(T_i > t \ | \ X(0) = i)\quad\text{by markovian property}
\\&= P(X(u) = i \text{ for } 0 < u \leq s \ | \ X(0) = i)P(T_i > t \ | \ X(0) = i)\quad\text{by stationarity}
\\&= P(T_i > s \ | \ X(0) = i)P(T_i > t \ | \ X(0) = i)
\end{aligned}
\end{equation*}
\end{proof}
\end{proposition}

\subsection{Notation}
\label{develop--math6180:ROOT:page--ctmc.adoc---notation}

\begin{description}
\item[Transition probabilities] Given that \(X(0) = i\), we may want to know
    the probability that \(X(t) = j\).
    
    \begin{equation*}
    P_{ij}(t) = P(X(t) = j \ | \ X(0) = i)
    \end{equation*}
\item[Rate of leaving state] Given that the process is at state \(i\),
    the process stays at state \(i\) for an \(Exp(\nu_i)\) time
    before changing state.
\item[Next state probabilities] Given that the process
    is leaving state \(i\), we desire to know the probability that
    the next state is \(j\).
    This is also denoted \(P_{ij}\).
    Note that
    
    \begin{equation*}
    P_{ii} = 0,
    \quad\text{and}\quad
    \sum_{j} P_{ij} = 1
    \end{equation*}
    
    Note that we are yet to show that these probabilities are well defined
    (based on the transition probabilities \(P_{ij}(t)\)).
\item[Instantaneous transition rates] Define \(q_{ij} = \nu_iP_{ij}\) for \(i\neq j\).
    This may be interpreted as the rate at which the process which is in
    state \(i\) transitions to state \(j\).
    Also, define \(q_{ii} = -\sum_{j\neq i} q_{ij}\).
    Now, define the rate matrix \(Q = (q_{ij})\).
    Note that by definition of the diagonal entries, each row sums to zero.
\end{description}

\subsection{Some results}
\label{develop--math6180:ROOT:page--ctmc.adoc---some-results}

\begin{lemma}[{}]
For \(i \neq j\)

\begin{equation*}
P_{ii}(h) = 1- \nu_i h + o(h)
\quad\text{and}\quad
P_{ij}(h) = q_{ij} h + o(h)
\end{equation*}

or equivalently

\begin{equation*}
\lim_{h\to 0} \frac{1-P_{ii}(h)}{h} = \nu_i
\quad\text{and}\quad
\lim_{h\to 0} \frac{P_{ij}(h)}{h} = q_{ij}
\end{equation*}

\begin{admonition-todo}[{}]
proof
\end{admonition-todo}
\end{lemma}

\begin{theorem}[{Chapman-Kolmogorov}]
For all \(t,s \geq 0\),
and \(i, j\)

\begin{equation*}
P_{ij}(t+s) = \sum_k P_{ik}(t) P_{kj}(s)
\end{equation*}

\begin{admonition-note}[{}]
The proof of this is identical the proof for the DTMC version.
\end{admonition-note}
\end{theorem}

\subsection{Kolmogorov's equations}
\label{develop--math6180:ROOT:page--ctmc.adoc---kolmogorovs-equations}

The following pair of results by Kolmogorov can be used to determine explicit formulae for the transition probabilities.
Both ask the question ``By how much does the transition probability from \(i\) to \(j\) change after some specified time?''.
If we imagine a large set of samples, \(P_{ij}'(t)\) represents the following situation:
``If all samples start at state \(i\), what is the rate of change of samples in state \(j\) after some time?''

For the forward equations, consider the distribution at time \(t\).
The rate of change of the number of samples in state \(j\) depends on the incoming and outgoing rate.

\begin{itemize}
\item Samples may be flowing into state \(j\) from some other state \(k\).
    The amount this flow contributes is equal to
    the number of samples at state \(k\) at time \(t\) (\(P_{ik}(t)\))
    times the flow rate from \(k\) to \(j\) (\(q_{kj}\)).
\item Samples may be flowing out of state \(j\).
    The amount this flow subtracts is equal to the number of states at state \(j\)
    at time \(t\) (\(P_{ij}(t)\)) times the flow rate out of \(j\) (\(\nu_j\)).
\end{itemize}

\begin{theorem}[{Kolmogorov's Forward Equations}]
For states \(i\) and \(j\)

\begin{equation*}
P'_{ij}(t) = \sum_{k \neq j} q_{kj}P_{ik}(t) - \nu_jP_{ij}(t)
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
P'_{ij}(t)
&= \lim_{h \to 0} \frac{P_{ij}(t+h) - P_{ij}(t)}{h}
\\&= \lim_{h \to 0} \frac{\sum_{k} P_{ik}(t)P_{kj}(h) - P_{ij}(t)}{h}
\quad\text{by Chapan-Kolmogorov}
\\&= \lim_{h \to 0} \frac{\sum_{k \neq j} P_{ik}(t)P_{kj}(h) + P_{ij}(t)P_{jj}(h)- P_{ij}(t)}{h}
\\&= \sum_{k \neq j} P_{ik}(t)\lim_{h \to 0} \frac{ P_{kj}(h)}{h} + \lim_{h\to 0}P_{ij}(t)\frac{P_{jj}(h)- 1}{h}
\\&= \sum_{k \neq j} q_{kj}P_{ik}(t) - \nu_j P_{ij}(t)
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

For the backward equations, we determine the rate of change of the distribution at time \(t\),
we can look at the rate of change of the distribution at time \(t-s\) and apply the transition probabilities
(which are stationary) to get the desired rates of change.
Of particular note is when \(s=t\).
The rate of change of the number of samples in state \(j\) depends on the incoming and outgoing rate
to each other state \(k\) at the initial time.

\begin{itemize}
\item For \(k \neq i\), each state has zero initial samples,
    so the rate of change of the number of samples at state \(k\) is dominated by the flow rate \(q_{ik}\) for state \(i\).
    Now, of these states, we expect \(P_{kj}(t)\) of them to be at state \(j\) after time \(t\).
    So, the rate of change is equal to the product \(q_{ik}P_{kj}(t)\)
\item For \(k = i\), the state has some initial population
    and the rate of change of the number of samples at state \(i\) is dominated by the flow rate out of \(i\).
    Of the remaining samples, we expect \(P_{ij}(t)\) of them to be at state \(j\) at time \(t\).
    So, the rate of change is equal to \(-\nu_i P_{ij}(t)\).
\end{itemize}

\begin{theorem}[{Kolmogorov's Backward Equations}]
For states \(i\) and \(j\)

\begin{equation*}
P'_{ij}(t) = \sum_{k \neq i} q_{ik}P_{kj}(t) - \nu_iP_{ij}(t)
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
P'_{ij}(t)
&= \lim_{h \to 0} \frac{P_{ij}(t+h) - P_{ij}(t)}{h}
\\&= \lim_{h \to 0} \frac{\sum_{k} P_{ik}(h)P_{kj}(t) - P_{ij}(t)}{h}
\quad\text{by Chapan-Kolmogorov}
\\&= \lim_{h \to 0} \frac{\sum_{k \neq i} P_{ik}(h)P_{kj}(t) + P_{ii}(h)P_{ij}(t) - P_{ij}(t)}{h}
\\&= \sum_{k \neq i} P_{kj}(t)\lim_{h \to 0} \frac{P_{ik}(h)}{h} + \lim_{h \to 0}P_{ij}(t)\frac{P_{ii}(h) -1}{h}
\\&= \sum_{k \neq i} q_{ik}P_{kj}(t) - \nu_i P_{ij}(t)
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

Of particular note is that the \(P_{ij}(t)\) are solutions
to a system of linear differential equations with constant coefficients.
So, these equations are theoretically easy to solve.
Also, in the case where there are only a few states, we can simply find the characteristic
equation of the system and find the solution.

\subsection{Limiting probabilities and steady state}
\label{develop--math6180:ROOT:page--ctmc.adoc---limiting-probabilities-and-steady-state}

The Kolmogorov equations may be used to determine the limiting probabilities.
If we assume that the initial distribution does not matter,
we obtain the following from Kolmogorov's forward equations

\begin{equation*}
\sum_{k\neq j} q_{kj}P_k = \nu_j P_j
\end{equation*}

Where \(P_j\) denotes the limit of \(P_{i,j}(t)\) as \(t\to \infty\).
These \(P_j\) exist and are independent of \(i\)
if

\begin{itemize}
\item All states of the MC communicate. That is
    \(P_{i,j}(t) > 0\) for some \(t\geq 0\) for each pair of \(i\)
    and \(j\).
\item The MC is positive recurrent. That is,
    starting at state \(i\), the mean time of return is finite.
\end{itemize}

\section{Birth and death processes}
\label{develop--math6180:ROOT:page--ctmc.adoc---birth-and-death-processes}

A \textbf{birth and death process} is a special kind of CTMC such that
the states are \(\{0,1,2,3,\ldots\}\)
there are only transitions
between adjacent states.
The system is at state \(n\) when there are \(n\) individual in the system.
We assume that

\begin{itemize}
\item there are new arrivals with exponential rate \(\lambda_n\)
\item people leave the system with rate \(\mu_n\)
\item the time until the next arrival is independent of the time until the next departure.
\end{itemize}

So, \(\{\lambda_n\}_{n=0}^\infty\) are the \textbf{arrival (birth) rates}
and \(\{\mu_n\}_{n=0}^\infty\) are the \textbf{departure (death) rates}.
So, we have the following parameters for the system

\begin{equation*}
\begin{aligned}
\nu_0 &= \lambda_0 \\
\nu_i &= \lambda_i + \mu_i,\ i \geq 1\\
P_{0,1} &= 1\\
P_{i,i+1} &= \frac{\lambda_i}{\lambda_i + \mu_i},\ i\geq 1\\
P_{i,i-1} &= \frac{\mu_i}{\lambda_i + \mu_i},\ i\geq 1\\
\end{aligned}
\end{equation*}

Note \(\nu_i = \lambda_i + \mu_i\) since the time until the next birth or death
is the minimum of an \(Exp(\lambda_i)\) and \(Exp(\mu_i)\) random variables.

There are several popular special cases of birth and death processes

\begin{itemize}
\item The Poisson process is a pure birth process such that
    arrivals occur with constant rate \(\lambda\).
    So, \(\mu_n=0\) and \(\lambda_n = \lambda\)
    for all \(n \geq 0\).
\item The \textbf{Yule process} is another pure birth process
    where each member can give birth with rate \(\lambda\).
    So, \(\mu_n=0\) and \(\lambda_n = n\lambda\)
    for all \(n \geq 0\).
    Note that this is like a continuous time version of a branching process.
\item Linear growth models with immigration is such
    that
    
    \begin{itemize}
    \item each individual can give birth to another with rate \(\lambda\)
    \item immigrants arrive with rate \(\theta\)
    \item Persons die/emigrate with rate \(\mu\).
        So, \(\mu_n = n \mu\) and \(\lambda_n = n\lambda + \theta\)
        for all \(n \geq 0\).
    \end{itemize}
\item Queueing systems (discussed later).
\end{itemize}

\subsection{Population growth time}
\label{develop--math6180:ROOT:page--ctmc.adoc---population-growth-time}

Let \(T_i\) be the time taken to reach state \(i+1\) starting from state \(i\).
Then, we may be interested in the expected and variation of time to grow from \(i\) to \(j\).
That is,

\begin{equation*}
E[T_i] + E[T_{i+1}] + \cdots + E[T_{j-1}]
\end{equation*}

and

\begin{equation*}
Var[T_i] + Var[T_{i+1}] + \cdots + Var[T_{j-1}]
\end{equation*}

Note that the \(T_i\) must be independent due to the markovian property of the chain.

\begin{proposition}[{}]
\begin{equation*}
\begin{aligned}
E[T_0] &= \frac{1}{\lambda_0}\\
E[T_i] &= \frac{1}{\lambda_i} + \frac{\mu_i}{\lambda_i}E(T_{i-1}),\ i \geq 1\\
\end{aligned}
\end{equation*}

\begin{proof}[{}]
Working out \(E[T_0]\) is trivial since \(P_{0,1} = 1\) and the birth rate
is \(\lambda_0\).
For the second one, we condition on \(I_i\)
which is \(1\) if the first transition if from \(i\) to \(i+1\)
and \(0\) if the first transition is from \(i\) to \(i-1\).
So,

\begin{equation*}
\begin{aligned}
E[T_i]
&= E[T_i | I=0]P(I=0) + E[T_i | I=1]P(I=1)
\\&= \left(\frac{1}{\lambda_i + \mu_i} + E[T_{i-1} + T_i]\right)\frac{\mu_i}{\lambda_i + \mu_i} + \frac{1}{\lambda_i + \mu_i}\frac{\lambda}{\lambda_i + \mu_i}
\end{aligned}
\end{equation*}

Note that \(E[T_i | I=0]\) can be split up into the time of the first death (minimum of two exponentials)
plus the time of the population growth from \(i-1\) to \(i\) and then from \(i\) to \(i+1\).
So,

\begin{equation*}
\begin{aligned}
&E[T_i] = \frac{1}{\lambda_i + \mu_i} + \frac{\mu_i}{\lambda_i + \mu_i}E[T_{i-1}] + \frac{\mu_i}{\lambda_i + \mu_i}E[T_i]
\\&\implies
\frac{\lambda_i}{\lambda_i + \mu_i} E[T_i]
= \frac{1}{\lambda_i + \mu_i} + \frac{\mu_i}{\lambda_i + \mu_i}E[T_{i-1}]
\\&\implies
E[T_i]
= \frac{1}{\lambda_i} + \frac{\mu_i}{\lambda_i}E[T_{i-1}]
\end{aligned}
\end{equation*}
\end{proof}
\end{proposition}

\begin{proposition}[{}]
\begin{equation*}
\begin{aligned}
Var[T_0] &= \frac{1}{\lambda_0^2}\\
Var[T_i]
&= \frac{1}{\lambda_i(\lambda_i + \mu_i)} + \frac{\mu_i}{\lambda_i} Var[T_{i-1}]
+ (E[T_i] + E[T_{i-1}])^2  \frac{\mu_i}{\lambda_i + \mu_i}
\end{aligned}
\end{equation*}

\begin{proof}[{}]
Again working out \(Var[T_0]\) is trivial.

For \(i\geq 1\), we can split \(T_i\)
into \(T_i=X+Y\)
where \(X \sim Exp(\lambda_i + \mu_i)\) is the time until leaving state \(i\).
\(Y\) is the remainder which may be \(Y=T_{i-1} + T_i\) if \(I=0\)
and \(Y=0\) if \(I=1\).
Due to the markovian property, we know that \(X\perp Y\)
and hence we can focus on \(Var[Y]\).
So

\begin{equation*}
\begin{aligned}
Var[T_i]
&= Var[X] + Var[Y]
\\&= \frac{1}{(\lambda_i + \mu_i)^2} + E_I[Var_T[Y|I]] + Var_I[E_Y[Y|I]]
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{array}{|c|c|c|}\hline
 & I=0 & I=1\\\hline
E_T[Y|I] & E[T_i] + E[T_{i-1}] & 0\\\hline
Var_T[Y|I] & Var[T_i] + Var[T_{i-1}] & 0\\\hline
\end{array}
\end{equation*}

So

\begin{equation*}
\begin{aligned}
E_I[Var_T[Y|I]]
&= Var_T[Y|I=0]P(I=0) + Var_T[Y|I=1]P(I=1)
\\&= \frac{\mu_i}{\lambda_i + \mu_i}Var[T_i] + \frac{\mu_i}{\lambda_i + \mu_i} Var[T_{i-1}]
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
Var_I[E_T[Y|I]]
&= E_I[E_T[Y|I]^2] - E_I[E_T[Y|I]]^2
\\&= \left(E_T[Y|I=0]^2P(Y=0) - E_T[Y|I=1]^2P(Y=1)\right)
\\&\quad\quad+ \left(E_T[Y|I=0]P(Y=0) - E_T[Y|I=1]P(Y=1)\right)^2
\\&= \left((E[T_i] + E[T_{i-1}])^2\frac{\mu_i}{\lambda_i + \mu_i}\right) - \left((E[T_i] + E[T_{i-1}])\frac{\mu_i}{\lambda_i + \mu_i}\right)^2
\\&= (E[T_i] + E[T_{i-1}])^2 \left(\frac{\mu_i}{\lambda_i + \mu_i} - \left(\frac{\mu_i}{\lambda_i + \mu_i}\right)^2 \right)
\\&= (E[T_i] + E[T_{i-1}])^2 \left(\frac{\mu_i(\lambda_i + \mu_i) - \mu_i^2}{(\lambda_i + \mu_i)^2} \right)
\\&= (E[T_i] + E[T_{i-1}])^2 \frac{\lambda_i\mu_i}{(\lambda_i + \mu_i)^2}
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
Var[T_i]
&= \frac{1}{(\lambda_i + \mu_i)^2} + \frac{\mu_i}{\lambda_i + \mu_i}Var[T_i] + \frac{\mu_i}{\lambda_i + \mu_i} Var[T_{i-1}]
+ (E[T_i] + E[T_{i-1}])^2  \frac{\lambda_i\mu_i}{(\lambda_i + \mu_i)^2}
\end{aligned}
\end{equation*}

By rearranging, we get

\begin{equation*}
Var[T_i]
= \frac{1}{\lambda_i(\lambda_i + \mu_i)} + \frac{\mu_i}{\lambda_i} Var[T_{i-1}]
+ (E[T_i] + E[T_{i-1}])^2  \frac{\mu_i}{\lambda_i + \mu_i}
\end{equation*}
\end{proof}
\end{proposition}

\subsection{Transition probabilities for pure birth processes}
\label{develop--math6180:ROOT:page--ctmc.adoc---transition-probabilities-for-pure-birth-processes}

Recall that

\begin{equation*}
P_{i,j}(t) = P(X(t) = j \ | \ X(0) = i)
\end{equation*}

Under certain special conditions, it is possible to work out an explicit
form for this probability.
Consider a pure birth process.
Then, kolmogorov's forward equation reduces to

\begin{equation*}
\begin{aligned}
P'_{i,i}(t) &= -\nu_i P_{i,i}(t)
\\
P'_{i,j}(t) &= q_{j-1,j}P_{i,j-1}(t) - \nu_{j}P_{i,j}(t)\quad\text{for } i < j
\end{aligned}
\end{equation*}

since \(q_{j-1, j} = \lambda_{j-1}\) and \(\nu_j = \lambda_j\),
we have

\begin{equation*}
\begin{aligned}
P'_{i,i}(t) &= -\lambda_i P_{i,i}(t)
\\
P'_{i,j}(t) &= \lambda_{j-1}P_{i,j-1}(t) - \lambda_{j}P_{i,j}(t)\quad\text{for } i < j
\end{aligned}
\end{equation*}

So,

\begin{equation*}
\begin{aligned}
P_{i,i}(t) &= e^{-\lambda_i t}\\
\\
P_{i,j}(t) &= \lambda_{j-1} e^{-\lambda_jt}\int_0^t e^{\lambda_j s} P_{i,j-1}(s) \ ds\quad\text{for } i < j
\end{aligned}
\end{equation*}

If the \(\lambda_i\) are distinct, we can (not so easily) solve the above recursively to find that

\begin{equation*}
\begin{aligned}
P_{i,i}(t) &= e^{-\lambda_i t}\\
P_{i,j}(t) &=
\sum_{k=i}^j e^{-\lambda_k t}\prod_{\begin{array}{c}r=i\\\end{array}}^j \frac{\lambda_r}{\lambda_r-\lambda_k}
- \sum_{k=i}^{j-1} e^{-\lambda_k t}\prod_{\begin{array}{c}r=i\\\end{array}}^{j-1} \frac{\lambda_r}{\lambda_r-\lambda_k}
\end{aligned}
\end{equation*}

\begin{admonition-note}[{}]
I have not worked out this result on my own. An explanation can be found in proposition 6.1 of the Ross text.
\end{admonition-note}

\begin{proposition}[{Transition probabilities for Yule process}]
Consider a Yule process with \(\lambda_n = n\lambda\)
and suppose that \(X(0) = i\).
Then, the population size at time \(t\) has a negative binomial distribution
with parameters \(i\) and \(e^{-\lambda t}\).
That is, \(X(t)\) is the sum of \(i\) iid geometric
random variables with parameter \(e^{-\lambda t}\)
and

\begin{equation*}
P_{i,j}(t) = P(X(t) = j \ | \ X(0) = i) = \binom{j-1}{i-1}e^{-i\lambda t}(1-e^{-\lambda t})^{j-i}
\end{equation*}

for \(1 \leq i \leq j\).

\begin{proof}[{}]
We prove this inductively.
Note when \(j=i\),
the above reduces to

\begin{equation*}
P_{i,i}(t) = e^{-i\lambda t}
\end{equation*}

which we already established.
Now suppose the formula is suitable for some \(j\geq i\).
Then,

\begin{equation*}
\begin{aligned}
P_{i,j+1}(t)
&= \lambda_{j}e^{-\lambda_{j+1} t}\int_0^t e^{\lambda_{j+1} s} P_{i,j}(s) \ ds
\\&= j \lambda e^{-(j+1)\lambda t}\int_0^t e^{\lambda s}e^{j\lambda s} P_{i,j}(s) \ ds
\\&= j \lambda e^{-(j+1)\lambda t}\int_0^t e^{\lambda s} e^{j\lambda s} \binom{j-1}{i-1}e^{-i\lambda s}(1-e^{-\lambda s})^{j-i} \ ds
\\&= j \binom{j-1}{i-1} \lambda e^{-(j+1)\lambda t}\int_0^t e^{\lambda s} (e^{\lambda s} - 1)^{j-i} \ ds
\\&= j \frac{(j-1)!}{(i-1)!(j-i)!} \lambda e^{-(j+1)\lambda t} \left.\frac{1}{\lambda (j-i+1)} e^{\lambda s} (e^{\lambda s} - 1)^{j-i+1} \right|_{s=0}^t
\\&= \frac{j!}{(i-1)!(j-i+1)!} e^{-(j+1)\lambda t} (e^{\lambda t} - 1)^{j-i+1}
\\&= \binom{j}{i-1} e^{-(j+1)\lambda t}e^{(j-i+1)\lambda t}e^{-(j-i+1)\lambda t} (e^{\lambda t} - 1)^{j-i+1}
\\&= \binom{j}{i-1} e^{-i\lambda t} (1 - e^{-\lambda t})^{j+1-i}
\end{aligned}
\end{equation*}

So, by induction we have the desired result.
\end{proof}
\end{proposition}

\begin{proposition}[{Linear Growth model with Immigration}]
Let \(\lambda_n = n\lambda + \theta\) and \(\mu_n = n\mu\).
Let \(M(t) = E[X(t)]\) where \(M(0) = i\).
Then

\begin{equation*}
\begin{aligned}
M'(t) &= (\lambda - \mu)M(t) + \theta\\
M(t) &= \begin{cases}
\frac{\theta}{\lambda - \mu}\left[e^{(\lambda-\mu)t} - 1\right] + ie^{(\lambda -\mu)t},&\quad\lambda\neq \mu
\\
\theta t + i ,&\quad \lambda = \mu
\end{cases}
\end{aligned}
\end{equation*}

\begin{proof}[{}]
First note that

\begin{equation*}
\begin{aligned}
M(t)
&= E[X(t)]
\\&= \sum_{n=0}^\infty E[X(t) \ | \ X(0) = n] P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=0}^\infty mP(X(t) = m \ | \ X(0) = n) P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=0}^\infty mP_{n,m}(t) P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=1}^\infty mP_{n,m}(t) P(X(0) = n)
\end{aligned}
\end{equation*}

So

\begin{equation*}
\begin{aligned}
M'(t)
&= \sum_{n=0}^\infty \sum_{m=1}^\infty mP_{n,m}'(t) P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=1}^\infty m\left[\sum_{k\neq m}q_{k,m}P_{n,k}(t) - \nu_mP_{n,m}(t)\right] P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=1}^\infty m\left[q_{m-1,m}P_{n,m-1}(t) + q_{m+1,m}P_{n,m+1}(t) - \nu_mP_{n,m}(t)\right] P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=1}^\infty m\left[\lambda_{m-1}P_{n,m-1}(t) + \mu_{m+1}P_{n,m+1}(t) - (\mu_m + \lambda_m)P_{n,m}(t)\right] P(X(0) = n)
\\&= \sum_{n=0}^\infty \sum_{m=1}^\infty m\left[((m-1)\lambda + \theta)P_{n,m-1}(t) + (m+1)\mu P_{n,m+1}(t) - (m\mu + m\lambda + \theta)P_{n,m}(t)\right] P(X(0) = n)
\\&= \sum_{n=0}^\infty \left[
    \begin{aligned}
        &\sum_{m=1}^\infty m((m-1)\lambda + \theta)P_{n,m-1}(t)
        \\&+ \sum_{m=1}^\infty m(m+1)\mu P_{n,m+1}(t)
        \\&- \sum_{m=1}^\infty m(m\mu + m\lambda + \theta)P_{n,m}(t)
    \end{aligned}
    \right]P(X(0)=n)
\\&= \sum_{n=0}^\infty \left[
    \begin{aligned}
        &\sum_{m=0}^\infty (m+1)(m\lambda + \theta)P_{n,m}(t)
        \\&+ \sum_{m=2}^\infty (m-1)m\mu P_{n,m}(t)
        \\&- \sum_{m=1}^\infty m(m\mu + m\lambda + \theta)P_{n,m}(t)
    \end{aligned}
    \right]P(X(0)=n)
\\&= \sum_{n=0}^\infty \left[
    \begin{aligned}
        & \theta P_{n,0}(t) + 2(\lambda+\theta)P_{n,1}(t)
            - (\mu + \lambda + \theta)P_{n,1}(t)
        \\
        &+\sum_{m=2}^\infty \left[(m+1)(m\lambda + \theta) + (m-1)m\mu - m(m\mu+m\lambda+\theta) \right]P_{n,m}(t)
    \end{aligned}
    \right]P(X(0)=n)
\\&= \sum_{n=0}^\infty \left[
    \begin{aligned}
        & \theta P_{n,0}(t) + (\lambda - \mu) P_{n,1}(t)
            + \theta P_{n,1}(t)
        \\
        &+\sum_{m=2}^\infty \left[
            (m^2\lambda + m\theta + m\lambda + \theta) + (m^2\mu -m\mu)
            - (m^2\mu + m^2\lambda + m\theta)
        \right]P_{n,m}(t)
    \end{aligned}
    \right]P(X(0)=n)
\\&= \sum_{n=0}^\infty \left[
    \begin{aligned}
        & \theta P_{n,0}(t) + (\lambda - \mu) P_{n,1}(t) + \theta P_{n,1}(t)
        \\
        &+\sum_{m=2}^\infty \left[m(\lambda - \mu) + \theta
        \right]P_{n,m}(t)
    \end{aligned}
    \right]P(X(0)=n)
\\&= \sum_{n=0}^\infty \left[
    \sum_{m=0}^\infty \theta P_{n,m}(t)
    + \sum_{m=1}^\infty m(\lambda - \mu) P_{n,m}(t)
    \right]P(X(0)=n)
\\&= \sum_{n=0}^\infty \left[
    E[\theta \ | \ X(0)=n]
    + (\lambda - \mu)E[X(t) \ | X(0)=n]
    \right]P(X(0)=n)
\\&=
    E[\theta]
    + (\lambda - \mu)E[X(t)]
\\&= (\lambda - \mu)M(t) + \theta
\end{aligned}
\end{equation*}

The desired result then follows by solving the differential equation.
\end{proof}
\end{proposition}

\subsection{Limiting probabilities}
\label{develop--math6180:ROOT:page--ctmc.adoc---limiting-probabilities}

The equations of the limiting probabilities for a birth and death process reduces to

\begin{equation*}
\begin{aligned}
\lambda_0 P_0 &= \mu_1 P_1\\
(\lambda_i + \mu_i)P_i &= \mu_{i+1}P_{i+1} + \lambda_{n-1} P_{n-1}
\quad\text{for } i \geq 1
\end{aligned}
\end{equation*}

Upon solving these equations, we obtain that

\begin{equation*}
\begin{aligned}
P_1 &= \frac{\lambda_0}{\mu_1}P_0\\
P_2 &= \frac{\lambda_0\lambda_1}{\mu_1\mu_2}P_0\\
&\vdots\\
P_n &= \frac{\lambda_0\lambda_1\cdots \lambda_{n-1}}{\mu_1\mu_2\cdots\mu_n}P_0\\
&\cdots
\end{aligned}
\end{equation*}

Since the total probability must be one,

\begin{equation*}
\begin{aligned}
&1
= \sum_{n=0}^\infty P_n
= P_0 \left(1 + \sum_{n=1}^\infty \frac{\lambda_0\lambda_1\cdots \lambda_{n-1}}{\mu_1\mu_2\cdots\mu_n} \right)
\\
&\quad\implies
P_0 = \frac{1}{1 + \sum_{n=1}^\infty \frac{\lambda_0\lambda_1\cdots \lambda_{n-1}}{\mu_1\mu_2\cdots\mu_n}}
\end{aligned}
\end{equation*}

From this, we can work out any \(P_n\).
Also note, a necessary condition for the limiting probabilities to exist is that the above summation must converge.

\section{Queues}
\label{develop--math6180:ROOT:page--ctmc.adoc---queues}

In this section, we assume that there is a single queue (of either finite or infinite length)
of individuals from a homogeneous (and possibly infinite) population
being served by one or more (indistinguishable) servers.
An individual waits in the queue until a server is available
and leaves once they have been served.
Note that terminology regarding possible extensions is discussed in a subsection.

We denote this queue using the Kendall's notation

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6180/ROOT/mm1}
\end{figure}

Note that the ``M'' stands for \emph{markovian} or \emph{memoryless}
\href{https://en.wikipedia.org/wiki/Kendall\%27s\_notation}{[wikipedia]}.
We will typically let \(c\) denote the number of server and \(N\)
denote the maximum length (or capacity) of the queue.
We would also omit the trailing \(\infty/\infty\) if it is understood.

We can represent the state of a queuing system by the number of individuals in the system,
length of queue plus number of persons being served.
Since we assume exponential arrival \(\lambda\) and service time \(\mu\),
we can represent the queueing system using a CTMC, in particular, a birth and death process.

We define the following random variables

\begin{itemize}
\item the time spent in the system, \(R\)
\item the number of persons in the system, \(N\)
\item the waiting time in the queue, \(W\)
\item the length of the queue, \(L\)
\item the service time, \(S\). Note that we already know that \(S\sim Exp(\mu)\).
\end{itemize}

There are a few expectations which are interested in

\begin{itemize}
\item The number of persons in the system, \(L_s = E[N]\)
\item The number of persons in the queue, \(L_q = E[L]\)
\item The waiting time in the system, \(W_s = E[R]\)
\item The waiting time in the queue, \(W_q = E[W]\)
\end{itemize}

In order to aide finding these expectations, we have the following proposition.

\begin{proposition}[{Little's law}]
Let \(\lambda_e\) be the effective arrival rate in the system.

Little's law states that

\begin{itemize}
\item \(\lambda_e E[R] = E[N]\) or \(\lambda_e W_s = L_s\)
\item \(\lambda_e E[W] = E[L]\) or \(\lambda_e W_q = L_q\)
\end{itemize}
\end{proposition}

\begin{proposition}[{}]
\begin{itemize}
\item \(E[R] = E[W] + E[S]\) or \(W_s = W_q + \frac{1}{\mu}\)
\item \(E[N] = E[L] + P(N \geq 1)\)
    or \(L_s = L_q + \text{expected number of people being served}\)
\end{itemize}
\end{proposition}

\subsection{Single server with infinite capacity (M/M/1)}
\label{develop--math6180:ROOT:page--ctmc.adoc---single-server-with-infinite-capacity-mm1}

From the discussion of birth and death processes, the limiting probabilities are given by

\begin{equation*}
\pi_n = \left(\frac{\lambda}{\mu}\right)^n\pi_0
\quad\text{where } \pi_0 = 1-\frac{\lambda}{\mu} = 1-\rho
\end{equation*}

where \(\rho = \frac{\lambda}{\mu}\)
and exists if \(\rho < 1\).
That is, the limiting probabilities exist if and only if the rate of arrival is less than the rate of service,

\subsubsection{Important quantities}
\label{develop--math6180:ROOT:page--ctmc.adoc---important-quantities}

Note that

\begin{equation*}
L_s=E[N] = \sum_{n=0}^\infty n \pi_n = \sum_{n=0}^\infty n(1-\rho)\rho^n = \frac{\rho}{1-\rho}
\end{equation*}

and hence by Little's law
\(W_s = \frac{\rho}{\lambda(1-\rho)}\).
Also, the expected waiting time in the system is

\begin{equation*}
W_q = W_s - \frac{1}{\mu}
= \frac{\rho}{\lambda(1-\rho)} - \frac{1}{\mu}
= \frac{1}{\mu - \lambda} - \frac{1}{\mu} = \frac{\lambda}{\mu(\mu-\lambda)}
\end{equation*}

So, the expected length of the queue is

\begin{equation*}
L_q = \frac{\lambda^2}{\mu(\mu-\lambda)}
\end{equation*}

\subsubsection{Distribution of the waiting time}
\label{develop--math6180:ROOT:page--ctmc.adoc---distribution-of-the-waiting-time}

First note that if there are \(N=n\) persons in the system,
the waiting time of the \(N\)'th person, \(W|N=n\) is \(Gamma(n,\mu)\) distributed since
\(N\)'th person must wait through \(N\) iid \(Exp(\mu)\) service times (including their own).
Note that in the special case when \(N=0\), the waiting time is \(W=0\).

So

\begin{equation*}
\begin{aligned}
P(W\leq t)
&= \sum_{n=0}^\infty P(W\leq t \ | \ N = n)P(N=n)
\\&= P(N=0) + \sum_{n=1}^\infty P(W\leq t \ | \ N = n)P(N=n)
\\&= (1-\rho) + \sum_{n=1}^\infty \int_0^t \frac{\mu^n}{\Gamma(n)}x^{n-1}e^{-\mu x} \ dx (1-\rho)\rho^n
\\&= (1-\rho) + \int_0^t (1-\rho)\rho  \mu e^{-\mu x}\sum_{n=1}^\infty \frac{\mu^{n-1}}{(n-1)!}x^{n-1} \rho^{n-1} \ dx
\\&= (1-\rho) + \int_0^t (1-\rho)\rho  \mu e^{-\mu x} e^{\mu \rho x} \ dx
\\&= (1-\rho) + (1-\rho)\rho \mu\int_0^t e^{\mu (\rho-1) x} \ dx
\\&= (1-\rho) -\rho \left. e^{\mu (\rho-1) x} \right|_0^t
\\&= (1-\rho) + \rho (1-e^{\mu (\rho-1) t})
\\&= 1-\rho e^{-\mu (1-\rho) t}
\\&= 1-\rho e^{-(\mu-\lambda) t}
\end{aligned}
\end{equation*}

So, \(W\) is a mixed random variable with an atom of probability at \(W=0\).

\subsection{Single server with finite capacity (M/M/1/N/∞)}
\label{develop--math6180:ROOT:page--ctmc.adoc---single-server-with-finite-capacity-mm1n}

If the system has a fixed capacity of \(N\),
we can determine that

\begin{equation*}
\pi_n = \frac{1-\rho}{1-\rho^{N+1}}\rho^n
\quad\text{for } n=0,1,\ldots,N
\end{equation*}

where \(\rho = \frac{\lambda}{\mu}\)
Note that there is no reason to restrict \(\rho\)
since the summation of probabilities is finite.

\subsubsection{Important quantities}
\label{develop--math6180:ROOT:page--ctmc.adoc---important-quantities-2}

Note that

\begin{equation*}
L_s = \frac{\rho\left[1 + N\rho^{N+1} - (N+1)\rho^N\right]}{(1-\rho)(1-\rho^{N+1})}
\end{equation*}

To work out \(W_s\), we need the effective arrival rate \(\lambda_e\).
We can work this out to be

\begin{equation*}
\lambda_e = E[\text{arrival rate}] = \sum_{n=0}^{N-1}\lambda P(\text{there are n persons in the system})
= \lambda (1-\pi_N)
\end{equation*}

since when there are \(N\) persons in the system, no customers can arrive (ie arrival rate is zero).
From here, we can work everything out as usual.

\subsection{Multi-server with infinite capacity (M/M/c)}
\label{develop--math6180:ROOT:page--ctmc.adoc---multi-server-with-infinite-capacity-mmc}

Consider a multi-server queue with \(c\) servers and infinite capacity.
Let the rate of service be \(Exp(\mu)\) and the rate of arrival be \(Exp(\lambda)\).
So, the transition rates are

\begin{equation*}
\begin{aligned}
    \lambda_n &= \lambda \quad\text{for }n\geq 0\\
    \mu_n &= \begin{cases}
    n\mu &\quad\text{if } n < c\\
    c\mu &\quad\text{if } n \geq c
    \end{cases}
\end{aligned}
\end{equation*}

By solving these, we get

\begin{equation*}
\pi_n = \begin{cases}
\left(\frac{\lambda}{\mu}\right)^n \frac{1}{n!}\pi_0
&\quad\text{for }n < c
\\
\left(\frac{\lambda}{\mu}\right)^n \frac{1}{c!c^{n-c}}\pi_0
&\quad\text{for }n \geq c
\end{cases}
\end{equation*}

where

\begin{equation*}
\pi_0 = \frac{1}{
\sum_{n=0}^{c-1} \frac{\left(\lambda/\mu\right)^n}{n!}
+ \frac{\left(\lambda/\mu\right)^c}{c!}\frac{1}{1-\frac{\lambda}{c\mu}}}
\end{equation*}

Note that these limiting probabilities only exist if \(\lambda < c\mu\)

\subsection{Multi-server with finite capacity (M/M/c/N/∞)}
\label{develop--math6180:ROOT:page--ctmc.adoc---multi-server-with-finite-capacity-mmcn}

We can repeat similar analysis for multi-server queues with finite capacity.
\end{document}
