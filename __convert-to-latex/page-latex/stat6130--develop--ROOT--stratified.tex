\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Stratified Samples}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Suppose we have a heterogeneous population.
Then, to properly analyze this population, it may be more appropriate
to first split the whole population into
smaller groups (strata) such that the sampling units
within each strata are homogeneous with respect
to the \textbf{characteristic} under study.

\begin{admonition-note}[{}]
We only stratify based on the characteristic under study and possibly cofounders.
There is no reason to stratify based on unrelated characteristics.
\end{admonition-note}

When sampling from the population,
we treat each strata as a separate population and sample each strata
using SRS.
Note that drawn sampling units between strata are therefore independent.

We use the following notation

\begin{itemize}
\item \(K\) is the number of strata
\item \(N_i\) is the number of sampling units in strata \(i\).
    
    \begin{itemize}
    \item \(N =\sum_{i=1}^K N_i\)
    \item \(W_i = \frac{N_i}{N}\) is the weight of strata \(i\)
    \end{itemize}
\item \(Y_{ij}\) is the \(j\)'th sampling unit from strata \(i\).
    
    \begin{itemize}
    \item \(\overbar{Y}_i = \frac{1}{N_i}\sum_{j=1}^N Y_{ij}\)
    \item \(\overbar{Y} = \frac{1}{N}\sum_{i=1}^K\sum_{j=1}^{N_i}Y_{ij} = \sum_{i=1}^K W_i\overbar{Y}_i\)
    \end{itemize}
\item \(S_i\) is the sample proportion in strata \(i\) and \(S\)
    is the overall sample proportion
\end{itemize}

We also define

\begin{itemize}
\item \(n_i\) is the number of sampling units drawn from strata \(i\)
\item \(y_{ij}\) is the \(j\)'th sampling unit drawn from strata \(i\)
    
    \begin{equation*}
    \overbar{y}_i = \frac{1}{n_i}\sum_{j=1}^{n_i} y_{ij}
    \quad\text{and}\quad
    \overbar{y}_{st} = \sum_{i=1}^K W_i\overbar{y}_i
    \end{equation*}
\end{itemize}

\begin{proposition}[{Relationship between population and strata sample variance}]
\begin{equation*}
\frac{N-1}{N}S^2 = \sum_{i=1}^K \left(\frac{N_i-1}{N}\right)S_i^2 + \sum_{i=1}^K \frac{N_i}{N} (\overbar{Y}_i-\overbar{Y})^2
\end{equation*}

If \(\frac{N_i-1}{N_i}\approx 1\) and \(\frac{N-1}{N}\approx 1\) then

\begin{equation*}
S^2 = \sum_{i=1}^K W_iS_i^2 + \sum_{i=1}^K W_i(\overbar{Y}_i-\overbar{Y})^2
\end{equation*}

\begin{admonition-remark}[{}]
Something about this reminds me of ANOVA. I am not sure what.
\end{admonition-remark}
\end{proposition}

\section{Properties of sample mean}
\label{develop--stat6130:ROOT:page--stratified.adoc---properties-of-sample-mean}

Note that

\begin{equation*}
E[\overbar{y}_{st}]
= \sum_{i=1}^KW_i E[\overbar{y}_i]
= \sum_{i=1}^KW_i \overbar{Y}_i
= \overbar{Y}
\end{equation*}

Also, since sampling from the different strata are independent,

\begin{equation*}
Var(\overbar{y}_{st}) = \sum_{i=1}^K W_i^2Var(\overbar{y}_i)
\end{equation*}

SRSWOR::

+

\begin{equation*}
Var(\overbar{y}_{st}) = \sum_{i=1}^K W_i^2 \frac{N_i-n_i}{N_in_i} S_i^2
\end{equation*}

which has unbiased estimator

\begin{equation*}
\hat{Var}(\overbar{y}_{st}) = \sum_{i=1}^K W_i^2 \frac{N_i-n_i}{N_in_i} s_i^2
\end{equation*}

SRSWR::

+

\begin{equation*}
Var(\overbar{y}_{st})
= \sum_{i=1}^K W_i^2 \frac{N_i-1}{N_in_i} S_i^2
= \sum_{i=1}^K W_i^2 \frac{\sigma_i^2}{n_i}
\end{equation*}

which has unbiased estimator

\begin{equation*}
\hat{Var}(\overbar{y}_{st}) = \sum_{i=1}^K W_i^2 \frac{s_i^2}{n_i}
\end{equation*}

\section{Allocation problem and choice of sample size}
\label{develop--stat6130:ROOT:page--stratified.adoc---allocation-problem-and-choice-of-sample-size}

In stratified random sampling, we must be careful in choosing the strata sample sizes
\(n_1, \ldots n_k\) so that available resources are used effectively.
There are two main aspects in choosing the sample size

\begin{enumerate}[label=\arabic*)]
\item Minimize the cost given a specified precision
\item Maximize the precision given some cost
\end{enumerate}

\subsection{Equal Allocation}
\label{develop--stat6130:ROOT:page--stratified.adoc---equal-allocation}

The most simple allocation method allots the same sample size for all strata.
That is

\begin{equation*}
n_i = \frac{1}{K} n
\end{equation*}

\subsection{Proportional Allocation}
\label{develop--stat6130:ROOT:page--stratified.adoc---proportional-allocation}

In this allocation method, we choose the \(n_i\) so that they are proportional to
\(N_i\).
That is

\begin{equation*}
n_i\propto N_i \implies n_i = cN_i \implies n = \sum_{i=1}^K n_i = c\sum_{i=1}^K N_i = cN
\end{equation*}

where \(c = \frac{n}{N}\) is the \textbf{constant of proportionality}.
So

\begin{equation*}
n_i = \frac{n}{N}N_i = W_i n
\end{equation*}

Note that this also implies that \(w_i = W_i\)

\subsubsection{SRSWOR}
\label{develop--stat6130:ROOT:page--stratified.adoc---srswor}

Under this scheme, the variance under SRSWOR is

\begin{equation*}
\begin{aligned}
Var(\overbar{y}_{st})
&= \sum_{i=1}^K W_i^2\left(\frac{1}{n_i}-\frac{1}{N_i}\right)S_i^2
\\&= \sum_{i=1}^K W_i^2\left(\frac{1}{W_in}-\frac{1}{N_i}\right)S_i^2
\\&= \sum_{i=1}^K W_i^2\left(\frac{1}{W_in}-\frac{1}{NW_i}\right)S_i^2
\\&= \sum_{i=1}^K W_i\left(\frac{1}{n}-\frac{1}{N}\right)S_i^2
\\&= \frac{N-n}{Nn}\sum_{i=1}^K W_iS_i^2
\\&= \frac{1}{n}\sum_{i=1}^K W_iS_i^2 - \frac{1}{N}\sum_{i=1}^K W_iS_i^2
\end{aligned}
\end{equation*}

Note that this is less than the population variance under SRSWOR.
Therefore stratifying the data reduces the variance of the sample mean.

\subsection{Neyman/Optimal Allocation}
\label{develop--stat6130:ROOT:page--stratified.adoc---neymanoptimal-allocation}

\begin{admonition-caution}[{}]
Note that we use the \textbf{standard deviation} and not the \textbf{variance}.
\end{admonition-caution}

This method considers both the size of the stratum \(N_i\) as well as its variability \(S_i\)

\begin{equation*}
\begin{aligned}
&n_i \propto N_iS_i
\\&\implies n_i = C^*N_iS_i
\\&\implies C^* = \frac{n_i}{\sum_{i=1}^K N_iS_i}
\\&\implies n_i = \left(\frac{N_iS_i}{\sum_{k=1}^K N_kS_k}\right)n
\end{aligned}
\end{equation*}

\subsubsection{SRSWOR}
\label{develop--stat6130:ROOT:page--stratified.adoc---srswor-2}

Under this scheme, the variance under SRSWOR is

\begin{equation*}
\begin{aligned}
Var(\overbar{y}_{st})
&= \sum_{i=1}^K W_i^2\left(\frac{1}{n_i}-\frac{1}{N_i}\right)S_i^2
\\&= \sum_{i=1}^K W_i^2\frac{1}{n_i}S_i^2
     - \sum_{i=1}^K W_i^2\frac{1}{N_i}S_i^2
\\&= \sum_{i=1}^K W_i^2\frac{\sum_{k=1}^K N_kS_k}{nN_iS_i}S_i^2
     - \sum_{i=1}^K W_i^2\frac{1}{N_i}S_i^2
\\&= \sum_{i=1}^K W_i^2\frac{\sum_{k=1}^K W_kS_k}{nW_i}S_i
     - \frac{1}{N}\sum_{i=1}^K W_iS_i^2
\\&= \frac{1}{n}\left(\sum_{k=1}^K W_kS_k\right)\sum_{i=1}^K W_iS_i
     - \frac{1}{N}\sum_{i=1}^K W_iS_i^2
\\&= \frac{1}{n}\left(\sum_{i=1}^K W_iS_i\right)^2
     - \frac{1}{N}\sum_{i=1}^K W_iS_i^2
\end{aligned}
\end{equation*}

Note that by the Cauchy-Schwartz inequality
with \(x_i = \sqrt{W_i}\) and \(y_i = \sqrt{W_i}S_i\),
the variance under optimal allocation is less than
the variance under proportional allocation.

\subsection{Allocation based on cost}
\label{develop--stat6130:ROOT:page--stratified.adoc---allocation-based-on-cost}

This method considers the cost of the survey as well as the variability.
Let

\begin{itemize}
\item \(c_i\) be the cost per unit in strata \(i\)
\item \(C_0\) be the overhead costs (fixed cost)
\item \(C\) be the total budget
\end{itemize}

Then

\begin{equation*}
C = C_0 + \sum_{i=1}^K c_in_i
\end{equation*}

Then, we choose stata sample sizes \(n_i\) such that the variance of the sample
mean is minimized while the budget is kept.

\subsubsection{SRSWR}
\label{develop--stat6130:ROOT:page--stratified.adoc---srswr}

Recall that

\begin{equation*}
Var(\overbar{y}_{st}) = \sum_{i=1}^K \frac{W_i^2\sigma_i^2}{n_i}
\end{equation*}

So, we have Lagrangian function

\begin{equation*}
\mathcal{L} = \sum_{i=1}^K \frac{W_i^2\sigma_i^2}{n_i} + \lambda\left(\sum_{i=1}^K c_in_i - (C-C_0)\right)
\end{equation*}

Differentiating with respect to \(n_i\)

\begin{equation*}
\begin{aligned}
\frac{\partial \mathcal{L}}{\partial n_i} = - \frac{W_i^2\sigma_i^2}{n_i^2} + \lambda c_i = 0
\implies n_i = \frac{W_i\sigma_i}{\sqrt{\lambda c_i}}
\end{aligned}
\end{equation*}

Now, applying the constraint

\begin{equation*}
\begin{aligned}
&C - C_0 = \sum_{i=1}^K c_in_i
    = \sum_{i=1}^K c_i\frac{W_i\sigma_i}{\sqrt{\lambda c_i}}
    = \frac{1}{\sqrt{\lambda}} \sum_{i=1}^K W_i\sigma_i\sqrt{c_i}
\\&\quad\implies
\sqrt{\lambda} = \frac{\sum_{i=1}^K W_i\sigma_i\sqrt{c_i}}{C-C_0}
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
n_i
= \frac{W_i\sigma_i}{\sqrt{c_i}}\frac{C-C_0}{\sum_{k=1}^K W_k\sigma_k\sqrt{c_k}}
= \frac{(C-C_0)W_i\sigma_i/\sqrt{c_i}}{\sum_{k=1}^K W_k\sigma_k\sqrt{c_k}}
\end{equation*}

\begin{proposition}[{}]
If the unit allocation costs are equal, the cost based reduces to
Neyman allocation.

Let \(c^*\) be the equal cost per unit, then \(n = \frac{C-C_0}{c^*}\)
and

\begin{equation*}
n_i
= \frac{(C-C_0)W_i\sigma_i/\sqrt{c^*}}{\sum_{k=1}^K W_k\sigma_k\sqrt{c^*}}
= \frac{C-C_0}{c^*}\frac{W_i\sigma_i}{\sum_{k=1}^K W_k\sigma_k}
= \left(\frac{W_i\sigma_i}{\sum_{k=1}^K W_k\sigma_k}\right)n
\end{equation*}

which is the same as Neyman allocation.
\end{proposition}

\subsubsection{SRSWOR}
\label{develop--stat6130:ROOT:page--stratified.adoc---srswor-3}

Recall that

\begin{equation*}
Var(\overbar{y}_{st})
= \sum_{i=1}^K W_i^2\left(\frac{1}{n_i}-\frac{1}{N_i}\right)S_i^2
= \sum_{i=1}^K W_i^2\frac{S_i^2}{n_i} - \sum_{i=1}^K W_i^2\frac{S_i^2}{N_i}
\end{equation*}

Since the non-constant part of the above is similar to the variance under SRSWR,
we obtain a similar result

\begin{equation*}
n_i
= \frac{(C-C_0)W_iS_i/\sqrt{c_i}}{\sum_{k=1}^K W_kS_k\sqrt{c_k}}
\end{equation*}
\end{document}
