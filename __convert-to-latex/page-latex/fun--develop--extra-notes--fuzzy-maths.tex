\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Fuzzy Mathematics using Python}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% complex conjugate
\def\conj#1{\overbar{#1}}

% Title omitted
This webinar was to be held on the 23rd and 24th of December 2023
but due to an emergency, it was pushed back to the 30th and 31st of December.
Also, it was initially scheduled to start at 2 pm india time (4:30 am local)
but at 2:08 pm india time, they changed the time to 6pm india time (8:30 local).

The webinar was held by INFINITY Educational Solution
and the presenter was Gnanasekar M, Director, Infinity Research and Development Institute.

In the session, we used jupyter through anaconda to learn about fuzzy sets.
We use the standard numpy and matplotlib libraries in addition to
\href{https://pypi.org/project/scikit-fuzzy}{scikit-fuzzy}.

\section{Fuzzy sets}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---fuzzy-sets}

Fuzzy sets are defined using a \emph{membership function}.
Let \(\Theta\) be the \emph{universe of discourse}.
Then a fuzzy set $A$ is defined using a membership function
\(\mu_A : \Theta \to [0,1]\) were \(\mu_A(x)\) is the degree of which
\(x\) belongs to the fuzzy set \(A\).
Normal (crisp) sets may also be described using this machinery by defining
\(\mu_A(x)\) to be \(1\) if \(x\in A\) and \(0\) otherwise.
We now define the following terminology

\begin{description}
\item[Support] The support is the set of elements such that \(\mu_A\) is non-zero.
    Ie \(\{x \in \Theta: \mu_A(x) > 0\}\).
\item[Core] The core is the set of elements such that \(\mu_A\) is one.
    Ie \(\{x \in \Theta: \mu_A(x) =1\}\).
\item[Alpha-cut] An alpha-cut with \(\alpha \in (0,1]\) is the set
    of elements such that \(\mu_A\) is at least \(\alpha\).
    Ie \(\{x \in \Theta: \mu_A(x) \geq \alpha\}\).
\end{description}

We say that a fuzzy set is empty iff its membership function consists of only zero values.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/example-fuzzy-set}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0, 11, 0.1)

# Define the parameters of the triangular membership function
a = 2  # Leftmost point
b = 5  # Peak point
c = 8  # Rightmost point

# Generate the triangular membership function
membership_function = fuzz.trimf(x, [a, b, c])

# Find the support (elements with non-zero membership)
support = x[membership_function > 0]

# Find the core (elements with a membership value equal to 1)
core = x[membership_function == 1]

alpha = 0.6
alpha_cut = x[membership_function >= alpha]

# Plot the membership function
plt.plot(x, membership_function, 'b', linewidth=2, label='Triangular Membership Function')

plt.fill_between(support, 0, membership_function[membership_function > 0], color='gray', alpha=0.5, label='Support')
plt.fill_between(alpha_cut, 0, membership_function[membership_function >=alpha], color='black', alpha=0.5, label=f'Alpha-cut ({alpha})')
plt.scatter(core, np.ones_like(core), color='red', label='Core')

plt.title('Triangular Membership Function')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Set equality}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---set-equality}

In general equality between fuzzy sets is nuanced.
However, we can check if two fuzzy sets are approximately equal by checking
if their membership values are within some \(\varepsilon\) of each other.
That is \(A\) and \(B\) are approximately equal if

\begin{equation*}
\forall x \in \Theta: |\mu_A(x) - \mu_B(x)| < \varepsilon
\end{equation*}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/equality}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0,11,1) # start, stop (exclusive), step

# Create two fuzzy sets with trianglar membership functions
# This produces a triangle with zero at a, 1 at b and 0 at c (where a <= b <= c)
fuzzy_set_1 = fuzz.trimf(x, [3,6,9.08])
fuzzy_set_2 = fuzz.trimf(x, [3,6,9.])


# Plot the membership function of the twp fuzzy sets
plt.plot(x, fuzzy_set_1, 'b', label='Fuzzy_set_1')
plt.plot(x, fuzzy_set_2, 'r', label='Fuzzy_set_2')
plt.title('Empty fuzzy set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership value')
plt.legend()
plt.show()

# Check approximate equality of fuzzy sets
epsilon = 1e-6  # Small threshold for approximate equality

# We check if the difference in membership values is small
if np.all(np.abs(fuzzy_set_1 - fuzzy_set_2) < epsilon):
    print("Fuzzy sets are approximately equal.")
else:
    print("Fuzzy sets are not approximately equal.")
\end{listing}
\end{example}

\section{Subsets}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---subsets}

(were not well defined)

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/subset}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
# Define the universe of discourse (range of possible values)
universe_of_discourse = np.arange(0, 11, 1)

# Define the main set (triangular membership function)
main_set = fuzz.trimf(universe_of_discourse, [3, 6, 9])

# Define the subset (triangular membership function)
subset = fuzz.trimf(universe_of_discourse, [4, 7, 8])

# Create subplots
fig, axs = plt.subplots(2, 1, figsize=(8, 6))

# Plot the membership function of the main set in the first subplot
axs[0].plot(universe_of_discourse, main_set, 'b', label='Main Set')
axs[0].set_title('Triangular Fuzzy Main Set')
axs[0].set_xlabel('Universe of Discourse')
axs[0].set_ylabel('Membership Value')
axs[0].legend()

# Plot the membership function of the subset in the second subplot
axs[1].plot(universe_of_discourse, subset, 'r', label='Subset')
axs[1].set_title('Triangular Fuzzy Subset')
axs[1].set_xlabel('Universe of Discourse')
axs[1].set_ylabel('Membership Value')
axs[1].legend()

# Adjust layout for better spacing
plt.tight_layout()

# Show the plot
plt.show()
\end{listing}
\end{example}

\section{Combining fuzzy sets}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---combining-fuzzy-sets}

Let \(A\) and \(B\) be two fuzzy sets.
Then we can compose them using the usual boolean operations as follows

\begin{description}
\item[Union] \(\mu_{A\cup B}(x) = \max(\mu_A(x), \mu_B(x)\)
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/union}
    \end{figure}
    
    \begin{example}[{}]
    \begin{listing}[{}]
    import numpy as np
    import skfuzzy as fuzz
    import matplotlib.pyplot as plt
    
    # Define the universe of discourse (range of possible values)
    x = np.arange(0, 11, 0.1)
    
    # Define the parameters of two triangular fuzzy sets
    a1, b1, c1 = 2, 5, 8  # Fuzzy set 1
    a2, b2, c2 = 4, 7, 10  # Fuzzy set 2
    
    # Generate the membership functions for the two fuzzy sets
    membership_function1 = fuzz.trimf(x, [a1, b1, c1])
    membership_function2 = fuzz.trimf(x, [a2, b2, c2])
    # Perform union (OR) operation to create a composite fuzzy set
    union_membership = np.maximum(membership_function1, membership_function2)
    
    # Plot the membership functions and the union
    plt.plot(x, membership_function1, 'b', label='Fuzzy Set 1', linewidth=5)
    plt.plot(x, membership_function2, 'g', label='Fuzzy Set 2', linewidth=5)
    plt.plot(x, union_membership, 'r', label='Union (OR)')
    plt.title('Union of Two Fuzzy Sets')
    plt.xlabel('Universe of Discourse')
    plt.ylabel('Membership Value')
    plt.legend()
    plt.show()
    \end{listing}
    \end{example}
\item[Intersection] \(\mu_{A\cap B}(x) = \min(\mu_A(x), \mu_B(x)\)
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/intersection}
    \end{figure}
    
    \begin{example}[{}]
    \begin{listing}[{}]
    import numpy as np
    import skfuzzy as fuzz
    import matplotlib.pyplot as plt
    
    # Define the universe of discourse (range of possible values)
    x = np.arange(0, 11, 0.1)
    
    # Define the parameters of two triangular fuzzy sets
    a1, b1, c1 = 2, 5, 8  # Fuzzy set 1
    a2, b2, c2 = 4, 7, 10  # Fuzzy set 2
    
    # Generate the membership functions for the two fuzzy sets
    membership_function1 = fuzz.trimf(x, [a1, b1, c1])
    membership_function2 = fuzz.trimf(x, [a2, b2, c2])
    # Perform intersection (AND) operation to create a composite fuzzy set
    intersection_membership = np.minimum(membership_function1, membership_function2)
    
    # Plot the membership functions and the intersection
    plt.plot(x, membership_function1, 'b', label='Fuzzy Set 1',linewidth=5)
    plt.plot(x, membership_function2, 'g', label='Fuzzy Set 2',linewidth=5)
    plt.plot(x, intersection_membership, 'r', label='Intersection (AND)')
    plt.title('Intersection of Two Fuzzy Sets')
    plt.xlabel('Universe of Discourse')
    plt.ylabel('Membership Value')
    plt.legend()
    plt.show()
    \end{listing}
    \end{example}
\item[Complement] \(\mu_{\overbar{A}}(x) = 1 - \mu_A(x)\)
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/complement}
    \end{figure}
    
    \begin{example}[{}]
    \begin{listing}[{}]
    import numpy as np
    import skfuzzy as fuzz
    import matplotlib.pyplot as plt
    
    # Define the universe of discourse (range of possible values)
    x = np.arange(0, 11, 0.1)
    
    # Define the parameters of a triangular fuzzy set
    a, b, c = 2, 5, 8
    
    # Generate the membership function for the fuzzy set
    membership_function = fuzz.trimf(x, [a, b, c])
    
    # Perform complement (NOT) operation to create a new fuzzy set
    complement_membership = 1 - membership_function
    # Plot the membership function and its complement
    plt.plot(x, membership_function, 'b', label='Original Fuzzy Set', linewidth=5)
    plt.plot(x, complement_membership, 'r', label='Complement (NOT)', linewidth=5)
    plt.title('Complement (NOT) Operation for a Fuzzy Set')
    plt.xlabel('Universe of Discourse')
    plt.ylabel('Membership Value')
    plt.legend()
    plt.show()
    \end{listing}
    \end{example}
\item[Difference] \(\mu_{A-B}(x) = \max(\mu_A(x)-\mu_B(x), 0)\)
    
    \begin{admonition-note}[{}]
    This is different from \(A\cap B^c\). While both approaches
    are compatible with usual set difference, we prefer this one since it produces nicer
    results. (Plus its just notation anyhow)
    \end{admonition-note}
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/intersection}
    \end{figure}
    
    \begin{example}[{}]
    \begin{listing}[{}]
    import numpy as np
    import skfuzzy as fuzz
    import matplotlib.pyplot as plt
    
    # Define the universe of discourse (range of possible values)
    x = np.arange(0, 11, 0.1)
    
    # Define the parameters of two triangular fuzzy sets
    a1, b1, c1 = 2, 5, 8  # Fuzzy set 1
    a2, b2, c2 = 4, 7, 10  # Fuzzy set 2
    
    # Generate the membership functions for the two fuzzy sets
    membership_function1 = fuzz.trimf(x, [a1, b1, c1])
    membership_function2 = fuzz.trimf(x, [a2, b2, c2])
    # Perform intersection (AND) operation to create a composite fuzzy set
    difference_membership = np.maximum(membership_function1 - membership_function2, 0)
    other_difference = np.minimum(membership_function1, 1 - membership_function2)
    
    # Plot the membership functions and the intersection
    plt.plot(x, membership_function1, 'b', label='Fuzzy Set 1',linewidth=5)
    plt.plot(x, membership_function2, 'g', label='Fuzzy Set 2',linewidth=5)
    plt.plot(x, difference_membership, 'r', label='Difference (A - B)')
    plt.plot(x, other_difference, 'k', label='Other difference (A n B^c)')
    plt.title('Difference operation for Two Fuzzy Sets')
    plt.xlabel('Universe of Discourse')
    plt.ylabel('Membership Value')
    plt.legend()
    plt.show()
    \end{listing}
    \end{example}
\end{description}

\section{Some common fuzzy sets}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---some-common-fuzzy-sets}

\subsection{Triangular}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---triangular}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/triangular}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0, 11, 0.1)

# Define the parameters of the triangular membership function
a = 2  # Leftmost point
b = 5  # Peak point
c = 8  # Rightmost point
# Generate the triangular membership function
membership_function = fuzz.trimf(x, [a, b, c])

# Plot the membership function
plt.plot(x, membership_function, 'b', linewidth=2, label='Triangular Membership Function')
plt.title('Triangular Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Trapezoidal}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---trapezoidal}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/trapezoidal}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0, 11, 0.1)

# Define the parameters of the trapezoidal membership function
a = 2    # Leftmost point
b = 4    # Left-peak point
c = 7    # Right-peak point
d = 9    # Rightmost point
# Generate the trapezoidal membership function
membership_function = fuzz.trapmf(x, [a, b, c, d])

# Plot the membership function
plt.plot(x, membership_function, 'b', linewidth=2, label='Trapezoidal Membership Function')
plt.title('Trapezoidal Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Gaussian}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---gaussian}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/gaussian}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0, 11, 0.1)

# Define the parameters of the Gaussian membership function
mean = 5      # Mean or center of the distribution
std_dev = 2   # Standard deviation or spread of the distribution

# Generate the Gaussian membership function
membership_function = fuzz.gaussmf(x, mean, std_dev)
# Plot the membership function
plt.plot(x, membership_function, 'b', linewidth=2, label='Gaussian Membership Function')
plt.title('Gaussian Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Sigmoidal}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---sigmoidal}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/sigmoidal}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(-5, 5, 0.1)

# Define the parameters of the sigmoidal membership function
a = 1  # Controls the steepness of the S-curve
c = 0  # Shifts the S-curve horizontally

# Generate the sigmoidal membership function
membership_function = fuzz.sigmf(x, c, a)
# Plot the membership function
plt.plot(x, membership_function, 'b', linewidth=2, label='Sigmoidal Membership Function')
plt.title('Sigmoidal Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Generalized bell}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---generalized-bell}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/generalized-bell}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0, 11, 0.1)

# Define the parameters of the generalized bell membership function
c = 5   # Center or peak point
a = 2   # Width of the curve
b = 3   # Steepness of the curve
# Generate the generalized bell membership function
membership_function = fuzz.gbellmf(x, a, b, c)

# Plot the membership function
plt.plot(x, membership_function, 'b', linewidth=2, label='Generalized Bell Membership Function')
plt.title('Generalized Bell Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Singleton}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---singleton}

\begin{admonition-warning}[{}]
I am not sure if this is correct. I expected it to be a Kronecker delta function
\end{admonition-warning}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/singleton}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
# Define the universe of discourse (range of possible values)
x = np.arange(0, 11, 1)
# Choose a specific element for the singleton fuzzy set
c = 5
# Generate the membership function for the singleton fuzzy set
membership_function = fuzz.smf(x, c, c)
# Plot the membership function
plt.stem(x, membership_function, 'b', basefmt=' ', markerfmt='bo', label='Singleton Membership Function')
plt.title('Singleton Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\subsection{Linguistic}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---linguistic}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/linguistic}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Define the universe of discourse (range of possible values)
x = np.arange(0, 101, 1)

# Define linguistic terms and their associated membership functions
low = fuzz.trimf(x, [0, 0, 50])
medium = fuzz.trimf(x, [0, 50, 100])
high = fuzz.trimf(x, [50, 100, 100])
# Plot the linguistic fuzzy set
plt.plot(x, low, label='Low')
plt.plot(x, medium, label='Medium')
plt.plot(x, high, label='High')
plt.title('Linguistic Fuzzy Set')
plt.xlabel('Universe of Discourse')
plt.ylabel('Membership Value')
plt.legend()
plt.show()
\end{listing}
\end{example}

\section{Control systems}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---control-systems}

\begin{admonition-note}[{}]
I looked at \url{https://en.wikipedia.org/wiki/Fuzzy\_control\_system} for further clarification.
\end{admonition-note}

In a fuzzy control system, we have inputs, outputs and rules.
Suppose we have a system which has fan speed and fan temperature as inputs and an output as fan speed.

\begin{itemize}
\item temperature speed has three fuzzy sets: cool, warm and hot (\(T_A, T_B, T_C\))
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/control-systems/temperature}
    \end{figure}
\item humidity speed has three fuzzy sets: low, medium and high (\(H_A, H_B, H_C\))
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/control-systems/humidity}
    \end{figure}
\item fan speed has three fuzzy sets: low, medium and high (\(F_A, F_B, F_C\))
\end{itemize}

So for example, for each temperature, we set the likelihood that we will classify it as cool, warm and hot.
These likelihoods allow us to build our membership function.

We have three rules to access

\begin{itemize}
\item hot and high humidity yields high fan speed
\item warm and medium humidity yields medium fan speed
\item cool and low humidity yields low fan speed
\end{itemize}

Note that by using the fuzzy sets instead of actual values, we get simple rules rather than complicated mathematical rules.

So, say we are given a concrete value of temperature and humidity \((t,h)\) and we want to determine the fan speed \(f\)
using the given rules. We use the following steps

\begin{enumerate}[label=\arabic*)]
\item (Fuzzification) For each temperature set find the membership value of \(t\); so find \(T_A(t), T_B(t), T_C(t)\).
    Do the same for humidity using \(h\); so find \(H_A(h), H_B(h), H_C(h)\).
\item (Aggregation) For each rule, determine the membership of the output. So our rules are
    
    \begin{itemize}
    \item Hot and high yield high fan speed. So \(F_C(f) = \min(T_C(t), H_C(t))\) (since and corresponds to min)
    \item Warm and medium yield medium fan speed. So \(F_B(f) = \min(T_B(t), H_B(t))\)
    \item Cool and Low yield low fan speed. So \(F_A(f) = \min(T_A(t), H_A(t))\)
    \end{itemize}
\item (Defuzzification) Now that we have the membership values for each of the fan speeds, we can infer the desired fan speed.
    For each of the fan output speeds, clip the membership function to be at most as specified by the computed
    values of \(F_A(f), F_B(f), F_C(f)\). We use these clipped membership functions to find the fan speed
    \(\tilde{f}\).
    The choice of \(\tilde{f}\) may not be immediately obvious, but we can use different strategies:
    
    \begin{description}
    \item[Centroid] Find the center of mass (\(\tilde{f}\)) of the clipped membership functions (where the heights represent the masses).
        Note that if any of \(F(f)\) is zero, they do not contribute to the center of mass.
    \item[Height] Find the value of \(\tilde{f}\) which maximizes the clipped membership function
    \end{description}
\end{enumerate}

In the below diagram we see the clipped membership functions and the choice of fan speed using the centroid method

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-fun/extra-notes/fuzzy/control-systems/fan-speed}
\end{figure}

\begin{example}[{}]
\begin{listing}[{}]
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

# Create fuzzy input variables
temperature = ctrl.Antecedent(np.arange(0, 101, 1), 'temperature')
humidity = ctrl.Antecedent(np.arange(0, 101, 1), 'humidity')

# Create fuzzy output variable
fan_speed = ctrl.Consequent(np.arange(0, 101, 1), 'fan_speed', defuzzify_method='centroid')
# Define membership functions for input and output variables
temperature['HOT'] = fuzz.trimf(temperature.universe, [80, 100, 100])
temperature['WARM'] = fuzz.trimf(temperature.universe, [0, 50, 80])
temperature['COOL'] = fuzz.trimf(temperature.universe, [0, 0, 50])

humidity['HIGH'] = fuzz.trimf(humidity.universe, [70, 100, 100])
humidity['MEDIUM'] = fuzz.trimf(humidity.universe, [30, 50, 70])
humidity['LOW'] = fuzz.trimf(humidity.universe, [0, 0, 40])
fan_speed['LOW'] = fuzz.trimf(fan_speed.universe, [0, 0, 50])
fan_speed['MEDIUM'] = fuzz.trimf(fan_speed.universe, [0, 50, 100])
fan_speed['HIGH'] = fuzz.trimf(fan_speed.universe, [50, 100, 100])

# Define fuzzy rules
rule1 = ctrl.Rule(temperature['HOT'] & humidity['HIGH'], fan_speed['HIGH'])
rule2 = ctrl.Rule(temperature['WARM'] & humidity['MEDIUM'], fan_speed['MEDIUM'])
rule3 = ctrl.Rule(temperature['COOL'] & humidity['LOW'], fan_speed['LOW'])
# Create fuzzy control system
fan_ctrl = ctrl.ControlSystem([rule1, rule2, rule3])
fan_speed_ctrl = ctrl.ControlSystemSimulation(fan_ctrl)

# Set input values
fan_speed_ctrl.input['temperature'] = 30
fan_speed_ctrl.input['humidity'] = 33

# Perform fuzzy inference
fan_speed_ctrl.compute()

# Get output value
output_value = fan_speed_ctrl.output['fan_speed']
print("Fan Speed:", output_value)
# Visualize the membership functions and the result
temperature.view()
humidity.view()
fan_speed.view(sim=fan_speed_ctrl)
\end{listing}
\end{example}

\begin{admonition-note}[{}]
In this system, there are some combinations of inputs for which there are no applicable rules and hence
the fan speed is undefined.
\end{admonition-note}

\section{Other stuff}
\label{develop--fun:extra-notes:page--fuzzy-maths.adoc---other-stuff}

At the end of the second day, he (the presenter) briefly went through a few other topics

\begin{itemize}
\item fixed point mapping
    
    I don't think he did this correctly. Instead of applying mappings to the elements, he applied mappings
    to the membership values.
\item t-norms
    
    Applied t-norms to membership values. See (\url{https://en.wikipedia.org/wiki/T-norm} ). The code was straightforward.
    Just apply the t-norm to two sets
\item fuzzy metric space
    
    He simulated fuzzy distances (ie simulated \(|a-b| + rand(0,1)\)) rather than producing a membership function. I found this disappointing
\item neutroscophic fuzzy set
    
    I have to read up more on this
\item neutroscophic fuzzy Pythagorean set
\end{itemize}
\end{document}
