\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Markov Chains}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(X_0, X_1, \ldots\) be a sequence of discrete random variable
which can take values from \textbf{state space} \(S\), which is finite.
The sequence \(X_0, X_1, \ldots\) has the \textbf{Markovian property}
if each \(X_{t+1}\) depends only on \(X_t\).
That is,

\begin{equation*}
P(X_{t+1} = x_{t+1} \ | \ X_t=x_t, \ X_{t-1} = x_{t-1}, \ldots) = P(X_{t+1} = x_{t+1} \ | \ X_t = x_t)
\end{equation*}

The sequence \(X_0, X_1, \ldots\) is called a \textbf{markov chain} if they satisfy
the markovian property.
In this page, we will assume that transition  probabilities are independent of time.
That is

\begin{equation*}
\forall t,t' \geq 0: P(X_{t+1} = j \ | \ X_t = i) = P(X_{t'+1} = j \ | \ X_{t'} = i)
\end{equation*}

When using this assumption, we can neatly illustrate the markov chain
using one of the following

\begin{description}
\item[Transition Diagram] The transition diagram is an edge-weighted directed graph
    where the edge from state \(i\) to state \(j\)
    has weight \(P(X_{t+1}=j \ | \ X_{t} = i)\).
    Note that the sum of weights of edges leaving each node is 1.
    Also, for clarity, we may omit edges which have zero weight.
\item[Transition Matrix] A transition matrix, \(P\), is such that
    row \(i\) and column \(j\) contains
    the probability of transitioning from \(i\) to \(j\).
    That is \(P_{ij} = P(X_{t+1} =j \ | \ X_{t} = i)\).
    Note that the sum of entries in each row must be one.
    Also, when writing the matrix, we may often label the states which each row and column represents.
\end{description}

\begin{admonition-tip}[{}]
If the columns also sum to one, the markov chain is called \textbf{doubly stochastic}
\end{admonition-tip}

\begin{example}[{Markov chain example}]
The following diagram and matrix are an example of a markov chain with state space
\(S = \{1,2,3,4,5,6,7\}\)

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-math6180/ROOT/markov-chain-example}
\caption{Transition diagram}
\end{figure}

The transition matrix is

\begin{equation*}
% Idea to get labeled matrix from
% sindzicat on https://github.com/mathjax/MathJax/issues/2031
\begin{matrix}\\
\begin{matrix} 1\\2\\3\\4\\5\\6\\7 \end{matrix}
\left[\vphantom{\begin{matrix} 1\\2\\3\\4\\5\\6\\7 \end{matrix}}\right.
\end{matrix}
\hspace{0.2em}
\begin{matrix}
1&2&3&4&5&6&7\\
\frac15 & \frac35 & 0 & 0 & \frac15 & 0 & 0\\
0 & 0 & 1 & 0 & 0 & 0 & 0 \\
0 & \frac23 & 0 & \frac13 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 1 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 1\\
0 & 0 & 0 & 0 & 1 & 0 & 0\\
\end{matrix}
\hspace{0.2em}
\begin{matrix}\\
\left.\vphantom{\begin{matrix} 1\\2\\3\\4\\5\\6\\7 \end{matrix}}\right]
\end{matrix}
\end{equation*}
\end{example}

\begin{theorem}[{Chapman-Kolmogorv Theorem}]
Let \(P^{(n,m)}\) and \(P^{(m,l)}\) denote the transition matricies
from times \(n\) to \(m\), and times \(m\) to \(l\) (respectively).
Then, the transition matrix from times \(n\) to \(l\) is given by

\begin{equation*}
P^{(n,l)}
= P^{(n,m)}P^{(m,l)}
\end{equation*}

\begin{proof}[{}]
We prove this result component-wise

\begin{equation*}
\begin{aligned}
P^{(n,l)}_{ij}
= P(X_l =j | X_n = i)
&= \sum_{k} P((X_l = j \text{ and } X_m = k)| X_n =i)
\\&= \sum_{k} P(X_l = j | X_m = k | X_n =i) P(X_m =k | X_n =i)
\\&= \sum_{k} P(X_l = j | X_m = k) P(X_m =k | X_n =i)
\\&= \sum_{k} P^{(m,l)}_{kj} P^{(n,m)}_{ik}
\\&= \sum_{k} P^{(n,m)}_{ik} P^{(m,l)}_{kj}
\\&= [P^{(n,m)} P^{(m,l)}]_{ij}
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{corollary}[{Transition matrix for \(n\) steps}]
Let \(P\) be the transition (one-step) transition matrix
of a markov chain.
Then, the transition matrix for \(n\) steps is simply
given by \(P^n\).
\end{corollary}

\begin{theorem}[{Distribution after \(n\) steps}]
Let \(\vec{\pi}^{(0)}\) denote the distribution of \(X_0\).
Then, the distribution after \(n\) steps
is given by

\begin{equation*}
\vec{\pi}^{(n)} = \vec{\pi}^{(0)} P^n
\end{equation*}

Note that \(\vec{\pi}^{(n)}\) and \(\vec{\pi}^{(0)}\) are both row vectors.

\begin{proof}[{}]
We prove this component-wise.

\begin{equation*}
\vec{\pi}^{(n)}_i = P(X_n=i)
= \sum_{k} P(X_n=i \ | \ X_0=k)P(X_0=k)
= \sum_{k} P^n_{ki}\vec{\pi}^{(0)}_k
= (\vec{\pi}^{(0)}P^n)_i
\end{equation*}
\end{proof}
\end{theorem}

\section{Miscellaneous}
\label{develop--math6180:ROOT:page--markov-chains.adoc---miscellaneous}

\begin{description}
\item[Trajectory] a set of values for
    \(X_1, \ldots\) (or a finite subset).
    For example, \(X_1=1\), \(X_2=4\) and \(X_3=6\)
    is a trajectory and may be represented by \((1,4,6)\)
\item[Stochastic] Let \(M\) be a square matrix with non-negative entries
    
    \begin{description}
    \item[Row stochastic] each row of \(M\) sums to 1
    \item[Column stochastic] each column of \(M\) sums to 1
    \item[Doubly stochastic] each row and column of \(M\) sums to 1
    \end{description}
\end{description}

\section{Communication}
\label{develop--math6180:ROOT:page--markov-chains.adoc---communication}

We say that state \(i\) \textbf{communicates} with state \(j\)
if \(P_{ij}^{n} > 0\) for some \(n \geq 0\).
That is, we can reach from state \(i\) to state \(j\) after some finite number of steps.
We denote this by \(i \to j\).
It is important to note that \(i\to i\) always (when \(n=0\)).

Additionally, if \(i \to j\) and \(j \to i\),
we say that there is \textbf{two way communication} between \(i\) and \(j\).
We denote this by \(i \leftrightarrow\).

\begin{admonition-caution}[{}]
Some books call \(\to\) \textbf{accessible} and call \(\leftrightarrow\) \textbf{communicates}.
\end{admonition-caution}

\begin{lemma}[{}]
Communication is a transitive and reflexive relation.

\begin{proof}[{}]
As we have already stated \(i\to i\) since \(P^0 = I\) (identity matrix).
Now, suppose \(i\to j\) (after \(n\) steps) and \(j\to k\)
(after \(m\) steps).
Then,

\begin{equation*}
\begin{aligned}
P_{ik}^{n+m}
&= P(X_{m+n} = k \ | \ X_0=i)
\\&= \sum_{j'} P(X_{m+n} = k \ | \ X_n=j' \text{ and } X_0=i)P(X_n = j' \ | \ X_0 = i)
\quad\text{ by conditioning on } X_n
\\&\geq P(X_{m+n} = k \ | \ X_n=j \text{ and } X_0=i)P(X_n = j \ | \ X_0 = i)
\quad\text{by examining one term}
\\&= P(X_{m+n} = k \ | \ X_n=j)P(X_n = j \ | \ X_0 = i)
\quad\text{by markov property}
\\&= P(X_{m} = k \ | \ X_0=j)P(X_n = j \ | \ X_0 = i)
\quad\text{since the one step probabilities does not depend on time}
\\&= P_{jk}^m P_{ij}^n > 0
\end{aligned}
\end{equation*}

Therefore \(i\to k\)

\begin{admonition-remark}[{}]
This could have been easily proven using Chapman-Kolmogorv.
But the notation is a bit messy.
\end{admonition-remark}
\end{proof}
\end{lemma}

\begin{corollary}[{}]
Two way communication is an equivalence relation
\end{corollary}

Since two-way communication is an equivalence relation, we can then speak of \textbf{communicating classes}
which are the equivalence classes for this relation.
If there is a single communicating class, we say that the markov chain
is \textbf{irreducible}.

\begin{admonition-tip}[{}]
From a graph theoretic perspective, communicating classes are simply the strongly connected components.
\end{admonition-tip}

Let \(C\) be a communicating class.
Then, we say that it is \textbf{closed} if the probability of leaving \(C\) is zero.
That is

\begin{equation*}
\forall n\geq 0: \forall i \in C: \forall j \notin C: P^n_{ij} = 0
\end{equation*}

\section{Recurrent}
\label{develop--math6180:ROOT:page--markov-chains.adoc---recurrent}

Let \(f_{ij}\) be the probability of being in state \(j\) after a finite number
of steps, given that \(X_0 = i\).
That is

\begin{equation*}
f_{ij}
= P(X_n=j\text{ for some } n \geq 1 \ | \ X_0=i)
\end{equation*}

\begin{description}
\item[Recurrent] state \(i\) is \textbf{recurrent} if \(f_{ii} =1\).
    Furthermore, it is \textbf{positive recurrent} if the expected
    time until returning to state \(i\) is finite.
    Note that for finite-state markov chains,
    all recurrent states are positive recurrent.
\item[Transient] state \(i\) is \textbf{transient}.
\end{description}

\begin{theorem}[{}]
State \(i\) is recurrent iff

\begin{equation*}
\sum_{i=0}^\infty P_{ii}^n = \infty
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof comes from ``Introduction to probability models'' by Sheldon Ross.
\end{admonition-note}

Define \(I_n=1\) if \(X_n=i\) and \(0\) otherwise.
Then, consider the expected number of times which the markov chain will be in state
\(i\),

\begin{equation*}
E[N \ | \ X_0=i] =  E\left[\sum_{i=0}^\infty I_n \ | \ X_0=i\right]
\end{equation*}

By conditioning on the first return, we see that

\begin{equation*}
E[N \ | \ X_0=i]
= f_{ii}(1 + E[N \ | \ X_0=i]) + (1-f_{ii})(0)
= f_{ii}(1 + E[N \ | \ X_0=i])
\end{equation*}

\begin{itemize}
\item If state \(i\) is recurrent, \(f_{ii}=1\). So there is no solution
    in \(\mathbb{R}\). Since \(E[N \ | \ X_0=i]\) is the sum of positive variables, it must be \(\infty\)
\item If state \(i\) is transient, \(f_{ii} < 1\).
    So, there is a solution in \(\mathbb{R}\).
\end{itemize}

Now, note that

\begin{equation*}
\begin{aligned}
E[N \ | \ X_0=i]
&= \sum_{n=0}^\infty E[I_n \ | \ X_0=i]
\\&= \sum_{n=0}^\infty P(X_n =i \ | \ X_0=i]
\\&= \sum_{n=0}^\infty P_{ii}^n
\end{aligned}
\end{equation*}

Therefore, we get the desired result.
\end{proof}
\end{theorem}

\begin{sidebar}[{Quick investigation}]
The above relationship is more complicated than it appears.
Note that if \(i\to j\),

\begin{equation*}
\begin{aligned}
f_{ij}
&= \sum_{k} P(X_n=j\text{ for some } n \geq 1 \ | \ X_1 = k, X_0=i)P(X_1=k \ | \ X_0=i)
\\&= P_{ij} + \sum_{k\neq j} P_{ik}f_{kj}
\end{aligned}
\end{equation*}

If \(j\) is not accessible from \(i\), we immediately know that \(f_{ij} = 0\).
We'll ignore this case for now.

Consider the 3x3 case and focus on \(j=1\).

\begin{equation*}
\begin{bmatrix}
f_{11} \\ f_{21} \\ f_{31}
\end{bmatrix}
=
\begin{bmatrix}
P_{11} & P_{12} & P_{13}\\
P_{21} & P_{22} & P_{23}\\
P_{31} & P_{32} & P_{33}\\
\end{bmatrix}
\begin{bmatrix}
1 \\ f_{21} \\ f_{31}
\end{bmatrix}
\end{equation*}

Focus on the equations in the second and third rows.
If there is a unique solution, we have that

\begin{equation*}
\begin{bmatrix}
f_{21} \\ f_{31}
\end{bmatrix}
= \frac{1}{(1-P_{22})(1-P_{33}) -P_{23}P_{32}}
\begin{bmatrix}
1-P_{33} & P_{23}\\
P_{32} & 1-P_{22}\\
\end{bmatrix}
\begin{bmatrix}
1-P_{22}-P_{23} \\ 1-P_{32}-P_{33}
\end{bmatrix}
=
\begin{bmatrix}
1 \\ 1
\end{bmatrix}
\end{equation*}

This is not what we expect. Ideally, we should get a result less than 1.
We get more interesting results if the determinant is zero.
So, perhaps we should know what that determinant represents.
Note that

\begin{equation*}
(1-P_{22})(1-P_{33}) -P_{23}P_{32}
= (P_{21} + P_{23})(P_{31} + P_{32}) -P_{23}P_{32}
= P_{21}(P_{31} + P_{32}) + P_{23}P_{31}
\end{equation*}

Therefore the determinant is non-zero once both \(2\to 1\) and \(3 \to 1\).
Therefore, the result which we got (\(f_{21}=f_{31}=1\)) is to be expected.

From this little example, it is clear that determining the values of \(f_{ij}\)
is non-trivial.
\end{sidebar}

\begin{theorem}[{Recurrence is a class property}]
Let \(i\leftrightarrow j\).
Then \(i\) is recurrent iff \(j\) is recurrent.
\end{theorem}

\section{Periodic}
\label{develop--math6180:ROOT:page--markov-chains.adoc---periodic}

Consider state \(i\) and consider the set of all paths from \(i\)
to itself.
We define the period of \(i\), \(d(i)\), to be the gcd of the path lengths
from \(i\) to \(i\).
That is

\begin{equation*}
d(i) = \mathrm{gcd}\{n \geq 0: P^n_{ii} > 0\}
\end{equation*}

We say that state \(i\) is \textbf{periodic} if \(d(i) > 1\);
otherwise we state \(i\) is \textbf{aperiodic}.

Note that the period of the state is a long-term period of the set \(S=\{k : X_k=i \ | \ X_0=i\}\).
Now since \(d(i) = \mathrm{gcd}(S)\), there exists a linear combination
of \(S\) which is equal to \(d(i)\) where some coefficients are possibly negative.
Therefore, for sufficiently large path lengths, every path length must have a difference
which is a multiple of \(d(i)\).

\begin{lemma}[{The period is a class property}]
Suppose \(i\leftrightarrow j\). Then, \(d(i) = d(j)\).

\begin{proof}[{}]
This result is clear from the analysis of the limiting behaviour.
\end{proof}
\end{lemma}

\section{Equilibrium distribution}
\label{develop--math6180:ROOT:page--markov-chains.adoc---equilibrium-distribution}

We say that the distribution \(\vec{\pi}\) is an \textbf{equilibrium} distribution if

\begin{equation*}
\vec{\pi} = \vec{\pi}P
\end{equation*}

We are interested in whether or not an equilibrium distribution exists and whether it is unique.
Intuitively we would also like to know whether this equilibrium distribution
is the \textbf{limiting distribution}.
That is, given any initial distribution \(\vec{\pi}\), \(\lim_{n\to\infty} \vec{\pi}P^n\) exists
and does not depend on \(\vec{\pi}\).

A state \(i\) is called \textbf{ergodic} if it is aperiodic and positive recurrent.
If the markov chain is irreducible, we may also say that it is ergodic.

\begin{admonition-tip}[{}]
Often, only a subset of our states are aperiod and recurrent.
In such a case, we can still use the below theorems as long as the subset
forms a closed communicating class.
\end{admonition-tip}

\begin{theorem}[{Existence of limiting distribution}]
For an irreducible, ergodic markov chain, \(\lim_{n\to \infty} p_{ij}^{(n)}\) exists
and does not depend on \(i\).
We define

\begin{equation*}
\pi_j = \lim_{n\to\infty} p_{ij}^{(n)}
\end{equation*}

Then,
\(\vec{\pi}\) is the unique non-negative solution to the equation

\begin{equation*}
\pi_j = \sum_{i=1}^\infty \pi_ip_{ij} \quad\text{and}\quad \sum_{i=1}^\infty \pi_j=1
\end{equation*}
\end{theorem}

\begin{theorem}[{Ergodic Theorem}]
Let \(\{X_n\}\) be a irreducible, aperiodic and recurrent markov chain.
Then,

\begin{enumerate}[label=\arabic*)]
\item \(\lim_{n\to\infty} p_{ij}^{(n)}\) exists and only depends on \(j\).
    Define \(\pi_j = \lim_{n\to\infty} p_{ij}^{(n)}\)
\item If \(\mu_{ii}\) is the mean time of first return from state \(i\),
    then \(\pi_i = \frac{1}{\mu_{ii}}\)
\item The \(\pi_i\) satisfy \(\vec{\pi}=\vec{\pi}P\) where \(P\)
    is the transition matrix
\item \(\vec{\pi}\) is the stationary distribution of \(\{X_n\}\).
\end{enumerate}

Also, in the case where \(\{X_n\}\) is periodic with period \(d\),

\begin{equation*}
\lim_{n\to\infty} p_{ii}^{nd} = \frac{1}{\mu_{ii}}
\end{equation*}
\end{theorem}

\begin{theorem}[{Limiting distribution for double stochastic Markov chains}]
Suppose that we have an ergodic Markov chain which is doubly stochastic and has \(n\)
states.
Then \(\pi_i = \frac{1}{n}\) for \(i=1,\ldots n\).
\end{theorem}

\begin{theorem}[{Probability for transient states}]
Let \(i\) be a transient state and \(C\) be a closed class with limiting
distribution \(\vec{\pi}\).
Then, for any \(j\in C\)

\begin{equation*}
\lim_{n\to\infty} p_{ij}^{(n)} = \pi_i(C)\pi_j
\end{equation*}

where \(\pi_i(C)\) is the probability that \(X_n \in C\)
at some time, given that \(X_0=i\).
\end{theorem}

\begin{example}[{Large example}]
Consider the following transition diagram

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6180/ROOT/markov-chain-example2}
\end{figure}

Note that there communicating classes

\begin{itemize}
\item \(C_1 = \{1,2,3\}\) which is closed and recurrent
\item \(C_2 = \{4,5\}\) which is not closed and transient
\item \(C_3 = \{6,7\}\) which is closed and recurrent
\end{itemize}

We want to find \(\lim_{n\to\infty} p_{42}^{(n)}\).
Note that \(2\) is recurrent but \(4\) is transient.
So, we need to use

\begin{equation*}
\lim_{n\to\infty} p_{42}^{(n)} = \pi_{4}(C_1) \pi_2
\end{equation*}

Consider the transition matrix for \(C_1\)

\begin{equation*}
% Idea to get labeled matrix from
% sindzicat on https://github.com/mathjax/MathJax/issues/2031
\begin{matrix}\\
\begin{matrix} 1\\2\\3 \end{matrix}
\left[\vphantom{\begin{matrix} 1\\2\\3 \end{matrix}}\right.
\end{matrix}
\hspace{0.2em}
\begin{matrix}
1&2&3\\
0 & \frac12 & \frac12\\
\frac13 & \frac23 & 0\\
\frac23 & 0 & \frac13\\
\end{matrix}
\hspace{0.2em}
\begin{matrix}\\
\left.\vphantom{\begin{matrix} 1\\2\\3 \end{matrix}}\right]
\end{matrix}
\end{equation*}

This matrix is not doubly stochastic, but it is easy to see that the limiting distribution is

\begin{equation*}
\begin{matrix}\\
\vec{\pi} =
[\end{matrix}
\begin{matrix}
1 & 2 & 3 \\
\frac4{13} & \frac{6}{13} & \frac{3}{13}
\end{matrix}
\begin{matrix}\\ ]\end{matrix}
\end{equation*}

Also, by solving

\begin{equation*}
\begin{aligned}
\pi_{4}(C_1) &= \frac18 + \frac12\pi_4(C_1) + \frac14 \pi_5(C_1) \\
\pi_{5}(C_1) &= \frac18 + \frac12\pi_5(C_1) + \frac14 \pi_4(C_1) \\
\end{aligned}
\end{equation*}

We get \(\pi_4(C_1) = \frac12\) and hence

\begin{equation*}
\lim_{n\to\infty} p_{42}^{(n)} = \frac{6}{13}\times \frac12 = \frac3{13}
\end{equation*}

Alternatively, we could have used the following R code to get an approximate result

\begin{listing}[{}]
A = rbind(
  c(0, 1/2, 1/2, 0, 0, 0, 0),
  c(1/3, 2/3, 0, 0, 0, 0, 0),
  c(2/3, 0, 1/3, 0, 0, 0, 0),
  c(0, 1/8, 0, 1/2, 1/4, 0, 1/8),
  c(0, 0, 1/8, 1/4, 1/2, 1/8, 0),
  c(0, 0, 0, 0, 0, 3/5, 2/5),
  c(0, 0, 0, 0, 0, 2/5, 3/5)
)
Reduce(`%*%`, replicate(50, A, simplify=FALSE))
\end{listing}
\end{example}

\subsection{Using Diagonalized form}
\label{develop--math6180:ROOT:page--markov-chains.adoc---using-diagonalized-form}

If a Markov chain has transition matrix \(P\),
we can \emph{easily} find \(\lim_{n\to \infty}P^{n}\) if we can diagonalize \(P\).
That is, if \(P = CDC^{-1}\) and \(D\) is diagonal,

\begin{equation*}
\lim_{n\to\infty} P^{n} = C \left(\lim_{n\to\infty} D^{n}\right) C^{-1}
\end{equation*}

This process also works if we can find the Jordan-normal form of \(P\).

\begin{proposition}[{The eigenvalues of a stochastic matrtix have a modulus less than \(1\)}]
Let \(\lambda \in \mathbb{C}\) be an eigenvalue of \(P\) which is row-stochastic.
Then, \(|\lambda| \leq 1\).

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof comes from \url{https://math.stackexchange.com/questions/40320/proof-that-the-largest-eigenvalue-of-a-stochastic-matrix-is-1}
\end{admonition-note}

We prove this result by using the \(L_1\) norm.
Let \(\vec{x}\) be an eigenvector of \(P\).
Then,

\begin{equation*}
\begin{aligned}
|\lambda|\|\vec{x}\|
=\left\|\vec{x}P\right\|
&= \sum_{j} |\vec{x}P|_j
\\&= \sum_{j} \sum_{i} |x_iP_{ij}|
\\&\leq \sum_{j} \sum_{i} P_{ij}|x_i|
\\&= \sum_{i} |x_i|\sum_{j} P_{ij}
\\&= \sum_{i} |x_i|
\\&= \|\vec{x}\|
\end{aligned}
\end{equation*}

Therefore, we get the desired result
\end{proof}
\end{proposition}

\begin{example}[{Case Study: Limiting distribution for 2-state Markov Chain}]
Consider the transition matrix

\begin{equation*}
T = \begin{bmatrix}
1-\alpha & \alpha\\
\beta & 1-\beta
\end{bmatrix}
\end{equation*}

This matrix has characteristic equation

\begin{equation*}
0 = \det (T-\lambda I) = (\lambda -1)(\lambda-1+\alpha+\beta)
\end{equation*}

So, there are eigenvalues \(\lambda_1=1\) and \(\lambda_2=1-\alpha-\beta\).
Next, we find the eigenvectors.
We get

\begin{equation*}
\vec{r}_1 = \begin{bmatrix}1\\1\end{bmatrix}
\quad\text{and}\quad
\vec{r}_2 = \begin{bmatrix}-\alpha \\ \beta \end{bmatrix}
\end{equation*}

So,

\begin{equation*}
C = \begin{bmatrix}
1 & -\alpha\\
1 & \beta
\end{bmatrix}
\quad\text{and}\quad
C^{-1} = \frac{1}{\alpha + \beta}\begin{bmatrix}
\beta & \alpha\\
-1 & 1
\end{bmatrix}
\end{equation*}

and hence

\begin{equation*}
T = \begin{bmatrix}
1 & -\alpha\\
1 & \beta
\end{bmatrix}
\begin{bmatrix}
1 & 0 \\
0 & (1-\alpha-\beta)
\end{bmatrix}
\left(
\frac{1}{\alpha + \beta}
\begin{bmatrix}
\beta & \alpha\\
-1 & 1
\end{bmatrix}\right)
\end{equation*}

So, if \(\alpha+\beta \notin \{0,2\}\)

\begin{equation*}
\lim_{n\to \infty} T^n
= \frac{1}{\alpha + \beta}
\begin{bmatrix}
1 & -\alpha\\
1 & \beta
\end{bmatrix}
\begin{bmatrix}
1 & 0 \\
0 & 0
\end{bmatrix}
\begin{bmatrix}
\beta & \alpha\\
-1 & 1
\end{bmatrix}
= \frac{1}{\alpha + \beta}
\begin{bmatrix}
\beta & \alpha\\
\beta & \alpha\\
\end{bmatrix}
\end{equation*}

Therefore \(\vec{\pi} = \begin{bmatrix}\frac{\beta}{\alpha+\beta} & \frac{\alpha}{\alpha + \beta}\end{bmatrix}\)
\end{example}

\section{Time Reversibility}
\label{develop--math6180:ROOT:page--markov-chains.adoc---time-reversibility}

Consider a Markov chain \(X_0, X_1, \ldots\).
It will be interesting to know if the reverse process, ie \(X_n, X_{n-1}, \ldots\),
is also a Markov chain.
Consider the reversed one step probabilities

\begin{equation*}
Q_{ij} = P(X_{n-1}=j \ | \ X_n = i)
= \frac{P(X_n = i \ | \ X_{n-1} = j)P(X_{n-1} = j)}{P(X_{n} = i)}
= \frac{P_{ji}P(X_{n-1} = j)}{P(X_{n} = i)}
\end{equation*}

We must therefore know the distribution for \(X_{n-1}\) and \(X_n\) in order to determine
the \(Q_{ij}\).
One simple assumption may be that \(X_{n-1}\) and \(X_n\) have the same distribution
and hence they must have a stationary distribution.
Suppose this is the case. Then, we have that

\begin{equation*}
Q_{ij} = \frac{\pi_j P_{ji}}{\pi_i}
\iff
\pi_i Q_{ij} = \pi_j P_{ji}
\end{equation*}

We say that a Markov chain is \textbf{time reversible} if \(Q_{ij} = P_{ij}\)
or equivalently \(\pi_j P_{ji} = \pi_i P_{ij}\).
This is sometimes also called the \textbf{detailed balance}.

\begin{proposition}[{}]
Consider an irreducible Markov chain with transition probabilities \(P_{ij}\).
There exists a probability solution \(\vec{\pi}\)
that satisfies the detailed balance if and only if

\begin{itemize}
\item The markov chain has positive recurrent states
\item The markov chain is time reversible
\item The solution to \(\vec{\pi}\) is a unique stationary distribution
\end{itemize}
\end{proposition}

The previous result is very useful in determining the stationary distribution
of a markov chain.

\begin{example}[{Finding the stationary distribution of a time reversible MC}]
\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6180/ROOT/markov-chain-example-time-reversible}
\end{figure}

Note that this markov chain is aperiodic, irreducible and recurrent (and also positive recurrent since finite).
We need to find \(\vec{\pi}\) such that \(\pi_i P_{ij} = \pi_j P_{ji}\).

\begin{itemize}
\item From \(i=0, j=1\), \(\pi_0 p = \pi_1 (1-p)\)
\item From \(i=1, j=2\), \(\pi_1 p = \pi_2 (1-p)\)
\item From \(i=2, j=3\), \(\pi_2 p = \pi_3 (1-p)\)
\end{itemize}

By solving these equations, we get

\begin{equation*}
\pi_n = \pi_0 \left(\frac{p}{1-p}\right)^n
\end{equation*}

where

\begin{equation*}
\pi_0 = \frac{1}{1 + \frac{p}{1-p} + \left(\frac{p}{1-p}\right)^2 + \left(\frac{p}{1-p}\right)^3}
\end{equation*}
\end{example}
\end{document}
