\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Relations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
A \emph{relation} \(R\) is a set of \myautoref[{ordered pairs}]{develop--math3274:set-theory:page--pairs.adoc---ordered-pairs}.
Then, if \((x,y) \in R\), we write \(xRy\) and say that \(x\) is related
to \(y\).

Additionally, if \(R \subseteq A\times B\) for two sets \(A\) and \(B\),
we say that \(R\) is \emph{a relation from \(A\) to \(B\)}. Furthermore,
if \(A = B\), we simply say that \(R\) is \emph{a relation on \(A\)}.

Throughout this section, we would be primarily focused on relations between two sets.

\section{Basic Operations}
\label{develop--math3274:relations:page--index.adoc---basic-operations}

\subsection{Domain, Codomain and Range}
\label{develop--math3274:relations:page--index.adoc---domain-codomain-and-range}

Let \(R\) be a relation from \(A\) to \(B\), then we define the following

\begin{description}
\item[Domain] \(\dom R = \{x \in A: (x,y) \in R\}\)
\item[Codomain] We say that \(B\) is the \emph{codomain} of \(R\)
\item[Range] \(\ran R = \{y \in B: (x,y) \in R\}\)
\end{description}

\subsection{Composition and inverse}
\label{develop--math3274:relations:page--index.adoc---composition-and-inverse}

Let \(A,B,C\) be sets and \(R \subseteq A\times B\) and \(S\subseteq B\times C\).
Then, we define the \emph{composition} of \(R\) and \(S\) as follows

\begin{equation*}
S \circ R = \{(x,z) \in A \times C: \exists y \in B: (x,y) \in R \text{ and } (y,z) \in S\}
\end{equation*}

Furthermore, we define the \emph{inverse} of \(R\) to be

\begin{equation*}
R^{-1} = \{(y,x): (x,y) \in R\}
\end{equation*}

\section{Classification of relations}
\label{develop--math3274:relations:page--index.adoc---classification-of-relations}

Let \(R\) be a relation on \(A\) then we define the following properties

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}\SetCell[r=1, c=2]{}{Property} &  & {Requirement} & {Example} \\
\hline[\thicktableline]
\SetCell[r=2, c=1]{m, c}{Self} & {Reflexive} & {\(\forall x \in A: (x,x) \in R\)} & {equality} \\
\hline
 & {Irreflexive} & {\(\forall x \in A: (x,x) \notin R\)} & {strict inequality} \\
\hline
\SetCell[r=2, c=1]{m, c}{Symmetry} & {Symmetric} & {\(\forall x,y\in A: (x,y) \in R \implies (y,x) \in R\)} & {equality} \\
\hline
 & {Antisymmetric} & {\(\forall x,y\in A: (x,y) \in R \text{ and } (y, x) \in R \implies x = y\)} & {less than or equal to} \\
\hline
\SetCell[r=2, c=1]{m, c}{Transitivity} & {Transitive} & {\(\forall x,y,z \in A: (x,y) \in R \text{ and } (y,z) \in R \implies (x,z) \in R\)} & {equality} \\
\hline
 & {Atransitive} & {\(\forall x,y,z \in A: (x,y) \in R \text{ and } (y,z) \in R \implies (x,z) \notin R\)} & {\(R = \{(x,x+1): x \in \mathbb{N}\}\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\section{Equivalent relations}
\label{develop--math3274:relations:page--index.adoc---equivalent-relations}

Let \(X\) be a set and \(\sim\) be a relation on \(X\). Then, if \(\sim\)
is reflexive, symmetric and transitive, we call \(\sim\) an \emph{equivalence relation} on \(X\).

Then, for each \(x\in X\), we define the \emph{equivalence class} of \(x\) as

\begin{equation*}
x/~ = [x] = \{y \in X: x \sim y\}
\end{equation*}

Then, we define the \emph{quotient set} induced by \(\sim\) as

\begin{equation*}
X/\sim = \{[x] \in P(X): x\in X\}
\end{equation*}

Then, \(X/\sim\) forms a partition on \(X\).

\begin{admonition-remark}[{}]
Yes, \myautoref[{quotient groups}]{develop--math3272:ROOT:page--basics.adoc---quotient-groups}
are a special case of quotient sets.
\end{admonition-remark}

\subsection{Equivalence relations from partitions}
\label{develop--math3274:relations:page--index.adoc---equivalence-relations-from-partitions}

Let \(Y\subseteq P(X)\) be a partition on \(X\). That is,

\begin{equation*}
\forall x \in X: [\exists y \in Y: x \in Y] \text{ and } [\forall y_1, y_2 \in Y: x \in y_1 \text{ and } x \in y_2 \implies y_1 = y_2]
\end{equation*}

Then, \(Y\) \emph{induces} an equivalence relation on \(X\) defined by \(a\sim b\) iff

\begin{equation*}
\exists y \in Y: a \in Y \text{ and } b \in Y
\end{equation*}

Furthermore, we have the two following results

\begin{itemize}
\item For any equivalence relation, the partition it forms induces the original relation
\item For any partition, the set of equivalence classes of the relation it induces is the partition itself.
\end{itemize}
\end{document}
