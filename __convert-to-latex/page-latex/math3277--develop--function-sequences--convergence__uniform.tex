\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Uniform convergence}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\begin{admonition-remark}[{}]
I believe that this is the most natual way to define the convergence of a sequence of functions as it
requires that the function converges and not just its values.
\end{admonition-remark}

Consider the sequence of functions \(\{f_n\}\) defined on \(A\) and lets naively utilize the normal definition of a limit.
That is \(\{f_n\}\) has limit \(f\) iff

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall n \in \mathbb{N}: n > N \implies |f_n - f| < \varepsilon
\end{equation*}

Now the question remains, what does \(|f_n - f|\) \href{https://www.youtube.com/watch?v=nc-2Jg6b0Dw\&t=19s}{mean?}
Well, again lets consider what might be the most useful, a number proportional to the largest difference between the two functions.
Now, this maximum need not be attained, so instead lets take its supremum. That is

\begin{equation*}
|f_n - f| = \sup_{x \in A} |f_n(x) - f(x)|
\end{equation*}

and now we have reached our next type of convergence, \emph{uniform convergence}. That is, we say that \(\{f_n\}\)
is uniformly convergent to \(f\) iff

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall n \in \mathbb{N}: n > N \implies \sup_{x\in A}|f_n(x) - f(x)| < \varepsilon
\end{equation*}

This is a stronger condition than \myautoref[{pointwise convergence}]{develop--math3277:function-sequences:page--convergence/pointwise.adoc} and in fact preserves
all of the properties which we showed that pointwise convergence doesnt \myautoref[{[citation]}]{develop--math3277:function-sequences:page--convergence/uniform.adoc---preservation-of-properties}. Further, all sequences which are
uniformly convergent are pointwise convergent and both limits converge to the same value.

We can additionally transform our above definition into two equivalent statements

\begin{itemize}
\item By substituting the definition of the limit
    
    \begin{equation*}
    \lim_{n\to \infty} \sup_{x \in A} |f_n(x) - f(x)| = 0
    \end{equation*}
\item By substituting the upper bound property of the supremum
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall n \in \mathbb{N}: \forall x \in A: n > N \implies |f_n(x) - f(x)| < \varepsilon
    \end{equation*}
    
    \begin{admonition-note}[{}]
    In the reverse case, special consideration needs to be taken since the supremum may attain the value of \(\varepsilon\).
    This is easily mitigated by replacing \(\varepsilon\) with half its value first.
    \end{admonition-note}
\end{itemize}

\section{Geometric Interpretation}
\label{develop--math3277:function-sequences:page--convergence/uniform.adoc---geometric-interpretation}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math3277/function-sequences/uniform-convergence}
\end{figure}

\begin{admonition-remark}[{}]
You may think that pointwise convergence looks like this too, however, I need only introduce you to the sequence
\(\{f_n\}\) where \(f_n:(0,\infty)\to\mathbb{R}\) defined by \(f_n(x) = \frac{n}{nx+1}\) to convince you otherwise.
\end{admonition-remark}

\section{Preservation of properties}
\label{develop--math3277:function-sequences:page--convergence/uniform.adoc---preservation-of-properties}

These results are taken from
\href{https://en.wikipedia.org/wiki/Uniform\_convergence\#Applications}{the Wikipedia article for uniform convergence}.

\begin{admonition-remark}[{}]
Lol, all I had to do was look later in the notes to find these results.
\end{admonition-remark}

\subsection{Boundedness}
\label{develop--math3277:function-sequences:page--convergence/uniform.adoc---boundedness}

Let \(\{f_n\}\) be a uniformly convergent sequence which converges to \(f\).
Then if \(f\) is

\begin{itemize}
\item bounded, there exists an \(N \in \mathbb{N}\) such for all \(n > N\), \(f_n\) is bounded
\item unbounded, there exists an \(N \in \mathbb{N}\) such for all \(n > N\), \(f_n\) is unbounded
\end{itemize}

\begin{admonition-note}[{}]
The above statements are "if and only if" since \(f\) is either bounded or unbounded.
\end{admonition-note}

\begin{example}[{Proof}]
Let \(\varepsilon > 0\), then since \(\{N \in \mathbb{N}\}\) such that

\begin{equation*}
\forall n \in \mathbb{N}: \forall x \in A: n > N \implies |f_n(x) - f(x)| < \varepsilon
\end{equation*}

Then, we have two cases,

\begin{description}
\item[Bounded] If \(f\) is bounded between \(-M\) and \(M\), and hence
    
    \begin{equation*}
    |f_n(x)| = |f_n(x) - f(x) + f(x)| \leq |f_n(x) - f(x)|  + |f(x)| \leq \varepsilon + M
    \end{equation*}
\item[Unbounded] That is for each \(M > 0\), \(\exists x \in A\) such that \(|f(x)| > M\), then
    
    \begin{equation*}
    |f_n(x)| = |f_n(x) - f(x) + f(x)| \geq |f(x)| - |f_n(x) - f(x)| \geq M - \varepsilon
    \end{equation*}
\end{description}
\end{example}

\subsection{Continuity}
\label{develop--math3277:function-sequences:page--convergence/uniform.adoc---continuity}

Let \(\{f_n\}\) be a uniformly convergent sequence which converges to \(f\).
Then if \(\exists N \in \mathbb{N}\) such that \(\forall n > N\) \(f_n\) is continuous,
\(f\) is also continuous. Similarly, if each of the \(f_n\) are uniformly continuous,
\(f\) is also uniformly continuous.

\begin{admonition-remark}[{}]
I tried seeing if something similar happens for absolutly continuous functions,
however, by using a similar proof, I seem that its likely to fail, but I dont know enough
about the to give a counter example.
\end{admonition-remark}

\begin{example}[{Proof}]
We would prove this result for uniformly continuous functions, however, the pointwise
continuous case is identical, but instead you fix \(y\) before choosing \(\varepsilon\).

Let \(\varepsilon > 0\), then since \(\{f_n\}\) is uniformly convergent \(\exists\) \(N\) such
that for all \(n > N\),

\begin{equation*}
|f_n(t) - f(t)| < \frac{\varepsilon}{3}
\end{equation*}

Let \(N\) be the least value such that \(f_n\) is also uniformly continuous. Then,
consider an arbirary such \(f_n\) (suppose \(n = N+1\)), and since \(f_n\) is uniformly
continuous, \(\exists \delta > 0\) such that \(|x-y| < \delta\) implies that

\begin{equation*}
|f_n(x) - f_n(y)| < \frac{\varepsilon}{3}
\end{equation*}

Hence, for all \(|x-y| < \delta\)

\begin{equation*}
\begin{aligned}
|f(x) - f(y)|
&= |(f(x) - f_n(x)) - (f(y) - f_n(y)) + (f_n(x) + f_n(y))|
\\&\leq |f(x) - f_n(x)| + |f(y) - f_n(y)| + |f_n(x) + f_n(y)|
\\&< \varepsilon
\end{aligned}
\end{equation*}

and we are done.
\end{example}

\subsection{Integrability}
\label{develop--math3277:function-sequences:page--convergence/uniform.adoc---integrability}

Let \(\{f_n\}\) be a sequence of functions in \(\mathcal{R}[a,b]\) which
converges uniformly to \(f\).
Then, \(f \in \mathcal{R}[a,b]\) and
then

\begin{equation*}
\int_a^b f(x)\ dx = \int_a^b \lim_{n\to\infty} f_n(x) \ dx = \lim_{n\to\infty} \int_a^b f_n(x) \ dx
\end{equation*}

\begin{admonition-note}[{}]
We are speaking of the reimann integral in this document.
\end{admonition-note}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
What we would do is show that the limit upper reimann integrals
is equal to the reimann integral of the limit (and analogously for the lower reimann integral).
This way, we get a much more general result (idea taken from the wiki article).
\end{admonition-note}

Consider the following difference,

\begin{equation*}
\int_a^{\overline{b}} f(x) \ dx - \lim_{n\to \infty} \int_{a}^{\overline{b}} f_n(x)\ dx
= \lim_{n\to \infty}\left[ \int_a^{\overline{b}} f(x) \ dx - \int_{a}^{\overline{b}} f_n(x)\ dx \right]
= \lim_{n\to \infty}\left[ \int_a^{\overline{b}} (f(x) - f_n(x))\ dx \right]
\end{equation*}

We want to show that it is zero. Consider an aribrary \(P \in \mathcal{P}[a,b]\),
then

\begin{equation*}
\begin{aligned}
|U(P,f - f_n)|
&= \left|\sum_{i=1}^n M_i(P,f-f_n) \Delta x_i\right|
\\&\leq \sum_{i=1}^n |M_i(P,f-f_n)| \Delta x_i
\\&= \sum_{i=1}^n |\sup\{f(x) - f_n(x) \ | \ x \in [x_{i-1}, x_i]\}| \Delta x_i
\\&\leq \sum_{i=1}^n |\sup\{f(x) - f_n(x) \ | \ x \in [a,b]\}| \Delta x_i
\\&= (b-a) |\sup\{f(x) - f_n(x) \ | \ x \in [a,b]\}|
\\&\leq (b-a) |\sup\{|f(x) - f_n(x)| \ | \ x \in [a,b]\}|
\\&= (b-a) \sup\{|f(x) - f_n(x)| \ | \ x \in [a,b]\}
\\&= (b-a) |f - f_n|
\end{aligned}
\end{equation*}

Now, since \(\{f_n\}\) is uniformly convergent, \(|f - f_n| \to 0\) as \(n\to \infty\).
Then, by squeeze theorem, (note the lower bound by \(0\)),

\begin{equation*}
\int_a^{\overline{b}} f(x) \ dx - \lim_{n\to \infty} \int_{a}^{\overline{b}} f_n(x)\ dx
= \lim_{n\to \infty}\left[ \int_a^{\overline{b}} (f(x) - f_n(x))\ dx \right]
= \lim_{n\to \infty} |f - f_n| (b-a)
= 0
\end{equation*}

and hence we get the desired result that

\begin{admonition-note}[{}]
The proof for lower reimann integral is basically the same.
\end{admonition-note}
\end{example}

\subsection{Differentiability}
\label{develop--math3277:function-sequences:page--convergence/uniform.adoc---differentiability}

Let \(\{f_n\}\) be a sequence of differentiable functions defined on interval \(I\) such that

\begin{itemize}
\item \(\{f_n\}\) is convergent at at least one point in \(I\)
\item \(\{f_n'\}\) is uniformly convergent
\end{itemize}

Then, \(\{f_n\}\) is uniformly convergent to some function \(f\) and

\begin{equation*}
\forall x \in I: f'(x) = \lim_{n\to\infty} f_n'(x)
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-important}[{}]
See proof of Theorem 8.2.3 in `Introduction to Real Analysis' by Robert Bartle
and Donald Sherbert.
\end{admonition-important}

\begin{admonition-important}[{}]
The following `result' is, in general, false. Thanks to \href{https://mathoverflow.net/questions/6711/integrability-of-derivatives}{this question}
on the math stackexchange, example 35 of chapter 8 of `Counterexamples in Analysis' by John M.H. Olmsted
provides a (elaborate) counter example.
\end{admonition-important}

\begin{admonition-warning}[{}]
Throughout this proof we use the following `result'.

\begin{custom-quotation}[{}][{}]
If \(F\) is differentiable on \( [a,b]\)
then, \(F' \in \mathcal{R}[a,b]\)
\end{custom-quotation}

I am unsure of the validity of the above statement, but if it holds for each of the \(f_n\),
by \myautoref[{the section above}]{develop--math3277:function-sequences:page--convergence/uniform.adoc---integrability}, the limit of \(f_n'\) is also reimann integrable
which is sufficient for the proof below.
\end{admonition-warning}

Let \(\{f_n'\}\) converge uniformly to \(g\) and \(\{f\}\) converge at the point \(x_0\).
Then, we would show that \(\{f_n\}\) converges
uniformly to

\begin{equation*}
f(x) = f(x_0) + \int_{x_0}^x g(t) \ dt
\end{equation*}

and by the \myautoref[{first fundamental theorem of calculus}]{develop--math3277:riemann-integration:page--index.adoc---first-fundamental-theorem-of-calculus},
we obtain that \(f' = g\).

Then,

\begin{equation*}
\begin{aligned}
\left|f_n(x) - f(x)\right|
&= \left|f_n(x) - f(x_0) - \int_{x_0}^x g(t) \ dt\right|
\\&= \left|f_n(x) - f_n(x_0) + f_n(x_0)- f(x_0) - \int_{x_0}^x g(t) \ dt\right|
\\&\leq |f_n(x_0) - f(x_0)| + \left|f_n(x) - f_n(x_0) - \int_{x_0}^x g(t) \ dt\right|
\\&\leq |f_n(x_0) - f(x_0)| + \left|\int_{x_0}^x f_n'(t) \ dt - \int_{x_0}^x g(t) \ dt\right|
\\&\leq |f_n(x_0) - f(x_0)| + \left|\int_{x_0}^x (f_n'(t) - g(t)) \ dt\right|
\\&\leq |f_n(x_0) - f(x_0)| + \int_{x_0}^x |f_n'(t) - g(t)| \ dt
\\&= |f_n(x_0) - f(x_0)| + \int_{x_0}^{\overline{x}} |f_n'(t) - g(t)| \ dt
\end{aligned}
\end{equation*}

Now, consider arbriary partition \(P \in \mathcal{P}[a,b]\), then

\begin{equation*}
\begin{aligned}
U(P, |f_n' - g|)
&= \sum_{i=1}^n \sup\{|f_n'(x) - g(x)| \ | \ x \in [x_{i-1},x_i]\} \Delta x_i
\\&\leq \sum_{i=1}^n \sup\{|f_n'(x) - g(x)| \ | \ x \in [a,b]\} \Delta x_i
\\&= (b-a) \sup\{|f_n'(x) - g(x)| \ | \ x \in [a,b]\}
\\&= (b-a) |f_n' - g|
\end{aligned}
\end{equation*}

Now, consider arbrary \(\varepsilon > 0\). Then since \(\{f_n(x_0)\}\) converges to
\(f(x_0)\), \(\exists N_1 \in \mathbb{N}\) such that

\begin{equation*}
\forall  n > N_1: |f_n(x_0) - f(x_0)| < \frac{\varepsilon}{2}
\end{equation*}

and since \(\{f_n'\}\) converges uniformly to \(g\), \(\exists N_2 \in \mathbb{N}\) such that

\begin{equation*}
\forall n > N_2: |f_n' - g| < \frac{\varepsilon}{2(b-a)}
\end{equation*}

hence, we get that

\begin{equation*}
\forall n > N = \max(N_1,N_2): \left|f_n(x) - f(x)\right| < \frac{\varepsilon}{2} + (b-a) \frac{\varepsilon}{2(b-a)} = \varepsilon
\end{equation*}

and we are done.
\end{example}
\end{document}
