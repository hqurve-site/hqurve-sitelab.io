\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Point Estimators}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Let \(\theta\) be a population parameter. Then if \(\hat\theta\) is a random
variable, we can use it to \emph{estimate} the value of \(\theta\). In such a case,
we call \(\hat\theta\) an \emph{estimator} for \(\theta\).

An estimator need not be good. For example, if we let the \(\hat\theta\) be
the number of phone calls received by a specified building, we can use it to estimate
the average number of cars currently in another specified city. Clearly, \(\hat\theta\)
is terrible. However, it is still valid. Therefore, we have
various properties which we can use to judge an estimator.

\section{Unbiased}
\label{develop--math3465:point-estimators:page--index.adoc---unbiased}

\(\hat\theta\) is an \emph{unbiased} estimator of \(\theta\) if \(E[\hat\theta - \theta] = 0\).

\section{Consistent}
\label{develop--math3465:point-estimators:page--index.adoc---consistent}

The sequence of estimators \(\{\hat\theta_n\}\) is a \emph{consistent} estimator of \(\theta\) if \(\hat\theta_n\)
\myautoref[{converges in probability}]{develop--math3278:convergence:page--index.adoc---convergence-in-probability} to \(\theta\). That is,

\begin{equation*}
\forall \varepsilon > 0: \lim_{n\to \infty}P(|\hat\theta_n - \theta| > \varepsilon) = 0
\end{equation*}

\subsection{Sufficient conditions for consistency}
\label{develop--math3465:point-estimators:page--index.adoc---sufficient-conditions-for-consistency}

Let \(\{\hat\theta_n\}\) be a sequence of estimators of some parameter \(\theta\). Then,
\(\{\hat\theta_n\}\) is consistent if

\begin{equation*}
\lim_{n\to\infty} Bias(\theta_n) = 0 \quad\text{and}\quad \lim_{n\to \infty} Var(\theta_n) = 0
\end{equation*}

\begin{admonition-tip}[{}]
The first condition is sometimes referred to as \(\hat\theta_n\) being \emph{asymptotically unbiased}.
\end{admonition-tip}

\begin{example}[{Proof}]
Consider arbitrary \(\varepsilon > 0\) and \(\varepsilon' > 0\). Then since \(\lim_{n\to \infty} Bias(\hat\theta_n) =0\)
and \(\lim_{n\to \infty} Var(\hat\theta_n) =0\), there exists \(N\) such that for all \(n > N\)

\begin{equation*}
|Bias(\hat\theta_n)| = |E[\hat\theta] - \theta| < \varepsilon_1
\quad\text{and}\quad
Var(\hat\theta_n) < \varepsilon_2
\end{equation*}

where \(\varepsilon_1 < \varepsilon\) and \(\varepsilon_2\) are yet to be specified. Now, notice

\begin{equation*}
\begin{aligned}
&P\left(\left|\hat\theta_n - \theta\right| \leq \varepsilon\right)
\\&= P\left(\theta - \varepsilon \leq \hat\theta_n \leq \theta + \varepsilon\right)
\\&= P\left(\theta - E[\hat\theta_n] - \varepsilon \leq \hat\theta_n - E[\hat\theta_n] \leq \theta - E[\hat\theta_n] + \varepsilon\right)
\\&\geq P\left(\theta - E[\hat\theta_n] - \varepsilon < \varepsilon_1 - \varepsilon  \leq \hat\theta_n - E[\hat\theta_n] \leq -\varepsilon_1 + \varepsilon < \theta - E[\hat\theta_n] + \varepsilon\right)
\\&= P\left(\varepsilon_1 - \varepsilon  \leq \hat\theta_n - E[\hat\theta_n] \leq -\varepsilon_1 + \varepsilon\right)
\\&= P\left(\left| \hat\theta_n - E[\hat\theta_n] \right| \leq \varepsilon - \varepsilon_1\right)
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
P\left(\left|\hat\theta_n - \theta\right| > \varepsilon\right) \leq P\left(\left|\hat\theta_n - E[\hat\theta_n]\right| > \varepsilon - \varepsilon_1\right)
\end{equation*}

Now, let \(k = \frac{\varepsilon - \varepsilon_1}{\sqrt{Var(\hat\theta_n)}} > 0\). Then by
\myautoref[{Chebyshev's Inequaility}]{develop--math3278:convergence:page--index.adoc---chebyshevs-inequality}, we get that

\begin{equation*}
\begin{aligned}
P\left(\left|\hat\theta_n - \theta\right| > \varepsilon\right)
&\leq P\left(\left|\hat\theta_n - E[\hat\theta_n]\right| > \varepsilon - \varepsilon_1\right)
\\&= P\left(\left|\hat\theta_n - E[\hat\theta_n]\right| > k\sqrt{Var(\hat\theta_n)}\right)
\\&\leq \frac{1}{k^2}
\\&= \frac{Var(\hat\theta_n)}{(\varepsilon - \varepsilon_1)^2}
\\&< \frac{\varepsilon_2}{(\varepsilon - \varepsilon_1)^2}
\end{aligned}
\end{equation*}

Now, lets finally assign \(\varepsilon_1 = \frac{\varepsilon}{2} < \varepsilon\) and
\(\varepsilon_2 = \frac{\varepsilon'}{4\varepsilon^2}\).

\begin{admonition-remark}[{}]
These assignments could have been made when declaring the variables.
\end{admonition-remark}

Then, we get that

\begin{equation*}
P\left(\left|\hat\theta_n - \theta\right| > \varepsilon\right)
< \frac{\varepsilon_2}{(\varepsilon - \varepsilon_1)^2}
= \frac{\varepsilon'/ 4\varepsilon^2}{\varepsilon^2 /4}
= \varepsilon'
\end{equation*}

Therefore, since there exists \(N \in \mathbb{N}\) such that

\begin{equation*}
P\left(\left|\hat\theta_n - \theta\right| > \varepsilon\right)
< \varepsilon'
\end{equation*}

and the choice of \(\varepsilon' > 0\)  was arbitrary,

\begin{equation*}
\lim_{n\to\infty} P\left(\left|\hat\theta_n - \theta\right| > \varepsilon\right) = 0
\end{equation*}
\end{example}

Additionally, it is important to note that these conditions are by no means necessary.
The main problem (which I can see) is distributions with sufficiently ``heavy'' tails.

\begin{description}
\item[Consistent estimator which neither tends to be unbiased nor have zero variance] Let \(\theta =0\) and \(\hat\theta_n\) have the following discrete distribution
    
    \begin{table}[H]\centering
    \begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {\(x\)} & {\(0\)} & {\(n\)} \\
    \hline
    {\(P(\hat\theta_n = x)\)} & {\(1- \frac{1}{n}\)} & {\(\frac{1}{n}\)} \\
    \hline[\thicktableline]
    \end{tblr}
    \end{table}
    
    Then \(P(\hat\theta_n > \alpha) \leq \frac{1}{n}\) which implies that its consistent. However, notice that
    \(E(\hat\theta_n) = 1\) and \(Var(\hat\theta_n) = n - 1\).
\item[Consistent estimator which tends to be unbiased but does not tend to have zero variance] Let \(\theta =0\) and \(\hat\theta_n\) have the following discrete distribution
    
    \begin{table}[H]\centering
    \begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
    \hline[\thicktableline]
    {\(x\)} & {\(0\)} & {\(n\)} \\
    \hline
    {\(P(\hat\theta_n = x)\)} & {\(1- \frac{1}{n^2}\)} & {\(\frac{1}{n^2}\)} \\
    \hline[\thicktableline]
    \end{tblr}
    \end{table}
    
    Then \(P(\hat\theta_n > \alpha) \leq \frac{1}{n^2}\) which implies that its consistent. Additionally,
    \(E(\hat\theta_n) = \frac{1}{n}\) however \(Var(\hat\theta_n) = 1 - \frac{1}{n}\).
\item[Consistent estimator which tends to be biased but tends to have zero variance] Currently unknown (by me). My guess is that it is impossible but I am yet to formulate a proof.
    That is, a consistent estimator which tends to have zero variance is asymptotically unbiased.
\end{description}
\end{document}
