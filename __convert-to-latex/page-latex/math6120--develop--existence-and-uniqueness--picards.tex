\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Picard's Successive Approximations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Let \(D \subseteq \mathbb{R}^2\) be a domain and \(f: D \to \mathbb{R}\) be continuous.
Consider the integral equation

\begin{equation*}
x(t) = x_0 + \int_{t_0}^t  f(s, x(s)) \ ds
\end{equation*}

Then, notice that \(\phi_0(t) = x_0\) is a good approximation of a solution especially at \(t=t_0\).
Also notice that \(\phi_1(t) = x_0 + \int_{t_0}^t f(s, x_0) \ ds\) is likely also a good approximation of a solution.
Picard's successive approximations builds a sequence of \(\phi_n\) such that

\begin{equation*}
\phi_0(t) = x_0 \quad\text{ and }
\phi_{n+1}(t) = x_0 + \int_{t_0}^t f(s, \phi_n(s)) \ ds
\end{equation*}

It happens to work out that \(\{\phi_n\}\) builds better approximations for the solution and converges under certain conditions.
Also note that if at any time \(\phi_n\) is a solution, the sequence is constant for all \(n' > n\).

\section{Picard's Theorem: Convergence}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---picards-theorem-convergence}

Let \(f\) be a real valued function defined on a rectangle

\begin{equation*}
R = \{(t,x) \ | \ |t-t_0| \leq a,\ |x-x_0| \leq b\}
\end{equation*}

such that \(f\) is bounded by \(M\) on \(R\).
Then for each of Picard's successive approximations, \(\phi_n\)
defined on \(I = |t-t_0| \leq h = \min\left(a, \frac{b}{M}\right)\),

\begin{itemize}
\item \((t,\phi_n(t)) \in D\)
\item \(|\phi_n(t) - x_0| \leq M|t-t_0|\)
\item \(\phi_n\) is Lipschitz continuous
\end{itemize}

\begin{proof}[{}]
We prove both facts inductively on \(n\). Then the base case \(n=0\) holds trivially since \(\phi_0(t) = x_0\).

Now, suppose that both facts hold for some \(n \geq 0\). Then,
for each \(t \in I\),

\begin{equation*}
\begin{aligned}
\left|\phi_{n+1}(t) -x_0\right|
&= \left|\int_{t_0}^t f(s, \phi_n(s)) \ ds \right|
\\&\leq \int_{t_0}^t \left| f(s, \phi_n(s)) \right| \ ds \quad\text{Note if } t< t_0 \text{ we simply swap the limits}
\\&\leq \int_{t_0}^t M \ ds \quad\text{since } f \text{ is bounded}
\\&\leq M|t-t_0| \quad\text{Proving fact 2}
\\&\leq Mh
\\&\leq b \quad\text{Proving fact 1}
\end{aligned}
\end{equation*}

Also, notice that for any \(t_1, t_2 \in I\)

\begin{equation*}
|\phi_{n+1}(t_1) - \phi_{n+1}(t_2)| = \int_{t_1}^{t_2} |f(s, \phi_n(s))| \ ds \leq M|t_1 - t_2|
\end{equation*}

Therefore \(\phi_{n+1}\) is Lipschitz continuous.
\end{proof}

\begin{theorem}[{Picard-Lindeloff Theorem}]
Let \(D\) be a domain and \(f\) be Lipschitz in \(D\). Then, if \((t_0, x_0) \in D\)
is an interior point of

\begin{equation*}
R = \{(t,x) \ | \ |t-t_0| \leq a,\ |x-x_0| \leq b\} \subseteq D
\end{equation*}

Then, there exists a solution of the integral equation

\begin{equation*}
x(t) = x_0 + \int_{t_0}^t f(s,x(s)) \ ds
\end{equation*}

on the interval \(I = |t-t_0|\leq h\) where \(h = \min\left(a, \frac{b}{M}\right)\) where \(M = \max_{(t,x) \in R}|f|\).

\begin{admonition-note}[{}]
From the previous results we already know that the Picard successive approximations exist and are continuous on \(I\).
We just need to show that they converge. Furthermore, if they converge, we know that the solution is unique.
\end{admonition-note}

\begin{lemma}[{Part A}]
From Example 1, we expect that the difference between successive terms tends to 0.
Also from the example, an obvious first guess is that

\begin{equation*}
\forall n \geq 1: \forall t \in I:
|\phi_{n}(t) - \phi_{n-1}(t)| \leq A\frac{|t-t_0|^{n}K^n}{n!}
\end{equation*}

where \(A > 0\) is a constant.
It happens that this works where \(A= \frac{M}{K}\).

\begin{proof}[{}]
We prove this claim inductively on \(n\).
For the base case,

\begin{equation*}
|\phi_1(t) - \phi_0(t)| = \left|\int_{t_0}^t f(s, x_0)\ ds\right| \leq M|t - t_0|
\end{equation*}

Therefore \(A = \frac{M}{K} > 0\). Now, suppose that the statement is true for some \(n \geq 1\).
Then

\begin{equation*}
\begin{aligned}
\forall t \in I: \left|\phi_{n+1}(t) - \phi_n(t)\right|
&\leq \left|\int_{t_0}^t f(s, \phi_n(s)) - f(s, \phi_{n-1}(s) \ ds \right|
\\&\leq \int_{t_0}^t \left|f(s, \phi_n(s)) - f(s, \phi_{n-1}(s) \right| \ ds
\\&\leq \int_{t_0}^t K\left|\phi_n(s)- \phi_{n-1}(s) \right| \ ds \quad\text{since } f\text{ is Lipschitz}
\\&\leq \int_{t_0}^t AK \frac{|s-t_0|^nK^n}{n!} \ ds \quad\text{by hypothesis}
\\&= A \frac{|t-t_0|^{n+1}K^{n+1}}{(n+1)!} \ ds
\end{aligned}
\end{equation*}
\end{proof}
\end{lemma}

\begin{lemma}[{Part B}]
The \(\phi_n\) converges uniformly on \(I\)

\begin{proof}[{}]
We now aim to use the Weierstrass M-test to prove convergence.
Notice that

\begin{equation*}
\phi_n(t) = x_0 + \sum_{i=1}^n (\phi_i(t) - \phi_{i-1}(t))
\end{equation*}

and for each \(i\),

\begin{equation*}
\forall t \in I: |\phi_i(t) - \phi_{i-1}(t)| \leq A\frac{|t-t_0|^{i}K^{i}}{i!} \leq A\frac{(Kh)^i}{i!}
\end{equation*}

Since \(\sum_{i=1}^\infty \frac{(Kh)^i}{i!}\) converges, by the Weierstrass M-test,
the \(\phi_n\) converges uniformly to some \(\phi\) on \(I\).
\end{proof}
\end{lemma}

\begin{lemma}[{Part C}]
We need to verify the limit function is a solution

\begin{proof}[{}]
First notice that \((t,\phi(t)) \in R\) since

\begin{equation*}
\forall n \geq 0: \forall t \in I: |\phi_n(t) - x_0| \leq M|t-t_0| \leq Mh \leq b
\end{equation*}

Next, we need to see that \(\phi\) satisfies the integral equation.
and hence
First notice that \(f(s, \phi_n(s))\) uniformly converges to \(f(s,\phi(s))\)
since

\begin{equation*}
|f(s, \phi_n(s)) - f(s, \phi(s))| \leq K|\phi_n(s) - \phi(s)| \xrightarrow{uniformly} 0
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
\phi(t)
&= \lim_{n\to \infty} \phi_{n+1}(t)
\\&= x_0 + \lim_{n\to\infty} \int_{t_0}^t f(s, \phi_n(s)) \ ds
\\&= x_0 + \int_{t_0}^t \lim_{n\to\infty} f(s, \phi_n(s)) \ ds \quad\text{by uniform convergence of } f(s,\phi_n(s))
\\&= x_0 + \int_{t_0}^t f(s, \phi(s)) \ ds
\end{aligned}
\end{equation*}

Therefore \(\phi(t)\) is a solution to the integral equation.
\end{proof}
\end{lemma}
\end{theorem}

\subsection{Bounds of solution}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---bounds-of-solution}

\begin{corollary}[{Bounds of solution}]
Picard's successive approximations have a maximum error bounded as stated below

\begin{equation*}
\forall n \geq 0: \forall t \in I: |\phi(t) - \phi_n(t)| \leq \frac{M}{K}\frac{(Kh)^{n+1}}{(n+1)!}e^{Kh}
\end{equation*}

\begin{proof}[{}]
Let \(t \in I\) and note that

\begin{equation*}
\phi(t) = \phi_0(t) + \sum_{i=1}^\infty (\phi_{i}(t) - \phi_{i-1}(t)) = \phi_n(t) + \sum_{i=n+1}^\infty (\phi_{i}(t) - \phi_{i-1}(t))
\end{equation*}

Therefore

\begin{equation*}
\begin{aligned}
\left|\phi(t) - \phi\right|
&\leq \sum_{i=n+1}^\infty |\phi_i(t) - \phi_{i-1}(t)|
\\&\leq \frac{M}{K}\sum_{i=n+1}^\infty \frac{|t-t_0|^{i}K^i}{i!} \quad\text{From part A}
\\&\leq \frac{M}{K}\sum_{i=n+1}^\infty \frac{(Kh)^i}{i!}
\\&\leq \frac{M}{K}\sum_{i=n+1}^\infty \frac{(Kh)^{n+1} (Kh)^{i-(n+1)}}{i!} \binom{i!}{(n+1)!}
\\&= \frac{M}{K}\sum_{i=n+1}^\infty \frac{(Kh)^{n+1} (Kh)^{i-(n+1)}}{(n+1)!(i-(n+1))!}
\\&= \frac{M}{K}\frac{(Kh)^{n+1}}{(n+1)!}\sum_{i=n+1}^\infty \frac{(Kh)^{i-(n+1)}}{(i-(n+1))!}
\\&= \frac{M}{K}\frac{(Kh)^{n+1}}{(n+1)!}\sum_{i=0}^\infty \frac{(Kh)^{i}}{i!}
\\&= \frac{M}{K}\frac{(Kh)^{n+1}}{(n+1)!} e^{Kh}
\end{aligned}
\end{equation*}
\end{proof}

\begin{admonition-note}[{}]
We could have alternatively used this result to prove uniform convergence.
\end{admonition-note}
\end{corollary}

\subsection{Examples}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---examples}

\subsubsection{Example 1}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---example-1}

Consider \(x' = -x\) where \(x(0) = 1\) for \(t \geq 0\).
We expect the solution to be \(x(t) = e^{-t}\).
Then this may be translated to the integral equation

\begin{equation*}
x(t) = 1 - \int_0^t x(s) \ ds
\end{equation*}

By applying the approximations, we see that \(\phi_0(t) = 1\) and

\begin{equation*}
\phi_n(t) = \sum_{k=1}^n \frac{(-t)^k}{k!}
\end{equation*}

Therefore \(\phi_n\) converges to the unique solution.

\section{Non-local existence}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---non-local-existence}

Note that Picard's existence theorem only guarantees existence in a small region.
For this reason, we say that Picard's existence theorem guarantees \emph{local existence}.
However, how do we guarantee existence on the entire domain \(D\).

\begin{theorem}[{Non-local Existence theorem}]
Let \(f\) be a real Lipschitz continuous function with constant \(K\) on strip \(S\)

\begin{equation*}
S = \{(t,x) \ | \ |t - t_0| \leq a,\ |x|< \infty \}
\end{equation*}

for some \(a > 0\).
Then, the successive approximations exist as continuous functions on \(S\) and converge
uniformly to a unique solution of the integral equation

\begin{equation*}
x(t) = x_0 + \int_{t_0}^t f(s,x(s)) \ ds
\end{equation*}

\begin{admonition-important}[{}]
We cannot directly apply Picard's theorem since we do not know whether
\(f\) is bounded on \(S\) since the \(x\) variable is unbounded.
If we know that \(f\) is bounded, then we can just choose \(b = Ma\) to ensure
that \(h=a\) and hence the solution exists on \(S\).
\end{admonition-important}

\begin{proof}[{}]
We prove this result in the same way as Picard's theorem.

Note that we know that \((t,\phi_n) \in S\) since \(S\) is unbounded in \(x\).
We also know that the \(\phi_n\) are Lipschitz continuous inductively since

\begin{equation*}
|\phi_{n+1}(t_1) - \phi_{n+1}(t_2)|
\leq
\int_{t_1}^{t_2} |f(s, \phi_n(s))| \ ds
\leq \left(\max_{|s-t_0| \leq a} |f(s, \phi(s))|\right)|t_1-t_2|
\end{equation*}

From this we can easily obtain parts A, B and C directly since
they only depend on the existence and continuity of the \(\phi_n\).
Therefore, there is a solution to the integral equation.

This solution is also unique by considering arbitrarily larger
finite regions of \(S\).

\begin{admonition-remark}[{}]
I am not sure why, but sir proved that the successive approximations
are bounded.
\end{admonition-remark}
\end{proof}
\end{theorem}

\begin{corollary}[{Global Existence solution}]
Let \(f(t,x)\) be a continuous function on the entire plane
satisfying the Lipschitz condition on every strip \(S_a\)

\begin{equation*}
S_a = \{(t,x) \ | \ |t| \leq a, \ |x| < \infty\}
\end{equation*}

where \(a > 0\).
Then, the integral equation has a unique solution on the entire plane

\begin{equation*}
x(t) = x_0 + \int_{t_0}^t f(s,x(s)) \ ds
\end{equation*}

\begin{proof}[{}]
Let us define \(\phi(t)\) at each \(t\) as follows.

Consider arbitrary \(r \in \mathbb{R}\) and let \(a_r = \min(|r|, |t_0|)+1\). Then, by the
Non-local existence theorem, there exists a unique \(\phi_r(t)\) which satisfies the integral equation
on \(S_{a_r}\).
We now have a collection of \((r, \phi_r(t))\) and we can define the function \(\phi(t)\)
which has points \((r,\phi_r(r))\).
We claim that \(\phi(t)\) satisfies the integral equation

First note that if \(|r_1| < |r_2|\), then, \(\phi_{r_1}(t)\) and \(\phi_{r_2}(t)\)
both satisfy the integral equation on \(S_{a_r}\) and hence must coincide by uniqueness.
Therefore \(\phi\) satisfies the integral equation since

\begin{equation*}
\begin{aligned}
\forall t \in \mathbb{R}:
x_0 + \int_{t_0}^t f(s,\phi(s)) \ ds
&= x_0 + \int_{t_0}^t f(s,\phi_s(s)) \ ds
\\&= x_0 + \int_{t_0}^t f(s,\phi_r(s)) \ ds
\\&= \phi_r(s) = \phi_t(t) = \phi(t)
\end{aligned}
\end{equation*}

by taking some \(r \in \mathbb{R}\) with \(|r| > |s|\) for each \(s\) between \(t_0\) and \(t\).

We now need to show that the solution is unique. Suppose that \(\phi\) and \(\psi\) are solutions
to the integral equation and consider arbitrary \(t \in \mathbb{R}\).
Let \(a\) be such that \(S_a\) contains \(t\).
Then, since both satisfy the integral equation on \(S_a\), they must coincide on \(S_a\)
and hence coincide at \(t\).
Since the choice of \(t\) was arbitrary, \(\phi = \psi\) on the entire plane.
\end{proof}
\end{corollary}

\subsection{Examples}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---examples-2}

\subsubsection{Example 1}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---example-1-2}

Consider the ODE

\begin{equation*}
x' = \frac{x^3e^t}{1+x^2} + t^2\cos(x)
\end{equation*}

then, \(f(t,x)\) is equal to the left hand side.
Consider \(a > 0\) and \(S_a\). For a unique solution to exist,
we need to ensure that \(f\) is Lipschitz on \(S_a\).
Note that

\begin{equation*}
\begin{aligned}
\left|\frac{\partial f}{\partial x}\right|
&\leq \left| \frac{2x^4e^t}{(1+x^2)^2} - \frac{3x^2e^t}{1+x^2} - t^2\sin(x)\right|
\\&\leq \frac{2x^4e^t}{(1+x^2)^2} + \frac{3x^2e^t}{1+x^2} + t^2|\sin(x)|
\\&\leq 2e^t + 3e^t + t^2
\\&\leq 5e^a + a^2
\end{aligned}
\end{equation*}

Therefore \(f\) must be Lipschitz in \(S_a\).

Therefore, by the above corollary, there exists a unique solution on the entire plane.

\subsubsection{Example 2}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---example-2}

Consider the ODE

\begin{equation*}
x'=x^2
\end{equation*}

Then, unlike example 1, \(f(t,x)=x^2\) does not satisfy the Lipschitz conditions on any slice.

\section{Peano's Theorem}
\label{develop--math6120:existence-and-uniqueness:page--picards.adoc---peanos-theorem}

\begin{theorem}[{}]
Let \(f\) be a continuous, real-valued function which is bounded by some \(M>0\)
on \(D\). Let \((t_0, x_0)\) be a fixed point in \(D\) and consider
a rectangle of the form

\begin{equation*}
R = \{(t,x) \ | \ |t-t_0| \leq a,\ |x-x_0| \leq b\} \subseteq D
\end{equation*}

where \(a,b > 0\). Then, the integral equation

\begin{equation*}
x(t) = x_0 + \int_{t_0}^{t} f(s,x(s))\ ds
\end{equation*}

has at least one solution on an interval \(I = |t-t_0|\leq a\) if \(Ma \leq b\).
\end{theorem}

\begin{proof}[{Naive attempt}]
Our aim is to use the \myautoref[{Arzela-Ascoli Theorem}]{develop--math6120:appendix:page--function-sequences.adoc---arzela-ascoli-theorem}
with the Picard's successive approximations.

Note that Picard's successive approximations exist and are continuous on \(R\) since \(f\) is continuous.
However, we need to show that they are uniformly bounded and equicontinuous on \(R\) as well.

To prove uniformly bounded, note that

\begin{equation*}
\forall t \in I: |\phi_n(t)|
\leq |x_0| + \left|\int_{t_0}^t |f(s, \phi_{n-1}(s))| \ ds\right|
\leq |x_0| + M|t-t_0|
\leq |x_0| + Ma
\end{equation*}

To prove equicontinuous consider arbitrary \(t_1, t_2 \in I\) with \(t_2 < t_1\). Then \(\phi_n\) is Lipschitz
with constant \(K=M\).

\begin{equation*}
|\phi_n(t_1) - \phi_n(t_2)| \leq \int_{t_2}^{t_1} |f(s, \phi_{n-1}(s))| \ ds \leq M|t_2 - t_1|
\end{equation*}

\begin{admonition-note}[{}]
The we did not prove uniformly bounded and equicontinuous for \(n=0\). But, in this case it is trivial.
\end{admonition-note}

Therefore, \(\{\phi_n\}\) is a sequence of uniformly bounded and equicontinuous functions.
Hence, by the Arzela-Ascoli theorem, there exists a convergent subsequence \(\{\phi_{n_k}(t)\}\).
Therefore we have that

\begin{equation*}
\phi(t) = \lim_{k \to \infty} \phi_{n_k}(t) = \lim_{k\to\infty} \int_{t_0}^t f(s, \phi_{n_k -1}(s)) \ ds
\end{equation*}

Note that the index within the integral is \(n_k - 1\) instead of \(n_{k-1}\).
Therefore, we cannot progress any further.

\begin{admonition-remark}[{}]
A good way to think of this is that ``If it was possible to get a solution without the Lipschitz condition, Picard's theorem would have done so''.
\end{admonition-remark}
\end{proof}

\begin{proof}[{Actual proof}]
This proof comes from the notes. We define a sequence of \(\phi_n\) on \(I\) as follows

\begin{equation*}
\phi_n(t) = \begin{cases}
x_0,&\quad\text{if } |t-t_0| \leq \frac{a}{n}
\\
x_0 + \int_{t_0}^{t-\frac{a}{n}}f(s, \phi_n(s))\ ds, &\quad\text{if } t-t_0 \geq \frac{a}{n}
\\
x_0 - \int_{t+\frac{a}{n}}^{t_0}f(s, \phi_n(s))\ ds, &\quad\text{if } t-t_0 \leq -\frac{a}{n}
\end{cases}
\end{equation*}

Note that this function is not recursive in \(n\) but rather builds itself up starting from the interval
\(\left[t_0 - \frac{a}{n}, \ t_0 + \frac{a}{n}\right]\).
We now wish to prove that the sequence of \(\phi_n\) is uniformly bounded and equicontinuous.

For uniformly bounded, consider arbitrary \(t \in I\). Then

\begin{equation*}
|\phi_n(t)- x_0| \leq M|t-t_0| \leq Ma \leq b
\quad\text{since}\quad
|\phi_n(t)| \leq
\begin{cases}
0,&\quad\text{if } |t-t_0| \leq \frac{a}{n}
\\
\int_{t_0}^{t-\frac{a}{n}} M\ ds, &\quad\text{if } t-t_0 \geq \frac{a}{n}
\\
\int_{t+\frac{a}{n}}^{t_0} M\ ds, &\quad\text{if } t-t_0 \leq -\frac{a}{n}
\end{cases}
\end{equation*}

For equicontinuous, it is pretty simple to see that \(|\phi_n(t_1) - \phi_n(t_2)| \leq M|t_1-t_2|\)
by considering the nine cases.

Now, we have that the \(\{\phi_n\}\) are equicontinuous and uniformly bounded.
Hence, by the Arzela-Ascoli theorem, there exists a convergent subsequence \(\{\phi_{n_k}(t)\}\).
To prove that \(\phi(t)\) satisfies the integral equation, we take cases on the sign
of \(t-t_0\).

\begin{itemize}
\item If \(t-t_0 =0\), then \(\phi(t) = x_0\) as desired since for each \(n\), \(\phi_n(t_0) =x_0\).
\item If \(t-t_0 > 0\), consider large enough \(k\) such that \(\frac{a}{n_k} < |t-t_0|\). Then,
    
    \begin{equation*}
    \phi(t)
    = \lim_{k\to \infty} \phi_{n_k}(t)
    = x_0+\lim_{k\to \infty} \int_{t_0}^{t-\frac{a}{n_k}} f(s, \phi_{n_k}(s)) \ ds
    = x_0 + \int_{t_0}^{t} f(s, \phi(s)) \ ds
    \end{equation*}
    
    To prove the last equality, we split the integral as the difference between \(\int_{t_0}^t\)
    and \(\int_{t - \frac{a}{n_k}}^t\).
    
    \begin{itemize}
    \item For the second integral, we know that this tends to zero since the integrand is bounded.
    \item For the first integral, we may swap the limit and integral
        since \(f(s,\phi_{n_k}(s))\) is continuous and converges uniformly.
        
        To see this, first notice that each \(\phi_{n_k}(t)-x_0\) is bounded by \(Ma\) and hence \(\phi(t) -x_0\) is also bounded.
        So, consider the rectangle \(|t-t_0| \leq a\) and \(|x-x_0| \leq Ma\).
        Since \(f\) is bounded on this compact region, it must be uniformly continuous.
        So, consider arbitrary \(\varepsilon > 0\). Then, there exists \(\delta > 0\)
        such that \(|f(t_1, x_1) - f(t_2,x_2)| < \varepsilon\) whenever \(|t_1-t_2| < \delta\) and \(|x_1-x_2| < \delta\).
        Since \(\phi_{n_k} \to \phi\) uniformly, there exists a \(N > 0\) such that for each \(k \geq N\),
        \(|\phi_{n_k}(s) - \phi(s)| < \delta\) for all \(s \in I\).
        Therefore, there exists a \(N > 0\) such that
        
        \begin{equation*}
        \forall k \geq N: \forall s \in I: |f(s,\phi(s)) - f(s,\phi_{n_k}(s))| < \varepsilon
        \end{equation*}
    \end{itemize}
\item The case of \(t-t_0 < 0\) is similar to the case when \(t-t_0 > 0\).
\end{itemize}

Therefore, \(\phi\) is a solution of the integral equation.
\end{proof}

\begin{lemma}[{Uniqueness of monotonic solutions}]
\begin{admonition-warning}[{}]
This claim may be wrong. See below for counter example
\end{admonition-warning}

Let \(f\) be a continuous real-valued function on \(D\)
and \((t_0, x_0)\) is in the interior of \(D\).
Then if \(f\) is monotonic in \(x\), the integral equation

\begin{equation*}
x(t) = x_0 + \int_{t_0}^t f(s, x(s)) \ ds
\end{equation*}

has at most one solution on an interval containing \(t_0\).
\end{lemma}

Consider \(f(t,x) = \frac{2}{3}x^{\frac13}\). Then \(f\) is monotonic and the
IVP

\begin{equation*}
x'(t) =  \frac{2}{3}x^{\frac13}(t)
,\quad x(0) = 0
\end{equation*}

has two solutions \(\phi(t) = 0\) and

\begin{equation*}
\phi(t) = \begin{cases}
0, &\quad\text{if } t \leq 0\\
t^{\frac32}, &\quad\text{if } t > 0
\end{cases}
\end{equation*}
\end{document}
