\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Exercises}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\section{Planetary motion: An alternative magnitude of the gravitational force}
\label{develop--math6192:ROOT:page--exercises.adoc---planetary-motion-an-alternative-magnitude-of-the-gravitational-force}

Suppose instead, that the magnitude of the gravitational force was given by

\begin{equation*}
|\vec{F}| = \frac{GMm}{r^3}
\end{equation*}

Note

\begin{itemize}
\item The acceleration now changes to \(\frac{d^2 \vec{r}}{dt^2} = -\frac{k}{r^4}\vec{r}\)
\item The motion is still planar
\item Kepler's second law still holds (with the same constant \(h\))
\item \(\theta\) is still injective
\end{itemize}

The change is in the equation

\begin{equation*}
\frac{d^2r}{dt^2} -r\left(\frac{d\theta}{dt}\right)^2 = -\frac{k}{r^3}
\end{equation*}

So, we instead obtain that

\begin{equation*}
\begin{aligned}
&\frac{d^2r}{dt^2} - r\left(\frac{d\theta}{dt}\right)^2 = -\frac{k}{r^3}
\\&\implies
\frac{d^2}{d\theta^2}\left(\frac{1}{r}\right)\frac{-h^2}{r^2} - \frac{h^2}{r^3} = -\frac{k}{r^3}
\\&\implies
\frac{d^2}{d\theta^2}\left(\frac{1}{r}\right)+ \frac{1}{r} = \frac{k}{rh^2}
\\&\implies
\frac{d^2}{d\theta^2}\left(\frac{1}{r}\right) + \left(1- \frac{k}{h^2}\right)\frac{1}{r} = 0
\end{aligned}
\end{equation*}

Let \(\lambda = \sqrt{\left|1- \frac{k}{h^2}\right|}\).
Depending on the sign of the coefficient of \(\frac{1}{r}\), we get different results.

\begin{itemize}
\item If the coefficient is positive, \(h^2 > k\), we have that
    
    \begin{equation*}
    \frac{1}{r} = C\cos(\lambda(\theta - \alpha))
    \implies  r = \frac{1}{C\cos(\lambda(\theta - \alpha))}
    \end{equation*}
    
    and \(r\to \infty\)
\item If the coefficient is negative, \(h^2 < k\), we have that
    
    \begin{equation*}
    \frac{1}{r} = C\cosh(\lambda(\theta-\alpha))
    \implies r = \frac{1}{C\cosh(\lambda(\theta-\alpha))}
    \end{equation*}
    
    and \(r\to 0\)
\item If the coefficient is zero, \(h^2=k\), we have that
    
    \begin{equation*}
    \frac{1}{r} = C(\theta-\alpha)
    \implies r = \frac{1}{C(\theta-\alpha)}
    \end{equation*}
    
    and \(r\to 0\) or \(\infty\) depending on the evolution of \(\theta\).
\end{itemize}

where \(C,\alpha\) are some constants.
In all three cases, there are no stable orbits.

\section{Conservation of energy: Oscillating mass}
\label{develop--math6192:ROOT:page--exercises.adoc---conservation-of-energy-oscillating-mass}

The period of oscillation is given by

\begin{equation*}
T = 2\int_{x_{\min}}^{x_{\max}} \frac{dx}{\sqrt{\frac{2}{M}(E-U(x))}}
\end{equation*}

where \(x_{\min}\) and \(x_{\max}\) are determined using the set of
\(x\) values for which \(E > U(x)\). If such an interval
does not exist (eg unbounded), we do not have periodic behaviour

\subsection{Potential 1}
\label{develop--math6192:ROOT:page--exercises.adoc---potential-1}

In this example, we try
\(U(x) = -\frac{V_0}{\cosh^2(\alpha x)}\).
So, we want to find the set of values of \(x\) such that

\begin{equation*}
E > U(x) = -\frac{V_0}{\cosh^2(\alpha x)}
\end{equation*}

We assume that \(V_0 > 0\).

\begin{itemize}
\item If \(E \geq 0\), the set of \(x\) values is unbounded since the right hand side is always positive.
\item If \(E < 0\), we want to find
    
    \begin{equation*}
    \cosh^2(\alpha x) < \frac{-V_0}{E} \iff \cosh(\alpha x) < \sqrt{\frac{-V_0}{E}}
    \end{equation*}
    
    There is a bounded interval of \(x\) values.
    
    We will focus on the latter case.
    Let \(x_0 > 0\) be such that \(\cosh(\alpha x_0) = \sqrt{\frac{-V_0}{E}}\)
    and so we want
\end{itemize}

\begin{equation*}
T
= 2\int_{-x_0}^{x_0} \frac{dx}{\sqrt{\frac{2}{M}\left(E+\frac{V_0}{\cosh^2(\alpha x)}\right)}}
= 4\int_0^{x_0} \frac{dx}{\sqrt{\frac{2}{M}\left(E+\frac{V_0}{\cosh^2(\alpha x)}\right)}}
= 4\sqrt{\frac{M}{2}}\int_{0}^{x_0} \frac{dx}{\sqrt{1 + \frac{E}{V_0}\cosh^2(\alpha x)}}
\end{equation*}

\subsection{Potential 2}
\label{develop--math6192:ROOT:page--exercises.adoc---potential-2}

\begin{admonition-todo}[{}]
\{\}
\end{admonition-todo}

In this example, we try
\(U(x) = U_0\tan^2(\alpha x)\)

\section{Harmonic oscillators}
\label{develop--math6192:ROOT:page--exercises.adoc---harmonic-oscillators}

Use complex variables to find the amplitude and phase of the forced
oscillations for an oscillator that is subject to friction
and external periodic forcing, as described in the equation

\begin{equation*}
\ddot{x} + 2\gamma\dot{x} + \omega_0^2 x = \frac{f_0}{m}\exp(\beta t) \cos(\lambda t)
\end{equation*}

Note that we already know that the complementary solution is given by

\begin{equation*}
x_c = Ae^{\lambda_1 t} + Be^{\lambda_2 t}
\end{equation*}

where \(\lambda_{1,2} = -\gamma \pm \sqrt{\gamma^2 -\omega_0^2}\)
and we have that \(x_c \to 0\) (since \(\gamma > 0\)).

For the particular solution, we first convert the differential equation
to complex form

\begin{equation*}
\ddot{x} + 2\gamma\dot{x} + \omega_0^2 x = \frac{f_0}{m}\exp(\beta t) \exp(i\lambda t)
\end{equation*}

and we will take the real part of the particular solution at the end.
Note that this is valid since this is a linear ODE with real coefficients.
We will use undetermined coefficients where the solution is of the form
\(C\exp[(\beta + i\lambda)t]\).
Upon substitution

\begin{equation*}
\begin{aligned}
&
C\exp[(\beta + i\lambda)t]
\left[(\beta + i\lambda)^2 + 2\gamma (\beta + i\lambda) + \omega_0^2\right]
= \frac{f_0}{m}\exp(\beta t) \exp(i\lambda t)
\\&
\begin{aligned}[t]
\implies
C
&= \frac{f_0/m}{(\beta + i\lambda)^2 + 2\gamma (\beta + i\lambda) + \omega_0^2}
\\&= \frac{f_0/m}{(\beta^2 - \lambda^2 + 2\gamma\beta + \omega_0^2) + 2i\lambda(\beta + \gamma)}
\end{aligned}
\end{aligned}
\end{equation*}

So, the amplitude is given by

\begin{equation*}
a = \frac{f/m}{\sqrt{(\beta^2 - \lambda^2 + 2\gamma\beta + \omega_0^2) + 4\lambda^2(\beta + \gamma)^2}}
\end{equation*}

and the phase is given by

\begin{equation*}
\begin{aligned}
\alpha
&= \arg \frac{f_0/m}{(\beta^2 - \lambda^2 + 2\gamma\beta + \omega_0^2) + 2i\lambda(\beta + \gamma)}
\\&= -\arg \left((\beta^2 - \lambda^2 + 2\gamma\beta + \omega_0^2) + 2i\lambda(\beta + \gamma)\right)
\\&= -\tan^{-1} \frac{ 2\lambda(\beta + \gamma)}{\beta^2 - \lambda^2 + 2\gamma\beta + \omega_0^2}
\end{aligned}
\end{equation*}

\section{Calculus of Variations - Lagrange Multipliers}
\label{develop--math6192:ROOT:page--exercises.adoc---calculus-of-variations-lagrange-multipliers}

Find the shape of a chain that is hanging at equilibrium between two fixed
points, where the length of the chain is given

\begin{admonition-tip}[{}]
The potential energy of the chain is minimal in this state
\end{admonition-tip}

We assume that the chain is at rest. So, its velocity is zero at each point.
Let the two points of the chain be \((a,0)\) and \((b,0)\).
So, since the chain is hanging downward (due to gravity), \(y\) is negative.
Note that the potential contributed by each point is given by

\begin{equation*}
\delta U = (\delta m)gy
\end{equation*}

The change in mass is proportional to the change in length.
So, if \(m\) is the total mass of the chain,

\begin{equation*}
\delta m = \frac{m}{L}\sqrt{1+(y')^2} \ \delta x
\end{equation*}

So that if we integrate over the chain, we get that the mass is \(m\).
Therefore

\begin{equation*}
U = \int_a^b \delta U \ dx = \frac{mg}{L} \int_a^b y \sqrt{1+(y')^2} \ dx
\end{equation*}

and the total length is given by

\begin{equation*}
L = \int_a^b \sqrt{1+(y')^2} \ dx
\end{equation*}

So, to minimize the potential energy, by the theory of Lagrange multipliers,
we minimize

\begin{equation*}
I(y)
= \int_a^b \frac{mg}{L} y \sqrt{1+(y')^2} - \lambda \sqrt{1+(y')^2} \ dx
= \int_a^b (Ay-\lambda) \sqrt{1+(y')^2} \ dx
\end{equation*}

where \(A = \frac{mg}{L}\). Note that the integrand is

\begin{equation*}
F(y,y') = (Ay-\lambda) \sqrt{1+(y')^2}
\end{equation*}

Since this is a function of only \(y\) and \(y'\), we can use the following Euler-Lagrange equation

\begin{equation*}
B = F - y'F_{y'}
= (Ay-\lambda) \sqrt{1+(y')^2} - (Ay-\lambda)\frac{(y')^2}{\sqrt{1+(y')^2}}
= (Ay-\lambda) \frac{1}{\sqrt{1+(y')^2}}
\end{equation*}

By rearranging, we get

\begin{equation*}
x + C = \int \frac{dy}{\sqrt{\left(\frac{Ay-\lambda}{B}\right)^2 - 1}}
\end{equation*}

Let \(\frac{Ay-\lambda}{B} = \cosh t\) and we obtain that

\begin{equation*}
x + C
= \frac{B}{A}\int \frac{\sinh t}{\sqrt{\cosh^2 t - 1}} dt
= \frac{B}{A}t
= \frac{B}{A}\cosh^{-1}\left(\frac{Ay-\lambda}{B}\right)
\end{equation*}

Therefore

\begin{equation*}
y = \frac{1}{A}\left(\lambda + B\cosh\left(\frac{A}{B}(x+C)\right)\right)
\end{equation*}

where \(A = \frac{mg}{L}\) and, \(B\) and \(C\) are constants.

\section{Calculus of variations - Least action principle}
\label{develop--math6192:ROOT:page--exercises.adoc---calculus-of-variations-least-action-principle}

\subsection{Problem 1}
\label{develop--math6192:ROOT:page--exercises.adoc---problem-1}

Consider the following double pendulum illustrated below

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/double-pendulum}
\caption{Double pendulum with masses \(m_1\) and \(m_2\) and lengths \(L_1\) and \(L_2\) (respectively)}
\end{figure}

Find an expression for the kinetic energy and potential energy for each mass
and the corresponding Lagrangians and Lagrange equations of motion.

Let the \(y\) axis point downward.
We will first find the locations of the masses.

\begin{equation*}
x_1 = L_1\sin\theta_1, \quad y_1 = L_1\cos\theta_1
\end{equation*}

and

\begin{equation*}
x_2 = L_1\sin\theta_1 + L_2\sin\theta_2,
\quad
y_2 = L_1\cos\theta_1 + L_2\cos\theta_2
\end{equation*}

So, the total kinetic energy of \(m_1\) and \(m_2\)

\begin{equation*}
T_1 = \frac{m_1}{2}\dot{\theta}_1^2L_1^2
\end{equation*}

\begin{equation*}
T_2 = \frac{m_2}{2}\left[
\dot{\theta}_1^2L_1^2 + \dot{\theta}_2^2L_2^2
+ 2\dot{\theta}_1\dot{\theta}_2L_1L_2\cos(\theta_1-\theta_2)
\right]
\end{equation*}

and the potential energy is

\begin{equation*}
U_1 = -m_1gL_1\cos\theta_1
,\quad
U_2 = -m_2g\left(L_1\cos\theta_1 + L_2\cos\theta_2\right)
\end{equation*}

So, the Lagrangian is given by

\begin{equation*}
\mathcal{L}= T_1+T_2-U_1-U_2
\end{equation*}

So, the Lagrange equations of motion are given by

\begin{equation*}
\begin{aligned}
0
&= \frac{\partial \mathcal{L}}{\partial \theta_1} - \frac{d}{dt}\frac{\partial \mathcal{L}}{\partial\dot{\theta}_1}
\\&=
\left[
0+(-m_2\dot{\theta}_1\dot{\theta}_2L_1L_2\sin(\theta_1-\theta_2)) -(m_1gL_1\sin\theta_1)-(m_2gL_1\sin\theta_1)
\right]
\\&\quad - \frac{d}{dt}\left[
m_1\dot{\theta}_1L_1^2 + (m_2\dot{\theta}_1L_1^2 + m_2\dot{\theta}_2L_1L_2\cos(\theta_1-\theta_2))
- 0 - 0
\right]
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
0
&= \frac{\partial \mathcal{L}}{\partial \theta_2} - \frac{d}{dt}\frac{\partial \mathcal{L}}{\partial\dot{\theta}_2}
\\&=
[
0 + (m_2\dot{\theta}_1\dot{\theta}_2L_1L_2\sin(\theta_1-\theta_2))
    - 0 -(m_2gL_2\sin\theta_2)
]
\\&\quad - \frac{d}{dt}\left[
0
+ (m_2\dot{\theta}_2L_2^2 + m_2\dot{\theta}_1L_1L_2\cos(\theta_1-\theta_2))
- 0 - 0
\right]
\end{aligned}
\end{equation*}

\begin{admonition-todo}[{}]
maybe simplify
\end{admonition-todo}

\subsection{Problem 2}
\label{develop--math6192:ROOT:page--exercises.adoc---problem-2}

Consider the double pendulum illustrated below.
Note that mass \(m_1\) is constrained to move only on the \(x\)-axis.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/double-pendulum2}
\caption{Double pendulum with mass \(m_1\) constrained to the x-axis and mass \(m_2\) attached with a string of length \(L\)}
\end{figure}

If the spring constant is \(k\), find the Lagrangian formulation
to find the frequencies of oscillation of both masses.

WLOG, suppose that the spring is at equilibrium at \((0,0)\).
So, the position of \(m_1\) is \((x_1,x_2) = (x, 0)\)
and the position of \(m_2\) is

\begin{equation*}
x_2 = x + L\sin\theta,\quad y_2 = L\cos\theta
\end{equation*}

So, the total kinetic energy is given by

\begin{equation*}
\begin{aligned}
T
&= \frac{m_1}{2}\dot{x}^2 + \frac{m_2}{2}\left[(\dot{x} + L\dot{\theta}\sin\theta)^2 + L^2\dot{\theta}^2\cos^2\theta\right]
\\&= \frac{m_1+m_2}{2}\dot{x}^2 + \frac{m_2}{2}L^2\dot{\theta}^2 + m_2L\dot{x}\dot{\theta}\sin\theta
\end{aligned}
\end{equation*}

and the total potential energy is

\begin{equation*}
U = \frac{k}{2}x^2 -m_2gL\cos\theta
\end{equation*}

So, the Lagrangian is given by

\begin{equation*}
\mathcal{L} = T-U
\end{equation*}

The Euler-Lagrange equation for \(x\) is given by

\begin{equation*}
\begin{aligned}
0
&= \frac{\partial \mathcal{L}}{\partial x} - \frac{d}{dt}\frac{\partial \mathcal{L}}{\partial \dot{x}}
\\&= (0 - kx) - \frac{d}{dt}\left[
    [(m_1+m_2)\dot{x} + m_2L\dot{\theta}\sin\theta]
    - 0
\right]
\\&\implies\quad kx + (m_1+m_2)\ddot{x} + m_2L[\ddot{\theta}\sin\theta + \dot{\theta}^2\cos\theta] = 0
\end{aligned}
\end{equation*}

The Euler-Lagrange equation for \(\theta\) is given by

\begin{equation*}
\begin{aligned}
0
&= \frac{\partial \mathcal{L}}{\partial \theta} - \frac{d}{dt}\frac{\partial \mathcal{L}}{\partial \dot{\theta}}
\\&= [m_2L\dot{x}\dot{\theta}\cos\theta - (m_2gL\sin\theta)]
- \frac{d}{dt}\left[
    (m_2L^2\dot{\theta} + m_2L\dot{x}) - 0
\right]
\\&\implies\quad \dot{x}\dot{\theta}\cos\theta-g\sin\theta -L[\ddot{\theta} + \ddot{x}] = 0
\end{aligned}
\end{equation*}

\begin{admonition-todo}[{}]
find frequency
\end{admonition-todo}

\section{Concepts in Mechanics - Fluid inside cylindrical vessel}
\label{develop--math6192:ROOT:page--exercises.adoc---concepts-in-mechanics-fluid-inside-cylindrical-vessel}

Consider an ideal fluid inside a cylindrical vessel which is placed on top of a rotating table.
What is the shape of the surface of the liquid inside the cylinder if the table rotates with angular velocity \(\omega\)?

We will assume that the liquid is also rotating with angular velocity \(\omega\)
in perfect concentric rings with no turbulence or inter-molecule interactions.

We use coordinates \((r,\theta, z)\) where \(r\) is the radius from the center,
\(\theta\) is some angle and \(z\) is the height.
We can resolve these to Cartesian coordinates using

\begin{equation*}
x = r\cos\theta,\quad y = r\sin\theta
\end{equation*}

We assume that there is no vertical or radial velocity in the fluid.
So

\begin{equation*}
\begin{aligned}
\dot{x} &= -r\dot{\theta}\sin\theta = -r\omega \sin\theta = -\omega y\\
\dot{y} &= r\dot{\theta}\cos\theta = r\omega\cos\theta = \omega x\\
\end{aligned}
\end{equation*}

So,

\begin{equation*}
\vec{v}(t,x,y,z) = (-\omega y, \omega x, 0)
\end{equation*}

Now, let us look at Euler's equation

\begin{equation*}
-\frac{1}{\rho}\nabla P + \vec{g} =
\frac{\partial\vec{v}}{\partial t} + (\vec{v} \cdot \nabla)\vec{v}
\end{equation*}

We can evaluate everything except for \(\nabla P\).
The left hand side works out to be

\begin{equation*}
\begin{aligned}
&\frac{\partial\vec{v}}{\partial t} + (\vec{v} \cdot \nabla)\vec{v}
\\&= \vec{0}
+ \left(-\omega y \frac{\partial}{\partial x} + \omega x \frac{\partial}{\partial y}\right) (-\omega y, \omega x, 0)
\\&=
\left(-\omega^2 x,-\omega^2y, 0\right)
\end{aligned}
\end{equation*}

So, we have that

\begin{equation*}
\begin{aligned}
-\frac{1}{\rho} P_x + 0 &= -\omega^2 x
\\
-\frac{1}{\rho} P_y + 0 &= -\omega^2 y
\\
-\frac{1}{\rho} P_z -g &= 0
\end{aligned}
\end{equation*}

where \(g\approx 9.81\) is the acceleration due to gravity.
We assume that \(P(r,z)\) and does not vary with \(\theta\).
So

\begin{equation*}
(P_x, P_y, P_z) = \nabla P = \left(P_r \frac{\partial r}{\partial x}, P_r \frac{\partial r}{\partial y}, P_z\right)
\end{equation*}

Since \(r = \sqrt{x^2+y^2}\),

\begin{equation*}
\frac{\partial r}{\partial x} = \frac{x}{\sqrt{x^2+y^2}} = \frac{x}{r}
\end{equation*}

and likewise \(\frac{\partial r}{\partial y} = \frac{y}{r}\).
Therefore, we have that

\begin{equation*}
\begin{aligned}
-\frac{1}{\rho} P_r \frac{x}{r} &= -\omega^2 x
\\
-\frac{1}{\rho} P_r \frac{y}{r} &= -\omega^2 y
\\
-\frac{1}{\rho} P_z -g &= 0
\end{aligned}
\end{equation*}

After rearranging, we get

\begin{equation*}
P_r = \omega^2 \rho r,
\quad\text{and}\quad
P_z = -g\rho
\end{equation*}

So, the pressure increases as \(r\) and \(z\) increase.
Therefore,

\begin{equation*}
P(r,z) = \rho\left(\frac{\omega^2 r^2}{2} - gz\right) + C
\end{equation*}

where \(C\) is a constant of integration.
If we assume that the surface pressure is the same, the shape of the surface is given by the contour lines.
So the shape of the surface is given by

\begin{equation*}
z = \frac{1}{g}\left(\frac{\omega^2r^2}{2} + \frac{C-P_0}{\rho}\right)
\end{equation*}

where \(P_0\) is the pressure at the surface.

\subsection{Using the Lagrangian}
\label{develop--math6192:ROOT:page--exercises.adoc---using-the-lagrangian}

\begin{admonition-remark}[{}]
I am not too sure how correct the below is (I haven't properly set things up and as a result, I don't understand it fully).
But it is good to see that we get the same result (modulo a constant which I don't understand why it isn't there).
Also, by using the Lagrangian, we need some path. What is the action being performed?
\end{admonition-remark}

We can instead use the Lagrangian to find the shape of our surface.
Let \(h\) be the height of our fluid.
Let \(A\) be an arbitrary part of the area of the container.
So, the total potential energy is given by

\begin{equation*}
U
= \iint_A \int_0^{h} \rho g z \ dz \ dA
= \iint_A \frac{1}{2} \rho g h^2 \ dA
\end{equation*}

Also, the total kinetic energy is given by

\begin{equation*}
T
= \iint_A \int_0^{h} \frac{\rho (\dot{x}^2 + \dot{y}^2 + \dot{z}^2)}{2} \ dz \ dA
= \iint_A \frac{\rho (\dot{x}^2+\dot{y}^2)}{2} h \ dA
\end{equation*}

Note that \(\dot{z} = 0\) since was assume there is no vertical motion and things are stable.
So, the Lagrangian is given by

\begin{equation*}
\mathcal{L} = T-U = \frac{1}{2}\rho \iint_A \left[gh^2 - (\dot{x}^2+\dot{y}^2) h\right] \ dA
\end{equation*}

We have three variables/functions, \((x,y,h)\).
So, there are three Euler-Lagrange equations.
We will omit the constant factor of \(\frac{1}{2}\rho\).

\begin{equation*}
0 = \frac{\partial \mathcal{L}}{\partial x} - \frac{d}{dt}\left(\frac{\partial \mathcal{L}}{\partial \dot{x}}\right)
= 0 - \frac{d}{dt}\iint_A (-2\dot{x}h) \ dA
\quad\implies \iint_A \dot{x}h \ dA = \text{constant wrt } t
\end{equation*}

We get a similar equation for \(\dot{y}\).
For \(h\), we get

\begin{equation*}
0 = \frac{\partial \mathcal{L}}{\partial h} - \frac{d}{dt}\left(\frac{\partial \mathcal{L}}{\partial \dot{h}}\right)
= \iint_A [2gh - (\dot{x}^2 + \dot{y}^2)] \ dA
\end{equation*}

Since the above holds for all \(A\), we have that

\begin{equation*}
h = \frac{1}{2g}(\dot{x}^2 + \dot{y}^2) = \frac{1}{2g}\omega^2r^2
\end{equation*}

\section{Concepts in Mechanics - Lagrangian vs Eulerian Equations of Motion}
\label{develop--math6192:ROOT:page--exercises.adoc---concepts-in-mechanics-lagrangian-vs-eulerian-equations-of-motion}

\subsection{Problem 1}
\label{develop--math6192:ROOT:page--exercises.adoc---problem-1-2}

\begin{custom-quotation}[{}][{}]
Consider the particular case where

\begin{equation*}
x_0 = x(t_0) = \xi
\end{equation*}

and

\begin{equation*}
v = \frac{1}{1+\xi^2}
\end{equation*}

Use the equation

\begin{equation*}
x = \xi + \int_{t_0}^t v(\xi, t) \ dt
\end{equation*}

to find the parametric representation of the caustics.
Hence find the critical point \((x_{cr}, t_{cr})\)
at which the caustic appears. You should get two points,
but one of them is unrealistic as it is not possible to have a negative
value for \(t_{cr}\)
\end{custom-quotation}

First, using the above equation we get that

\begin{equation*}
x
= \xi + \int_{t_0}^t \frac{1}{1+\xi^2} \ dt
= \xi + \frac{t-t_0}{1+\xi^2}
\end{equation*}

We differentiate wrt \(\xi\) to find the critical point

\begin{equation*}
0 = \frac{dx}{d\xi} = 1 -2\xi \frac{(t-t_0)}{(1+\xi^2)^2}
\implies t = t_0 + \frac{(1+\xi^2)^2}{2\xi}
\end{equation*}

So,

\begin{equation*}
x = \xi + \frac{1+\xi^2}{2\xi}
\end{equation*}

Also, at the critical point

\begin{equation*}
0 = \frac{dt}{d\xi} = \frac{2\xi (4\xi)(1+\xi^2) - 2(1+\xi^2)^2}{4\xi^2}
\end{equation*}

By solving we get real solutions of \(\xi = \pm \frac{1}{\sqrt{3}}\).
So, we either get

\begin{equation*}
(x_{cr},t_{cr}) = \left(\pm \sqrt{3},\ t_0 \pm \frac{8}{3\sqrt{3}}\right)
\end{equation*}

Note that only one solution is valid since \(t_{cr} \geq t_0\)

\begin{equation*}
(x_{cr},t_{cr}) = \left(\sqrt{3},\ t_0 + \frac{8}{3\sqrt{3}}\right)
\end{equation*}

\subsection{Problem 2}
\label{develop--math6192:ROOT:page--exercises.adoc---problem-2-2}

\begin{custom-quotation}[{}][{}]
For the velocity distribution of particles in Lagrange coordinates,

\begin{equation*}
v = 2\xi^2 t
\end{equation*}

Find the equation for the caustics and the density in Euler coordinates
\(\rho(x,t)\).
\end{custom-quotation}

We follow the same process as previously,

\begin{equation*}
x
= \xi + \int_{t_0}^t 2\xi^2t \ dt
= \xi + \xi^2(t^2-t_0^2)
\end{equation*}

This is the parametric representation of the caustics.

Next, recall the one-dimensional continuity equation

\begin{equation*}
\frac{\partial \rho}{\partial t} + \frac{\partial \rho}{\partial x} v + \rho\frac{\partial v}{\partial x} =0
\end{equation*}

So, by using Lagrange coordinates

\begin{equation*}
\frac{d\rho}{dt} = -\rho \frac{\partial v}{\partial x}
\end{equation*}

where \(v = \frac{d x}{d t}\).
From this point, we only need to compute \(\frac{\partial v}{\partial x}\).
We get

\begin{equation*}
\frac{\partial v}{\partial x}
= \frac{\partial v}{\partial \xi}\frac{\partial\xi}{\partial x}
= \frac{4\xi t}{1 + 2\xi(t^2-t_0^2)}
\end{equation*}

So

\begin{equation*}
\begin{aligned}
\rho
&= \frac{\rho_0}{\exp\left\{\int_{t_0}^t \frac{\partial v}{\partial x} \ dt\right\}}
\\&= \frac{\rho_0}{\exp\left\{\int_{t_0}^t \frac{4\xi t}{1 + 2\xi(t^2-t_0^2)} \ dt \right\}}
\\&= \frac{\rho_0}{\exp\left\{\int_{t_0}^t \frac{\partial }{\partial t} \ln (1 + 2\xi(t^2-t_0^2)) \ dt \right\}}
\\&= \frac{\rho_0}{1 + 2\xi(t^2-t_0^2)}
\end{aligned}
\end{equation*}

\begin{admonition-note}[{}]
We get the same result as if we just used \(\rho = \frac{\rho_0}{\partial x / \partial \xi}\)
\end{admonition-note}

By rearranging the formula for \(x\), we get \(\xi = \frac{-1 \pm \sqrt{1+4(t^2-t_0^2)}}{2(t^2-t_0^2)}\)
So, we get

\begin{equation*}
\rho(x,t) = \frac{\rho_0}{\pm 2\sqrt{1+4x(t^2-t_0^2)}}
\end{equation*}

We use only the positive solution since density is non-negative.

\section{Linear stability Analysis - Instabilities at the interface of two fluids}
\label{develop--math6192:ROOT:page--exercises.adoc---linear-stability-analysis-instabilities-at-the-interface-of-two-fluids}

Please see the relevant section as it is done there.

\section{Heat-flow}
\label{develop--math6192:ROOT:page--exercises.adoc---heat-flow}

\begin{admonition-note}[{}]
This problem is in the slides only.
\end{admonition-note}

\begin{custom-quotation}[{}][{}]
Find the self-similar solution for the temperature distribution about a unit sphere with
oscillating temperature on the surface

\begin{equation*}
T(1,t) = T_0 \exp(i\omega t)
\end{equation*}

by assuming that the temperature solution is of the form

\begin{equation*}
T = \left[\frac{f(r)}{r}\right]\exp(i\omega t)
\end{equation*}

and by substituting into the heat conduction equation

\begin{equation*}
\frac{\partial T}{\partial t} = \kappa \frac{1}{r^2}\frac{\partial}{\partial r}\left(t^2\frac{\partial T}{\partial r}\right)
\end{equation*}
\end{custom-quotation}

Note that

\begin{equation*}
\frac{\partial T}{\partial t} = i\omega \left[\frac{f(r)}{r}\right]\exp(i\omega t)
\end{equation*}

and

\begin{equation*}
\frac{\partial T}{\partial r} = \frac{rf'(r) -f(r)}{r^2}\exp(i\omega t)
\end{equation*}

So,

\begin{equation*}
\frac{\partial}{\partial r} \left(r^2\frac{\partial T}{\partial r}\right)
= \frac{\partial }{\partial r}\left[rf'(r) -f(r)\right]\exp(i\omega t)
= rf''(r)\exp(i\omega t)
\end{equation*}

By substituting into the heat conduction equation

\begin{equation*}
\begin{aligned}
i\omega \frac{f(r)}{r} &= \kappa \frac{rf''(r)}{r^2}
\\\implies\quad
i\omega f(r) &= \kappa f''(r)
\\\implies\quad
0 &= f''(r) - \frac{i\omega}{\kappa} f(r)
\end{aligned}
\end{equation*}

The characteristic roots are

\begin{equation*}
\lambda
= \pm \sqrt{i}\sqrt{\frac{\omega}{\kappa}}
= \pm (1+i)\sqrt{\frac{\omega}{2\kappa}}
\end{equation*}

Therefore

\begin{equation*}
f(r) = A\cosh\left( r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)
+ B\sinh\left(r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)
\end{equation*}

for some constants \(A\) and \(B\).
So

\begin{equation*}
T(r,t) = \frac{
A\cosh\left( r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)
+ B\sinh\left(r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)
}{r} \exp(i\omega t)
\end{equation*}

Note that we expect that the temperature remains bounded within the sphere.
So, the following limit should exist

\begin{equation*}
\lim_{r\to 0} T(r,t)
= \lim_{r\to 0} \frac{
A\cosh\left( r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)
+ B\sinh\left(r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)
}{r} \exp(i\omega t)
\end{equation*}

Note that \(\sinh \to 0\) as its argument goes to \(0\).
So, we require that \(A =0\).
Therefore

\begin{equation*}
T(r,t) = B\frac{\sinh\left(r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)}{r} \exp(i\omega t)
\end{equation*}

Next, we can use the boundary condition at \(r=1\),

\begin{equation*}
T_0 \exp(i\omega t) = T(1,t) = B \sinh\left((1+i) \sqrt{\frac{\omega}{2\kappa}}\right)\exp(i\omega t)
\end{equation*}

Therefore,

\begin{equation*}
T(r,t) = \frac{T_0}{\sinh\left((1+i) \sqrt{\frac{\omega}{2\kappa}}\right)}
\frac{\sinh\left(r (1+i) \sqrt{\frac{\omega}{2\kappa}}\right)}{r} \exp(i\omega t)
\end{equation*}

Let \(\mu = \sqrt{\frac{\omega}{2\kappa}}\).
Then,

\begin{equation*}
\begin{aligned}
T(r,t)
&= \frac{T_0}{\sinh\left((1+i) \mu\right)} \frac{\sinh\left(r (1+i) \mu\right)}{r} \exp(i\omega t)
\\&= \frac{T_0}{r}
\frac
{\exp[(1+i)\mu r] - \exp[-(1+i)\mu r]}
{\exp[(1+i)\mu] - \exp[-(1+i)\mu]}
\exp(i\omega t)
\\&= \frac{T_0}{r}
\frac
{\exp(\mu r)\exp(i\mu r) - \exp(-\mu r)\exp(-i\mu r)}
{\exp(\mu)\exp(i\mu) - \exp(-\mu)\exp(-i\mu)}
\exp(i\omega t)
\\&= \frac{T_0}{r}
\frac
{e^{\mu r}e^{i\mu r} - e^{-\mu r}e^{-i\mu r}}
{e^\mu e^{i\mu} - e^{-\mu}e^{-i\mu}}
\frac
{e^\mu e^{-i\mu} - e^{-\mu}e^{i\mu}}
{e^\mu e^{-i\mu} - e^{-\mu}e^{i\mu}}
\exp(i\omega t)
\\&= \frac{T_0}{r}
\frac
{e^{\mu r}e^{i\mu r} - e^{-\mu r}e^{-i\mu r}}
{e^{2\mu} + e^{-2\mu} - e^{2i\mu} - e^{-2i\mu}}
\frac
{e^\mu e^{-i\mu} - e^{-\mu}e^{i\mu}}
{e^\mu e^{-i\mu} - e^{-\mu}e^{i\mu}}
\exp(i\omega t)
\end{aligned}
\end{equation*}

\begin{admonition-todo}[{}]
Comment
\end{admonition-todo}
\end{document}
