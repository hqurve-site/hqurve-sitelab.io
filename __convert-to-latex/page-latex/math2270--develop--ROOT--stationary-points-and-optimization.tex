\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Stationary points and Optimization}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
\section{Stationary points}
\label{develop--math2270:ROOT:page--stationary-points-and-optimization.adoc---stationary-points}

Let \(f: D \rightarrow \mathbb{R}\) be a function, such that
\(D \subseteq \mathbb{R}^n\). A \emph{critical (stationary or
level) point} of \(f\) is a point \(\bm{x}_0\) at
which all first partial derivatives vanish, ie

\begin{equation*}
\forall i = 0, 1, \ldots, n: \left.\frac{\partial f}{\partial x_i}\right|_{\bm{x}_0}
\end{equation*}

Geometrically, a critical point is a place where the graph of
\(f\) is horizontal (ie all tangent lines are horizontal).
Additionally, The rational for this definition can be obtained by
looking at the definition of differentiable function. Furthermore, while
we call \(\bm{x}_0\) the critical point,
\(f(\bm{x}_0)\) is called the critical value.

\subsection{Maximums and minimums}
\label{develop--math2270:ROOT:page--stationary-points-and-optimization.adoc---maximums-and-minimums}

\(\bm{x}_0 \in D\) is said to be

\begin{itemize}
\item a \emph{relative (local) maximum} if open subset
    \(\exists: B \subseteq D\) such that
    \(\forall \bm{x}\in B: f(\bm{x}_0) \geq f(\bm{x})\).
\item a \emph{relative (local) minimum} if open subset
    \(\exists: B \subseteq D\) such that
    \(\forall \bm{x}\in B: f(\bm{x}_0) \leq f(\bm{x})\).
\item a \emph{absolute (global) maximum} if
    \(\forall \bm{x}\in D: f(\bm{x}_0) \geq f(\bm{x})\).
\item a \emph{absolute (global) minimum} if
    \(\forall \bm{x}\in D: f(\bm{x}_0) \leq f(\bm{x})\).
\end{itemize}

\begin{theorem}[{Location of local maxima and minima}]
If \(f\) is continuous and all first partial
derivatives exists, then the local maxima and minima of \(f\)
occur either at the boundary of \(D\) or at the critical
points.
\end{theorem}

\begin{theorem}[{Extreme Value Theorem}]
If \(R\) is a closed region in \(\mathbb{R}^n\),
which \(f\) is continuous on, then \(\exists\) at
least one point in \(R\) where \(f\) has an absolute
maximum value and another where \(f\) has an absolute minimum
value.
\end{theorem}

\subsection{Classification of stationary points}
\label{develop--math2270:ROOT:page--stationary-points-and-optimization.adoc---classification-of-stationary-points}

Let \(f\) be a function fo two variables \(x\) and
\(y\) such that its first and second order partial derivatives
are continuous on an open disk containing the stationary point
\((a, b)\). That is

\begin{equation*}
f_x(a,b) = 0\quad\quad f_y(a,b) = 0
\end{equation*}

Now define

\begin{equation*}
p = f_{xx}(a,b)
    \quad\quad
q = f_{xx}(a,b)
    \quad\quad
r = f_{xy}(a,b)
\end{equation*}

Then

\begin{itemize}
\item \((a,b)\) is a local minima if \(pq - r^2 > 0\)
    and \(p > 0\) (or \(q > 0\)).
\item \((a,b)\) is a local maxima if \(pq - r^2 > 0\)
    and \(p < 0\) (or \(q < 0\)).
\item \((a,b)\) is a saddle point if \(pq - r^2 > 0\)
\item If \(pq -r^2 = 0\), no conclusion can be made
\end{itemize}

\begin{admonition-note}[{}]
Since the second order partial derivatives of \(f\) are
continuous, \(f_{xy} = f_{yx}\)
\end{admonition-note}

\section{Lagrange Multipliers}
\label{develop--math2270:ROOT:page--stationary-points-and-optimization.adoc---lagrange-multipliers}

\emph{Lagrange multipliers} are a useful tool when trying to optimize (find
relative extrema) a function subject to one or more constraints.

Let \(f: D \rightarrow \mathbb{R}\) be a continuously
differentiable function, such that
\(D \subseteq \mathbb{R}^n\). Then the relative extrema of
\(f\) subject to the \(k\) constraints
\(\varPhi_i(x_1, x_2, \ldots, x_n) = 0\) for
\(i = 1, 2,\ldots, k\) (which are also continuously
differentiable) are also relative extrema of the following auxiliary
function

\begin{equation*}
F(x_1, x_2, \ldots, x_n, \lambda_1, \lambda_2, \ldots, \lambda_k)
    = f + \lambda_1\varPhi_1 + \lambda_2\varPhi_2 + \cdots + \lambda_k\varPhi_k
\end{equation*}

where \(\lambda_1, \lambda_2, \ldots, \lambda_k\) (independent
of \(x_1, x_2, \ldots, x_n\)) are called \emph{Lagrange
multipliers}.

Therefore, finding the relative extrema of \(f\) with
constraints \(\varPhi_1, \varPhi_2, \ldots, \varPhi_k\)
reduces to finding solutions to

\begin{equation*}
F_{x_1} = 0, \;
    F_{x_2} = 0, \;
    \ldots,\;
    F_{x_n} = 0
\end{equation*}

and

\begin{equation*}
F_{\lambda_1} = 0, \;
    F_{\lambda_2} = 0, \;
    \ldots,\;
    F_{\lambda_k} = 0
\end{equation*}

Notice that these last \(k\) equations are equivalent to the
constraints \(\varPhi_i(x_1, x_2, \ldots, x_n) = 0\) (for
\(i = 1, 2,\ldots, k\)).

\subsection{Reasoning}
\label{develop--math2270:ROOT:page--stationary-points-and-optimization.adoc---reasoning}

Consider any point \(\bm{x}\) which satisfies all constraints.
Then the set of directions allowed by all constraints is precisely the
space of directions perpendicular to the gradients of all constraints.
That is, if the space of allowable moves is \(A\) at
\(\bm{x}\) and the span of the constraints’ gradients is
\(S\), then \(A = S^{\perp}\). Furthermore, by
considering the (constrained) points at which \(f\) has an
extremum (ie doesn’t change), we see that the allowable directions must
be perpendicular to the gradient of \(f\) otherwise, we could
increase/decrease \(f\) by moving in that allowed direction.
Therefore, \(\nabla f(\bm{x}) \in A^\perp = S\) and hence
there exists \(\lambda_1, \lambda_2, \ldots, \lambda_k\) such
that

\begin{equation*}
\nabla f(\bm{x}) = \sum_{i=1}^k \lambda_i \nabla \varPhi_i(\bm{x})
    \quad\Longleftrightarrow\quad
    \nabla f(\bm{x}) - \sum_{i=1}^k \lambda_i \nabla \varPhi_i(\bm{x}) = \bm{0}
\end{equation*}

This condition along with the constraints are equivalent finding to
stationary points of the auxiliary function. Note that this condition is
necessary but not sufficient (ie not all solutions are indeed
solutions).
\end{document}
