\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Vacuous Proofs}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
I will start by saying that I am not against the method of vacuous proofs
but rather I am against the way in which they are introduced to students.
When being thought a proof by contradiction or a proof by contrapositive
or direct proof, we resort to direct definition using boolean expressions
and truth tables. However, for a vacuous proof, it is introduced as an
``advanced method of proof'' which has caused some students to regard
this method of proof as something distinct rather than something that they
have prove themselves using tools they already have.

\begin{admonition-important}[{}]
This first paragraph was written prior to learning more and hence
reflects my initial view of the topic (its a wonder what a few hours could do).
I am leaving it as is in order to showcase the learning process.
\end{admonition-important}

\section{Example of how this method of teaching can cause students to have misconceptions}
\label{develop--main:opinions:page--vacuous.adoc---example-of-how-this-method-of-teaching-can-cause-students-to-have-misconceptions}

Consider the following
interaction which occurred during one class

\begin{sidebar}[{Situation}]
During a previous class, the lecturer went over why the empty set is
\myautoref[{open}]{develop--math3277:topology:page--open.adoc}. He explained

\begin{custom-quotation}[{lecturer}][{}]
Since there does not exist an element in the
empty set which would disprove that it is open, vacuously
the empty set is open.
\end{custom-quotation}

This explanation is actually perfectly fine (actually excellent since
it alludes to the basis of a vacuous proof; more on this later)
however to a student
who doesn't know that a vacuous proof is just a method of proof,
it can lead to a misconception. In fact, during the next class,
a student asked the following question

\begin{custom-quotation}[{student}][{}]
Since vacuously, there doesn't exist an element in the empty
set to disprove that it is open, can't we also say that there
doesn't exist an element in the empty set to disprove that it
is not open?
\end{custom-quotation}

Additionally, the student had initially asked a more general question
of if a vacuous proof can prove that two contradicting statements
are true.

\begin{admonition-note}[{}]
I'm paraphrasing what was actually said.
\end{admonition-note}
\end{sidebar}

Firstly, we need to define what we mean by a set being `open'.
A set \(S\) is open iff

\begin{equation*}
\forall x \in S: \exists r > 0: B(x,r) \subseteq S
\end{equation*}

\begin{admonition-tip}[{}]
More information about the symbols used could be found
on the \myautoref[{topology page}]{develop--math3277:topology:page--index.adoc}.
\end{admonition-tip}

Let \(P(x)\) denote \(\exists r > 0: B(x,r) \subseteq S\)
as it is not really relevant to our discussion. Then,
\(S\) is open iff \(\forall x \in S: P(x)\). Now,
when \(S = \varnothing\), why is this statement true.
We can consider any of the following arguments

\begin{itemize}
\item \(S\) is open iff \(x \in S \implies P(x)\). Now, since
    \(x \in S\) is false when \(S = \varnothing\) (by definition),
    we get that the overall implication is true. Although this approach makes the most sense
    we then have to as ourselves what set \(x\) is in? Is there a universal set.
    Additionally, we implicitly take that this statement is quantified using the universal quantifier.
\item We can equivalently rewrite the statement as \(\lnot [ \exists x \in S: \lnot P(x) ]\)
    Then since \(S = \varnothing\), the statement of existence is trivially false and helse
    the negation is true.
\end{itemize}

\section{Whats the big issue about?}
\label{develop--main:opinions:page--vacuous.adoc---whats-the-big-issue-about}

At the heart of the issue, lies the universal quantifier and what to do if the set is empty.
There is no right answer, however, by convention, we take it to be true, for several reasons.

\begin{description}
\item[Interoperability with existential quantifier] Consider the statement \(\exists x \in S: P(x)\)
    and consider the case when \(S = \varnothing\). Then, it is most convenient to have this
    statement be false. This is indeed the convention. Now, recall the following equivalence
    
    \begin{equation*}
    \exists x \in S: P(x) \equiv \lnot[ \forall x \in S: \lnot P(x)]
    \end{equation*}
    
    This equivalence holds when \(S\) is non-empty, so it would be very convenient if it also
    held when \(S=\varnothing\). In this case, we see that the universal quantifier must therefore
    be true when the set is empty.
\end{description}

\section{Why convenience matters}
\label{develop--main:opinions:page--vacuous.adoc---why-convenience-matters}

As discussed, the choice of whether to take universal quantifier as true when the set
is empty is completely arbitrary. In fact, it is simply a matter of notation.
We could just as easily take it to be false however, in this case, we might end up needing
to over explain several results.

Take again, the example with open sets
\href{https://math.stackexchange.com/q/447170}{this stack exchange question} make several points
as to why we would like to define the empty set as open.

\begin{description}
\item[Intersection of two disjoint open sets] The intersection of two open sets is also open, so it would be nice if the result also held
    for two disjoint sets.
\item[The entire set is closed] A set is closed iff its complement is open. Alternatively, we say that a set
    is closed if it contains all of its limit points. Then the entire set
    is closed and hence its complement, the empty set, must be open.
\item[Pre-image of continuous functions] Let \(f\) be continuous and \(A\) be an open set. Then, \(f^{-1}(A)\)
    is open (this could either be proven or be a definition). Now, if \(A\)
    is disjoint from the range of \(f\), we get that the pre-image of \(A\)
    is the empty set.
\end{description}

At the end of the day, the choice of whether the empty set is open is a choice.
If we take it to be false, we can adjust our statements to still be true. However,
the convenience which comes from it being open cannot be ignored and hence it has been
taken as convention (and in fact a definition).

\section{My comments}
\label{develop--main:opinions:page--vacuous.adoc---my-comments}

Initially, the plan of this section was to satirical rant about definitions
and modes of teaching.
However, after giving it much thought (I planned to write this section at least
one week prior to actually doing it) and actually looking into the subject,
I have realized that there is more to it than just a definition. Convenience matters.

I leave with \href{https://math.stackexchange.com/a/403149}{this} quote on the subject by a stackexchange user.

\begin{custom-quotation}[{fedja}][{}]
You are not alone who is puzzled with that. Lewis Carroll (who was a logician by trade) also objected to ``vacuous truth''.
The choice that the modern logic makes is due more to the convenience of drawing implications without worrying if some
sets are empty or not at each step, than to the intuition. You want to be able to combine the sentences ``Everybody who is
not too short can pick an apple on this tree'' and ``Everybody who is not too tall will be able to enter the vault'' into
``Everybody who is neither too short, nor too tall will be able to pick the apple and go to the vault with it'' without
heavy thinking of how the threshold heights in those cases are related to each other. This forces you to assign the
``true'' value to the last statement even if you need to be at least 6 feet tall to reach the apple and no more than 5
feet tall to enter the vault.

In general, such vacuous usage of "for every" is not uncommon in the real life. When the sheriff in a small town proclaims
publicly that ``Every criminal will be caught and punished!'', the streets are patrolled day and night, and there are no
crimes in the town, you don't call him a liar and the police department a worthless waste of taxpayer money, quite the
opposite: you suspect that he is so right that even the to-be-criminals believe this statement of his and abstain from misdeeds.
\end{custom-quotation}
\end{document}
