\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Sets}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Definition}
\label{develop--math2273:ROOT:page--sets.adoc---definition}

We may define a set as a collection of ''well-defined'' objects; these
objects are usually referred to as elements. In this collection, no two
objects are identical.

\begin{admonition-note}[{}]
When we say an object is well defined, it means that all instances
of it are exactly the same (this is the reason we define the rationals
in terms of equivalence classes).
\end{admonition-note}

\section{Set builder notation}
\label{develop--math2273:ROOT:page--sets.adoc---set-builder-notation}

Strictly speaking we define a set \(S\) using a predicate
\(P\) as follows

\begin{equation*}
x \in S \iff P(x)
\end{equation*}

However, this notation is sometimes cumbersome. Instead, we can use the
equivalent set builder notation

\begin{equation*}
S =  \{x : P(x)\}
\end{equation*}

\section{Subset}
\label{develop--math2273:ROOT:page--sets.adoc---subset}

\(A\) is a subset of \(B\) (written as
\(A \subseteq B\)) is equivalent to the following statement

\begin{equation*}
\forall x \in A : x \in B
\end{equation*}

\section{Set equality}
\label{develop--math2273:ROOT:page--sets.adoc---set-equality}

Two sets \(A\) and \(B\) are equal if they consists
of exactly the same elements.

\begin{equation*}
A = B \quad \equiv \quad A \subseteq B  \text{ and }  B \subseteq A
\end{equation*}

\section{Proper subset (strict subset)}
\label{develop--math2273:ROOT:page--sets.adoc---proper-subset-strict-subset}

A strict subset means that the two sets are not equal ie

\begin{equation*}
A \subset B \quad \equiv \quad A \subseteq B \text{ and } A \neq B
\end{equation*}

\section{Empty set \(\varnothing\)}
\label{develop--math2273:ROOT:page--sets.adoc---empty-set-latex-backslash-latex-backslashvarnothing-latex-backslash}

The empty set (\(\varnothing\)) is the set that has no
elements. Note that the empty set is a subset of all sets; this can be
proved using contradiction or the fact that the universal operator
defaults to true on empty sets.

\section{Set union}
\label{develop--math2273:ROOT:page--sets.adoc---set-union}

The union of two sets \(A\) and \(B\) is

\begin{equation*}
A \cup B = \{x: x \in A \wedge x \in B\}
\end{equation*}

\section{Set intersection}
\label{develop--math2273:ROOT:page--sets.adoc---set-intersection}

The intersection of two sets \(A\) and \(B\) is

\begin{equation*}
A \cap B = \{x: x \in A \vee x \in B\}
\end{equation*}

\begin{admonition-note}[{}]
\(A \cap B \subseteq A \cup B\)
\end{admonition-note}

\section{Disjoint sets}
\label{develop--math2273:ROOT:page--sets.adoc---disjoint-sets}

Two sets \(A\) and \(B\) are disjoint if they have
no common elements. That is their intersection is the empty set.

\section{Set difference}
\label{develop--math2273:ROOT:page--sets.adoc---set-difference}

The difference between a set \(A\) and a set \(B\)
is the set

\begin{equation*}
A \backslash B = A - B = \{x: x\in A \wedge x \notin B\}
\end{equation*}

If \(A \subseteq X\), then \(X \backslash A\) (or
\(A'\) or \(A^c\)) is referred to as the complement
of \(A\) in \(X\).

\section{De Morgan’s Laws}
\label{develop--math2273:ROOT:page--sets.adoc---de-morgans-laws}

If \(A\) and \(B\) are subsets of X, then

\begin{itemize}
\item \((A \cup B)^c = A^c \cap B^c\)
\item \((A \cap B)^c = A^c \cup B^c\)
\end{itemize}

\section{Note on the natural numbers (\(\mathbb{N}\))}
\label{develop--math2273:ROOT:page--sets.adoc---note-on-the-natural-numbers-latex-backslash-latex-backslashmathbb-latex-openbracen-latex-closebrace-latex-backslash}

In this course (and other algebra courses), we define
\(\mathbb{N}\) beginning at 0. That is

\begin{equation*}
\mathbb{N} = \mathbb{Z}^+ \cup \{0\} = \{n: n \in \mathbb{Z} \wedge n \geq 0\}
\end{equation*}

\section{Cartesian product}
\label{develop--math2273:ROOT:page--sets.adoc---cartesian-product}

The Cartesian product \(A \times B\) of two sets
\(A\) and \(B\) in the set consisting of all ordered
paris \((x, y)\) where \(x \in A\) and
\(y \in B\). That is

\begin{equation*}
A \times B = \{(x, y): x \in A \wedge y \in B\}
\end{equation*}

If \(A = B\), then we write \(A \times B\) as
\(A^2\)

\begin{admonition-note}[{}]
Sadly the notation for ordered pairs and intervals are the same however
it is usually simple to decipher which is which based on the context.
\end{admonition-note}

\subsection{Extension}
\label{develop--math2273:ROOT:page--sets.adoc---extension}

We can extend this notion of cartesian product to more than two sets.
That is the cartesian product of \(A_1, A_2, \dots ,A_n\) is

\begin{equation*}
A_1 \times A_2 \times \ldots \times A_n =
    \{(x_1, x_2, \ldots , x_n) : x_i \in A_i \quad \forall i \in 1, 2, \ldots , n\}
\end{equation*}

Similarly if \(A_1 = A_2 = \ldots = A_n\) then we denote the
cartesian product as \(A^n\).
Additionally, each element

\begin{equation*}
x = (x_1, x_2, \ldots , x_n) \in A_1 \times A_2 \times \ldots \times A_n
\end{equation*}

is called an \(n\)-tuple, and each \(x_i \in A_i\)
is called a component or coordinate entry of \(x\).
\end{document}
