\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{SIS model}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In there are SIS model, there are only the susceptible and infected populations.
This model works well for diseases which are endemic.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/epidemic-SIS}
\caption{Flow diagram for SIS model}
\end{figure}

We assume that

\begin{itemize}
\item The total population size, \(N=S(t) + I(t)\), is constant. No births and no deaths.
\item All infected individuals are infectious
\item A susceptible individual (from class \(S\)) becomes
    infected when in contact with an infectious individual with transmission rate constant \(\beta\)
\item An infected individual (from class \(I\)) becomes susceptible with probability \(\alpha\) in each unit time.
\item A previously infected individual which can become susceptible and infected again.
\end{itemize}

\section{Governing equations}
\label{develop--math6192:ROOT:page--epidemic-models/sis.adoc---governing-equations}

\begin{equation*}
\begin{aligned}
S' &= -\beta IS + \alpha I\\
I' &= \beta IS -\alpha I\\
\end{aligned}
\end{equation*}

Since \(N=S(t) + I(t)\) is constant, we only need one of the above equations.
Therefore, we can only work with \(I\) as

\begin{equation*}
I'
= \beta I(N-I) -\alpha I
= I(\beta N - \alpha -\beta I)
= I(\beta N - \alpha)\left(1 - \frac{\beta I}{\beta N-\alpha}\right)
= rI\left(1 - \frac{I}{K}\right)
\end{equation*}

where \(r = \beta N-\alpha\) and \(K = \frac{r}{\beta} = \frac{\beta N - \alpha}{\beta}\).
By solving, we get

\begin{equation*}
I(t) = \frac{KBe^{rt}}{1+Be^{rt}}
\end{equation*}

where \(B = \frac{I(0)}{K-I(0)}\)

\begin{proof}[{Solution}]
\begin{equation*}
\begin{aligned}
\int_0^t r \ ds &= \int_0^t \frac{I'(s) \ ds}{I(s)\left(1-\frac{I(s)}{K}\right)}
\\\iff \quad
rt &= \int_0^t \frac{I'(s) }{I(s)} + \frac{I'(s)}{K-I(s)} \ dt
\\\iff\quad
rt &= \left.\ln I(s) - \ln (K-I(s))\right]_0^t
\\\iff \quad
rt &= \left.\ln \frac{I(s)}{K-I(s)}\right]_0^t
\\\iff \quad
rt &= \ln \frac{I(t)}{K-I(t)} - \ln \frac{I(0)}{K-I(0)}
\\\iff \quad
\frac{I(t)}{K-I(t)} &= \frac{I(0)}{K-I(0)}e^{rt}
\\\iff \quad
I(t) &= \frac{KBe^{rt}}{1+Be^{rt}}
\end{aligned}
\end{equation*}

where \(B = \frac{I(0)}{K-I(0)}\)
\end{proof}

\section{Limiting behaviour}
\label{develop--math6192:ROOT:page--epidemic-models/sis.adoc---limiting-behaviour}

The limiting behaviour of the system depends on
the sign of \(r = \beta N - \alpha\).

\begin{itemize}
\item If \(r > 0\), \(I_{\infty} = K\) and the disease is endemic
\item If \(r < 0\), \(I_\infty =0\) and the disease will eventually be eradicated
\end{itemize}

For this reason, we chose the basic reproduction number to be

\begin{equation*}
R_0 = \frac{\beta N}{\alpha}
\end{equation*}

Note that \(I_{\infty} = K\).

\section{Equilibrium points}
\label{develop--math6192:ROOT:page--epidemic-models/sis.adoc---equilibrium-points}

Note that there are two equilibrium points

\begin{equation*}
I^*_1 = 0, \text{ and } I^*_2 = K = \frac{\beta N-\alpha}{\beta}
\end{equation*}

As discussed before,

\begin{itemize}
\item If \(R_0 < 1\), \(I_1^*=0\) is globally stable since all solutions
    tend to it
\item If \(R_0 > 1\), \(I_2^*=K\) is globally stable since all solutions tend to it
\end{itemize}

\section{SIS model with saturating treatment}
\label{develop--math6192:ROOT:page--epidemic-models/sis.adoc---sis-model-with-saturating-treatment}

It may be more realistic that the recovery rate depends on the number of infected persons.
For example, the recovery rate may decrease as \(I\) increases due to limited health care resources.
So, suppose that the recovery rate is \(\frac{\alpha}{1+I(t)}\).
Then, our governing equations are

\begin{equation*}
\begin{aligned}
S' &= -\beta IS + \frac{\alpha}{1+I} I\\
I' &= \beta IS - \frac{\alpha}{1+I} I\\
\end{aligned}
\end{equation*}

and as before, we may ignore the first since \(S+I=N\).
So, we have that

\begin{equation*}
I' = \beta I(N-I) - \frac{\alpha}{1+I}I
\end{equation*}

We are interested in the equilibrium points which occur when

\begin{equation*}
0 = I'
= \beta I(N-I) - \frac{\alpha}{1+I} I
= \frac{\beta I}{1+I}\left[(1+I)(N-I) - \frac{\alpha}{\beta}\right]
\end{equation*}

There is a trivial equilibrium at \(I^*=0\) and possibly two more due to the quadratic term.

\begin{figure}[H]\centering
\includegraphics[width=0.9\linewidth]{images/develop-math6192/ROOT/epidemic-SIS-saturating-roots}
\caption{Illustration of \(I'\) vs \(I\)}
\end{figure}

Note that there are four possible cases

\begin{itemize}
\item There are three roots
    
    \begin{itemize}
    \item There are two negative roots. In this case \(I^*=0\) is the only valid equilibrium and it
        is globally stable
    \item There is a positive and negative root. Let the equilibria be \(I^*_1=0\) and \(I^*_2 > 0\).
        Note that \(I^*_1\) is an unstable equilibrium and \(I^*_2\) is globally stable
    \item There are two positive roots. Let the equilibria be \(I^*_1=0\) and \(0 < I^*_2 < I^*_3\).
        Note that \(I^*_1\) and \(I^*_3\) are both locally asymptotically stable
        and the \(I^*_2\) is unstable. This is referred to as \textbf{bistability} and the stable
        equilibrium obtained depends on the initial condition \(I(0)\).
    \end{itemize}
\item There is only one root and it is a valid equilibrium \(I^*=0\).
    In this case, it is globally stable
\end{itemize}
\end{document}
