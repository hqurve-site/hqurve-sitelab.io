\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Connectedness}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(X\) be a topological space.
A \emph{separation} of \(X\) is a pair of open sets \(U, V \subseteq X\)
with

\begin{equation*}
U \cup V = X,\quad
U\cap V = \varnothing,\quad
U, V\neq \varnothing
\end{equation*}

The topological space \(X\) is called \emph{connected} if there are no separations of \(X\).

\begin{proposition}[{}]
The topological space \(X\) is not connected iff there exists a non-trivial clopen set
(ie not \(\varnothing\) nor \(X\)).

\begin{proof}[{}]
First suppose that \(X\) is not connected. Then, there exists non-trivial disjoint open \(U, V\)
with \(U\cup V = X\). Since \(U = X - V\), \(U\) is closed and hence non-trivially clopen.

Conversely, suppose that \(U\) is a non-trivial clopen set. Then,
\(X-U\) is open and \(U, X-U\) forms a separation of \(X\).
\end{proof}
\end{proposition}

For any given non-empty subset \(Y \subseteq X\), we say that \(Y\) is \emph{connected} if it is connected in
the subspace topology.
Since the subspace topology on \(Y \subseteq Z \subseteq X\) is the same as the subspace topology
on \(Y \subseteq X\), there is no ambiguity when we say that a set \(Y\) is connected with respect to
the topology on \(X\).
We often use the following result to determine whether we have a separation on \(Y\).

\begin{proposition}[{}]
Let \(Y \subseteq X\). Then, two open sets \(U,V \subseteq X\) form a separation on \(Y\) if

\begin{itemize}
\item \(U \cap Y \neq \varnothing\) and \(V\cap Y \neq \varnothing\)
\item \(Y \subseteq U\cap V\)
\item \(Y \cap U \cap V = \varnothing\)
\end{itemize}

This follows directly from the definition of open in the subspace topology.
Also note that \(U\cap V\) may be non-empty, but the elements must exist outside of \(Y\).
\end{proposition}

We also have the following lemma

\begin{lemma}[{}]
Suppose \(Y \subseteq X\) and \(A, B \subseteq Y\) (not necessarily open) such that
\(A\cap B = \varnothing\), \(A, B \neq \varnothing\) and \(A \cup B = Y\),
then the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(A, B\) give a separation of \(Y\)
\item \(A\) contains no limit point of \(B\) and \(B\) contains no limit point of \(A\)
\end{enumerate}

\begin{proof}[{}]
First suppose that \(A\) and \(B\) give a separation of \(Y\).
Then, both sets are closed in \(Y\) and hence

\begin{equation*}
\limit_Y(A)\cap B \subseteq \closure_Y(A)\cap B = A \cap B = \varnothing
\end{equation*}

and likewise \(\limit(B) \cap A = \varnothing\).

Conversely, suppose that the second statement is true. Then,

\begin{equation*}
B \cap \closure_Y(A) = (B\cap A) \cup (B\cap \limit_Y(A)) = \varnothing
\implies \closure_Y(A) \subseteq Y - B = A \implies A =\closure_Y(A)
\end{equation*}

Therefore \(A\) is closed  in \(Y\) and likewise \(B\) is closed in \(Y\).
Therefore both \(A\) and \(B\) are open in \(Y\) and we have a separation.
\end{proof}
\end{lemma}

\begin{lemma}[{Connected subsets in separations}]
Suppose that \(U, V\) is a separation of \(X\)
and \(Y \subseteq X\) is connected.
Then either \(Y \subseteq U\) or \(Y \subseteq V\)

\begin{proof}[{}]
BWOC, suppose that \(U\cap Y \neq \varnothing\) and \(V\cap Y \neq \varnothing\).
Then, \(U, V\) form a separation on \(Y\) as well.
This is a contradiction since \(Y\) is connected.
Therefore either \(U\cap Y = \varnothing\) or \(V\cap Y = \varnothing\).
Both cant be empty since \(Y \subseteq U\cup V = X\).
Therefore either

\begin{itemize}
\item \(U\cap Y \neq \varnothing\) and hence \(Y\subseteq U\)
\item \(V\cap Y \neq \varnothing\) and hence \(Y\subseteq V\)
\end{itemize}

as desired.
\end{proof}
\end{lemma}

\begin{theorem}[{}]
Let \(A\) be a connected subspace of \(X\).
If \(A \subseteq B \subseteq \closure(A)\) then, \(B\) is also connected.

\begin{admonition-tip}[{}]
This may be restated as follows. A connected set with some or all of its limit points is also connected.
\end{admonition-tip}

\begin{proof}[{}]
Suppose that we have two open sets \(C, D \subseteq B\) such that
\(C\cup D = B\), \(C \cap D = \varnothing\).
Then, from the above lemma, either \(A \subseteq C\) or \(A\subseteq D\).
WLOG, suppose that \(A\subseteq C\). Then

\begin{itemize}
\item \(B\cap \limit(A)\cap D \subseteq B\cap \limit(C) \cap D = \varnothing\)
\item \(B\cap A \cap D \subseteq B\cap C\cap D = \varnothing\).
\end{itemize}

Then, by taking the union of the above two sets, we get that

\begin{equation*}
\varnothing = B\cap (A \cup \limit(A)) \cap D = B\cap \closure(A) \cap D = B\cap D
\end{equation*}

which implies that \(D =\varnothing\).
Therefore, there is no separation on \(B\) and \(B\) is connected as desired.
\end{proof}
\end{theorem}

\section{Connected components}
\label{develop--math6620:ROOT:page--connected.adoc---connected-components}

Consider arbitrary \(x \in X\), then we define the \emph{connected component}
of \(x\) to be

\begin{equation*}
C(x) = \bigcup_{C \text{ is connected and }x \in C} C
\end{equation*}

We already know that this set must be non-empty since the singleton \(\{x\}\)
is connected. However, we are yet to show that \(C(x)\) is itself connected.
We use the following theorem to prove this result.

\begin{theorem}[{Unions of connected sets}]
Let \(\{U_\alpha\}_{\alpha \in I}\) be a non-empty collection of connected subsets of \(X\).
Suppose that there exists \(x \in X\) such that \(x\in U_\alpha\) for each \(\alpha \in I\).
Then

\begin{equation*}
\bigcup_{\alpha \in I}U_\alpha
\end{equation*}

is connected.

\begin{proof}[{}]
Let \(U = \bigcup_{\alpha \in I}U_\alpha\) and suppose BWOC that there is a separation
\(C, D \subseteq U\).
Then, from the previous lemma, for each \(\alpha \in I\), either
\(U_\alpha \subseteq C\) or \(U_\alpha \subseteq D\).
If there exists \(\alpha_1, \alpha_2 \in I\) such that \(U_{\alpha_1} \subseteq C\)
and \(U_{\alpha_2} \subseteq D\), this implies that \(x \in C \cap D = \varnothing\).
Therefore \(U \subseteq C\) or \(U \subseteq D\) which implies that
\(D = \varnothing\) or \(C = \varnothing\) respectively.
In both cases, we get a separation and hence \(U\) is connected.
\end{proof}
\end{theorem}

Additionally, the connected components form a partition on \(X\).
First notice that if \(C(x) \cap C(y) \neq \varnothing\), both sets
are the same.
We use the previous result to prove that \(C(x) \cup C(y)\) is connected.
Since the union contains \(x\), \(C(x) \cup C(y) \subseteq C(x)\) and likewise \(C(x) \cup C(y) \subseteq C(y)\).
Therefore, the two connected components must be equal.
This result is summarized nicely in the following theorem

\begin{theorem}[{}]
Let \(X\) be a topological space. Then

\begin{enumerate}[label=\arabic*)]
\item \(X\) is the disjoint union of its connected components
\item Each connected component is connected
\item If \(Y \subseteq X\) is connected. Then, \(Y \subseteq C(x)\) for some \(x \in X\)
\end{enumerate}

\begin{proof}[{}]
We have already proven the second result.
For the first result follows from the fact that the following forms an equivalence relation

\begin{equation*}
x\sim y \iff \exists \text{connected C}: x,y \in C
\end{equation*}

since

\begin{itemize}
\item \(x\sim x\) as \(\{x\}\) is connected
\item \(x\sim y\) implies \(y\sim x\) trivially
\item \(x\sim y\) and \(y \sim z\), then \(C(x) = C(y) = C(z)\) and hence \(x,z \in C(x)\) and \(x\sim z\).
\end{itemize}

Therefore the equivalence classes are the connected components.

Finally, for the last result. Consider the set

\begin{equation*}
\bigcup_{y\in Y} C(y)
\end{equation*}

This set is connected since for each \(y\), \(Y \subseteq C(y)\) and hence there exists \(x \in Y (\subseteq X)\) such
that \(x \in C(y)\) for all \(y \in Y\).
\end{proof}
\end{theorem}

\begin{admonition-tip}[{}]
A topological space in which the connected components are singletons is called \emph{totally disconnected}.
\end{admonition-tip}

\section{Path Connected}
\label{develop--math6620:ROOT:page--connected.adoc---path-connected}

Let \(X\) be a topological space and \(a,b \in X\). Then, we define a \emph{path} from \(a\) to \(b\) to be
a continuous function \(f: [0,1] \to X\) such that \(f(0)=a\)
and \(f(1)=b\).

A space \(X\) is called \emph{path connected} if for any \(a,b \in X\),
there exists a path from \(a\) to \(b\) which is contained in \(X\).

\begin{proposition}[{}]
Let \(X\) be a path-connected topological space. Then, \(X\) is connected.

\begin{proof}[{}]
Suppose \(X\) is not connected and let \(U, V\) be a separation of \(X\).
Fix some \(a \in U\) and \(b \in V\).
Since \(X\) is path connected, there exists a continuous function \(f:[0,1] \to X\)
with \(f(0)=a\) and \(f(1)=b\).

Consider \(f^{-1}(U)\).
Let

\begin{equation*}
A = \{t \in [0,1] \ | \ [0,t] \subseteq f^{-1}(U)\}
\end{equation*}

Let \(\alpha = \sup(A)\) which exists since \(0\in A\).
Since \(\alpha\) is a limit point of \(f^{-1}(U)\), we have that \(\alpha \in f^{-1}(U)\)
since \(U\) is clopen.
If \(\alpha < 1\), this implies there exists \(\delta > 0\) such that
\([\alpha, \alpha+\delta)\subseteq f^{-1}(U)\) since \(U\) is clopen.
However, this invalidates the fact that \(\alpha\) is the supremum.
Therefore, \(\alpha=1\).
But this implies that \([0,1]= f^{-1}(U)\) and \(V\cap f([0,1]) = \varnothing\).
Therefore, no separation \(U,V\) exists and \(X\) is connected.
\end{proof}
\end{proposition}

Likewise to usual connectedness, we may define path-components.
However, the definition is slightly different.
We define an equivalence relation \(\sim_p\) such that \(a\sim_p b\) iff there
exists a path from \(a\) to \(b\).
The equivalence classes of this relation are called \emph{path-components}.

\begin{theorem}[{}]
Let \(A\subseteq X\) be path connected.
Then, \(A\) is contained in a path component.

\begin{proof}[{}]
Let \(\sim_p^X\) be the equivalence relation on \(X\)
and \(\sim_p^A\) be the equivalence relation on \(A\).
Then, since continuous functions \([0,1] \to A\)
are also continuous when considered as \([0,1]\to X\),
we have that \(a\sim_p^A b \implies a\sim_p^X b\).

Now, focus on some fixed \(a_0 \in A\).
Then, for each \(a' \in A\),
\(a_0 \sim_p^X a'\) and \(A\)
is contained in the equivalence class of \(a_0\)
wrt \(\sim_p^X\).
\end{proof}
\end{theorem}

\section{Nice theorems}
\label{develop--math6620:ROOT:page--connected.adoc---nice-theorems}

\begin{theorem}[{Contoniuous images of connected sets are connected}]
Let \(f: X\to Y\) be continuous and \(X\) be connected.
Then \(f(X)\) is connected.

\begin{proof}[{}]
Suppose \(A, B\) form a separation on \(f(X)\). Then
\(f^{-1}(A)\) and \(f^{-1}(B)\) are both clopen and

\begin{itemize}
\item \(f^{-1}(A) \cup f^{-1}(B) = X\)
\item \(f^{-1}(A) \cap f^{-1}(B) = \varnothing\)
\item \(f^{-1}(A), f^{-1}(B) \neq \varnothing\)
\end{itemize}

Therefore we have a separation on \(X\). This is a contradiction.
\end{proof}
\end{theorem}

\subsection{Locally Connected}
\label{develop--math6620:ROOT:page--connected.adoc---locally-connected}

Let \(X\) be a topological space. Then,
we say that \(X\) is \emph{locally-connected}
at \(x \in X\) if for each open neighbourhood \(U\)
of \(x\), there exists open \(V\)
such that \(x\in V\subseteq U\) and \(V\) is connected.
If we do not specify \(x\), we mean that \(X\) is locally
connected for each \(x \in X\).

We may analogously define \emph{locally path-connected}.

\begin{admonition-tip}[{}]
We may define `locally P' similarly for any adjective \(P\).
\end{admonition-tip}

\begin{admonition-caution}[{}]
It is possible for a set to be connected but not locally connected.
For example, consider the \myautoref[{Topologist's sine curve}]{develop--math6620:ROOT:page--connected.adoc--sine-curve}.
\end{admonition-caution}

\begin{theorem}[{}]
Let \(X\) be a topological space
Then, the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(X\) is locally connected.
\item For each open \(U \subseteq X\), each connected component of \(U\)
    is open.
\end{enumerate}

Also, the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(X\) is locally path-connected.
\item For each open \(U \subseteq X\), each path-connected component of \(U\)
    is open.
\end{enumerate}

\begin{proof}[{}]
\begin{admonition-note}[{}]
We only prove the result for `connected'; not `path-connected'.
However, the proof is identical.
\end{admonition-note}

\((1) \implies (2)\).
Consider \(U \subseteq X\) and connected component \(C\) of \(U\).
For each \(x \in C\), define \(V_x\)
to be such that \(V_x\) is open and connected and \(x \in V_x\subseteq U\).
Then,

\begin{equation*}
C = \cup_{x\in C} V_x
\end{equation*}

Therefore \(C\) is open in \(U\) (and hence open in \(X\)).
Since the choice of \(U\) and \(C\) were arbitrary, that each
connected component of each open set is open.

Next, \((2)\implies (1)\).
Focus on some \(x \in X\) and open \(U\) containing \(x\).
Let \(C\) be the connected component of \(x\) containing \(U\).
Since \(C\) is open, we can just choose \(V=C\)
and \(X\) is locally connected at \(x\).
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(X\) be a topological space.
Then

\begin{enumerate}[label=\arabic*)]
\item Path connected components are contained in connected components.
\item If \(X\) is locally path connected, then the connected components are path connected.
\end{enumerate}

\begin{proof}[{}]
The first statement is evident since each path connected component \(C\), is path connected
and hence connected (from a previous proposition). Therefore, \(C\) must be contained
in a connected component of \(X\).

For the second statement, focus on connected component \(C\).
Since locally path-connected implies locally connected, we have that \(C\) is open.
Now, focus on some fixed \(p_0 \in C\) and let \(P \subseteq C\) be its path-connected component.
If \(P \neq C\), let \(\{P_\alpha\}\) be the set of other path connected components
contained in \(C\). Since \(C\) is open, \(P\) and each of the \(P_\alpha\) are
open and \(P'=\bigcup_{\alpha}P_\alpha\) is open.
However, this causes \(P, P'\) to form a separation on \(C\).
This contradicts the fact that \(C\) is connected.
Therefore, our assumption that \(P\neq C\) is wrong and \(C\) is path connected.
\end{proof}
\end{theorem}

\section{Real numbers}
\label{develop--math6620:ROOT:page--connected.adoc---real-numbers}

One motivator for connectedness are intervals in real numbers.
Recall that an interval in \(\mathbb{R}\) is a set \(I\) such that

\begin{equation*}
\forall x,y \in I: \forall z \in \mathbb{R}: x < z < y \implies z \in I
\end{equation*}

This was also a previous definition of connectedness in previous courses.
In the real numbers we would show that intervals are the only sets which are topologically connected.

\begin{lemma}[{}]
The only sets in \(\mathbb{R}\) which are clopen are \(\varnothing\) and \(\mathbb{R}\).

\begin{proof}[{}]
Suppose \(A \subseteq \mathbb{R}\) is a non-empty clopen set.
Consider the set

\begin{equation*}
\mathcal{I} = \{(a,b) \ | \ (a,b) \subseteq A\}
\end{equation*}

Since \(A\) is non-empty and open, \(\mathcal{I}\) is non-empty.
So for some fixed \(a\), consider \(\{b \ | \ (a,b) \in \mathcal{I}\}\).
Again, this set is non-empty.
Suppose that this set is bounded above and has supremum \(b^*\).
Then, \(b^* \in \limit(A)\) and \(b^* \in A\). Since \(A\) is open
there exists \((x,y) \subseteq A\) such that \(b^* \in (x,y)\). However, this contradicts the fact
that \(b^*\) is the supremum of \(\{b \ | \ (a,b) \in \mathcal{I}\}\) since \(y > b^*\)
and \((a, y) \subseteq A\). Therefore, \(\{b \ | \ (a,b) \in \mathcal{I}\}\) is
not bounded above and \((a, \infty) \subseteq A\).

We may similarly prove that \((-\infty, b) \subseteq A\) for some \(b > a\).
Therefore \(A = \mathbb{R}\) and we are done.
\end{proof}
\end{lemma}

\begin{corollary}[{}]
The set \(\mathbb{R}\) is connected.
\end{corollary}

\begin{lemma}[{Bounded intervals are connected}]
The interval \((a,b) \subseteq\mathbb{R}\) is connected in \(\mathbb{R}\).

\begin{admonition-tip}[{}]
As a corollary, closed and half open intervals are also open since they lie between \((a,b)\)
and \(\closure((a,b)) = [a,b]\).
\end{admonition-tip}

\begin{proof}[{}]
Let \(U \cap (a,b)\) and \(V\cap (a,b)\) form a separation on \((a,b)\).
Since \((a,b)\) is open in \(\mathbb{R}\), we could instead suppose that \(U,V\subseteq (a,b)\)
for convenience.
WLOG, consider arbitrary \(x  < y\) with \(x \in U\) and \(y \in V\).
Now, consider the \(x^* = \sup\{z \in (a,b): [x,z] \subseteq U\}\).
This supremum exists since \([x,x] \subseteq U\) and \([x,z] \subseteq U\) implies that
\(z < b\).

Note that \(x^* \in U\) since the supremum is a limit point and \(U\) is closed in \((a,b)\).
However, since \(U\) is open in \(\mathbb{R}\), there exists \(\varepsilon > 0\) such that
\((x^* - \varepsilon, x^* + \varepsilon) \subseteq U\). This invalidates the fact that \(x^*\)
is the supremum since \(\left[x, x^* + \frac{\varepsilon}{2}\right] \subseteq U\).
Therefore we have contradicted the assumption that a separation exists and hence \((a,b)\) is connected.
\end{proof}
\end{lemma}

\begin{theorem}[{}]
Let \(I \subseteq \mathbb{R}\).
Then \(I\) is an connected iff \(I\) is an interval.

\begin{proof}[{}]
First, suppose that \(I\) is an interval.
We aim to show that the connected component of each \(x\in I\), \(C(x)\) is just \(I\).
Focus on some fixed \(x \in I\) and consider \(C(x)\). If \(y \in I\) and

\begin{itemize}
\item \(y > x\), then \([x,y] \subseteq I\) is connected and hence \([x,y] \subseteq C(x)\)
\item \(y < x\), then \([y,x] \subseteq I\) is connected and hence \([x,y] \subseteq C(x)\)
\end{itemize}

Therefore \(I \subseteq C(x) = I\) and \(I\) has only one connected component and is hence connected.

Conversely, suppose that \(I\) is connected. Suppose there exists
\(x,y \in I\) and \(z \in \mathbb{R} - I\) with \(x < z < y\).
Then, \((-\infty, z), (z, \infty)\) forms a separation on \(I\).
Therefore \(I\) is an interval.
\end{proof}
\end{theorem}

\subsection{Common theorems}
\label{develop--math6620:ROOT:page--connected.adoc---common-theorems}

\begin{theorem}[{Intermediate value theorem}]
Let \(f: [a,b] \to \mathbb{R}\) be continuous. Then for any \(z\) between \(f(a)\)
and \(f(b)\), there exists \(c \in [a,b]\) with \(f(c) = z\).

\begin{proof}[{}]
Recall that the image of \(f\) would be connected. Since all connected sets in \(\mathbb{R}\)
are intervals, we have that \(z \in f([a,b])\) and there exists \(c \in [a,b]\)
with \(f(c) = z\) as desired.
\end{proof}
\end{theorem}

\section{Interesting examples}
\label{develop--math6620:ROOT:page--connected.adoc---interesting-examples}

\subsection{Asymptotic curve}
\label{develop--math6620:ROOT:page--connected.adoc---asymptotic-curve}

Consider the sets

\begin{equation*}
A_0 = \{(0,y) \ |  y \in \mathbb{R}\}
\quad\text{and}\quad
A_1 = \left\{\left(x, \frac{1}{x}\right) \ \middle| \ x \in \mathbb{R}\right\}
\end{equation*}

We claim that \(A_0 \cup A_1\) is not connected.

\begin{figure}[H]\centering
\includegraphics[width=0.4\linewidth]{images/develop-math6620/ROOT/connected-example-1}
\end{figure}

To show this we need only show that \(A_0 \cap \limit(A_1) = A_1\cap \limit(A_0) = \varnothing\).

Notice both \(A_0\) and \(A_1\) are closed since

\begin{itemize}
\item \(A_0 = \pi_1^{-1}(\{0\})\)
\item \(A_1 = A_2 \cap \left[0, \infty\right)^2\) with \(A_2 = (\times)^{-1}(\{1\}) = \{(x,y) \ | \ xy=1\}\)
\end{itemize}

Therefore we have that \(A_0 \cup A_1\) is not connected.

\subsection{Topologist's sine curve}
\label{develop--math6620:ROOT:page--connected.adoc--sine-curve}

Consider the sets

\begin{equation*}
B_0 = \{(0, y) \ | \ -1 \leq y \leq 1\}
\quad\text{and}\quad
B_1 = \left\{\left(x, \sin\frac{1}{x}\right) \ \middle| \ x > 0\right\}
\end{equation*}

We claim that \(B=B_0 \cup B_1\) is connected
but not path-connected nor locally connected.

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math6620/ROOT/connected-example-2}
\end{figure}

To see that \(B\) is connected.
Notice that \(B_0 \subseteq \limit(B_1)\) since

\begin{equation*}
(0,y) = \lim_{n\to \infty} \left(\frac{1}{\alpha +2\pi n}, \sin (\alpha + 2\pi n)\right)
\quad\text{where}\ \sin(\alpha) = y
\end{equation*}

for each \((0,y) \in B_0\). Therefore
\(B_1 \subseteq B \subseteq\closure(B_1)\)
and \(B_1\) is connected as it is the image of \((0, \infty)\) with the mapping
\(t\mapsto \left(t, \sin\frac{1}{t}\right)\).
Therefore \(B\) is connected.

\subsection{Not locally connected}
\label{develop--math6620:ROOT:page--connected.adoc---not-locally-connected}

Next, to see that \(B\) is not locally connected,
consider \(U=B\cap \left(\mathbb{R}\times \left(-\frac12, \frac12\right)\right)\).
It is sufficient to show that the connected components of \(U\) are not open.
In particular, we want to show that \(C = \{0\}\times\left(-\frac12, \frac12\right)\)
is a connected component which is not open in \(B_0 \cup B_1\).
We can already see that \(C\) is connected.
Consider the sets

\begin{equation*}
\begin{aligned}
C_n &= B \cap \left(\left(\frac{1}{(n+1)\pi + \frac{\pi}{2}}, \frac{1}{n\pi + \frac{\pi}{2}}\right)\times \left(-\frac12, \frac12\right)\right)
\quad\text{for } n=0,1\ldots
\\
C_{-1} &= B \cap \left(\left(\frac{1}{\frac{\pi}{2}}, \infty\right)\times \left(-\frac12, \frac12\right)\right)
\end{aligned}
\end{equation*}

Then, each of these are also connected and \(U=C \cup C_{-1} \cup \bigcup_{n=0}^\infty C_n\).
Since each of the \(C_i\) are disconnected from each other and \(C\), they form connected components
of \(U\). In particular \(C\) is a connected component of \(U\).
Also, to see that \(C\) is not open, we would show that \(U-C\) is not closed.
Notice that \((0,0) \in \limit(U-C)\) since \((0,0) = \lim_{n\to\infty} \left(\frac{1}{n\pi}, \sin(n\pi)\right)\).
Therefore, \(U-C\) is not closed and \(C\) is not open.
Since we have a connected component of \(U\) which is not open, \(B\)
is not locally connected.

\subsection{Not path connected}
\label{develop--math6620:ROOT:page--connected.adoc---not-path-connected}

Now, we want to show that \(B\) is not path connected.
Consider a path \(p: [0,1] \to B\) with \(p(0) \in B_0\).
We want to show that \(p([0,1]) \subseteq B_0\).
Consider the set

\begin{equation*}
I=\{t \in [0,1] \ | \ p([0,t]) \subseteq B_0\}
\end{equation*}

and let \(t_0\) be the supremum of \(I\).
BWOC suppose that \(t_0 < 1\).
Since \(t_0\) is a limit point of \(B_0\) which is closed, \(t_0\in I\).
Let \(p(t_0) = (0, y_0)\) and
\(U = B\cap \left(\mathbb{R}\times \left(y_0 - \frac14, y_0 + \frac14\right)\right)\).
Since \(p\) is continuous, there exists \(\delta > 0\)
such that \(p((t_0-\delta, t_0+\delta)) \subseteq U\).
Since \(t_0 =\sup I\), there exists \(t_1 \in (t_0, t_0 + \delta)\)
such that \(t_1 \in B_1\).
Let \(p(t_1) = (\frac{1}{x_1}, \sin(x_1))\).
We now aim to show that \(p([t_0, t_1])\) is not connected

\begin{enumerate}[label=\arabic*)]
\item If \(y_0 \geq 0\), let \(n\) be such that \(2\pi n + \frac{3\pi}{2} > x_1\).
    Then, \(B \cap \left(\left(-\infty, \frac{1}{2\pi n + \frac{3\pi}{2}}\right)\times \mathbb{R}\right)\)
    and \(B \cap \left(\left(\frac{1}{2\pi n + \frac{3\pi}{2}}, \infty\right)\times \mathbb{R}\right)\)
    form a separation for \(p([t_0, t_1])\) since \(p(t_0)\) is in the first but \(p(t_1)\) is in the second.
\item If \(y_0 \leq 0\), let \(n\) be such that \(2\pi n + \frac{\pi}{2} > x_1\).
    Then, \(B \cap \left(\left(-\infty, \frac{1}{2\pi n + \frac{\pi}{2}}\right)\times \mathbb{R}\right)\)
    and \(B \cap \left(\left(\frac{1}{2\pi n + \frac{\pi}{2}}, \infty\right)\times \mathbb{R}\right)\)
    form a separation for \(p([t_0, t_1])\) since \(p(t_0)\) is in the first but \(p(t_1)\) is in the second.
\end{enumerate}

This is a contradiction to the assumption that \(t_0 < 1\).
Therefore any paths originating in \(B_0\) must remain in \(B_0\)
and there is no path from \(B_0\) to \(B_1\),
\end{document}
