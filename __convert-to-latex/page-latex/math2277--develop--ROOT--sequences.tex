\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Sequences}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A sequence \(\{x_n\}\) of real numbers is simply a function
\(f: \mathbb{N} \to \mathbb{R}\) where
\(f(n) = x_n\).

\section{Convergence}
\label{develop--math2277:ROOT:page--sequences.adoc---convergence}

Let \(\{x_n\}\) be a sequence. Then \(\{x_n\}\)
\emph{converges} converges iff \(\exists x \in \mathbb{N}\) such
that

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}:
    \forall n \in \mathbb{N}: n > N \implies |x_n - x| < \varepsilon
\end{equation*}

Then, we call \(x\) the \emph{limit of the sequence}
\(\{x_n\}\) and write

\begin{equation*}
\lim_{n \to \infty} x_n = x
    \text{ or }
    x_n \to x \text{ as }  n \to \infty
\end{equation*}

If \(\{x_n\}\) does not converge, we say that it \emph{diverges}

\subsection{Note:}
\label{develop--math2277:ROOT:page--sequences.adoc---note}

many of the proofs of properties that are also exhibited by function
limits are very similar to those for sequences. Hence, most proofs would
be omitted due to their similarity unless otherwise stated.

\subsection{Uniqueness of the limit}
\label{develop--math2277:ROOT:page--sequences.adoc---uniqueness-of-the-limit}

Let \(\{x_n\}\) be a sequence, then, if it converges, its
limit is unique.

\subsection{Nice Properties}
\label{develop--math2277:ROOT:page--sequences.adoc---nice-properties}

Let \(\{x_n\}\) and \(\{y_n\}\) be two convergent
sequences such that

\begin{equation*}
\lim_{n\to \infty}x_n = x
    \text{ and }
    \lim_{n \to \infty} y_n = y
\end{equation*}

\begin{enumerate}[label=\roman*)]
\item \(\lim_{n \to \infty} (x_n + y_n) = x + y\)
\item \(\lim_{n \to \infty} (x_n y_n) = x y\)
\item \(\lim_{n \to \infty} (kx_n) = kx\) where
    \(k \in \mathbb{R}\)
\item \(\lim_{n \to \infty} \frac{x_{n}}{y_{n}} = \frac{x}{y}\) if
    \(y \neq 0\).
\end{enumerate}

\subsection{Boundedness}
\label{develop--math2277:ROOT:page--sequences.adoc---boundedness}

Let \(\{x_n\}\) be a bounded sequence, then
\(\{x_n\}\) is bounded.

\begin{example}[{Proof}]
This proof is basically identical to that provided by Dr Job.

Firstly, take \(\varepsilon = 1\) (any number could work
really). Then, \(\exists N \in \mathbb{N}\) such that

\begin{equation*}
\forall n \in \mathbb{N}: n > N \implies |x_n - x| < \varepsilon
        \implies |x_n| = |x_n -x + x| \leq |x_n -x | + |x| < |x| + \varepsilon
\end{equation*}

Then, let \(M\) be defined as

\begin{equation*}
M = \max \{|x_1|, |x_2|, \ldots, |x_N|, |x|+\varepsilon\}
\end{equation*}

Then, \(M > 0\) and exists since the set is finite and

\begin{equation*}
\forall n \in \mathbb{N}: |x_n| \leq M
\end{equation*}

and hence \(\{x_n\}\) is bounded.

 ◻
\end{example}

\subsection{Order preservation}
\label{develop--math2277:ROOT:page--sequences.adoc---order-preservation}

Let \(\{x_n\}\) and \(\{y_n\}\) be two convergent
sequences with limits \(x\) and \(y\). Then if
\(\exists N \in \mathbb{N}\) such that
\(\forall n > N\): \(x_n \leq y_n\),
\(x \leq y\). As a corollary, the squeeze/sandwich theorem
applies for sequences as well.

\section{Sequential definition of continuity}
\label{develop--math2277:ROOT:page--sequences.adoc---sequential-definition-of-continuity}

Let \(A \subseteq \mathbb{R}\) and
\(f \colon A \to \mathbb{R}\) and
\(a \in A \cap A'\). Then, the following statements are
equivalent

\begin{enumerate}[label=\arabic*)]
\item \(f\) is continuous at \(a\).
\item if \(\{x_n\}\) is a sequence which converge to
    \(a\), then \(\{f(x_n)\}\) is a sequence that
    converges to \(f(a)\).
\end{enumerate}

\begin{example}[{Proof}]
Firstly, let \(f\) be continuous at \(a\) and
\(\{x_n\}\) be a sequence that converges to \(a\)
and consider arbitary \(\varepsilon > 0\). Then by continuity
\(\exists \delta > 0\) such that

\begin{equation*}
\forall x \in A: |x - a| < \delta \implies |f(x) - f(a) | <\varepsilon
\end{equation*}

Next, since \(\{x_n\}\) converges to \(a\),
\(\exists N \in \mathbb{N}\) such that

\begin{equation*}
\forall n \in \mathbb{N}: n > N \implies|x_n - a| < \delta \implies |f(x_n) - f(a)| < \varepsilon
\end{equation*}

Therefore, \(\{f(x_n)\}\) converges to \(f(a)\).

Conversely, suppose that \(f\) is not continuous at
\(a\), we want to show that \(\exists\) a sequence
\(\{x_n\}\) such that \(\{f(x_n)\}\) does not
converge to \(f(a)\). We will construct such a sequence. Note
that since \(f\) is discontinuous at \(a\),

\begin{equation*}
\exists \varepsilon > 0: \forall \delta > 0: \exists x \in A: |x -a| < \delta \ \land\ |f(x) - f(a)| \geq \varepsilon
\end{equation*}

We define
\(x_n \in \{x: |x - a| < \tfrac{1}{n} \ \land\ |f(x) - f(a)| \geq \varepsilon\}\).
Then, notice that such a set is non-empty and by the axiom of choice we
can generate a sequence of \(\{x_n\}\). Notice that such a
sequence would necessarily converge to \(a\) since the
\(x_n\) is contained in smaller and smaller intervals.
However, notice that \(\{f(x_n)\}\) does not converge to
\(f(a)\), so we are done.

 ◻
\end{example}

\subsection{Note:}
\label{develop--math2277:ROOT:page--sequences.adoc---note-2}

In hindsite, there was no need for me to use the axiom of choice since
all that was required was the existence of such a set and not the actual
construction. I should have just considered an arbitrary such sequence
and shown the necessary properties. Also, since the set of such
sequences is non-empty we would have satisfied existence.

\section{Monotone Convergence Theorem}
\label{develop--math2277:ROOT:page--sequences.adoc---monotone-convergence-theorem}

Let \(\{x_n\}\) be a bounded monotone sequence, then
\(\{x_n\}\) converges.

\begin{example}[{Proof}]
This proof is basically the same as that in the notes. WLOG, we suppose
that \(\{x_n\}\) is monotonically increasing. And we let
\(E = \{x_n: n \in \mathbb{N}\}\) then we will show that
\(\lim_{n \to \infty} x_n = \sup(E)\).

Let \(\varepsilon > 0\) and by the characterization of
supremum, \(\exists x_N \in E\) such that
\(x_N > \sup(E) - \varepsilon\). Then, since the sequence is
monotonically increasing

\begin{equation*}
\forall n \in \mathbb{N}: n > N
        \implies \sup(E) - \varepsilon < x_N \leq x_n \leq \sup(E)
        \implies |x_n - \sup(E)| < \varepsilon
\end{equation*}

Then, we are done

 ◻
\end{example}

\section{Cauchy Sequences}
\label{develop--math2277:ROOT:page--sequences.adoc---cauchy-sequences}

Let \(\{x_n\}\) be a sequence, then \(\{x_n\}\) is
called a \emph{Cauchy sequence} iff

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}:
    \forall m, n \in \mathbb{N}:
    m, n > N \implies |x_n - x_m| < \varepsilon
\end{equation*}

\subsection{Cauchy convergence criterion}
\label{develop--math2277:ROOT:page--sequences.adoc---cauchy-convergence-criterion}

Let \(\{x_n\}\) be a real sequence, then

\begin{equation*}
\{x_n\} \text{ is convergent}
    \iff
    \{x_n\} \text{ is Cauchy}
\end{equation*}

\section{Subsequences}
\label{develop--math2277:ROOT:page--sequences.adoc---subsequences}

Let \(\{x_n\}\) be a real sequence, and \(\{n_k\}\)
be a sequence of strictly increasing natural numbers. Then
\(\{x_{n_k}\}\) is called a \emph{subsequence} of
\(\{x_n\}\).

\subsection{Convergence of subsequences}
\label{develop--math2277:ROOT:page--sequences.adoc---convergence-of-subsequences}

Let \(\{x_n\}\) be a real sequence, then \(\{x_n\}\)
converges iff all of its subsequences converge to the same value.

\subsection{Bolzano-Weierstrass Theorem for sequences}
\label{develop--math2277:ROOT:page--sequences.adoc---bolzano-weierstrass-theorem-for-sequences}

Let \(\{x_n\}\) be a bounded sequence, then
\(\{x_n\}\) has at least one convergent subsequence.

\begin{admonition-note}[{}]
This theorem is equivalent to the Bolzano-Weierstrass theorem for limit
points.
\end{admonition-note}

\section{Sequential Compactness}
\label{develop--math2277:ROOT:page--sequences.adoc---sequential-compactness}

Let \(A \subseteq \mathbb{R}\), then \(A\) is called
\emph{sequentially compact} iff every sequence in \(A\) has a
convergent subsequence whose limit belongs to \(A\).

\section{Useful result}
\label{develop--math2277:ROOT:page--sequences.adoc---useful-result}

Let \(A \subseteq \mathbb{R}\), then \(A\) is
sequentially compact iff \(A\) is closed and bounded.
\end{document}
