\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Multivariate Normal}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
\begin{admonition-tip}[{}]
The multivariate normal is also discussed in \myautoref[{}]{develop--math3278:multivariate-normal:page--index.adoc}.
\end{admonition-tip}

Let \(\vec{X} = (X_1, \ldots X_p)\) be a vector of random variables
with mean \(\vec{\mu}\) and covariance matrix \(\mat{\Sigma}\).
Then, the pdf of \(\vec{X}\) is

\begin{equation*}
f(\vec{x}) = \frac{1}{\sqrt{(2\pi)^p |\mat{\Sigma}|}} \exp\left\{
-\frac{(\vec{x} - \vec{\mu})^T \mat{\Sigma}^{-1}(\vec{x} - \vec{\mu})}{2}
\right\}
\end{equation*}

and we denote

\begin{equation*}
\vec{X} \sim MVN(\vec{\mu}, \mat{\Sigma})
\end{equation*}

\section{Bivariate normal}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---bivariate-normal}

The multivariate normal distribution can be simplified for the special case of \(p=2\).

Let \(\vec{X} = (X,Y)\), \(\vec{\mu} = (\mu_X, \mu_Y)\) and
\(\mat{\Sigma} = \begin{bmatrix}
\sigma_X^2 & \rho\sigma_X\sigma_Y\\
\rho\sigma_X\sigma_Y & \sigma_Y^2\\
\end{bmatrix}\)
where \(\rho\) is the correlation between \(X\) and \(Y\).
Then, the pdf of the bivariate normal distribution is

\begin{equation*}
f(x,y) = \frac{1}{2\pi\sigma_X\sigma_Y\sqrt{1-\rho^2}}
    \exp\left\{-\frac{
        \left(\frac{x-\mu_X}{\sigma_X}\right)^2
        - 2\rho\left(\frac{x-\mu_X}{\sigma_X}\right)\left(\frac{y-\mu_Y}{\sigma_Y}\right)
        + \left(\frac{y-\mu_Y}{\sigma_Y}\right)^2
        }{2(1-\rho^2)}
    \right\}
\end{equation*}

\section{Properties}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---properties}

\subsection{Marginal}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---marginal}

The marginal distribution of a MVN is obtained by extracting the relevant entries
in \(\mat{\mu}\) and \(\mat{\Sigma}\).

\begin{example}[{Marginal distribution of MVN}]
Consider

\begin{equation*}
\begin{bmatrix}
X_1\\X_2\\X_3
\end{bmatrix}
\sim
MVN\left(
\begin{bmatrix}
2\\-3\\4
\end{bmatrix},
\
\begin{bmatrix}
1 & 1 & 7\\
1 & 3 & 2\\
7 & 2 & 2
\end{bmatrix}
\right)
\end{equation*}

Then,

\begin{itemize}
\item The marginal distribution of \(X_2\) is \(N(-3, 3)\)
\item The marginal distribution of \((X_3,X_1)\) is
    \(MVN\left(\begin{bmatrix}4 \\ 2\end{bmatrix},\ \begin{bmatrix}2 & 7 \\ 7 & 1\end{bmatrix}\right)\)
\end{itemize}
\end{example}

\subsection{Independence}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---independence}

The multivariate normal distribution has the special property that all uncorrelated random variables
are independent.
Therefore, checking for independence is as simple as examining the relevant entries in the covariance matrix \(\Sigma\).

\begin{example}[{Independence of MVN}]
Consider

\begin{equation*}
\begin{bmatrix}
X_1\\X_2\\X_3
\end{bmatrix}
\sim
MVN\left(
\begin{bmatrix}
2\\-3\\4
\end{bmatrix},
\
\begin{bmatrix}
1 & 0 & 7\\
0 & 3 & 0\\
7 & 0 & 2
\end{bmatrix}
\right)
\end{equation*}

Then \(X_2\) is independent of \((X_1, X_3)\)
since \(\sigma_{12} = \sigma_{32} = 0\)
\end{example}

\subsection{Conditional distributions}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---conditional-distributions}

Let

\begin{equation*}
\begin{bmatrix}
\vec{X}\\
\vec{Y}
\end{bmatrix}
\sim MVN\left(
\begin{bmatrix}
\vec{\mu}_X\\
\vec{\mu}_Y
\end{bmatrix}
,\
\begin{bmatrix}
\mat{\Sigma}_{XX} & \mat{\Sigma}_{XY}\\
\mat{\Sigma}_{YX} & \mat{\Sigma}_{YY}\\
\end{bmatrix}
\right)
\end{equation*}

Then, the conditional distribution of \(X\) given \(Y\)
is multivariate normal with

\begin{equation*}
\begin{aligned}
E[\vec{X}|\vec{Y}=\vec{y}]
&=
    \vec{\mu}_X + \mat{\Sigma}_{XY}\mat{\Sigma}_{YY}^{-1}
    (\vec{y} - \vec{\mu}_Y)
\\
Var[\vec{X}|\vec{Y}=\vec{y}]
&= \mat{\Sigma}_{XX} - \mat{\Sigma}_{XY}\mat{\Sigma}_{YY}^{-1}\mat{\Sigma}_{YX}
\end{aligned}
\end{equation*}

\begin{admonition-caution}[{}]
In order to apply this result, please first ensure the random
vector is laid out as required.
\end{admonition-caution}

\begin{example}[{Conditional distribution}]
Let

\begin{equation*}
\begin{bmatrix}X_1 \\ X_2\\X_3\end{bmatrix}
\sim MVN\left(
    \begin{bmatrix}-3\\ 1\\ 4\end{bmatrix},
    \
    \begin{bmatrix}
    1 & -2 & 0\\
    -2 & 5 & 0\\
    0 & 0 & 2
    \end{bmatrix}
\right)
\end{equation*}

We desire to find the distribution of \(X_2\) given that \(X_1=1\)
and \(X_3=3\).
First, we lay out the vectors as required with the conditionals below

\begin{equation*}
\begin{bmatrix}X_2\\X_1 \\ X_3\end{bmatrix}
\sim MVN\left(
    \begin{bmatrix}1 \\-3\\ 4\end{bmatrix},
    \
     \begin{bmatrix}
    5 & -2 & 0\\
    -2 & 1 & 0\\
    0 & 0 & 2
    \end{bmatrix}
\right)
\end{equation*}

Now, we may apply the result.

\begin{equation*}
E[X_2 | X_1=x_1, X_3=x_3]
= 1
    + \begin{bmatrix}-2 & 0\end{bmatrix}
        \begin{bmatrix}1 & 0\\0 & 2\end{bmatrix}^{-1}
        \begin{bmatrix}x_1 +3 \\ x_3 -4\end{bmatrix}
= -2x_1-5
\end{equation*}

and

\begin{equation*}
Var[X_2 | X_1=x_1, X_3=x_3]
= 5 -
    \begin{bmatrix}-2 & 0\end{bmatrix}
    \begin{bmatrix}1 & 0\\0 & 2\end{bmatrix}^{-1}
    \begin{bmatrix}-2 \\ 0\end{bmatrix}
= 1
\end{equation*}

Notice that \(x_3\) is not part of the result since it is independent of \(X_2\).
\end{example}

\subsection{Transformations}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---transformations}

Let \(\vec{X} \sim MVN(\vec{\mu},\ \mat{\Sigma})\)
and \(\mat{A}\) be full rank. Then,

\begin{equation*}
\mat{A}\vec{X} \sim MVN(\mat{A}\vec{\mu},\ \mat{A}\mat{\Sigma}\mat{A}^T)
\end{equation*}

\begin{example}[{Transformation of multivariate normal}]
Let

\begin{equation*}
\begin{bmatrix}X_1\\X_2 \\ X_3\end{bmatrix}
\sim MVN\left(
    \begin{bmatrix}3 \\4\\ -3\end{bmatrix},
    \
    \begin{bmatrix}
    2 & 1 & 3\\
    1 & 4 & -2\\
    3 & -2 & 8
    \end{bmatrix}
\right)
\end{equation*}

We want to find the joint distribution of \(Y_1 = X_1 + X_3\)
and \(Y_2 = 2X_2\).

\begin{equation*}
\vec{Y} = \begin{bmatrix}
Y_1\\Y_2
\end{bmatrix}
= \mat{A}\vec{X}
\quad\text{where}\quad
\mat{A} = \begin{bmatrix}
1 & 0 & 1\\
0 & 2 & 0
\end{bmatrix}
\end{equation*}

So, \(\vec{Y}\) is normally distributed with

\begin{equation*}
E[\vec{Y}] = \mat{B}E[\vec{X}] = \begin{bmatrix}0\\8\end{bmatrix}
\end{equation*}

and

\begin{equation*}
Var[\vec{Y}] = \mat{B}Var[\vec{X}]\mat{B}^T = \begin{bmatrix}
16 & -2\\
-2 & 16
\end{bmatrix}
\end{equation*}
\end{example}

\subsection{Linear combinations}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---linear-combinations}

Suppose we have a sample \(\vec{X}_1, \ldots \vec{X}_n\)
where \(\vec{X}_i \sim MVN(\vec{\mu}_i, \mat{\Sigma})\)
(note the covariance matrix is the constant).
Let \(\vec{b} = (b_1,\ldots b_n)\)
and \(\vec{c} = (c_1, \ldots c_n)\)
be constants.
Then, the joint distribution of

\begin{equation*}
\begin{aligned}
\vec{V}_1 &= b_1\vec{X}_1 + \cdots + b_n\vec{X}_n\\
\vec{V}_2 &= c_1\vec{X}_1 + \cdots + c_n\vec{X}_n\\
\end{aligned}
\end{equation*}

is multivariate normal with mean

\begin{equation*}
\begin{bmatrix}
\vec{V}_1 \\ \vec{V}_2
\end{bmatrix}
\sim
MVN\left(
\begin{bmatrix}
\sum_{i=1}^n b_i\vec{\mu}_i
\\
\sum_{i=1}^n c_i\vec{\mu}_i
\end{bmatrix},
\
\begin{bmatrix}
(\vec{b}\cdot\vec{b})\mat\Sigma & (\vec{b}\cdot\vec{c})\mat\Sigma
\\
(\vec{c}\cdot\vec{b})\mat\Sigma & (\vec{c}\cdot\vec{c})\mat\Sigma
\end{bmatrix},
\right)
\end{equation*}

\begin{admonition-remark}[{}]
At first I was wondering why both \(\vec{V}_1\)
and \(\vec{V}_2\) have to be the sum of the same
\(n\) variables. But, I then realized that we can
set some of the coefficients to zero.
\end{admonition-remark}

\section{Principal components, distance and contours}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---principal-components-distance-and-contours}

The quantity \((\vec{X}-\vec{\mu})^T\mat{\Sigma}^{-1}(\vec{X}-\vec{\mu})\) may be interpreted
as the \textbf{statistical distance} or \textbf{Mahalanobis distance}.
So, consider the contour

\begin{equation*}
(\vec{x}-\vec{\mu})^T\mat{\Sigma}^{-1}(\vec{x}-\vec{\mu}) = c^2
\end{equation*}

This locus corresponds to a ellipsoid in \(p\)-dimensional space
with axes \(\pm c\sqrt{\lambda_i} \vec{e}_i\) where
\((\lambda_i, \vec{e}_i)\) are eigenpairs of \(\mat{\Sigma}\)
and the \(\vec{e}_i\) are normalized.

\begin{admonition-note}[{}]
This is result 4.7 from Applied Multivariate statistics
\end{admonition-note}

Additionally,

\begin{equation*}
(\vec{X}-\vec{\mu})^T\mat{\Sigma}^{-1}(\vec{X}-\vec{\mu}) \sim \chi^2_p
\end{equation*}

\section{Sample statistics}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---sample-statistics}

Consider the sample statistics
\(\overbar{\vec{X}}\) and \(\mat{S}\)
where

\begin{equation*}
\overbar{\vec{X}} = \frac{1}{n} \sum_{i=1}^n \vec{X}_i
,\quad\text{and}\quad
\mat{S} = \frac{1}{n-1}\sum_{i=1}^n (\vec{X}_i - \overbar{\vec{X}})(\vec{X}_i - \overbar{\vec{X}})^T
\end{equation*}

Then \(\overbar{\vec{X}}\) and \(\mat{S}\) are independent
and sufficient statistics for \((\vec{\mu}, \mat{\Sigma})\)
with sampling distributions

\begin{itemize}
\item \(\overbar{\vec{X}} \sim MVN\left(\vec{\mu}, \frac{1}{n}\mat{\Sigma}\right)\)
\item \((n-1)\mat{S} \sim W_{n-1}(\cdot \ | \ \mat\Sigma)\)
\end{itemize}

\begin{admonition-note}[{}]
\(W_m(\cdot \ | \ \mat{\Sigma})\) is the \textbf{Wishart
distribution} with \(m\) degrees of freedom
and is defined as the distribution of \(\sum_{j=1}^m \vec{Z}_j\vec{Z}_j^T\)
where \(Z_j\) are iid \(MVN(\vec{0}, \mat\Sigma)\).
\end{admonition-note}

\section{TODO}
\label{develop--stats:ROOT:page--multivariate-normal.adoc---todo}

\begin{itemize}
\item Generating data (emperical = false?)
\item Law of Large numbers (Result 4.12)
\item Central limit theorem (Result 4.13)
    
    \begin{itemize}
    \item (4-28)
    \end{itemize}
\item Sample statistics:
    
    \begin{itemize}
    \item MLEs
    \item Sufficient
    \item Unbiased
    \end{itemize}
\item Measuring variance
    
    \begin{itemize}
    \item generalized variance (determinant of covariance matrix)
    \item Trace
    \end{itemize}
\end{itemize}
\end{document}
