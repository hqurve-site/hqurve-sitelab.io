\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Clustering}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Clustering a technique used to find the \emph{natural grouping} of objects (data points).
This grouping can allow us to assess dimensionality, outliers and may suggest hypotheses
based on observed relationships.
Clustering is distinct from classification since clustering deals with the case where the groups
and the number of groups are both unknown.
Clustering methods are primitive and typically do not assume any particular structure
of the data.

In order to perform clustering, the notion of \textbf{similarity} is required.
A good clustering method produces groups that are have

\begin{itemize}
\item high \textbf{intra}-class similarity. That is if we take two items from the \textbf{same} group, they are \textbf{similar}
\item low \textbf{inter}-class similarity. That is, if we take two items from \textbf{different} groups, they are \textbf{different}.
\end{itemize}

The \textbf{stability} of a clustering algorithm can be assessed by comparing
the clusters before and after applying small perturbations to the objects.

\section{Distance functions}
\label{develop--stats:ROOT:page--clustering.adoc---distance-functions}

In order to measure distance, we use distance functions.
Whenever possible, metrics should be used, however, most methods can
also work with subjective distance functions.
Some examples of distance functions include

\begin{description}
\item[Euclidean metric] \(d(\vec{x}, \vec{y}) = \sqrt{(\vec{x}-\vec{y})^T(\vec{x}-\vec{y})}\).
    This can be used as the default
\item[Statistical distance] \(d(\vec{x}, \vec{y}) = \sqrt{(\vec{x}-\vec{y})^T\mat{A}(\vec{x}-\vec{y})}\).
    Ordinarily \(\mat{A}=\mat{S}^{-1}\), but this may not be the best if the data has distinct groups.
\item[Minkowski metric] \(d(\vec{x}, \vec{y}) = \sqrt[m]{\sum_{i=1}^p |x_i-y_i|^m}\)
\item[Canberra metric] \(d(\vec{x}, \vec{y}) = \sum_{i=1}^p \frac{|x_i-y_i|}{x_i+y_i}\) (for non-negative variables only)
\item[Czekanowski metric] \(d(\vec{x}, \vec{y}) = 1- \frac{\sum_{i=1}^p \min(x_i, y_i)}{\sum_{i=1}^p (x_i+y_i)}\) (for non-negative variables only)
\end{description}

\begin{admonition-todo}[{}]
Table 12.1
\end{admonition-todo}

\section{Hierarchical Clustering Methods}
\label{develop--stats:ROOT:page--clustering.adoc---hierarchical-clustering-methods}

A hierarchical clustering produces groupings for all numbers
of groups \(1,\ldots N\)
as well as a relationship between the groupings of different sizes.
To do this, it organizes groups into a tree-like structure
where nodes are groups and parent nodes have the items of their two
children.

This structure can be illustrated using a dendrogram which
illustrates the mergers and divisions that have been made at successive levels.
The diagram allows us to hypothesize why certain objects are similar.
image::dendrogram.png[width=30\%]
For example, the above diagram indicates that

\begin{itemize}
\item groups \(\{3\}\) and \(\{5\}\) merge/split at level 2
\item groups \(\{1\}\) and \(\{3,5\}\) merge/split at level 3
\item groups \(\{2\}\) and \(\{4\}\) merge/split at level 5
\item groups \(\{1,3,5\}\) and \(\{2,4\}\) merge/split at level 6
\end{itemize}

The level is the \textbf{intercluster distance} (dissimilarity) between the two groups.
There are several dissimilarities which are built from an original
distance function \(d\).

\begin{description}
\item[Single linkage] minimum distance/nearest neighbour.
    That is \(d(U,V) = \min\{d(u,v): u\in U, v\in V\}\)
\item[Complete linkage] maximum distance/farthest neighbour.
    That is \(d(U,V) = \max\{d(u,v): u\in U, v\in V\}\)
\item[Average linkage] average distance.
    That is \(d(U,V) = \frac{1}{|U|\ |V|}\sum_{u\in U}\sum_{v\in V}d(u,v)\)
\item[Centroid linkage] the distance between the centroids of the two groups.
\end{description}

There are two common approaches for producing a hierarchical clustering:

\begin{description}
\item[Agglomerative] Such methods start with groups of individual objects and successively
    merge groups into a single group.
\item[Divisive] Such methods start with a single group containing all objects
    and successively divide groups until all groups contain one object each.
\end{description}

\section{Agglomerative Methods}
\label{develop--stats:ROOT:page--clustering.adoc---agglomerative-methods}

The algorithm is as follows

\begin{enumerate}[label=\arabic*)]
\item Start with \(N\) clusters, each containing a single object and a \(N\times N\)
    matrix \(\mat{D}\) of distances
\item Search the distance matrix for the most similar pair of clusters
    \(U,V\) with distance \(d(U,V)\)
\item Merge clusters \(U,V\) to produce cluster \((UV)\)
    and update the distance matrix as follows
    
    \begin{enumerate}[label=\alph*)]
    \item Delete the rows and columns for \(U\) and \(B\)
    \item Add a new row and column, giving the distances between \((UV)\) and the other
        remaining groups
    \end{enumerate}
\item Repeat steps \(2\) and \(3\) \(N-1\) times,
    recording the merges and the levels at which they took place.
\end{enumerate}

The computation of step 3b in the above algorithm cannot be optimized in general.
But we can simplify it from some common linkages

\begin{description}
\item[Single linkage] \(d((UV), W) = \min\{d(U,W), d(V, W)\}\)
\item[Complete linkage] \(d((UV), W) = \max\{d(U,W), d(V, W)\}\)
\item[Average linkage] Cannot be optimized
\end{description}

In \texttt{R} we can perform agglomerative hierarchical clustering
as follows

\begin{listing}[{}]
# define distance matrix. d[i,j] is the distance from i to j
dist.matrix = ..
rownames(dist.matrix) <- names
colnames(dist.matrix) <- rownames(dist.matrix)

library(cluster)
library(dendextend)

# convert matrix to distance object
dist.obj <- as.dist(dist.matrix)

# perform clustering
# method = "single" or "connected" means single linkage
# method = "complete" means complete linkage
# method = "average" means complete linkage
result <- hclust(dist.object, method = "single")

# plot dendrogram
plit(result)
\end{listing}

\subsection{Ward's Hierarchical Clustering}
\label{develop--stats:ROOT:page--clustering.adoc---wards-hierarchical-clustering}

Ward's Hierarchical clustering method is an agglomerative method
which tries to minimize the \emph{loss of information} from joining two
groups.
The method is usually implemented with ``loss of information'' meaning
an increase in an \textbf{error sum of squares} (ESS) criterion.
The error sum of squares for \(K\) groups is defined
as

\begin{equation*}
ESS = \sum_{i=1}^K ESS_i
\end{equation*}

where \(ESS_i\) is the sum of squares of the items
in group \(k\) from the group mean.
At each stage, each possible pair of groups is assessed for merging
and the combination with the smallest \textbf{increase} in ESS is chosen.

The results of Ward's method can be illustrated on a dendrogram
where the values on the vertical axis illustrate the ESS.

\section{Non-hierarchical Clustering Methods}
\label{develop--stats:ROOT:page--clustering.adoc---non-hierarchical-clustering-methods}

Non-hierarchical methods can be used to cluster objects into \(K\) groups.
Unlike hierarchical methods, there is usually
no relationship between the clustering into \(K\) groups
and \(K+1\) groups.
However, Non-hierarchical clustering methods are less computationally intensive
and can be applied to much larger datasets than can hierarchical techniques.

Non-hierarchical methods rely on either a

\begin{itemize}
\item initial partition of the data into groups
\item initial seeds which will form the nuclei of the initial groups
\end{itemize}

The initial configuration should be random selected and free of bias
since clustering methods should be able to recover the natural structure of the data.

\subsection{K-means}
\label{develop--stats:ROOT:page--clustering.adoc---k-means}

The K-means method is a fixed point method for clustering data.
In each stage, the centers of each group are determined and objects
are grouped based on the center they are nearest to.
The algorithm is as follows

\begin{enumerate}[label=\arabic*)]
\item Partition the objects into \(K\) initial groups
\item Determine the centroids of each of the groups
\item For each object
    
    \begin{enumerate}[label=\alph*)]
    \item Compare its distance to each of the centroids
    \item If it is closer to a centroid of a group it is not currently assigned to,
        reassign the object to the other group.
    \item Recompute the centroids of the affected groups before proceeding
        to the next object
    \end{enumerate}
\item Repeat steps 2 and 3 until no more reassignments occur
\end{enumerate}

Note that initial centroids (seed points) can be specified instead
of initial groups.

It is not hard to see that the final result is dependent
on the initial groups/seed points as well as the order of the objects.
However, most reassignments
will occur in the first few iteration.
To assess the stability, the algorithm can be rerun with
a different initial configuration.

Overall, the K-means method seeks to minimize

\begin{equation*}
\sum_{i} d^2_{i, c(i)}
\end{equation*}

where \(d^2_{i,c(i)}\) is the squared distance of the \(i\)'th
object from the centroid of the group it is assigned to.

In \texttt{R} we can perform the kmeans algorithm
as follows

\begin{listing}[{}]
# define objects where each row represents a different object
x <- ...

# run kmeans
# centers can either be the number of clusters or the initial centers.
# If the number of clusters is specified, a random set of the objects are chosen to be the initial centers
result <- kmeans(x, centers, iter.max=10)

# plot the result
# assuming the x matrix has two columns
plot(x[,1], x[,2], type="n")
text(x[,1], x[,2], labels=as.character(result$cluster))
\end{listing}

Note that we should not fix the number of groups in advance
since

\begin{itemize}
\item If two or more seed values lie within the same group,
    their resulting groups will be poorly differentiated
\item The existence of an outlier might produce a group
    with very disperse objects
\item Even if the population is known to consist of \(K\) groups,
    the sampling method may be such that data from the smallest
    group does not appear in the sample. So, forcing the data
    into \(K\) groups will lead to a nonsensical clustering
\end{itemize}

It is also always a good idea to rerun the algorithm multiple
times with several initial configurations.

\subsection{Determining the number of groups}
\label{develop--stats:ROOT:page--clustering.adoc---determining-the-number-of-groups}

There are a number of ways to determine the optimal number of groups
when using non-hierarchical clustering

\begin{description}
\item[Hartigan's rule of thumb] Let \(C_k\) and \(C_{k+1}\) represent the \(k-means\) clustering result with \(k\)
    and \(k+1\) groups (respectively).
    Then, it is justifiable to add the extra group when
    
    \begin{equation*}
    \left(\frac{\text{Within SS for }C_k}{\text{Within SS for }C_{k+1}}-1\right)
    (n-k-1) > 10
    \end{equation*}
    
    That is, adding the extra group reduces the total within (residual) sum of squares significantly
\item[Elbow method] Plot the within (residual) sum of squares as a function of the number of groups.
    We choose \(k\) such that adding another group does not significantly decrease
    the within sum of squares. Such a k will be apparent by an \emph{elbow} in the plotted curve.
\item[Average Silhouette method] The average silhouette method chooses the number of clusters
    that maximizes the average silhouette.
    The silhouette of a group measures how well each object lies in the group.
    So a high average silhouette indicates a good clustering
\item[Gap statistic] The gap statistic compares the total within intra-cluster variation
    with the expected values under null reference distribution of the data.
    The estimate of the optimal number of groups
    will be the value that maximizes the gap-statistic.
    That is, the clustering structure is far away from the random uniform distribution
    of points.
    
    To produce the gap statistic,
    
    \begin{enumerate}[label=\arabic*)]
    \item Produce clusterings for different numbers of groups \(k=1,\ldots K\)
        and let \(W_k\) be the within-dispersion measures (residual sum of squares)
    \item Generate \(B\) reference datasets from a uniform and
        cluster each one giving a within dispersion measure \(W_{kb}^*\).
        Estimate the gap statistic as
        
        \begin{equation*}
        Gap(k) = \frac{1}{B} \sum_{b=1}^B \log(W_{kb}^*) - \log (W_k)
        \end{equation*}
    \item Let \(\overbar{l} = \frac{1}{B}\sum_{b=1}^B \log(W_{kb}^*)\)
        and compute the standard deviation as
        
        \begin{equation*}
        sd_k = \sqrt{\frac{1}{B}[\log(W_{kb}^*) - \overbar{l}]^2}
        \end{equation*}
        
        and define \(s_k = sd_k\sqrt{1+\frac{1}{B}}\).
    \item Choose the number of clusters via
        
        \begin{equation*}
        \hat{k} = \text{smallest }k \text{ such that } Gap(k)\geq Gap(k+1)-s_{k+1}
        \end{equation*}
    \end{enumerate}
\end{description}

\section{Statistical-model clustering}
\label{develop--stats:ROOT:page--clustering.adoc---statistical-model-clustering}

We can use statistical models to perform clustering.
To do so, we may assume that the data comes
from a mixture distribution with pdf

\begin{equation*}
f(\vec{x}| \vec{\theta}) = \sum_{k=1}^K p_kf_k(\vec{x} | \vec{\theta})
\end{equation*}

where \(p_k\) is the proportion of the data from distribution
\(f_k\) and \(\vec{\theta}\) are some set of parameters.
In order to estimate the parameters, we can use the
\myautoref[{EM Algorithm}]{develop--stat6180:ROOT:page--em.adoc} which is implemented
using the \texttt{mclust} library in R.
After estimating the parameters, we assign object
\(j\) to group \(k\) if it maximizes

\begin{equation*}
P(k| \vec{x}_j) =
\frac
{\hat{p}_jf_k(\vec{x}_j|\hat{\vec{\theta}})}
{\sum_{i=1}^K \hat{p}_if_k(\vec{x}_i|\hat{\vec{\theta}})}
\end{equation*}
\end{document}
