\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Vector Calculus}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
\begin{admonition-note}[{}]
This section mostly focuses on 3 dimensional euclidean vectors;
however, most of these concepts can be extended to other vector spaces.
\end{admonition-note}

\section{Basics}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---basics}

\subsection{Vector and scalar fields}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---vector-and-scalar-fields}

A \emph{vector field} associates a vector with each particular point while a
\emph{scalar field} associates a scalar value with each point. Basically,
both are just functions outputting vectors and scalars respectively. In
this section a \emph{vector function} would refer to a function outputting
vectors while a \emph{scalar function} would refer to a function outputting
scalars.

\subsection{Limit definition}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---limit-definition}

A vector function \(\bm{F}(u)\) is said to have a limit value
\(\bm{P}\) at point \(u_0\) if for any
\(\varepsilon > 0\), \(\exists \delta > 0\) such
that

\begin{equation*}
\left| u - u_0 \right| < \delta \implies \left| \bm{F}(u)- \bm{P} \right| < \varepsilon
\end{equation*}

and is written as

\begin{equation*}
\lim_{u\rightarrow u_0} \bm{F}(u) = \bm{P}
\end{equation*}

where \(\left| \cdot \right|\) is a norm (may differ for
domain and codomain). Note that this is basically the same
\(\varepsilon-\delta\) definition and this observation is true
for most definitions including continuity and other properties.

\subsection{Differential}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---differential}

If

\begin{equation*}
\bm{F}(u) = F_1(u)\hat{i} + F_2(u)\hat{j} + F_3(u)\hat{k}
\end{equation*}

then

\begin{equation*}
\frac{\partial \bm{F}}{\partial t}
    = \frac{\partial F_1}{\partial t}\hat{i}
    + \frac{\partial F_2}{\partial t}\hat{j}
    + \frac{\partial F_3}{\partial t}\hat{k}
\end{equation*}

Actually, the total differential of \(\bm{F}\) is just the
Jacobian matrix of \(\bm{F}\) and these are just results of
the jacobian matrix.

\subsection{Derivatives of products}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---derivatives-of-products}

Consider vector function \(\bm{A}\) and \(\bm{B}\),
and the scalar function \(\phi\). then

\begin{itemize}
\item \(\displaystyle\frac{\partial }{\partial t}\left( \phi \bm{A} \right) = \phi \frac{\partial \bm{A}}{\partial t} + \frac{\partial \phi}{\partial t}\bm{A}\)
\item \(\displaystyle\frac{\partial }{\partial t}\left( \bm{A}\bm{\cdot}\bm{B} \right) = \bm{A}\bm{\cdot}\frac{\partial \bm{B}}{\partial t} + \frac{\partial \bm{A}}{\partial t}\bm{\cdot}\bm{B}\)
\item \(\displaystyle\frac{\partial }{\partial t}\left( \bm{A}\times \bm{B} \right) = \bm{A}\times \frac{\partial \bm{B}}{\partial t} + \frac{\partial \bm{A}}{\partial t}\times\bm{B}\)
\end{itemize}

Notice that they are just the usual product rule just with different
operators. In fact, the product rule holds for all bilinear operators.
However, be careful with operators which are not commutative (such as
the cross product).

\subsection{Integrals}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---integrals}

Integrals, like derivatives, are computed component-wise. That is if

\begin{equation*}
\bm{F}(u) = F_1(u)\hat{i} + F_2(u)\hat{j} + F_3(u)\hat{k}
\end{equation*}

\begin{equation*}
\int_C\bm{F}(t)dt = \int_C F_1(t) dt\hat{i} + \int_C F_2(t) dt\hat{j} + \int_C F_3(t) dt\hat{k}
\end{equation*}

\section{Parameterized curves}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---parameterized-curves}

Consider the curve \(\bm{r}(t)\) where
\(t\in \mathbb{R}\).

\subsection{Arc length}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---arc-length}

The arc length of \(\bm{r}\), \(s\), from a point
\(a\) is given by

\begin{equation*}
s(t)= \int_a^t \sqrt{
            \left( \frac{dx}{du} \right)^2
            +\left( \frac{dy}{du} \right)^2
            +\left( \frac{dz}{du} \right)^2
        } du
        = \int_a^t \left| \bm{r}'(u) \right| du
\end{equation*}

\subsection{Tangent and smooth}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---tangent-and-smooth}

Furthermore, the curve is called \emph{smooth} on interval \(I\) if
\(\bm{r}'\) is continuous and
\(\bm{r}' \neq \bm{0}\) on \(I\). Then the length of
unit tangent vector is given by

\begin{equation*}
\bm{T}(t)  = \frac{\bm{r}'(t)}{\left| \bm{r}'(t) \right|}
\end{equation*}

and indicates the direction of \(\bm{r}\).

\subsection{Curvature}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---curvature}

The curvature at a given point is a measure of how quickly the curve
changes direction and it is given by

\begin{equation*}
\kappa = \left| \frac{d\bm{T}}{ds} \right|
\end{equation*}

and can be simplified using the chain rule as

\begin{equation*}
\kappa = \left| \frac{d\bm{T}}{ds} \right| = \left| \frac{d\bm{T}/dt}{ds/dt} \right| = \frac{\left| \bm{T}'(t) \right|}{\left| \bm{r}'(t) \right|}
\end{equation*}

since \(\frac{ds}{dt} = \left| \bm{r}'(t) \right|\).
Additionally, it can be written as

\begin{equation*}
\kappa = \frac{\left| \bm{r}'(t) \times \bm{r}''(t) \right|}{\left| \bm{r}'(t) \right|^3}
\end{equation*}

by rewriting \(\left| \bm{T}'(t) \right|\) using the fact that
\(\left| \bm{f}(t) \right| = c \implies \bm{f}(t) \bm{\cdot}\bm{f}'(t) = 0\).

\subsection{Normal and Binormal vectors}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---normal-and-binormal-vectors}

The normal and binormal vectors are unit vectors which are both
perpendicular to the curve at a point in addition to being perpendicular
to each other. The \emph{unit normal} is defined using
\(\bm{T}(t)\) as

\begin{equation*}
\bm{N}(t) = \frac{\bm{T}'(t)}{\left| \bm{T}'(t) \right|}
\end{equation*}

since \(\left| \bm{T}(t) \right| =1\) and hence
\(\bm{T}(t) \bm{\cdot}\bm{T}'(t) = 0\). The \emph{binormal vector}
is simply defined as

\begin{equation*}
\bm{B}(t) = \bm{T}(t) \times \bm{N}(t)
\end{equation*}

which is already a unit vector since \(\bm{T}\) and
\(\bm{N}\) are orthogonal unit vectors.

\subsection{Notable planes}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---notable-planes}

The plane determined by the normal and binormal vectors
\(\bm{N}\) and \(\bm{B}\) at a point \(P\)
is called the \emph{normal plane} and consists of all vectors orthogonal to
the curve at \(P\). Whilst the plane determined by the vectors
\(\bm{T}\) and \(\bm{N}\) is called the \emph{osculating
plane} of the curve at \(P\) and it is the plane that comes
closest to containing part of the curve near \(P\).

The circle that lies in the osculating plane at \(P\), has the
same tangent as the curve at \(P\), lies on the concave side
of the curve (toward which \(\bm{N}\) points) and has radius
\(\frac{1}{\kappa}\) is called the \emph{osculating circle} and is
the circle which best describes how the curve behaves near
\(P\).

\section{Direction cosines}
\label{develop--math2270:ROOT:page--vector-calculus.adoc---direction-cosines}

Direction cosines are a simple way to define the direction of a vector
(in a symmetric manner). Consider the points
\(\bm{P}_0= (x_0, y_0, z_0)\) and \(\bm{P}=(x,y,z)\)
in \(\mathbb{R}^3\) then the direction cosines are defined as

\begin{equation*}
\cos(\alpha) = \frac{x-x_0}{\left| \bm{P_0P} \right|}
    ,\quad
    \cos(\beta) = \frac{y-y_0}{\left| \bm{P_0P} \right|}
    ,\quad
    \cos(\gamma) = \frac{z-z_0}{\left| \bm{P_0P} \right|}
\end{equation*}

where \(\alpha, \beta, \gamma \in [0, \pi]\)

\begin{equation*}
\left| \bm{P_0P} \right| = s = \sqrt{(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2}
\end{equation*}

Then \(\cos^2(\alpha) + \cos^2(\beta) + \cos^2(\gamma) = 1\).
Therefore, using the chain rule, the derivative of function a
\(f\) at \(\bm{P}_0\) in direction
\(\bm{P_0P}\) is given by

\begin{equation*}
\begin{aligned}
    \frac{\partial f}{\partial s}
    &= \frac{\partial f}{\partial x}\frac{\partial x}{\partial s}
        + \frac{\partial f}{\partial y}\frac{\partial y}{\partial s}
        + \frac{\partial f}{\partial z}\frac{\partial z}{\partial s}
        \\
    &= \frac{\partial f}{\partial x}\cos(\alpha)
        + \frac{\partial f}{\partial y}\cos(\beta)
        + \frac{\partial f}{\partial z}\cos(\gamma)\end{aligned}
\end{equation*}

which is called the directional derivative of \(f\) in
direction \(\alpha\), \(\beta\),
\(\gamma\).

\begin{admonition-remark}[{}]
Really, I don’t understand why this is done since this is just a special
case of the chain rule and can be computed just as easily without
considering angles and just using unit vectors instead. The case for
direction cosines is worsened even further since they are not even
independent. Furthermore, two angles are not sufficient since there are
two solutions for the sum of squares formula.
\end{admonition-remark}
\end{document}
