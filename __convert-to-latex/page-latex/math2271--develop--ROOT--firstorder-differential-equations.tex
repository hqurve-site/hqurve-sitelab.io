\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{First-order differential equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\(\def\bm#1{{\bf #1}}\)
Contradicting what the title suggests, the scope of this section is
limited further to set of first-degree ODEs.

Then in general, a first-order first-degree ODE can be written as one of
the following (equivalent) forms

\begin{equation*}
\frac{dy}{dx} = f(x, y)
    \quad\text{or}\quad
    M(x, y) + N(x, y) \frac{dy}{dx} = 0
    \quad\text{or}\quad
    M(x, y)dx + N(x, y)dy = 0
\end{equation*}

\section{Remark}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---remark}

yes, I too die every time I see the total differential but I do
understand its usefulness (as it no longer restricts \(x\) to
be the independent variable).

\section{Existence and Uniqueness of solutions}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---existence-and-uniqueness-of-solutions}

Consider the first order IVP

\begin{equation*}
y' = f(x, y), \quad y(x_0) = y_0
\end{equation*}

If \(f\) and \(\frac{\partial f}{\partial y}\) are
continuous in a closed rectangle

\begin{equation*}
R = \{|x - x_0| \leq a, |y - y_0| \leq b\}
\end{equation*}

then \(\exists\) a unique solution \(y=y(x)\) of the
IVP in an interval \(|x - x_0| \leq h \leq a\).

\section{Linear Equations}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---linear-equations}

Consider a first-order linear ODE

\begin{equation*}
\frac{dy}{dx} + p(x)y(x) = g(x)
\end{equation*}

Then, suppose we multiply the equation by a function \(u(x)\)
such that the left hand side can be written as the derivative of a
product. That is,

\begin{equation*}
u(x)g(x) = u(x)\left[\frac{dy}{dx} + p(x)y(x)\right] = \frac{d}{dx}\left[u(x)y(x)\right]
    \iff \frac{d}{dx}u(x) = p(x)u(x)
\end{equation*}

Which occurs iff \(u(x) = e^{\int p(x) dx}\). We call
\(u\) the integrating factor and we get that

\begin{equation*}
\begin{aligned}
    &\frac{d}{dx}\left[e^{\int p(x) dx}y(x)\right] = e^{\int p(x) dx}g(x)\\
    \implies & y(x)e^{\int p(x) dx} = \int  e^{\int p(x) dx}g(x) dx\\
    \implies & y(x) =e^{-\int p(x) dx} \int  e^{\int p(x) dx}g(x) dx\end{aligned}
\end{equation*}

\section{Separable Equations}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---separable-equations}

An first-order ODE of the form

\begin{equation*}
M(x, y) + N(x, y)\frac{dy}{dx} = 0
\end{equation*}

is called \emph{separable} if \(M\) is a function of
\(x\) only and \(N\) is a function of
\(y\) only. Then, the equation can be rewritten as

\begin{equation*}
N(y) \frac{dy}{dx} = -M(x)
\end{equation*}

which can be easily reduced by integration wrt \(x\) to yield
a possibly implicit equation for \(y\).

\begin{admonition-note}[{}]
Separable equations are also exact, as seen below.
\end{admonition-note}

\section{Exact Equations}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---exact-equations}

Consider a first-order ODE of the form

\begin{equation*}
\numberandlabel{fo:exact:1}
    M(x, y)dx + N(x, y)dy = 0
\end{equation*}

Then, the left hand side resembles the total differential of a function.
Let \(u(x, y)\) be such a function (if it exists). Then

\begin{equation*}
du = \frac{\partial u}{\partial x} dx + \frac{\partial u}{\partial y} dy
\end{equation*}

And hence, if we can find a function \(u\) such that
\(M = \frac{\partial u}{\partial x}\) and
\(N = \frac{\partial u}{\partial y}\) then the solution to
\(\eqref{fo:exact:1}\) is

\begin{equation*}
du(x, y) = 0 \iff u(x, y) = c
\end{equation*}

where \(c\) is a constant. In the case that
\(u(x, y)\) exists, then the ODE \(\eqref{fo:exact:1}\) is
called \emph{exact}.

\subsection{Necessary and sufficient}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---necessary-and-sufficient}

If \(M(x, y)\), \(N(x, y)\),
\(\frac{\partial M}{\partial y}\) and
\(\frac{\partial M}{\partial x}\) are all continuous, then

\begin{equation*}
\eqref{fo:exact:1} \text{ is exact} \iff \frac{\partial M}{\partial y} = \frac{\partial N}{\partial x}
\end{equation*}

\begin{example}[{Proof}]
If \(\eqref{fo:exact:1}\) is exact, then
\(\exists u(x, y)\) such that

\begin{equation*}
M = \frac{\partial u}{\partial x} \quad\text{and}\quad N = \frac{\partial u}{\partial y}
\end{equation*}

then, by the premise and by Clairaunt’s Theorem

\begin{equation*}
\frac{\partial M}{\partial y}
        = \frac{\partial^2 u}{\partial y \partial x}
        = \frac{\partial^2 u}{\partial x \partial y }
        = \frac{\partial N}{\partial x}
\end{equation*}

Now, for the reverse direction. Let \(w(x, y)\) be a function
such that

\begin{equation*}
M(x, y) = \frac{\partial w}{\partial x}
\end{equation*}

then, by the premise and by Clairaunt’s Theorem

\begin{equation*}
\frac{\partial M}{\partial y}
        = \frac{\partial^2 w}{\partial y \partial x}
        = \frac{\partial^2 w}{\partial x \partial y }
        = \frac{\partial N}{\partial x}
\end{equation*}

And, by integrating wrt \(x\), we get that

\begin{equation*}
\frac{\partial w}{\partial y}
        = \int \frac{\partial^2 w}{\partial x \partial y } dx
        = \int \frac{\partial N}{\partial x} dx
        = N(x, y) + f'(y)
\end{equation*}

where \(f(y)\) is an arbitrary function. Now, if we take

\begin{equation*}
u(x, y) = w(x, y) - f(y)
\end{equation*}

then, the ODE is exact since,

\begin{equation*}
\frac{\partial u}{\partial x} = \frac{\partial w}{\partial x} = M(x, y)
        \quad\text{and}\quad
        \frac{\partial u}{\partial y} = \frac{\partial w}{\partial y} - f'(y) = N(x, y) + f'(y) - f'(y) = N(x, y)
\end{equation*}

 ◻
\end{example}

\subsection{Use of Integrating Factor}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---use-of-integrating-factor}

Suppose \(\eqref{fo:exact:1}\) is not exact, then one approach to
make it exact is by multiplying by a function \(\mu(x, y)\) to
get

\begin{equation*}
\mu(x, y)M(x, y)dx + \mu(x, y)N(x, y)dy = 0
\end{equation*}

Then, by the above theorem, we must have that

\begin{equation*}
(\mu M)_y = (\mu N)_x \iff \mu_y M + \mu M_y = \mu_x N + \mu N_x
\end{equation*}

Now, this equation is not simple to solve without some restrictions. So,
consider the following cases

\begin{itemize}
\item \(\mu\) is a function of \(x\) only. Then
    
    \begin{equation*}
    \frac{\mu_x}{\mu} = \frac{M_y - N_x}{N}
    \end{equation*}
    
    must be a function of \(x\) only and hence
    \(\mu(x) = \exp\left(\int \frac{M_y - N_x}{N} dx\right)\) is
    the integrating factor to use.
\item \(\mu\) is a function of \(y\) only. Then
    
    \begin{equation*}
    \frac{\mu_y}{\mu} = \frac{M_y - N_x}{-M}
    \end{equation*}
    
    must be a function of \(y\) only and hence
    \(\mu(y) = \exp\left(\int \frac{M_y - N_x}{-M} dy\right)\) is
    the integrating factor to use.
\item \(\mu\) is a function of \(u=xy\) only. Then
    
    \begin{equation*}
    \frac{\mu'}{\mu} = \frac{N_x - M_y}{xM - yN}
    \end{equation*}
    
    must be a function of \(xy\) only and hence
    \(\mu(u) = \exp\left(\int \frac{N_x - M_y}{xM - yN} du\right)\)
    is the integrating factor to use.
    
    Note, that if \(M = yf(xy)\) and \(N = xg(xy)\)
    where \(f \neq g\), then the integrating factor reduces to
    
    \begin{equation*}
    \begin{aligned}
                \mu(u)
                &= \exp\left(\int \frac{N_x - M_y}{xM - yN} du\right)\\
                &= \exp\left(\int \frac{g(xy) +xyg'(xy) - f(xy)-xyf'(xy)}{xyf(xy) - xyg(xy)} du\right)\\
                &= \exp\left(\int \frac{g(u) + ug'(u) - f(u)-uf'(u)}{uf(u) - ug(u)} du\right)\\
                &= \exp\left(-\int \frac{1}{u} + \frac{f'(u) -g'(u)}{f(u) - g(u)} du\right)\\
                &= \exp\left(-\ln(u) - \ln(f(u) - g(u))\right)\\
                &= \frac{1}{u\left[f(u) - g(u)\right]}\\
                &= \frac{1}{xy\left[f(xy) - g(xy)\right]}
            \end{aligned}
    \end{equation*}
\end{itemize}

\section{Homogenous Equations}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---homogenous-equations}

Recall that a first-order ODE is homogeneous iff it can be written as

\begin{equation*}
M(x, y) + N(x, y)\frac{dy}{dx} = 0
\end{equation*}

where both \(M\) and \(N\) are homogenous functions
of the same degree. Then, let \(t = \tfrac{1}{x}\), we get
that

\begin{equation*}
\begin{aligned}
    & M(x, y) + N(x, y)\frac{dy}{dx}
    = \left(\frac{1}{x}\right)^k M\left(1, \frac{y}{x}\right) + \left(\frac{1}{x}\right)^k N\left(1, \frac{y}{x}\right)\frac{dy}{dx} = 0
    \\
    & \iff \frac{dy}{dx} = \frac{M\left(1, \frac{y}{x}\right)}{N\left(1, \frac{y}{x}\right)} = f\left(\frac{y}{x}\right)\end{aligned}
\end{equation*}

Then, by letting \(y = ux\),
\(\frac{dy}{dx} = x\frac{du}{dx} + u\) and hence

\begin{equation*}
x\frac{du}{dx} + u = \frac{dy}{dx} = f(u) \iff \left(\frac{1}{f(u) - u}\right)\frac{du}{dx} = \frac{1}{x}
\end{equation*}

which can be solved for \(u\) and then solved for
\(y\) using substitution.

\section{Bernoulli Equations}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---bernoulli-equations}

A Bernoulli equation is a first-order ODE of the form

\begin{equation*}
\numberandlabel{fo:berno:1}
    y' + P(x)y = R(x)y^\alpha
\end{equation*}

where \(\alpha \in \mathbb{R}\). However, note that if
\(\alpha=0\), we get a linear first-order while if
\(\alpha =1\), we get that the ODE is separable. Then, we will
only consider \(\alpha \in \mathbb{R} - \{0, 1\}\). Then, we
use the substitution \(\nu = y^{1-\alpha}\) which implies that

\begin{equation*}
\frac{d\nu}{dx} = (1-\alpha)y^{-\alpha}\frac{dy}{dx}
\end{equation*}

Now, by dividing \(\eqref{fo:berno:1}\) by \(y^{\alpha}\)
then using the substitution, we get that

\begin{equation*}
y^{-\alpha}\frac{dy}{dx} + P(x)y^{1-\alpha} = R(x)
    \implies
    \frac{1}{1-\alpha}\frac{d\nu}{dx} + P(x)\nu = R(x)
\end{equation*}

which is a first-order linear ODE and can be easily solved.

\section{Riccati Equations}
\label{develop--math2271:ROOT:page--firstorder-differential-equations.adoc---riccati-equations}

A Riccati equation is a first-order ODE of the form

\begin{equation*}
y' = P(x)y^2 + Q(x)y + R(x)
\end{equation*}

In order to generate the family of solutions, at least one must be
known. Let this solution be \(S(x)\), then we can represent
the difference between solutions as
\(y - S(x) = \tfrac{1}{z}\) and get

\begin{equation*}
\begin{aligned}
    &y' - S' = P(x)(y^2 - S^2) + Q(x)(y - S)\\
    &\implies \frac{d}{dx}\left(\frac{1}{z}\right) = P(x)\left(\frac{1}{z}\right)\left(\frac{1}{z}+2S\right) + Q(x)\left(\frac{1}{z}\right)\\
    &\implies -\frac{z'}{z^2} = P(x)\left(\frac{1}{z}\right)\left(\frac{1}{z}+2S\right) + Q(x)\left(\frac{1}{z}\right)\\
    &\implies z' = -P(x)\left(1+2Sz\right) - Q(x)z\\
    &\implies z' = -(2P(x)S + Q(x))z - P(x)\end{aligned}
\end{equation*}

which is a first-order linear ODE. Therefore for simplicity, the
substitution \(y = S(x) + \tfrac{1}{z}\) can be used to
generate other solutions.
\end{document}
