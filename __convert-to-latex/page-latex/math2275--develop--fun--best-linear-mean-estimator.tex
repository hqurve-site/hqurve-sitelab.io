\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Best Linear Mean Estimator}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% life is a lie
\def\bigchi{\chi}
\DeclareMathOperator{\SST}{SST}
\DeclareMathOperator{\SSE}{SSE}
\DeclareMathOperator{\SSTr}{SSTr}
\DeclareMathOperator{\SSB}{SSB}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\MSTr}{MSTr}
\DeclareMathOperator{\MSB}{MSB}

\def\bm#1{\mathbf #1}

{} \def\paren#1{\left({#1}\right)}
% Title omitted
Suppose we have a random sample \(X_1, \ldots, X_n\) from a population with known
variance \(\sigma^2\) and unknown mean \(\mu\). We can then
estimate the mean as

\begin{equation*}
    \hat\mu = \sum \alpha_i X_i
\end{equation*}

Note: summation bounds are omitted for simplicity.

\section{Preface}
\label{develop--math2275:fun:page--best-linear-mean-estimator.adoc---preface}

Before we start, we need something to compare our result to. We will be using the sample mean

\begin{equation*}
    \overbar{X} = \tfrac{1}{n} \sum X_i
\end{equation*}

This is unbiased and has a variance of \(\tfrac{\sigma^2}{n}\), and hence a MSE of the same value.

\section{Preliminary Analysis}
\label{develop--math2275:fun:page--best-linear-mean-estimator.adoc---preliminary-analysis}

The bias, variance and MSE can be computed as

\begin{equation*}
    Bias = E[\hat\mu - \mu] = \sum\alpha_i E[X_i] - \mu = \paren{\sum \alpha_i - 1} \mu
\end{equation*}

\begin{equation*}
    Variance = Var[\hat\mu] = Var[\sum \alpha_i X_i] = \sum \alpha_i^2 Var[X_i] = \paren{\sum \alpha_i^2}\sigma^2
\end{equation*}

\begin{equation*}
    MSE = \paren{\sum \alpha_i^2}\sigma^2 + \paren{\sum \alpha_i - 1}^2 \mu^2
\end{equation*}

By the cauchy-schwartz inequality \(\sum \alpha_i^2 \geq \tfrac{1}{n} \paren{\sum \alpha_i}^2\)
with equality when \(\forall i, j: \alpha_i = \alpha_j\). Since we want to minimize the MSE,
we will let this common value be \(\alpha\) and the MSE simplifies to

\begin{equation*}
\numberandlabel{1}
    MSE
        = \paren{\sum \alpha^2}\sigma^2 + \paren{\sum \alpha - 1}^2 \mu^2
        = n\alpha^2\sigma^2 + (n\alpha - 1)^2 \mu^2
\end{equation*}

We can compute \(\tfrac{\partial MSE}{\partial \alpha} = 2n\alpha\sigma^2 + 2n(n\alpha -1)\mu^2\)
and set it equal to zero to obtain the value of \(\alpha\) that minimizes the \(MSE\) (note that it is a quadratic in \(\alpha\) with a `smile').

\begin{equation*}
\begin{aligned}
    & 2n\alpha\sigma^2 + 2n(n\alpha -1)\mu^2 = 0\\
    \iff & \alpha\sigma^2 + (n\alpha - 1)\mu^2 = 0\\
    \iff & \alpha\sigma^2 + n\alpha\mu^2 - \mu^2 = 0\\
    \iff & \alpha(\sigma^2 + n\mu^2) = \mu^2\\
    \iff & \alpha = \frac{\mu^2}{\sigma^2 + n\mu^2}
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
    MSE
    &= n\alpha^2\sigma^2 + (n\alpha - 1)^2 \mu^2\\
    &= n\alpha^2\sigma^2 + n^2\alpha^2\mu^2 - 2n\alpha\mu^2 + \mu^2\\
    &= n\alpha^2(\sigma^2 + n\mu^2) - 2n\alpha\mu^2 + \mu^2\\
    &= n\paren{\frac{\mu^2}{\sigma^2 + n\mu^2}}^2(\sigma^2 + n\mu^2) - 2n\paren{\frac{\mu^2}{\sigma^2 + n\mu^2}}\mu^2 + \mu^2\\
    &= \frac{n\mu^4}{\sigma^2 + n\mu^2} - \frac{2n\mu^4}{\sigma^2 + n\mu^2} + \mu^2\\
    &= \frac{-n\mu^4}{\sigma^2 + n\mu^2} + \mu^2\\
    &= \frac{-n\mu^4 + \sigma^2\mu^2 + n\mu^4}{\sigma^2 + n\mu^2}\\
    &= \frac{\sigma^2\mu^2}{\sigma^2 + n\mu^2}\\
    &= \frac{\sigma^2}{n}\paren{\frac{n\mu^2}{\sigma^2 + n\mu^2}}
\end{aligned}
\end{equation*}

Note that this final expression for the \(MSE\) is less than the MSE of the \(\overbar{X}\) implying that
our estimator \(\hat\mu = \frac{\mu^2}{\sigma^2 + n\mu^2} \sum X_i \) is a better estimator for \(\mu\) than \(\overbar{X}\).
However, this result seems useless to me as it requires knowledge of the value of \(\mu\) beforehand.

\section{Further Investigation}
\label{develop--math2275:fun:page--best-linear-mean-estimator.adoc---further-investigation}

Okay, lets focus on that last expression for the MSE \(\eqref{1}\) prior to minimizing it. Suppose
that we have some information about \(\mu\), specifically that \(\mu^2 \in [a^2, b^2\)]. Then,
we can infer that

\begin{equation*}
    n\alpha^2\sigma^2 + (n\alpha - 1)^2 a^2 \leq  MSE \leq n\alpha^2\sigma^2 + (n\alpha - 1)^2 b^2
\end{equation*}

Now, we (I) aren't too interested in the lower bound so we can focus on the upper one and
work on minimizing it. In fact, it is identical to the \(\eqref{1}\) we minimized prior, except that \(\mu\)
has been replace with \(b\). We can then use the previous results which minimize the expression. That is

\begin{equation*}
    \alpha = \frac{b^2}{\sigma^2 + n b^2}
    \quad\text{and}\quad
    MSE \leq \frac{\sigma^2}{n}\paren{\frac{n b^2}{\sigma^2 + n b^2}}
\end{equation*}

Again, this is yields a better estimator than \(\overbar{X}\) whilst only imposing the
reasonable restriction that we know an upper bound for \(\mu\). Also, it should
be noted that as \(b \to \infty\) the upper bound of the \(MSE\) tends to \(\tfrac{\sigma^2}{n}\);
so in fact, any bound yield a better estimator for \(\mu\).

Now, the question arises as to what happens if we do not know the value of \(\sigma^2\).
What is to be done in such a case and can we use bounds to yield a better estimator than
\(\overbar{X}\)?

\begin{admonition-remark}[{}]
Okay, so I am yet to investigate this last statement but if I find
that it is indeed possible. Why do we use \(\overbar{X}\) if there are better estimators?
Perhaps we do and I am yet to be exposed to them.
\end{admonition-remark}
\end{document}
