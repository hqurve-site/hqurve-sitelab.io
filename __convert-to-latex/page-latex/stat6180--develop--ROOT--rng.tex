\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Random Number Generation}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In this section, we want to generate numbers from a specific distribution.
We will assume that we can generate iid uniform [0,1] numbers.

\section{Inverse CDF Method}
\label{develop--stat6180:ROOT:page--rng.adoc---inverse-cdf-method}

Intuitively, the \textbf{inverse CDF method} works by mapping the interval \([0,1]\)
onto the support of the desired random variable.
The method is based on the following theorem.

\begin{theorem}[{}]
Let \(X\) be a random variable with cdf \(F(x)\).
Consider the inverse cdf \(F^{-1}(y)\)
defined by

\begin{equation*}
F^{-1}(y) = \min \{ x \in \mathbb{R}: F(x) \geq y \}
\end{equation*}

if an inverse is not readily applicable.
Note that the minimum exists since \(F\) is left continuous.
Also, note that \(F^{-1}\) is monotonic increasing since \(F\) is monotonic increasing.

Let \(U \sim Unif[0,1]\). Then, the random variable \(X'=F^{-1}(U)\)
has the same distribution as \(X\).

\begin{proof}[{}]
We can prove this directly using cdfs

\begin{equation*}
\begin{aligned}
P(F^{-1}(U) \leq x')
&= P(\{u: F^{-1}(u) \leq x'\})
\\&= P(\{u: (\min \{x: F(x) \geq u\}) \leq x'\})
\\&= P(\{u: F(x') \geq u\}) \quad \text{ since } x'\text{ is in the set in the above minimum}
\\&= P(U \leq F(x'))
\\&= F(x')
\end{aligned}
\end{equation*}

So, \(X' = F^{-1}(U)\) has the same cdf as \(X\).
\end{proof}
\end{theorem}

Although the method is the same for all random variables (continuous, discrete and mixed),
it is useful to see the special cases of continuous and discrete random variables.

\subsection{Continuous random variables}
\label{develop--stat6180:ROOT:page--rng.adoc---continuous-random-variables}

Suppose we have a continuous random variable with cdf \(F(x)\) with support \([ a , b ]\).
Then, the inverse function \(F^{-1}\) is well defined on \([ a , b ]\).
To generate a random number,
we

\begin{enumerate}[label=\arabic*)]
\item generate \(u \sim U[0,1]\)
\item set the desired random number to be \(x = F^{-1}(u)\)
\end{enumerate}

\begin{example}[{Random numbers from exponential random variable}]
Consider a \(X\sim Exp(2)\).
Then the pdf is \(f(x) = 2e^{-2x}\) and \(x > 0\).
So, the cdf is \(F(x) = 1-e^{-2x}\)
and the inverse cdf is

\begin{equation*}
F^{-1}(y) = -\frac12 \ln(1-y)
\end{equation*}

So, to generate the values, we can use the following code

\begin{listing}[{}]
n <- 1000
# generate n uniform
U <- runif(n)
# apply inverse to get the samples for the exponential(2) distribution
X <- -log(1-U) / 2
\end{listing}

Note that since \(1-U\) has the same distribution as \(U\),
we could have simply used \texttt{X {\textless}- -log(U) / 2}
\end{example}

\subsection{Discrete random variables}
\label{develop--stat6180:ROOT:page--rng.adoc---discrete-random-variables}

Sampling from discrete random variables is a bit more involved than
continuous random variables.
Conceptually, the idea is to allocate separate \emph{buckets} for
each of the atoms of probability and then choose one of the buckets
randomly based on their probabilities.

Suppose we want to sample from \(X\sim Bin(2, 0.25)\).
This random variable has cdf

\begin{equation*}
F(x) = \begin{cases}
0,&\quad\text{for } x < 0\\
\frac{9}{16},&\quad\text{for } 0 \leq x < 1\\
\frac{15}{16},&\quad\text{for } 1 \leq x < 2\\
1,&\quad\text{for } 2 \leq x\\
\end{cases}
\end{equation*}

So, if we generate \(u\) from a uniform \([0,1]\),
we assign the random value \(x\) by

\begin{equation*}
x = F^{-1}(u)= \begin{cases}
0,&\quad\text{if } 0 < u < \frac{9}{16}\\
1,&\quad\text{if } \frac{9}{16} < u < \frac{15}{16}\\
2,&\quad\text{if } \frac{15}{16} < u < 1\\
\end{cases}
\end{equation*}

Note that each of the intervals for \(u\)
have width equal to \(P(X=x)\).

\section{Rejection Sampling}
\label{develop--stat6180:ROOT:page--rng.adoc---rejection-sampling}

\begin{custom-quotation}[{}][{}]
Rejection sampling is a general algorithm to generate samples from a distribution
with \textbf{target desnity} \(p_1\) based on the ability to generate
random variables from \textbf{trial density} \(p_2\) such that

\begin{equation*}
M = \sup_x \frac{p_1(x)}{p_2(x)} < \infty
\end{equation*}

This is essentially saying that the tails of the trial density are not infinitely fatter
than those of the target density.
The basic algorithm is

\begin{enumerate}[label=\arabic*)]
\item Generate \(U \sim Unif(0,1)\)
\item Generate \(X \sim p_2\)
\item If \(U \leq \frac{p_1(X)}{Mp_2(X)}\), then accept \(X\) as a realization from
    \(p_1\), otherwise try again.
\end{enumerate}
\end{custom-quotation}

\begin{admonition-note}[{}]
In an exam, quote the above
\end{admonition-note}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/rejection-sampling-area}
\end{figure}

To understand why the above algorithm works, consider the following.
Suppose that we are sampling a point \((X,Y)\) uniformly from the area under
\(f\). Then this is equivalent to first sampling \(X\) according to pdf
\(f\) and then sampling \(Y\) uniformly from \((0, f(X))\).
Using this intuition, we can rephrase the algorithm as follows

\begin{enumerate}[label=\arabic*)]
\item Generate \(X \sim p_2\) and \(U \sim (0,1)\).
\item Let \(Y = UMp_2(X)\). Then \((X, Y)\) is a point sampled
    uniformly from the area under \(Mp_2\)
\item Keep \((X,Y)\) is it also lies under \(p_1\).
    That is \(Y \leq p_1(X)\) or \(U \leq \frac{p_1(X)}{Mp_2(X)}\).
    Otherwise, repeat the process
\item Under the condition that \((X,Y)\) lies under \(p_1\),
    \((X,Y)\) is also sampled uniformly from the area under \(p_1\).
\item Set \(X\) to be the random value sampled from \(p_1\).
\end{enumerate}

We can also formally prove that this algorithm indeed works.

\begin{theorem}[{Probability of accepting}]
Suppose that we are sampling density \(p_1\)
by using the trial density \(p_2\) and factor \(M\).
Then

\begin{equation*}
P(accept) = \frac{1}{M}
\end{equation*}

So, although any large enough \(M\) can work. We wish to minimize \(M\)
in order to maximize the amount of accepted values.

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
P(accept)
&= P\left(U \leq \frac{p_1(X)}{Mp_2(X)}\right)
\\&= E_X\left[P\left(U \leq \frac{p_1(X)}{Mp_2(X)} | X\right)\right]
\\&= E_X\left[\frac{p_1(X)}{Mp_2(X)}\right] \quad\text{ since } \forall x: M \geq \frac{p_1(x)}{p_2(x)}
\\&= \int_{\mathbb{R}} \frac{p_1(x)}{Mp_2(x)}p_2(x) \ dx
\\&= \frac{1}{M}\int_{\mathbb{R}} p_1(x) \ dx
\\&= \frac{1}{M}
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{theorem}[{Distribution of output}]
Suppose that we are sampling density \(p_1\)
by using the trial density \(p_2\) and factor \(M\).
Then

\begin{equation*}
f(x|accept) = p_1(x)
\end{equation*}

Therefore, we are indeed sampling from the correct distribution.

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
f(x|accept)
&= \frac{f(x, accept)}{f(accept)}
\\&= \frac{f(accept | x)p_2(x)}{f(accept)} \quad\text{since we sample from } p_2
\\&= \frac{P(accept | x)p_2(x)}{1/M}
\\&= \frac{P\left(U \leq \frac{p_1(x)}{Mp_2(x)} | x\right)p_2(x)}{1/M}
\\&= \frac{\frac{p_1(x)}{Mp_2(x)}p_2(x)}{1/M}
\\&= p_1(x)
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{example}[{Sampling from beta}]
Suppose we want to sample from target distribution
\(Beta(2,2)\) which has pdf \(6x(1-x)\)
using a trial distribution of \(U(0,1)\).
Note that \(M=\frac{3}{2}\) and we can
use the following code
to perform the random number generation

\begin{listing}[{}]
set.seed(100)
n <- 10000
alpha <- 2
beta <- 2
M <- 6/4

x <- runif(n, 0, 1)
u <- runif(n, 0, 1)
accepted <- u <= dbeta(x, alpha, beta) / (M* dunif(x))

accepted.rate <- mean(accepted)

# accepted values
accept <- x[accepted]
hist(accept)
\end{listing}
\end{example}

\section{Metropolis-Hastings Algorithm}
\label{develop--stat6180:ROOT:page--rng.adoc---metropolis-hastings-algorithm}

The Metropolis-Hastings algorithm is a MCMC method for
producing a sequence of random numbers which have a desired
target distribution.
This algorithm requires

\begin{itemize}
\item The \textbf{target} pdf (possibly without normalizing constant) \(f(x)\)
\item The \textbf{proposal} pdf \(g(\cdot | x^{(t)})\),
    which is parameterized by the previous value \(x^{(t)}\).
\end{itemize}

The algorithm starts with an initial value \(x^{(0)}\)
and continues as follows

\begin{enumerate}[label=\arabic*)]
\item Generate \(x^*\) from \(g(\cdot | x^{(t)})\)
\item Compute the \textbf{Metropolis-Hastings ratio}
    \(R(x^{(t)}, x^*)\)
    
    \begin{equation*}
    R(x^{(t)},x^*) = \frac{f(x^*) g(x^{(t)}|x^*)}{f(x^{(t)})g(x^*|x^{(t)})}
    \end{equation*}
\item set the sample value \(x^{(t+1)}\) as
    
    \begin{equation*}
    x^{(t+1)} = \begin{cases}
    x^*, &\quad\text{with probability }min\{R(x^{(t)}, x^*), 1\}\\
    x^{(t)}, &\quad\text{otherwise}
    \end{cases}
    \end{equation*}
\end{enumerate}

\begin{admonition-tip}[{}]
To remember, we can think of the ratio as the probability of \(x^{(t)}\)
over the probability of \(x^{*}\).
\end{admonition-tip}

\begin{admonition-tip}[{}]
We often work on the log scale when computing the ratio.
\end{admonition-tip}

The values produced by the Metropolis-Hastings algorithm
from a markov chain.
In the case that the chain is irreducible and aperiodic,
the limiting distribution is precisely the target distribution
\(f\) (with normalizing constants).
However, since it is a markov chain, two issues arise

\begin{description}
\item[Dependence on initial value] This dependence can be tested by using multiple initial values
    and possibly removing the some initial \textbf{burn-in} period
\item[Autocorrelation] The autocorrelation can be tested for by using acf plots
    and only using every, for example, 100th value.
\end{description}

There are also two special cases of the MH algorithm

\begin{description}
\item[Independent Metropolis-Hastings] This is when the proposal distribution is independent
    of the previous value. That is \(g(. | x^{(t)}) = g(.)\)
\item[Random-walk Metropolis] This is when the proposal distribution is symmetric.
    That is \(g(x|x^{(t)}) = g(x^{(t)}|x)\)
\end{description}

\begin{example}[{Independent MH for gamma using normal}]
We want to sample from a \(Gamma(\alpha,\beta)\) distribution
given that we know how to sample from a normal
distribution.
We want to ensure that we have similar characteristics in the
proposal distribution.
So we use \(N\left(\frac{\alpha}{\beta}, \frac{\alpha}{\beta^2}\right)\).
So, the MH ratio is

\begin{equation*}
r(x^{(t)}, x^*) =
\frac{p(x^*)g(x^{(t)}|x^*)}{p(x^{(t)})g(x^*|x^{(t)})}
=\frac{p(x^*)g(x^{(t)})}{p(x^{(t)})g(x^*)}
\end{equation*}

where \(p\) is the density of the gamma distribution
and \(g\) is the density of the normal distribution.

\begin{listing}[{}]
gamm <- function(n, a, b) {
    mu <- a/b
    sig <- sqrt(a/b^2)

    output <- rep(NA, n)
    # set the initial as the average
    x <- mu
    output[1] <- x
    for (i in 2:n) {
        # get  proposed value
        x.star <- rnorm(1, mu, sig)
        # calculate ratio
        r <- (
            (dgamma(x.star, a, b) * dnorm(x, mu, sig))
            / (dgamma(x, a, b) * dgamma(x, mu, sig))
        )

        # check if to add
        if (runif(1) < r) {
            x <- x.star
        }
        output[i] <- x
    }
    return(output)
}

set.seed(100)
vec <- gamm(100000, 2.3, 2.4)
hist(vec, seq(0, 10, 0.1), col=rgb(0,0,1,1/4), xlim=c(0,5))
hist(rgamma(100000, 2.3, 2.4), seq(0, 10, 0.1),
     col=rgb(1,0,0,1/4), xlim=c(0,5), add=TRUE)
legend(4, 0.6, legend=c("MH", "rgamma"), fill=c("blue", "red"))

acf(vec)
\end{listing}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/MH-independent}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/MH-independent-acf}
\end{figure}

The distribution of the output is pretty okay. But there is significant autocorrelation.
This autocorrelation is due to the fact that we often repeat values.
\end{example}

\begin{example}[{Random walk MH for gamma using normal}]
We want to sample from a \(Gamma(\alpha,\beta)\) distribution
given that we know how to sample from a normal
distribution with mean \(x^{(t)}\).
We want to ensure that we have similar characteristics in the
proposal distribution.
So we use \(N\left(x^{(t)}, \frac{\alpha}{\beta^2}\right)\).
So, the MH ratio is

\begin{equation*}
r(x^{(t)}, x^*) =
\frac{p(x^*)g(x^{(t)}|x^*)}{p(x^{(t)})g(x^*|x^{(t)})}
=\frac{p(x^*)}{p(x^{(t)})}
\end{equation*}

where \(p\) is the density of the gamma distribution

\begin{listing}[{}]
gamm <- function(n, a, b) {
    mu <- a/b
    sig <- sqrt(a/b^2)

    output <- rep(NA, n)
    # set the initial as the average
    x <- mu
    output[1] <- x
    for (i in 2:n) {
        # get  proposed value
        x.star <- rnorm(1, x, sig)
        # calculate ratio
        r <- dgamma(x.star, a, b) / dgamma(x, a, b)

        # check if to add
        if (runif(1) < r) {
            x <- x.star
        }
        output[i] <- x
    }
    return(output)
}

set.seed(100)
vec <- gamm(100000, 2.3, 2.4)

hist(vec, seq(0, 10, 0.1), freq=FALSE, col=rgb(0,0,1,1/4), xlim=c(0,5))
hist(rgamma(100000, 2.3, 2.4), seq(0, 10, 0.1),
    freq=FALSE, col=rgb(1,0,0,1/4), xlim=c(0,5), add=TRUE)
legend(4, 0.6, legend=c("MH", "rgamma"), fill=c("blue", "red"))

acf(vec)
\end{listing}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/MH-random-walk}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/MH-random-walk-acf}
\end{figure}

The distribution of the output is much better than the independent version.
But there is much more autocorrelation.
\end{example}

\subsection{Application to Bayesian inference}
\label{develop--stat6180:ROOT:page--rng.adoc---application-to-bayesian-inference}

The MH algorithm is particularly useful for Bayesian inference
since

\begin{itemize}
\item The posterior distribution of a parameter may not be of a well known family
\item The MH algorithm does not require the normalizing constant of the pdf
\end{itemize}

\begin{example}[{Random walk MH for gamma using normal}]
Suppose we sample \(Y\sim Binom(n, \theta)\) where \(\theta \sim Beta(0.5, 0.5)\).
The posterior distribution is therefore

\begin{equation*}
f(\theta|y) \propto \theta^{y-0.5}(1-\theta)^{n-y-0.5}I(0 < \theta < 1)
\end{equation*}

Note that this is a \(Beta(y+0.5, n-y+0.5)\).
Suppose we didn't know this.

So, we sample from the distribution using proposal
\(\theta^* \sim N(\theta^{(t)}, 0.4^2)\).
Since this proposal is symmetric, the ratio is

\begin{equation*}
r = \frac{f(\theta^*|y)}{f(\theta^{(t)}|y)}
\end{equation*}

In the below code, we use the log scale for the ratio.

\begin{listing}[{}]
n <- 10000
set.seed(100)

# use log scale for ratio
log_f <- function(theta, y=3, n=10) {
    if (theta < 0 || theta > 1) return(-Inf)
    (y-0.5)*log(theta) + (n-y-0.5)*log(1-theta)
}

theta <- 0.5 # initial
output <- rep(NA, n)
for (i in 1:n) {
    theta.star <- rnorm(1, theta, 0.4)
    logr <- log_f(theta.star) - log_f(theta)

    if (log(runif(1)) < logr) {
        theta <- theta.star
    }
    output[i] <- theta
}

hist(output, seq(0, 1, 0.05), freq=FALSE,
    col=rgb(0,0,1,1/4), xlim=c(0,1), ylim=c(0, 3))
hist(rbeta(n, 3+0.5, 10-3+0.5), seq(0, 1, 0.05),
    freq=FALSE, col=rgb(1,0,0,1/4), xlim=c(0,1), add=TRUE)
legend(0.7, 0.6, legend=c("MH", "rbeta"), fill=c("blue", "red"))

acf(output)
\end{listing}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/MH-bayesian-posterior-independent}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6180/ROOT/MH-bayesian-posterior-independent-acf}
\end{figure}

This is a very good sample,
but there is significant autocorrelation.
\end{example}

\section{Specialized Algorithms}
\label{develop--stat6180:ROOT:page--rng.adoc---specialized-algorithms}

\subsection{Box-Muller Algorithm}
\label{develop--stat6180:ROOT:page--rng.adoc---box-muller-algorithm}

The Box-Muller algorithm is a special algorithm
which generates two independent standard normal random variables.

If \(Y_1, Y_2 \sim U(0,1)\).
Then,

\begin{equation*}
\begin{aligned}
X_1 &= \sqrt{-2\log Y_1} \cos (2\pi Y_2)\\
X_2 &= \sqrt{-2\log Y_1} (\sin 2\pi Y_2)\\
\end{aligned}
\end{equation*}

are independent \(N(0,1)\) random variables.
\end{document}
