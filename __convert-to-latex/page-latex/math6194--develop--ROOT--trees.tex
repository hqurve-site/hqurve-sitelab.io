\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Trees and generating functions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Recall that a \textbf{generating function} for the sequence \(\{a_n\}_{n=0}^\infty\)
is defined as

\begin{equation*}
\sum_{n=0}^\infty a_nx^n
\end{equation*}

and the \textbf{exponential generating function} for the same sequence is defined
as

\begin{equation*}
\sum_{n=0}^\infty a_n \frac{x^n}{n!}
\end{equation*}

In this section, we will look at classes of graphs, in particular trees,
and use generating functions to determine how many of these trees exist.
We define the following

\begin{description}
\item[Tree] A tree is an acyclic connected graph
\item[Rooted tree] A tree in which one node is called the \textbf{root}. In a rooted tree,
    we can speak of the height. The height is the maximum path length from the root node
    to another node. Note that the height of a tree with one node is \(0\).
\item[Planted tree] A \emph{rooted tree} in which the order of subtrees of each node is important.
\item[Labeled tree] A tree in which the nodes are assigned labels. Typically \(1\ldots n\).
\item[Full binary tree] A rooted tree where each node as either 0 or 2 children
\end{description}

\section{Pruning}
\label{develop--math6194:ROOT:page--trees.adoc---pruning}

Let \(T\) be a tree.
\textbf{Pruning} refers to the process of removing all the end nodes
of a tree simultaneous.
Note that it only makes sense to prune trees with at least three nodes
and the result is a subtree of the original.

If we repeat this process on a given tree, we will eventually end up
with \(K_1\) or \(K_2\).

\begin{description}
\item[Central] We call the tree \textbf{central} if \(K_1\) is obtained.
    The original node which is now the final last node is called the \textbf{centeral node}
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-math6194/ROOT/pruning-central}
    \end{figure}
\item[Bicentral] We call the tree \textbf{bicentral} if \(K_2\) is obtained.
    The original nodes which are now the final two nodes are called the \textbf{centers}
    and their connecting edge is called the \textbf{center edge}
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-math6194/ROOT/pruning-bicentral}
    \end{figure}
\end{description}

\section{Enumeration of rooted trees}
\label{develop--math6194:ROOT:page--trees.adoc---enumeration-of-rooted-trees}

Let \(r_n\) be the number of rooted trees with \(n\geq 1\) vertices.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6194/ROOT/enumeration-rooted-trees}
\caption{Illustration of small values of \(r_n\)}
\end{figure}

Note that root trees with \(n+1\) nodes are composed of a root as well as some
number of subtrees.
Let \(j_k\) denote the number of subtrees with \(k\) nodes.
Then, we must have that \(j_1 + 2j_2 + \ldots nj_n=n\).
Also, since there are \(r_k\) trees to choose from with repetition,
the number of ways to select the \(j_k\) subtrees
is \(\binom{r_k + j_k -1}{r_k-1}\) and

\begin{equation*}
r_{n+1} = \sum_{j_1+2j_2+\cdots nj_n = n} \prod_{k=1}^n \binom{r_k + j_k -1}{r_k-1}
\end{equation*}

\begin{admonition-note}[{}]
This formula also works in the degenerate case when \(n=0\),
since there is exactly one term in the summation and the empty product is \(1\).
Alternatively, we could let the \(j_k\) simply be an infinite sequence.
In such a case, we have exactly one term in the summation and the infinite product of \(1\)s.
\end{admonition-note}

Therefore, we obtain the following generating function for \(r_n\)

\begin{equation*}
\begin{aligned}
r(x)
&= \sum_{n=1}^\infty r_nx^n
\\&= \sum_{n=0}^\infty r_{n+1}x^{n+1}
\\&= \sum_{n=0}^\infty x^{n+1}\sum_{j_1+2j_2+\cdots nj_n = n} \prod_{k=1}^n \binom{r_k + j_k -1}{r_k-1}
\\&= \sum_{n=0}^\infty x\sum_{j_1+2j_2+\cdots nj_n = n} \prod_{k=1}^n \binom{r_k + j_k -1}{r_k-1}x^{kj_k}
\\&= x \sum_{n=0}^\infty\sum_{j_1+2j_2+\cdots nj_n = n} \prod_{k=1}^n \binom{r_k + j_k -1}{r_k-1}x^{kj_k}
\\&= x \sum_{n=0}^\infty\sum_{j_1+2j_2+\cdots nj_n = n} \prod_{k=1}^\infty \binom{r_k + j_k -1}{r_k-1}x^{kj_k}
\\&= x \sum_{j_1+2j_2+ \cdots < \infty} \prod_{k=1}^\infty \binom{r_k + j_k -1}{r_k-1}x^{kj_k}
\\&= x \sum_{j_1,j_2,\ldots} \prod_{k=1}^\infty \binom{r_k + j_k -1}{r_k-1}x^{kj_k}
\hspace{5em}(\star)
\\&= x \prod_{k=1}^\infty \left(\sum_{j_k=0}^\infty \binom{r_k + j_k -1}{r_k-1}x^{kj_k}\right)
\\&= x \prod_{k=1}^\infty (1-x^k)^{-r_k}
\end{aligned}
\end{equation*}

Note that the step at \((\star)\) is not really correct; but we can ignore infinite products in the final result.

\begin{admonition-tip}[{}]
This result can be obtained more simply using Polya's enumeration theorem as done in
\myautoref[{Math3610}]{develop--math3610:cycle-index:page--rooted-trees.adoc}.
\end{admonition-tip}

\section{Enumeration of labelled rooted trees}
\label{develop--math6194:ROOT:page--trees.adoc---enumeration-of-labelled-rooted-trees}

Let \(R_n\) be the number of labelled rooted trees with \(n\) nodes.

\begin{figure}[H]\centering
\includegraphics[width=0.7\linewidth]{images/develop-math6194/ROOT/enumeration-labeled-rooted-trees}
\caption{Illustration of small values of \(R_n\)}
\end{figure}

In order to determine \(R_n\), we define \(R_n(m)\)
to be the number of labelled rooted trees with \(n\) nodes
and \(m\) subtrees at the root.
So, for \(n \geq 2\)

\begin{equation*}
R_n = \sum_{m=1}^{n-1} R_n(m)
\end{equation*}

Suppose that the subtrees are ordered and have \(j_1, \ldots j_m\) nodes respectively,
where the \(j_k \geq 1\).
First, we choose the root label in one of \(n\) ways.
Then, we can assign the labels to the subtrees in \(\binom{n-1}{j_1,\ldots j_m}\) ways.
Also, there are \(R_{j_k}\) ways to arrange the \(j_k\) nodes in the \(k\)'th subtree.
Therefore, there are \(\binom{n-1}{j_1,\ldots j_m} R_{j_1}\cdots R_{j_m}\) ways to have the subtrees
in some order.
But since we don't care about the order of the subtrees,
we must divide by \(m!\). Note that this deduplication is on the chosen labels and not on the sizes of the subtrees.
Therefore

\begin{equation*}
R_n(m)
= n \sum_{\begin{array}{c}j_1+\cdots j_m=n-1\\j_k\geq 1\end{array}}\binom{n-1}{j_1,\ldots j_m} \frac{R_{j_1}\cdots R_{j_m}}{m!}
= \frac{n!}{m!} \sum_{\begin{array}{c}j_1+\cdots j_m=n-1\\j_k\geq 1\end{array}} \prod_{k=1}^m \frac{R_{j_k}}{j_k!}
\end{equation*}

for \(n \geq 2\).
We use the exponential generating function

\begin{equation*}
\begin{aligned}
R(x)
&= \sum_{n=1}^\infty R_n\frac{x^n}{n!}
\\&= R_1 x + \sum_{n=2}^\infty R_n\frac{x^n}{n!}
\\&= R_1 x + \sum_{n=2}^\infty \sum_{m=1}^{n-1} \frac{R_n(m)}{n!}x^n
\\&= R_1 x + \sum_{m=1}^\infty \sum_{n=m+1}^\infty \frac{R_n(m)}{n!}x^n
\\&= R_1 x + x\sum_{m=1}^\infty \frac{1}{m!} \sum_{n=m+1}^\infty \sum_{\begin{array}{c}j_1+\cdots j_m=n-1\\j_k\geq 1\end{array}} \prod_{k=1}^m \frac{R_{j_k} x^{j_k}}{j_k!}
\\&= R_1 x + x\sum_{m=1}^\infty \frac{1}{m!} \sum_{n=m}^\infty \sum_{\begin{array}{c}j_1+\cdots j_m=n\\j_k\geq 1\end{array}} \prod_{k=1}^m \frac{R_{j_k} x^{j_k}}{j_k!}
\\&= R_1 x + x\sum_{m=1}^\infty \frac{1}{m!} \prod_{k=1}^m \sum_{j_k=1}^\infty \frac{R_{j_k} x^{j_k}}{j_k!}
\\&= R_1 x + x\sum_{m=1}^\infty \frac{1}{m!} \prod_{k=1}^m R(x)
\\&= R_1 x + x\sum_{m=1}^\infty \frac{1}{m!} R(x)^m
\\&= x + x\sum_{m=1}^\infty \frac{1}{m!} R(x)^m
\\&= x\sum_{m=0}^\infty \frac{1}{m!} R(x)^m
\\&= x\exp(R(x))
\end{aligned}
\end{equation*}

\begin{admonition-todo}[{}]
Determine \(R_n\) from the above relation
\end{admonition-todo}

\begin{example}[{Computation using sagemath}]
This series can be computed automatically for us using the above relation and sagemath

\begin{listing}[{}]
# set to use rationals
P.<x> = LazyPowerSeriesRing(QQ)

# Create power series to be evaluated at x=0
R = P.undefined(0)

# specify recurrence relation
R.define(x * exp(R))

# inspect result
[R[n] * factorial(n) for n in range(1,11)]

# [1, 2, 9, 64, 625, 7776, 117649, 2097152, 43046721, 1000000000]
\end{listing}
\end{example}

\subsection{Alternative derivation}
\label{develop--math6194:ROOT:page--trees.adoc---alternative-derivation}

\begin{admonition-todo}[{}]
I am not sure which book this comes from. It was just sent as a text message.
\end{admonition-todo}

Let \(S_n\) be the set of \((n-1)\)-tuples consisting of the edges
of labelled rooted trees with \(n\) nodes.
Then, we can count \(|S_n|\) in one of two ways

\begin{itemize}
\item \(|S_n| = (n-1)!R_n\)
    since each of these trees have \(n-1\) edges which can be arranged in \((n-1)!\) ways.
\item \(|S_n| = \prod_{k=1}^{n-1}n (n-k)\) where in the \(k\)'th iteration, we choose a start and end node for the \(k\)'th edge.
    In the first iteration, we choose any node to be the start and any of the \((n-1)\) remaining edges to be the end.
    In each later operation (\(k\geq 2\)), we can again choose any node to be the start but the end must
    
    \begin{itemize}
    \item not have been chosen to be an end node as yet; note that at the beginning of the \(k\)'th iteration, \(k-1\) end nodes have been chosen already.
    \item not be the node that we chose to be the start.
    \end{itemize}
    
    In total, there are \(k\) nodes which we cannot chose to be end node in the \(k\)'th iteration.
\end{itemize}

Therefore, we have that the number of labelled rooted trees with \(n\) nodes is

\begin{equation*}
R_n = \frac{|S_n|}{(n-1)!}
= \frac{1}{(n-1)!}\prod_{k=1}^{n-1}n (n-k)
= \frac{1}{(n-1)!}n^{n-1}(n-1)!
= n^{n-1}
\end{equation*}

From this, we also see that the number of labelled unrooted trees with \(n\) nodes
is \(n^{n-2}\) since we can choose any of the \(n\) nodes to be the root.

\section{Enumeration of full binary trees}
\label{develop--math6194:ROOT:page--trees.adoc---enumeration-of-full-binary-trees}

Let \(b_n\) be the number of full binary trees with \(n\) vertices.

Consider \(b_{n+1}\).
If \(n\geq 2\), there must be two subtrees

\begin{itemize}
\item If \(n\) is odd, we can say that the sizes of the subtrees are \(s< t\).
    We can choose the trees from any of the \(b_s\)  and \(b_t\) respectively.
    Then
    
    \begin{equation*}
    b_{n+1} = b_1b_{n-1} + b_2b_{n-2} + \cdots + b_{\frac{n-1}{2}} b_{\frac{n+1}{2}}
    \end{equation*}
\item If \(n\) is even, the above works, but there is an extra case where \(s=t\).
    Then, we must choose \(2\) from \(b_{s}\) with repetition; this is \(\binom{b_s+1}{2}\),
    So
    
    \begin{equation*}
    \begin{aligned}
    b_{n+1}
    &= b_1b_{n-1} + b_2b_{n-2} + \cdots + b_{\frac{n}{2} - 1} b_{\frac{n}{2} + 1} + \binom{b_{\frac{n}{2}}+1}{2}
    \\&= b_1b_{n-1} + b_2b_{n-2} + \cdots + b_{\frac{n}{2} - 1} b_{\frac{n}{2} + 1} + \frac{b_{\frac{n}{2}}^2 + b_{\frac{n}{2}}}{2}
    \end{aligned}
    \end{equation*}
\end{itemize}

Let \(b(x)\) be the (ordinary) generating function of \(\{b_n\}\).
Consider \(\frac12 b(x)^2 + \frac12 b(x^2)\) and the coefficient of \(x^n\)

\begin{itemize}
\item If \(n\) is odd, it is
    
    \begin{equation*}
    \sum_{s < t,\ s+t=n} b_sb_t
    \end{equation*}
    
    This is precisely \(b_{n+1}\)
\item If \(n\) is even, it is
    
    \begin{equation*}
    \frac12\sum_{s+t=n} b_sb_t + \frac12 b_{\frac{n}{2}}
    = \sum_{s < t,\ s+t=n} b_sb_t + \frac12 b_{\frac{n}{2}}^2+ \frac12 b_{\frac{n}{2}}
    \end{equation*}
    
    This is again \(b_{n+1}\)
\end{itemize}

Therefore, we have that

\begin{equation*}
\begin{aligned}
b(x)
&= \sum_{n=1}^\infty b_n x^n
\\&= b_1x + \sum_{n=2}^\infty b_n x^n
\\&= b_1x + x\sum_{n=1}^\infty b_{n+1} x^n
\\&= b_1x + x\left(\frac12 b(x)^2 + \frac12 b(x^2)\right)
\\&= x + \frac{x}2 b(x)^2 + \frac{x}2 b(x^2)
\end{aligned}
\end{equation*}

Note that we know that \(b_1=1\) by inspection.
Also, by induction we can see that the \(b_n\) when \(n\) is even is \(0\).

\begin{example}[{Computation using sagemath}]
This series can be computed automatically for us using the above relation and sagemath

\begin{listing}[{}]
# set to use integers
P.<x> = LazyPowerSeriesRing(ZZ)

# Create power series to be evaluated at x=0
b = P.undefined(0)

# specify recurrence relation
b.define(x + x/2 * b^2 + x/2 * b(x^2))

# inspect result
b[1:20]

# [1, 0, 1, 0, 1, 0, 2, 0, 3, 0, 6, 0, 11, 0, 23, 0, 46, 0, 98]
\end{listing}
\end{example}

\section{Prufers Code}
\label{develop--math6194:ROOT:page--trees.adoc---prufers-code}

Consider a labelled tree \(T\).
If we want to serialize this tree for data transmission or storage, we can take one of a few approaches such as:

\begin{itemize}
\item A recursive data structure of the form \texttt{Tree(n, [Tree])}.
    This form is relatively simple but there are multiple ways to represent the same tree.
\item A list of edges \texttt{(n,m)}.
    Accidents may occur with this form as this structure can also be used to encode any arbitrary graph.
\end{itemize}

In 1918, \href{https://en.wikipedia.org/wiki/Heinz\_Pr\%C3\%BCfer}{Prüfer} developed an encoding scheme which can be used to uniquely represent
labelled trees.
Suppose our tree has \(n\) nodes labelled \(L_n=1,\ldots n\).
The Prufer code of the tree belongs to the set \(L_n^{n-2}\) (sequences of length \(n-2\)).
To generate the Prufer code, we repeatedly remove nodes from the tree until there are only two nodes left.
Specifically,

\begin{enumerate}[label=\arabic*)]
\item Consider the set of end nodes of the tree.
\item Select the node with the smallest label
\item Record the label of the adjacent node and remove the selected node
\item Repeat while there are more than two nodes
\end{enumerate}

Note that we do not record the label of the node being removed, but the adjacent node.
We often conduct this process using a table which has a column for the code so far (initially empty)
and another for the labels of the end vertices (usually

\begin{example}[{Prufer code generation}]
Consider the following graph

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6194/ROOT/prufer-original}
\end{figure}

Then, we generate the code with the following table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Code} & {Labels at end-vertices} \\
\hline[\thicktableline]
{} & {4,5,6,7,8} \\
\hline
{3} & {5,6,7,8} \\
\hline
{3,3} & {6,7,8} \\
\hline
{3,3,1} & {7,8} \\
\hline
{3,3,1,3} & {3,8} \\
\hline
{3,3,1,3,2} & {2,8} \\
\hline
{3,3,1,3,2,1} & {1,8 (stop)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Note we stop when the remaining graph only has two nodes, 1 and 8.
The Prufer code is therefore \texttt{331321} which has length 6 since there are 8 nodes
\end{example}

Decoding the Prufer code is also done using a table with an iterative method.
There are two columns,

\begin{itemize}
\item \(S\) consisting of the remaining code
\item \(\overbar{S}\) which initially consists of elements of \(L_n\) which are not in \(S\)
\end{itemize}

We then conduct the following procedure

\begin{enumerate}[label=\arabic*)]
\item Add an edge from the first element of \(S\) to the smallest element of \(\overbar{S}\).
\item Remove the smallest element of \(\overbar{S}\).
\item Remove the first element, \(i\), of \(S\). If \(i\) does not exist in \(S\), add it to \(\overbar{S}\).
\item Repeat the above while \(S\) is non-empty.
\item When \(S\) is empty, \(\overbar{S}\) will have two elements. Add an edge between these two elements.
\end{enumerate}

\begin{example}[{Decoding process}]
We will decode \texttt{331321} which was generated in the previous example.
Since the code is 6 elements long, the nodes have numbers \(J_8\).
So, the table is

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(S\)} & {\(\overbar{S}\)} & {Action} \\
\hline[\thicktableline]
{331321} & {4,5,6,7,8} & {Add \((3,4)\)} \\
\hline
{31321} & {5,6,7,8} & {Add \((3,5)\)} \\
\hline
{1321} & {6,7,8} & {Add \((1,6)\)} \\
\hline
{321} & {7,8} & {Add \((3,7)\)} \\
\hline
{21} & {3,8} & {Add \((2,3)\)} \\
\hline
{1} & {2,8} & {Add \((1,2)\)} \\
\hline
{} & {1,8} & {Add \((1,8)\) and stop} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Note that the edges are the same as in the original graph.
\end{example}

\begin{lemma}[{}]
Consider tree \(T\) with \(n\) vertices with label set \(L\).
Let \(S = (p_1, \ldots p_{n-2}) \in L^{n-2}\) be the Prufer code of \(T\).
Let \(\overbar{S} = L - S = \{q_1, \ldots q_r\}\) with \(q_1 < q_2 < \cdots < q_r\).
Then,

\begin{enumerate}[label=\arabic*)]
\item \(\overbar{S}\) is the set of labels of end vertices
\item \(q_1\) is the first label to be removed
\item \(p_1\) is the vertex adjacent to \(q_1\)
\item The remaining tree \(T - \{q_1\}\) has Prufer code \((p_2, \ldots p_{n_2})\)
    with labels \(L - \{q_1\}\)
\end{enumerate}

\begin{proof}[{}]
We will prove this by induction on \(n\) for \(n\geq 3\).

For the base case, there are only thee possible labelled trees with \(3\) nodes.

\begin{itemize}
\item \texttt{1 -- 2 -- 3}. In this case, \(S = (2)\) and \(\overbar{S} = \{1,3\}\)
\item \texttt{2 -- 1 -- 3}. In this case, \(S = (1)\) and \(\overbar{S} = \{2,3\}\)
\item \texttt{1 -- 3 -- 2}. In this case, \(S = (3)\) and \(\overbar{S} = \{1,2\}\)
\end{itemize}

In all three cases the lemma holds.

Now, suppose that the lemma holds for some \(n \geq 3\). Consider a tree on \(n+1\) nodes.
Let \(q_1\) be the first label to be removed and \(p_1\) be the label it was attached to.
Then \(q_1 \in \overbar{S}\) and \(p_1\) is the first item in the prufer code (we have proven the second and third properties).
Also, the remaining tree clearly has nodes \(T - \{q_1\}\)
and the remainder of the prufer code does not depend on the first step; so the final property holds.

Let \(\overbar{S}'\) be the value of \(\overbar{S}\) for \(T - \{q_1\}\).
Since the only difference between \(T\) and \(T-\{q_1\}\) (with respect to end vertices)
is that \(p_1\) may now be an end vertex, we know that the end vertices of \(T\)
are \((\overbar{S}' - \{p_1\}) \cup \{q_1\}\).
However, we know that

\begin{equation*}
\overbar{S}
= L - (p_1, p_2, \ldots p_{n-2})
= ((L-\{q_1\}) - (p_2, \ldots p_{n-2}) - \{p_1\})\cup \{q_1\}
= (\overbar{S}' - \{p_1\})\cup \{q_1\}
\end{equation*}

Since \(T-\{q_1\}\) is a tree with labels \(L - \{q_1\}\).
Therefore, we must have that \(\overbar{S}\) are the end vertices of \(T\).
Therefore, we have proven the first property and the result holds by induction.
\end{proof}
\end{lemma}

\begin{theorem}[{Prufer decoding and coding processes are inverses of each other}]
The decoding process is the inverse of the prufer encoding scheme.

\begin{admonition-todo}[{}]
Proof
\end{admonition-todo}
\end{theorem}

\begin{corollary}[{}]
There are exactly \(n^{n-2}\) labelled unrooted trees with \(n\) nodes.
\end{corollary}

\section{Otter's Theorem}
\label{develop--math6194:ROOT:page--trees.adoc---otters-theorem}

We call two graphs isomorphic iff there exists a bijection between the vertices that
preserve adjacency.
An automorphism is an isomorphism that maps a graph to itself.

We say that two vertices are \textbf{similar} if there is a graph automorphism that swaps them.
From this definition, it is clear that the vertices of a graph can be grouped into
equivalence classes based on similarity.
A \textbf{symmetry edge} is an edge that join two similar vertices.
Note that for trees, the only possible symmetry edge is a bicentral edge.

We may also say that two edges are similar if there is a graph automorphism that swaps them.

\begin{theorem}[{Otter's formula}]
Let \(T\) be a tree and

\begin{itemize}
\item \(p^*\) be the number of vertex similarity classes
\item \(q^*\) be the number of edge similarity classes
\item \(s\) be the number of symmetry edges. Note \(s \in \{0,1\}\)
\end{itemize}

Then,

\begin{equation*}
p^* - (q^* - s) = 1
\end{equation*}

\begin{proof}[{}]
We prove when \(s=0\) and \(s=1\) separately.

If there is no symmetry edge, \(s=0\) and there is either one central
vertex or two dissimilar central vertices.
Note that there is a subtree which contains exactly one vertex from each similarity
class.
To do this, select one of the central vertices to be the root
and remove duplicate similar nodes one at a time.
Note that it is possible to always remove parents after children
since if two parents are similar, their children are also similar.
In the end, we have a tree with \(p^*\) nodes.
Also, note that one of each of the similar edges will be preserved.
So there are \(q^*\) edges.
Since in trees there is one more node that vertex

\begin{equation*}
p^* - q^* = 1  \implies p^* - (q^*-s) = 1
\end{equation*}

Next, if there is a symmetry edge, \(s=1\).
In this case, the subtree which contains exactly one vertex from each similarity class
is also possible, but there is one edge missing, the bicentral edge.
So, this subtree has \(p^*\) nodes and \(q^*-1\) edges.
So, again

\begin{equation*}
p^* - (q^* -1) = 1\implies p^*-(q^*-s)=1
\end{equation*}

In both cases, we get the desired result.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(r(x)\) be the generating function for the number of rooted trees with
\(n\) vertices and \(t(x)\) be the generating function for the number
of unrooted trees with \(n\) vertices.
Then,

\begin{equation*}
t(x) = r(x) - \frac12 \left[r(x)^2 - r(x^2)\right]
\end{equation*}

\begin{proof}[{}]
Consider trees with \(n\) vertices
and define \(p^*_i\), \(q_i^*\) and \(s_i\) to be the as defined in the previous
theorem for the \(i\)'th unrooted tree with \(n\) vertices.
Then,

\begin{equation*}
t_n = \sum_{i=1}^{t_n} 1
= \sum_{i=1}^{t_n} p_i^* - \sum_{i=1}^{t_n} (q_i^* - s_i)
\end{equation*}

Note that \(\sum_{i=1}^{t_n} p_i^*= r_n\) since each node of each tree
is a valid root and nodes from the same similarity class yield the same rooted tree.
Therefore,

\begin{equation*}
t_n = r_n - \sum_{i=1}^{t_n} (q_i^* - s_i)
\end{equation*}

Next, note that \(q_i^*\) may be thought of the number of edge rooted trees
that can be formed and \(q_i^* - s_i\) is the number of edge
rooted trees where the root edge is not a symmetry edge.
So \(\sum_{i=1}^{t_n} (q_i^* - s_i)\) is the number of edge rooted trees with \(n\)
vertices that are not rooted at a symmetry edge.

To form such an edge rooted tree, we can take two dissimilar
rooted trees with \(j\geq1\) and \(n-j\geq 1\) nodes respectively.
If \(n\) is even, we must be careful when \(j = \frac{n}{2}\)
to avoid forming a symmetry edge.
So when \(j=\frac{n}{2}\), we can choose from one of \(\binom{r_{\frac{n}{2}}}{2}\) possible pairs.
So,

\begin{equation*}
\begin{aligned}
\sum_{i=1}^{t_n} (q_i^* - s_i)
&= \begin{cases}
r_1r_{n-1} + r_2r_{n-2} + \cdots r_{\frac{n-1}{2}}r_{\frac{n+1}{2}},&\quad\text{if } n\text{ odd}
\\
r_1r_{n-1} + r_2r_{n-2} + \cdots r_{\frac{n}{2}-1}r_{\frac{n}{2} + 1} + \frac12 r_{\frac{n}{2}}r_{\frac{n}{2}} - \frac12 r_{\frac{n}{2}},&\quad\text{if } n\text{ even}
\end{cases}
\\
&= \begin{cases}
r_1r_{n-1} + r_2r_{n-2} + \cdots r_{\frac{n-1}{2}}r_{\frac{n+1}{2}},&\quad\text{if } n\text{ odd}
\\
r_1r_{n-1} + r_2r_{n-2} + \cdots r_{\frac{n}{2}-1}r_{\frac{n}{2} + 1} + \frac12 r_{\frac{n}{2}}r_{\frac{n}{2}} + \frac12 r_{\frac{n}{2}} - r_{\frac{n}{2}},&\quad\text{if } n\text{ even}
\end{cases}
\\
&= \begin{cases}
\text{coeff of } x^n \text{ in } \frac12 r(x)^2,&\quad\text{if } n\text{ odd}
\\
\text{coeff of } x^n \text{ in } \frac12 \left[r(x)^2 - r(x^2)\right],&\quad\text{if } n\text{ even}
\end{cases}
\\
&=
\text{coeff of } x^n \text{ in } \frac12 \left[r(x)^2 - r(x^2)\right]
\end{aligned}
\end{equation*}

So

\begin{equation*}
\begin{aligned}
&t_n = r_n - \text{coeff of } x^n \text{ in } \frac12 \left[r(x)^2 - r(x^2)\right]
\\&\implies
t(x) = r(x) -  \frac12 \left[r(x)^2 - r(x^2)\right]
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}
\end{document}
