\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Axiom of Extension}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
\begin{custom-quotation}[{}][{}]
Two sets are equal if and only if they have the same elements
\end{custom-quotation}

How do you begin to interpret this?
A first approach, this is merely a definition. Of course, when we say two sets are equal,
we mean that they have the same elements. However, what do the words 'equal' and 'same'
mean?

\section{Definition of equal}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---definition-of-equal}

Well, firstly, in one \href{https://www.merriam-webster.com/dictionary/equal}{english dictionary},
equality has several meanings

\begin{itemize}
\item of the same measure, quantity, amount or number
\item identical in mathematical value or logical definition: (equivalent)
\item regarding or affecting all objects in the same way
\end{itemize}

The first and second definitions are very similar. However,
slightly different. The first means that the for any two 'equal' objects,
they satisfy some similar property. We usually denote this as
'equivalence' in equivalence classes. The second however seems to allude to
a sort of isomorphism. For example the real number \(2\) and matrix
\(\begin{pmatrix} 2 & 0 \\ 0 & 1\end{pmatrix}\)
are in a sense identical when considering the isomorphic fields \(\mathbb{R}\)
and \(\left\{\begin{pmatrix}x & 0 \\ 0 & 1\end{pmatrix}\ :\ x\in \mathbb{R}  \right\}\).
However, neither of these two definitions capture what we want when we say two sets
are equal.

Perhaps the third definition does better justice. Two objects
are equal if they affect everything in the same way. More precisely,
two objects are equal if for any predicate, both objects yield the same truthness.
This is a very nice definition and really captures what it means to be equal;
to be equal in all ways. When we say \(x = 2\). We mean that \(x\)
and \(2\) can be used interchangeably, in any situation, without consequence.
This is the definition we are looking for

Now, back to sets. Two sets are equal mean that they can be used interchangeably without
consequence. But doesn't this mean that they are the same thing.

\begin{admonition-note}[{}]
There is a difference. Is there just one \(2\) or multiple \(2\)'s 'existing'
in the universe?
\end{admonition-note}

Yes... but does it matter? Yes, it does matter. Suppose there are multiple 'equal'
objects, well in this case, since two sets are equal iff they have the same elements,
if we have one set with duplicate elements while another without duplicate.
Then clearly these two sets are not equal. However... they have the 'same' elements.

\section{What does term 'same elements' mean?}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---what-does-term-same-elements-mean}

Sets with and without duplicates. Are they equal. Yes, they clearly have the 'same' elements.
Same.... Yes of course these two sets are equal,
there seems to be little practical difference. Quantifiers of existence and universality
seem to work very similarly. The definition in itself is a bit recursive since the term
'same elements' could be interpreted as

\begin{custom-quotation}[{}][{}]
An element belonging to set \(A\) is necessary and sufficient for an element
to belong to set \(B\)
\end{custom-quotation}

We have just one... little issue. Iteration. This is a similar issue as to when
we define sets by simply a predicate. If a set is finite, we (humans) can clearly
list all of its elements. However, from a predicate alone, it seems impossible.
Similarly, consider the following two 'sets'

\begin{equation*}
\{1,2,3\} \stackrel{?}{=} \{1,2,2,3\}
\end{equation*}

Perhaps by putting numbers there we have caused a point of contention.
``Sets do not have duplicates'' they say. Whats the problem?

\section{Perhaps I am looking too deep into this}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---perhaps-i-am-looking-too-deep-into-this}

This page was prompted by simply the axiom given in Halmos'
Naive Set Theory. The words, same and equal have so many strings attached.
Just giving an axiom like that prompts so many questions.

\begin{admonition-important}[{}]
I know I know, the book isnt a formal one, but its a very good food for thought.
\end{admonition-important}

\section{Back to my senses}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---back-to-my-senses}

Perhaps we should remember. An axiom is not a definition;
it is \emph{merely} a property which we would like our collection
of objects (in this case sets) to satisfy. Additionally, from this axiom, we see that
our collection of objects, must also come with two additional abilities

\begin{itemize}
\item A method to determine whether they are equal. That is an equivalence relation
    on the collection.
\item A method to determine what elements the set has and determine that
    they are the same.
\end{itemize}

Perhaps the second can be made concrete with a predicate \(\in\) and one
of the previously encountered meanings of having the same elements.

\begin{equation*}
[\forall x \in A: x \in B] \text{ and } [\forall x \in B : x \in A]
\end{equation*}

or more simply \(x \in A \iff x \in B\). Our axiom of extension simply
states that this statement is equivalent to equality in our collection.

Furthermore, from this, we see that sets with duplicate also satisfy the axiom
of extension.

Halmos goes on to give an non-example with humans and ancestors

\begin{custom-quotation}[{}][{}]
We define \(x \in A\) if \(x\) is an ancestor of \(A\) (parents, grandparents ...).
Then the axiom of extension would require that two humans are equal
iff they have the same ancestors.
\end{custom-quotation}

Perhaps the first thing to note about his non-example is that there is no separation of
elements and sets. Next, if we consider the collection of elements \(x \in A\), we
mean the set of ancestors of \(A\). In this non-example we therefore see that
an axiom is not neither a definition, nor a truth. It is a property which must be
satisfied.

\subsection{Adjusted example}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---adjusted-example}

I don't know much about biology, but wouldnt two people having the same set of
ancestors mean that they have the same DNA, the magic that makes them work.
In this sense, if we defined human equality by DNA equality, then we get that
the collection of humans exhibits the axiom of extension. Furthermore, this is an
excellent example that further exemplified Halmos' point. The axiom of
extension is not a definition, it is a property. Using the DNA example,
we can actually prove that it exhibits the axiom of extension.

\begin{admonition-important}[{}]
Throughout this, we assume that the DNA of a child is uniquely determined
by the DNA of the parents.
\end{admonition-important}

\subsubsection{Proof of axiom of extension}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---proof-of-axiom-of-extension}

\begin{admonition-note}[{}]
Throughout this, the idea of same ancestors means that the ancestors have
the same DNA
\end{admonition-note}

Firstly, if two persons are equal (DNA), then they have the same parents (DNA) and they have
the same parents and so on. Hence, WLOG take an ancestor of the first person.
Then this person must be finitely along our chain of parents constructed and hence
must also be an ancestor of the second person. Therefore

\begin{equation*}
A = B \implies [x \in A \iff x \in B]
\end{equation*}

Now, conversely, suppose that \(A\) and \(B\) have the same ancestors.
Then they have the same parents and hence \(A\) and \(B\) must also be the equal.
Therefore

\begin{equation*}
[x \in A \iff x \in B] \implies A = B
\end{equation*}

\subsubsection{Example of two sets of siblings marrying}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---example-of-two-sets-of-siblings-marrying}

Consider persons A, B, C and D where A and B are siblings and C and D are siblings.
Then if A and C have child E while B and D have child F. Then we see that
E = F and furthermore, their ancestors are the same

\subsubsection{Comment}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---comment}

From this, we see another important point. Equality is simply an equivalence relation
on the collection. This all seems very similar to just an exercise in proving
that something is a group. So far for our sets, we require two operators,
\(=\) (equality) and \(\in\) (containment).

From this, we can open our eyes to realize that a set, or rather a collection of sets,
is just a structure with several operations and axioms in a similar manner
to groups and vectors. Perhaps, this is why authors shy away from defining
what a set is. Like vectors, there is no one collection of sets and hence no
definition. We only have a set of axioms for which a collection need to satisfy
for its elements to be considered sets.

\begin{admonition-remark}[{}]
In hindsight, this should have been obvious from the use of the word "axioms" alone.
I wonder if I read more if this point would be emphasized.
\end{admonition-remark}

\subsection{Further reminiscing on Halmos' words}
\label{develop--math3274:reading-notes:page--axiom-of-extension.adoc---further-reminiscing-on-halmos-words}

\begin{admonition-remark}[{}]
Daaga did say, sometimes in this course, we sometimes only realize
something after it has been emphasized.
\end{admonition-remark}

Halmos makes the following comment about the axiom of extension

\begin{custom-quotation}[{}][{}]
With greater pretentiousness and less clarity: a set is determined by its
extension.
\end{custom-quotation}

He did not say 'defined' but 'determined'. This is an important difference.
Remember, it is just an axiom, not a definition.
\end{document}
