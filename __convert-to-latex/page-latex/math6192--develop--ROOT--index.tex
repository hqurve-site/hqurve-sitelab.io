\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Math 6192 - Advanced Mathematical Modelling}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
This course is taught by Dr. Dyer.

\section{Table of contents}
\label{develop--math6192:ROOT:page--index.adoc---table-of-contents}

\begin{itemize}
\item \myautoref[{}]{develop--math6192:ROOT:page--planetary.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--conservation.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--harmonic.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--calculus-of-variations.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--mechanics-concepts.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--linear-stability-analysis.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--heat-flow.adoc}
\item \myautoref[{}]{develop--math6192:ROOT:page--epidemiology.adoc}
    
    \begin{itemize}
    \item \myautoref[{}]{develop--math6192:ROOT:page--epidemic-models/sir.adoc}
    \item \myautoref[{}]{develop--math6192:ROOT:page--epidemic-models/sis.adoc}
    \item \myautoref[{}]{develop--math6192:ROOT:page--epidemic-models/sir-demography.adoc}
    \item \myautoref[{}]{develop--math6192:ROOT:page--epidemic-models/population-growth.adoc}
    \end{itemize}
\item \myautoref[{}]{develop--math6192:ROOT:page--exercises.adoc}
\end{itemize}

\section{Basics}
\label{develop--math6192:ROOT:page--index.adoc---basics}

The core steps in mathematical modelling include

\begin{description}
\item[Describe the problem] We must first describe the problem of interest clearly.
    Without a description, we cannot know what we are hoping to solve/analyze.
\item[Obtain mathematical equations] Without mathematical equations, we cannot use our tools.
    This may very difficult.
\item[Obtain a simplified model] The simplified model makes many assumptions but it gives us a baseline
    of what to expect and gives us a basis from which we can improve.
\item[Clearly specify assumptions] No model is without assumptions. We must be aware of the assumptions
    we make as we will need to verify how well they hold in real life.
\item[Obtaining parameters] Parameters for our model must be realistic or feasible.
    Parameters are used to fit our model to the real world situation.
\end{description}

Once a model has been created, we may want to

\begin{itemize}
\item analyze critical quantities/parameters which govern the overall system
\item fit to data to simulation/prediction
\end{itemize}

Models may be classified as follows

\begin{description}
\item[Linear/Nonlinear] 
\item[Static/Dynamic] (with respect to time)
\item[Discrete/Continuous] 
\item[Deterministic/Stochastic] 
\end{description}

\section{Summary of important results}
\label{develop--math6192:ROOT:page--index.adoc---summary-of-important-results}

\subsection{Moving frame}
\label{develop--math6192:ROOT:page--index.adoc---moving-frame}

Often, it my be useful to ensure that the average velocity
of our system is zero.
We can do this by using the following change in coordinates

\begin{equation*}
\vec{\tilde{r}}_i = \vec{r}_i - \vec{R} \quad\text{where }
\vec{R} = \frac{\sum_{i} m_i\vec{r}_i}{\sum_i m_i}
\end{equation*}

\subsection{Potential wells}
\label{develop--math6192:ROOT:page--index.adoc---potential-wells}

When trying to determine what the period of a system is,
we will get

\begin{equation*}
t = \int \frac{dx}{\sqrt{\frac{2}{M}(E-U(x))}}
\end{equation*}

So, we must

\begin{enumerate}[label=\arabic*)]
\item determine the set of \(x\) values for which the above is well defined
    (ie the term in the square root is positive).
\item If the set of \(x\) values is unbounded, there is no oscillation.
    However, if it is bounded, we integrate between the bounds to get our period.
\end{enumerate}

\subsection{Calculus of variations}
\label{develop--math6192:ROOT:page--index.adoc---calculus-of-variations}

We wish to find the stationary points of the functional

\begin{equation*}
I(\vec{y}) = \int_a^b F(x,\vec{y},\vec{y}') \ dx
\end{equation*}

where \(\vec{y}(x)\).
To do this, we solve the Euler-Lagrange equations

\begin{equation*}
\frac{\partial F}{\partial y_i} - \frac{d}{dx} \left(\frac{\partial F}{\partial y_i'}\right) = 0
\quad\text{for } i=1,2,3,\ldots n
\end{equation*}

If \(y\) is a scalar and \(F(y,y')\), we have the simplified equation of
\(F-F_{y'}y'\) is constant.

\subsubsection{Lagrange Multipliers}
\label{develop--math6192:ROOT:page--index.adoc---lagrange-multipliers}

Suppose we have the restriction that \(\int_a^b G(x,y,y') \ dx =k\).
Then, we instead perform calculus of variations on.

\begin{equation*}
J(y) = \int_a^b (F-\lambda G) \ dx
\end{equation*}

\subsubsection{Least action principle}
\label{develop--math6192:ROOT:page--index.adoc---least-action-principle}

The least action principle states that in a system, the action is minimized
where the action is defined as

\begin{equation*}
S(x) = \int_{t_1}^{t_2} \mathcal{L} \ dt
\end{equation*}

where for classical mechanics \(\mathcal{L} = T-U\) (difference of kinetic
and potential energies). This formulation allows us to easily determine
the behaviour of the system.

\subsection{Concepts in Mechanics}
\label{develop--math6192:ROOT:page--index.adoc---concepts-in-mechanics}

\begin{admonition-todo}[{}]
Look at Lagrangian equations of motion
\end{admonition-todo}

\subsection{Linear stability analysis}
\label{develop--math6192:ROOT:page--index.adoc---linear-stability-analysis}

When looking at a system, we can look at the stability of the linearized system to determine
its general behaviour.
The steps include

\begin{enumerate}[label=\arabic*)]
\item Find the points of stability
\item Introduce small perturbations at each point of stability
\item Remove non-linear terms: products of dependent variables and their derivatives
\item Assume the form of normal-modes where we assume an exponential form for variables where we expect the direction of waves.
\item Solve the linearized system (dispersion relation) to find the characteristic roots.
\end{enumerate}

If the real part of any of the characteristic roots is positive, the system
is unstable at an unstable equilibrium at that point.

\subsection{Common equations}
\label{develop--math6192:ROOT:page--index.adoc---common-equations}

In the following equations,

\begin{itemize}
\item \(P\) is the pressure
\item \(\rho\) is the density
\item \(\vec{v}\) is the velocity vector
\item \(T\) is the temperature
\end{itemize}

\begin{theorem}[{Continuity equation}]
\begin{equation*}
\frac{\partial \rho}{\partial t} + \nabla \cdot(\rho\vec{v}) = 0
\end{equation*}
\end{theorem}

\begin{theorem}[{Euler equation}]
\begin{equation*}
-\frac{1}{\rho} \nabla P + \vec{g}
% = \frac{dv}{dt}
= \frac{\partial \vec{v}}{\partial t} + (\vec{v} \cdot\nabla) \vec{v}
\end{equation*}

where \(g\) is an external force.
\end{theorem}

\begin{theorem}[{Navier-Stokes Equation}]
\begin{equation*}
-\frac{1}{\rho} \nabla P + \frac{\eta}{\rho} \nabla^2\vec{v} + \vec{g}
% = \frac{dv}{dt}
= \frac{\partial \vec{v}}{\partial t} + (\vec{v} \cdot\nabla) \vec{v}
\end{equation*}

where \(\eta\) is the \emph{dynamic viscosity}.

\begin{admonition-note}[{}]
\(\frac{\eta}{\rho}\) may often be written as \(\nu\), the \emph{kinematic viscosity}.
\end{admonition-note}

\begin{admonition-important}[{}]
This equation is for uniform viscosity.
Usually, the \(\nu\nabla^2\vec{v}\) term is \(\nabla\cdot \vec{\tau}\)
where \(\tau\) is a tensor.
\end{admonition-important}
\end{theorem}

\begin{theorem}[{Convection diffusion equation (heat flow)}]
\begin{equation*}
(\vec{v}\cdot\nabla) T + \frac{\partial T}{\partial t} = \kappa \nabla^2 T
\end{equation*}

where \(\kappa\) is the thermal diffusivity.
\end{theorem}

\section{Miscellaneous Tips / Interesting topics}
\label{develop--math6192:ROOT:page--index.adoc---miscellaneous-tips-interesting-topics}

\begin{itemize}
\item An Introduction to Mathematical Epidemiology by Maia Martcheva is an excellent resource for Epidemiology and many of the notes are based on this book.
\item Homotopy Analysis
\item Spring mass pendulum, second equation resonance
\item Global stability analysis
\item How patterns in fur form
\item Bifurcation points
\item Optimal control prevention measure
\item How to get parameters in a epidemiological model
\item Matlab toolbox: CONTENT / MATCONT
\item Hopf bifurcation
\end{itemize}
\end{document}
