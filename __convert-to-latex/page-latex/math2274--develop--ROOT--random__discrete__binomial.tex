\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Binomial Distribution}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A \emph{binomial experiment} consists of \(n\) independent
bernoulli trials each with probability of success \(p\). Let
\(X\) be the number of successful outcomes in \(n\)
trials. Then

\begin{equation*}
X \sim Binomial(n, p)
\end{equation*}

if the following three holds

\begin{itemize}
\item there are \(n\) bernoulli trials
\item the trials are independent
\item probability of success of each trial is the same (\(p\))
\end{itemize}

Furthermore, the probability function of \(X\) is

\begin{equation*}
f(x) = b(x; n, p) = P(X = x) = \binom{n}{x}p^x(1-p)^{n-x}
\end{equation*}

for \(x = 0, 1, 2, \ldots, n\).

\section{Moment}
\label{develop--math2274:ROOT:page--random/discrete/binomial.adoc---moment}

\begin{equation*}
\begin{aligned}
    E[X^r]
    &= \sum_{x=0}^n x^r\binom{n}{x}p^x(1-p)^{n-x} \\
    &= \sum_{x=1}^n x^r\binom{n}{x}p^x(1-p)^{n-x} \\
    &= n \sum_{x=1}^n x^{r-1}\binom{n-1}{x-1}p^x(1-p)^{n-x} \\
    &= n p\sum_{x=1}^n x^{r-1}\binom{n-1}{x-1}p^{x-1}(1-p)^{(n-1)-(x-1)} \\
    &= n p\sum_{x=0}^n (x+1)^{r-1}\binom{n-1}{x}p^{x}(1-p)^{(n-1)-x} \\
    &= n p E[(Y + 1)^{r-1}]\end{aligned}
\end{equation*}

where \(Y \sim Binomial(n-1, p)\).

\section{Expectation}
\label{develop--math2274:ROOT:page--random/discrete/binomial.adoc---expectation}

\begin{equation*}
E[X] = n p E[(Y + 1)^0] = np
\end{equation*}

\section{Variance}
\label{develop--math2274:ROOT:page--random/discrete/binomial.adoc---variance}

\begin{equation*}
E[X^2] = n p E[(Y + 1)^1] = np((n-1)p + 1)
\end{equation*}

Therefore

\begin{equation*}
Var[X] = E[X^2] - E[X]^2 = np((n-1)p + 1) - (np)^2 =  np(np - p + 1 - np) = np(1-p)
\end{equation*}

\section{Moment Generating Function}
\label{develop--math2274:ROOT:page--random/discrete/binomial.adoc---moment-generating-function}

\begin{equation*}
\begin{aligned}
    M_X(t)
    &= E[e^{Xt}]\\
    &= \sum_{x=0}^n \binom{n}{x} e^{xt}p^x(1-p)^{n-x}\\
    &= \sum_{x=0}^n \binom{n}{x} \left( pe^t \right)^{x}(1-p)^{n-x}\\
    &= \left( 1-p + pe^t \right)^{n}\end{aligned}
\end{equation*}

Note that this can alternatively be seen as the moment generating
function of the sum of \(n\) independent Bernoulli random
variables. Therefore, by the uniqueness theorem, the sum of
\(n\) independent Bernoulli random variables has a binomial
distribution.

\section{Poisson approximation to binomial}
\label{develop--math2274:ROOT:page--random/discrete/binomial.adoc---poisson-approximation-to-binomial}

If \(X \sim Binomial(n, p)\) and \(n\) is ''very
large'' (say \(n \geq 100\)) and \(p\) is ''very
small'' (say \(p \leq 0.01\)) and \(np\) is
''moderate'' (small) then it can be shown that the distribution of
\(X\) can be approximated by a
\(Poisson(\lambda = np)\) distribution.

\section{Normal approximation to binomial}
\label{develop--math2274:ROOT:page--random/discrete/binomial.adoc---normal-approximation-to-binomial}

If \(X \sim Binomial(n, p)\) and \(n\) is ''large''
and \(X\) is not heavily skewed (say \(np > 5\) and
\(n(1-p)>5\) and \(np(1-p)>5\)) then it can be shown
(via central limit theorem) that the distribution can be approximated by
a \(N(np, np(1-p))\) distribution. In order to utilize this
approximation, discrete values are converted to intervals as follows

\begin{equation*}
P(X = k) = P(k-0.5 < X^* < k+0.5) \quad\text{where $X^* \sim N(np, np(1-p))$}
\end{equation*}

Note that this is just a rule of thumb. Furthermore, by assuming the
probabilities contributed by the tails not included in the range of
\(X\) is negligible, the following are obtained

\begin{equation*}
P(X \leq k) = P(X^* < k+0.5)\quad\text{and}\quad P(X < k) = P(X^* < k-0.5)
\end{equation*}

\begin{equation*}
P(X \geq k) = P(X^* > k-0.5)\quad\text{and}\quad P(X > k) = P(X^* > k+0.5)
\end{equation*}
\end{document}
