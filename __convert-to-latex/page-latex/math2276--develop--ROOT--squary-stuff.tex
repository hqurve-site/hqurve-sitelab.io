\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Squary stuff}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Future me wondering why I called it this is because its three mostly
unrelated topics (at least with my current ignorance) jumbled into one.
They don’t deserve a separate section each

\section{Perfect rectangles}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---perfect-rectangles}

A rectangle is called perfect if it can be tiled with incongruent
squares, that is squares of all different sizes

\subsection{As a weighted directed graph}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---as-a-weighted-directed-graph}

A perfect rectangle can be represented by a weighted directed graph
where nodes represent boundaries of the squares and edges represent the
squares themselves. In this graph, only the horizontal or vertical edges
are considered and starting at one of the outer boundaries, conduct the
following algorithm

\begin{enumerate}[label=\arabic*)]
\item Create a node for the boundary
\item Draw edges outwards for the boundary node, weighted by the lengths of
    the squares
\item Either one of the two following occur
    
    \begin{enumerate}[label=\alph*)]
    \item an edge (square) completely encompasses a boundary.
    \item multiple square edges completely encompasses a boundary.
        
        In either case, create a node for this boundary and draw outward edges
        for each of the squares on the other side of the boundary.
    \end{enumerate}
\item Repeat the last step until opposing outer boundary is reached.
\end{enumerate}

The key observation is that at no point is the third step not possible.
If it weren’t, the tiling won’t be valid since there is a boundary which
is not completely covered by squares on at least one side.

The resulting directed graph follows Kirchhoff’s circuit laws as follows

\begin{itemize}
\item Since each boundary is the same length on either side, the weight in
    is equal to the weight out.
\item Notice that a walk on the graph amounts to moving perpendicular to the
    selected outer boundary by the amount specified by the weight of each
    edge. Notice that moving to the direction of an edge is negative motion.
    Therefore, if a cycle is created, the same edge is returned to and the
    net motion is zero.
\end{itemize}

These are therefore both necessary and sufficient requirements for a
perfect rectangle configuration.

\subsection{Result 1: The smallest square cannot be on the boundary}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---result-1-the-smallest-square-cannot-be-on-the-boundary}

The proof of this is quite simple and there are two cases

\begin{enumerate}[label=\arabic*)]
\item Case 1: The smallest square is at the corner. This is clearly
    impossible since no two larger squares can be on both sides of it.
\item Case 2: The smallest square is on an edge only. By considering the two
    adjacent squares
\end{enumerate}

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,f] Q[c,f]}, measure=vbox}
{\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[width=0.8\linewidth]{images/develop-math2276/ROOT/squary-stuff/boundary-squares-1}
\caption{Corner}
\end{subfigure}} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[width=0.8\linewidth]{images/develop-math2276/ROOT/squary-stuff/boundary-squares-2}
\caption{Edge}
\end{subfigure}} \\
\end{tblr}
\end{table}

\subsection{Result 2: The smallest square must be surrounded by exactly \(4\) other squares which have a common vertex with the smallest square}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---result-2-the-smallest-square-must-be-surrounded-by-exactly-latex-backslash4-latex-backslash-other-squares-which-have-a-common-vertex-with-the-smallest-square}

The following are not possible (enumeration by square at bottom)

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,f] Q[c,f] Q[c,f]}, measure=vbox}
{\begin{subfigure}{0.3\textwidth}\centering
\includegraphics[width=0.8\linewidth]{images/develop-math2276/ROOT/squary-stuff/surrounding-1}
\caption{Case 1}
\end{subfigure}} & {\begin{subfigure}{0.3\textwidth}\centering
\includegraphics[width=0.8\linewidth]{images/develop-math2276/ROOT/squary-stuff/surrounding-2}
\caption{Case 2}
\end{subfigure}} & {\begin{subfigure}{0.3\textwidth}\centering
\includegraphics[width=0.8\linewidth]{images/develop-math2276/ROOT/squary-stuff/surrounding-3}
\caption{Case 3}
\end{subfigure}} \\
\end{tblr}
\end{table}

The only possible configuration is

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math2276/ROOT/squary-stuff/surrounding-4}
\caption{Valid situation}
\end{figure}

\subsection{Result 3: It is impossible to partition a rectangle into \(n\leq 5\) incongruent squares}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---result-3-it-is-impossible-to-partition-a-rectangle-into-latex-backslashn-latex-backslashleq-5-latex-backslash-incongruent-squares}

By result 1, the number of squares must at least be \(5\).

\subsection{See also}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---see-also}

\begin{itemize}
\item Squaring the square
\item \url{http://www.squaring.net/}
\item Simple perfect squared rectangles
\item Perfect rectangles
\end{itemize}

\section{Orthogonality of Latin squares}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---orthogonality-of-latin-squares}

Although latin squares were discussed before as an application to Hall’s
marriage theorem, it will be discussed again here. Recap, a
\(n \times n\) latin square is an \(n \times n\)
matrix satisfying the following

\begin{itemize}
\item Each row contains each of the number \(1, 2, \ldots, n\)
    exactly once
\item Each column contains each of \(1,\ldots, n\) exactly once.
\end{itemize}

Two latin squares are said to be \emph{orthogonal} (or Graeco-Latin squares)
if all \(n^2\) ordered pairs are distinct. Furthermore, a set
of latin squares are called mutually orthogonal if it is pairwise
orthogonal.

\subsection{The 36 officer problem}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---the-36-officer-problem}

Orthogonal latin squares were first examined by Euler in connection with
the following problem

Given 36 officers of 6 ranks and 6 regiments, can they be arranged in a
square so that each row and column contains exactly one officer of each
rank and exactly one officer of each regiment.

This is equivalent to finding a pair of orthogonal latin squares of
order six where the first square contains the ranks while the second
contains the regiments. Since each officer can be represented by a pair
containing his rank then regiment, all pairs of these latin squares must
be unique.

\subsection{Known results}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---known-results}

\begin{itemize}
\item If \(n=p^m\), where \(p\) is a prime, then
    \(\exists\) \(n-1\) mutually orthogonal latin
    squares of order \(n\).
\item The maximum number of latin squares, of order \(n\), which
    are mutually orthogonal is \(n-1\)
\item There is a pair of orthogonal latin squares of all orders except
    \(n=2\) and \(n=6\). In fact, it was initially
    conjectured by Euler that there were no latin squares of form
    \(4k+2\) but this was later disproven.
\end{itemize}

\subsection{See more}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---see-more}

\begin{itemize}
\item \url{http://buzzard.ups.edu/squares.html}
\end{itemize}

\section{Magic Squares}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---magic-squares}

A magic square of order \(n\) is an \(n\times n\)
matrix of integer \(1,\ldots, n^2\) such that the row, column
and corner diagonal sums are all equal. Additionally, it is easily seen
that this sum must be \(\frac{n(n^2+1)}{2}\).

\subsection{De La Loubère method for odd orders}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---de-la-loubère-method-for-odd-orders}

Magic squares of odd orders can be constructed using the following
method

\begin{itemize}
\item Place a \(1\) in the middle position of the rightmost
    column.
\item Place each successive integer to the bottom right of the previously
    placed integer. That is if the \(k\) was placed at
    \((i, j)\), place \(k+1\) at
    \((i+1, j+1)\) where edges wrap around.
\item If the next position is already occupied, place \(k+1\) at
    \((i-1, j)\) instead.
\end{itemize}

\subsection{Construction using orthogonal Latin squares}
\label{develop--math2276:ROOT:page--squary-stuff.adoc---construction-using-orthogonal-latin-squares}

Consider two orthogonal Latin squares where the sums of the diagonals
are equal. Then consider the matrix of pairs of elements from the two
latin squares. If each pair \((a, b)\) is replaced by
\(n(a-1) + b\) then the resulting matrix is a magic square.
This is quite easy to verify

\begin{itemize}
\item each of the numbers from \(1\) to \(n^2\) occur
    once since \(n(a-1)+b\) is a sort of division result when
    dividing each element by \(n\) and the orthogonality ensures
    the uniqueness of each pair.
\item all the row and column sums are equal since in both cases the sums
    contain all \(0 \leq a-1 \leq n-1\) and
    \(1 \leq b \leq n\) by the condition of latin squares.
\item by the added condition that the sums of the diagonals of the latin
    squares are equal, the sum of the diagonals of the resulting magic
    square are equal.
\end{itemize}
\end{document}
