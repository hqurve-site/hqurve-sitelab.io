\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Principle of Incluson Exclusion}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Suppose that we have a have a collection of subsets of \(A\), \(\{A_i\}_{i \in I}\).
Then, we may want to know how many of the elements of \(A\) are contained by exactly \(r\) of the \(A_i\).
We define \(N(r)\) to be this value.
It is interesting to note that we can actually determine \(N(r)\) knowing only
the size of \(A\) as well as the sizes of \(\bigcap_{j\in J} A_i\) where \(J \subseteq I\).
We define

\begin{equation*}
S_k
= \sum_{\begin{matrix}J \subseteq I,\\ |J| = k\end{matrix}} \left|\bigcap_{j\in J} A_j\right|
= \sum_{i_1 < i_2 < \cdots < i_k} N(A_{i_1}A_{i_2}\cdots A_{i_k})
= \sum_{i_1 < i_2 < \cdots < i_k} N_{i_1i_2\cdots i_k}
\end{equation*}

We also define \(S_0 = |A|\) (ie the empty intersection is the universe, \(S\)).

\begin{admonition-note}[{}]
The right expressions are just notation for the first.
\end{admonition-note}

In order to relate the \(S_k\) to \(N(r)\), we need the following lemmas

\begin{lemma}[{Binomial theorem}]
Let \(n \geq 0\). Then,

\begin{equation*}
(1+x)^n = \sum_{k=0}^n \binom{n}{k}x^k
\end{equation*}

This is a well known result, but we write it out so that it is easier to prove the next theorem.
\end{lemma}

\begin{lemma}[{Alternating sums}]
Let \(n \geq r \geq 0\). Then,

\begin{equation*}
\sum_{k=r}^n (-1)^k \binom{k}{r}\binom{n}{k} = \begin{cases}
0, &\quad \text{if } n > r\\
(-1)^r, &\quad \text{if } n = r\\
\end{cases}
\end{equation*}

\begin{proof}[{}]
First differentiate the binomial theorem \(r\) times to obtain

\begin{equation*}
\frac{n!}{(n-r)!}(1+x)^{n-r} = \sum_{k=r}^n \binom{n}{k}\frac{k!}{(k-r)!}x^{k-r}
\end{equation*}

by dividing both sides by \(r!\) and substituting \(x=-1\), we get

\begin{equation*}
\binom{n}{r}(0)^{n-r} = (-1)^{-r}\sum_{k=r}^n (-1)^{k}\binom{n}{k}\binom{k}{r}
\end{equation*}

Note that if \(n=r\), we actually don't get zero on the left hand size, but exactly \(1\).
However, if \(n > r\), the left hand side is zero. So, by rearranging, we get the desired result.
\end{proof}
\end{lemma}

We claim that

\begin{equation*}
\begin{aligned}
N(r)
&= \sum_{s=r}^{\infty} (-1)^{s-r}\binom{s}{r} S_s
\\&= S_r - \binom{r+1}{r}S_{r+1} + \binom{r+2}{r}S_{r+2} - \cdots
\end{aligned}
\end{equation*}

where the above sum eventually terminates.
The special case when \(r=0\) is commonly called the \emph{Principle of Inclusion-Exclusion} (PIE).

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof was presented in class and it is very beautiful and simple.
\end{admonition-note}

We will consider each object in \(A\) separately and see how it contributes to the summation on the right hand side.
Consider object \(x \in A\)

\begin{itemize}
\item If \(x\) occurs in exactly \(r\) of the \(A_i\), then it contributes \(1\) to \(S_r\)
    and zero to all \(S_s\) where \(s > r\).
\item If \(x\) occurs in exactly \(t > r\) of the \(A_i\), focus on some \(S_s = \sum_{i_1 < i_2 < \cdots < i_s} N_{i_1i_2\cdots i_s}\)
    
    \begin{itemize}
    \item If \(s \leq t\), \(x\) occurs in exactly \(\binom{t}{s}\) of the terms in \(S_s\) and hence contributes
        \((-1)^{s-r}\binom{s}{r}\binom{t}{s}\) to the overall sum.
    \item If \(s > t\), then \(x\) occurs in none of the terms of \(S_s\) and hence contributes nothing to the overall sum.
    \end{itemize}
    
    By summing over the \(s \geq r\), we get that \(x\) contributes
    \(\sum_{s=r}^t (-1)^{s-r}\binom{s}{r}\binom{t}{s} =0\) (since \(t > 0\)) to the overall sum.
\end{itemize}

Therefore the summation

\begin{equation*}
\sum_{s=r}^{\infty} (-1)^{s}\binom{s}{r} S_s
\end{equation*}

only counts \(x \in A\) which belong to exactly \(r\) of the \(A_i\) and counts each once.
Hence the above summation must be exactly \(N(r)\).
\end{proof}

\begin{example}[{Three sets}]
Consider a set of objects where each object can have any of three properties
such that

\begin{equation*}
\begin{array}{lll}
N(A) = 12 &\quad N(AB)=4 &\quad N(ABC)=1\\
N(B)=10 & \quad N(AC)=6 \\
N(C)=12 & \quad N(BC)= 5
\end{array}
\end{equation*}

Then, from the previous results, we can determine \(N(1)\), \(N(2)\) and \(N(3)\).
Note that we cannot determine \(N(0)\).

\begin{equation*}
\begin{aligned}
N(1)
&= (12 + 10 + 12) - \binom{2}{1}(4+6+5) + \binom{3}{1}(1)
\\&= 34-2(15)+3
\\&= 7
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
N(2)
&= (4+6+5) - \binom{3}{2}(1)
\\&= 15- 3
\\&= 12
\end{aligned}
\end{equation*}

and \(N(3)=1\) (trivially).
\end{example}

\section{Matrix}
\label{develop--math6194:ROOT:page--principle-inclusion-exclusion.adoc---matrix}

\begin{admonition-note}[{}]
This is precisely the same as \myautoref[{Special Matrix Inversion}]{develop--math2276:ROOT:page--useful-results.adoc---special-matrix-inversion}.
\end{admonition-note}

Note that we can write the relationship between the \(N(r)\) and \(S_s\) as follows

\begin{equation*}
\begin{array}{rcccccccccccccc}
N(0) &=& S_0 &-& S_1 &+& S_2 &-& S_3 &+& S_4 &-& S_5 &+&\cdots\\
N(1) &=&    & & S_1 &-& 2S_2 &+& 3S_3 &-& 4S_4 &+& 5S_5 &-&\cdots\\
N(2) &=&    & &    & & S_2 &-& 3S_3 &+& 6S_4 &-& 10S_5 &+&\cdots\\
N(3) &=&    & &    & &     && S_3 &-& 4S_4 &+& 10S_5 &-&\cdots\\
N(4) &=&    & &    & &     &&    & & S_4 &-& 5S_5 &+&\cdots\\
    &\vdots &
\end{array}
\end{equation*}

Since the above sums all eventually terminate, we essentially have a matrix
in reduced row echelon form.
So, we can also determine the \(S_s\) in terms of the \(N(r)\).
We obtain that

\begin{equation*}
\begin{array}{rcccccccccccccc}
S_0 &=& N(0) &+& N(1) &+& N(2) &+& N(3) &+& N(4) &+& N(5) &+&\cdots \\
S_1 &=&     & & N(1) &+& 2N(2) &+& 3N(3) &+& 4N(4) &+& 5N(5) &+&\cdots \\
S_2 &=&     & &      & & N(2) &+& 3N(3) &+& 6N(4) &+& 10N(5) &+&\cdots \\
S_3 &=&     & &      & &      & & N(3) &+& 4N(4) &+& 10N(5) &+&\cdots \\
S_4 &=&     & &      & &      & &      &+&  N(4) &+& 5N(5) &+&\cdots \\
&\vdots &
\end{array}
\end{equation*}

The first row is clear, but we need to prove the later rows.
Specifically, we want to prove that

\begin{equation*}
S_s = \sum_{r=s}^\infty \binom{r}{s}N(r)
\end{equation*}

\begin{proof}[{}]
Let \(s \geq 0\) and recall that

\begin{equation*}
S_s
= \sum_{\begin{matrix}J \subseteq I,\\ |J| = s\end{matrix}} \left|\bigcap_{j\in J} A_j\right|
\end{equation*}

Consider arbitrary \(x \in A\) with exactly \(r\) properties.

\begin{itemize}
\item If \(r < s\), \(x\) does not contribute to \(S_s\)
\item If \(r \geq s\), \(x\) contributes to \(S_s\) exactly \(\binom{r}{s}\) times.
\end{itemize}

Since we have \(N(r)\) objects with exactly \(r\geq s\) properties,
all contribute a total of \(\binom{r}{s}N(r)\) to \(s\).
Therefore by summing over \(r \geq s\), we get the desired result.
\end{proof}
\end{document}
