\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Quotient Topology and Space}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-note}[{}]
This information comes from Topology by Munkres.
\end{admonition-note}

Let \(X\) and \(Y\) be topological spaces and \(p: X\to Y\) be surjective.
We call \(p\) a \emph{quotient map} if for each \(V \subseteq Y\),
\(V\) is open iff \(p^{-1}(V)\) is open.

\begin{admonition-warning}[{}]
\(p\) need not be a homeomorphism nor need \(p(U)\) be open for each open
\(U \subseteq X\).
\end{admonition-warning}

Note that this is a stronger condition than continuity and hence sometimes has the name `strong continuity'.
Also like continuity, we can define quotient maps in terms of closed sets.
That is, \(p\) is a quotient map if for each \(V \subseteq Y\),
\(V\) is closed iff \(p^{-1}(V)\) is closed.
Please see \myautoref[{}]{develop--math6620:special-topologies:page--quotient.adoc---more-on-quotient-maps} for more.

We now have the tools to define the quotient topology.
Let \(X\) be a topological space, \(A\) be a set and \(p: X \to A\) be surjective.
Then, there exists exactly one topology \(\mathcal{T}\) on \(A\) to which \(p\)
is a quotient map. We call \(\mathcal{T}\) the \emph{quotient topology} induced by \(p\).
However, the existence and uniqueness of the quotient topology is not immediately apparent.

\begin{proposition}[{Form of the quotient topology}]
Consider the set \(\mathcal{T} \subseteq \mathcal{P}(A)\) defined by

\begin{equation*}
V \in \mathcal{T} \iff p^{-1}(V) \text{ is open}
\end{equation*}

Then,

\begin{itemize}
\item \(\mathcal{T}\) is a topology on \(A\).
\item \(p\) is a quotient map when using \(\mathcal{T}\) as a topology on \(A\)
\item \(\mathcal{T}\) is the only topology on \(A\) which makes \(p\) a quotient map
\end{itemize}

\begin{proof}[{}]
First notice that \(\varnothing, A \in \mathcal{T}\) since \(p^{-1}(\varnothing) = \varnothing\)
and by the surjectivity of \(p\), \(p^{-1}(A) = X\).
Also,

\begin{equation*}
p^{-1}\left(\bigcup_{\alpha \in I} V_\alpha\right) = \bigcup_{\alpha \in I} p^{-1}(V_\alpha)
\quad\text{and}\quad
p^{-1}\left(\bigcap_{i=1}^n V_i\right) = \bigcap_{i =1}^n p^{-1}(V_i)
\end{equation*}

Therefore we have the necessary conditions for arbitrary unions and finite intersections.
Therefore \(\mathcal{T}\) is a topology on \(A\).

From the definition of \(\mathcal{T}\), we automatically have that \(p\) is quotient map
when using \(\mathcal{T}\) as a topology on \(A\).

Finally, suppose there exists another topology \(\mathcal{T}'\) on \(A\) such that \(p\)
is a quotient map.
If \(V' \in \mathcal{T}'\), \(p^{-1}(V')\) is open since \(p: X\to \mathcal{T}'\) is a quotient map.
Therefore, \(V' \in \mathcal{T}\) (by definition of \(\mathcal{T}\))
and \(\mathcal{T}' \subseteq \mathcal{T}\).
Conversely if \(V \in \mathcal{T}\), \(p^{-1}(V)\) is open by definition of \(\mathcal{T}\).
Then by the fact that \(p: X \to \mathcal{T}'\) is a quotient map, \(V \in \mathcal{T}'\)
and \(\mathcal{T} \subseteq \mathcal{T}'\).
Therefore, the two topologies are the same.
\end{proof}
\end{proposition}

\section{Quotient space}
\label{develop--math6620:special-topologies:page--quotient.adoc---quotient-space}

Let \(X\) be a topological space and \(\mathcal{X}\) be a partition of \(X\) (ie disjoint subsets whose union is \(X\)).
Let \(p: X\to \mathcal{X}\) map each \(x \in X\) to the \(X^* \in \mathcal{X}\) containing \(x\).
Then, \(p\) is a surjective map.
We call the quotient topology induced by \(p\), a \emph{quotient space} of \(\mathcal{X}\).

Recall that each partition \(\mathcal{X}\) of \(X\) can be uniquely determined as the equivalence classes of
an equivalence relation \(\sim\) on \(X\) where \(\mathcal{X} = X / \sim\).
Therefore, we may equivalently define a quotient space on using an equivalence relation \(\sim\).
Notice that the open sets in \(X/\sim\) consists of equivalence classes whose union
is open in \(X\).
We may also call the quotient space on \(X/\sim\) the \emph{decomposition space} or \emph{identification space}
as the open sets identify each pair of equivalent points.

\subsection{Converting a disk to a sphere}
\label{develop--math6620:special-topologies:page--quotient.adoc---converting-a-disk-to-a-sphere}

Consider the closed unit ball in \(\mathbb{R}^2\)  (ie closed unit disk)

\begin{equation*}
X = \{(x, y) \ | \ x^2 + y^2 \leq 1\}
\end{equation*}

and let \(\sim\) be the equivalence relation with equivalence classes consisting of \(\{(x,y)\}\)
for each \(x^2 + y^2 < 1\) and \(S_1 = \{(x,y) \ | \ x^2+y^2=1\}\).
Then, the open sets on \(X/\sim\) have a basis consisting of

\begin{itemize}
\item open balls in the interior of \(X\) (wrt \(\mathbb{R}^2\)) (well rather the singletons of elements in open balls)
\item open rings with with radii consisting of open sets of \((0, 1]\).
\end{itemize}

Then, we would show that \(X/\sim\) is homeomorphic to the subspace topology on
\(S_2 \subseteq \mathbb{R}^3\) defined by

\begin{equation*}
S_2 = \{(x,y,z) \ | \ x^2+y^2+z^2 = 1\}
\end{equation*}

The idea is that we can ``wrap'' the perimeter of \(X\) to form a sphere in \(\mathbb{R}^3\).
This concept is similar to the \href{https://en.wikipedia.org/wiki/Riemann\_sphere}{Riemman sphere} but with only a finite disk.
Consider the function \(p: X/\sim \to S_2\) defined by \(p(S_1) = (0,0,1)\)

\begin{equation*}
p(\{(x,y)\}) = \left(
x\frac{\sqrt{1-(x^2+y^2)}}{\sqrt{x^2+y^2}}
,\
y\frac{\sqrt{1-(x^2+y^2)}}{\sqrt{x^2+y^2}}
,\
\sqrt{x^2+y^2}
\right)
= \left(\alpha\cos\theta, \alpha\sin\theta, r\right)
\end{equation*}

For the usual definitions of \((r, \theta)\) and \(\alpha\) such that the point lies on \(S_2\).
This function is clearly bijective.
After a little work, I guess it is also possible to show that \(p\) is a homeomorphism.

\subsection{Converting a plane to a torus}
\label{develop--math6620:special-topologies:page--quotient.adoc---converting-a-plane-to-a-torus}

Consider the closed unit square in \(\mathbb{R}^2\) and the equivalence relation defined by equivalence classes

\begin{itemize}
\item \(\{(x,y)\}\) for each interior point \((x,y)\)
\item \(\{-1,1\}\times (-1,1)\), \((-1, 1) \times\{-1,1\}\)
\item \(\{(-1,-1), (-1, 1), (1, -1), (1, 1)\}\)
\end{itemize}

Then, the quotient space is homeomorphic to a torus by first joining the top and bottom sides; then curving the left and right sides together.
Note that the chosen equivalence classes given a good indication of how we join the edges.

\section{More on quotient maps}
\label{develop--math6620:special-topologies:page--quotient.adoc---more-on-quotient-maps}

\begin{proposition}[{Saturated}]
An alternative way of defining quotient maps are as follows.
Let \(C \subseteq X\).
We say that \(C\) is \emph{saturated} (wrt \(p\)) if
\(C\) contains each \(p^{-1}(\{y\})\) which it intersects.
That is

\begin{equation*}
\forall y \in Y: C \cap p^{-1}(\{y\}) \neq \varnothing \implies p^{-1}(\{y\}) \subseteq C
\end{equation*}

Therefore, \(C = p^{-1}(V)\) for some \(V \subseteq Y\).
Using the concept of saturated sets, we may equivalently define a quotient map as a continuous mapping
that maps saturated open sets of \(X\) to open sets of \(Y\).

\begin{proof}[{}]
First suppose that \(p\) is a quotient map. Then, \(p\) is automatically continuous.
Now, consider a saturated open set of \(p^{-1}(V)\).
Then, by definition \(p(p^{-1}(V)) = V\).

Conversely, suppose that \(p\) is a continuous mapping which maps saturated open sets of
\(X\) to open sets of \(Y\). Consider arbitrary \(V \subseteq Y\).

\begin{itemize}
\item If \(V\) is open, by the continuity of \(V\), \(p^{-1}(V)\) is open.
\item If \(p^{-1}(V)\) is open, by the nature of \(p\), \(V = p(p^{-1}(V))\) is open.
\end{itemize}

Therefore, \(V\) is open iff \(p^{-1}(V)\) is open, and \(p\) is a quotient map.
\end{proof}
\end{proposition}

\begin{proposition}[{Continuous open (or closed) maps are quotient maps}]
Let \(X\) and \(Y\) be two topological spaces and \(p: X\to Y\).
We say that \(p\) is an open map if \(p\) maps open sets in \(X\) to open sets in \(Y\);
likewise we say that \(p\) is a closed map if \(p\) maps closed sets in \(X\) to closed sets in \(Y\).

If \(p\) is surjective continuous map and either an open map or closed map,
\(p\) is a quotient map.

\begin{proof}[{}]
Suppose that \(p\) is an open map.
Consider arbitrary \(V \subseteq Y\).

\begin{itemize}
\item If \(V\) is open, by the continuity of \(p\), \(p^{-1}(V)\) is also open.
\item If \(p^{-1}(V)\) is open, \(V = p(p^{-1}(V))\) is also open.
\end{itemize}

Therefore \(p\) is a quotient map.

The proof for closed maps is the same, but uses closed sets instead.
\end{proof}
\end{proposition}

\subsection{Example 1}
\label{develop--math6620:special-topologies:page--quotient.adoc---example-1}

\begin{admonition-note}[{}]
This is almost verbatim from the book, but with a little more explanation.
\end{admonition-note}

Let \(X = [0, 1]\cup [2,3]\) be a subspace of \(\mathbb{R}\) and \(Y = [0, 2]\) be a subspace
of \(\mathbb{R}\).
Also let \(p: X\to Y\) be defined by

\begin{equation*}
p(x) = \begin{cases}
x, & \quad\text{if } x \in [0,1]\\
x-1, & \quad\text{if } x \in [2,3]\\
\end{cases}
\end{equation*}

Then, \(p\) is surjective

\begin{itemize}
\item continuous since for each \([a,b]\) in \(Y\), the preimage either lies entirely in one of the two
    disjoint intervals of \(X\) or is the union of two closed intervals in \(X\).
\item closed since it clearly maps closed intervals to closed intervals.
\end{itemize}

Therefore \(p\) is a quotient map. However, it is not an open map since
the image of the open set \([0,1]\) in \(X\) is not open in \(Y\).

Let \(A = [0,1) \cup [2,3]\) be a subspace of \(X\) (or equivalently a subspace of \(\mathbb{R}\)).
Then in this case, the map \(q: A\to Y\) defined by restricting \(p\) to \(A\) is continuous
and surjective. But it is not a quotient map since \([2,3]=p^{-1}([1,2])\) is open in \(A\)
but \([1,2]\) is not open in \(Y\).

\section{Theorems}
\label{develop--math6620:special-topologies:page--quotient.adoc---theorems}

\begin{theorem}[{Quotient maps on subspaces}]
Let \(p: X\to Y\) be a quotient map and let \(A\) be a subspace of \(X\) which is saturated with respect
to \(p\). Let \(q:A\to p(A)\) be the restriction of \(p\) on \(A\).
Then, \(q\) is a quotient map if any if either of the following two conditions hold

\begin{itemize}
\item \(A\) is either open or closed in \(X\)
\item \(p\) is either an open or closed map
\end{itemize}

\begin{proof}[{}]
First notice that \(q\) must be surjective since \(A\) is saturated.
Let \(W \subseteq Y\) be such that \(p^{-1}(W) = A\). Note that \(W\) need not be open.
Also, notice that

\begin{equation*}
\forall V \subseteq Y: q^{-1}(V\cap W) = A \cap p^{-1}(V) = p^{-1}(W) \cap p^{-1}(V) = p^{-1}(W \cap V)
\end{equation*}

Now, suppose that \(A\) is open in \(X\).
Since \(p\) is a quotient map and \(A = p^{-1}(W)\) is open, \(W\) must also be open.
Hence open sets in \(A\) and \(p(A)\) are also open in \(X\) and \(Y\) respectively.
Therefore for any \(V' \subseteq p(A) = W\), since \(p^{-1}(V') = q^{-1}(V')\),
and hence \(V'\) is open iff \(q^{-1}(V')\) is open and \(q\) is a quotient map.
The proof for closed \(A\) is similar.

If \(p\) is instead open.
Consider arbitrary \(V' \subseteq p(A) = W\),

\begin{itemize}
\item If \(V'=V\cap W\) is open, \(q^{-1}(V') = p^{-1}(V') = p^{-1}(V\cap W) = p^{-1}(V) \cap p^{-1}(W) = p^{-1}(V) \cap A\).
    Since \(p^{-1}(V)\) is open in \(X\), \(q^{-1}(V')\) is open in \(A\).
\item If \(q^{-1}(V') = U \cap A\) is open. Since \(q^{-1}(V') = p^{-1}(V')\) and \(A\) is saturated,
    \(V' = p(U\cap A) = p(U)\cap p(A)\). Since \(p(A)\) is open in \(Y\),
    \(q(U\cap A)= V'\) is open in \(p(A)\).
\end{itemize}

Therefore \(q\) is a quotient map. The proof for closed \(p\) is similar.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(p: X\to Y\) be a quotient map and \(Z\) be a topological space.
Suppose that \(g: X\to Z\) is constant on each \(p^{-1}(\{y\})\)
ie

\begin{equation*}
p(x) = p(x') \implies g(x) = g(x')
\end{equation*}

Then, \(g\) induces a function \(f: Y\to Z\) such that \(f\circ p= g\).
This function is defined as follows: for each \(y\in Y\) define
\(f(y) = g(x)\) where \(x \in p^{-1}(y)\); since \(g\) is constant on \(p^{-1}(\{y\})\),
this function is well defined.

\begin{figure}[H]\centering
\includegraphics[width=0.4\linewidth]{images/develop-math6620/special-topologies/induced_function}
\end{figure}

Also

\begin{itemize}
\item The induced map \(f\) is continuous iff \(g\) is continuous
\item The induced map \(f\) is a quotient map iff \(g\) is a quotient map
\end{itemize}

\begin{proof}[{}]
First suppose that \(f\) is continuous since both \(f\) and \(p\) are continuous,
\(g = f\circ p\) is also continuous.
Conversely suppose that \(g\) is continuous
and consider arbitrary \(W \subseteq Z\).
Then, \(p^{-1}(f^{-1}(W))=g^{-1}(W)\) is open. Since \(p\) is a quotient map,
we have that \(f^{-1}(W)\) is also open and \(f\) is continuous.

Note that \(f\) is surjective iff \(g\) is surjective.

Now, suppose that \(f\) is a quotient map. Consider arbitrary \(W \subseteq Z\).
Then \(g^{-1}(W) = p^{-1}(f^{-1}(W))\) is open iff \(W\) is open (follow the chain).
Conversely if \(g\) is a quotient map,

\begin{equation*}
W \text{ is open}
\iff g^{-1}(W) = p^{-1}(f^{-1}(W)) \text{ is open}
\iff f^{-1}(W) \text{ is open}
\end{equation*}

The first equivalence happens since \(g\) is a quotient map and the second
equivalence happens since \(p\) is a quotient map.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(g: X\to Z\) be surjective and continuous. Let \(\sim\) be an equivalence relation
defined with \(x\sim x'\) iff \(g(x) = g(x')\).
Then

\begin{enumerate}[label=\arabic*)]
\item The map \(g\) induces a bijective continuous function \(f: X/\sim \to Z\).
    This function is homeomorphic iff \(g\) is a quotient map.
\item If \(Z\) is Hausdorff, so is \(X/\sim\)
\end{enumerate}

\begin{proof}[{}]
From definition of \(\sim\), the induced map \(f\) must be bijective.
Also, since the canonical map from \(X\to X/\sim\) is a quotient map by definition of the quotient
space, we can use the previously discussed proposition and obtain that \(f: X/\sim \to Z\)
is a quotient map iff \(g\) is a quotient map.
Therefore \(f\) is a homeomorphism iff \(g\) is a quotient map.

Now, suppose that \(Z\) is Hausdorff. Consider arbitrary \([x]_\sim, [y]_\sim \in X/\sim\)
with \([x]_\sim \neq [y]_\sim\). Therefore \(g(x) \neq g(y)\) and there exists
disjoint open neighbourhoods \(U, V \subseteq Z\) of \(g(x)\) and \(g(y)\).
Since \(g\) is continuous, we have that the induced function \(f\) is continuous and hence
\(f^{-1}(U)\) and \(f^{-1}(V)\) are open. They must also be disjoint since
\(U\) and \(V\) are disjoint. Also \(f^{-1}(U)\) and \(f^{-1}(V)\) contain \([x]_\sim\)
and \([y]_\sim\) respectively since \(g(x) \in U\) and \(g(y) \in V\).
Therefore we have found disjoint open sets containing \([x]_\sim\)
and \([y]_\sim\) and hence \(X/\sim\) is Hausdorff.
\end{proof}
\end{theorem}
\end{document}
