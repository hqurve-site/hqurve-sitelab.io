\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Properties of Statistics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
In this page, we let \(\chi\) represent the joint sample space.
We study different properties of statistics.

The properties and their intuition are

\begin{description}
\item[Sufficiency] Does this statistic capture all the information necessary to estimate \(\theta\)?
    For example, can we generate the MLE using this statistic
\item[Ancillary] This statistic captures no information about \(\theta\).
    So, knowing this statistic does not give us a better idea of what \(\theta\) may be.
\item[Complete] This statistic only captures information about \(\theta\).
\end{description}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stat6110/ROOT/information-diagram}
\caption{Amount of information captured by each type of statistic}
\end{figure}

\section{Sufficiency}
\label{develop--stat6110:ROOT:page--properties-of-statistics.adoc---sufficiency}

Let \(T(\vec{X})\) be a statistic where \(\vec{X} = (X_1,\ldots X_n)\) are iid
with a population parameter \(\theta\).
Then, \(T(X)\) is \textbf{sufficient} for \(\theta\) if and only if

\begin{equation*}
f_{\vec{X}}(\vec{x} \ | \ T(\vec{X}) = t, \theta) = h(\vec{x})
\end{equation*}

That is, the above function does not depend on \(\theta\).

\begin{theorem}[{Equivalent definition for sufficiency}]
Let \(f_{\vec{X}}(\vec{x} \ | \ \theta)\) be the joint pdf/pmf of \(\vec{X}\)
and let \(q(t \ |  \ \theta)\) be the pdf/pmf of \(T(\vec{X})\).
Then \(T(\vec{X})\) is sufficient for \(\theta\) if for \(\vec{x} \in \chi\)

\begin{equation*}
\frac{f_{\vec{X}}(\vec{x} \ |\ \theta)}{q(T(\vec{x}) \ | \ \theta)}
\end{equation*}

is a constant function of \(\theta\).

\begin{proof}[{}]
\begin{admonition-note}[{}]
We prove the discrete case. The continuous case is identical.
\end{admonition-note}

We use the definition of sufficiency directly.

\begin{equation*}
\begin{aligned}
P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = t,\ \theta)
&= \frac{
    P(\vec{X} = \vec{x} , \ T(\vec{X}) = t,\ \theta)
}{
    P(T(\vec{X}) = t,\ \theta)
}
\\&= \begin{cases}
    \frac{
        P(\vec{X} = \vec{x},\ \theta)
    }{
        P(T(\vec{X}) = t,\ \theta)
    }
    &\quad\text{if } T(\vec{x}) = t\\
    0 & \quad\text{otherwise}
\end{cases}
\\&= \begin{cases}
    \frac{
        f_{\vec{X}}(\vec{x} \ | \ \theta)
    }{
        q(T(\vec{x}) \ | \ \theta)
    }
    &\quad\text{if } T(\vec{x}) = t\\
    0 & \quad\text{otherwise}
\end{cases}
\end{aligned}
\end{equation*}

Therefore, we it is clear that

\(P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = t, \ \theta)\) is constant with respect to \(\theta\)
if and only if
\(
\frac{
    f_{\vec{X}}(\vec{x} \ | \ \theta)
}{
    q(T(\vec{x}) \ | \ \theta)
}
\) is constant with respect to \(\theta\).
\end{proof}
\end{theorem}

\begin{theorem}[{Fisher factorization theorem}]
Let \(f_{\vec{X}}(\vec{x} \ | \ \theta)\) be the joint pdf/pmf of \(\vec{X}\).
A statistic \(T(\vec{X})\) is sufficient for \(\theta\) if and only if
there exists functions \(g(t | \theta)\) and \(h(\vec{x})\)
such that

\begin{equation*}
\forall \vec{x} \in \chi: \forall \theta \in \Omega:
f_{\vec{X}}(\vec{x} \ | \ \theta) = g(T(\vec{x}) \ | \ \theta)h(\vec{x})
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
We prove the discrete case. The continuous case is identical.
\end{admonition-note}

For the forward direction, we can use the definition of sufficiency.
Suppose \(T(\vec{X})\) is sufficient for \(\theta\).
Let \(g(t \ | \ \theta) = P(T(\vec{X}) = t \ | \ \theta)\)
and \(h(\vec{x}) = P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = T(\vec{x}))\).
Since \(T(\vec{X})\) is sufficient for \(\theta\), \(h(\vec{x})\) is constant
with respect to \(\theta\). So, we have that

\begin{equation*}
f(\vec{x} \ | \ \theta)
= P(\vec{X} = \vec{x}\ | \ \theta)
= P(\vec{X} = \vec{x}, \ T(\vec{X}) = T(\vec{x})\ | \ \theta)
= P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = T(\vec{x}),\ \theta)P(T(\vec{X}) = T(\vec{x})\ | \ \theta)
= h(\vec{x})g(T(\vec{x}) \ | \ \theta)
\end{equation*}

Hence, we have proven that such functions \(g\) and \(h\) exist.

Now, we prove the converse. Suppose there exists functions \(g(t \ | \ \theta)\) and \(h(\vec{x})\) satisfying the necessary
condition. We need to prove that \(T(\vec{X})\) is sufficient.
To do this, we use the previous theorem.
We need to find an expression for the distribution of \(T(\vec{X})\).
Let

\begin{equation*}
A_t = \{\vec{x} \in \chi: \ T(\vec{x}) = t \}
\end{equation*}

Then, the pmf of \(T(\vec{X})\) is

\begin{equation*}
q(t \ | \theta) = P(T(\vec{X}) = t \ | \ \theta)
= \sum_{\vec{y} \in A_t}f_{\vec{X}}(\vec{y} \ |\ \theta)
= \sum_{\vec{y} \in A_t}g(T(\vec{y}) \ | \ \theta)h(\vec{y})
= \sum_{\vec{y} \in A_t}g(t \ | \ \theta)h(\vec{y})
\end{equation*}

So,

\begin{equation*}
\frac{f_{\vec{X}}(\vec{x} \ | \ \theta)}{q(T(\vec{x}) \ | \theta)}
= \frac{g(T(\vec{x}) \ | \ \theta)h(\vec{x})}{\sum_{\vec{y} \in A_t}g(T(\vec{x}) \ | \ \theta)h(\vec{y})}
= \frac{h(\vec{x})}{\sum_{\vec{y} \in A_t}h(\vec{y})}
\end{equation*}

which is clearly constant wrt to \(\theta\). This implies that \(T(\vec{X})\) is sufficient for \(\theta\).
\end{proof}
\end{theorem}

\subsection{Minimal Sufficiency}
\label{develop--stat6110:ROOT:page--properties-of-statistics.adoc---minimal-sufficiency}

A sufficient statistic \(T(\vec{X})\) is called a \textbf{minimal sufficient} statistic
if for any other sufficient statistic \(T'(\vec{X})\),
\(T(\vec{X})\) is a function of \(T'(\vec{X})\).

\begin{theorem}[{}]
Let \(f_{\vec{X}}(\vec{x} \ | \ \theta)\) be the joint pdf/pmf of random sample
\(\vec{X} \in \chi\). Suppose that there exists \(T(\vec{x})\)
such that for all \(\vec{x}, \vec{y} \in \chi\),
the ratio

\begin{equation*}
\frac{f_{\vec{X}}(\vec{x} \ | \ \theta)}{f_{\vec{X}}(\vec{y} \ | \ \theta)}
\end{equation*}

is constant as a function of \(\theta\) if and only if \(T(\vec{x}) = T(\vec{y})\).
Then, \(T(\vec{X})\) is a minimal sufficient statistic.
\end{theorem}

\begin{example}[{Different definitions for sufficiency for Bernoulli(p)}]
Let \(X_1,\ldots X_n \sim Ber(p)\) distribution.
Then, we want to consider \(T(\vec{X}) = \sum_{i=1}^n X_i\)

For the definition of sufficiency, we need the distribution of \(T(\vec{X})\).
This is simple since we know that the sum of bernoulli random variables is binomial.
Therefore,

\begin{equation*}
\begin{aligned}
f_{\vec{X}}(\vec{x}\ |\ T(\vec{X}) = t, p)
&= \frac{P(\vec{X} = \vec{x}, \ T(\vec{X}) = t\ | \ p)}{P(T(\vec{X}) = t \ | \ p)}
\\&= \begin{cases}
\frac{P(\vec{X} = \vec{x}, \ T(\vec{X}) = T(\vec{x})\ | \ p)}{P(T(\vec{X}) = T(\vec{x}) \ | \ p)}
,&\quad \text{if } T(\vec{x}) = t\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\\&= \begin{cases}
\frac{P(\vec{X} = \vec{x}\ | \ p)}{P(T(\vec{X}) = T(\vec{x}) \ | \ p)}
,&\quad \text{if } T(\vec{x}) = t\\
&\quad\text{since the space of }T(\vec{X}) =T(\vec{x})\text{ is a superset of } \vec{X} = \vec{x}
\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\\&= \begin{cases}
\frac{\prod_{i=1}^n p^{x_i}(1-p)^{1-x_i}}{\binom{n}{\sum_{i=1}^n x_i}p^{\sum_{i=1}^n x_i}(1-p)^{n-\sum_{i=1}^n x_i}}
,&\quad \text{if } T(\vec{x}) = t\\
\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\\&= \begin{cases}
\frac{1}{\binom{n}{\sum_{i=1}^n x_i}}
,&\quad \text{if } T(\vec{x}) = t\\
\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\end{aligned}
\end{equation*}

This is independent of \(p\) and hence \(T(\vec{X})\) is a sufficient statistic for \(p\).

If we use the equivalent definition for sufficiency,
we obtain the same term as the piecewise function above when \(T(\vec{x}) =t\).
So, in practice, there is no benefit to using this over the definition
besides the fact that we can avoid the use of a piecewise function.

Finally, we can use the fisher factorization theorem.

\begin{equation*}
\begin{aligned}
f_{\vec{X}}(\vec{x} \ | \ p)
= \prod_{i=1}^n p^{x_i}(1-p)^{1-x_i}
= p^{\sum_{i=1}^n x_i}(1-p)^{n-\sum_{i=1}^n x_i}
= p^{T(\vec{x})}(1-p)^{n-T(\vec{x})}
\end{aligned}
\end{equation*}

So, if we let

\begin{equation*}
g(t|p) = p^{t}(1-p)^{n-t}, \quad\text{and}\quad
h(\vec{x}) = 1
\end{equation*}

The fisher factorization theorem holds and we have that \(T(\vec{X})\)
is a sufficient statistic for \(p\).

We can now check whether this statistic is minimal sufficient.
To do this, we use the respective theorem. Note that

\begin{equation*}
\frac{f_{\vec{X}}(\vec{x} \ | \ p)}{f_{\vec{X}}(\vec{y} \ | \ p)}
= \frac{p^{T(\vec{x})}(1-p)^{n-T(\vec{x})}}
    {p^{T(\vec{y})}(1-p)^{n-T(\vec{y})}}
= \left(\frac{p}{1-p}\right)^{T(\vec{x}) - T(\vec{y})}
\end{equation*}

So, the above is constant wrt \(p\) iff \(T(\vec{x}) = T(\vec{y})\).
Therefore, we have that \(T(\vec{x})\) is minimal sufficient for \(p\).
\end{example}

\section{Ancillary}
\label{develop--stat6110:ROOT:page--properties-of-statistics.adoc---ancillary}

A statistic \(T(\vec{X})\) is an \textbf{ancillary} statistic for \(\theta\) if its distribution
does not depend on \(\theta\).

\begin{admonition-note}[{}]
This implies that the likelihood function is constant and hence information about
the statistic does not provide information about the parameter.
\end{admonition-note}

\begin{example}[{Ancillary from location family}]
Let \(X_1,\ldots X_n\) be from a location family with pdf \(f(x-\mu)\)
where \(-\infty < \mu < \infty\).
Then, the range \(R = X_{(n)} - X_{(1)}\) is an ancillary statistic.

Assume that the cdf exists and it is \(F(x-\mu)\).
To see this, let \(Z_i = X_i - \mu\).
Then the \(Z_i\) are iid with pdf \(f(x)\) and cdf \(F(x)\).
The cdf of \(R\) is then

\begin{equation*}
F_R(r|\mu) = P(R\leq r | \mu)
= F(X_{(n)}-X_{(1)} \leq r | \mu)
= F(Z_{(n)}-Z_{(1)} \leq r | \mu)
= F(Z_{(n)}-Z_{(1)} \leq r)
\end{equation*}

since the \(Z_i\) do not depend on \(\mu\).
So, \(R\) is an ancillary statistic.
\end{example}

\section{Complete}
\label{develop--stat6110:ROOT:page--properties-of-statistics.adoc---complete}

\begin{admonition-important}[{}]
This is a property of a \textbf{family} of random variables, not just one random variable.
Also, this can be rephrased for a family of random variables, not just a statistic.
\end{admonition-important}

Let \(\tau = \{f_T(t|\sigma) \ | \ \tau \in \Sigma\}\)
be a family of pdfs (or pms) for a statistic \(T(\vec{X})\).
The family of probability distributions is called \textbf{complete}
if for all \(g(t)\)

\begin{equation*}
\left\{\forall \theta \in \Omega: E[g(T) \ | \ \theta] = 0\right\}
\implies
\left\{\forall \theta \in \Omega: P(g(T) =0\ | \ \theta) =1\right\}
\end{equation*}

That is \(g(T) = 0\) \textbf{almost surely}.
Equivalently, we say that \(T(\vec{X})\) is a \textbf{complete statistic}.

\begin{example}[{Not complete with finite parameter space}]
\begin{admonition-tip}[{}]
This argument can be extended to any finite parameter space for the Poisson family
\end{admonition-tip}

Let \(\Omega = \{1,2\}\)
and \(\tau = \left\{f_T: f_T(t|\lambda)=\frac{e^{-\lambda}\lambda^t}{t!}, t=0,\ldots, \lambda\in \Omega\right\}\).
We will show that this family is not complete.
To do this, define

\begin{equation*}
g(t) = \begin{cases}
g_0 ,\quad&\text{if } t=0\\
g_1 ,\quad&\text{if } t=1\\
g_2 ,\quad&\text{if } t=2\\
0 ,\quad&\text{otherwise}\\
\end{cases}
\end{equation*}

Then,

\begin{itemize}
\item When \(\lambda=1\), \(E[g(T) | \lambda=1] = g_0 + g_1 + \frac12g_2 = 0\)
\item When \(\lambda=2\), \(E[g(T) | \lambda=1] = g_0 + 2g_1 + 2g_2 = 0\)
\end{itemize}

We then find \((g_0, g_1, g_2)\) which lies in the nullspace
of the matrix defined by the above two expressions.
This can be done by row reduction.
One possible choice is

\begin{equation*}
g(t) = \begin{cases}
2 ,\quad&\text{if } t=0\\
-3 ,\quad&\text{if } t=1\\
2 ,\quad&\text{if } t=2\\
0 ,\quad&\text{otherwise}\\
\end{cases}
\end{equation*}

From this we obtain that the two expectations are zero but \(g(t) \not\equiv 0\).
So the family is not complete.
\end{example}

\begin{example}[{Complete}]
Let \(\Omega = (0,\infty)\)
and \(\tau = \left\{f_T: f_T(t|\lambda)=\frac{e^{-\lambda}\lambda^t}{t!}, t=0,\ldots, \lambda\in \Omega\right\}\).
Let \(T(\vec{X}) = \sum_{i=1}^n X_i\) where the \(X_i\) are iid \(Poisson(\lambda)\).
We will show that this \(T(\vec{X})\) is a complete statistic.

First note that \(T(\vec{X}) \sim Poisson(n\lambda)\)
Consider an arbitrary \(g(t)\) such that \(E[g(T) | \lambda]=0\) for all \(\lambda > 0\).
Then this implies that

\begin{equation*}
\sum_{t=0}^\infty g(t)\frac{n^t}{t!}\lambda^t = 0\quad \forall \lambda > 0
\end{equation*}

We view the above as a power series in \(\lambda\).
This implies that each coefficient is zero and hence \(g(t) \equiv 0\),
hence proving completeness.

To see why this implies that each coefficient is zero, recall that the expectation
exists iff we have absolute convergence. Therefore, we can take derivatives and
inductively prove that each coefficient must be zero.
\end{example}

\section{Theorems}
\label{develop--stat6110:ROOT:page--properties-of-statistics.adoc---theorems}

The numbers quoted are the references in the book by Casella and Berger (2002)

\begin{proposition}[{Functions of statistics}]
Let \(T(\vec{X})\) be a statistic and \(T^* = r(T)\)
where \(r\) is a one-to-one function
Then

\begin{itemize}
\item If \(T\) is sufficient, then \(T^*\) is also sufficient
\item If \(T\) is complete, then \(T^*\) is also complete.
\end{itemize}
\end{proposition}

\begin{proposition}[{}]
Let \(T(\vec{X})\) be a complete, sufficient statistic. Then,
\(T(\vec{X})\) is also \textbf{minimal sufficient}.
\end{proposition}

\begin{theorem}[{Basu's Theorem (Theorem 6.2.24)}]
If \(T(\vec{X})\) is a complete sufficient statistic, then \(T(\vec{X})\)
is \textbf{independent} of every ancillary statistic.
\end{theorem}

\begin{example}[{Example of ancillary}]
Let \(X_1,\ldots X_n \sim exp(\theta)\).
Then \(T(\vec{X}) = \sum_{i=1}^n X_i\) is
a complete, sufficient statistic.
Define

\begin{equation*}
U(\vec{X}) = \frac{X_1}{\sum_{i=1}^n X_i}
\end{equation*}

It is a well known fact that \(U(\vec{X}) \sim Beta(1,n-1)\).

Since \(U(\vec{X})\) is an ancillary statistic, by
Basu's theorem, \(T(\vec{X})\perp U(\vec{X})\)
\end{example}
\end{document}
