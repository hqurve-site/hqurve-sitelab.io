\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Information theorems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
In this section, we seek to find \textbf{uniformly minimum variance unbiased estimators} (UMVUEs).
Let \(X_1,\ldots X_n\) be a sample. Then, we say that
\(W(\vec{X})\) is the best unbiased estimator (or UMVUE) of \(\tau(\theta)\) if
for all \(\theta\)

\begin{itemize}
\item \(E[W(\vec{X}) | \theta] = \tau(\theta)\)
\item \(Var[W(\vec{X}) | \theta] \leq Var[W'(\vec{X}) | \theta]\) for any other unbiased estimator
    \(W'(\vec{X})\).
\end{itemize}

To find such an estimator, we use the concept of \textbf{Fisher Information}
and its associated theorems.

\section{Fisher Information}
\label{develop--stat6110:ROOT:page--information.adoc---fisher-information}

Let \(X\) be a random variable with pdf \(f_X(x|\theta)\).
Then, the fisher information number of \(X\)

\begin{equation*}
I(\theta) = E\left[\left(\frac{\partial}{\partial \theta} \log f_X(X|\theta)\right)^2\right]
\end{equation*}

The term being squared,
\(S(X|\theta) = \frac{\partial}{\partial \theta} \log f_X(X|\theta)\),
is sometimes called the \textbf{score function} of \(X\).
Note that \(E[S(X|\theta)] = 0\) as long as the partial
derivative can be moved outside the associated integral.

We can also find the Fisher information of a sample
\(\vec{X}= (X_1,\ldots X_n)\)
with joint
pdf \(f_{\vec{X}}(\vec{x} | \theta)\) by defining
the associated score function.
In the case where the \(X_i\) are iid,
the fisher information of the sample
can be reformulated as \(I_n(\theta) = nI(\theta)\).

\begin{lemma}[{Equivalent form of Fisher Information}]
Consider random variable \(X\) with pdf \(f_X(x|\theta)\).
Suppose that

\begin{itemize}
\item \(
    \frac{\partial}{\partial \theta} \int_{\chi} f_X(x|\theta) \ dx
    =
    \int_{\chi} \frac{\partial}{\partial \theta}f_X(x|\theta) \ dx
    \)
\item \(
    \frac{\partial}{\partial \theta} \int_{\chi} \frac{\partial}{\partial \theta}f_X(x|\theta) \ dx
    =
    \int_{\chi} \frac{\partial^2}{\partial \theta^2}f_X(x|\theta) \ dx
    \)
\end{itemize}

Then

\begin{equation*}
I(\theta) = Var\left[\frac{\partial}{\partial \theta} \log f_X(X|\theta)\right]
= -E\left[\frac{\partial^2}{\partial \theta^2} \log f_X(X|\theta)\right]
\end{equation*}

\begin{proof}[{First equality}]
For the first equality,
notice that \(E[S(X|\theta)]=0\) by the first condition.
Therefore

\begin{equation*}
Var[S(X|\theta)]
= E[S(X|\theta)^2] - E[S(X|\theta)]^2
= I(\theta) - 0^2
\end{equation*}
\end{proof}

\begin{proof}[{Second equality}]
Note that

\begin{equation*}
\begin{aligned}
-E\left[\frac{\partial^2}{\partial \theta^2} \log f_X(X|\theta)\right]
&=-E\left[\frac{\partial}{\partial \theta}
\frac{\frac{\partial}{\partial \theta}f_X(X|\theta)}{f_X(X|\theta)^2}
\right]
\\&=-E\left[
\frac{f_X(X|\theta)\frac{\partial^2}{\partial \theta^2}f_X(X|\theta) - \frac{\partial}{\partial \theta}f_X(X|\theta) \frac{\partial}{\partial \theta}f_X(X|\theta) }{f_X(X|\theta)^2}
\right]
\\&=-E\left[
\frac{\frac{\partial^2}{\partial \theta^2}f_X(X|\theta) }{f_X(X|\theta)}
- S(X|\theta)^2
\right]
\\&=I(\theta)-E\left[
\frac{\frac{\partial^2}{\partial \theta^2}f_X(X|\theta) }{f_X(X|\theta)}
\right]
\\&=I(\theta)-\int_{\chi}
\frac{\frac{\partial^2}{\partial \theta^2}f_X(X|\theta) }{f_X(X|\theta)}
f_X(X|\theta)
\ dx
\\&=I(\theta)-\int_{\chi}
\frac{\partial^2}{\partial \theta^2}f_X(X|\theta)
\ dx
\\&=I(\theta)-\frac{\partial^2}{\partial \theta^2}\int_{\chi}
f_X(X|\theta)
\ dx
\\&= I(\theta) - 0
\end{aligned}
\end{equation*}
\end{proof}
\end{lemma}

\section{Cramer-Rao Theorem}
\label{develop--stat6110:ROOT:page--information.adoc---cramer-rao-theorem}

\begin{theorem}[{Cramer-Rao (Theorem 7.3.9)}]
Let \(X_1,\ldots X_n\) be a random sample
with joint pdf \(f_{\vec{X}}(\vec{x} | \theta)\).
Suppose that \(W(\vec{X})\) is an estimator satisfying

\begin{itemize}
\item \(E[W(\vec{X}) | \theta] = \tau(\theta)\) for all \(\theta \in \Omega\).
    That is, \(W(\vec{X})\) is an unbiased estimator of \(\tau(\theta)\)
\item \(Var[W(\vec{X})|\theta] < \infty\)
\item For \(h(\vec{x})=1\) and \(h(\vec{x}) = W(\vec{x})\)
    
    \begin{equation*}
    \frac{\partial}{\partial \theta}\int_{\chi}h(\vec{x}) f_{\vec{X}}(\vec{x}|\theta) \ d\vec{x}
    =
    \int_{\chi}h(\vec{x}) \frac{\partial}{\partial \theta}f_{\vec{X}}(\vec{x}|\theta) \ d\vec{x}
    \end{equation*}
\end{itemize}

Then, a lower bound for \(Var[W(\vec{X})|\theta]\) is

\begin{equation*}
Var[W(\vec{X}) | \theta]
\geq \frac{
    \left(\frac{\partial}{\partial \theta}\tau(\theta)\right)^2
}{
    E\left[\left(\frac{\partial}{\partial \theta} \log f_{\vec{X}}(\vec{X} | \theta)\right)^2\right]
}
\end{equation*}
\end{theorem}

\begin{proof}[{}]
Recall that the covariance acts like an inner product for random variables.
So, the Cauchy-Schwarz Inequality holds

\begin{equation*}
Cov[U,V]^2 \leq Var[U]Var[V]
\end{equation*}

If we let \(U = W(\vec{X})\) and \(V=S(\vec{X}|\theta)=\frac{\partial}{\partial \theta}\log f_{\vec{X}}(\vec{X} | \theta)\),
and rearrange the above, we obtain

\begin{equation*}
Var[W(\vec{X}) | \theta] \geq \frac{
    Cov[W(\vec{X} | \theta), \ S(\vec{X} | \theta)]^2
}{
    Var(S(\vec{X} | \theta))
}
\end{equation*}

The denominator is simply
the information of the sample \(E[S(\vec{X} | \theta)^2]\)
since the change of differential and integral condition for \(h(\vec{x}) = 1\) implies \(E[S(\vec{X} | \theta)] =0\).

The numerator is simplified as follows

\begin{equation*}
\begin{aligned}
Cov[W(\vec{X} | \theta), S(\vec{X} | \theta)]
&= E[W(\vec{X})S(\vec{X}) | \theta] - E[W(\vec{X} | \theta)]E[S(\vec{X} | \theta)]
\\&= E[W(\vec{X})S(\vec{X}) | \theta] - E[W(\vec{X} | \theta)](0)
\\&= E[W(\vec{X})S(\vec{X}) | \theta]
\\&= \int_{\chi} W(\vec{x}) \left(\frac{\partial}{\partial \theta} \log f_{\vec{X}}(\vec{x} | \theta)\right) f_{\vec{X}}(\vec{x} | \theta)\ d\vec{x}
\\&= \int_{\chi} W(\vec{x}) \frac{\partial}{\partial \theta} f_{\vec{X}}(\vec{x} | \theta)\ d\vec{x}
\\&= \frac{\partial}{\partial \theta}\int_{\chi} W(\vec{x})  f_{\vec{X}}(\vec{x} | \theta)\ d\vec{x}
    \quad\text{by change of differential and integral for }h(\vec{x}) = W(\vec{x})
\\&= \frac{\partial}{\partial \theta}E[W(\vec{x} | \theta)]
\\&= \frac{\partial}{\partial \theta}\tau(\theta)
\end{aligned}
\end{equation*}

So, we have the desired inequality.
\end{proof}

\begin{corollary}[{IID case}]
Let \(X_1,\ldots X_n\) be iid
with pdf \(f_{X}(x | \theta)\).
Let \(W(\vec{X})\) be an estimator satisfying the same conditions
as the Cramer-Rao Theorem.
Then

\begin{equation*}
Var[W(\vec{X}) | \theta]
\geq \frac{
    \left(\frac{\partial}{\partial \theta}\tau(\theta)\right)^2
}{
    nE\left[\left(\frac{\partial}{\partial \theta} \log f_X(X | \theta)\right)^2\right]
}
\end{equation*}
\end{corollary}

\begin{corollary}[{Attainment of CR bound (Corollary 7.3.15)}]
Let \(X_1,\ldots X_n\) be a sample with joint pdf \(f_{\vec{X}}(\vec{x} | \theta)\)
and \(W(\vec{X})\) be an estimator satisfying the conditions
of the Cramer-Rao Theorem.
Then, the CR lower bound is attained by \(W(\vec{X})\)
if and only if there exists a function \(a(\theta)\)
such that

\begin{equation*}
S(\vec{x}| \theta) = a(\theta)[W(\vec{x}) - \tau(\theta)]
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
In the below analysis, we can treat \(\theta\) as a constant.
\end{admonition-note}

Since the Cramer-Rao theorem was proven using the Cauchy-Schwarz inequality,
we know that equality is attained iff the two
``vectors'' are scalar multiples of each other, say \(a(\theta)\).
Additionally, since the Covariance is invariant with respect to shifts (addition of constant),
the vector space being used consists of the equivalence classes of random variables
where two random variables are ``equal'' if and only if they differ by some constant amount.

From the above analysis, we see that equality is attained if and only if
\(S(\vec{X}|\theta)\) and \(a(\theta)W(\vec{X}|\theta)\) belong to the same
equivalence class. That is,

\begin{equation*}
S(\vec{X} | \theta) = a(\theta)W(\vec{X}|\theta) + b(\theta)
\end{equation*}

for some \(b(\theta)\). By taking the expectation on either side,
we obtain that \(0 = a(\theta)\tau(\theta) + b(\theta)\).
Therefore, we see that equality is attained iff

\begin{equation*}
S(\vec{X} | \theta) = a(\theta)[W(\vec{X} | \theta) - \tau(\theta)]
\end{equation*}
\end{proof}
\end{corollary}

\section{Rao-Blackwell theorem}
\label{develop--stat6110:ROOT:page--information.adoc---rao-blackwell-theorem}

The Rao-Blackwell theorem allows us to get a better estimator
for \(\tau(\theta)\) given an unbiased estimator and a sufficient statistic.
The problem however with this theorem is that

\begin{enumerate}[label=\arabic*)]
\item The new estimator may be difficult to compute
\item The new estimator may depend on \(\theta\). For example, we may obtain that \(\phi(T) = T + \theta\).
    So, we cannot estimate \(\theta\) without already knowing \(\theta\).
\end{enumerate}

\begin{theorem}[{Rao-Blackwell}]
Let \(W(\vec{X})\) be an unbiased estimator of \(\tau(\theta)\)
and \(T(\vec{X})\) be a sufficient statistic for \(\theta\).
Define statistic \(\phi(T) = E[W|T]\).
Then

\begin{itemize}
\item \(E[\phi(T) | \theta] = \tau(\theta)\)
\item \(Var[\phi(T) | \theta] \leq Var[W(\vec{X}) | \theta]\)
\end{itemize}

\begin{admonition-note}[{}]
\(\phi(T)\) is a random variable since it is really
\((\phi \circ T)(\vec{X})\)
\end{admonition-note}

\begin{proof}[{}]
From the law of conditional expectation,

\begin{equation*}
E[\phi(T) | \theta]
= E[E[W(\vec{X}) | T(\vec{X})]| \theta]
= E[W(\vec{X})| \theta] = \tau(\theta)
\end{equation*}

From the law of conditional variance

\begin{equation*}
\begin{aligned}
Var[W(\vec{X})|\theta]
&= Var[E[W(\vec{X}) | T(\vec{X})]|\theta] + E[Var[W(\vec{X}) | T(\vec{X})] | \theta]
\\&\geq Var[E[W(\vec{X}) | T(\vec{X})]|\theta]
\\&= Var[\phi(T) | \theta]
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\section{Best estimates}
\label{develop--stat6110:ROOT:page--information.adoc---best-estimates}

\begin{theorem}[{Best (Theorems 7.3.23)}]
Let \(T\) be a complete and sufficient statistic for parameter \(\theta\).
Let \(\phi(T)\) be any estimator based on \(T\).
Then \(\phi(T)\) is the \textbf{unique} best unbiased estimator of its expected value.
\end{theorem}
\end{document}
