\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cool stuff}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
This is just a list of things which I find cool. Each time I find something, it would be added here for historical
purposes.

\section{Nice condition for limit inferior and limit superior.}
\label{develop--main:ROOT:page--cool.adoc---nice-condition-for-limit-inferior-and-limit-superior}

I think the following is an equvalent way of saying that \(L\) is the limit superior of a sequence \(x_n\).

\begin{equation*}
\forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall n \in \mathbb{N}: n > N \implies x_n < L + \varepsilon
\end{equation*}

\begin{equation*}
\forall \varepsilon >0: \exists N \in \mathbb{N}: \exists n \in \mathbb{N}: n > N \text{ and } |x_n - L| < \varepsilon
\end{equation*}

I thought of this while going to sleep and it makes stuff more understandable

\section{Definition of the Riemann Integrable}
\label{develop--main:ROOT:page--cool.adoc---definition-of-the-riemann-integrable}

I love how simplisitic and intuitive it is in contrast to the limit definition which includes
the norm going to 0.

Does this follow from the fact that a limit exists iff the limit superior is equal to the limit inferior?

I think last time I saw a definition for riemann integrals, instead of taking the
extremum in each interval, we just the value of the function at one of the endpoints. This older definition
doesnt seem to give a well defined value for exhotic functions such as the dirichlet function.

Interesting that the limit of the riemann sum existing implies that the function is reimann integrable. Still
it remains that the riemann sum is not one value but a family of values

\section{Frobenius Normal Form}
\label{develop--main:ROOT:page--cool.adoc---frobenius-normal-form}

How do you determine if two matricies are similar? \url{https://en.wikipedia.org/wiki/Frobenius\_normal\_form}

\section{Dividing evenly}
\label{develop--main:ROOT:page--cool.adoc---dividing-evenly}

If there are two people, we let one divide and the other choose. This is the best{\texttrademark} way.

If there are three people, we are left with a problem. We could let one divide and then let the
other two choose and then evenly divide between themselves. If the person making the first choice
is working alone, its best for him to divide into three as evenly as possible as the other two
are bound to choose the best 2/3 as they are both guaranteed to get at least half of the remainder.
This method easily generalizes so that \(n\) people each get \(1/n\).

However, the first could pair with someone and instead split it into 1/2, 1/2, 0. The person he has paired
with would choose 0 while the other would choose 1/2. Therefore the pair would overall get 3/4
and hence each get 3/8. Is there a better method which ensures that even if \(k\) of \(n\)
people group together, the most they could jointly accumulate is \(\frac{k}{n}\)?

\section{Cayley-Hamilton Theorem: Forcing a commutative ring}
\label{develop--main:ROOT:page--cool.adoc---cayley-hamilton-theorem-forcing-a-commutative-ring}

In one of the
\href{https://en.wikipedia.org/wiki/Cayley\%E2\%80\%93Hamilton\_theorem\#A\_proof\_using\_polynomials\_with\_matrix\_coefficients}{proofs of the Cayley-Hamilton Theorem}
on wikipedia, a problem arose when thinking of a polynomial with matrix coefficients, not as a formal structure
but rather as a function of some yet to be defined matrix \(t\). The problem
is that the product of two polynomials need not follow the rules which we expect. For example

\begin{equation*}
(At) \cdot (Bt) = AtBt \neq ABt^2
\end{equation*}

in general since the ring of matrices is not commutative.
Their solution was to utilize the centralizer of an element (the matrix
which they would eventually set \(t\) equal to)
which would necessarily form a commutative ring, and show that
all their elements of interest belong to this subring.

This is especially genius since it allows them to work with polynomials
as they desired despite the general non-commutative nature of matrices.

The really important thing to note is that you
may not always have what you desire. But, if you focus only
on what you need, perhaps things may work out.

\begin{admonition-remark}[{}]
Wow, getting really philosophical there.
\end{admonition-remark}

\section{Triplex numbers}
\label{develop--main:ROOT:page--cool.adoc---triplex-numbers}

A thorough investigation of rings isomorphic to \(R[X]/((X^3 - 1))\)
by Casual Graphman (Andrew Osborne)

\url{https://www.youtube.com/watch?v=dvI7dGXFgm8}

\section{Topology on Open Balls}
\label{develop--main:ROOT:page--cool.adoc---topology-on-open-balls}

During a conversation with Dr Tweedle about analysis with groups,
he kept on saying that we cannot really talk about topology/limits unless
we have some notion of an open ball. Initially, I thought he was
saying that we required a metric space, which we can use to define
open balls \myautoref[{[Read More]}]{develop--math3277:topology:page--index.adoc}.

However, after ending the call (perhaps hours later),
it dawned on me that he may have been saying that we dont need the entire
complexity of a metric space, but only need a way to define open balls.
So far, this is my guess as to what is required to discuss limits using open
balls.

Let \(X\) be a set. Then for each \(x \in X\) and \(r \in \mathbb{R}^+\),
we we have open ball \(B(x, r) \subseteq X\) with the following properties

\begin{itemize}
\item \(x \in B(x,r)\)
\item \(r_1 < r_2 \implies B(x, r_1) \subset B(x,r_2)\)
\item \(y \in B(x,r) \iff [\exists r' > 0: B(y,r') \subseteq B(x,r)]\)
\end{itemize}

The purpose of the first and second properties are quite self explanatory.
However, for the third property, the forward direction implies that our ball is
open while the reverse direction gives us a characterization of elements in our ball.
Additionally, note that the first property is a bit redundant, however, for emphasis, I believe
its okay.
\end{document}
