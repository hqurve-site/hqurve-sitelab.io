\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cardinal Arithmetic}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
Let \(\kappa\) and \(\lambda\) be two cardinal numbers
with representatives \(K\) and \(L\).
Then, we define

\begin{description}
\item[Addition] \(\kappa + \lambda = |K \cup L|\) if \(K\) and \(L\) are disjoint.
\item[Multiplication] \(\kappa \lambda = |K \times L|\)
\item[Exponentiation] \(\kappa^\lambda = |K^L|\)
\end{description}

Our main question now is whether these operations are well defined.
Also, for addition, it is worth pointed out that \(K \approx K \times \{0\}\)
and \(L \approx L \times \{1\}\) and hence disjoint sets exist.

\begin{example}[{Proof of well defined}]
Let \(\kappa = |K_1| = |K_2|\) and \(\lambda = |L_1| = |L_2|\)
where \(f: K_1 \to K_2\) and \(g: L_1 \to L_2\)
are bijections.

Then firstly, we define \(\phi_+: (K_1 \cup L_1) \to (K_2 \cup L_2)\) by

\begin{equation*}
\phi_+(x) = \begin{cases}
f(x) &\quad\text{if } x \in K_1\\
g(x) &\quad\text{if } x \in L_1\\
\end{cases}
\end{equation*}

Then, since \(K_1\) and \(L_1\) are disjoint, this is a well defined
function. Furthermore, it must be bijective. Therefore, addition
is well defined.

Next, we define \(\phi_\times: (K_1 \times L_1) \to (K_2 \times L_2)\) by

\begin{equation*}
\phi_\times((x,y)) = (f(x), g(x))
\end{equation*}

Again, clearly bijective.

Finally, for exponentiation, we would define two bijective functions.
We define \(\phi_1: K_2\)

Finally, we define \(\phi_{\wedge}: K_1^{L_1} \to K_2^{L_2}\) by

\begin{equation*}
\phi_{\wedge}(h) = g \circ h \circ f^{-1}
\end{equation*}

Then, this clearly maps to \(K_2^{L_2}\). Furthermore, we can use the fact
that \(g\) and \(f\) are bijective to also show that

\begin{equation*}
g \circ h_1 \circ f^{-1} = g \circ h_2 \circ f^{-1} \iff h_1 = g^{-1} \circ g \circ h_1 \circ f^{-1} \circ f = h_2
\end{equation*}

which implies that \(\phi_{\wedge}\) is injective. Also, for any \(i \in K_2^{L_2}\),

\begin{equation*}
\phi_{\wedge}(g^{-1} \circ i \circ f) = i
\end{equation*}

Therefore, \(\phi_{\wedge}\) must be a bijection.
\end{example}

\section{Properties}
\label{develop--math3274:cardinality:page--arithmetic.adoc---properties}

\subsection{Addition and multiplication}
\label{develop--math3274:cardinality:page--arithmetic.adoc---addition-and-multiplication}

\begin{itemize}
\item \(+\) and \(\times\) are both commutative and associative
\item \(0\) is the identity for \(+\)
\item \(1\) is the identity for \(\times\)
\item \(\times\) distributes over \(+\)
\end{itemize}

\begin{example}[{Proof}]
Let \(\kappa = |K|\), \(\lambda = |L|\) and \(\mu = |M|\).
Then, by the associativity and commutativity of unions,
\(+\) is automatically associative and commutative.

Next, \(\times\) is easily commutative with bijection \((x,y) \mapsto (y,x)\).
Also, it is associative with bijection \((x, (y,z)) \mapsto ((x,y), z)\).

Next, \(0\) is the identity for \(+\) since \(\varnothing\)
is the identity for unions. Also, \(1\) is the identity for \(\times\)
since \(x \mapsto (x,u)\) is a bijection where \(1 = |\{u\}|\).

Next, to show that \(\times\) distributes over \(+\), notice that

\begin{equation*}
\begin{aligned}
(\kappa + \lambda)\mu
&= |K \cup L|\mu
\\&= |(K \cup L) \times M|
\\&= |(K \times M) \cup (L \times M)|
\\&= |K \times M| + |L \times M|
\\&= \kappa\mu + \lambda\mu
\end{aligned}
\end{equation*}
\end{example}

\subsection{Exponents}
\label{develop--math3274:cardinality:page--arithmetic.adoc---exponents}

\begin{itemize}
\item \(1^\kappa = 1\) and \(\kappa^1 = \kappa\)
\item \(0^\kappa = 0\) if \(\kappa \neq 0\)
\item \(\kappa^0 = 1\) if we take the empty set to be a function.
\item \(\kappa^{\lambda + \mu} = \kappa^\lambda \kappa^\mu\)
\item \(\kappa^{\lambda \mu} = (\kappa^\lambda)^\mu\)
\item \((\kappa\lambda)^\mu = \kappa^\mu \lambda^\mu\)
\end{itemize}

\begin{example}[{Proof}]
Let \(\kappa = |K|\), \(\lambda = |L|\) and \(\mu = |M|\).
Also, let \(1 = \{u\}\)

There is exactly one function which maps \(K\) to \(\{u\}\).
Also, there is a bijection between functions \(\{u\} \to K\) and \(K\)
based on what the function maps \(u\) to.

Next, if \(\kappa \neq 0\), there is no function from \(K\) to \(\varnothing\)
since for each \(a \in K\), there is nothing which it can map to.

Next, if we take the empty set to be a function, we get that there
is exactly one function from \(\varnothing\) to \(K\).

Next, we want to show that

\begin{equation*}
\kappa^{\lambda + \mu} = |K^{L \cup M}| \stackrel{?}{=} |K^L \times K^M| = \kappa^\lambda\kappa^\mu
\end{equation*}

Then, we map \((f,g)\) to \(h: (L \cup M) \to K\) by

\begin{equation*}
h(x) = \begin{cases}
f(x) &\quad\text{if } x \in L\\
g(x) &\quad\text{if } x \in M\\
\end{cases}
\end{equation*}

Then, since this is a well defined function from \(L \cup M\) to \(K\). Also,
if \(h_1 = h_2\) it implies that \(f_1 = f_2\) and \(g_1 = g_2\).
Conversely, for each \(i: (L \cup M) \to K\), there exists functions
\(f = i|_L\) and \(g = i|_M\) such that \((f,g)\) maps to \(i\).
Therefore, this mapping \((f,g) \to h\) is bijective.

Next, we want to show that

\begin{equation*}
\kappa^{\lambda \mu} = |K^{L \times M}| \stackrel{?}{=} |(K^L)^M| = (\kappa^\lambda)^\mu
\end{equation*}

Then, we map \(f: M \to K^L\) to \(g: (M\times L) \to K\)
where \(g((x,y)) = f(x)(y)\). Suppose that \(f_1\) and \(f_2\)
both map to \(g\), then

\begin{equation*}
\begin{aligned}
&[\forall x \in M, y \in L: f_1(x)(y) = f_2(x)(y)]
\\&\implies [\forall x \in M: f_1(x) = f_2(x)]
\\&\implies f_1 = f_2
\end{aligned}
\end{equation*}

Next, if \(g \in (M \times L) \to K\), we define
\(f: M \to K^L\) where \(f(x) = \{y \mapsto g(x,y)\}\).
Hence we get that \(f\) maps to \(g\) and hence
our mapping is bijective.

Finally, we want to show that

\begin{equation*}
(\kappa\lambda)^\mu = |(K \times L)^M| \stackrel{?}{=} |K^M \times L^M| = \kappa^\mu \lambda^\mu
\end{equation*}

Then, we map \((f,g)\) to \(h: M \to (K\times L)\) where

\begin{equation*}
h(x) = (f(x),g(x))
\end{equation*}

This function must be injective. Also, for each \(h : M \to (K\times L)\),
we can define \(f: M\to K\) and \(g: M \to L\) by the first
and second coordinates of the mapping of \(h\).
Therefore, our mapping is bijective.
\end{example}

\subsection{Powerset}
\label{develop--math3274:cardinality:page--arithmetic.adoc---powerset}

Let \(A\) be a set, then

\begin{equation*}
|A| < |P(A)| = |2^A| = 2^{|A|}
\end{equation*}

Note that \(2\) has two different meanings in the above equation. In \(2^A\), \(2 = \mathcal{N}_2 = \{0,1\}\)
while in \(2^{|A|}\), \(2 = |\{0,1\}|\).

\section{Injective homomorphism from \(\mathbb{N}\) to cardinals}
\label{develop--math3274:cardinality:page--arithmetic.adoc---injective-homomorphism-from-latex-backslash-latex-backslashmathbb-latex-openbracen-latex-closebrace-latex-backslash-to-cardinals}

We can embed the natural numbers into cardinal numbers by the injective
homomorphism

\begin{equation*}
\phi(n) = |\mathcal{N}_n|
\end{equation*}

which preserves addition, multiplication and exponentiation.

\begin{example}[{Proof}]
Clearly this function is injective.

Notice that

\begin{equation*}
\begin{aligned}
\phi(m) + \phi(n)
&= |\mathcal{N}_m| + |\mathcal{N}_n|
\\&= |\mathcal{N}_m| + |\{x: m\leq x < n + m\}|
\\&= |\mathcal{N}_m \cup\{x: m\leq x < n + m\}|
\\&= |\{x: 0 \leq x < n + m\}\}|
\\&= |\mathcal{N}_{m+n}|
\\&= \phi(m + n)
\end{aligned}
\end{equation*}

\begin{admonition-todo}[{}]
Multiplication and exponentiation. Possible idea is induction
\end{admonition-todo}
\end{example}
\end{document}
