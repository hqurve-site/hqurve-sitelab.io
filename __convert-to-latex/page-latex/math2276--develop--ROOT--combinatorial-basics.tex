\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Combinatorial Basics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Perhaps the most important thing in mathematics is counting. Of course,
the simplest way to count is by enumerating over all objects. However,
this can prove tedious when there is a large number of objects. In order
to combat this, many different strategies of varying difficulty are
employed. Moreover, these strategies can be used together to follow
complex situations where no one strategy suffices. These strategies will
be explored in this section.

\section{Basic principles}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---basic-principles}

\subsection{Multiplication}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---multiplication}

Suppose there is an algorithm which enumerates over all possible objects
(once). If in each of the \(k\) stages of the algorithm, the
number of following outcomes, \(n_i\), is constant, the total
number of objects, \(N\), is given by

\begin{equation*}
N = n_1 n_2 \ldots n_k = \prod_{i=1}^k n_i
\end{equation*}

\subsection{Addition}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---addition}

If there are \(k\) disjoint sets each containing
\(n_i\) items. The total number of ways, \(N\), of
choosing one item is

\begin{equation*}
N = n_1 + n_2 +\ldots + n_k = \sum_{i=1}^k n_i
\end{equation*}

\subsection{Division}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---division}

Sometimes, it is easier to count the number of cases including
repetition and then attempt to remove it afterwards. If the count
including repetition was \(n\) but each of the objects counted
was repeated \(k\) times, the count excluding repetition,
\(N\), is simply

\begin{equation*}
N = \frac{n}{k}
\end{equation*}

\subsection{One to one correspondence}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---one-to-one-correspondence}

Suppose we have two sets \(A\) and \(B\) and there
exists a bijection \(f\) between the two. This means that each
element in \(B\) is uniquely identified by \(A\)
through \(f\). Hence the cardinality of sets \(A\)
and \(B\) must be the same.\newline
In practice, if we want to count the number of elements in
\(A\), we can either directly find it or we could
alternatively find a set \(B\) with bijection \(f\)
and instead count the number of elements in \(B\).

\subsubsection{Generalization:}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---generalization}

Note that the relation \(f\) need not be a bijection but in
fact it can be any relation where each element in \(A\) is
related to exactly \(m\) elements in \(B\). In this
case we instead find that \(|A| = \frac{|B|}{m}\). Also, note
that the division principle is based on this generalization.

\subsection{Recursion}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---recursion}

If there is a complex problem where the number of outcomes
\(f(n)\) is based on one or more smaller cases, the problem is
said to be recursive. This is the same principle that mathematical
induction is based upon. Additionally, just like mathematical induction
a boundary condition is needed.\newline
Although there is no one solution for recursive problems a few
strategies will be discussed later on.

\section{Permutations}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---permutations}

Strictly speaking, a permutation \(\pi\) is a bijection on the
set \(S\). Although this fact won’t be used (yet?), its useful
and interesting to think of a permutation as a function. Anyways … in
these notes permutations will be thought of as orderings.\newline
So now, given \(n\) distinct objects, how many ways are there
to order them?\newline
Here are two ways to approach this problem:

\begin{enumerate}[label=\arabic*)]
\item Recursive approach\newline
    Let this number be \(p(n)\) and note that
    \(p(1) = 1\). Note that there are \(n\) ways to
    choose the first item and for each choice of the first item, there are
    \(p(n-1)\) ways to order the remaining \(n-1\)
    items. Simply put
    
    \begin{equation*}
    p(n) = \begin{cases}
                    1 \quad &\text{if } n = 1\\
                    n\times p(n-1) \quad &\text{otherwise}
                \end{cases}
    \end{equation*}
\item Direct approach\newline
    Using the information gained from the first approach we can clearly see
    that
    
    \begin{equation*}
    p(n) - n(n-1)(n-2)\ldots (2)(1)
    \end{equation*}
    
    since there are \(n\) ways to place the first item,
    \(n-1\) for the second, \(n-2\) for the third and so
    on.\newline
    Additionally, since the function \(p\) is very common, we
    denote it with a special notation
    
    \begin{equation*}
    n! = p(n)
    \end{equation*}
    
    pronounced ''\(n\) factorial''
\end{enumerate}

\begin{admonition-note}[{}]
We define \(0! = 1\). In many of the following formulae
\(0!\) appears and in fact it makes sense since there is only
\(1\) way to order nothing. In general, you need not worry
whether the argument of the factorial is positive number as many
arguments also work when the \(n=0\).
\end{admonition-note}

\subsection{Permutation of non-distinct objects}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---permutation-of-non-distinct-objects}

Suppose there are \(k\) classes of objects each with
\(n_i\) identical objects. Such that
\(\sum_{i=1}^k n_i = n\) where \(n\) is the total
number of objects. Then how many different permutations are there?\newline
The simplest solution is to first act as if all the objects are distinct
and then removing repetition. If the \(n\) objects were
distinct, we get \(n!\). However, for each of different
classes, there are \(n_i!\) ways to reorder the objects such
that we get the same permutation (since they are identical). Therefore,
the total number of ways to permute these objects, \(N\), is
given by

\begin{equation*}
N = \frac{n!}{n_1! n_2! \ldots n_k!} = \frac{n!}{\prod_{i=1}^k n_i!}
\end{equation*}

\section{Ordered selection}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---ordered-selection}

Suppose a selection of \(r\) objects must be made from a set
\(n\) distinct objects. In how many ways can this be done? We
denote this number \({}^{n}\!P_{r}\). Here are two approaches

\subsection{Approach 1}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---approach-1}

Perhaps the simplest approach to this problem is by using an algorithm
similar to that used in the ''Permutations''. However, we stop after the
\(r\)’th step. This means that the last number to multiply is
\(n - r + 1\). Therefore

\begin{equation*}
{}^{n}\!P_{r} = n(n-1)(n-2)\ldots(n-r+2)(n-r+1)
\end{equation*}

And after some algebraic manipulation we get

\begin{equation*}
{}^{n}\!P_{r} = \frac{n(n-1)(n-2)\ldots(n-r+2)(n-r+1)(n-r)(n-r-1)\ldots(2)(1)}{(n-r)(n-r-1)\ldots(2)(1)} = \frac{n!}{(n-r)!}
\end{equation*}

\subsection{Approach 2}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---approach-2}

Although this approach is not initially apparent, it is in fact more
elegant. One way to enumerate over all possible selections is to first
write each of the permutations of the \(n\) objects and only
take the first \(r\) objects.\newline
Of course we can see that we get all possible selections of
\(r\) objects but not we obviously have repeats. But how many?
Well, the number of repetitions of a single selection of \(r\)
objects is precisely the number of ways the last \(n-r\)
objects appear. This is simply \((n-r)!\) and therefore our
final answer is

\begin{equation*}
{}^{n}\!P_{r} = \frac{n!}{(n-r)!}
\end{equation*}

\begin{admonition-note}[{}]
This argument also works on the boundary of \(r=n\) due to the
definition of \(0!\). Moreover, note that in this case, the
\({}^{n}\!P_{r}\) reduces to \(n!\) as expected.
\end{admonition-note}

\section{Unordered selection}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---unordered-selection}

Now why did we go through ordered selections first? Well of course, its
because the concept of unordered selection readily applies. Simply put
the number of ways to select \(r\) objects from
\(n\) objects where the order doesn’t matter is given by

\begin{equation*}
{}^{n}C_{r} = \binom{n}{r} = \frac{{}^{n}\!P_{r}}{r!} = \frac{n!}{r!(n-r)!}
\end{equation*}

How does this formula work? Well very similar to the second approach of
computing \({}^{n}\!P_{r}\). We first choose our
\(r\) objects from \(n\) with order. However, since
the objects are distinct, there are exactly \(r!\) ways that
this set of \(r\) objects was selected, so we divide.

\section{Ordered Selection with repetition}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---ordered-selection-with-repetition}

Quite simply, the number of ways to choose \(r\) objects from
\(n\) distinct objects with repetition is

\begin{equation*}
n^r
\end{equation*}

This is because we have an algorithm of \(r\) steps where we
have \(n\) choices at each step.

\section{Unordered Selection with repetition}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---unordered-selection-with-repetition}

What is the number of ways to place \(n\) objects into
\(r\) classes of objects with repetition? This at first seems
like a pretty hard problem since we cant just divide because each
selection may does not have the same number of occurrences. So how
exactly do we solve this problem?

\subsection{Solution}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---solution}

Suppose we have our selection of \(n\) objects. Then we can
order the classes and place each of the \(n\) objects into
their respective class (in a line). Finally, we place bars between each
class. Now we have a line of bars and objects objects of the same class
are next to each other. Now, notice a few things:

\begin{itemize}
\item the number of bars is \(r-1\) and hence the length of the
    line is \(r+n-1\)
\item the classification of the objects is uniquely determined by the
    positioning of the bars as well as the order of the unique classes.
\end{itemize}

Therefore, there exists a one to one correspondence between choosing
placing \(r-1\) bars into \(r+n-1\) spaces and
placing \(n\) objects into \(r\) classes. Therefore,
the number of ways to place \(n\) objects into \(r\)
classes is

\begin{equation*}
\binom{r+n-1}{r-1} = \binom{n+r-1}{n}
\end{equation*}

\section{Principle of Inclusion-Exclusion}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---principle-of-inclusion-exclusion}

Let \(S\) be a set of objects and
\(P_1, \ldots, P_r\) be properties which the elements of
\(S\) may or may not possess (ie predicates on
\(S\)). Let \(N(i, j, \ldots, k)\) denote the number
of elements of \(S\) which possess properties
\(P_i, P_j, \ldots, P_k\) (and possibly others). Then the
number of elements of \(S\) possessing at least one of the
properties is

\begin{equation*}
\sum_i N(i) - \sum_{i < j} N(i, j) + \sum_{i < j < k} N(i, j, k) - \ldots + (-1)^{r-1} N(1, 2, \ldots, r)
\end{equation*}

\begin{admonition-note}[{}]
the proof of this property is simply by considering how much each
element in \(S\) contributes to the sum. If the element
satisfies no properties, its contribution is \(0\) however, if
it contributes to \(t\geq 1\) properties, its contribution is
\(1\).
\end{admonition-note}

\subsection{Inverted version}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---inverted-version}

There is another version of this which is perhaps a little more useful.
What is the number of objects not fitting a certain criteria is
required. Well simply this is the total number of objects minus the
number of objects fitting the criteria. Now if there are multiple
criteria, we can simply apply the principle of inclusion-exclusion then
invert. Therefore, the number of ways in which a set of criteria is not
met is given by

\begin{equation*}
N - \sum_i N(i) + \sum_{i < j} N(i, j) - \sum_{i < j < k} N(i, j, k) + \ldots + (-1)^{r} N(1, 2, \ldots, r)
\end{equation*}

where \(N\) is the total number of objects.

\section{Pigeonhole Principle}
\label{develop--math2276:ROOT:page--combinatorial-basics.adoc---pigeonhole-principle}

The pigeonhole principle, also referred to as Dirichlet’s drawer
principle, asserts the following

Given \(n > m\) objects, partitioned into \(m\)
subsets, there exists at least one subset with more than one object in
it.

This is easy to prove (by contradiction) and even though it is easily
proven on spot it is much easier to quote the result.

Furthermore, the principle has a more generalized form as follows

Given \(n > km\) objects, partitioned into \(m\)
subsets, there exists at least one subset with more than \(k\)
objects in it.
\end{document}
