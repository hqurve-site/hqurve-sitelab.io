\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Linear Transformations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Let \(V, W\) be vector spaces with field \(\mathbb{K}\). Then,
\(T: V \to W\) is a \emph{linear transformation} iff

\begin{itemize}
\item \(\forall \vec{v}_1, \vec{v}_2 \in V: T(\vec{v}_1 + \vec{v}_2) = T(\vec{v}_1) + T(\vec{v}_2)\)
\item \(\forall \vec{v} \in V: \forall \alpha \in \mathbb{K}: T(\alpha\vec{v}) = \alpha T(\vec{v})\)
\end{itemize}

Furthermore, if \(V = W\), we call \(T\) a \emph{linear operator}.

The set of linear transformations from \(V\) to \(W\) is denoted
\(\mathcal{L}(V, W)\) and the set of linear operators on \(V\) is denoted by \(\mathcal{L}(V, V)\)
or \(\mathcal{L}(V)\).

Some nice examples of linear transformations include

\begin{itemize}
\item The coordinate vector: \(T(\vec{v}) = [\vec{v}]_{\mathcal{B}}\)
\item The zero mapping: \(\theta(\vec{v}) = \vec{0}_W\)
\item The identity mapping: \(I_V(\vec{v}) = \vec{v}\)
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-math3273/linear-transformations/identity}
    \end{figure}
\end{itemize}

\section{Operations on \(\mathcal{L}(V, W)\)}
\label{develop--math3273:linear-transformations:page--index.adoc---operations-on-latex-backslash-latex-backslashmathcal-latex-openbracel-latex-closebracev-w-latex-backslash}

Notice that, \(\mathcal{L}(V, W)\) forms a vectorspace with field \(\mathbb{K}\) where
the operations are defined as

\begin{equation*}
\begin{aligned}
&\forall \vec{v} \in V: (S+T)(\vec{v}) = S(\vec{v}) + T(\vec{v})
\\&\forall \vec{v} \in V: (\alpha T)(\vec{v}) = \alpha T(\vec{v}) = T(\alpha \vec{v})
\end{aligned}
\end{equation*}

Additionally, if we also consider \(\mathcal{L}(W, X)\) we get that

\begin{equation*}
\forall T \in \mathcal{L}(V, W): \forall U \in \mathcal{L}(W,X): U\circ T \in \mathcal{L}(V,X)
\end{equation*}

In particular if \(V=W=X\), the set of linear operators \(\mathcal{L}(V)\), it forms a
\myautoref[{monoid}]{develop--math2272:ROOT:page--groups/index.adoc} by using function composition as the operator and
the identity is the identity mapping.

\section{\(T\)-invariants}
\label{develop--math3273:linear-transformations:page--index.adoc---latex-backslasht-latex-backslash-invariants}

Let \(T \in \mathcal{L}(V)\) and \(U\subseteq V\) such that \(T(U) \subseteq U\).
Then we say that \(U\) is \emph{invariant} with respect to \(T\), or simply,
\(U\) is \emph{\(T\)-invariant}. Then if \(p\) is a polynomial,
\(p(T) \in \mathcal{L}(V)\) and the following
hold

\begin{itemize}
\item \(p(T)(U)\) is \(T\)-invariant
\item \((p(T))^{-1}(U) = \{\vec{v} \in V: p(T)(\vec{v})\}\) (preimage) is \(T\)-invariant
\item \(\{\vec{0}\}\) and \(V\) are \(T\)-invariant.
\end{itemize}

\begin{example}[{Proof}]
For this, we let

\begin{equation*}
p(x) = \sum_{i=0}^n a_i x^i
\end{equation*}

Firstly, consider \(\vec{v} \in p(T)(U)\). Then there exists \(\vec{u} \in U\)
such that \(\vec{v} = p(T)(\vec{u})\). Hence

\begin{equation*}
T(\vec{v}) = T(p(T)(\vec{u}))
= T\left(\sum_{i=0}^n a_i T^i(\vec{u})\right)
= \sum_{i=0}^n a_i T^{i+1}(\vec{u})
= \sum_{i=0}^n a_i T^{i}(T(\vec{u}))
= p(T)(T(\vec{u}))
\in p(T)(U)
\end{equation*}

since \(T(\vec{u}) \in U\).

Next, consider \(\vec{v} \in (p(T))^{-1}(U)\). Then \(p(T)(\vec{v}) \in U\)
and

\begin{equation*}
p(T)(T(\vec{v}))
= \sum_{i=0}^n a_i T^{i+1}(\vec{v})
= T\left(\sum_{i=0}^n a_i T^i(\vec{v})\right)
= T(p(T)(\vec{v}))
\in U
\end{equation*}

since \(p(T)(\vec{v}) \in U\). Hence we get that \(T(\vec{v}) \in (p(T))^{-1}(U)\).

Finally, \(T(\vec{0}) = \vec{0}\) so the trivial subspace is \(T\) invariant
and by definition \(T(V) \subseteq V\).
\end{example}

In particular, for any \(T\) the following hold

\begin{itemize}
\item \(\ker(T)\) is \(T\)-invariant since \(\ker(T) = T^{-1}(\{\vec{0}\})\)
\item \(T(V)\) is \(T\)-invariant
\item \((T-\lambda)(V)\) is \(T\)-invariant
\item \(\ker[(T-\lambda)^k]\) is \(T\)-invariant
\end{itemize}

\subsection{Partitioning the space}
\label{develop--math3273:linear-transformations:page--index.adoc---partitioning-the-space}

Let \(f\) be a polynomial such that \(f(T) = \theta\) and \(f=gh\) where \(gcd(g,h) = 1\),
then

\begin{equation*}
V = \ker(g(T)) \oplus \ker(h(T))
\end{equation*}

Notice that if \(f = \prod_{i=1}^n g_i\) where \(gcd(g_i, g_j) = g_i\delta_{ij}\) then
we can extend this statement as

\begin{equation*}
V = \ker(g_1(T)) \oplus \ker(g_2(T)) \oplus \cdots \oplus \ker(g_n(T))
\end{equation*}

Additionally, each of the \(\ker(g_1(T))\) are \(T\)-invariant from above.

\begin{admonition-important}[{}]
The initial statement on its own is not enough to prove this generalized
statement, however, the proof below, provides a sufficient statement.
\end{admonition-important}

\begin{example}[{Proof}]
We would actually prove a more general version of the statement
since it allows for easy extension to larger products. Let \(gcd(g,h) = 1\)
then

\begin{equation*}
\ker(g(T)h(T)) = \ker(g(T)) \oplus \ker(h(T))
\end{equation*}

\begin{admonition-note}[{}]
\(\ker(f(T)) = V\) since \(f(T) = \theta\).
\end{admonition-note}

\begin{admonition-important}[{}]
Throughout this proof we use the following two results

\begin{itemize}
\item \(f(T)g(T) = g(T)f(T)\). This holds since both sides polynomials of \(T\)
    which commute since powers of \(T\) commute.
\item \((f(T)g(T))(\vec{v}) \equiv f(T)g(T)(\vec{v}) = f(T)[g(T)(\vec{v})\). This holds since
    
    \begin{equation*}
    \begin{aligned}
    \left(\left(\sum_{i} a_i T^i\right)\left(\sum_{j} b_j T^j\right)\right)(\vec{v})
    &= \left(\sum_{i,j} a_ib_j T^{i+j}\right)(\vec{v})
    \\&= \sum_{i,j} a_ib_j T^{i+j}(\vec{v})
    \\&= \sum_{i,j} a_i T^{i}(b_jT^j(\vec{v}))
    \\&= \sum_{i} a_i T^{i}\left(\sum_j b_jT^j(\vec{v})\right)
    \end{aligned}
    \end{equation*}
\end{itemize}
\end{admonition-important}

Firstly, since \(gcd(g,h) = 1\), there exists \(a,b \in \mathbb{K}[X]\) such that
\(1=ag+bh\). Then by applying \(T\) to both sides of the polynomial

\begin{equation*}
I = a(T)g(T) + b(T)h(T)
\end{equation*}

Now, consider arbitrary \(\vec{v} \in \\ker(g(T)h(T))\) then from above

\begin{equation*}
\vec{v} = a(T)g(T)(\vec{v}) + b(T)h(T)(\vec{v})
\end{equation*}

Then notice that

\begin{equation*}
g(T)[b(T)h(T)(\vec{v})]
= g(T)b(T)h(T)(\vec{v})
= b(T)g(T)h(T)(\vec{v})
= b(T)[g(T)h(T)(\vec{v})]
= b(T)(\vec{0})
= \vec{0}
\end{equation*}

hence \(b(T)h(T)(\vec{v}) \in \ker(g(T))\). Next,

\begin{equation*}
h(T)[a(T)g(T)(\vec{v})]
= h(T)a(T)g(T)(\vec{v})
= a(T)g(T)h(T)(\vec{v})
= a(T)[g(T)h(T)(\vec{v})]
= a(T)(\vec{0})
= \vec{0}
\end{equation*}

hence \(a(T)g(T)(\vec{v}) \in \ker(h(T))\). Therefore

\begin{equation*}
\ker(g(T)) + \ker(h(T)) = \ker(g(T)h(T))
\end{equation*}

Next, suppose that \(\vec{v} \in \ker(g(T)) \cap \ker(h(T))\). Then

\begin{equation*}
\vec{v}  = a(T)g(T)(\vec{v}) + b(T)h(T)(\vec{v}) = a(T)(\vec{0}) + b(T)(\vec{0}) = \vec{0}
\end{equation*}

Hence

\begin{equation*}
\ker(g(T)) \oplus \ker(h(T)) = \ker(g(T)h(T))
\end{equation*}
\end{example}
\end{document}
