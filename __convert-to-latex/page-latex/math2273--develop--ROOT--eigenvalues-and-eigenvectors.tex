\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Eigenvalues and Eigenvectors}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Definition}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---definition}

Let \(\mathbb{F}\) be a field and
\(A \in \mathbb{F}^{n\times n}\).

An element \(\lambda \in \mathbb{F}\) is called an
\emph{eigenvalue} or \emph{characteristic root} of \(A\) iff

\begin{equation*}
\exists \underline{v} \in \mathbb{F}^n \backslash \{\underline{0}\}:
    A \underline{v} = \lambda \underline{v}
\end{equation*}

where any corresponding vector \(v\) is called an
\emph{eigenvector} or \emph{characteristic vector}. Furthermore,

\begin{equation*}
A \underline{v} = \lambda \underline{v} \implies (A - \lambda I_n) \underline{v} = \underline{0}
\end{equation*}

where \(I_n \in \mathbb{F}^{n\times n}\) (or simply
\(I\)) is the identity element for matrix multiplication.
Therefore, \(\underline{v} \in Null(A -\lambda I_n)\),
referred to as the \emph{eigenspace} corresponding to \(\lambda\),
and since \(\underline{v} \neq \underline{0}\) it follows that
\(\det(A - \lambda I_n) = 0\). In fact, its not hard to see
that the converse is also true, that is

\begin{equation*}
\lambda \text{ is an eigenvalue} \Longleftrightarrow \det(A -\lambda I) = 0
\end{equation*}

\subsection{Characteristic polynomial and equation}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---characteristic-polynomial-and-equation}

The polynomial \(p_A(\lambda) = det(A - \lambda I)\) is the
\emph{characteristic polynomial} of \(A\) and has order
\(n\). Additionally, the equation
\(p_A(\lambda) = 0\) is \emph{characteristic equation}

\subsection{Computing eigenvalues and eigenvectors}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---computing-eigenvalues-and-eigenvectors}

In order to determine the eigenvalues of \(A\), the
characteristic equation needs to be solved. Then, for each eigenvalue
\(\lambda\), the eigenspace space is determined by finding the
solution set of
\((A - \lambda I) \underline{v} = \underline{0}\). All vectors
(excluding \(\underline{0}\)) are eigenvectors corresponding
to \(\lambda\).

\section{Spectrum}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---spectrum}

The \emph{spectrum} of a matrix \(A\in \mathbb{F}^{n \times n}\),
denoted \(\sigma(A)\) is the collection eigenvalues of
\(A\) (not necessarily) distinct. Additionally, the \emph{algebraic
multiplicity}, of \(\lambda \in \sigma(A)\) is the number of
times it appears in \(A\) or equivalently, the multiplicity of
the root \(\lambda\) in \(p_A(\lambda) = 0\).
Furthermore, the \emph{geometric multiplicity} of
\(\lambda \in \sigma(A)\) is the dimension of the eigenspace
of \(\lambda\) or equivalently the dimension of
\(Null(A-\lambda I)\).

\subsection{Relationship of geometric and algebraic multiplicities}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---relationship-of-geometric-and-algebraic-multiplicities}

The geometric multiplicity is always at most the algebraic multiplicity.

\begin{example}[{Proof}]
Consider eigenvalue \(\lambda\) with geometric multiplicity
\(k\). Then their exists \(k\) orthonormal vectors,
\(\underline{b}_1, \ldots, \underline{b}_{k}\), forming a
basis for \(Null(A - \lambda I)\). We can now extend this
orthonormal basis to span the entirety of \(\mathbb{F}^n\).
Let these vectors be
\(\underline{b}_{k + 1}, \ldots, \underline{b}_n\). Then,

\begin{equation*}
\begin{bmatrix}
            \underline{b}_1 & \underline{b}_2 & \cdots & \underline{b}_n
        \end{bmatrix}^T
        \begin{bmatrix}
            \underline{b}_1 & \underline{b}_2 & \cdots & \underline{b}_n
        \end{bmatrix}
        =
        \begin{bmatrix}
            \left\langle \underline{b}_1, \underline{b}_1 \right\rangle & \left\langle \underline{b}_1, \underline{b}_2 \right\rangle & \cdots & \left\langle \underline{b}_1, \underline{b}_n \right\rangle\\
            \left\langle \underline{b}_2, \underline{b}_1 \right\rangle & \left\langle \underline{b}_2, \underline{b}_2 \right\rangle & \cdots & \left\langle \underline{b}_2, \underline{b}_n \right\rangle\\
            \vdots & \vdots & \ddots & \vdots\\
            \left\langle \underline{b}_n, \underline{b}_1 \right\rangle & \left\langle \underline{b}_n, \underline{b}_2 \right\rangle & \cdots & \left\langle \underline{b}_n, \underline{b}_n \right\rangle
        \end{bmatrix}
        = I
\end{equation*}

That is
\(P = \begin{bmatrix} \underline{b}_1 & \underline{b}_2 & \cdots & \underline{b}_n \end{bmatrix}\)
is an orthogonal matrix. Then, let \(D = P^{-1} A P\) and
since for \(1 \leq i \leq k\)

\begin{equation*}
\underline{b}_i^T A \underline{b}_i = \underline{b}_i^T (\lambda \underline{b}_i) = \lambda \left\langle \underline{b}_i, \underline{b}_i \right\rangle = \lambda
\end{equation*}

the upper left hand of \(D\) is the diagonal matrix
\(\lambda I_{k}\). Furthermore,

\begin{equation*}
P^{-1} (A - x I) P = P^{-1} A P - x P^{-1} I P = D - xI \quad\text{where } x\in \mathbb{F}
\end{equation*}

where the upper left hand of \(D-xI\) is the diagonal matrix
\((\lambda - x)I_{k}\). Therefore, since
\(p_A(x) = \det(A-xI) =\det(D-xI)\) contains a factor
\((\lambda - x)^{k}\), the algebraic multiplicity is at least
\(k\).

 ◻
\end{example}

\subsection{Similar matrices}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---similar-matrices}

If two matrices \(A\) and \(B\) are similar, they
share the same characteristic polynomial. This result is clear since
\(A\) and \(B\) represent the same transformation
however it can also be seen by the following equality chain

\begin{equation*}
\begin{aligned}
    p_A(\lambda)
    &= \det(A - \lambda I)\\
    &= \det(P^{-1} B P - \lambda P^{-1} I P)\\
    &= \det(P^{-1} (B - \lambda I ) P)\\
    &= \det(P^{-1})\det(B - \lambda I) \det(P)\\
    &= \det(B - \lambda I)\\
    &= p_B(\lambda)\end{aligned}
\end{equation*}

\subsection{Diagonalizable}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---diagonalizable}

Recall, a matrix \(A\) is diagonalizable if it is similar to a
diagonal matrix. Then the following statements are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(\exists P \in \mathbb{F}^{n\times n}\) such that
    \(P^{-1} A P = diag(d_1, d_2, \ldots, d_n)\)
\item \(A\) has \(n\) linearly independent eigenvectors
    and the \(i\)’th column of \(P\) is the eigenvector
    corresponding to \(d_i\) and geometric and algebraic
    multiplicities are equal for all eigenvalues \(\lambda\).
\end{enumerate}

\begin{example}[{Proof}]
Firstly,

\begin{equation*}
P^{-1} A P = diag(d_1, d_2, \ldots, d_n)
        \implies AP = P \, diag(d_1, d_2, \ldots, d_n)
\end{equation*}

Now, by focusing on the \(i\)’th column of the matrices of
above, \(A \underline{v}_i = d_i\underline{v}_i\), that is
\(\underline{v}_i\) is an eigenvalue corresponding to
\(d_i\). Furthermore, since \(P\) is invertible,
each of the \(\underline{v}_i\) are linearly independent and
hence the geometric multiplicity of each of the \(d_i\) is at
its maximum. That is the geometric multiplicity is equal to the
algebraic multiplicity for all eigenvalues.

Conversely, by constructing the matrix \(P\) using the
\(n\) linearly independent eigenvectors corresponding to
\(d_1, d_2, \ldots, d_n \in \sigma(A)\), we obtain an
invertible matrix \(P\). This is possible since the geometric
multiplicity is equal to the algebraic multiplicity and that no
eigenvector can be part of two eigenspaces. Therefore, we get that
\(A P = P\, diag(d_1, d_2, \ldots, d_n)\) and since
\(P\) is invertible, we get the desired result.

 ◻
\end{example}

\subsection{Cayley-Hamilton Theorem}
\label{develop--math2273:ROOT:page--eigenvalues-and-eigenvectors.adoc---cayley-hamilton-theorem}

Each matrix \(A \in \mathbb{F}^{n\times n}\) satisfies its own
characteristic equation.
\end{document}
