\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Combinatorial Arguments for rational statements}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% complex conjugate
\def\conj#1{\overbar{#1}}

% Title omitted
This is promped by "A weird proof that \(e \leq 3\)" by
\href{https://www.youtube.com/channel/UC21xJ4CJRyoGvI4Icr-J3aQ}{Liam Appelbe}

[Youtube video \href{https://youtube.com/watch?v=myKkhVy74V4}]

The end card of the video proposes 4 problems. For each problem, there would be two equivalent proofs:
a simple worded explaination and a set theoretic proof. Note that on this page, \(0\) is considered
as a natural number and we define \(J_k\) to be the set of natural numbers less than or equal to \(k\).

\section{\(\sum_{k=0}^n k(n-k) = \binom{n+1}{3}\) difficulty: \(3/5\)}
\label{develop--fun:math:page--v1.adoc---latex-backslash-latex-backslashsum-latex-underscore-latex-openbracek0-latex-closebrace-latex-caretn-kn-k-latex-backslashbinom-latex-openbracen1-latex-closebrace-latex-openbrace3-latex-closebrace-latex-backslash-difficulty-latex-backslash35-latex-backslash}

Consider the right hand expression. Then, this is simply number of ways of choosing \(3\) of \(n+1\) items.

For the left hand expression, consider a line of \(n+1\) items which are zero-indexed. Then, \(k\)
represents one of these items. We then choose one of the \(k\) items preceeding the \(k\)'th item
and one of the \(n-k\) items following the \(k\)'th item. Then, this is an equivalent way of choosing \(3\)
from \(n+1\) where the \(k\)'th item is the middle choice.
Note that if \(k \in \{0,n\}\), obviously, there are no choices to make since
the middle element is at the end.

\subsection{Set theory}
\label{develop--fun:math:page--v1.adoc---set-theory}

We can also do this in a more rigourous fashion using set theory. Consider the two sets

\begin{itemize}
\item \(\bigcup_{k=0}^n \{a,b \in J_{n+1} \ \mid \ a < k < b\}\)
\item \(\{a,k,b \in J_{n+1} \ \mid \ a < k < b\}\)
\end{itemize}

Then, the cardinality of these two sets are the left and right side (respectively) of the desired equality. Then,
the result follows since both sets are clearly equal.

\section{\(\sum_{k=0}^n k^2\binom{n}{k} = 2^{n-1}\binom{n+1}{2} = 2^{n-1}n + 2^{n-2}n(n-1)\) difficulty: \(4/5\)}
\label{develop--fun:math:page--v1.adoc---latex-backslash-latex-backslashsum-latex-underscore-latex-openbracek0-latex-closebrace-latex-caretn-k-latex-caret2-latex-backslashbinom-latex-openbracen-latex-closebrace-latex-openbracek-latex-closebrace-2-latex-caret-latex-openbracen-1-latex-closebrace-latex-backslashbinom-latex-openbracen1-latex-closebrace-latex-openbrace2-latex-closebrace-2-latex-caret-latex-openbracen-1-latex-closebracen-2-latex-caret-latex-openbracen-2-latex-closebracenn-1-latex-backslash-difficulty-latex-backslash45-latex-backslash}

Consider the middle expression and notice that it counts the number of ways of colouring \(n+1\) objects in red, blue
and white where exactly two objects are coloured white.

For the first expression, consider the same \(n+1\) objects. We first choose \(k\) of the first \(n\) objects;
we call these objects squishy. Then, of the squishy objects,
we make two independent selections. We then colour based on the following three cases

\begin{itemize}
\item If the two selections are the same, we colour the object white. We also colour the \(n+1\)'th object white
\item If the two selections are different, we colour them both white and colour the \(n+1\)'th object
    
    \begin{itemize}
    \item red if the first selection is before the first
    \item blue if the first selection is after the first
    \end{itemize}
\end{itemize}

We then colour all remaining squishy objects red and the unselected objects blue. Note that from a colouring
we can uniquely determine a selection which produces such a colouring.

For the final expression, consider the same \(n+1\) objects. If we colour the last object
white, we then choose one of the other \(n\) objects to be white and then
colour the remaining objects red and blue in one of \(2^{n-1}\) ways.

Otherwise, we make two different selections of the first \(n\) objects to be white. If the first
selection is before the first, we colour the \(n+1\)'th object red, otherwise we colour it blue.
We then colour the remaining objects in one of \(2^{n-2}\) ways.

Hence, we have proven that all three expressions are equal.

\subsection{Set theoretic}
\label{develop--fun:math:page--v1.adoc---set-theoretic}

Consider the following set

\begin{equation*}
\{(W,R,B) \ \mid \ W \sqcup R \sqcup B = J_{n+1}, |W| = 2\}
\end{equation*}

We would show that the following sets are equivalent to set above (and hence to each other)

\begin{itemize}
\item \(\bigcup_{k=0}^n\left\{(a,b,C) \ \mid \ \ C \subseteq J_{n},\ |C| = k, \ a,b \in C \right\}\)
\item \(\{(W,R) \ \mid \ W \subseteq J_{n+1},\ |W| = 2, \ R \subseteq J_{n+1} - W\}\)
\item \(
    \{(x,n, S) \ | \ x \in J_{n}, \ S \subseteq J_n - \{x\}\}
    \ \cup\ \{(x,y, S) \ \mid | \ x,y \in J_n,\ x\neq y,\ S \subseteq J_n - \{x,y\}\}
    \)
\end{itemize}

where each of the above sets has the cardinality of the respective expressions.

\subsubsection{Second equivalent to reference}
\label{develop--fun:math:page--v1.adoc---second-equivalent-to-reference}

The reference set is equivalent to the middle set by the following function

\begin{equation*}
(W,R) \mapsto (W, R, J_{n+1} - W - R)
\end{equation*}

\subsubsection{First and second equivalent}
\label{develop--fun:math:page--v1.adoc---first-and-second-equivalent}

The first and second sets are equivalent by the following function

\begin{equation*}
(a,b,C) \mapsto \begin{cases}
(\{a,n\},\ C - \{a\}) &\quad\text{if } a =b\\
\left(\{a,b\},\ (C - \{a,b\}) \cup \{n\}\right) &\quad\text{if } a < b\\
\left(\{a,b\},\ C - \{a,b\}\right) &\quad\text{if } a > b
\end{cases}
\end{equation*}

This function is clearly injective; however, it is a little less obvious as to why it is surjective.
To see that it is surjective, consider its inverse

\begin{equation*}
(\{s,t\}, R) \mapsto \begin{cases}
(s,\ s,\ R \cup\{s\}) &\quad\text{if } t = n\\
(s,\ t,\ (R - \{n\})\cup\{s, t\}) &\quad\text{if } n \in R\\
(s,\ t,\ R \cup\{s, t\}) &\quad\text{if } n \not\in R \text{ and } t \neq n
\end{cases}
\end{equation*}

where \(s < t\).

\subsubsection{Second and third equivalent}
\label{develop--fun:math:page--v1.adoc---second-and-third-equivalent}

The second and third sets are equivalent by the following function

\begin{equation*}
(x,y,S) \mapsto \begin{cases}
(\{x,y\}, S) \quad&\text{if } y = n\\
(\{x,y\}, S) \quad&\text{if } x < y \text{ and } y \neq n\\
(\{x,y\}, S \cup \{n\}) \quad&\text{if } x > y
\end{cases}
\end{equation*}

We only needed two cases, however, the first two cases were split to emphasize which
of the two sets the element came from. Also, this function is clearly bijective.

\section{\(\binom{n}{k-1}\binom{n}{k+1} \leq \binom{n}{k}^2\) difficulty: \(6/5\)}
\label{develop--fun:math:page--v1.adoc---latex-backslash-latex-backslashbinom-latex-openbracen-latex-closebrace-latex-openbracek-1-latex-closebrace-latex-backslashbinom-latex-openbracen-latex-closebrace-latex-openbracek1-latex-closebrace-latex-backslashleq-latex-backslashbinom-latex-openbracen-latex-closebrace-latex-openbracek-latex-closebrace-latex-caret2-latex-backslash-difficulty-latex-backslash65-latex-backslash}

\section{\(\sqrt[n]{\prod x_i} \leq \frac{1}{n}\sum x_i\) difficulty: \emoji{💀}\(/5\)}
\label{develop--fun:math:page--v1.adoc---latex-backslash-latex-backslashsqrtn-latex-openbrace-latex-backslashprod-x-latex-underscorei-latex-closebrace-latex-backslashleq-latex-backslashfrac-latex-openbrace1-latex-closebrace-latex-openbracen-latex-closebrace-latex-backslashsum-x-latex-underscorei-latex-backslash-difficulty-latex-backslashemoji-latex-openbrace-latex-closebrace-latex-backslash5-latex-backslash}

This is the AM-GM inequality where \(x_i \in \mathbb{N}\). The video also gave a transformed
form which only includes natural numbers.

\begin{equation*}
n^n\prod_{i=1}^n x_i \leq \left(\sum_{i=1}^n x_i\right)^n
\end{equation*}
\end{document}
