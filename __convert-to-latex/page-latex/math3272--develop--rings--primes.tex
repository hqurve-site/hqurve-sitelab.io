\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Translating primes numbers to rings}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
\def\normalsubgroup{\trianglelefteq}
\DeclareMathOperator{\ord}{ord}

% Title omitted
Consider the concept of prime numbers in \(\mathbb{Z}\). Then, we have the following
properties

\begin{itemize}
\item The only positive numbers which divide \(p\) are itself and \(1\).
    More generally \(p = ab\) implies \(a\) or \(b\) is \(\pm 1\)
    (the only units in \(\mathbb{Q}\))
\item The fundamental theorem of arithmetic. Every integer can be uniquely
    written as a product of primes
\item \(p | ab\) implies that \(p | a\) or \(p | b\)
\item \(\mathbb{Z}/p\mathbb{Z}\) is a field
\end{itemize}

We can almost translate each of these into the language of rings.
We would do so as follows

\begin{description}
\item[Irreducible] We say that \(p \in R\) is irreducible if \(p = ab\) implies
    that either \(a\) or \(b\) is a unit.
\item[Unique Factorization Domains] We say that integral domain \(D\) is
    a unique factorization domain is each element \(r \in D\) can be
    written as a product of irreducible elements and a unit.
\item[Prime Ideals] An ideal \(P \neq R\) is said to be prime iff \(ab \in P\)
    implies that \(a \in P\) or \(b \in P\). Additionally, if \(P=(p)\),
    we say that \(p\) is a prime element.
\item[Maximal Ideals] We say that ideal \(M \neq R\) is maximal iff \(M \subseteq J \subseteq P\)
    (where \(J\) is an ideal) implies that \(J = M\) or \(P\).
    
    \begin{admonition-remark}[{}]
    Yes I know, its not the same. We'll get to that.
    \end{admonition-remark}
\end{description}

\section{Remarks on nomenclature}
\label{develop--math3272:rings:page--primes.adoc---remarks-on-nomenclature}

Today, the notion of irreducibility greatly closely mirrors
what we take today as the definition of prime numbers; however,
instead we denote the name `prime' to ideals. Why is this?
Stack exchange user \texttt{mweiss} provides the following explanation

\begin{custom-quotation}[{mweiss}][{\url{https://math.stackexchange.com/a/813827}}]
I'm not an expert in the history of ring theory but this is, I think,
pretty close to a correct answer:

You are right that the notion of "prime integer" predates the more general notions of "prime element"
and "irreducible element" in an arbitrary ring. In fact, prime numbers go back to ancient Greece!
But there is a missing link in the evolution of that original notion into the (two distinct)
modern notions: namely, the notion of a prime ideal.

Ideals were regarded as a kind of "generalized number"; in fact, the original terminology
was "ideal number", only later shortened to "ideal". One ideal \(I\)
was said to divide another ideal \(J\) if and only if \(J \subseteq I\).
A prime ideal is then defined, in precise analogy with the "classical" definition of prime numbers
(i.e. as indecomposables) to be an ideal that is not divisible by any ideals other than itself and the entire ring.

Once "prime ideal" was defined, the next development was to say that an element was prime
if it generated a prime ideal. It is a fairly straightforward exercise to show that this translates
directly to the modern definition of prime element. It is also fairly easy to show that
(as long as there are no zero-divisors in the ring) every prime element is indecomposable
in the classic sense. So everything fits together quite nicely.

It is only at this point that somebody starts looking at rings like \(\mathbb{Z}[\sqrt{-5}]\),
which are not unique factorization domains, and realizes that those rings can contain elements
that are indecomposable in the classic sense, but do not generate prime ideals. Whoah! So we need
a name for those types of elements. "Prime" is already taken, so they get called "irreducible".

So there you have it. The elements that we now call "irreducible elements", despite the fact
that they have the property that we usually associate with "prime numbers", were not called
"prime elements" because that word was already in use for elements that generate "prime ideals",
which are defined in direct analogy with how we "usually" define prime numbers.
\end{custom-quotation}

Do not take this exert as final or true, but instead use it as a way of thought. Also,
as \texttt{Nishant} points out, this definition of prime ideals is actually that of maximal ideals.
The truth of the matter is that all three exhibit properties which we liken to that of prime
numbers and hence the choice of which best \emph{deserves} the title of `prime' may change with the
culture.

\section{Characterizations}
\label{develop--math3272:rings:page--primes.adoc---characterizations}

\subsection{Characterization I of prime ideals}
\label{develop--math3272:rings:page--primes.adoc---characterization-i-of-prime-ideals}

Let \(P\) be an ideal in \(R\). Then, \(P\)
is prime iff

\begin{equation*}
AB \subseteq P \implies [ A \subseteq P \text{ or } B \subseteq P ]
\end{equation*}

where \(A\) and \(B\) are ideals.

\begin{example}[{Proof}]
Lets first consider the converse. Consider, \(ab \in P\)
then, \((a)(b) \subseteq P\) and hence \((a) \subseteq P\)
or \((b) \subseteq P\) and hence \(a \in P\) or \(b \in P\).
Therefore \(P\) is prime.

Now, suppose that \(AB \subseteq P\) and suppose that \(A \not\subseteq P\).
Then, \(\exists a \in A \backslash P\). Consider arbitrary \(b \in B\).
Then since \(ab \in P\), \(b \in P\) and hence \(B \subseteq P\)
and we are done.
\end{example}

\subsection{Characterization II of prime ideals}
\label{develop--math3272:rings:page--primes.adoc---characterization-ii-of-prime-ideals}

Let \(P\) be an ideal in \(R\). Then

\begin{equation*}
P \text{ is prime} \iff R/P\text{ is an integral domain}
\end{equation*}

\begin{example}[{Proof}]
This is straightforward. Suppose that \(P\) is prime. Then,

\begin{equation*}
\begin{aligned}
(a + P)(b + P)= 0 + P
&\iff ab +P = 0 +P
\\&\iff ab \in P
\\&\iff a \in P \text{ or } b \in P
\\&\iff a +P = 0 +P \text{ or } b + P = 0 + P
\end{aligned}
\end{equation*}

Therefore \(R/P\) is an integral domain.

Conversely if \(R/P\) is an integral domain,

\begin{equation*}
ab \in P \implies (a+P)(b+P) = 0 + P \implies a \in P \text{ or } b \in P
\end{equation*}

from above
\end{example}

\subsection{Characterization of maximal ideals}
\label{develop--math3272:rings:page--primes.adoc---characterization-of-maximal-ideals}

Let \(M\) be an ideal in \(R\). Then,

\begin{equation*}
M \text{ is maximal} \iff R/M \text{ is a field}
\end{equation*}

\begin{example}[{Proof}]
By \myautoref[{this result}]{develop--math3272:rings:page--fields.adoc---ideals-in-fields}, we only need to show that
\(M\) is maximal iff the only ideals of \(R/M\) are \(R/M\) and \(0\).

Consider an ideal of \(R/M\). Then, by the
\myautoref[{lattice isomorphism theorem}]{develop--math3272:rings:page--isomorphism-theorems.adoc---lattice-isomorphism-theorem},
we get that this ideal must have the form \(J/M\). Furthermore, \(J\) must be an ideal of \(R\)
which contains \(M\). Then since \(M\) is maximal, either \(J = M\) or \(J=R\).
Hence, \(J/M = M/M = \{0 + M\}\) or \(J/M = R/M\). Then since the only
ideals of \(R/M\) are itself and the zero ideal, we get that \(R/M\) is a field.

Conversely, suppose that \(R/M\) is a field and consider an ideal \(J\) which contains \(M\).
Then \(J/M\) is an ideal in \(R/M\) and hence \(J=R\) or \(J=M\). Therefore,
\(M\) is maximal.
\end{example}
\end{document}
