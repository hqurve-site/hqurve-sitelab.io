\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Structure of  \&latex-backslash;(S \&latex-underscore;n \&latex-backslash;)}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\{{#1}\right\}}
\DeclareMathOperator{\sign}{sign}

% Title omitted
\begin{admonition-tip}[{}]
For a more in-depth discussion of \(S_n\) and the properties
used here, please see \myautoref[{}]{develop--math2272:ROOT:page--permutations/index.adoc}.
\end{admonition-tip}

Firstly, recall that a permutation is simply a bijection
on a set.

Now, let \(n \in \mathbb{N}\) and \(X\) be a set such that \(|X| = n\). Then,
we define \(S_n\) to be the set of permutations on \(X\).
Alternatively, instead of fixing \(X\), we could have said that \(S_n\)
is the set of permutations on \(n\) labels.
Then, \(S_n\) forms a group under function composition.

\section{Cycle classes}
\label{develop--math3610:cycle-index:page--structure-symmetric.adoc---cycle-classes}

Now, consider some \(\sigma \in S_n\) and decompose
it into a product of disjoint cycles. We then define the \emph{cycle structure}
of \(\sigma\) as

\begin{equation*}
1^{k_1}2^{k_2} \cdots n^{k_n}
\end{equation*}

where \(\sigma\) has \(k_i\) \(i\)-cycles in its decomposition. Hence,

\begin{equation*}
k_1 + 2k_2 + 3k_3 + \cdots nk_n = n
\end{equation*}

since each label must appear exactly once.
We then say that two permutations belong to the same \emph{cycle class}
iff they share the same cycle structure.

Let \(C(k_1, k_2, \ldots k_n)\) be the number of cycles of a given
class. To do this, we first fix the cycle decomposition
so that shorter cycles are to the left.

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math3610/cycle-index/cycle-class}
\end{figure}

Next, we will these \(n\) spaces with \(n\) labels. We can do this in \(n!\) ways.
However, notice that within each \(i\) cycle, there are \(i\) ways
to represent it. Since there are \(k_i\) \(i\)-cycles, we therefore get
\(\prod_{i=1}^n i^{k_i}\) repetitions by changing the representation
of each cycle. Additionally, the \(k_i\) \(i\)-cycles can be permuted in any of \(k_i!\)
different ways. Hence we get \(\prod_{i=1}^n k_i!\) repetitions
by rearranging the cycles. Therefore, each of the permutations can be represented
in \(\prod_{i=1}^n i^{k_i}k_i!\) distinct ways and hence

\begin{equation*}
C(k_1, k_2, \ldots k_n) = \frac{n!}{\prod_{i=1}^n i^{k_i}k_i!}
= \frac{n!}{1^{k_1}k_1!\ 2^{k_2}k_2!\ \cdots\  n^{k_n}k_n!}
\end{equation*}

Now, immediately, if we take the sum of \(C(k_1, k_2, \ldots k_n)\) over all
valid arguments, we would get the total number of permutations, \(n!\).
That is

\begin{equation*}
n!
= \sum_{k_1 + 2k_2 + \cdots nk_n = n} C(k_1, k_2, \ldots k_n)
= \sum_{k_1 + 2k_2 + \cdots nk_n = n} \frac{n!}{1^{k_1}k_1!\ 2^{k_2}k_2!\ \cdots\ n^{k_n}k_n!}
\end{equation*}

\section{Generating function}
\label{develop--math3610:cycle-index:page--structure-symmetric.adoc---generating-function}

We would now attempt to find a generating function for \(C(k_1, k_2, \ldots k_n)\).
Firstly, let

\begin{equation*}
C_n(t_1, t_2, \ldots t_n)
= \sum C(k_1, k_2, \ldots k_n)t_1^{k_1} t_2^{k_2} \cdots t_n^{k_n}
= \sum \frac{n!}{k_1!k_2! \cdots k_n!}\left(\frac{t_1}{1}\right)^{k_1} \left(\frac{t_2}{2}\right)^{k_2} \cdots \left(\frac{t_n}{n}\right)^{k_n}
\end{equation*}

where the summation is over \(n = k_1, + 2k_2, \ldots nk_n\) and \(C_0 = 1\) (sort of by definition).
We call \(C_n\)
the \emph{cycle indicator of \(S_n\)}. This is not the generating function
we are looking for however, notice that it encodes each of the cycle structures of \(S_n\).

Next, we define

\begin{equation*}
\begin{aligned}
A(x)
&= \sum_{n=0}^\infty C_n \frac{x^n}{n!}
\\&= \sum_{n=0}^\infty \sum_{n = k_1, + 2k_2, \ldots nk_n} \frac{n!}{k_1!k_2! \cdots k_n!}\left(\frac{t_1}{1}\right)^{k_1} \left(\frac{t_2}{2}\right)^{k_2} \cdots \left(\frac{t_n}{n}\right)^{k_n} \frac{x^n}{n!}
\\&= \sum_{n=0}^\infty \sum_{n = k_1, + 2k_2, \ldots nk_n} \frac{1}{k_1!k_2! \cdots k_n!}\left(\frac{xt_1}{1}\right)^{k_1} \left(\frac{x^2t_2}{2}\right)^{k_2} \cdots \left(\frac{x^nt_n}{n}\right)^{k_n}
\end{aligned}
\end{equation*}

We want to get rid of that summation over \(n\).
Then notice that for each tuple \((k_1, k_2, \ldots k_n)\), there is exactly
one \(n\) such that \(n = \sum i k_i\). Then, we can collapse both summations into one and instead
enumerate over the set the set of
sequences of natural numbers (including zero), \(\mathcal{B}\).
However, we must only utilize the sequences which have finitely many non-zero terms.
That is,

\begin{equation*}
A(x)
= \sum_{k_1 + k_2 + \cdots < \infty} \prod_{i=1}^\infty \frac{1}{k_i!} \left(\frac{x^it_i}{i}\right)^{k_i}
\end{equation*}

Now, we are doing to do something a bit strange. We are going to remove this restriction
of there being finitely many non-zero terms.
However, in the future, we would only focus on terms with finite powers of \(x\), or finitely many \(t_i\)
or finitely many \(k_i \neq 0\).
Then, we get

\begin{equation*}
\begin{aligned}
A(x)
&= \sum_{\vec{k}\in \mathcal{B}} \prod_{i=1}^\infty \frac{1}{k_i!} \left(\frac{x^it_i}{i}\right)^{k_i}
\\&= \prod_{i=1}^\infty \left[\sum_{k_i = 0}^\infty \frac{1}{k_i!}\left(\frac{x^it_i}{i}\right)^{k_i}\right]
\\&= \prod_{i=1}^\infty \exp\left(\frac{x^it_i}{i}\right)
\\&= \exp\left(\sum_{i=1}^\infty \frac{x^it_i}{i}\right)
\\&= \exp\left(xt_1 + \frac{x^2t_2}{2} + \cdots + \frac{x^nt_n}{n} + \cdots\right)
\end{aligned}
\end{equation*}

Notice that that exchange of product and summation is valid since each sequence of \(k_i\) occurs
exactly once in both formulae. That is, for any sequence \(k_1, k_2, \ldots k_n\ldots \in \mathcal{B}\),
the product

\begin{equation*}
\prod_{i=1}^\infty \frac{1}{k_i!}\left(\frac{x_i^i t_i}{i}\right)^{k_i}
\end{equation*}

occurs once in
the of summation of products (trivially) as well as the product of summations by taking the relevant \(t_i^{k_i}\) term
in each part of the product.

Also, remember that this formula only makes sense for terms with finitely many \(t_i\) or equivalently,
finite powers of \(x\).
\end{document}
