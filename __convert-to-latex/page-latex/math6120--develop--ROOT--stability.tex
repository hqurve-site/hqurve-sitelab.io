\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Stability Analysis}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Often differential equations are used to model real world phenomena.
In these cases, we may not care analytic solutions but rather their nature.
For example, if there is a differential equation modelling the sway of a building,
we may wish to guarantee that the sway eventually dampens.
Or if a differential equation models the position of a particle in the Large Hadron Collider, we may
wish to know that the particles remain close even though their initial positions are all unique.
In these cases we care about the \emph{stability} of the system/solution.
There are three types of stability which are of interest

\begin{description}
\item[Laplace] A solution is stable in the Laplace sense if it is bounded
\item[Lyapunov] A solution is stable in the Lyapunov sense if small perturbations
    in the initial conditions yield solutions which are close to the original.
\item[Poincare] Discusses periodic systems. (TODO: better wording)
\end{description}

Consider the differential equation

\begin{equation*}
x' = f(t,x)
\end{equation*}

with the set of solutions \(S\).
Note that the following results also hold for systems of differential equations.
Also note that stability is defined per solution. We say that the system
is stable if all of its solutions are stable.

We would take Lyapunov stability as the default.
Formally we define the following

\begin{description}
\item[Stability] we say that the solution \(x(t)\) of the differential equation \emph{stable} if
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists \delta = \delta(t_0, \varepsilon) > 0:
    \forall \tilde{x}(t) \in S:
    |x(t_0) - \tilde{x}(t_0)| < \delta \implies
    \forall t \geq t_0: |x(t) - \tilde{x}(t)| < \varepsilon
    \end{equation*}
\item[Uniform stability] We say that \(x(t)\) is \emph{uniformly stable} if
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists \delta = \delta(\varepsilon) > 0:
    \forall t_1 > t_0:
    \forall \tilde{x}(t) \in S:
    |x(t_1) - \tilde{x}(t_1)| < \delta \implies
    \forall t \geq t_1: |x(t) - \tilde{x}(t)| < \varepsilon
    \end{equation*}
\item[Asymptotic stability] We say that solution \(x(t)\) is \emph{asymptotically stable} if
    it is stable and
    
    \begin{equation*}
    \exists \delta >0: \forall \tilde{x}(t) \in S:
    |x(t_0) - \tilde{x}(t_0)| < \delta \implies \lim_{t\to\infty} |x(t) - \tilde{x}(t)| = 0
    \end{equation*}
\item[Uniformly asymptotic stability] We say that \(x(t)\) is \emph{uniformly asymptotically stable} if
    it is uniformly stable and
    
    \begin{equation*}
    \begin{aligned}
    &
    \exists \delta> 0: \forall \eta > 0: \exists T=T(\eta)>0:
    \\&\quad
    \forall t_1 > t_0:
    \forall \tilde{x}(t) \in S:
    |x(t_1) - \tilde{x}(t_1)| < \delta \implies \forall t\geq t_1 + T: |x(t) - \tilde{x}(t)| < \eta
    \end{aligned}
    \end{equation*}
\end{description}

It is immediately apparent that if \(f\) is periodic in \(t\) or autonomous (independent of \(t\)),
all (asymptotically) stable solutions are also uniformly (asymptotically) stable.

\begin{example}[{}]
Every solution of \(x'=0\) is uniformly stable but not asymptotically stable since the solutions are constant.
\end{example}

\begin{example}[{}]
Every solution of \(x'=t\) is of the form \(x(t, t_0, x_0) = \frac{t^2}{2} - \frac{t_0^2}{2} + x_0\).
Therefore it is clearly uniformly stable since the difference between two solutions is simply
the difference between the initial points.
\end{example}

\begin{example}[{}]
Consider the differential equation \(x' = p(t)x(t)\) which has solution

\begin{equation*}
x(t, t_0, x_0) = x_0 \exp\int_{t_0}^t p(s) \ ds
\end{equation*}

Consider some solutions \(x(t)\).
Then

\begin{itemize}
\item \(x(t)\) is stable iff \(\exp \int_{t_0}^t p(s) \ ds\) is bounded which happens
    iff \(\int_{t_0}^t p(s) \ ds\) is bounded above.
\item \(x(t)\) is uniformly stable iff
    \(\int_{t_1}^t p(s) \ ds\) is bounded above for each \(t \geq t_1 \geq t_0\).
\item \(x(t)\) is asymptotically stable iff \(\int_{t_0}^{\infty} p(s) \ ds = -\infty\)
\end{itemize}

Consider the case where \(p(t) = \sin(\ln t) + \cos(\ln t)-1.25\). Then,

\begin{equation*}
\int_{1}^t p(s) = \left. s\sin(\ln s) - 1.25s\right|_{1}^t = t\sin (\ln t) - 1.25(t-1)
\end{equation*}

Therefore the system is asymptotically stable and stable.
But it is not uniformly stable since
if \(t= e^{\left(2n +\frac12\right)\pi}\) and \(t_1 = e^{2n\pi}\)

\begin{equation*}
\int_{t_1}^t p(s) \ ds
=
e^{\left(2n+\frac12\right)\pi}\sin\left(\left(2n+\frac12\right)\pi\right)
- e^{\left(2n+\frac12\right)\pi}\sin\left(2n\pi\right)
+ \frac{\pi}{2}
=
e^{\left(2n+\frac12\right)\pi}
+ \frac{\pi}{2}
\to \infty
\end{equation*}
\end{example}

\section{Fixed points}
\label{develop--math6120:ROOT:page--stability.adoc---fixed-points}

We say that \(x_e\) is a \emph{fixed point} of the system \(x' = f(t, x)\) if
\(f(t,x_e) = 0\). In such a case we see that the constant solution \(x(t) = x_e\)
is a solution to the differential equation.
We are interested in the behaviour of fixed point solutions.

For convince we may often assume that \(t_0=0\). We may also apply the transformation
\(x= y+x_e\) to obtain that

\begin{equation*}
y'(t) = x'(t) - x_e' = f(t,x(t)) = f(t,y(t) + x_e) = g(t, y(t))
\end{equation*}

This differential equation has the same dynamics as the original and has fixed point
of \(0\).
Therefore we can assume that \(x_e = 0\) by applying the necessary transformation.

Let us consider the different definitions of stability when applied
to our fixed point solution at \(0\).

\begin{description}
\item[Stability] 
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists \delta = \delta(t_0, \varepsilon) > 0:
    \forall \tilde{x}(t) \in S:
    |\tilde{x}(t_0)| < \delta \implies
    \forall t \geq t_0: |\tilde{x}(t)| < \varepsilon
    \end{equation*}
\item[Uniform stability] 
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists \delta = \delta(\varepsilon) > 0:
    \forall t_1 > t_0:
    \forall \tilde{x}(t) \in S:
    |\tilde{x}(t_1)| < \delta \implies
    \forall t \geq t_1: |\tilde{x}(t)| < \varepsilon
    \end{equation*}
\item[Asymptotic stability] stable and
    
    \begin{equation*}
    \exists \delta >0: \forall \tilde{x}(t) \in S:
    |\tilde{x}(t_0)| < \delta \implies \lim_{t\to\infty} |\tilde{x}(t)| = 0
    \end{equation*}
\item[Uniformly Asymptotic stability] uniformly stable and
    
    \begin{equation*}
    \exists \delta> 0: \forall \eta > 0: \exists T=T(\eta)>0:
    \forall t_1 > t_0:
    \forall \tilde{x}(t) \in S:
    |\tilde{x}(t_1)| < \delta \implies \forall t\geq t_1 + T: |\tilde{x}(t)| < \eta
    \end{equation*}
\end{description}

In all cases, stability requires that \(\tilde{x}(t)\) be ``near'' the origin \(x_e=0\).

\section{Linear Systems}
\label{develop--math6120:ROOT:page--stability.adoc---linear-systems}

Consider the linear system

\begin{equation*}
X ' =A(t)X
\end{equation*}

\begin{theorem}[{}]
Let \(\Phi(t)\) be a fundamental matrix of the system then,
the system is

\begin{enumerate}[label=\arabic*)]
\item stable iff \(\Phi(t)\) is bounded on \([t_0, \infty)\)
\item uniformly stable iff there exists \(M > 0\) such that
    
    \begin{equation*}
    \forall t \geq s \geq t_0: \left\|\Phi(t)\Phi^{-1}(s)\right\| \leq M
    \end{equation*}
\item asymptotically stable iff \(\lim_{t\to\infty} \Phi(t) = 0\)
\item uniformly asymptotically stable if there exists \(M, \alpha > 0\) such that
    
    \begin{equation*}
    \forall t \geq s \geq t_0: \left\|\Phi(t)\Phi^{-1}(s)\right\| \leq Me^{-\alpha (t-s)}
    \end{equation*}
\end{enumerate}
\end{theorem}

\begin{admonition-note}[{}]
For each of the proofs recall that the solutions of the system with initial conditions
\(X(t_1) = X_1\) is \(\Phi(t)\Phi^{-1}(t_1)X_1\).
\end{admonition-note}

\begin{proof}[{1}]
WLOG suppose that \(\Phi(t_0) = E\).

First suppose that all the solutions are stable. That is

\begin{equation*}
\forall X_0: \forall \varepsilon > 0: \exists \delta > 0:
\forall \tilde{X}_0:  \|X_0 - \tilde{X}_0\| < \delta \implies
\forall t \geq t_0: \|\Phi(t)X_0 - \Phi(t)\tilde{X}_0\| < \varepsilon
\end{equation*}

In particular notice that the zero-solution is stable.
Let \(\delta > 0\) (by letting \(\varepsilon =1\)) such that the ``condtion'' is satisfied.
Then, \(\tilde{X}_0 = \frac{\delta}{2}E_i\) satisfies the conditions
\(\|0 - \tilde{X}_0\| < \delta\) and hence

\begin{equation*}
\|\Phi(t)0 - \Phi(t)\tilde{X}_0\| = \frac{\delta}{2}\left\|\Phi_i(t)\right\| < 1
\implies \|\Phi_i(t)\| < \frac{2}{\delta}
\end{equation*}

Therefore for any \(X_0\), we have that
\(\|\Phi(t)X_0\| \leq \frac{2n}{\delta}\|X_0\|\) and the solution is bounded.

Conversely, suppose all solutions are bounded
and let \(M>0\) be such that \(\|\Phi(t)\| \leq M\).
Then,

\begin{equation*}
\forall X_0: \forall \varepsilon > 0:
\exists \delta = \frac{\varepsilon}{M}:
\forall \tilde{X}_0:
\|X_0 - \tilde{X}_0\| < \delta \implies
\forall t > t_0:
\|\Phi(t)X_0 - \Phi(t)\tilde{X}_0\| < M\delta \leq \varepsilon
\end{equation*}

and hence all solutions are stable.
\end{proof}

\begin{proof}[{2}]
First suppose that the system is uniformly stable.
Then, the zero solution is stable and hence

\begin{equation*}
\exists \delta > 0: \forall t_1 \geq t_0:
\forall \tilde{X}_1:
\|\tilde{X}_1\| < \delta
\implies \forall t \geq t_1:
\left\|\Phi(t)\Phi^{-1}(t_1)\tilde{X}_1\right\| < 1
\end{equation*}

By taking \(\tilde{X}_1 = \frac{\delta}{2} E_i\), we get get the desired result.

Conversely, suppose that the given inequality holds.
Then, for each \(X_0\), we have that

\begin{equation*}
\begin{aligned}
&\forall \varepsilon > 0:
\exists \delta = \frac{\varepsilon}{M} > 0: \forall t_1 \geq 0: \forall \tilde{X}_1:
\|\Phi(t_1)\Phi^{-1}(t_0)X_0 -\tilde{X}_1\| < \delta
\\&\implies
\forall t \geq t_1:
\left\|\Phi(t)\Phi^{-1}(t_0)X_0 - \Phi(t)\Phi^{-1}(t_1)\tilde{X}_1\right\|
\leq \|\Phi(t)\Phi^{-1}(t_1)\| \|\Phi(t_1)\Phi^{-1}(t_0)X_0 -\tilde{X}_1\|
< M \delta < \varepsilon
\end{aligned}
\end{equation*}
\end{proof}

\begin{proof}[{3}]
Suppose that the system is asymptotically stable.
Then, the zero solution
is also asymptotically stable and hence
there exists \(\delta > 0\) such that

\begin{equation*}
\|\tilde{X}_0\| < \delta \implies
\lim_{t\to\infty} \|\Phi(t)X_0\| = 0
\end{equation*}

Then, for each \(X_0 = \frac{\delta}{2} E_i\), we obtain that each of the columns
of \(\Phi(t)\) tend to 0 and hence \(\Phi(t)\) tends to 0.

Conversely, if \(\Phi(t)\) tends to zero, it is clear that the system
is stable since \(\Phi(t)\) is bounded and asymptotically stable since each solution
is of the form \(\Phi(t)C\).
\end{proof}

\begin{proof}[{4}]
First suppose that there exists \(M, \alpha > 0\) such that

\begin{equation*}
\forall t \geq s \geq t_0: \left\|\Phi(t)\Phi^{-1}(s)\right\| \leq Me^{-\alpha(t-s)}
\end{equation*}

Then clearly every solution is uniformly stable since \(\|\Phi(t)\Phi^{-1}(s)\| \leq M\).
Now, consider solution \(\Phi(t)\Phi^{-1}(t_0)X_0\)
and let \(\delta = \frac{1}{M}\).
Consider arbitrary
Then for any \(0 < \eta <1\), let \(T = \frac{1}{\alpha}\ln\frac{1}{\eta} > 0\).
Then

\begin{equation*}
\begin{aligned}
&\forall \tilde{X}_1: \forall t_1 \geq t_0:
\left\|\Phi(t_1)\Phi^{-1}(t_0)X_0 - \tilde{X}_1\right\| < \delta
\\&\implies \forall t \geq t_1 + T:
\begin{aligned}[t]
\left\|\Phi(t)\Phi^{-1}(t_0)X_0 - \Phi(t)\Phi^{-1}(t_1)\tilde{X}_1\right\|
&\leq \left\|\Phi(t)\Phi^{-1}(t_1)\right\|\left\|\Phi(t_1)\Phi^{-1}(t_0)X_0 - \tilde{X}_1\right\|
\\&< Me^{-\alpha(t-t_1)}\delta
\\&\leq e^{-\alpha T} = \eta
\end{aligned}
\end{aligned}
\end{equation*}

Therefore every solution is uniformly asymptotically stable.

Conversely suppose that every solution is uniformly asymptotically stable.
Focus on the zero solution.
Then, there exists \(\delta > 0\) and \(T > 0\) (by letting \(\eta =\frac{\delta}{4n}\))
such that

\begin{equation*}
\forall \tilde{X}_1: \forall t_1 \geq t_0: \|\tilde{X}_1\| < \delta
\implies \forall t \geq t_1 + T: \|\Phi(t)\Phi^{-1}(t_1)\tilde{X}_1\| < \frac{\delta}{4n}
\end{equation*}

Let \(\Psi(t, s) = \Phi(t)\Phi^{-1}(s)\) and consider
consider \(\tilde{X}_{1i} = \frac{\delta}{2}E_i\)
for \(i=1,\ldots n\).
Then,

\begin{equation*}
\forall t_1 \geq t_0:
\forall t \geq t_1 + T: \|\Phi(t)\Phi^{-1}(t_1)\tilde{X}_{1i}\|
= \frac{\delta}{2}\|\Psi_i(t, t_1)\| < \frac{\delta}{4n}
\end{equation*}

and hence

\begin{equation*}
\forall t_1 \geq t_0:
\forall t \geq t_1 + T:
= \|\Psi_i(t, t_1)\| < \frac{1}{2n}
\end{equation*}

and hence \(\|\Phi(t)\Phi^{-1}(s)\| \leq \frac{1}{2}\) for all \(t \geq s+T\) and \(s \geq t_0\).

Now, consider arbitrary \(t \geq s \geq t_0\). Let \(k = \left\lfloor \frac{t-s}{T} \right\rfloor\).
So,

\begin{equation*}
k \leq \frac{t-s}{T}
\quad\text{and}\quad k > \frac{t-s}{T} - 1
\end{equation*}

Then

\begin{equation*}
\begin{aligned}
\|\Phi(t)\Phi^{-1}(s)\|
&= \|\Phi(t)\Phi^{-1}(t-T) \Phi(t-T)\Phi^{-1}(t-2T) \cdots \Phi(t-kT)\Phi^{-1}(s)\|
\\&= \left\|\left(\prod_{i=0}^{k-1} \Phi(t-iT)\Phi^{-1}(t-(i+1)T)\right)
\Phi(t-kT)\Phi^{-1}(s)\right\|
\\&\leq \|\Phi(t-kT)\Phi^{-1}(s)\| \prod_{i=0}^{k-1} \|\Phi(t-iT)\Phi^{-1}(t-(i+1)T)\|
\\&\leq M \frac{1}{2^k}
\quad\text{since } t-kT \geq s
\text{ and } t-iT \geq t-(i+1)T + T \text{ and } t-(i+1)T \geq t-kT \geq s
\\&= M \exp\left(-\ln(2)k\right)
\\&\leq M \exp\left(-\ln(2) \left(\frac{t-s}{T} - 1\right)\right)
\\&= 2M \exp\left(-\frac{\ln 2}{T}(t-s)\right)
\end{aligned}
\end{equation*}

Since the choice of \(t\geq s \geq t_0\) was arbitrary and \(2M\) and \(\frac{\ln 2}{T}\) do not depent
on \(t\) and \(s\), we have the desired result with \(\alpha = -\frac{\ln 2}{T}\).
That is

\begin{equation*}
\forall t \geq s \geq t_0: \|\Phi(t)\Phi^{-1}(s)\| \leq 2M \exp\left(-\frac{\ln 2}{T}(t-s)\right)
\end{equation*}
\end{proof}

\begin{corollary}[{}]
All the characteristic roots of \(A\) have negative real parts,
if and only if every solution of \(X'=AX\) is asymptotically stable.

\begin{proof}[{}]
Recall that if all characteristic roots of \(A\) have negative real parts,
there exists \(\alpha , M > 0\) such that

\begin{equation*}
\|\Phi(t)\| \leq Me^{-\alpha(t-t_0)}
\end{equation*}

hence \(\Phi(t) \to 0\) as \(t\to \infty\) and the system is asymptotically stable.

Conversely, suppose all solutions are asymptotically stable.
Then, \(\lim_{t\to \infty} \Phi(t) = 0\).
Let \((\lambda, V)\) be any eigenpair of \(A\).
Then \(X(t) = Ve^{\lambda (t-t_0)}\) is a solution to the system
and \(Ve^{\lambda (t-t_0)} = \Phi(t)C\) for some \(C\).
Therefore \(\lim_{t\to\infty} Ve^{\lambda (t-t_0)} = 0\).
Since \(V\neq 0\), we have that \(\lambda < 0\).
\end{proof}
\end{corollary}

Now consider the perturbed system

\begin{equation*}
X' = A(t)X + B(t)
\end{equation*}

Then, the stability and type of stability of this system is only dependent on the stability
of \(X' = A(t)X\). This follows from the fact that the difference of two solutions
of the above perturbed system is a solution to the homogeneous system.

Finally consider the system

\begin{equation*}
X' = [A(t) + B(t)]X(t)
\end{equation*}

Often it may be difficult to determine the fundamental solution associated with \(A(t)+B(t)\),
while the fundamental solution of \(A(t)\) may be simple to find.

\begin{theorem}[{}]
Suppose that

\begin{equation*}
\int_{t_0}^\infty B(t) \ dt < \infty
\end{equation*}

If \(X'=A(t)X\) is uniformly stable, it follows that \(X' = [A(t)+B(t)]X \)
is also uniformly stable.

\begin{proof}[{}]
Let \(\Phi(t)\) be the fundamental matrix associated with \(X'=A(t)X\).
From variation of parameters, we obtain that

\begin{equation*}
X(t) = \Phi(t)\Phi^{-1}(t_0)X_0 + \int_{t_0}^t \Phi(t)\Phi^{-1}(s)B(s)X(s) \ ds
\end{equation*}

Now, since \(X'=A(t)X\) is uniformly stable, there exists \(M > 0\)
such that \(\|\Phi(t)\Phi^{-1}(s)\| < M\) for \(t \geq s \geq 0\).
Also, let \(N > 0\) such that \(\int_{t_0}^\infty \|B(s)\| \leq N\).

Consider arbitrary \(\varepsilon > 0\) and let \(\delta = \frac{\varepsilon}{2Me^{MN}} > 0\).
Suppose \(X(t)\) and \(\tilde{X}(t)\) are two solutions
with \(\|X(t_1) - \tilde{X}(t_1)\| < \delta\).
We need to show that for \(t \geq t_1\), \(\|X(t) - \tilde{X}(t)\| < \varepsilon\).
Note that for \(t \geq t_1\)

\begin{equation*}
\begin{aligned}
\|X(t)-\tilde{X}(t)\|
&= \left\|
\begin{aligned}
&
\Phi(t)\Phi^{-1}(t_0)X_0 + \int_{t_0}^t \Phi(t)\Phi^{-1}(s)B(s)X(s) \ ds
\\&\hspace{2em}
- \Phi(t)\Phi^{-1}(t_0)\tilde{X}_0 - \int_{t_0}^t \Phi(t)\Phi^{-1}(s)B(s)\tilde{X}(s) \ ds
\end{aligned}
\right\|
\\&\leq \left\|
\begin{aligned}
&
\Phi(t)\Phi^{-1}(t_0)X_0 + \int_{t_0}^{t_1} \Phi(t)\Phi^{-1}(s)B(s)X(s) \ ds
\\&\hspace{2em}
- \Phi(t)\Phi^{-1}(t_0)\tilde{X}_0 - \int_{t_0}^{t_1} \Phi(t)\Phi^{-1}(s)B(s)\tilde{X}(s) \ ds
\end{aligned}
\right\|
\\&\quad\quad
+ \left\|\int_{t_1}^t \Phi(t)\Phi^{-1}(s)B(s)X(s) \ ds - \int_{t_1}^t \Phi(t)\Phi^{-1}(s)B(s)\tilde{X}(s) \ ds\right\|
\\&\quad\text{by splitting the integral into }[t_0, t_1] \text{ and } [t_1, t]
\\&\leq \|\Phi(t)\Phi^{-1}(t_1)\|\left\|
\begin{aligned}
&
\Phi(t_1)\Phi^{-1}(t_0)X_0 + \int_{t_0}^{t_1} \Phi(t_1)\Phi^{-1}(s)B(s)X(s) \ ds
\\&\hspace{2em}
- \Phi(t_1)\Phi^{-1}(t_0)\tilde{X}_0 - \int_{t_0}^{t_1} \Phi(t_1)\Phi^{-1}(s)B(s)\tilde{X}(s) \ ds
\end{aligned}
\right\|
\\&\quad\quad
+ \int_{t_1}^t \left\|\Phi(t)\Phi^{-1}(s)\right\|\left\|B(s)\right\| \|X(s) - \tilde{X}(s)\| \ ds
\\&\quad\text{by writing }\Phi(t) = \Phi(t)\Phi^{-1}(t_1)\Phi(t_1) \text{ and applying integral norm inequality}
\\&= \|\Phi(t)\Phi^{-1}(t_1)\|\left\|X(t_1)-\tilde{X}(t_1)\right\|
+ \int_{t_1}^t \left\|\Phi(t)\Phi^{-1}(s)\right\|\left\|B(s)\right\| \|X(s) - \tilde{X}(s)\| \ ds
\\&< M\delta
+ \int_{t_1}^t M\|B(s)\| \|X(s) - \tilde{X}(s)\| \ ds
\end{aligned}
\end{equation*}

and by Gronwall's inequality

\begin{equation*}
\|X(t) - \tilde{X}(t)\| \leq \delta M \exp \left(\int_{t_1}^t M\|B(s)\| \ ds \right)\leq \delta M e^{MN} = \frac{\varepsilon}{2} < \varepsilon
\end{equation*}

Therefore, the perturbed system is uniformly stable since

\begin{equation*}
\forall \varepsilon>0: \exists \delta = \frac{\varepsilon}{2Me^{MN}} > 0: \forall t_1 \geq t_0: \|X(t_1) - \tilde{X}(t_1)\| < \delta
\implies \forall t \geq t_1: \|X(t) - \tilde{X}(t)\| < \varepsilon
\end{equation*}
\end{proof}
\end{theorem}
\end{document}
