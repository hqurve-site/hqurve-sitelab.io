\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Factor Analysis}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Suppose we have conduct a survey using a questionnaire.
Factor analysis allows us to answer the following questions

\begin{enumerate}[label=\arabic*)]
\item How many \textbf{factors} or underlying quantities does the questionnaire measure?
\item Which questions measure which factors?
\end{enumerate}

\begin{example}[{Psychometric Survey}]
A psychometric survey for prospective employees
may may want to measure multiple factors, such as honesty,
creativity and confidence.
Instead of using a single question to measure each of these factors,
the questionnaire will use multiple, possibly overlapping questions per factor.
Factor analysis will allow the employers to determine how well the
questionnaire was designed and allow them to analyze
how well each of the questions measure the underlying variables.
\end{example}

\begin{example}[{Data reanalysis}]
A shipping company needs to know when their ships need maintenance.
To do this they use a large set of quantitative and qualitative measurements
which are obtained from onboard diagnostic systems and crew reports.
For example, the average engine temperature may be used as an indicator
of when the engine needs servicing.
However, environmental temperatures many significantly distort
the inference based on engine temperature alone.
Factor analysis will allow us to determine what other measurements may be used
as an auxiliary to engine temperature and the significance of each of these
measurements.
\end{example}

Note that in the above examples, factor analysis does not assign
names to the factors.
The names and meanings of each of the factors must be reverse engineered
based on the data and the prior information.

The typical steps of factor analysis are as follows

\begin{enumerate}[label=\arabic*)]
\item Collect data
\item Extract the initial factors (perhaps via PCA)
\item Choose the number of factors to retain
\item Choose an estimation method
\item Rotate and interpret factors
\item Decide if changes need to be made to the model (eg dropping/including items).
    If any changes are made, repeat steps 4 and 5
\item Construct scales/scores and use them in further analysis (eg as independent variables in a regression
    model).
\end{enumerate}

There are also two types of factor analysis

\begin{description}
\item[Exploratory] This is done when we do not know the factor structure.
\item[Confirmatory] This is done when we want to test an expected factor structure.
\end{description}

\section{The Orthogonal Factor Model}
\label{develop--stats:ROOT:page--analysis/factor-analysis.adoc---the-orthogonal-factor-model}

Suppose we have observable random vector \(\vec{X} \in \mathbb{R}^p\)
and underlying common factors \(\vec{F}\in \mathbb{R}^m\)
and specific factors \(\vec{\varepsilon} \in \mathbb{R}^p\).
Then the \textbf{orthogonal factor model} is

\begin{equation*}
\begin{array}{ccccc cccccc}
X_1 -\mu_1 &=& l_{11}F_1 &+& l_{12}F_2 &+& \cdots &+& l_{1m}F_{m} &+& \varepsilon_1\\
X_2 -\mu_2 &=& l_{21}F_1 &+& l_{22}F_2 &+& \cdots &+& l_{2m}F_{m} &+& \varepsilon_2\\
&\vdots &\\
X_p -\mu_p &=& l_{p1}F_1 &+& l_{p2}F_2 &+& \cdots &+& l_{pm}F_{m} &+& \varepsilon_p\\
\end{array}
\end{equation*}

or equivalently

\begin{equation*}
\vec{X} - \vec{\mu} = \mat{L}\vec{F} + \vec{\varepsilon}
\end{equation*}

where
\(\mat{L}\) is is the matrix of \textbf{factor loadings} (constant)
and \(\vec{\mu}\) is the mean vector (also constant)
and the following conditions are satisfied

\begin{itemize}
\item \(\vec{F}\) and \(\vec{\varepsilon}\) are independent
\item \(E[\vec{F}]=\vec{0}\) and \(Cov[\vec{F}] = \mat{I}\)
\item \(E[\vec{\varepsilon}] = \vec{0}\) and \(Cov[\vec{\varepsilon}]=\mat{\Psi}\)
    where \(\mat{\Psi}\) is a diagonal matrix.
\end{itemize}

\begin{admonition-note}[{}]
If \(Cov[\vec{F}]\) is not a diagonal matrix,
the model is instead a \textbf{oblique factor model}
\end{admonition-note}

Note that in this model, the common and specific factors are both
unobservable.

\subsection{Covariance structure}
\label{develop--stats:ROOT:page--analysis/factor-analysis.adoc---covariance-structure}

Factor analysis aims to recover the common factors by estimating the loadings.
In order to estimate the loadings, note the covariance structure
of the observable data \(\vec{X}\).

\begin{proposition}[{}]
Let \(\vec{X}\) be a random vector which follows the orthogonal factor
model. Then

\begin{enumerate}[label=\arabic*)]
\item \(Cov[\vec{X}] = \mat{\Sigma} = \mat{L}\mat{L}^T + \mat{\Psi}\).
    That is
    
    \begin{equation*}
    \begin{aligned}
    Var[X_i] &= l_{i1}^2 + \cdots l_{im}^2 + \psi_i
    \\
    Cov[X_i, X_k] &= l_{i1}l_{k1} + \cdots l_{im}l_{km}
    \end{aligned}
    \end{equation*}
\item \(Cov[\vec{X}, \vec{F}] = \mat{L}\).
    That is
    
    \begin{equation*}
    Cov[X_i, F_j] = l_{ij}
    \end{equation*}
\end{enumerate}
\end{proposition}

Therefore, we can use the sample covariance matrix in order to estimate
the parameters of the factor model.

Note that the covariance structure of the model can be simplified as

\begin{equation*}
Var[X_i] = h_i^2 + \psi_i
\end{equation*}

where \(h_i^2 = l_{i1}^2 + \cdots l_{im}^2\) is the \textbf{communality} and
measures the amount of variance of \(X_i\) explained by the common factors.
Sometimes the \(\psi_i\) is referred to as the \textbf{uniqueness}.

\begin{itemize}
\item If the \(X_i\) is \textbf{informative} of the factors, the communality is high.
\item If the \(X_j\) is \textbf{not informative} of the factors, the uniqueness is high.
\end{itemize}

\subsection{Model fitting}
\label{develop--stats:ROOT:page--analysis/factor-analysis.adoc---model-fitting}

The following are common methods which are used
to determine the parameters of the model:

\begin{itemize}
\item principal component analysis
\item principal factor/axis
\item maximum likelihood estimation
\end{itemize}

A benefit of principal component analysis is that it also
informs us how many factors the model should use.

\subsection{Factor Rotation}
\label{develop--stats:ROOT:page--analysis/factor-analysis.adoc---factor-rotation}

Note that if \(\mat{L}\) is a loading matrix and
\(\mat{T}\) is an orthogonal matrix (\(\mat{T}\mat{T}^T = \mat{I}\)),
then both \(\mat{L}\) and \(\mat{L}^*=\mat{T}\mat{L}\)
yield the same covariance structure of \(\vec{X}\).
Therefore the choice of \(\mat{L}\) is not unique.

Since the choice of \(\mat{L}\) is not unique and does not affect the fit
of the model, we perform \textbf{factor rotation} to increase
the \textbf{ease of interpretation}.
Common criteria are

\begin{description}
\item[Varimax] Most often the \textbf{varimax criterion} is used for factor rotation.
    This criterion maximizes the variables of the squared loadings
    for each factor by making some of then as large as possible and the rest
    as small (close to zero) as possible).
\item[Quartimax] The \textbf{quartimax criterion} maximizes
    the variance of the squared loadings on each variable.
    It tends to produce factors with high loadings for all variables.
\end{description}

\section{Implementing Factor Analysis}
\label{develop--stats:ROOT:page--analysis/factor-analysis.adoc---implementing-factor-analysis}

\begin{listing}[{}]
# data with observations as rows and variables as columns
data <- ..

# perform factor analysis
factanal(data, factors=3, rotation = "varimax")
\end{listing}

\begin{admonition-tip}[{}]
See \url{https://www.geo.fu-berlin.de/en/v/soga-r/Advances-statistics/Multivariate-approaches/Factor-Analysis/A-Simple-Example-of-Factor-Analysis-in-R/index.html}
\end{admonition-tip}
\end{document}
