\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Block Designs}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Block designs arise due to the need for efficient product testing.
Suppose there are several varieties of a certain product and different
facilities to test them at. In order to make the tests as fair as
possible, it would be best if

\begin{itemize}
\item each facility tested the same number of varieties
\item each pair of varieties are tested by the same number of facilities
\end{itemize}

Although, one way to do this is for each facility to test all of the
varieties, it would be very wasteful and time consuming. The problem now
is to find more economical ways of testing the product.

\section{Definition}
\label{develop--math2276:ROOT:page--block-designs.adoc---definition}

A \emph{block design} is a family of \(b\) subsets of a set
\(S\), of \(\nu\) elements, such that for some fixed
\(k\) and \(\lambda\)

\begin{enumerate}[label=\arabic*)]
\item each subset has \(k\) elements
\item each pair of elements in \(S\) occur together in exactly
    \(\lambda\) subsets.
\end{enumerate}

The elements of \(S\) are called \emph{varieties} and the subsets
of \(S\) are called blocks.

\subsection{Parameter definitions}
\label{develop--math2276:ROOT:page--block-designs.adoc---parameter-definitions}

\begin{itemize}
\item \(\nu\) - number of varieties/elements
\item \(b\) - number of blocks
\item \(r\) - number of blocks in which each variety appears
\item \(k\) - number of elements per block
\item \(\lambda\) - number of subsets in which each pair occurs
\end{itemize}

\section{Theorem on the parameters of the a block design}
\label{develop--math2276:ROOT:page--block-designs.adoc---theorem-on-the-parameters-of-the-a-block-design}

Prior, it was taken for granted that each variety lies in exactly
\(r\) blocks. This theorem both proves this as well as the
following two restrictions on the parameters of a block design.

\begin{equation*}
r(k-1) = \lambda(\nu - 1) \quad\text{and}\quad bk = \nu r
\end{equation*}

\begin{example}[{Proof}]
Consider any one variety and suppose that it occurs in
\(r\) blocks, for some \(r\). Then each of the
\(r\) blocks contain \(k-1\) other varieties which
our chosen variety makes pairs with. From this, the total number of
pairs including our chosen variety is \(r(k-1)\). On the other
hand, since each pair occurs \(\lambda\) times and there are
\(\nu-1\) distinct pairs including our chosen variety, the
total number of pairs including our chosen variety is
\(\lambda(\nu -1)\). Therefore

\begin{equation*}
r(k-1) = \lambda(\nu - 1)
\end{equation*}

Furthermore, since \(k\), \(\lambda\) and
\(\nu\) are constants, \(r\) is also a constant and
must be equal for varieties.

For the second restriction, consider the total number of tests. Since
each variety occurs in \(r\) blocks, we get
\(\nu r\) tests. On the other hand, since each block contains
\(k\) varieties, there are \(bk\) tests. Therefore

\begin{equation*}
bk = \nu r
\end{equation*}

 ◻
\end{example}

\subsection{Lemmas}
\label{develop--math2276:ROOT:page--block-designs.adoc---lemmas}

\begin{itemize}
\item \(r = \lambda \implies k = \nu \implies b = r\). This
    implies that all elements are in each block and hence
    \(b = \nu = r = k = \lambda\)
\item \(r \geq \lambda\). BWOC, suppose \(r < \lambda\).
    This implies that \(\nu < k\) which implies that
    \(b < r\). This is clearly impossible.
\end{itemize}

\section{Classification}
\label{develop--math2276:ROOT:page--block-designs.adoc---classification}

Often a block designs are referred to as a
\((b, \nu, r, k, \lambda)\)-configuration. Although any block
design’s parameters must satisfy the above theorem, the converse is not
necessarily true. That is, there is no guarantee that a particular
configuration exists, for any five parameters satisfying the above
theorem.

\subsection{Square/symmetric designs}
\label{develop--math2276:ROOT:page--block-designs.adoc---squaresymmetric-designs}

A block design is called \emph{square} or \emph{symmetric} if the number of
blocks, \(b\), is equal to the number of varieties,
\(\nu\). Such designs have no implication that the incidence
matrix is symmetric. Furthermore, since \(b = \nu\) implies
that \(r = k\), square designs are often called
\((\nu, k, \lambda)\) configurations.

\section{Representations}
\label{develop--math2276:ROOT:page--block-designs.adoc---representations}

\subsection{Geometrical}
\label{develop--math2276:ROOT:page--block-designs.adoc---geometrical}

A block design may be represented by a set of points and lines (not
necessarily straight) where

\begin{itemize}
\item each point represents a variety
\item each line represents a block
\end{itemize}

The set of points which a particular line passes through indicates the
elements of that particular block.

\subsection{Matrix}
\label{develop--math2276:ROOT:page--block-designs.adoc---matrix}

A simpler way to represent a block design is using an \emph{incidence matrix}
which is a \(b \times \nu\) matrix where each element is
defined as

\begin{equation*}
a_{ij} = \begin{cases}
    1 \quad &\text{if the $i$'th block contains the $j$'th variety}\\
    0 \quad &\text{otherwise}
\end{cases}
\end{equation*}

By representing a block design as a matrix, there is no need to have to
worry about the clutter of labeling each of the varieties and blocks.
Furthermore, it allows for easy inspection of the design as

\begin{itemize}
\item each column represents a variety
\item each row represents a block
\end{itemize}

\section{Fisher’s Inequality}
\label{develop--math2276:ROOT:page--block-designs.adoc---fishers-inequality}

For a \((b, \nu, r, k, \lambda)\)-configuration

\begin{equation*}
b \geq \nu
\end{equation*}

\begin{example}[{Proof}]
Let \(A\) be the incidence matrix and let
\(C = A^T A\). Then \(C\) is a
\(\nu \times \nu\) matrix such that

\begin{equation*}
c_{ij} = \sum_{h = 1}^\nu a'_{i h} a_{h j} = \sum_{h=1}^\nu a_{h i} a_{h j}
\end{equation*}

Notice that this is simply the count of how many blocks the varieties
\(i\) and \(j\) both appear in. In the case when
\(i = j\) (same variety) \(c_{ij} = r\) however when
\(i \neq j\), \(c_{ij} = \lambda\). Therefore,

\begin{equation*}
C = A^T A
    = \begin{bmatrix}
        r & \lambda & \lambda & \cdots & \lambda\\
        \lambda & r & \lambda & \cdots & \lambda\\
        \lambda & \lambda & r &        & \lambda\\
        \vdots  &  \vdots  &  & \ddots & \vdots \\
        \lambda & \lambda & \lambda &\cdots  & r
    \end{bmatrix}
    = (r- \lambda) I + \lambda J
\end{equation*}

where \(I\) is the identity matrix and \(J\) is a
matrix consisting of \(1\)s. Now consider the determinant of
\(C\)

\begin{equation*}
\begin{aligned}
    \begin{vmatrix}
        r & \lambda & \lambda & \cdots & \lambda\\
        \lambda & r & \lambda & \cdots & \lambda\\
        \lambda & \lambda & r &        & \lambda\\
        \vdots  &  \vdots  &  & \ddots & \vdots \\
        \lambda & \lambda & \lambda &\cdots  & r
    \end{vmatrix}
    &=
    \begin{vmatrix}
        r           & \lambda    & \lambda    & \cdots & \lambda\\
        \lambda - r & r -\lambda & 0          & \cdots & 0      \\
        \lambda - r & 0          & r -\lambda &        & 0      \\
        \vdots      &  \vdots    &            & \ddots & \vdots \\
        \lambda - r & 0          & 0          & \cdots & r - \lambda
    \end{vmatrix}\\
    &=
    \begin{vmatrix}
        r + \lambda(\nu - 1) & \lambda    & \lambda    & \cdots & \lambda\\
        0                    & r -\lambda & 0          & \cdots & 0      \\
        0                    & 0          & r -\lambda &        & 0      \\
        0                    &  \vdots    &            & \ddots & \vdots \\
        0                    & 0          & 0          & \cdots & r - \lambda
    \end{vmatrix}\\
    &= \left(r + \lambda(\nu - 1)\right) (r-\lambda)^{\nu - 1}\\
    &= \left(r + r(k-1)\right) (r-\lambda)^{\nu - 1}\\
    &= rk (r-\lambda)^{\nu - 1}\end{aligned}
\end{equation*}

Suppose that \(b < \nu\), that is \(A\) has fewer
rows than columns. We shall extend \(A\) by adding
\(\nu - b\) rows to the bottom of it and we call this new
matrix \(B\). Then

\begin{equation*}
B^T B = (r-\lambda) I + \lambda J
\end{equation*}

since each column still contains \(r\) \(1\)s and
any two columns have \(\lambda\) common \(1\)s.
Clearly \(|B^T B| = |B^T||B| = |B^T| 0 = 0\) since there
exists rows of \(0\)s. However, since
\(r > \lambda\) (equality is not allowed) and hence
\(rk (r-\lambda)^{\nu - 1} \neq 0\). This is a
contradiction. ◻
\end{example}

\section{Square Block Designs}
\label{develop--math2276:ROOT:page--block-designs.adoc---square-block-designs}

As briefly discussed before, in the spacial case \(\nu = b\)
(and further \(k = r\)), a block design is called square or
symmetric. These designs are referred to as
\((\nu, k, \lambda)\)-configurations which now only have to
satisfy the following restriction

\begin{equation*}
k(k-1) = \lambda(\nu - 1)
\end{equation*}

Additionally, this symmetry does not refer to incidence matrix symmetric
(which need not exist) but rather to the following four properties. The
properties are

\begin{enumerate}[label=\arabic*)]
\item Any row contains \(k\) \(1\)s
\item Any column contains \(k\) \(1\)s
\item Any pair of columns both have \(1\)s in exactly
    \(\lambda\) rows
\item Any pair of rows both have \(1\)s in exactly
    \(\lambda\) columns.
\end{enumerate}

The last of these properties are yet to be proven. However,
\((4)\), along with \((2)\), implies that the
transpose of a square incidence matrix is also an incidence matrix. This
new block design is referred to as a \emph{dual} of the original (provided
that it is different). Furthermore, we will prove that \((1)\)
and \((4)\) are equivalent to \((2)\) and
\((3)\). This will be done by showing the following three
equivalences

\begin{itemize}
\item \((2) \wedge (3) \Longleftrightarrow A^T A = (k - \lambda)I + \lambda J\)
\item \((1) \wedge (4) \Longleftrightarrow A A^T = (k - \lambda)I + \lambda J\)
\item \(A^T A = (k - \lambda)I + \lambda J \Longleftrightarrow A A^T = (k - \lambda)I + \lambda J\)
\end{itemize}

where \(A\) is the incidence matrix. ie \(A\) is a
\(\nu \times \nu\) matrix consisting of \(0\)s and
\(1\)s.

\subsection{First and Second equivalences}
\label{develop--math2276:ROOT:page--block-designs.adoc---first-and-second-equivalences}

The proof of these two are quite similar and quite simple. So only the
first will be proven. That is, the following will be proven

\begin{equation*}
(2) \wedge (3) \Longleftrightarrow A^T A = (k - \lambda)I + \lambda J
\end{equation*}

\begin{example}[{Proof}]
The forward and reverse directions will be done separately
(despite being quite similar).

\begin{itemize}
\item Forward direction\newline
    Since each column contains exactly \(k\) \(1\)s, the
    dot product of any column with itself is \(k\) and hence the
    leading diagonal of \(A^T A\) consists of \(k\)s.
    Furthermore, since any pair of columns both have \(1\)s in
    exactly \(\lambda\) rows, the dot product of any two columns
    is \(\lambda\) and entries off the leading diagonal consists
    of \(\lambda\)s. Hence, the result follows.
\item Reverse direction\newline
    Since the leading diagonal consists of \(k\)s, the dot product
    of any column with itself is \(k\) and hence there are exactly
    \(k\) \(1\)s in each column (by the restriction of
    only \(0\)s and \(1\)s in the matrix). On the other
    hand, since terms off the leading diagonal consists of
    \(\lambda\)s, the dot product of any two columns is
    \(\lambda\) and hence any pair of columns both have
    \(1\)s in exactly \(\lambda\) rows (since the
    product of any other combination would contribute nothing to the dot
    product).
\end{itemize}

 ◻
\end{example}

\subsection{Final equivalence}
\label{develop--math2276:ROOT:page--block-designs.adoc---final-equivalence}

Let \(A\) be a \(\nu \times \nu\) matrix consisting
of \(0\)s and \(1\)s. Then

\begin{equation*}
A^T A = (k - \lambda)I + \lambda J \Longleftrightarrow A A^T = (k - \lambda)I + \lambda J
\end{equation*}

Note that it suffices to show forward direction since \(A^T\)
also satisfies the necessary conditions and hence

\begin{equation*}
A A^T = (A^T)^T (A^T) = (k - \lambda)I + \lambda J \implies A^T A = (A^T)(A^T)^T = (k - \lambda)I + \lambda J
\end{equation*}

The case of \(\lambda = k\) will also be ignore since the
result immediately follows. Therefore since
\(k = r \geq \lambda\) only the case when
\(k > \lambda\) will be considered.

\begin{example}[{Proof}]
Since

\begin{equation*}
A^T A = (k - \lambda)I + \lambda J
\end{equation*}

each column of \(A\) consists of \(k\)
\(1\)s and hence

\begin{equation*}
JA = kJ \Longleftrightarrow J A^{-1} = \frac{1}{k} J
\end{equation*}

since \(|A^T A| \neq 0\) implies that \(A^{-1}\)
exists. Also

\begin{equation*}
\begin{aligned}
        J A^T
        &= J \left[ (k-\lambda) A^{-1} + \lambda J A^{-1} \right] \\
        &= (k-\lambda) JA^{-1} + \frac{\lambda}{k} J^2 \\
        &= \frac{k - \lambda}{k} J + \frac{\lambda \nu}{k} J \\
        &= \frac{1}{k}\left[ (k - \lambda) + \lambda \nu  \right] J
    \end{aligned}
\end{equation*}

Now consider the term \(JA^T J\). On one hand

\begin{equation*}
JA^T J = J(J A)^T = J (k J) = k J^2 = k \nu J
\end{equation*}

while on the other

\begin{equation*}
J A^T J = (J A^T) J
        = \frac{1}{k}\left[ (k - \lambda) + \lambda \nu  \right] J^2
        = \frac{\nu}{k} \left[ (k - \lambda) + \lambda \nu  \right] J
\end{equation*}

Therefore
\(\frac{1}{k} \left[ (k - \lambda) + \lambda \nu  \right] = k\)
and hence

\begin{equation*}
JA^T = k J = (k J )^T = (JA)^T = A^T J
\end{equation*}

which implies that \(AJ = JA\). This final result shows that

\begin{equation*}
\begin{aligned}
        AA^T
        &= A A^T A A^{-1}\\
        &= A \left[ (k - \lambda)I + \lambda J + \right] A^{-1}\\
        &= (k - \lambda)I + \lambda A J A^{-1}\\
        &= (k - \lambda)I + \lambda J A A^{-1}\\
        &= (k - \lambda)I + \lambda J
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\section{Finite projective planes}
\label{develop--math2276:ROOT:page--block-designs.adoc---finite-projective-planes}

Finite projective planes are a geometric interpretation of square block
designs where \(\lambda = 1\), ie each pair occurs once.
Finite projective planes are a result of two areas of geometry, finite
geometry and projective geometry. Finite projective planes consists of
two objects, points and lines, where the lines consists of finite
points. In terms of block designs, the points are analogous to varieties
while the lines are analogous to blocks. This allows for a geometrical
approach to block designs.

Furthermore, the restriction on \(\lambda\) implies that
\(\nu = k^2 - k + 1\) and we can therefore parameterize
projective planes with the following restraints

\begin{itemize}
\item \(\nu = n^2 + n +1\)
\item \(k = n + 1\)
\item \(\lambda = 1\)
\end{itemize}

Where \(n\) refers to the order, ie a projective plane of
order \(n\) has the previously stated configuration.
Furthermore, the properties of square block designs now become

\begin{enumerate}[label=\arabic*)]
\item Each line has \(n+1\) points
\item Each point lies on \(n+1\) lines
\item Each pair of points are joined by exactly one line
\item Each pair of lines intersect at exactly one point
\end{enumerate}

The last two of these properties gives rational to why a geometrical
approach can be taken. Moreover, the last property is an important
aspect of projective geometry in which any pair of lines intersect at
exactly one point, unlike Euclidean geometry.

\subsection{Known results}
\label{develop--math2276:ROOT:page--block-designs.adoc---known-results}

Finite projective planes don’t exist for all orders. In fact, there is
not much we know. However, here are a few results we know so far

\begin{enumerate}[label=\arabic*)]
\item A finite projective plane exists if the order is a power of a prime
\item No plane of any other order is known to exist
\item A plane of order \(n\) does not exist if \(n\) is
    congruent to \(1\) or \(2\) module \(4\)
    and is divisible by a prime of form \(4k+3\) to an odd power.
    In fact, this divisibility condition is equivalent to not being able to
    represent \(n\) as the sum of two squares.
\end{enumerate}

The smallest case not covered by the above is \(10\) which
proven not to exist by computer calculations. Next is \(12\)
which is yet to be proven or disproven.

\section{Cyclic block designs}
\label{develop--math2276:ROOT:page--block-designs.adoc---cyclic-block-designs}

Given a difference set \(S\) of \(\mathbb{Z}_\nu\),
a cyclic \((\nu, k, \lambda)\) block design can immediately be
constructed where the \(t\)’th block is defined by

\begin{equation*}
b_t = \{t + e:  e\in S\}
\end{equation*}

The difference set \(S\) consists of \(k\) elements
such that there are exactly \(\lambda\) pairs
\((x, y)\) such that \(x-y \equiv a (mod\; \nu)\)
for all \(0<a<\nu\). The word ''cyclic'' is used to describe
such block designs since the elements in the block just cycle with an
offset \(t\). An outline of the proof of the generation of
cyclic block designs is as follows.

\begin{example}[{Proof}]
Consider any two elements \(u\) and \(v\).
By definition they must appear in exactly \(\lambda\) blocks
together (we have to prove that). Suppose they appear in the
\(t\)’th block, then there are \(x,y\) such that

\begin{equation*}
u \equiv x + t (mod\; \nu)\quad\text{and}\quad v \equiv y + t(mod\; \nu)
\end{equation*}

Then \(u-v \equiv x-y(mod\; \nu)\). Then, since
\(S\) is a difference set, there are \(\lambda\)
pairs \((x, y)\) such for which this property holds. Therefore
if \(S\) is a difference set, the block design is valid.
Additionally, the converse is also true: if the block design is valid
then \(S\) is a difference set. ◻
\end{example}
\end{document}
