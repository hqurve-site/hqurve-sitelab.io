\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Concepts in Mechanics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-tip}[{}]
Recall the notation \(\nabla = \vec{i} \frac{\partial}{\partial x}  + \vec{j} \frac{\partial}{\partial y} + \vec{k}\frac{\partial}{\partial z}\)
\end{admonition-tip}

\section{Laws of Conservation}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---laws-of-conservation}

\subsection{Continuity equation}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---continuity-equation}

\begin{admonition-tip}[{}]
This section is based on the textbook ``Fluid Mechanics'' by Landau and Lifshitz.
\end{admonition-tip}

Consider a fluid of variable density \(\rho(t,\vec{x})\) which has velocity \(\vec{v}(t,\vec{x})\).
Focus some (fixed) volume \(V\) though which this fluid travels.
Note that \(V\) does not constrain the flow of the fluid (eg a pipe) but is rather just a region of interest.
Let \(S\) be the surface of \(V\) and \(\hat{n}\) be the normal of \(S\) (at any point) which points outward of the volume.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/fluid-flow}
\caption{Illustration of the volume \(V\) in a pipe of fluid with laminar flow}
\end{figure}

There are two ways to determine the mass of the fluid in \(V\).
On one hand, we can directly measure the mass using the density

\begin{equation*}
m = \iiint_V \rho dV
\end{equation*}

Alternatively, we can look at the change of the mass in \(V\).
To determine the change, we can look at the mass exiting the volume
at the surface.
That will be

\begin{equation*}
\frac{\partial m}{\partial t} = - \iint_S \rho \vec{v}\cdot\hat{n} \ dS
\end{equation*}

By combining both forms, we obtain that

\begin{equation*}
\iiint_V \frac{\partial p}{\partial t} \ dV + \iint_S \rho \vec{v}\cdot\hat{b} \ dS = 0
\end{equation*}

any by \myautoref[{Gauss' divergence theorem}]{develop--math2270:ROOT:page--surface-integrals.adoc--gauss-divergence-theorem},

\begin{equation*}
0
= \iiint_V \frac{\partial \rho}{\partial t} \ dV + \iiint_V \nabla\cdot (\rho \vec{v}) \ dV
= \iiint_V \left[\frac{\partial \rho}{\partial t} + \nabla\cdot (\rho \vec{v}) \right] \ dV
\end{equation*}

Now, since the above holds for all regions \(V\), we must have that

\begin{equation*}
\frac{\partial \rho}{\partial t} + \nabla\cdot (\rho \vec{v}) = 0
\end{equation*}

This is called the \emph{continuity equation}.
Also, the quantity \(\rho\vec{v}\) is often called \emph{mass flux} and is denoted
by \(\vec{j_p}\).

If the fluid is \emph{incompressible}, \(\rho\) is constant and hence we obtain the following
simplified continuity equation

\begin{equation*}
\nabla \cdot \vec{v} = 0
\end{equation*}

\subsection{Considering pressure and forces}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---considering-pressure-and-forces}

Suppose that the fluid has pressure \(P(t,\vec{x})\)
and some external force per unit mass (acceleration) \(\vec{g}\) (eg gravity).
Then, we can compute the net force which is acting on the volume \(V\)
as follows since pressure is force divided by area.

\begin{equation*}
\vec{F}
= \iint_S -P\hat{n} \ dS + \iiint_V \vec{g} \ dm
= \iint_S -P\hat{n} \ dS + \iiint_V \rho\vec{g} \ dV
\end{equation*}

Then, again by Gauss' divergence theorem (with a twist), we get

\begin{equation*}
\vec{F} = \iiint_V -\nabla P \ dV + \iiint_V \rho\vec{g} \ dV
\end{equation*}

Alternatively, we can determine the force using Newton's
second law.
In particular

\begin{equation*}
\begin{aligned}
\vec{F}
&= \iiint_V \frac{d\vec{v}}{dt} \ dm
\\&= \iiint_V \rho\left(
    \frac{\partial\vec{v}}{\partial t}
    + \frac{\partial\vec{v}}{\partial x}\frac{dx}{dt}
    + \frac{\partial\vec{v}}{\partial y}\frac{dy}{dt}
    + \frac{\partial\vec{v}}{\partial z}\frac{dz}{dt}
    \right) \ dV
\\&= \iiint_V \rho\left(
    \frac{\partial\vec{v}}{\partial t}
    + (\vec{v} \cdot \nabla)\vec{v}
    \right) \ dV
\end{aligned}
\end{equation*}

So, we have that

\begin{equation*}
\iiint_V -\nabla P + \rho \vec{g} \ dV
= \vec{F}
= \iiint_V \rho\left(
    \frac{\partial\vec{v}}{\partial t}
    + (\vec{v} \cdot \nabla)\vec{v}
    \right) \ dV
\end{equation*}

From this, we get Euler's equation

\begin{equation*}
-\frac{1}{\rho}\nabla P + \vec{g} =
\frac{\partial\vec{v}}{\partial t} + (\vec{v} \cdot \nabla)\vec{v}
\end{equation*}

Under steady state (\(\vec{v}=0\)), we get that

\begin{equation*}
\frac{1}{\rho}\nabla P = \vec{g}
\end{equation*}

\begin{example}[{Changes in atmospheric pressure due to altitude.}]
We will derive the \emph{Barometric formula}.
First, consider the \(z\) component of the steady state Euler equation

\begin{equation*}
\frac{1}{\rho} \frac{dP}{dz} = -g
\end{equation*}

where \(g\approx 9.81\) since \(z\) is the
height from sea-level.

We use the ideal gas law to obtain that

\begin{equation*}
\rho = \frac{m}{V} = \frac{M}{RT}P
\end{equation*}

where

\begin{itemize}
\item \(V\) is the volume
\item \(m\) is the mass
\item \(M\) is the mass of one mole of gas (ie the mass of some fixed number of particles)
\item \(R\) is the universal gas constant
\end{itemize}

So,

\begin{equation*}
\frac{1}{P}\frac{dP}{dz} = \frac{gM}{RT}
\end{equation*}

and by integrating both sides, we get

\begin{equation*}
\ln P - \ln P_0 = \frac{gM}{RT}z
\end{equation*}

where \(P_0\) is the pressure at sea level.
Therefore,

\begin{equation*}
P = P_0 \exp\left(-\frac{Mg}{RT}z\right)
\approx P_0 \exp\left(-\frac{z}{10^4}\right)
\end{equation*}

Therefore, the air pressure drops by a factor of \(3\) (approximately \(e\)) for
every 10km increase in altitude.
\end{example}

\section{Lagrangian vs Eulerian Equations of Motion}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---lagrangian-vs-eulerian-equations-of-motion}

\begin{admonition-tip}[{}]
See \url{https://en.wikipedia.org/wiki/Lagrangian\_and\_Eulerian\_specification\_of\_the\_flow\_field}
\end{admonition-tip}

Suppose we want to study the flow of a fluid.
Then, there are two ways:

\begin{description}
\item[Eulerian] We focus on a specific location in space
    and examine the different properties of the fluid.
    So for example, we may look at \(v(\vec{x},t)\)
    or \(\rho(\vec{x}, t)\)
\item[Lagrangian] We focus on a single particle
    which starts at position \(\vec{x}_0\)
    and is influenced by the velocity field \(v(\vec{x}, t)\).
    We call this position \(\xi(\vec{x}_0, t)\)
    and it follows the velocity field \(v(\vec{x}, t)\).
\end{description}

Note that at \(t=t_0\), we have that
\(\vec{x} = \xi\).

We can relate the Lagrangian and Eulerian descriptions using

\begin{equation*}
\vec{v}(\xi(\vec{x}_0, t), t) = \frac{\partial \xi}{\partial t}(\vec{x}_0, t)
\end{equation*}

\subsection{Solving hard equations}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---solving-hard-equations}

Suppose we have the following equation to solve

\begin{equation*}
a(x,t)\frac{\partial \theta}{\partial x} + b(x,t)\frac{\partial \theta}{\partial t} = g(x,t,\theta)
\end{equation*}

This is a hard equation to solve.
We can rearrange the above equation to get

\begin{equation*}
\frac{\partial \theta}{\partial t}
+ \frac{a(x,t)}{b(x,t)}\frac{\partial \theta}{\partial t}
= \frac{g(x,t,\theta)}{b(x,t)}
\end{equation*}

It we imagine \(x(t)\), we can get a solution by
assuming that the left hand side the result of the chain rule of \(\frac{d\theta}{dt}\).
In such a case,

\begin{equation*}
\frac{a(x,t)}{b(x,t)} = \frac{dx}{dt} = v(x,t)
\end{equation*}

If we let \(x(t) = \phi(t,t_0,x_0)\) be the solution to the above,
we would get that

\begin{equation*}
\frac{d\theta}{dt} = \frac{g(\phi(t,t_0,x_0), t, \theta)}{b(\phi(t,t_0,x_0), t)}
\end{equation*}

So, by making the assumption that \(x(t)\), we can convert our
partial differential equation into two univariate ODEs.

Let \(x(t_0) = \xi\).
The term \(\xi\) is often called a \emph{generalized coordinate}.
Then, it is interesting to note that solving for \(\theta\)
only requires us to know only the initial value \((\xi, t_0)\)
(and one more constant from the ODE) rather than information
for all \((x,t)\).
Also, it is interesting that usually \(x\) and \(t\) are independent
variables; but despite this, this method allows us to get results.

\begin{admonition-important}[{}]
Everything in the above is a result of me looking at the results
in the section in the notes and working backwards to get a feasible explanation.
So, it could be entirely wrong.
\end{admonition-important}

\begin{example}[{Example}]
Consider

\begin{equation*}
x\frac{\partial \theta}{\partial x} + 2t \frac{\partial \theta}{\partial t} = 3x^2\theta
\end{equation*}

Given that \(\theta(t=1) = 5x^2\).
First, by rearranging, we get,

\begin{equation*}
\frac{\partial \theta}{\partial t} + \frac{x}{2t}\frac{\partial \theta}{\partial x} = \frac{3x^2\theta}{2t}
\end{equation*}

First, we solve for \(x(t)\) as follows

\begin{equation*}
\frac{dx}{dt} = \frac{x}{2t} \implies x(t) = x_1\sqrt{\frac{t}{t_1}}
\end{equation*}

where \(t_1\) and \(x_1\) are constants.
So, we can now rewrite our equation as

\begin{equation*}
\frac{d\theta}{dt} = \frac{3x^2}{2t}\theta = \frac{3x_1^2}{2t_1}\theta
\end{equation*}

and we obtain that

\begin{equation*}
\theta(t)
= \theta_1 \exp\left(\frac{3x_1^2}{2t_1}(t-t_1)\right)
= \theta_1 \exp\left(\frac{3x^2}{2t}(t-t_1)\right)
\end{equation*}

where \(\theta(t_1) = \theta_1\).
Finally, we can use our initial condition
at \(t_1=t=1\), to obtain that

\begin{equation*}
5\frac{x^2}{t} = \theta(t_1) = \theta_1
\end{equation*}

Note that we have to use \(\frac{x^2}{t}\) instead of just \(x^2\)
since the latter is not a constant while the former is.
So, we obtain that

\begin{equation*}
\theta(x,t)
= 5\frac{x^2}{t} \exp\left(\frac{3x^2}{2t}(t-1)\right)
\end{equation*}
\end{example}

\subsection{Lagrangian coordinates}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---lagrangian-coordinates}

Consider the following velocity field in Lagrangian coordinates

\begin{equation*}
v(\xi,t) = \xi+ t, \ t_0=0
\end{equation*}

We can find the velocity field in Eulerian coordinates as follows

\begin{equation*}
\frac{dx}{dt} = v(\xi, t) \implies x = \xi(1+t) + \frac{t^2}{2}
\end{equation*}

So,

\begin{equation*}
\xi = \frac{x-\frac{t^2}{2}}{1+t}
\end{equation*}

and

\begin{equation*}
v = \frac{x-\frac{t^2}{2}}{1+t} + t
\end{equation*}

\section{Equations of Caustics}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---equations-of-caustics}

In optics, a \emph{caustic} is the envelop of light rays that are reflected and refracted
off a (possibly) curved surface and projected onto another.
It is the surface to which each of the light rays are tangent, hence resulting
in a concentration of light.

In this section, we will

\begin{enumerate}[label=\arabic*)]
\item Derive equations for critical points in terms of Lagrangian coordinates
\item Determine the behaviour of the caustics
\end{enumerate}

\subsection{Equations for critical points for density}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---equations-for-critical-points-for-density}

Since we can model light using waves, we can look for the critical points of density
in fluids to find caustics.

\subsubsection{Determining an initial equation for density}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---determining-an-initial-equation-for-density}

Recall that the continuity equation in one dimension

\begin{equation*}
\frac{\partial \rho}{\partial t} + \frac{\partial \rho}{\partial x}v(x,t) + \rho\frac{\partial v}{\partial x} = 0
\end{equation*}

and by using Lagrangian equations (ie using multivariate chain rule), we get

\begin{equation*}
\frac{d\rho}{dt} = -\rho \frac{\partial v}{\partial x}
\end{equation*}

Where \(x = \phi(t;x_1, t_1)\) is the solution to

\begin{equation*}
\frac{dx}{dt} = v(x,t) , \quad x(t_0) = \xi
\end{equation*}

Hence \(\xi = x(t_0) = \phi(t_0; x_1, t_1)\).
Since \((x_1,t_1)\) is arbitrary we can set them to \((x, t)\) to obtain

\begin{equation*}
\xi = \phi(t_0; x,t) \implies x = \phi(t;x(t_0), t_0) = \phi(t;\xi, t_0)
\end{equation*}

Therefore, by solving the Lagrangian equation, to get

\begin{equation*}
\rho
= \frac{\rho_0}{\exp\left\{\int_{t_0}^t \frac{\partial}{\partial x}v(x,t) \ dt\right\}}
= \frac{\rho_0}{\exp\left\{\int_{t_0}^t \frac{\partial}{\partial x} v\left(\phi(t;\xi, t_0), t\right) \ dt\right\}}
\end{equation*}

\subsubsection{Finding critical points}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---finding-critical-points}

We will now aim to simplify this.
Define \(y = \frac{\partial x}{\partial \xi}\).
Then, since \(x(t_0) = \xi\), \(y(t_0) = 1\)
and \(\frac{\partial v}{\partial \xi} = \frac{\partial v}{\partial x}\frac{\partial x}{\partial \xi} = \frac{\partial v}{\partial x}y\).
(Recall that when doing partial differentiation, we assume everything else is constant; so it is like univariate differentiation.)

\begin{admonition-todo}[{}]
Understand how the below formula is obtained.
\end{admonition-todo}

We also somehow get that \(\frac{dy}{dt} = \frac{\partial v}{\partial x} y\),
which can be used to simplify our expression for \(\rho\) as

\begin{equation*}
\rho
= \frac{\rho_0}{\exp\left\{\int_{t_0}^t \frac{1}{y}\frac{\partial v}{\partial \xi}  \ dt\right\}}
= \frac{\rho_0}{\partial x/ \partial \xi}
= \frac{\rho_0}{1 + \int_{t_0}^t \frac{\partial v(\xi, t)}{\partial \xi} \ dt }
\end{equation*}

\subsection{Behaviour of caustics}
\label{develop--math6192:ROOT:page--mechanics-concepts.adoc---behaviour-of-caustics}

Suppose that \(v(\xi)\) (independent of time).
Then, we have that

\begin{equation*}
x = \xi + v(\xi)(t-t_0)
\end{equation*}

We are interested in finding critical points \((x_{cr},t_{cr})\)
at which time, the first and second derivatives, \(\frac{dx}{d\xi}\)
and \(\frac{d^2x}{d\xi^2}\), are both zero.
So, we have that

\begin{equation*}
1 + v_\xi(\xi_{cr})(t_{cr}-t_0) = 0, \ v_{\xi\xi}(\xi_{cr}) = 0
\end{equation*}

From the first, we have that

\begin{equation*}
x = \xi - \frac{v(\xi)}{v_\xi(\xi)},
\quad
t = t_0 - \frac{1}{v_\xi(\xi)}
\end{equation*}

and by differentiating with wrt \(\xi\), we get

\begin{equation*}
\frac{dt}{d\xi} = \frac{v_{\xi\xi}(\xi_{cr}}{[v_{\xi}(\xi)]^2}
,\quad
\frac{dx}{d\xi} = \frac{v(\xi) v_{\xi\xi}(\xi)}{[v_{\xi}(\xi)^2]^2}
\end{equation*}

At the critical points, \(\frac{dt}{d\xi} = \frac{dx}{d\xi} = \frac{d^2x}{d\xi^2} = 0\).

Consider the Taylor series (wrt \(\xi\)) of \(t\) and \(x\) about the critical point.
We must have that

\begin{equation*}
\begin{aligned}
t - t_{cr} &= A_2\eta^2 + A_3\eta^3+\cdots
\\
x - x_{cr} &= B_3\eta^3+\cdots
\end{aligned}
\end{equation*}

where \(\eta = \xi - \xi_{cr}\). So, by assuming the higher order
terms are negligible, we have that \(\eta^2\)
and \(\eta^3\) may be written as linear combinations
of \((t-t_{cr})\) and \((x-x_{cr})\).
That is,

\begin{equation*}
\eta^2 = a_1(t-t_{cr}) + b_1(x-x_{cr})
,\quad\text{and}\quad
\eta^3 = a_2(t-t_{cr}) + b_2(x-x_{cr})
\end{equation*}

Now, since \(x\) has zero-first and second derivatives, we have that

\begin{equation*}
\frac{\partial x}{\partial \xi} \sim (\xi - \xi_{cr})^2 =\eta^2
= (\eta^{3})^{2/3} \sim (x-x_{cr})^{2/3}
\end{equation*}

So, we see that the density has the following relationship

\begin{equation*}
\rho \sim \frac{\rho_0}{(x-x_{cr})^{2/3}}
\end{equation*}
\end{document}
