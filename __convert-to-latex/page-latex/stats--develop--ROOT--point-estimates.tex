\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Point estimates}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Often, when studying a population, there are some parameters which we do not know.
In such cases, it may prove useful for us to estimate the values of these parameters.
In order to do this, a sample is taken from the population and a statistic is generated.

Let \(\theta\) be a parameter and \(X_1, \ldots X_n\) be a sample from the population.
There may be many different statistics, \(T(\vec{X})\), which we can choose to be the point estimator of \(\theta\),
but we want to choose the best one.
If we choose \(T\) to be the point estimator of \(\theta\),
we denote it by \(\hat{\theta} = T\).

\begin{admonition-important}[{Estimate vs estimator}]
The estimate is a value while the estimator is a random variable (since it is a function
of random variables).
\end{admonition-important}

\section{Properties of estimates}
\label{develop--stats:ROOT:page--point-estimates.adoc---properties-of-estimates}

The properties and their intuition are

\begin{description}
\item[Bias] On average, what is the error of using this estimate?
\item[Consistency] Does more data lead to a more accurate estimate?
\end{description}

\subsection{Bias}
\label{develop--stats:ROOT:page--point-estimates.adoc---bias}

The bias of \(\hat{\theta}\) is defined as \(Bias(\hat{\theta}) = E[\hat{\theta} -\theta]\).
An estimator is unbiased if its bias is zero.

\subsection{Consistency}
\label{develop--stats:ROOT:page--point-estimates.adoc---consistency}

Consider a sequence of estimators \(\{\hat{\theta}_n\}_{n=1}^\infty\)
from samples with respective sizes \(n\). The estimator is

\begin{description}
\item[Weakly consistent] if it converges in probability to \(\theta\).
\item[Strongly consistent] if it converges almost surely to \(\theta\). That is, the \(\hat{\theta}_n\)
    converge to \(\theta\) with probability 1.
\end{description}

\begin{proposition}[{}]
Let \(\{\hat{\theta}_n\}\) be a sequence of estimators for parameter \(\theta\).
Then, \(\{\hat{\theta}_n\}\) is weakly consistent if the following hold

\begin{itemize}
\item \(\lim_{n\to\infty} Bias[\hat{\theta}_n] = 0\). That is, the estimators are \textbf{asymptotically unbiased}
\item \(\lim_{n\to \infty} Var[\hat{\theta}_n ] = 0\)
\end{itemize}
\end{proposition}

\section{Properties of statistics}
\label{develop--stats:ROOT:page--point-estimates.adoc---properties-of-statistics}

The properties and their intuition are

\begin{description}
\item[Sufficiency] Does this statistic capture all the information necessary to estimate \(\theta\)?
    For example, can we generate the MLE using this statistic
\item[Ancillary] This statistic captures no information about \(\theta\).
    So, knowing this statistic does not give us a better idea of what \(\theta\) may be.
\item[Complete] This statistic only captures information about \(\theta\).
\end{description}

\subsection{Sufficiency}
\label{develop--stats:ROOT:page--point-estimates.adoc---sufficiency}

The statistic \(T\) is a \textbf{sufficient} statistic for \(\theta\) if the conditional
distribution of \(\vec{X}\) given \(T(\vec{X})\) does not depend on \(\theta\).
For example, if a sample \(\vec{X}\) has joint pdf \(f(\vec{x} \ | \ \theta)\),

\begin{equation*}
f(\vec{x} \ | \ T(\vec{x})=t, \theta) = h(\vec{x})
\end{equation*}

where \(h\) is a function of \(\vec{x}\) only.

\begin{admonition-important}[{}]
\(T\) need not be an estimator of \(\theta\).
\end{admonition-important}

\begin{proposition}[{}]
Consider sample \(\vec{X}\) has joint pdf \(f(\vec{x} \ | \ \theta)\).
Let \(T(\vec{X})\) be a statistic with pdf \(\nu(t \ | \ \theta)\).
Then, \(T(\vec{x})\) is sufficient for \(\theta\) if and only if

\begin{equation*}
\frac{f(\vec{x} \ |  \theta)}{\nu(T(\vec{x}) \ | \ \theta)} = u(\vec{x})
\end{equation*}

That is, the ratio is constant with respect to \(\theta\).
\end{proposition}

\begin{proposition}[{Fisher factorization criterion}]
Consider sample \(\vec{X}\) has joint pdf \(f(\vec{x} \ | \ \theta)\).
Let \(T(\vec{X})\) be a statistic.
Then, \(T(\vec{x})\) is sufficient for \(\theta\) if and only if
\(f(\vec{x} \ | \ \theta)\) can be factorized as

\begin{equation*}
f(\vec{x} \ |  \theta) = u(\vec{x})\nu(t, \theta)
\end{equation*}
\end{proposition}

\subsubsection{Minimal sufficiency}
\label{develop--stats:ROOT:page--point-estimates.adoc---minimal-sufficiency}

A sufficient statistic \(T(\vec{X})\) is called a \textbf{minimal sufficient} statistic
if for any other sufficient statistic \(T'(\vec{X})\),
\(T(\vec{X})\) is a function of \(T'(\vec{X})\).

\begin{theorem}[{}]
Let \(f_{\vec{X}}(\vec{x} \ | \ \theta)\) be the joint pdf/pmf of random sample
\(\vec{X} \in \chi\). Suppose that there exists \(T(\vec{x})\)
such that for all \(\vec{x}, \vec{y} \in \chi\),
the ratio

\begin{equation*}
\frac{f_{\vec{X}}(\vec{x} \ | \ \theta)}{f_{\vec{X}}(\vec{y} \ | \ \theta)}
\end{equation*}

is constant as a function of \(\theta\) if and only if \(T(\vec{x}) = T(\vec{y})\).
Then, \(T(\vec{X})\) is a minimal sufficient statistic.
\end{theorem}

\subsection{Ancillary}
\label{develop--stats:ROOT:page--point-estimates.adoc---ancillary}

A statistic \(T(\vec{X})\) is an \textbf{ancillary} statistic for \(\theta\) if its distribution
does not depend on \(\theta\).

\begin{admonition-note}[{}]
This implies that the likelihood function is constant and hence information about
the statistic does not provide information about the parameter.
\end{admonition-note}

\begin{example}[{Ancillary from location family}]
Let \(X_1,\ldots X_n\) be from a location family with pdf \(f(x-\mu)\)
where \(-\infty < \mu < \infty\).
Then, the range \(R = X_{(n)} - X_{(1)}\) is an ancillary statistic.

Assume that the cdf exists and it is \(F(x-\mu)\).
To see this, let \(Z_i = X_i - \mu\).
Then the \(Z_i\) are iid with pdf \(f(x)\) and cdf \(F(x)\).
The cdf of \(R\) is then

\begin{equation*}
F_R(r|\mu) = P(R\leq r | \mu)
= F(X_{(n)}-X_{(1)} \leq r | \mu)
= F(Z_{(n)}-Z_{(1)} \leq r | \mu)
= F(Z_{(n)}-Z_{(1)} \leq r)
\end{equation*}

since the \(Z_i\) do not depend on \(\mu\).
So, \(R\) is an ancillary statistic.
\end{example}

\subsection{Complete}
\label{develop--stats:ROOT:page--point-estimates.adoc---complete}

\begin{admonition-important}[{}]
This is a property of a \textbf{family} of random variables, not just one random variable.
Also, this can be rephrased for a family of random variables, not just a statistic.
\end{admonition-important}

Let \(\tau = \{f_T(t|\sigma) \ | \ \tau \in \Sigma\}\)
be a family of pdfs (or pms) for a statistic \(T(\vec{X})\).
The family of probability distributions is called \textbf{complete}
if for all \(g(t)\)

\begin{equation*}
\left\{\forall \theta \in \Omega: E[g(T) \ | \ \theta] = 0\right\}
\implies
\left\{\forall \theta \in \Omega: P(g(T) =0\ | \ \theta) =1\right\}
\end{equation*}

That is \(g(T) = 0\) \textbf{almost surely}.
Equivalently, we say that \(T(\vec{X})\) is a \textbf{complete statistic}.

\section{Minimizing error}
\label{develop--stats:ROOT:page--point-estimates.adoc---minimizing-error}

We can also measure the error of an estimator using the \textbf{mean squared error}

\begin{equation*}
MSE(\hat{\theta}) = E[(\hat{\theta} -\theta)^2] = Var[\hat{\theta}] + Bias[\hat{\theta}]^2
\end{equation*}

Given that the estimate is unbiased, we want to therefore minimize the variance of the estimator.
We can therefore compare the efficiency for two estimators \(\hat{\theta}_1\) and
\(\hat{\theta}_2\) using the relative efficiency

\begin{equation*}
RelEff(\hat{\theta}_1, \hat{\theta}_2) = \frac{Var[\hat{\theta}_1]}{Var[\hat{\theta}_2]}
\end{equation*}

This leads to the idea of a \textbf{minimum variance unbiased estimator} (MVUE).

\section{Maximizing likelihood}
\label{develop--stats:ROOT:page--point-estimates.adoc---maximizing-likelihood}

We can also chose our estimator by ensuring that it maximizes the probability (or likelihood) of our observation.
Such an estimator is called the \textbf{maximum likelihood estimator} (MLE).

Suppose we have random sample \(X_1,\ldots X_n\) with pdf \(f(x \ | \ \theta)\).
We assume that the \(x_i\) are fixed since we have their data.
The likelihood function is simply the joint pdf as a function of \(\theta\). That is

\begin{equation*}
\mathcal{L}(\theta \ | \ \vec{x}) = \prod_{i=1}^n f(x_i \ | \ \theta)
\end{equation*}

The value \(\hat{\theta}_{MLE}\) which maximizes the likelihood function is called the
\textbf{maximum likelihood estimate}.

Since the product may be numerically unstable, we instead maximize the log-likelihood function

\begin{equation*}
\mathcal{l}(\theta \ | \ \vec{x}) = \ln \mathcal{L}(\theta \ | \ \vec{x})
\end{equation*}

The same maximum is obtained since \(\ln t\) is strictly increasing.

\begin{lemma}[{Invariance Property}]
If \(\tau(\theta)\) is a real valued function of \(\theta\).
Then the MLE of \(\tau(\theta)\) is \(\tau(\hat{\theta}_{MLE})\)
where \(\hat{\theta}_{MLE}\) is the MLE of \(\theta\).
\end{lemma}

\section{Method of moments}
\label{develop--stats:ROOT:page--point-estimates.adoc---method-of-moments}

Suppose that the population is characterized by \(k\) parameters \(\theta_1, \ldots \theta_k\).
The method of moments is conducted as follows:

\begin{enumerate}[label=\arabic*)]
\item Determine the first \(k\) moments of the sample data
\item Derive expressions for the first \(k\) moments for the population, in terms of \(\theta_1, \ldots \theta_k\)
\item Solve the system for the \(\theta_i\)
\end{enumerate}

This method is not the best, but it is simple and can at least give us an initial
guess for the population parameters.
\end{document}
