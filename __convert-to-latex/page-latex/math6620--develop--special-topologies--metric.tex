\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Metrics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \((X, d)\) be a metric space.
Then, this metric space generates a topology with basis

\begin{equation*}
\left\{B(x, \varepsilon) \ | \ x\in X, \ \varepsilon > 0\right\}
\end{equation*}

This topology is called the \emph{metric topology}.

Conversely any topology which can be generated using a basis of the above form is called
\emph{metrizable}.

\begin{proposition}[{}]
Let \((X, d)\) be a metric space. Then, the metric topology is the coarsest
topology on which \(d: X\times X\to \mathbb{R}\) is continuous (where the domain
has the product topology).

\begin{proof}[{}]
First we show that \(d\) is continuous.
Consider arbitrary \((a,b) \subseteq \mathbb{R}\) and \((x,y) \in d^{-1}((a,b))\).
We would show that \(B(x, \varepsilon) \times B(y, \varepsilon)\)
is contained in \(d^{-1}((a,b))\) for some \(\varepsilon > 0\)
which is yet to be specified.

Consider \((x', y') \in B(x, \varepsilon) \times B(y, \varepsilon)\),
then

\begin{equation*}
d(x', y') \leq d(x', x) + d(x,y) + d(y,y') \leq 2\varepsilon + d(x,y)
\end{equation*}

Also,

\begin{equation*}
d(x,y) \leq d(x,x') + d(x', y') + d(y', y) \leq 2\varepsilon + d(x', y')
\implies d(x', y') \geq d(x,y) - 2\varepsilon
\end{equation*}

We want \(d(x',y') \in (a,b)\).
By letting \(\varepsilon = \frac{\min(b-d(x,y), d(x,y)-a)}{2}\), we get this property.
Therefore, \(d^{-1}((a,b))\) is open for all basis elements \((a,b) \subseteq \mathbb{R}\)
and hence \(d\) is continuous on the metric topology.

Now, suppose that \(\mathcal{T}'\) is another topology on \(X\) on which \(d\) is continuous.
Consider arbitrary open ball \(B(x, \varepsilon)\).
We need to show this ball is open in \(\mathcal{T}'\).
Consider arbitrary \(y \in B(x,\varepsilon)\).

Since \((-\infty, \varepsilon)\) is open, \(d^{-1}((-\infty, \varepsilon))\) is open in \(\mathcal{T}'\).
Since \((x,y) \in d^{-1}((-\infty,\varepsilon))\), there exists
open neighbourhoods \(U, V \in \mathcal{T}'\) of \(x\) and \(y\)
respectively such that \(U\times V \subseteq d^{-1}((-\infty, \varepsilon))\).
Then, for any \(v \in V\), \(d(x,v) < \varepsilon\) and hence \(V\) is a neighbourhood of
\(y\) contained in \(B(x, \varepsilon)\).

Therefore, \(B(x, \varepsilon)\) is open in \(\mathcal{T}'\) and the metric topology
is coarser than \(\mathcal{T}'\).
\end{proof}
\end{proposition}

\section{Equivalent topologies}
\label{develop--math6620:special-topologies:page--metric.adoc---equivalent-topologies}

\begin{lemma}[{}]
Let \(d_1\) and \(d_2\) be metrics on \(X\) which generate topologies
\(\mathcal{T}_1\) and \(\mathcal{T}_2\) respectively.
If there exists \(c_1 > 0\) such that

\begin{equation*}
c_1d_2(x, y) \leq d_1(x,y)
\end{equation*}

then \(\mathcal{T}_1\)  is finer than \(\mathcal{T}_2\)
\end{lemma}

\begin{admonition-tip}[{}]
In order to remember the order of the inequality, remember that we are trying
to fit a \(d_1\) ball inside a \(d_2\) ball. In order to know whether
we made the \(d_1\) ball small enough, we need to be able to bound \(d_2\) by \(d_1\).
\end{admonition-tip}

\begin{proof}[{}]
Let \(B_1\) and \(B_2\) represent open balls using \(d_1\) and \(d_2\)
respectively.

Consider \(B_2(x, \varepsilon)\) and \(y \in B_2(x,\varepsilon)\).
Consider the ball \(B_1(y, \delta)\) for some \(\delta\) yet to be specified.
We wish to find \(\delta > 0\) such that \(B_1(y, \delta) \subseteq B_2(x,\varepsilon)\).
Consider arbitrary \(z \in B_1(y, \delta)\); then,

\begin{equation*}
d_2(z,x) \leq d_2(z, y) + d_2(y,x) \leq \frac{1}{c_1}d_1(z,y) + d_2(x,y)
< \frac{1}{c_1}\delta + d_2(x,y)
\end{equation*}

By choosing \(\delta = c_1(\varepsilon - d_2(x,y))\) we obtain
that \(d_2(z,x) < \varepsilon\) and hence \(z \in B_2(x,\varepsilon)\).
Since the choice of \(z\) was arbitrary, \(B_1(y, \delta) \subseteq B_2(x,\varepsilon)\)
and hence every \(y \in B_2(x,\varepsilon)\) has a open
neighbourhood in \(\mathcal{T}_1\) which is contained in \(B_2(x, \varepsilon)\).

Therefore, \(\mathcal{T}_1\) is finer than \(\mathcal{T}_2\)
\end{proof}

\begin{corollary}[{}]
Let \(d_1\) and \(d_2\) be metrics on \(X\) which generate topologies
\(\mathcal{T}_1\) and \(\mathcal{T}_2\) respectively.
If there exists \(c_1, c_2 > 0\) such that

\begin{equation*}
c_1d_2(x, y) \leq d_1(x,y) \leq c_2d_2(x,y)
\end{equation*}

then \(\mathcal{T}_1 = \mathcal{T}_2\).
\end{corollary}

\subsection{Standard bounded metric}
\label{develop--math6620:special-topologies:page--metric.adoc---standard-bounded-metric}

Let \((X, d)\) be a metric space.
Often it is useful to have a metric which is bounded.
It happens that we can define another metric on \(X\) which yields
the same topology as \(d\).
We define the \emph{standard bounded metric} corresponding to \(d\)
as

\begin{equation*}
\overbar{d}(x,y) = \min(d(x,y), 1)
\end{equation*}

Then, \(\overbar{d}\) forms a metric on \(X\) and \(d\) and \(\overbar{d}\)
generate the same topology on \(X\).
Since we only care about the topology generated, we can simply assume
that any metric we are working with is a standard bounded metric.

\begin{proof}[{}]
Clearly, \(\overbar{d}\) satisfies non-negativity, coincidence and symmetry.
For the triangle inequality, notice that for any \(a, b \geq 0\),

\begin{equation*}
\min(a+b,1) \leq \min(a, 1) + \min(b,1)
\end{equation*}

Therefore,

\begin{equation*}
\overbar{d}(x, z)
= \min(d(x,z), 1)
\leq \min(d(x,y) + d(y,z), 1)
\leq \min(d(x,y),1) + \min(d(y,z),1)
= \overbar{d}(x,y) + \overbar{d}(y,z)
\end{equation*}

Therefore \(\overbar{d}\) is a metric.

Now, note that \(\overbar{d}(x,y) \leq d(x,y)\).
So the topology induced by \(d\) is finer than the topology induced by \(\overbar{d}\).

Now, consider arbitrary \(B_d(x, \varepsilon)\) in the topology generated by \(d\).
Let \(y \in B_d(x, \varepsilon)\). Then, there exists \(\delta > 0\)
such that \(B_d(y, \delta) \subseteq B_d(x, \varepsilon)\).
If we consider \(\overbar{\delta} = \min\left(\delta, \frac{1}{2}\right)\),

\begin{equation*}
B_{\overbar{d}}(y, \overbar{\delta}) = B_d(y, \overbar{\delta}) \subseteq B_d(y, \delta) \subseteq B_d(x, \varepsilon)
\end{equation*}

since the balls with radius less than \(1\) are the same in both metrics.
Hence, the topology induced by \(\overbar{d}\) is finer than the topology induced
by \(d\). Therefore, the two topologies are the same.
\end{proof}

\section{Metrics on real sequences}
\label{develop--math6620:special-topologies:page--metric.adoc---metrics-on-real-sequences}

Consider the set \(\mathbb{R}^\omega\) which is the set of real sequences.
Then, from \myautoref[{}]{develop--math6620:special-topologies:page--arbitrary-product.adoc--example-with-R},
we know that there are the box and product topologies are distinct when applied to
\(\mathbb{R}^\omega\). In this section, we show that the product topology
is metrizable and that there is a metrizable topology which is strictly between
the product and box topologies.

We define the \emph{uniform metric}, \(\overbar{\rho}\) as

\begin{equation*}
\overbar{\rho}(\vec{x}, \vec{y}) = \sup_{i \in \mathbb{N}} \min(|x_i - y_i|, 1)
\end{equation*}

and

\begin{equation*}
D(\vec{x}, \vec{y}) = \sup_{i \in \mathbb{N}} \frac{\min(|x_i - y_i| , 1)}{i+1}
\end{equation*}

These two functions form metrics on \(\mathbb{R}^\omega\).
For the triangle inequality, recall that \(\min(|x_i - y_i|, 1)\)
is a standard bounded metric.
The topology defined by the uniform metric is called the \emph{uniform topology}

\begin{proposition}[{}]
Let \(\mathbb{R}^{oo} \subseteq \mathbb{R}^\omega\) be the set real sequences
with finitely many non-zero terms.
Then,

\begin{enumerate}[label=\arabic*)]
\item \(D\) generates the product topology on \(\mathbb{R}^\omega\)
\item The closure of \(\mathbb{R}^{oo}\) in the uniform topology is given by
    
    \begin{equation*}
    \left\{\vec{x} \in \mathbb{R}^\omega \ \middle| \ \lim_{i\to\infty} x_i =0 \right\}
    \end{equation*}
\item the uniform topology is strictly finer than
    the product topology on \(\mathbb{R}^\omega\) but strictly coarser than
    the box topology on \(\mathbb{R}^w\)
\end{enumerate}

\begin{proof}[{Statement 1}]
\begin{admonition-note}[{}]
Proof from class
\end{admonition-note}

First consider arbitrary open basis element
\(U^* = U_0 \times \cdots \times U_n \times \mathbb{R} \times \cdots\)
for some \(n \geq 0\). Consider arbitrary \(\vec{x} \in U^*\).
Then, there exists \(\varepsilon_i > 0\) such that

\begin{equation*}
\left(x_i - \varepsilon_i(i+1), x_i - \varepsilon_i(i+1)\right) \subseteq U_i
\end{equation*}

for each \(0 \leq i \leq n\).
Define \(\varepsilon = \min\left(\frac12, \min \varepsilon_i\right)\).
Then, for each \(\vec{y} \in B(\vec{x}, \varepsilon)\) (using metric \(D\)),
we have that

\begin{equation*}
\frac{|y_i - x_i|}{i+1} = \frac{\min(|y_i - x_i|, 1)}{i+1} < \varepsilon \leq \varepsilon_i
\implies y_i \in U_i
\end{equation*}

Hence \(\vec{y} \in U^*\). Note we do not need to consider \(i > n\)
since \(U^*\) has \(\mathbb{R}\) in these components.
Therefore, we have shown that the topology generated by \(D\)
is finer than the product topology.

Conversely, consider arbitrary \(B(\vec{x}, \varepsilon)\).
We would show that this set is open in the product topology.
For each \(i \geq 0\), define

\begin{equation*}
U_i = \left\{y_i \in \mathbb{R} \ \middle| \ \frac{\min(|x_i - y_i|, 1)}{i+1} < \varepsilon\right\}
= \begin{cases}
(x_i - \varepsilon(i+1), x_i + \varepsilon(i+1)) ,&\quad\text{if } \varepsilon(i+1) < 1\\
\mathbb{R} ,&\quad\text{if } \varepsilon(i+1) \geq 1\\
\end{cases}
\end{equation*}

Then, \(\prod U_i\) is open in the product topology and equal to \(B(\vec{x}, \varepsilon)\).
Therefore, the topology generated by \(D\) is coarser than the product topology
and the two topologies are equal.
\end{proof}

\begin{proof}[{Statement 2}]
First suppose that \(\vec{x} \in \closure(\mathbb{R}^{oo})\) in the uniform topology.
Consider arbitrary \(\varepsilon > 0\) (with \(\varepsilon < 1\)) and \(B(\vec{x}, \varepsilon)\) which is open.
Then, \(\vec{y} \in \mathbb{R}^{oo} \cap B(\vec{x}, \varepsilon) \neq \varnothing\).
Let \(N\) be such that \(y_i = 0\) for all \(i > N\).
Then,

\begin{equation*}
\forall i \geq N+1: |x_i| = |x_i - y_i| \leq \overbar{\rho}(\vec{x}, \vec{y}) < \varepsilon
\end{equation*}

Since the choice of \(\varepsilon\) was arbitrary, we have that \(\lim_{i\to \infty}x_i = 0\).

Conversely, suppose that \(\lim_{i\to \infty}x_i = 0\). We want to show
that \(\vec{x} \in \closure(\mathbb{R}^{oo})\). Consider arbitrary open ball
\(B(\vec{y}, \varepsilon)\) containing \(\vec{x}\) with \(\varepsilon < 1\).
Let \(N > 0\) be such that for all \(i \geq N\), \(|x_i| < \varepsilon - \overbar{\rho}(\vec{x}, \vec{y})\).
Then, define

\begin{equation*}
z_i = \begin{cases}
x_i, &\quad\text{if } i < N\\
0, &\quad\text{if } i \geq N\\
\end{cases}
\end{equation*}

Then, \(\vec{z} \in \mathbb{R}^{oo}\) and

\begin{equation*}
|y_i - z_i| = \begin{cases}
|y_i - x_i| \leq \overbar{\rho}(\vec{x}, \vec{y}) < \varepsilon
,&\quad\text{if } i \leq N\\
|y_i| \leq |y_i-x_i| + |x_i| < \overbar{\rho}(\vec{x}, \vec{y}) + \varepsilon - \overbar{\rho}(\vec{x}, \vec{y})
= \varepsilon
,&\quad\text{if } i \geq N\\
\end{cases}
\end{equation*}

Hence \(\overbar{\rho}(\vec{y}, \vec{z}) < \varepsilon\) and \(\vec{z} \in \mathbb{R}^{oo} \cap B(\vec{y}, \varepsilon)\).
Since the choice of \(B(\vec{y}, \varepsilon)\) containing \(\vec{x}\) was arbitrary,
we have that \(\vec{x} \in \closure(\mathbb{R}^{oo})\).
\end{proof}

\begin{proof}[{Statement 3}]
Note that \(D(\vec{x}, \vec{y}) \leq \overbar{\rho}(\vec{x}, \vec{y})\). Therefore,
the uniform topology is finer than the product topology.
Furthermore, since the closures of \(\mathbb{R}^{oo}\) differ, the topologies must be distinct.

Now, notice that any open ball \(B_{\overbar{\rho}}(\vec{x}, \varepsilon)\) may be written as
the product of open intervals \((x_i-\varepsilon, x_i+\varepsilon)\).
Hence, \(B_{\overbar{\rho}}(\vec{x}, \varepsilon)\) is open in the box topology
and the box topology is finer than the uniform topology.
Furthermore, since the closures of \(\mathbb{R}^{oo}\) differ, the topologies must be distinct.
\end{proof}
\end{proposition}

\section{Metric defintions of topological properties}
\label{develop--math6620:special-topologies:page--metric.adoc---metric-defintions-of-topological-properties}

Previously, we defined the following properties using topological properties only.
However, we want to show that these definitions agree with their usual definitions in metric spaces

\begin{description}
\item[Continuity] Let \((X, d_X)\) and \((Y, d_Y)\) be metric spaces.
    A function \(f: X \to Y\) is continuous iff
    
    \begin{equation*}
    \forall \varepsilon > 0: \forall x \in X: \exists \delta > 0: d_X(x, y) < \delta \implies d_Y(f(x), f(y)) < \varepsilon
    \end{equation*}
\item[Convergence] Let \((X, d)\) be a metric space and \(\{x_n\}\) be a sequence and \(x \in X\).
    Then the sequence \myautoref[{converges}]{develop--math6620:ROOT:page--interior-closure.adoc---convergence} to \(x\)
    iff
    
    \begin{equation*}
    \forall \varepsilon > 0: \exists N \geq 0: \forall n \geq N: d(x, x_n) < \varepsilon
    \end{equation*}
\end{description}

\begin{lemma}[{The sequence lemma}]
Let \(X\) be a topological space and \(A \subseteq X\).
If there is a sequence of points in \(A\)
which converges to \(x\) then \(x \in \closure A\).

The converse is true if \(X\) is metrizable.

\begin{proof}[{}]
The first statement is clearly true by the topological definition of convergence.

Now, suppose that \(X\) is metrizable with metric \(d\). Then, we can define
the sequence of \(x_n\) where \(d(x_n, x) < \frac{1}{n}\).
Since \(x \in \closure A\), we know that \(B\left(x, \frac{1}{n}\right) \cap A \neq \varnothing\)
and hence each \(x_n\) exists.
\end{proof}
\end{lemma}

A counter example of the converse of the above lemma is \(\mathbb{R}\) with the co-countable
topology and \(A = \mathbb{R} - \{0\}\). Then, \(\closure(A) = \mathbb{R}\);
however there exists no sequence \(\{x_n\} \subseteq A\) which converges to \(0\).
The arises from the fact that for each sequence, there exists an open set which contains no elements of the sequence.

\begin{admonition-tip}[{}]
The sequence lemma may be used to prove that a particular space is not metrizable.
\end{admonition-tip}

\begin{theorem}[{}]
Let \(f: X \to Y\) be a function where \(X\) is a metric space and \(Y\)
is a topological space.
Then, the function \(f\) is continuous iff
every convergent sequence \(\{x_n\}\) which converges
to \(x\), we have that \(\{f(x_n)\}\) converges to \(f(x)\).

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof is from the notes.
\end{admonition-note}

First suppose that \(f\) is continuous and consider the sequence
\(\{x_n\}\) which converges to \(x\). Now, consider an open set
\(V\) containing \(f(x)\). Then, by the continuity of \(f\),
\(f^{-1}(V)\) is open in \(X\) and there exists an \(N > 0\)
such that for each \(n \geq N\), \(x_n \in f^{-1}(V)\)
and hence \(f(x_n) \in V\). Therefore, \(f(x_n)\) converges to \(f(x)\).

Conversely suppose that for
every convergent sequence \(\{x_n\}\) which converges
to \(x\), we have that \(\{f(x_n)\}\) converges to \(f(x)\).
Recall that an equivalent definition of continuous is that for each set \(A \subseteq X\),

\begin{equation*}
f(\closure_X A) \subseteq \closure_Y f(A)
\end{equation*}

Also, from a
\myautoref[{previous lemma}]{develop--math6620:special-topologies:page--metric.adoc--necessary-and-sufficient-closure}, \(z \in \closure_Z B\)
iff there is a sequence of points in \(B\) which converges to \(z\).

So, consider arbitrary \(x \in \closure_X A\). We want to show that
\(f(x) \in \closure_Y f(A)\).
Since \(x \in \closure_X A\), there exists
\(\{x_n\}\) which is contained in \(A\) and converges to \(x\).
So, consider \(f(x)\). By hypothesis, \(\{f(x_n)\}\)
converges to \(f(x)\). Also, \(f(x_n)\) is contained in \(f(A)\).
Therefore, \(f(x) \in \closure_Y f(A)\) as desired.
Since the choice of \(A\) was arbitrary, \(f\) is continuous.
\end{proof}
\end{theorem}

\subsection{Uniform Convergence}
\label{develop--math6620:special-topologies:page--metric.adoc---uniform-convergence}

Let \(X\) be a topological space and \(Y\) be a metric space.
Let \(\{f_n\}\) be a sequence of functions from \(X\) to \(Y\) and \(f:X\to Y\).
Then, we say that \(\{f_n\}\) converges to \(f\) \emph{uniformly}
if

\begin{equation*}
\forall \varepsilon > 0: \exists N > 0: \forall x \in X: d(f_n(x), f(x)) < \varepsilon
\end{equation*}

\begin{theorem}[{}]
Let \(X\) be a topological space and \(Y\) be a metric space.
If \(\{f_n\}\) is a set of continuous functions from \(X\) to \(Y\)
which converge uniformly to \(f\), then \(f\) is continuous

\begin{proof}[{}]
Consider arbitrary \(B(f(x), \varepsilon) \subseteq Y\) and
\(y \in f^{-1}(B(f(x), \varepsilon))\).
We want to find an open set containing \(y\) which is contained in \(f^{-1}(B(f(x), \varepsilon))\).

Since \(f_n \to f\) uniformly, there exists \(n > 0\) such that

\begin{equation*}
\forall x \in X: d(f_n(x), f(x)) < \frac{\varepsilon}{3}
\end{equation*}

Also, by the continuity of \(f_n\), \(V = f_n^{-1}\left(B\left(f_n(y), \frac{\varepsilon}{3}\right)\right)\)
is open. Then,

\begin{equation*}
\forall z \in V:
d(f(x), f(y)) \leq d(f(x), f_n(x)) + d(f_n(x), f_n(y)) + d(f_n(y), f(y)) < 3\frac{\varepsilon}{3} = \varepsilon
\implies z \in f^{-1}(B(f(x), \varepsilon))
\end{equation*}

Therefore, \(y \in V \subseteq f^{-1}(B(f(x), \varepsilon))\).
Since the choice of \(y\) was arbitrary, \(f^{-1}(B(f(x), \varepsilon))\)
is open and \(f\) is continuous.
\end{proof}
\end{theorem}
\end{document}
