\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Optimization}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Dynamic programming is the method of reducing a problem into smaller sub problems which can be solved
in a recursive manner.
This is usually done using the idea of \emph{stages} where the result of current stage depends on the result
of the previous one.
Clearly, this idea naturally leads itself to a computer programming paradigm.

\section{Integer programming with a single constraint (Knapsack problem)}
\label{develop--math6194:ROOT:page--optimization.adoc---integer-programming-with-a-single-constraint-knapsack-problem}

Consider the following problem

\begin{equation*}
\begin{aligned}
\text{maximize } &\sum_{i=1}^N u_i(x_i)\\
\text{given } & \sum_{i=1}^n w_i(x_i) \leq M, \ x_i \in \mathbb{Z}^+ \cup \{0\}
\end{aligned}
\end{equation*}

where the utility function, \(u_i\), and the weight function, \(w_i\), are strictly increasing.
We may solve this problem iteratively by iteratively introducing the variables.
To do this, we use the function \(f_k: \mathbb{R}^* \to \mathbb{R}^*\) which is defined by

\begin{equation*}
f_k(m) = \max \left\{\sum_{i\leq k} u_i(x_i) \ \middle| \ \sum_{i \leq k} w_i(x_i) = m\right\}
\end{equation*}

This function may be thought of the maximum attainable utility given that we
have a weight of exactly \(m\).
Note that

\begin{itemize}
\item \(f_0(m) = 0\ \forall m\geq 0\)
\item \(f_k\) need not be increasing
\item \(f_k\) need not be defined everywhere
\end{itemize}

Because of the final property, we need only represent \(f_k\) at the values at which it is defined.
Let \(S_k\) be the values of \(m\) for which \(f_k\) is defined (including \(0\)).
Also, note that by default \(f_k\) does not specify the value(s) of \(x_i\) which attain the maximum
value, but we can implement bookkeeping if needed.

The idea of the algorithm is to develop \(f_{k+1}\) using \(f_k\).
The steps are as follows

\begin{enumerate}[label=\arabic*)]
\item Initialize \(f_0 = 0\) and \(S_0 = \{0\}\)
\item Initialize \(k=0\)
\item If \(k = N\), evaluate \(\max f_k\) and stop.
\item Determine \(S_{k+1}\) and \(f_{k+1}\) as follows
    
    \begin{equation*}
    S_{k+1} = \left\{m + w_{k+1}(x_{k+1}) \ \middle| \ m \in S_k,\ x_{k+1} \in \mathbb{Z}^+ \cup \{0\}, \ m+w_{k+1}(x_{k+1}) \leq M\right\}
    \end{equation*}
    
    and
    
    \begin{equation*}
    f_{k+1}(m) = \max \left\{ f_k(a) + u_{k+1}(x_{k+1}) \ \middle| \ a \in S_k, \ x_{k+1} \in \mathbb{Z}^+ \cup \{0\},\ a+w_{k+1}(x_{k+1}) = m\right\}
    \end{equation*}
    
    Note that we only need to compute \(f_{k+1}\) for values in \(S_{k+1}\)
\item Increment \(k\) and go to step 3
\end{enumerate}

Note that this algorithm is \(O(M^2N)\) if the number of values of \(x_k\) which satisfy \(w_{k}(x_k) \leq M\)
is proportional to \(M\).

\begin{proof}[{Validity of algorithm}]
We will now prove this algorithm inductively.
We need only prove that our calculations for \(f_{k}\) and \(S_{k}\) are correct.
The base case is trivially true.
So, suppose that we have \(f_k\) and \(S_k\) which are accurate.
Clearly \(S_{k+1}\) is accurate since

\begin{equation*}
\begin{aligned}
S_{k+1}
&= \left\{\sum_{i\leq k+1} w_i(x_i) \leq M\right\}
\\&= \left\{\sum_{i\leq k} w_i(x_i) + w_{k+1}(x_{k+1}) \leq M\right\}
\\&= \left\{m + w_{k+1}(x_{k+1}) \leq M \ \middle| \ m \in S_k, \ x_{k+1} \geq 0\right\}
\end{aligned}
\end{equation*}

Next the definition of \(f_{k+1}\). We need only care about the values
\(m \in S_{k+1}\)

\begin{equation*}
\begin{aligned}
f_{k+1}(m)
&= \max \left\{\sum_{i\leq k+1} u_i(x_i) \ \middle| \ \sum_{i \leq k+1} w_i(x_i) = m\right\}
\\&= \max \left\{\sum_{i\leq k} u_i(x_i)  + u_{k+1}(x_{k+1})\ \middle| \ \sum_{i \leq k} w_i(x_i) + w_{k+1}(x_{k+1}) = m\right\}
\\&= \max \left\{f_k(a)  + u_{k+1}(x_{k+1})\ \middle| \ a \in S_k, \ a  + w_{k+1}(x_{k+1}) = m\right\}
\end{aligned}
\end{equation*}

Therefore by the principle of mathematical induction, we have proven the desired result.
\end{proof}

\subsection{Example: Prepper}
\label{develop--math6194:ROOT:page--optimization.adoc---example-prepper}

\begin{admonition-tip}[{}]
Definition of prepper: \url{https://www.merriam-webster.com/dictionary/prepper}

\begin{custom-quotation}[{Merrian-Webster Dictionary}][{}]
A person who gathers materials and makes plans in preparation for surviving a major disaster or cataclysm (such as worldwide economic collapse or war)
\end{custom-quotation}
\end{admonition-tip}

A prepper can fill his \emph{bug-out bag} with cans of soup, biscuits and guidebooks.
Each kind of object has a known weight and a certain `utility' for the journey.
Let \(W=10\) be the total weight he can carry.
Then, if we have three classes of objects \(P_1\), \(P_2\) and \(P_3\),
our problem solve the following problem

\begin{equation*}
\begin{aligned}
\text{maximize }&u_1x_1 + u_2x_2 + u_3x_3\\
\text{given that } & w_1 x_1 + w_2 x_2 + w_3x_3 = 10
\end{aligned}
\end{equation*}

where

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(i\)} & {1} & {2} & {3} \\
\hline
{\(w_i\)} & {4} & {3} & {1} \\
\hline
{\(u_i\)} & {5} & {2} & {1} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

So, we can use the algorithm.
Note that we can skip the first iteration

\begin{table}[H]\centering
\caption{Stage 1}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(x_1\)} & {0} & {1} & {2} \\
\hline
{\(m\)} & {0} & {4} & {8} \\
\hline
{\(f_1(m)\)} & {0} & {5} & {10} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

For stage 2, we consider the set of values of \(m\) and \(x_2\)

\begin{table}[H]\centering
\caption{Stage 2 intermediary}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(m\)} & \SetCell[r=1, c=4]{}{0} &  &  &  & \SetCell[r=1, c=3]{}{4} &  &  & {8} \\
\hline
{\(x_2\) st \(m + w_2(x_2) \leq 10\)} & {0} & {1} & {2} & {3} & {0} & {1} & {2} & {0} \\
\hline
{\(m+w_2(x_2)\)} & {0} & {3} & {6} & {9} & {4} & {7} & {10} & {8} \\
\hline
{\(f_1(m) + u_2(x_2)\)} & {0} & {2} & {4} & {6} & {5} & {7} & {9} & {10} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{table}[H]\centering
\caption{Stage 2}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(m\)} & {0} & {3} & {4} & {6} & {7} & {8} & {9} & {10} \\
\hline
{\(f_2(m)\)} & {0} & {2} & {5} & {4} & {7} & {10} & {6} & {9} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

For stage 3, we only care about \(m+w_3(x_3) = 10\)

\begin{table}[H]\centering
\caption{Stage 3}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(m\)} & {0} & {3} & {4} & {6} & {7} & {8} & {9} & {10} \\
\hline
{\(x_3\) st \(m + w_3(x_3) = 10\)} & {10} & {7} & {6} & {4} & {3} & {2} & {1} & {0} \\
\hline
{\(f_2(m) + u_3(x_3)\)} & {10} & {9} & {9} & {8} & {3} & {12} & {7} & {9} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Therefore, \(f_3(10) = 12\).
By looking at the choices, we see that \(x_3=2\), \(x_2=0\) and \(x_1=2\).

\section{Summing problem}
\label{develop--math6194:ROOT:page--optimization.adoc---summing-problem}

Suppose that we have a set of positive integers \(a_1, \ldots a_n\).
We will like to know if it is possible to chose
\(x_i \in \{0,1\}\) such that

\begin{equation*}
S = a_1x_1 + a_2x_2 + \cdots + a_nx_n
\end{equation*}

for some fixed value \(S\) and possibly determine the set of values of \(x_i\) for which
the desired sum is attained.
This is a \emph{hard} problem in general and finding a solution is typically \(O(2^n)\).
However, there is a special class of \(\{a_i\}\) such that the problem is trivial.

The sequence \(a_1, a_2, \ldots\) is called a \emph{super-increasing} sequence if

\begin{equation*}
\forall j \geq 1: \quad \sum_{i=1}^j a_i < a_{j+1}
\end{equation*}

A common example of a super-increasing sequence is the powers of \(2\).

Once we know that our sequence is super-increasing, the problem is easy to solve.
We start with \(a_n\). If \(S \geq a_n\), then \(x_n\) must be \(1\),
otherwise there is no way that the smaller \(a_i\) can sum to \(S\).
If \(S < a_n \), \(x_n=0\).
Then, we repeat the process by replacing \(S\) with \(S-a_nx_n\)
and looking at \(a_{n-1}\) instead.

\subsection{Merkel-Hellman System for Public Key Cryptography}
\label{develop--math6194:ROOT:page--optimization.adoc---merkel-hellman-system-for-public-key-cryptography}

\begin{admonition-important}[{}]
This system was proven to be insecure by Shamir (of RSA fame).
Still, it is conceptually interesting.
\end{admonition-important}

Suppose that Alice wants to provide a way for Bob to send her secret messages
without worrying about sniffing attacks.
She can use the Merkel-Hellman system.

\begin{enumerate}[label=\arabic*)]
\item First, she generates a private key which consists of
    
    \begin{itemize}
    \item A super-increasing sequence \(a_1, \ldots a_n\)
    \item Co-prime integers \(p,q\) such that \(q > \sum_{i=1}^n a_i\)
    \end{itemize}
    
    Let \(f(x) = px \mod q\) which is linear and since \(gcd(p,q)=1\), \(f^{-1}\) exists.
    Her public key consists of integers \(b_i = f(a_i)\).
\item Once Bob has Alice's public key, he encodes his message \(M=(x_1, \ldots x_n)\) (possibly padded, bits of \(M\))
    as \(E = \sum_{i=1}^n b_ix_i\) and sends \(E\) to Alice.
\item Alice can then decode Bob's message by applying \(f^{-1}\) to \(E\) and solving
    \(\sum_{i=1}^n a_i x_i = f^{-1}(E)\) for the \(x_i\), which is easy to solve since the \(a_i\) are super increasing.
\end{enumerate}

The key things to note are

\begin{itemize}
\item The public key \(b_1, \ldots b_n\) looks random and is not unique to \(a_1, \ldots a_n\).
    So, we cannot easily recover the private key from the public key
\item Since \(f\) and its inverse \(f^{-1}(x) = p^{-1}x \mod q \) are linear,
    
    \begin{equation*}
    \begin{aligned}
    f^{-1}(E)
    &= f^{-1}\left(\sum_{i=1}^n b_i x_i\right)
    \\&= p^{-1}\sum_{i=1}^n b_i x_i \mod q
    \\&= \sum_{i=1}^n (p^{-1}b_i \mod q) x_i \mod q
    \\&= \sum_{i=1}^n a_i x_i \mod q
    \\&= \sum_{i=1}^n a_i x_i
    \end{aligned}
    \end{equation*}
\end{itemize}

\begin{figure}[H]\centering
\includegraphics[width=0.95\linewidth]{images/develop-math6194/ROOT/merkel-hellman}
\caption{Illustration of Merkel-Hellman system}
\end{figure}

\subsection{Shamir's algorithm for solving the ``hard'' system.}
\label{develop--math6194:ROOT:page--optimization.adoc---shamirs-algorithm-for-solving-the-latex-backtick-latex-backtickhard-latex-quote-latex-quote-system}

The problem with the Merkel-Hellman system is that the assumption which it based on is incorrect.
While solving

\begin{equation*}
E = \sum_{i=1}^n b_i x_i
\end{equation*}

may look hard and the naive algorithm is \(O(2^n)\), it can actually be solved in polynomial time.
The key observation is that we do not care about the original private key, we only need to find
some other \(p', q'\) such that \((p')^{-1} b_i \mod q\) forms a super-increasing sequence.
Shamir prove that finding these \(p'\) and \(q'\) can be done in polynomial time
in \url{https://ieeexplore.ieee.org/document/4568386} .
\end{document}
