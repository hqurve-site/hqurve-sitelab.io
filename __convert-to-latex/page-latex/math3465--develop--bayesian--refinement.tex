\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Parameter Refinement}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Consider parameter \(\theta\) with distribution \(\pi(\theta)\). Then
we define the following

\begin{description}
\item[Prior distribution] the initial distribution of \(\theta\)
\item[Posterior distribution] the refined distribution of \(\theta\) after observations are made
\end{description}

\section{Finding the Posterior distribution}
\label{develop--math3465:bayesian:page--refinement.adoc---finding-the-posterior-distribution}

Let the prior distribution of \(\theta\) be \(\pi(\theta)\) and suppose
we made some observation \(\vec{x}\) with joint pdf \(f_n(\vec{x}| \theta)\).
Then using \myautoref[{Bayes' theorem}]{develop--math2274:ROOT:page--conditional-probability.adoc---bayes-theorem}
(in reality, \myautoref[{conditional pdfs}]{develop--math3278:distribution-theory:page--several-random-variables.adoc---conditional-density}),
than posterior distribution of \(\pi(\theta)\) is

\begin{equation*}
\pi(\theta | \vec{x})
= \frac{f(\vec{x}, \theta)}{f_{\vec{X}}(\vec{x})}
= \frac{f(\vec{x}, \theta)}{\int_{\mathbb{R}} f(\vec{x},\theta_1) \ d\theta_1}
= \frac{\pi(\theta)f_n(\vec{x}| \theta)}{\int_{\mathbb{R}} \pi(\theta_1)f_n(\vec{x}|\theta_1) \ d\theta_1}
\end{equation*}

where \(f\) is the joint pdf of \(\theta\) and \(\vec{x}\).

\begin{admonition-note}[{}]
The equation shown here is generalizable even if \(\theta\) is a vector. We only
took it to be a single real variable since it makes writing the integral (marginally easier).
In general, the integral would be taken over the respective space \(\Omega\).
\end{admonition-note}

\subsection{Order of applying information}
\label{develop--math3465:bayesian:page--refinement.adoc---order-of-applying-information}

One would expect that when presented with two sets of information at once, it should
not matter in what order we supply the information to the distribution nor if
we supply both at once. Although just from the formulation of the posterior distribution (along with definitions
of marginal and conditional pdfs) we expect this to be the case, but it would also be nice to show it manually.

More concretely, if we have have information \(\vec{x}\) and \(\vec{y}\)

\begin{equation*}
\pi(\theta | (\vec{x}, \vec{y})) = \pi(\theta | \vec{x} | \vec{y}) = \pi(\theta | \vec{y} | \vec{x})
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-remark}[{}]
Again, there is no real reason to do this since we are using formulations.
The statement we are proving is general to distribution theory.
\end{admonition-remark}

Let \(f\) be the joint pdf of \(\vec{X}\) and \(\vec{Y}\) given \(\theta\), and \(\theta\). Then

\begin{equation*}
\begin{aligned}
\pi(\theta | \vec{x} | \vec{y})
&= \dfrac{\pi(\theta | \vec{x})f(\vec{y} | \theta | \vec{x})}{\int_{\mathbb{R}} \pi(\theta_1 | \vec{x}) f(\vec{y} | \theta_1 | \vec{x}) \ d\theta_1 }
\\&= \dfrac{
        \frac{\pi(\theta)f(\vec{x} | \theta)}{\int_{\mathbb{R}} \pi(\theta_2)f(\vec{x}|\theta_2) \ d\theta_2}f(\vec{y} | \theta | \vec{x})
    }{
        \int_{\mathbb{R}} \frac{\pi(\theta_1)f(\vec{x} | \theta_1)}{\int_{\mathbb{R}} \pi(\theta_2)f(\vec{x}|\theta_2) \ d\theta_2} f(\vec{y} | \theta_1 | \vec{x}) \ d\theta_1
    }
\\&= \dfrac{
        \frac{\pi(\theta)f(\vec{x} | \theta)}{\int_{\mathbb{R}} \pi(\theta_2)f(\vec{x}|\theta_2) \ d\theta_2}
        \frac{f((\vec{y} | \theta), (\vec{x} | \theta))}{f(\vec{x} | \theta)}
    }{
        \int_{\mathbb{R}}
            \frac{\pi(\theta_1)f(\vec{x} | \theta_1)}{\int_{\mathbb{R}} \pi(\theta_2)f(\vec{x}|\theta_2) \ d\theta_2}
            \frac{f((\vec{y} | \theta_1), (\vec{x} | \theta_1))}{f(\vec{x} | \theta_1)}
        \ d\theta_1
    }
\\&= \dfrac{
        \frac{\pi(\theta)f(\vec{x} | \theta)}{\int_{\mathbb{R}} \pi(\theta_2)f(\vec{x}|\theta_2) \ d\theta_2}
        \frac{f(\vec{x} , \vec{y} | \theta)}{f(\vec{x} | \theta)}
    }{
        \int_{\mathbb{R}}
            \frac{\pi(\theta_1)f(\vec{x} | \theta_1)}{\int_{\mathbb{R}} \pi(\theta_2)f(\vec{x}|\theta_2) \ d\theta_2}
            \frac{f(\vec{x},\vec{y} | \theta_1)}{f(\vec{x} | \theta_1)}
        \ d\theta_1
    }
\\&= \dfrac{
        \pi(\theta)f(\vec{x} , \vec{y} | \theta)
    }{
        \int_{\mathbb{R}}
            \pi(\theta_1) f(\vec{x},\vec{y} | \theta_1)
        \ d\theta_1
    }
\\&= \pi(\theta | (\vec{x},\vec{y}))
\end{aligned}
\end{equation*}

And we are done since showing the other equation is analogous.
\end{example}

\section{Conjugate Families}
\label{develop--math3465:bayesian:page--refinement.adoc---conjugate-families}

Let \(\pi(\theta)\) be the prior distribution and \(X\) be a random variable.
Then if the posterior distribution \(\pi(\theta | x)\) belongs
to the same \emph{family} as \(\pi(\theta)\), we call \(\pi(\theta)\)
and \(\pi(\theta | x)\) \emph{conjugate distributions} and
\(\pi(\theta)\) is the \emph{conjugate prior} for \(f(x | \theta)\).

\subsection{Beta and Bernoulli}
\label{develop--math3465:bayesian:page--refinement.adoc---beta-and-bernoulli}

The beta distribution is self conjugate for the Bernoulli distribution.
In particular if we have a prior with distribution \(\theta \sim Beta(\alpha, \beta)\)
and \(X_1, \ldots X_n\) are iid \(Bernoulli(\theta)\) then our posterior
is

\begin{equation*}
(\theta | \vec{X}) \sim Beta\left(\alpha + \sum_{i=1}^n x_i, \beta + n - \sum_{i=1}^n x_i\right)
\end{equation*}

\begin{example}[{Proof}]
Firstly, their joint pdf is given by

\begin{equation*}
\begin{aligned}
f(\theta, \vec{x})
&= \pi(\theta)\prod_{i=1}^n \theta^{x_i}(1-\theta)^{1-x_i}
\\&= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)}\theta^{\alpha -1}(1-\theta)^{\beta - 1}\theta^{\sum_{i=1}^n x_i}(1-\theta)^{n- \sum_{i=1}^n x_i}
\\&= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)}\theta^{\alpha -1 + \sum_{i=1}^n x_i}(1-\theta)^{\beta - 1 + n- \sum_{i=1}^n x_i}
\end{aligned}
\end{equation*}

Let \(y = \sum_{i=1}^n x_i\).
Then the marginal of \(\vec{x}\) is

\begin{equation*}
\begin{aligned}
f(\vec{x})
&= \int_{\mathbb{R}} f(\theta, \vec{x}) \ d\theta
\\&= \int_{\mathbb{R}}\frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)}\theta^{\alpha -1 + y}(1-\theta)^{\beta - 1 + n- y} \ d\theta
\\&= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)} \frac{\Gamma(\alpha + y)\Gamma(\beta + n - y)}{\Gamma(\alpha + \beta + n)}
    \int_{\mathbb{R}}\frac{\Gamma(\alpha + \beta + n)}{\Gamma(\alpha + y) \Gamma(\beta + n -y)}\theta^{\alpha -1 + \sum_{i=1}^n x_i}(1-\theta)^{\beta - 1 + n- \sum_{i=1}^n x_i} \ d\theta
\\&= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)} \frac{\Gamma(\alpha + y)\Gamma(\beta + n - y)}{\Gamma(\alpha + \beta + n)}
\end{aligned}
\end{equation*}

since the integral is the total probability of a beta.
Then,

\begin{equation*}
\begin{aligned}
\pi(\theta | \vec{x})
&= \frac{
        f(\theta, \vec{x})
    }{
        f(\vec{x})
    }
\\&= \frac{
        \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)}\theta^{\alpha -1 + y}(1-\theta)^{\beta - 1 + n- y}
    }{
        \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)} \frac{\Gamma(\alpha + y)\Gamma(\beta + n - y)}{\Gamma(\alpha + \beta + n)}
    }
\\&= \frac{\Gamma(\alpha + \beta + n)}{\Gamma(\alpha + y)\Gamma(\beta + n -y)} \theta^{\alpha -1 + y}(1-\theta)^{\beta - 1 + n- y}
\\&= \frac{\Gamma(\alpha + \beta + n)}{\Gamma\left(\alpha + \sum_{i=1}^n x_i \right)\Gamma\left(\beta + n - \sum_{i=1}^n x_i\right)}
        \theta^{\alpha -1 + \sum_{i=1}^n x_i}(1-\theta)^{\beta - 1 + n- \sum_{i=1}^n x_i}
\end{aligned}
\end{equation*}
\end{example}

\begin{admonition-tip}[{}]
A \(Beta(1,1)\) distribution is uniform and hence perfect if you know nothing about a given probability.
\end{admonition-tip}

\subsection{Normal}
\label{develop--math3465:bayesian:page--refinement.adoc---normal}

The normal distribution is self conjugate for itself.
In particular, if we have a prior distribution of \(\theta \sim N(\mu, \nu^2)\)
and \(X_1, \ldots X_n\) are iid \(N(\theta, \sigma^2)\), then our posterior is

\begin{equation*}
(\theta | \vec{X}) \sim N\left(\frac{\mu\sigma^2 + n\overbar{x}\nu^2}{\sigma^2 + n\nu^2},\frac{\nu^2\sigma^2}{\sigma^2 + n\nu^2}\right)
\end{equation*}

\begin{example}[{Proof}]
Firstly, the joint distribution of \(\vec{X}\) and \(\theta\)

\begin{equation*}
\begin{aligned}
f(\theta, \vec{x})
&= \frac{1}{\sqrt{2\pi\nu^2}}\exp\left\{\frac{-(\theta - \mu)^2}{2\nu^2}\right\}
    \prod_{i=1}^n \frac{1}{\sqrt{2\pi \sigma^2}} \exp\left\{\frac{-(x_i - \theta)^2}{2\sigma^2}\right\}
\\&= \frac{1}{\sqrt{(2\pi)^2\nu^2\sigma^{2n}}}\exp\left\{\frac{-(\theta - \mu)^2}{2\nu^2} - \frac{\sum_{i=1}^n (x_i - \theta)^2}{2\sigma^2}\right\}
\\&= \frac{1}{\sqrt{(2\pi)^2\nu^2\sigma^{2n}}}\exp\left\{\frac{-(\theta - \mu)^2}{2\nu^2} - \frac{\sum_{i=1}^n (x_i - \overbar{x})^2 + n(\overbar{x} - \theta)^2}{2\sigma^2}\right\}
\\&= \frac{1}{\sqrt{(2\pi)^2\nu^2\sigma^{2n}}}\exp\left\{
        -\frac{1}{2}\left[
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\theta^2
            -2\left(\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}\right)\theta
            + \frac{\mu^2}{\nu^2} + \frac{n\overbar{x}^2}{\sigma^2}
            + \frac{\sum_{i=1}^n (x_i - \overbar{x})^2}{\sigma^2}
        \right]
        \right\}
\\&= \frac{1}{\sqrt{(2\pi)^2\nu^2\sigma^{2n}}}\exp\left\{
        -\frac{1}{2}\left[
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\left(\theta - \frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2
            - \frac{\left(\frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}
            + \frac{\mu^2}{\nu^2} + \frac{n\overbar{x}^2}{\sigma^2}
            + \frac{\sum_{i=1}^n (x_i - \overbar{x})^2}{\sigma^2}
        \right]
        \right\}
\\&= \frac{1}{\sqrt{(2\pi)^2\nu^2\sigma^{2n}}}\exp\left\{
        -\frac{1}{2}
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\left(\theta - \frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2
        \right\}
    \\&\hspace{10em}
    \exp\left\{-\frac{1}{2}
        \left[
            - \frac{\left(\frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}
            + \frac{\mu^2}{\nu^2} + \frac{n\overbar{x}^2}{\sigma^2}
            + \frac{\sum_{i=1}^n (x_i - \overbar{x})^2}{\sigma^2}
        \right]
        \right\}
\\&= k(\vec{x})\exp\left\{
        -\frac{1}{2}
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\left(\theta - \frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2
        \right\}
\end{aligned}
\end{equation*}

Where we hide that mess behind \(k\) for now. Then, the marginal distribution of \(\vec{x}\) is

\begin{equation*}
f(\vec{x}) = \int_{\mathbb{R}} k(\vec{x}) \exp\left\{
        -\frac{1}{2}
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\left(\theta - \frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2
        \right\} \ d\theta
=k(\vec{x}) \sqrt{2\pi\frac{1}{\left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)}}
\end{equation*}

And hence the conditional distribution of \(\theta\) is

\begin{equation*}
\begin{aligned}
\pi(\theta | \vec{x})
&= \frac{
    k(\vec{x})\exp\left\{
        -\frac{1}{2}
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\left(\theta - \frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2
        \right\}
    }{k(\vec{x}) \sqrt{2\pi\frac{1}{\left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)}}}
\\&= \frac{1}{\sqrt{2\pi\frac{1}{\left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)}}}
    \exp\left\{
        -\frac{1}{2}
            \left(\frac{1}{\nu^2} + \frac{n}{\sigma^2}\right)\left(\theta - \frac{\frac{\mu}{\nu^2} + \frac{n\overbar{x}}{\sigma^2}}{\frac{1}{\nu^2} + \frac{n}{\sigma^2}}\right)^2
        \right\}
\\&= \frac{1}{\sqrt{2\pi\frac{1}{\left(\frac{\sigma^2 + n\nu^2}{\nu^2\sigma^2}\right)}}}
    \exp\left\{
        -\frac{1}{2}
            \left(\frac{\sigma^2 + n\nu^2}{\nu^2\sigma^2}\right)\left(\theta - \frac{\mu\sigma^2 + n\overbar{x}\nu^2}{\sigma^2 + n\nu^2}\right)^2
        \right\}
\\&= \frac{1}{\sqrt{2\pi\frac{\nu^2\sigma^2}{\sigma^2 + n\nu^2}}}
    \exp\left\{
        -\frac{1}{2\frac{\nu^2\sigma^2}{\sigma^2 + n\nu^2}}
            \left(\theta - \frac{\mu\sigma^2 + n\overbar{x}\nu^2}{\sigma^2 + n\nu^2}\right)^2
        \right\}
\end{aligned}
\end{equation*}

And hence we are done
\end{example}

\subsection{Normal-Gamma}
\label{develop--math3465:bayesian:page--refinement.adoc---normal-gamma}

We define the precision of a variable with normal distribution to be \(\tau = \frac{1}{\sigma^2}\)

If we have random variables \(\mu\) and \(\tau\) such that

\begin{equation*}
(\mu| \tau) \sim N\left(\mu_0, \frac{1}{\lambda_0\tau}\right)
\quad\text{and}\quad
\tau \sim \Gamma\left(\alpha_0, \frac{1}{\beta_0}\right)
\end{equation*}

and \(X_1, \ldots X_n\) are iid \(N\left(\mu, \frac{1}{\tau}\right)\). Then,

\begin{equation*}
(\mu | \tau | \vec{x}) \sim N\left(\frac{\lambda_0 \mu_0 + n\overbar{x}}{\lambda_0 + n}, \frac{1}{\tau(\lambda_0 + n)}\right)
\quad\text{and}
(\tau | \vec{x}) \sim \Gamma\left(\alpha_0 + \frac{n}{2}, \cfrac{1}{\beta_0 + \frac{1}{2}S^2 + \frac{\lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)}}\right)
\end{equation*}

Equivalently

\begin{equation*}
(\mu | \tau | \vec{x}) \sim N\left(\mu_1, \frac{1}{\lambda_1\tau}\right)
\quad\text{and}
(\tau | \vec{x}) \sim \Gamma\left(\alpha_1, \cfrac{1}{\beta_1}\right)
\end{equation*}

where

\begin{equation*}
\begin{aligned}
&\mu_1 = \frac{\lambda_0 \mu_0 + n\overbar{x}}{\lambda_0 + n}
&\quad
& \lambda_1 = \lambda_0 + n
\\
&\alpha_1 = \alpha_0 + \frac{n}{2}
&\quad& \beta_1 = \beta_0 + \frac{1}{2}S^2 + \frac{\lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)}
\end{aligned}
\end{equation*}

and \(S^2 = \sum_{i=1}^n (x_i - \overbar{x})\).

\begin{example}[{Proof}]
Firstly, the joint pdf of \(\theta = (\mu, \tau)\) is

\begin{equation*}
f(\mu, \tau)
= \frac{\sqrt{\lambda_0\tau}}{\sqrt{2\pi}}\exp\left\{\frac{-\lambda_0\tau}{2}(\mu - \mu_0)^2\right\}
\frac{\beta_0^{\alpha_0}}{\Gamma(\alpha_0)} \tau^{\alpha_0 - 1} \exp\left\{-\tau\beta_0\right\}
\end{equation*}

Also, the conditional pdf of \(\vec{X} | (\mu, \tau)\) is

\begin{equation*}
f(\vec{x} | \mu, \tau) = \left(\frac{\tau}{2\pi}\right)^{\frac{n}{2}} \exp\left\{\frac{-\tau}{2}\sum_{i=1}^n(x_i - \mu)^2\right\}
\end{equation*}

Therefore, the joint pdf of \((\vec{x},\mu, \tau)\)is

\begin{equation*}
\begin{aligned}
f(\vec{x}, \mu, \tau)
&=  \frac{\sqrt{\lambda_0\tau}}{\sqrt{2\pi}}\exp\left\{\frac{-\lambda_0\tau}{2}(\mu - \mu_0)^2\right\}
     \frac{\beta_0^{\alpha_0}}{\Gamma(\alpha_0)} \tau^{\alpha_0 - 1} \exp\left\{-\tau\beta_0\right\}
     \left(\frac{\tau}{2\pi}\right)^{\frac{n}{2}} \exp\left\{\frac{-\tau}{2}\sum_{i=1}^n(x_i - \mu)^2\right\}
\\&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{\tau}{2\pi}\right)^{\frac{n+1}{2}}\tau^{\alpha_0 - 1}
    \exp\left\{-\mu^2\frac{\lambda_0\tau + n\tau}{2} + \mu \left(\lambda_0 \tau\mu_0 + \tau\sum x_i\right) \right\}
    \\&\hspace{12em}\exp\left\{-\frac{\lambda_0\tau}{2}\mu_0^2 - \frac{\tau}{2}\sum x_i^2 - \tau\beta_0\right\}
\end{aligned}
\end{equation*}

Notice that \(n\overbar{x} = \sum x_i\) and

\begin{equation*}
S^2
= \sum (x_i - \overbar{x})^2
= \sum x_i^2 - 2\overbar{x}\sum x_i + n\overbar{x}^2
= \sum x_i^2 - n\overbar{x}^2
\end{equation*}

Then,

\begin{equation*}
\begin{aligned}
f(\vec{x}, \mu, \tau)
&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{\tau}{2\pi}\right)^{\frac{n+1}{2}}\tau^{\alpha_0 - 1}
    \exp\left\{-\mu^2\frac{\lambda_0\tau + n\tau}{2} + \mu \left(\lambda_0 \tau\mu_0 + \tau n\overbar{x}\right) \right\}
    \\&\hspace{12em}\exp\left\{-\frac{\lambda_0\tau}{2}\mu_0^2 - \frac{\tau}{2}[S^2 + n\overbar{x}^2] - \tau\beta_0\right\}
\end{aligned}
\end{equation*}

Next, notice that

\begin{equation*}
\int_{\mathbb{R}} e^{-at^2 + bt} \ dt
= \frac{\sqrt{2\pi}}{\sqrt{2a}}\int_{\mathbb{R}} \frac{\sqrt{2a}}{\sqrt{2\pi}} e^{-\frac{(t-b/(2a))^2}{2/(2a)} + \frac{b^2}{4a}} \ dt
= \frac{\sqrt{2\pi}}{\sqrt{2a}} e^{\frac{b^2}{4a}}
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
&f(\vec{x}, \tau)
\\&= \int_{\mathbb{R}} f(\vec{x}, \mu, \tau) \ d\mu
\\&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{\tau}{2\pi}\right)^{\frac{n+1}{2}}\tau^{\alpha_0 - 1}
    \exp\left\{-\frac{\lambda_0\tau}{2}\mu_0^2 - \frac{\tau}{2}[S^2 + n\overbar{x}^2] - \tau\beta_0\right\}
    \frac{\sqrt{2\pi}}{\sqrt{\lambda_0\tau + n\tau}}\exp\left\{\frac{\left(\lambda_0 \tau\mu_0 + \tau n \overbar{x}\right)^2}{2(\lambda_0\tau + n\tau)}\right\}
\\&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
    \frac{1}{\sqrt{\lambda_0 + n}}
    \tau^{\alpha_0 - 1 +\frac{n}{2}}
    \exp\left\{-\frac{\tau}{2}\left(\lambda_0\mu_0^2 + S^2 + n\overbar{x}^2 - \frac{\left(\lambda_0\mu_0 + n\overbar{x}\right)^2}{\lambda_0 + n} + \frac{2}{\beta_0}\right)\right\}
\\&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
    \frac{1}{\sqrt{\lambda_0 + n}}
    \tau^{\alpha_0 - 1 +\frac{n}{2}}
    \exp\left\{-\frac{\tau}{2}\left(
        \lambda_0\mu_0^2 + S^2 + n\overbar{x}^2
        - \frac{\lambda_0^2\mu_0^2 + 2\lambda_0\mu_0n\overbar{x} + n^2\overbar{x}^2}{\lambda_0 + n}
        + 2\beta_0
    \right)\right\}
\\&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
    \frac{1}{\sqrt{\lambda_0 + n}}
    \tau^{\alpha_0 - 1 +\frac{n}{2}}
    \exp\left\{-\frac{\tau}{2}\left(
        \frac{n\lambda_0\mu_0^2 + (\lambda_0 + n)S^2 - 2\lambda_0\mu_0n\overbar{x} + \lambda_0n\overbar{x}^2}{\lambda_0 + n}
        + 2\beta_0
    \right)\right\}
\\&= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
    \frac{1}{\sqrt{\lambda_0 + n}}
    \tau^{\alpha_0 - 1 +\frac{n}{2}}
    \exp\left\{-\tau\left(
        \frac{(\lambda_0 + n)S^2 + \lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)}
        + \beta_0
    \right)\right\}
\end{aligned}
\end{equation*}

Next, again notice that

\begin{equation*}
\int_0^\infty t^{a-1}e^{-bt} \ dt
= \frac{\Gamma(a)}{b^a}\int_0^\infty \frac{1}{(1/b)^a \Gamma(a)}t^{a-1}e^{-\frac{t}{1/b}} \ dt
= \frac{\Gamma(a)}{b^a}
\end{equation*}

Therefore,

\begin{equation*}
f(\vec{x})
= \int_0^\infty f(\vec{x}, \tau) \ d\tau
= \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
    \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
    \frac{1}{\sqrt{\lambda_0 + n}}
    \frac{
            \Gamma\left(\alpha_0 + \frac{n}{2}\right)
        }{
            \left(
                \frac{(\lambda_0 + n)S^2 + \lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)} + \beta_0
            \right)^{\alpha_0 + \frac{n}{2}}
        }
\end{equation*}

Now, we have enough to compute the posterior distributions.

\begin{equation*}
\begin{aligned}
f(\mu | \tau | \vec{x})
&= \frac{f(\mu, \tau, \vec{x})}{f(\tau, \vec{x})}
\\&= \frac{
        \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
        \left(\frac{\tau}{2\pi}\right)^{\frac{n+1}{2}}\tau^{\alpha_0 - 1}
        \exp\left\{-\mu^2\frac{\lambda_0\tau + n\tau}{2} + \mu \left(\lambda_0 \tau\mu_0 + \tau n\overbar{x}\right) \right\}
        \exp\left\{-\frac{\lambda_0\tau}{2}\mu_0^2 - \frac{\tau}{2}[S^2 + n\overbar{x}^2] - \tau\beta_0\right\}
    }{
        \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
        \left(\frac{\tau}{2\pi}\right)^{\frac{n+1}{2}}\tau^{\alpha_0 - 1}
        \exp\left\{-\frac{\lambda_0\tau}{2}\mu_0^2 - \frac{\tau}{2}[S^2 + n\overbar{x}^2] - \tau\beta_0\right\}
        \frac{\sqrt{2\pi}}{\sqrt{\lambda_0\tau + n\tau}}\exp\left\{\frac{\left(\lambda_0 \tau\mu_0 + \tau n \overbar{x}\right)^2}{2(\lambda_0\tau + n\tau)}\right\}
    }
\\&= \frac{
        \exp\left\{-\mu^2\frac{\lambda_0\tau + n\tau}{2} + \mu \left(\lambda_0 \tau\mu_0 + \tau n\overbar{x}\right) \right\}
    }{
        \frac{\sqrt{2\pi}}{\sqrt{\lambda_0\tau + n\tau}}\exp\left\{\frac{\left(\lambda_0 \tau\mu_0 + \tau n \overbar{x}\right)^2}{2(\lambda_0\tau + n\tau)}\right\}
    }
\\&= \frac{\sqrt{\tau}\sqrt{\lambda_0 + n}}{\sqrt{2\pi}}
        \exp\left\{
            -\mu^2\frac{\lambda_0\tau + n\tau}{2} + \mu \left(\lambda_0 \tau\mu_0 + \tau n\overbar{x}\right)
            -\frac{\left(\lambda_0 \tau\mu_0 + \tau n \overbar{x}\right)^2}{2(\lambda_0\tau + n\tau)}
        \right\}
\\&= \frac{\sqrt{\tau}\sqrt{\lambda_0 + n}}{\sqrt{2\pi}}
        \exp\left\{\frac{-\tau}{2}\left(
            \mu^2(\lambda_0 + n) - 2\mu \left(\lambda_0 \mu_0 + n\overbar{x}\right)
            \frac{\left(\lambda_0\mu_0 + n \overbar{x}\right)^2}{\lambda_0 + n}
        \right)\right\}
\\&= \frac{\sqrt{\tau}\sqrt{\lambda_0 + n}}{\sqrt{2\pi}}
        \exp\left\{\frac{-\tau(\lambda_0 + n)}{2}\left(
            \mu^2 - 2\mu \frac{\left(\lambda_0 \mu_0 + n\overbar{x}\right)}{\lambda_0 + n}
            \frac{\left(\lambda_0\mu_0 + n \overbar{x}\right)^2}{(\lambda_0 + n)^2}
        \right)\right\}
\\&= \frac{\sqrt{\tau}\sqrt{\lambda_0 + n}}{\sqrt{2\pi}}
        \exp\left\{\frac{-\tau(\lambda_0 + n)}{2}\left(
            \mu - \frac{\lambda_0 \mu_0 + n\overbar{x}}{\lambda_0 + n}
        \right)^2\right\}
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
(\mu | \tau | \vec{x}) \sim N\left(\frac{\lambda_0 \mu_0 + n\overbar{x}}{\lambda_0 + n}, \frac{1}{\tau(\lambda_0 + n)}\right)
\end{equation*}

Next, for \((\tau | \vec{x})\)

\begin{equation*}
\begin{aligned}
f(\tau | \vec{x})
&= \frac{f(\tau, \vec{x})}{f(\vec{x})}
\\&= \frac{
        \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
        \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
        \frac{1}{\sqrt{\lambda_0 + n}}
        \tau^{\alpha_0 - 1 +\frac{n}{2}}
        \exp\left\{-\tau\left(
            \frac{(\lambda_0 + n)S^2 + \lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)} + \beta_0
        \right)\right\}
    }{
     \frac{\beta_0^{\alpha_0}\sqrt{\lambda_0}}{\Gamma(\alpha_0)}
        \left(\frac{1}{2\pi}\right)^{\frac{n}{2}}
        \frac{1}{\sqrt{\lambda_0 + n}}
        \frac{
                \Gamma\left(\alpha_0 + \frac{n}{2}\right)
            }{
                \left(
                    \frac{(\lambda_0 + n)S^2 + \lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)} + \beta_0
                \right)^{\alpha_0 + \frac{n}{2}}
            }
    }
\\&= \frac{
        \left(
            \frac{(\lambda_0 + n)S^2 + \lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)} + \beta_0
        \right)^{\alpha_0 + \frac{n}{2}}
    }{\Gamma\left(\alpha_0 + \frac{n}{2}\right)}
        \tau^{\alpha_0 - 1 +\frac{n}{2}}
        \exp\left\{-\tau\left(
            \frac{(\lambda_0 + n)S^2 + \lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)} + \beta_0
        \right)\right\}
\\&= \frac{
        \left(
            \beta_0 + \frac{1}{2}S^2 + \frac{\lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)}
        \right)^{\alpha_0 + \frac{n}{2}}
    }{\Gamma\left(\alpha_0 + \frac{n}{2}\right)}
        \tau^{\alpha_0 - 1 +\frac{n}{2}}
        \exp\left\{-\tau\left(
            \beta_0 + \frac{1}{2}S^2 + \frac{\lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)}
        \right)\right\}
\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
(\tau | \vec{x}) \sim \Gamma\left(\alpha_0 + \frac{n}{2}, \cfrac{1}{\beta_0 + \frac{1}{2}S^2 + \frac{\lambda_0 n ( \overbar{x} - \mu_0)^2}{2(\lambda_0 + n)}}\right)
\end{equation*}
\end{example}
\end{document}
