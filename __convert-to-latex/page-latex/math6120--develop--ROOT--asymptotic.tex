\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Asymptotic Behaviour and Stability Analysis}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
\begin{admonition-note}[{}]
This was written before \myautoref[{}]{develop--math6120:ROOT:page--stability.adoc}.
\end{admonition-note}

In this section we seek to determine the nature of solutions to differential
equations. This is useful since we often are unable to determine an explicit
form for solutions to the equation.
These methods would allow us to state the nature of solutions without knowing the explicit form.

In general, we call a solution, equation or matrix \emph{stable} if it (or the solutions
associated with it) tend to zero as \(t\to\infty\). Otherwise, we call it \emph{unstable}.

Also, suppose we have two functions \(\phi(t), \psi(t)\).
We say that they are asymptotically equivalent if

\begin{equation*}
\lim_{t\to\infty} \frac{f(t)}{g(t)} =1
\end{equation*}

Since the above definition has problems when \(g(t)\) has infinite occurrences of being zero,
we may use \emph{little o-notation} and write

\begin{equation*}
f(t) = g(t)(1+o(1))
\end{equation*}

\begin{admonition-tip}[{}]
We say that \(h(t) = o(k(t))\) iff for each \(\varepsilon > 0\), there exists
\(t_0 > 0\) such that

\begin{equation*}
\forall t > t_0: |h(t)| < \varepsilon |k(t)|
\end{equation*}

So, in our case, \(h(t) = o(1)\) iff \(\lim_{t\to\infty} h(t) = 0\)
\end{admonition-tip}

\section{Linear equations}
\label{develop--math6120:ROOT:page--asymptotic.adoc---linear-equations}

\subsection{Constant coefficients}
\label{develop--math6120:ROOT:page--asymptotic.adoc---constant-coefficients}

Consider the linear system with constant coefficients

\begin{equation*}
Lx = x^{(n)} + b_1x^{(n-1)} + \cdots b_nx =0
\end{equation*}

Then, the characteristic equation is

\begin{equation*}
P(\lambda) = \lambda^n + b_1\lambda^{n-1} + \cdots b_n =0
\end{equation*}

\begin{theorem}[{}]
Let all the characteristic roots of \(P(\lambda)\) have negative real
parts and \(\rho > 0\) such that \(-\rho > \max_i \Re(\lambda_i)\).
Then, if \(x(t)\) is a solution to the associated linear system,
there exists a constant \(M>0\) such that

\begin{equation*}
\forall t \geq 0: |x(t)| \leq Me^{-\rho t}
\end{equation*}

and hence \(\lim_{t\to \infty} x(t) = 0\).

\begin{proof}[{}]
The linearly independent solutions of the differential equation have
the form

\begin{equation*}
x_k(t) = t^{m_i}e^{\lambda_i t}
\end{equation*}

where \(m_i\) is a non-negative integer.
Then,

\begin{equation*}
|x_k(t)| = t^{m_i} e^{t\Re \lambda_i}
\leq t^{m_i} e^{-\rho t + t(\Re \lambda_i + \rho)}
\leq t^{m_i} e^{t(\Re \lambda_i + \rho)} e^{-\rho t}
\end{equation*}

since \(\Re \lambda_i + \rho < 0\), there exists a \(M_i > 0\)
such that \(t^{m_i}e^{t(\Re \lambda_i + \rho)} < M_i\).

Now, since \(x\) is a solution of the linear system, it must a linear combination of
the linearly independent solutions and hence

\begin{equation*}
|x(t)| = \left|\sum c_it^{m_i}e^{\lambda_i t}\right| \leq \sum |c_it^{m_i}e^{\lambda_i t}|
\leq \sum M_i|c_i|e^{-\rho t}
= M e^{-\rho t}
\end{equation*}

where \(M = \sum M_i |c_i|\).
\end{proof}
\end{theorem}

\begin{corollary}[{}]
If

\begin{itemize}
\item all the characteristic roots with multiplicity greater than one have negative real parts
\item all the characteristic roots with multiplicity equal to one have non-positive real parts
\end{itemize}

Then, the solutions of the differential equation are bounded on \([0, \infty)\).

\begin{proof}[{}]
The proof follows mostly the same as above, however we have to be careful
when examining \(t^{m_i}e^{t(\Re \lambda_i + \rho)}\)l

\begin{itemize}
\item If \(m_i > 0\), we have that \(\Re \lambda_i + \rho < 0\) and hence it is bounded
\item If \(m=0\), we have that \(\Re\lambda_i + \rho \leq 0\) and hence it is bounded
\end{itemize}
\end{proof}
\end{corollary}

Note that the above theorems would typically require us to first determine the roots of the characteristic
equation. However, for equations with high degrees, this may be difficult.
So, instead we use the following theorem

\begin{theorem}[{Routh-Hurwitz Criteria}]
Given equation

\begin{equation*}
P(\rho) = \lambda^n + b_1\lambda^{n-1} + \cdots b_n =0
\end{equation*}

with \emph{real coefficents}. Let

\begin{equation*}
D_1 = b_1
,\quad
D_2 = \begin{vmatrix}
b_1 & b_3 \\
1 & b_2
\end{vmatrix}
,\quad
D_3 = \begin{vmatrix}
b_1 & b_3 & b_5\\
1 & b_2 & b_4\\
0 & b_1 & b_3
\end{vmatrix}
,\ldots\quad
D_k = \begin{bmatrix}
b_1 & b_3 & b_5 & \cdots & b_{2k-1}\\
1 & b_2 & b_4 & \cdots & b_{2k-2}\\
0 & b_1 & b_3 & \cdots & b_{2k-3}\\
\vdots & \vdots & \vdots & \ddots & \vdots\\
0 & 0 & 0 & \cdots & b_k
\end{bmatrix}
\end{equation*}

with \(b_i =0\) if \(i > n\).

Then, all roots of \(P(\rho)=0\) have negative real parts iff
\(D_k > 0\) for \(k=1,2,3,\ldots n\).

\begin{admonition-tip}[{}]
To see the pattern of the \(D_k\), focus on the diagonal entries.
\end{admonition-tip}
\end{theorem}

\subsection{Second order linear}
\label{develop--math6120:ROOT:page--asymptotic.adoc---second-order-linear}

Consider the differential equation

\begin{equation*}
x'' + a(t)x = 0
\end{equation*}

where \(a(t)\) is continuous for \(t \geq 0\).

\begin{theorem}[{}]
If \(a(t) > 0\) and \(a'(t)\) exists and \(a'(t) \geq 0\), then
solutions of the differential equation are bounded.

\begin{proof}[{}]
\begin{admonition-note}[{}]
I don't know if it is possible to prove this result without using the below ``trick''.
\end{admonition-note}

Multiply both sides of the equation by \(x(t)\) and integrate.

\begin{equation*}
\begin{aligned}
0
&= \int_0^t x'(s)x''(s) \ ds + \int_0^t a(s)x'(s)x(s) \ ds
\\&= \frac12 \int_0^t \frac{d}{ds}\left[x'(s)^2\right] \ ds + \frac12\int_0^t a(s)\frac{d}{ds}\left[x(s)^2\right] \ ds
\\&= \frac12 \left[x'(t)^2 - x'(0)^2\right]
+ \frac12\left(\left[a(s)x(s)^2\right]_0^t - \int_0^t a'(s)x(s)^2 \ ds\right)
\\&\geq \frac12 \left[- x'(0)^2\right]
+ \frac12\left(a(t)x(t)^2 - a(0)x(0)^2 - \int_0^t a'(s)x(s)^2 \ ds\right)
\end{aligned}
\end{equation*}

after rearranging, we obtain that

\begin{equation*}
a(t)x(t)^2
\leq x'(0)^2 + a(0)x(0)^2 + \int_0^t a'(s) x(s)^2 \ ds
= C + \int_0^t \frac{a'(s)}{a(s)}a(s) x(s)^2 \ ds
\end{equation*}

where \(C = x'(0)^2 + a(0)x(0)^2\).
Then, by applying Gronwall's inequality

\begin{equation*}
a(t)x(t)^2 \leq C\exp\int_0^t \frac{a'(s)}{a(s)} \ ds = C \exp (\ln a(t) - \ln a(0)) = a(t) (C a(0))
\end{equation*}

and hence \(x(t)\) is bounded by \(\sqrt{Ca(0)}\).
\end{proof}
\end{theorem}

\begin{theorem}[{}]
If \(\int_1^\infty s|a(s)| \ ds\) is finite, then all solutions are asymptotic to
\(a_0 + a_1t\) for some \(a_0, a_1\) which are not both zero.

\begin{proof}[{}]
Consider arbitrary \(t_0 \geq 0\). Then

\begin{equation*}
\begin{aligned}
x(t)
&= x(t_0) + \int_{t_0}^t x'(s) \ ds
\\&= x(t_0) + \int_{t_0}^t x'(t_0) + \int_{t_0}^s x''(u) \ du \ ds
\\&= x(t_0) + x'(t_0)t - x'(t_0)t_0 + \int_{t_0}^t \int_{t_0}^s x''(u) \ du \ ds
\\&= x(t_0) + x'(t_0)t - x'(t_0)t_0 + \int_{t_0}^t \int_u^t x''(u) \ ds \ du \quad\text{by changing order of integration}
\\&= x(t_0) + x'(t_0)t - x'(t_0)t_0 + \int_{t_0}^t (t-u) x''(u) \ du
\\&= x(t_0) + x'(t_0)t - x'(t_0)t_0 - \int_{t_0}^t (t-s) a(s)x(s) \ ds
\end{aligned}
\end{equation*}

\begin{admonition-note}[{}]
I am unsure how to progress from here. In class, we applied inequalities to show that
\(\frac{x(t)}{t}\) is bounded to produce one \(x_1(t)=a_1t\) and then
derived another solution using Abel's formula. However, I find this strange since
the \(a_1\) and \(a_0\) are not unique. What does it mean for \(x(t)\)
to tend to \(a_1t + a_0\)?
\end{admonition-note}
\end{proof}
\end{theorem}

\section{Linear systems}
\label{develop--math6120:ROOT:page--asymptotic.adoc---linear-systems}

\subsection{Constant coefficients}
\label{develop--math6120:ROOT:page--asymptotic.adoc---constant-coefficients-2}

\begin{theorem}[{Stability with constant coefficients}]
Let \(A\) be a constant matrix whose eigenvalues have negative real parts.
Then, if \(X\) is a solution to

\begin{equation*}
X' = AX
\end{equation*}

we have

\begin{equation*}
\lim_{t\to \infty} \|X(t)\| = 0
\end{equation*}

\begin{admonition-note}[{}]
For this reason, we say that \(A\) is stable iff all of its eigenvalues
have negative real parts.
\end{admonition-note}

\begin{proof}[{}]
Let \(\Phi(t) = e^{At}\). Then, from Putzer's Algorithm

\begin{equation*}
\Phi_{ij}(t) = p_{ij}(t)e^{\lambda_j t}
\end{equation*}

where \(p_{ij}(t)\) is a polynomial.
Then, if \(\rho > 0\) such that \(-\rho = \max\Re \lambda_j\),
there exists \(M > 0\) such that

\begin{equation*}
|\Phi_{ij}(t)| \leq Me^{-\rho t}
\end{equation*}

for each \(i,j\).
Therefore

\begin{equation*}
\|X(t)\| = \left|\sum_{i,j=1}^n c_{ij}\Phi_{ij}(t)\right| \leq e^{-\rho t} M\sum_{i,j=1}^n |c_{ij}|
\end{equation*}

and the result follows.
\end{proof}
\end{theorem}

\begin{theorem}[{Asymptotically linear equations}]
Let \(A\) be a constant stable matrix and \(B(t)\)
be an \(n\times n\) continuous matrix such that

\begin{equation*}
\lim_{t\to \infty} \|B(t)\| =0
\end{equation*}

Then, all solutions of the system

\begin{equation*}
Z' = (A+B(t))Z = AZ + B(t)Z
\end{equation*}

are stable.

\begin{proof}[{}]
First notice that if \(Z\) is a solution to the above system
and \(X'=AX\), we have that

\begin{equation*}
(Z-X)' = A(Z-X) + B(t)Z
\end{equation*}

Then,

\begin{equation*}
Z(t)-X(t) = \int_{t_0}^t \Phi(t-s)B(s)Z(s) \ ds
\end{equation*}

where \(\Phi(t) = e^{At}\).
Then

\begin{equation*}
e^{\rho t}\|Z(t)\|
\leq \|X(t)\|e^{\rho t} + \int_{t_0}^t e^{\rho t}\|\Phi(t-s)\|\|B(s)\|\|Z(s)\| \ ds
\leq M + \int_{t_0}^t Me^{\rho s}\|B(s)\|\|Z(s)\| \ ds
\end{equation*}

Then, by Gronwall's inequality we have that

\begin{equation*}
e^{\rho t}\|Z(t)\| \leq M e^{M\int_{t_0}^t \|B(s)\| \ ds}
\implies \|Z(t)\| \leq Me^{M \int_{t_0}^t (\|B(s)\| -\rho) \ ds -\rho t_0}
\end{equation*}

Since \(B(s) \to 0\), the \(\|B(s)\| -\rho\) eventually becomes negative
and \(\|Z(t)\| \to 0\) as desired.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(A\) be a constant stable matrix and \(f: I\times \mathbb{R}^n \to \mathbb{R}^n\).
Let \(\alpha: I\to \mathbb{R}^n\) such that \(\alpha(t) \to 0\) and

\begin{equation*}
\|f(t,x)\| \leq \alpha(t)\|X\|
\end{equation*}

Then, the system

\begin{equation*}
Z' = AZ + f(t,Z)
\end{equation*}

is stable.

\begin{proof}[{}]
First notice that if \(Z\) is a solution to the above system
and \(X'=AX\), we have that

\begin{equation*}
(Z-X)' = A(Z-X) + f(t,Z)
\end{equation*}

and hence

\begin{equation*}
Z(t) = X(t) + \int_{t_0}^t \Phi(t-s)f(s, Z(s))\ ds
\end{equation*}

where \(\Phi(t) = e^{At}\).
Then,

\begin{equation*}
e^{\rho t}\|Z(t)\|
\leq \|X(t)\|e^{\rho t} + \int_{t_0}^t e^{\rho t}\|\Phi(t-s)\|\|f(s, Z(s))\| \ ds
\leq M + \int_{t_0}^t Me^{\rho s}\alpha(s)\|Z(s)\| \ ds
\end{equation*}

and by Gronwall's inequality,

\begin{equation*}
e^{\rho t}\|Z(t)\| \leq M e^{M\int_{t_0}^t \alpha(s) \ ds}
\implies \|Z(t)\| \leq Me^{M \int_{t_0}^t (\alpha(s) -\rho) \ ds -\rho t_0}
\end{equation*}

Consider arbitrary \(\varepsilon > 0\). Then, there exists
\(t_1\) such that \(\alpha(s) < \frac{\rho}{2}\) for all \(s > t_1\)
and hence

\begin{equation*}
\forall t > t_1: \|Z(t)\| \leq Me^{M \int_{t_0}^{t_1} (\alpha(s) -\rho) \ ds -\rho t_0 - (t-t_1)\frac{\rho}{2}}
\end{equation*}

Therefore there exists \(t >t_1\) such that \(\|Z(t)\| < \varepsilon\).
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Consider the system

\begin{equation*}
X' = AX + g(t)
\end{equation*}

where \(A\) is stable.
Suppose there exists constants \(L > 0\)
and \(\sigma > 0\) such that

\begin{equation*}
\|g(t)\| \leq Le^{\sigma t}
\end{equation*}

Then, for every solution \(X(t)\) of the system, there exists
\(M > 0\) and \(\rho > 0\) such that \(X(t) \leq Me^{\rho t}\).

\begin{proof}[{}]
Since \(X\) is a solution, there exists a constant vector \(C\)
such that

\begin{equation*}
X(t) = e^{At} C + \int_{t_0}^t e^{(s-t_0)A} g(s) \ ds
\end{equation*}

Let \(K, \rho > 0\) such that \(\|e^{At}\| \leq Ke^{\rho t}\)
Then,

\begin{equation*}
\begin{aligned}
\|X(t)\|
&\leq \|C\|Ke^{\rho t} + \int_{t_0}^t Ke^{\rho (t-s)} Le^{\sigma s} \ ds
\\&= \|C\|Ke^{\rho t} + KL e^{\rho t}\int_{t_0}^t e^{s(\sigma-\rho)} \ ds
\\&= \|C\|Ke^{\rho t} + \frac{KL}{\sigma -\rho} e^{\rho t} \left[e^{t(\sigma-\rho)} - e^{t_0(\sigma-\rho)}\right]
\end{aligned}
\end{equation*}

After manipulating, it should be possible find suitable constants.
\end{proof}
\end{theorem}

\subsection{Variable coefficients}
\label{develop--math6120:ROOT:page--asymptotic.adoc---variable-coefficients}

Consider the system

\begin{equation*}
X' = A(t)X
\end{equation*}

where \(A(t)\) is continuous.
In this section, we consider the eigenvalues of \(A(t) + A^T(t)\).
Since this sum symmetric (equal to transpose), the eigenvalues would be real valued functions.

\begin{theorem}[{Stable, bounded and unbounded}]
Let \(M\) be the largest eigenvalue of \(A(t) + A^T(t)\)
and \(m\) be the smallest (most negative) eigenvalue.
Then

\begin{itemize}
\item If \(\lim_{t\to\infty} \int_{t_0}^t M(s) \ ds = -\infty\), all solutions tend to zero
\item If \(\limsup_{t\to \infty} \int_{t_0}^t m(s) \ ds = +\infty\)
    every non-zero solution is unbounded.
\item If \(\lim_{t\to\infty} \int_{t_0}^t M(s) \ ds < \infty\)
    every solution is bounded.
\end{itemize}

\begin{admonition-thought}[{}]
I am unsure why \(\limsup\) is used.
\end{admonition-thought}

\begin{proof}[{}]
\begin{admonition-tip}[{}]
We can prove that \(\|AB\| \leq \|A\|\|B\|\) (\(l_2\) norm) by using
Cauchy-Schwartz
\end{admonition-tip}

Consider the \(l_2\) norm and let \(\Phi\) be a fundamental matrix. Then,

\begin{equation*}
\begin{aligned}
\frac{d}{dt}\|\Phi(t)C\|^2
&= \frac{d}{dt}\left(C^T\Phi^T(t)\Phi(t)C\right)
\\&= C^T(\Phi^T(t))'\Phi(t)C + C^T\Phi^T(t)\Phi'(t)C
\\&= C^T(A(t)\Phi(t))^T\Phi(t)C + C^T\Phi^T(t)A(t)\Phi(t)C
\\&= C^T\Phi^T(t)(A^T(t) + A(t))\Phi(t)C
\\&\leq \|C^T\Phi^T(t)\|\|A^T(t) + A(t)\|\|\Phi(t)C\|
\\&= \|\Phi(t)C\|^2\|A^T(t) + A(t)\|
\\&\leq KM(t)\|\Phi(t)C\|^2 \quad\text{by considering jordan-normal form}
\end{aligned}
\end{equation*}

By rearranging and integrating, we get

\begin{equation*}
\|\Phi(t)C\|^2 \leq \|\Phi(t_0)\|^2\exp\int_{t_0}^t K M(s) \ ds
\end{equation*}

Now, if \(\int_{t_0}^\infty M(s) = -\infty\), we see that \(\Phi(t)C \to 0\)
and if \(\int_{t_0}^\infty M(s) < \infty\), we obtain that \(\|\Phi(t)C\|\) is bounded.

Also,

\begin{equation*}
\begin{aligned}
\frac{d}{dt}\|\Phi(t)C\|^2
\\&\leq \|C^T\Phi^T(t)\|\|A^T(t) + A(t)\|\|\Phi(t)C\|
\\&= \|\Phi(t)C\|^2\|A^T(t) + A(t)\|
\\&\geq m(t)\|\Phi(t)C\|^2 \quad\text{by considering jordan-normal form}
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
\|\Phi(t)C\|^2 \geq \|\Phi(t_0)\|^2\exp\int_{t_0}^t K m(s) \ ds
\end{equation*}

and hence if \(\int_{t_0}^t m(s) \ ds = \infty\), we obtain that every non-zero
solution is unbounded.
\end{proof}
\end{theorem}

\begin{lemma}[{Bounded inverse}]
Let

\begin{equation*}
\lim_{t\to \infty} \int_{t_0}^t \trace A(s) \ ds > -\infty
\end{equation*}

If a fundamental matrix \(\Phi(t)\) of the system is (uniformly) bounded,
then its inverse \(\Phi(t)^{-1}\) is also (uniformly) bounded.

Also since \(\det \Phi(t) \neq 0\), no non-zero solution tends to zero.

\begin{admonition-thought}[{}]
What does uniformly bounded mean?
\end{admonition-thought}

\begin{proof}[{}]
From Abel's identity

\begin{equation*}
\det \Phi(t) = \det \Phi(t_0) \exp \int_{t_0}^t \trace A(s) \ ds
\end{equation*}

and hence

\begin{equation*}
\Phi^{-1}(t)
= \frac{\operatorname{adj} \Phi(t)}{\det \Phi(t)}
= \frac{\operatorname{adj} \Phi(t)}{\Phi(t_0)}\exp -\int_{t_0}^t \trace A(s) \ ds
\end{equation*}

Since \(\Phi(t)\) is bounded, \(\operatorname{adj} \Phi(t)\) must also be bounded.
Also, \(-\int_{t_0}^t \trace A(s) \ ds \) is bounded above by hypothesis.
Therefore, we get the desired result.
\end{proof}
\end{lemma}

The behaviour of the perturbed system

\begin{equation*}
X' = A(t)X + g(t)
\end{equation*}

is closely related to the behaviour of the homogenous system.

\begin{theorem}[{}]
Suppose all solutions of the homogenous system tend to zero.

\begin{itemize}
\item If any one solution of the perturbed system is bounded, then all solutions of the perturbed system are bounded.
\item If any one solution of the perturbed system tends to zero, then all solutions of the perturbed system tend to zero.
\end{itemize}

\begin{proof}[{}]
\begin{admonition-note}[{}]
Only the first property is proven, the second property is similarly proven, just take the limit instead.
\end{admonition-note}

Let \(\Psi_1\) be the solution to the perturbed system which is bounded (by \(M_1\))
and \(\Psi\) be any other solution to the perturbed system.
Then \(\Psi(t) - \Psi_1(t)\) is a solution to the homogenous system and tends to zero by hypothesis.
Therefore \(\Psi(t) - \Psi_1(t)\) is bounded by some \(M_2\)
and
\(\Psi\) is also bounded since

\begin{equation*}
\|\Psi(t)\| \leq \|\Psi(t) - \Psi_1(t)\| + \|\Psi_1(t)\| \leq M_2 + M_1
\end{equation*}
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Suppose

\begin{equation*}
\liminf_{t\to\infty} \int_{t_0}^t \trace A(s) \ ds > -\infty
\quad\text{or }\trace A(s) = 0
\end{equation*}

and \(g\) be such that \(\int_{t_0}^\infty \|g(s)\| \ ds < \infty\).
Then, if every solution of the homogenous system is bounded, every solution to the perturbed system
is also bounded.

\begin{proof}[{}]
Let \(\Phi(t)\) be the fundamental matrix. Then, a solution of the system is given by

\begin{equation*}
\phi(t) = \Phi(t)\Phi^{-1}(t_0)X_0 + \int_{t_0}^t \Phi(t)\Phi^{-1}(s)g(s) \ ds
\end{equation*}

by applying the triangle inequality, we obtain that

\begin{equation*}
\|\phi(t)\| \leq \|\Phi(t)\|\|\Phi^{-1}(t_0)X_0\| + \int_{t_0}^t \|\Phi(t)\|\|\Phi^{-1}(s)\|\|g(s)\| \ ds
\end{equation*}

Now, since \(\liminf_{t\to\infty} \int_{t_0}^t \trace A(s) \ ds > -\infty\), we know that \(\Phi^{-1}(t)\)
must be bounded and hence \(\phi(t)\) is bounded as desired.
\end{proof}
\end{theorem}

\begin{theorem}[{}]
Let \(A(t)\) and \(B(t)\) be such that

\begin{itemize}
\item \(\int_0^\infty \|B(s)\| \ ds < \infty\)
\item \(\int_0^\infty \trace A(s) \ ds > - \infty\)
\end{itemize}

If all solutions of the homogenous system are bounded, all solutions to

\begin{equation*}
Z' = [A(t) + B(t)]Z
\end{equation*}

are also bounded.

\begin{proof}[{}]
Let \(X\) be a solution to the system \(X' = A(t)X\) and \(\Phi(t)\) be the fundamental matrix. Then,

\begin{equation*}
(Z-X)' = [A(t) + B(t)]Z - A(t)X = A(t)(Z-X) + B(t)Z
\end{equation*}

and hence

\begin{equation*}
Z(t) = X(t) + \Phi(t)\Phi(t_0)C + \int_{t_0}^t \Phi(t)\Phi^{-1}(s) B(s)Z(s) \ ds
\end{equation*}

Since \(\Phi(t)\), \(\Phi^{-1}(t)\) and \(X(t)\) are all bounded,
there exists \(M, K > 0\) such that

\begin{equation*}
\|Z(t)\| \leq M + \int_{t_0}^t K \| B(s) \| \| Z(s) \| \ ds
\end{equation*}

and by Gronwall's inequality,

\begin{equation*}
\|Z(t)\| \leq M\exp\int_{t_0}^t K\|B(s)\| \ ds
\end{equation*}

and hence \(Z(t)\) is bounded.
\end{proof}
\end{theorem}
\end{document}
