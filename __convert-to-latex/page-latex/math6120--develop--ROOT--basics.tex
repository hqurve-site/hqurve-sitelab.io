\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Basics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
(I am not sure how much preface to put)

The following definitions are relevant to a 2d plane.

\begin{description}
\item[Connected] A non-empty set \(S\) is called \emph{connected} if any two points of \(S\)
    can be joined by a continuous curve which lies entirely in \(S\)
\item[Open] A non-empty set \(S\) is called \emph{open} if each point can be enclosed by
    a circle which lies entirely in \(S\).
\item[Domain] Open and connected
\item[Boundary point] A point \(p\) is a \emph{boundary point} of domain \(D\) if
    every circle around \(p\) contains points in \(D\) and points which are not in \(D\).
\item[Closed domain] A domain together with its boundary points
\end{description}

The set of real or complex-valued functions having \(k\) continuous
derivatives on \(I\) is denoted by \(C^k(I)\)

\section{What is a differential equation?}
\label{develop--math6120:ROOT:page--basics.adoc---what-is-a-differential-equation}

Equations which express a relationship between variables and their derivatives are called differential equations.
We do not classify the following as differential equations

\begin{itemize}
\item \(\frac{d}{dx}(xy) = y + x \frac{dy}{dx}\). This is an identity
\item \(\left.\frac{dy}{dx}\right|_x = y_{x+1}\) since the left and right hand side are evaluated at different positions
\item \(\frac{dy}{dx} = \int_0^x e^sy(s) \ ds\). Note that we can convert this to a differential equation using
    the fundamental theorem of calculus (part 1).
\end{itemize}

\section{Solutions}
\label{develop--math6120:ROOT:page--basics.adoc---solutions}

A function \(\phi(t)\) is a called a solution of a differential equation
(on interval \(I\)) of order \(n\) iff

\begin{enumerate}[label=\arabic*)]
\item \(\phi(t)\) is \(n\) times differentiable on \(I\)
\item \(\phi(t)\) satisfies the equation for each \(t \in I\)
\end{enumerate}

\section{Forms}
\label{develop--math6120:ROOT:page--basics.adoc---forms}

\begin{description}
\item[General/implicit form] 
    
    \begin{equation*}
    F(t, x, x', \ldots, x^{(n)}) = 0
    \end{equation*}
    
    where \(n \geq 1\)
    and \(F\) is a function from some subset of \(\mathbb{R}^{n+2}\)
    to \(\mathbb{R}\).
\item[Normal/explicit/canonical form] 
    
    \begin{equation*}
    x^{(n)} = f(t, x, \ldots x^{(n-1)})
    \end{equation*}
    
    where \(n\geq 1\)
    and \(f\) is a function from some subset of \(\mathbb{R}^{n+1}\)
    to \(\mathbb{R}\).
\item[Operator form] 
    
    \begin{equation*}
    L(x) = r(t)
    \end{equation*}
    
    where \(L\) is a operator on \(x\).
    In this context,
    an operator is a function which maps from functions to functions.
    Note that the right hand side consists only of the independent variable.
    The left hand side may have \(t\) in it.
\end{description}

\begin{admonition-caution}[{}]
I believe this only makes sense in the case of linear operators.
\end{admonition-caution}

\section{Degree and Order}
\label{develop--math6120:ROOT:page--basics.adoc---degree-and-order}

\begin{description}
\item[Order] The order of a differential equation is the order
    of the highest derivative present in the equation.
\item[Degree] The degree of a differential equation is the power of the highest derivative.
\end{description}

\section{Linear}
\label{develop--math6120:ROOT:page--basics.adoc---linear}

A differential equation in operator form is called \emph{linear}
if the operator is linear.
For our cases, there are two fundamental linear operators of interest

\begin{itemize}
\item Scalar multiplication: \(L(x) = p(t)x\)
\item Differentiation: \(L(x) = x'\)
\end{itemize}

Since linear operators are closed under linear combinations and composition,
we see that a linear differential equation of order \(n\)
has the form

\begin{equation*}
L(x) := \sum_{k=0}^n p_k(t) x^{(k)} = r(t)
\end{equation*}

If \(r(t) = 0\), then we call the linear DE \emph{homogeneous}
otherwise we call it \emph{non-homogeneous}.

\section{Separable}
\label{develop--math6120:ROOT:page--basics.adoc---separable}

The differential equation \(x' = f(t, x)\) is \emph{separable} if there exists
a continuous function \(h(t)\) and a continuously differentiable function \(g(x) \neq 0\)
such that

\begin{equation*}
f(t, x) = h(t)g(x)
\quad\text{or}\quad
f(t,x) = \frac{h(t)}{g(x)}
\end{equation*}

In the case \(h(t)=1\), then the equation becomes \(x' = g(x)\)
and is called an \emph{autonomous equation} otherwise it is called \emph{non-autonomous}.

Such equations can be solved using the separation of variables technique.

\section{Exact}
\label{develop--math6120:ROOT:page--basics.adoc---exact}

Consider the following differential equation

\begin{equation*}
M(t,x) \ dt + N(t,x) \ dx = 0
\end{equation*}

This DE is called \emph{exact} if there exists a function \(u(t,x)\)
such that \(M\ dx + N\ dx = du\). That is, the differential equation is equivalent to

\begin{equation*}
\frac{d}{dt} u (t,x) = 0
\end{equation*}

In the case where \(M\) and \(N\) are continuous with continuous partial derivatives in
a given closed rectangle, a necessary and sufficient condition for the DE to be exact is

\begin{equation*}
\frac{\partial M}{\partial x} = \frac{\partial N}{\partial t}
\end{equation*}

\begin{proof}[{}]
Clearly the forward direction is true by Clairaut's theorem.
So, now we take that
\(\frac{\partial M}{\partial x} = \frac{\partial N}{\partial t}\)
and we need to find \(u\) such that \(u_t = M\) and \(u_x=N\).

Since \(M\) is continuous, by the FTC (part 1),
there exists \(G(t,x)\) such that \(G_t = M\)
and \(G\) has continuous second derivatives.
We may choose such a \(G(t,x) = \int_{t_0}^t M(s) \ ds\).
Then,

\begin{equation*}
\frac{\partial N}{\partial t}
= \frac{\partial M}{\partial x}
= \frac{\partial^2 G}{\partial x \partial t}
= \frac{\partial }{\partial t} \left(\frac{\partial G}{\partial x}\right)
\implies \frac{\partial }{\partial t}\left(N-\frac{\partial G}{\partial x}\right)  =0
\end{equation*}

We may now take

\begin{equation*}
u(t,x) = G(t,x) + \int_{x_0}^x N(t,s) - G_x(t,s) \ ds
\end{equation*}

We may then verify that that the equation is exact since

\begin{equation*}
\frac{\partial }{\partial t}u(t,x)
= M(t,x) + \int_{x_0}^x \frac{\partial }{\partial t}\left(N(t,s)- G_x(t,s)\right) \ ds
= M(t,x) + 0
\end{equation*}

\begin{equation*}
\frac{\partial}{\partial x} u(t,x) = G_x(t,x) + N(t,x)-G_x(t,x) = N(t,x)
\end{equation*}

\begin{admonition-note}[{}]
We could have equivalently taken

\begin{equation*}
u(t,x) = \int_{t_0}^t M(r,x) \ dr + \int_{x_0}^x N(t,s) \ ds
    - \int_{x_0}^x\int_{t_0}^t P(r,s) \ ds \ dr
\end{equation*}

where

\begin{equation*}
P(r,s) = M_x(r,s) = N_t(r,s)
\end{equation*}

Defining \(u\) like this is symmetric and is easier to compute.
It is also worth knowing that the order of integration does not matter in the third integral
since \(P\) is continuous on a closed region which implies that it must be bounded and hence
we may use Fubini's theorem.
\end{admonition-note}

\begin{admonition-important}[{}]
The use of the Leibniz rule is valid since the partial derivatives of the functions being integrated are
continuous.
\end{admonition-important}
\end{proof}

\begin{admonition-remark}[{}]
In the notes, the following remark is made. In the proof, we showed that
one valid \(u\) has the form

\begin{equation*}
u(t,x) = G(t,x) + \int_{x_0}^x N(t,s) - G_x(t,s) \ ds
\end{equation*}

where \(G_t = M\). In this form the integrated term is independent of \(t\) (has zero partial derivative).
Therefore, we may simply say that

\begin{equation*}
u(t,x) = \int M(t,x) \ dt + \int \text{terms of }N\text{ without }t \ dx
\end{equation*}
\end{admonition-remark}

\subsection{Conversion to Exact}
\label{develop--math6120:ROOT:page--basics.adoc---conversion-to-exact}

We may force a differential equation to be exact by multiplying by an \emph{integrating factor}
\(\mu(t,x)\).
The resulting equation is now

\begin{equation*}
\mu(t,x) M(t,x)\ dt + \mu(t,x) N(t,x) \ dx = 0
\end{equation*}

Then, this DE is exact iff

\begin{equation*}
M\mu_x + \mu M_x = (M\mu)_x = (N\mu_t) = N\mu_t + \mu N_t
\end{equation*}

There are three special cases of interest

\begin{description}
\item[Function of t only] 
    
    \begin{equation*}
    \frac{\mu'}{\mu} = \frac{M_x - N_t}{N}
    \end{equation*}
\item[Function of x only] 
    
    \begin{equation*}
    \frac{\mu'}{\mu} = \frac{N_t - M_x}{M}
    \end{equation*}
\item[Function of xy only] 
    
    \begin{equation*}
    \frac{\mu'}{\mu} = \frac{N_t - M_x}{tM -xN}
    \end{equation*}
    
    Note that if \(M= xf(tx)\) and \(N=tg(tx)\) with \(f\neq g\),
    then solving this yields the integrating factor
    \(\mu(tx) = \frac{1}{tM -xN}\)
\end{description}

In all three cases, we first ensure that the right hand side is of the correct form,
then solve for \(\mu\).

\subsubsection{Other Integrating factors}
\label{develop--math6120:ROOT:page--basics.adoc---other-integrating-factors}

\begin{itemize}
\item \(\frac{1}{Mt + Nx}\) if \(Mt + Nx \neq 0\) and \(\frac{M}{N}\) is a function of \(\frac{t}{x}\)
    
    \begin{proof}[{}]
    \begin{equation*}
    \frac{\partial}{\partial x}\frac{M}{Mt + Nx}
    = \frac{1}{(Mt +Nx)^2}
    \left(
    M_x(Mt +Nx) - M(M_x t +N_x x + N)
    \right)
    = \frac{1}{(Mt +Nx)^2}
    \left(
    M_xNx  - MN_x x -NM
    \right)
    \end{equation*}
    
    \begin{equation*}
    \frac{\partial}{\partial t}\frac{N}{Mt + Nx}
    = \frac{1}{(Mt +Nx)^2}
    \left(
    N_t(Mt+Nx) - N(M_tt + M + N_tx)
    \right)
    = \frac{1}{(Mt +Nx)^2}
    \left(
    N_tM t - NM_t t- NM
    \right)
    \end{equation*}
    
    Note both are equal iff
    
    \begin{equation*}
    \begin{aligned}
    &M_xNx - MN_x x  = N_tM t - NM_t t
    \\&\iff \frac{M_x}{M}x - \frac{N_x}{N}x  = \frac{N_t}{N}t - \frac{M_t}{M}t
    \\&\iff \begin{bmatrix}t&x\end{bmatrix} \cdot \begin{bmatrix}\frac{\partial}{\partial t} & \frac{\partial}{\partial x}\end{bmatrix}\ln\left(\frac{M}{N}\right)
        = \left(t \frac{\partial}{\partial t} + x \frac{\partial}{\partial x}\right)\ln\left(\frac{M}{N}\right) = 0
    \\&\iff \exists \mu : \begin{bmatrix}\frac{\partial}{\partial t} & \frac{\partial}{\partial x}\end{bmatrix}\ln\left(\frac{M}{N}\right)
            = \begin{bmatrix}-\mu x & \mu t\end{bmatrix}
            \quad\text{where }\mu \text{ is a function}
    \end{aligned}
    \end{equation*}
    
    From a bit of trial and error, we expect that \(\ln\left(\frac{M}{N}\right)\)
    is a function of \(\frac{t}{x}\). The substitution \(t=ux\) does not work on its own.
    Instead notice that the differential is very similar to the chain rule. In particular,
    if \((t,x)\) is a function of \((u, v)\), we get that for any function \(f(t,x)\)
    
    \begin{equation*}
    \frac{\partial f}{\partial v}
    = \frac{\partial f}{\partial t}\frac{\partial t}{\partial v}
    + \frac{\partial f}{\partial x}\frac{\partial x}{\partial v}
    \end{equation*}
    
    Therefore, we want \(\frac{\partial t}{\partial v} = t\) and \(\frac{\partial x}{\partial v}=x\).
    The clear solution to this is \(t=ue^v\) and \(x=\frac{1}{u}e^v\).
    However, with this, it is impossible to get \(x=0\).
    So, instead we use \(t=\sqrt{u}v\) and \(x = \frac{v}{\sqrt{u}}\).
    Then, we get that
    
    \begin{equation*}
    \begin{aligned}
    &\text{Multiplication by the integrating factor yields an exact DE}
    \\&\iff \left(t \frac{\partial}{\partial t} + x \frac{\partial}{\partial x}\right)\ln\left(\frac{M}{N}\right) = 0
    \\&\iff \left(\frac{1}{v}\frac{\partial}{\partial v}\right)\ln\left(\frac{M}{N}\right) = 0
    \\&\iff \ln\left(\frac{M}{N}\right) = f(u) = f\left(\frac{t}{x}\right)
    \\&\iff \frac{M}{N} = g\left(\frac{t}{x}\right)
    \end{aligned}
    \end{equation*}
    
    Therefore, the integrating factor \(\frac{1}{Mx+Ny}\) is valid if
    \(\frac{M}{N}\) is a function of \(\frac{t}{x}\).
    
    \begin{admonition-note}[{}]
    Verification is not necessary, but it is nice to see that it works
    \end{admonition-note}
    
    To verify, we return to the necessary and sufficient condition for exact. We showed above
    that the integrating factor causes the equation to be exact iff
    
    \begin{equation*}
    M_xNx - MN_x x  = N_tM t - NM_t t
    \end{equation*}
    
    By substituting \(M = gN\) where \(g\) is a function of \(\frac{t}{x}\), we see that
    
    \begin{equation*}
    M_xNx - MN_xx
    = \left(-\frac{t}{x^2}g'N + gN_x\right)Nx - gNN_xx
    = -\frac{t}{x}g'N^2
    \end{equation*}
    
    and
    
    \begin{equation*}
    N_tM t - NM_t t
    = N_t gN t - N\left(\frac{1}{x}g'N + gN_t\right)t
    = - \frac{t}{x} g'N^2
    \end{equation*}
    
    Therefore, we have not made any mistakes
    \end{proof}
\item \(t^\alpha x^\beta\) for some \(\alpha\) and \(\beta\) if \(M\) and \(N\) are (multivariate) polynomials in \(t\) and \(x\).
    
    \begin{admonition-warning}[{}]
    I do not know how correct this is. Consider \(M=1\) and \(N=t^2+x^2\).
    Then,
    
    \begin{equation*}
    \beta t^\alpha x^{\beta-1}
    =
    (t^\alpha x^\beta M)_x
    =
    (t^\alpha x^\beta N)_t
    =
    (\alpha+2)t^{\alpha+1} x^\beta + \alpha t^{\alpha-1}x^\beta
    \end{equation*}
    
    Then, at least one of the two terms on the right must be non-zero.
    However, the powers of \(x\) do not match.
    \end{admonition-warning}
\end{itemize}

\section{Boundary value problem and initial value problem}
\label{develop--math6120:ROOT:page--basics.adoc---boundary-value-problem-and-initial-value-problem}

Differential equations may also have constraints. Two common sets of constraints are
\emph{initial conditions} and \emph{boundary conditions}.
A problem with one of these constraints is called

\begin{description}
\item[Initial value problem (IVP) (or Cauchy's problem)] if initial values are given. Usually of the form \(x(t_0) = x_0\)
\item[Boundary value problem (BVP)] if the boundary conditions are given. Usually of the form \(x(t_0)=x_1\)
    and \(x(t_1)=x_1\).
    Note that this is a one dimensional example on an interval.
    For higher dimensional spaces, the boundary consists of the topological boundary.
\end{description}

\section{Well posed}
\label{develop--math6120:ROOT:page--basics.adoc---well-posed}

We say that a differential equation is \emph{well posed} if

\begin{enumerate}[label=\arabic*)]
\item A solution exists satisfying the conditions
\item There is a unique solution corresponding to each of the conditions
\item Solutions depend continuously on the conditions.
\end{enumerate}

\begin{admonition-remark}[{}]
Perhaps the final condition is needed since any set of conditions measured in the real
world has some intrinsic error. If the solution is continuous on the conditions,
the error of the solution is dependent on the error of the conditions.
\end{admonition-remark}

\section{Special classifications}
\label{develop--math6120:ROOT:page--basics.adoc---special-classifications}

\subsection{Bernoulli}
\label{develop--math6120:ROOT:page--basics.adoc---bernoulli}

A Bernoulli equation is one of the form

\begin{equation*}
x'+p(t) x = r(t)x^\alpha
\end{equation*}

If \(\alpha = 0\) or \(\alpha=1\), this DE is linear otherwise it is non linear.
By setting \(v = x^{1-\alpha}\) we can transform this into a linear DE as follows

\begin{equation*}
\begin{aligned}
v'
= (1-\alpha)x^{-\alpha}x'
= (1-\alpha)x^{-\alpha}(rx^\alpha - px)
= (1-\alpha)(r - px^{1-\alpha})
= (1-\alpha)(r - pv)
\end{aligned}
\end{equation*}
\end{document}
