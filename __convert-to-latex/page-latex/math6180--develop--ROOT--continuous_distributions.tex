\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Continuous Distributions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\section{Uniform}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---uniform}

A \(Uniform[a,b]\) distribution has the following pdf on support \([a,b]\)

\begin{equation*}
f(x) = \frac{1}{b-a}
\end{equation*}

and

\begin{equation*}
E[X] = \frac{a+b}{2},\quad\text{and}\quad Var[X] = \frac{(b-a)^2}{12}
\end{equation*}

and its MGF is given by

\begin{equation*}
E[e^{tX}] = \int_a^b e^{tx}\frac{1}{b-a} \ dx
= \begin{cases}
\frac{e^{tb}-e^{ta}}{t(b-a)},\quad &\text{if } t\neq 0\\
1,\quad&\text{if } t= 0\\
\end{cases}
\end{equation*}

\section{Exponential}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---exponential}

The \(Exponential(\lambda)\) distribution is often used to model times and is useful in this course because of the memoryless property (see below).
The support is given by \(\lambda > 0\) and \(x \geq 0\)

\begin{equation*}
f(\lambda) = \lambda e^{-\lambda x}
\end{equation*}

\begin{equation*}
E[X] = \frac{1}{\lambda},\quad\text{and}\quad Var[X] = \frac{1}{\lambda^2}
\end{equation*}

\begin{equation*}
E[e^{tX}] = \int_0^\infty \lambda e^{(t-\lambda) x} \ dx
= \frac{\lambda}{\lambda-t}
\end{equation*}

\begin{admonition-note}[{}]
The MGF is only defined when \(t < \lambda\).
\end{admonition-note}

\subsection{Memoryless property}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---memoryless-property}

The memoryless property states if \(X\sim Exp(\lambda)\)

\begin{equation*}
P(X > s+t | X > s) = P(X > t)
\end{equation*}

\begin{admonition-tip}[{}]
This is clear to see by substituting the definition of conditional probability and the pdf and simplifying
\end{admonition-tip}

\subsection{Minimum}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---minimum}

Let \(X_1, \ldots X_n\) be independent exponential random variables with \(X_i \sim \exp(\lambda_i)\).
Then

\begin{itemize}
\item \(\min(X_1, \ldots X_n) \sim \exp\left(\sum_{i=1}^n \lambda_i\right)\)
\item \(P(\min(X_1, \ldots X_n) = X_i) = \frac{\lambda_i}{\sum_{j=1}^n \lambda_j}\)
\end{itemize}

\begin{proof}[{}]
For the first property

\begin{equation*}
\begin{aligned}
P(\min(X_1, \ldots X_n) > t)
&= P(X_1 > t,\ X_2 >t, \ \ldots,\ X_n > t)
\\&= \prod_{i=1}^n P(X_i> t) \quad\text{since the } X_i \text{ are independent}
\\&= \prod_{i=1}^n e^{-\lambda_i t}
\\&= e^{-\sum{i=1}^n \lambda_i t}
\end{aligned}
\end{equation*}

Hence \(\min(X_1, \ldots X_n) \sim \exp\left(\sum_{i=1}^n \lambda_i\right)\).

Next,

\begin{equation*}
\begin{aligned}
P(X_i = \min(X_1, \ldots X_n))
&= P(X_i < X_j \text{ for } j \neq i)
\\&= \int_0^\infty P(X_i < X_j \text{ for } j \neq i \ | \ X_i = t)\lambda_i e^{-\lambda_i t} \ dt
    \quad\text{ by conditioning on } X_i
\\&= \int_0^\infty P(t < X_j \text{ for } j \neq i)\lambda_i e^{-\lambda_i t} \ dt
\\&= \int_0^\infty\lambda_i e^{-\lambda_i t} \prod_{j \neq i} e^{-\lambda_j t} \ dt
\\&= \int_0^\infty\lambda_i \prod_{j =1}^n e^{-\lambda_j t} \ dt
\\&= \frac{\lambda_i}{\sum_{i=1}^n \lambda_i} \int_0^\infty \left(\sum_{i=1}^n \lambda_i\right)
        e^{-\left(\sum_{i=1}^n \lambda_i\right)t }  \ dt
\\&= \frac{\lambda_i}{\sum_{i=1}^n \lambda_i}
\end{aligned}
\end{equation*}

as desired
\end{proof}

\section{Gamma}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---gamma}

The \(Gamma(\alpha, \beta)\) distribution is useful as it can be though of as the sum of exponential distributions.
The support is given by \(\alpha, \beta, x > 0\)

\begin{equation*}
f(x) = \frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-\beta x}
\end{equation*}

\begin{equation*}
E[X] = \frac{\alpha}{\beta},\quad\text{and}\quad Var[X] = \frac{\alpha}{\beta^2}
\end{equation*}

\begin{equation*}
\begin{aligned}
E[e^{tX}]
&= \int_0^\infty e^{tx}\frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-\beta x} \ dx
\\&= \int_0^\infty \frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{(t-\beta) x} \ dx
\\&= \left(\frac{\beta}{\beta-t}\right)^\alpha\int_0^\infty \frac{(\beta-t)^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-(\beta-t) x} \ dx
\\&= \left(\frac{\beta}{\beta-t}\right)^\alpha
\end{aligned}
\end{equation*}

\section{Normal}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---normal}

The \(Normal(\mu, \sigma^2)\) distribution is often the default distribution of choice as a result of the central limit theorem.
It's support is all real numbers and its pdf is given by

\begin{equation*}
f(x) = \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right)
\end{equation*}

\begin{equation*}
E[X] = \mu,\quad\text{and}\quad Var[X] = \sigma^2
\end{equation*}

\begin{equation*}
\begin{aligned}
E[e^{tX}]
&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right) \exp(tx) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right) \exp(tx) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2-2tx\sigma^2}{2\sigma^2}\right) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2-2t(x-\mu)\sigma^2 - 2t\mu\sigma^2}{2\sigma^2}\right) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu - t\sigma^2)^2- (t\sigma^2)^2 - 2t\mu\sigma^2}{2\sigma^2}\right) \ dx
\\&= \exp\left(\frac{(t\sigma^2)^2 + 2t\mu\sigma^2}{2\sigma^2}\right)\int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu - t\sigma^2)^2}{2\sigma^2}\right) \ dx
\\&= \exp\left(\frac{(t\sigma^2)^2 + 2t\mu\sigma^2}{2\sigma^2}\right)
\\&= \exp\left(\frac{t^2\sigma^2 + 2t\mu}{2}\right)
\end{aligned}
\end{equation*}

\subsection{Central Limit Theorem}
\label{develop--math6180:ROOT:page--continuous-distributions.adoc---central-limit-theorem}

\begin{admonition-important}[{}]
This is an informal statement of the theorem.
Please see \myautoref[{Math3278 -- Moment Generating functions}]{develop--math3278:moment-generating-functions:page--theorems.adoc}
for a more formal statement.
\end{admonition-important}

Let \(X_1,\ldots X_n\) be a random sample from a population with finite
variance and finite mean.
Then,

\begin{equation*}
\overbar{X}_n \approx N\left(\mu, \frac{\sigma^2}{n}\right)
\end{equation*}

This approximation holds better when \(n\) is large (usually if \(n\geq 30\)).
\end{document}
