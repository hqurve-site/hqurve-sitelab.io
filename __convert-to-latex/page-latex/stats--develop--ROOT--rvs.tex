\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Random variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Let \((\Omega, \mathcal{F}, P)\) be a \href{https://en.wikipedia.org/wiki/Probability\_space}{probability space} where

\begin{itemize}
\item \(\Omega\) is the sample space.
\item \(\mathcal{F} \subseteq 2^\Omega\) is the event space.
\item \(P: \mathcal{F} \to [0,1]\) is a probability measure.
\end{itemize}

A random variable is a measurable function \(X:\Omega \to S\), a mapping from the sample space
to the state space. For convenience, we often ignore formal definitions and think
of \(X\) as a variable which can take some set of values.

\section{Concepts}
\label{develop--stats:ROOT:page--rvs.adoc---concepts}

\subsection{Distribution function}
\label{develop--stats:ROOT:page--rvs.adoc---distribution-function}

Let \(X\) be a real valued random variable (\(S = \mathbb{R}\)).
Then the distribution function \(F\) is defined as

\begin{equation*}
F(x) = P(X \leq x) = P(\{\omega \in \Omega: X(\omega) \leq x\})
\end{equation*}

This function obeys the following properties

\begin{itemize}
\item \(F\) is monotonically increasing
\item \(F: \mathbb{R} \to [0,1]\)
\item \(F(-\infty) = 0\) and \(F(\infty) =1\)
\item \(F\) is right continuous. That is \(\forall x \in \mathbb{R}: F(x^+) = F(x)\)
\end{itemize}

Conversely, any function which obeys the above, uniquely defines a random variable.

Since \(F\) may have left discontinuities, we may have that \(F(x^-) \neq F(x)\).
In such a case, we have an atom of probability at \(X =x\) with mass \(F(x) - F(x^-)\).

\subsection{Atoms of probability}
\label{develop--stats:ROOT:page--rvs.adoc---atoms-of-probability}

We say that \(X\) has an atom of probability at \(x \in S\)
if

\begin{equation*}
P(\{\omega \in\Omega: X(\omega) = x\}) = P(X = x) > 0
\end{equation*}

Note that the first expression is formal while the second is a short hand convenience.

\subsection{Independence}
\label{develop--stats:ROOT:page--rvs.adoc---independence}

Two random variables \(X,Y\) are said to be independent if for each pair of events \(A, B\in \mathcal{F}\),

\begin{equation*}
P(X \in A, \ Y \in B) = P(X\in A)P(Y\in B)
\end{equation*}

\section{Transformations of random variables}
\label{develop--stats:ROOT:page--rvs.adoc---transformations-of-random-variables}

Let \(X\) be a random variable and \(h: S \to S'\) be a transformation.
Then, we can define a new random variable \(Y = h \circ X\) on the original sample
space.
It is important to note the representation of this random variable

\begin{description}
\item[Cumulative distribution functions] \(P(Y \leq y) = P(h(X) \leq y) = P(X \in h^{-1}(-\infty, y))\)
\item[Probability density functions] Let \(f\) be the pdf of \(X\) and \(g\) be the pdf of \(Y\).
    Then, if \(h\) is injective on \(S\).
    
    \begin{equation*}
    g(y) = f(h^{-1}(y)) \left|\frac{\partial h^{-1}}{\partial y}\right|
    \end{equation*}
    
    where \(\frac{\partial h^{-1}}{\partial y}\) is the jacobian matrix of the inverse \(h^{-1}\).
    
    Also, if \(h\) is not injective, the sample space of \(X\) must first be partitioned
    into \(S_1, \ldots S_n\) such that \(h\) is injective on each partition
    and
    
    \begin{equation*}
    g(y) = \sum_{i=1}^n I_{h(S_i)}(y) f(h_i^{-1}(y)) \left|\frac{\partial h_i^{-1}}{\partial y}\right|
    \end{equation*}
    
    where \(h_i: S_i \to S'\) and \(I_A\) is the indicator function.
\end{description}

\section{Expectation and variance}
\label{develop--stats:ROOT:page--rvs.adoc---expectation-and-variance}

The expectation of a random variable \(X\) is defined as

\begin{equation*}
E[X] = \int_\Omega x \ dP
\end{equation*}

This is a Lebesgue integral weighted by \(P\) and hence
only exists if the positive and negative integrals are both finite.

\begin{admonition-tip}[{}]
Instead of this heavy machinery of Lebesgue integrals, we can simply use
normal integration/summation.
\end{admonition-tip}

\begin{theorem}[{Law of the unconscious statistician}]
We can also find the integrals of functions of random variables. For example,
the expectation of \(f(X)\) is

\begin{equation*}
E[f(X)] = \int_\Omega f(x) \ dP
\end{equation*}

Note that this is actually a theorem since \(f(X)\) (or rather \(f \circ X\))
is itself a random variable. \href{https://en.wikipedia.org/wiki/Law\_of\_the\_unconscious\_statistician}{[wikipedia link]}.
\end{theorem}

The \textbf{variance} of \(X\) is defined as

\begin{equation*}
Var[X] = E[(X-E[X])^2] = E[X^2] - E[X]^2
\end{equation*}

The \textbf{covariance} of \(X\) and \(Y\) is defined as

\begin{equation*}
Cov[X,Y] = E[(X-E[X])(Y-E[Y])] = E[XY] - E[X]E[Y]
\end{equation*}

Note that the covariance is an inner product.

\subsection{Theorems about expectations}
\label{develop--stats:ROOT:page--rvs.adoc---theorems-about-expectations}

There are two theorems which provide a relationship between probability and expectations.

\begin{theorem}[{Markov's Inequality}]
Let \(X\) be a random variable and \(\mu(X)\) be a non-negative function.
Then, if \(E[\mu(X)]\) exists

\begin{equation*}
\forall c > 0: P(\mu(X) \geq c) \leq \frac{E[\mu(X)]}{c}
\end{equation*}
\end{theorem}

\begin{theorem}[{Chebyshev's Inequality}]
Let \(X\) be a random variable.
Then, if \(Var[Z]\) exists,

\begin{equation*}
\forall \varepsilon > 0: P\left(\left|Z-E[Z]\right| \leq \varepsilon\right) \geq 1-\frac{Var[Z]}{\varepsilon^2}
\end{equation*}
\end{theorem}

Sometimes, it may be hard to find the expectation of a function of a random variable.
In some special cases, we can obtain a bound

\begin{theorem}[{Jenson's Inequality}]
Let \(\phi\) be a convex function in interval \(I\).
Let \(X\) be a random variable whose support lies in \(I\).
Then,

\begin{equation*}
\phi(E[X]) \leq E[\phi(X)]
\end{equation*}

\begin{admonition-note}[{}]
This inequality is strict if \(\phi\) is strictly convex
\end{admonition-note}
\end{theorem}

\begin{admonition-tip}[{}]
Recall that function \(f\) is convex if

\begin{equation*}
\forall x,y: \forall t \in [0,1]: f(tx + (1-t)y) \leq tf(x) + (1-t)f(y)
\end{equation*}

This holds if

\begin{itemize}
\item \(f'\) is monotonically increasing
\item \(f''\) is non-negative
\end{itemize}

Furthermore, \(f\) is called \textbf{strictly} convex if the about inequalities
are strict.
\end{admonition-tip}

\section{Moment generating function}
\label{develop--stats:ROOT:page--rvs.adoc---moment-generating-function}

The MGF of a random variable \(X\) is defined as

\begin{equation*}
M(t) = E[e^{tX}]
\end{equation*}

and only exists if \(M(t)\) is finite for \(t \in (-h,h)\) for some \(h > 0\).

\begin{theorem}[{Uniqueness of MGF}]
Let \(X\) and \(Y\) be random variables with MGFs.
Then

\begin{equation*}
F_X(z) = F_Y(z) \ \forall z\in \mathbb{R}
\quad\iff\quad
M_X(t) = M_Y(t) \ \forall t \in (-h,h) \text{ for some } h>0
\end{equation*}

\begin{admonition-tip}[{}]
This theorem is difficult to prove. One method consists of first proving
that the equal MGF implies equal characteristic function and then proving uniqueness of
characteristic functions.
\end{admonition-tip}
\end{theorem}

\begin{theorem}[{Shift theorems}]
Let \(X\) be a random variable with MGF and \(a, b\) be constants.
Let \(Y = aX+b\).
Then

\begin{equation*}
M_Y(t) = e^{bt}M_X(at)
\end{equation*}
\end{theorem}

\begin{theorem}[{Convolution}]
Let \(X\) and \(Y\) be independent random variables with MGFs.
Let \(Z = X+Y\)
Then,

\begin{equation*}
M_Z(t) = M_X(t) M_Y(t)
\end{equation*}
\end{theorem}
\end{document}
