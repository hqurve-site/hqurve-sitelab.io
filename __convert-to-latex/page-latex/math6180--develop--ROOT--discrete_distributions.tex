\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Discrete Distributions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-important}[{}]
This page has a very loose/informal discussion of random variables.
Please see \myautoref[{}]{develop--math2274:ROOT:page--random/index.adoc} for a more formal discussion.
I am typing this page only for revision purposes (and currently math2274 doesnt currently have pdf output).
\end{admonition-important}

\section{Bernoulli}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---bernoulli}

In a \(Bernoulli(p)\) random variable, there are two items in the sample space

\begin{itemize}
\item success which occurs with probability \(p\)
\item failure which occurs with probability \(1-p\)
\end{itemize}

The variable counts the number of successes.
The probability mass function can be compactly written as \(P(X=x) = p^x(1-p)^{1-x}\)
with support \(x \in \{0,1\}\).

\begin{equation*}
E[X] = p,
\quad\text{and}\quad
Var[X] = p(1-p)
\end{equation*}

and the MGF is given by

\begin{equation*}
E[e^{tX}] = (1-p) + pe^t
\end{equation*}

\section{Binomial}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---binomial}

A \(Binomial(n,p)\) counts the number of successes in \(n\) iid \(Bernoulli(p)\) trials.

\begin{equation*}
P(X=x) = \binom{n}{x}p^x(1-p)^{n-x}, \quad x=0,1,2,3,\ldots n
\end{equation*}

also

\begin{equation*}
E[X] = np,\quad\text{and}\quad Var[X] = np(1-p)
\end{equation*}

The MGF is given by

\begin{equation*}
\begin{aligned}
E[e^{tX}]
&= \sum_{x=0}^n \binom{n}{x}p^x(1-p)^{n-x}e^{tx}
\\&= (1-p)^n \sum_{x=0}^n \binom{n}{x}\left(\frac{e^tp}{1-p}\right)^x
\\&= (1-p)^n \left(1+\frac{e^tp}{1-p}\right)^n
\\&= \left((1-p)+ e^tp\right)^n
\end{aligned}
\end{equation*}

Immediately from the MGF, we see that a \(Binomial(n,p)\) RV is the
sum of \(n\) iid \(Bernoulli(p)\) RVs

\section{Multinomial}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---multinomial}

A \(Multinomial(n, p_1, \ldots p_k)\) is a vector random variable where there are \(n\)
iid trials where in each trial there are  \(k\) variants where variant
\(i\) has probability \(p_i\) of being chosen.
The entry \(X_i\) counts the number of variant \(i\) chosen.

\begin{equation*}
P(X_1=x_i, \ldots X_k=x_k)
= \binom{n}{x_1,\ldots x_k} \prod_{i=1}^k p_i^{x_i}
= n! \prod_{i=1}^k \frac{p_i^{x_i}}{x_i!}
,\quad \sum p_i = 1\text{ and } \sum x_i = n \text{ and } x_i \geq 0
\end{equation*}

The marginal distribution of \(X_i\) is clearly \(Binomial(n, p_i)\),
so we have the following properties

\begin{equation*}
E[X_i] = np_i,\quad\text{and} \quad Var[X_i] = np_i(1-p_i)
\end{equation*}

Also, the covariance is given by

\begin{equation*}
Cov[X_i, X_j] = -np_ip_j
\end{equation*}

since

\begin{equation*}
\begin{aligned}
E[X_1X_2]
&= \sum_{x_1+\cdots x_k = n, x_i \geq 0} x_1x_2 \binom{n}{x_1,\ldots x_k} \prod_{i=1}^k p_i^{x_i}
\\&= p_1p_2 \sum_{x_1+\cdots x_k = n, x_i \geq 0} x_1x_2 \binom{n}{x_1,\ldots x_k} p_1^{x_1-1} p_2^{x_2-1}\prod_{i=3}^k p_i^{x_i}
\\&= p_1p_2 \frac{\partial^2}{\partial p_1\partial p_2} \sum_{x_1+\cdots x_k = n, x_i \geq 0} \binom{n}{x_1,\ldots x_k} \prod_{i=1}^k p_i^{x_i}
\\&= p_1p_2 \frac{\partial^2}{\partial p_1\partial p_2} \left(\sum_{i=1}^k p_k\right)^n
\\&= n(n-1)p_1p_2 \left(\sum_{i=1}^k p_k\right)^n
\\&= n(n-1)p_1p_2
\end{aligned}
\end{equation*}

\begin{admonition-note}[{}]
I am just finding \(E[X_1, X_2]\) to avoid indexing issues. But the result holds by symmetry.
\end{admonition-note}

and the MGF is given by

\begin{equation*}
\begin{aligned}
E[e^{\vec{t}\cdot\vec{X}}]
&= \sum_{i=1}^n \binom{n}{x_1,\ldots x_k} \prod_{i=1}^k p_i^{x_i} e^{\vec{t}\cdot \vec{x}}
\\&= \sum_{i=1}^n \binom{n}{x_1,\ldots x_k} \prod_{i=1}^k p_i^{x_i} \prod_{i=1}^k e^{t_ix_i}
\\&= \sum_{i=1}^n \binom{n}{x_1,\ldots x_k} \prod_{i=1}^k (p_ie^{t_i})^{x_i}
\\&= \left(\sum_{i=1}^k p_ie^{t_i}\right)^n
\end{aligned}
\end{equation*}

\section{Geometric}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---geometric}

In a geometric process, independent \(Bernoulli(p)\) trials are performed until
there is a success.
There are two conventions for geometric random variables.

\subsection{Exclusive version}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---exclusive-version}

In this version, the random variable counts the number of failures until the first success.
So

\begin{equation*}
P(X=x) = (1-p)^xp,\quad x=0,1,2,\ldots
\end{equation*}

and

\begin{equation*}
E[X] = \frac{1-p}{p},\quad\text{and}\quad Var[X] = \frac{1-p}{p^2}
\end{equation*}

the MGF is given by

\begin{equation*}
E[e^{tX}]
= \sum_{x=0}^\infty p(1-p)^x e^{tx}
= p\sum_{x=0}^\infty [(1-p)e^t]^x
= \frac{p}{1- (1-p)e^t}
\end{equation*}

\subsection{Inclusive version}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---inclusive-version}

In this version, the random variable counts the number of trials until the first success.
So

\begin{equation*}
P(X=x) = (1-p)^{x-1}p,\quad x=1,2,\ldots
\end{equation*}

and

\begin{equation*}
E[X] = \frac{1}{p},\quad\text{and}\quad Var[X] = \frac{1-p}{p^2}
\end{equation*}

the MGF is given by

\begin{equation*}
E[e^{tX}]
= \sum_{x=1}^\infty p(1-p)^{x-1} e^{tx}
= pe^t \sum_{x=0}^\infty [(1-p)e^t]^{x-1}
= \frac{pe^t}{1- (1-p)e^t}
\end{equation*}

\section{Negative Binomial}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---negative-binomial}

A \(NegativeBinomial(k,p)\) RV performs iid \(Bernoulli(p)\) processes
and counts the number of failures until \(k\) successes.

\begin{equation*}
P(X=x) = \binom{k+x-1}{x}p^k(1-p)^x
\end{equation*}

This can be derived intuitively since there are \(k\) successes and \(x\) failures.
If we lay the outcomes of the individual trials in a line, there are \(x+k\) outcomes
and the last must be a success. So there are \(\binom{k+x-1}{x}\) ways to place the failures.

Note

\begin{equation*}
E[X] = \frac{k(1-p)}{p}
,\quad\text{and}\quad
Var[X] = \frac{k(1-p)}{p^2}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
E[e^{tX}]
&= \sum_{x=0}^\infty \binom{k+x-1}{x}p^k(1-p)^x e^{tx}
\\&= \frac{p^k}{(k-1)!}\sum_{x=0}^\infty \frac{(k+x-1)!}{x!}((1-p)e^t)^x
\\&= \frac{p^k}{(k-1)!}\sum_{x=0}^\infty \left[\frac{d^{k-1}}{ds^{k-1}} s^{x+k-1}\right]_{s= (1-p)e^t}
\\&= \frac{p^k}{(k-1)!} \left[\frac{d^{k-1}}{ds^{k-1}} \sum_{x=0}^\infty  s^{x+k-1}\right]_{s= (1-p)e^t}
\\&= \frac{p^k}{(k-1)!} \left[\frac{d^{k-1}}{ds^{k-1}} \frac{s^{k-1}}{1-s}\right]_{s= (1-p)e^t}
\\&= \frac{p^k}{(k-1)!} \left[\frac{d^{k-1}}{ds^{k-1}} \frac{1+ \sum_{i=0}^{k-2} s^i(s-1)}{1-s}\right]_{s= (1-p)e^t}
\\&= \frac{p^k}{(k-1)!} \left[\frac{d^{k-1}}{ds^{k-1}} \left(\frac{1}{1-s} + \sum_{i=0}^{k-2}-s^i\right)\right]_{s= (1-p)e^t}
\\&= \frac{p^k}{(k-1)!} \left[\frac{(k-1)!}{(1-s)^k}\right]_{s= (1-p)e^t}
\\&= \left(\frac{p}{1-(1-p)e^t}\right)^k
\end{aligned}
\end{equation*}

From this, is clear that the negative binomial is the sum of iid (exclusive) geometric random variables.

\section{Poisson}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---poisson}

A \(Poisson(\lambda)\) RV counts the number of occurrences of an event
which occurs with mean occurrence rate \(\lambda\).
The probability mass function is given by

\begin{equation*}
P(X=x) = \frac{e^{-\lambda}\lambda^x}{x!},\quad x=0,1,2,3,\ldots
\end{equation*}

and

\begin{equation*}
E[X] = Var[X] = \lambda
\end{equation*}

and the MFG is given by

\begin{equation*}
E[e^{tX}]
= \sum_{x=0}^\infty \frac{e^{-\lambda}\lambda^x}{x!}e^{tx}
= e^{-\lambda}\sum_{x=0}^\infty \frac{(\lambda e^t)^x}{x!}
= e^{-\lambda}e^{\lambda e^t}
= e^{\lambda (e^t-1)}
\end{equation*}

Therefore, it is clear that if \(X \sim Poisson(\lambda_1)\)
and \(Y\sim Poisson(\lambda_2)\), \(X+Y \sim Poisson(\lambda_1+\lambda_2)\).

\section{Hypergeometric}
\label{develop--math6180:ROOT:page--discrete-distributions.adoc---hypergeometric}

A \(Hypergeometric(N,M,n)\) process consists of \(N\) objects of which \(M (\leq N)\)
are tagged as ``special''.
We then draw \(n\) of the \(N\) objects without replacement.
The variable counts the number of special objects which were drawn.

\begin{equation*}
P(X=x) = \frac{\binom{M}{x}\binom{N-M}{n-x}}{\binom{N}{n}}
,\quad x = \max(0,n+M-N),\ldots \min(n,M)
\end{equation*}

Note that this probability mass function can be obtained by a simple counting argument.

\begin{equation*}
E[X] = np,
\quad\text{and}\quad
Var[X] = np(1-p)\frac{N-n}{N-1}\quad\text{where } p = \frac{M}{N}
\end{equation*}

There is no nice closed form for the MGF
\end{document}
