\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Countability}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
As its name implies, the countability of a set refers to the ability to
count the elements in that set. More specifically, the ability to
sequence all of the elements such that each element appears in the list.
There are two cases in which a set \(S\) is countable

\begin{itemize}
\item \(S\) is finite. That is, empty or equivalent to
    \(J_n\) for some \(n\).
\item \(S\) is equivalent to \(\mathbb{N}\).
\end{itemize}

The latter definition is due to the fact that a \emph{sequence} is simply a
bijection from \(\mathbb{N}\) to \(S\) as
\(\mathbb{N}\) is defined as a sequence of element. That is
\(\mathbb{N}\) has

\begin{itemize}
\item a first element \(0 = \varnothing \in \mathbb{N}\)
\item a successor function \(s\) such that
    \(\forall n \in \mathbb{N}: s(n) = n \cup \{n\} \in \mathbb{N}\)
\end{itemize}

In the case that \(|S| = |\mathbb{N}|\) we call
\(S\) \emph{denumerable} and represent its cardinality by
\(\aleph_0\).

\section{Some theorems including countable sets}
\label{develop--math2277:ROOT:page--countability.adoc---some-theorems-including-countable-sets}

\begin{enumerate}[label=\roman*)]
\item Every subset of a finite set is finite.
\item The union of two finite sets is finite.
\item Every subset of a denumerable set is countable.
\item An infinite subset of a denumerable set is denumerable.
\item The union of a finite and denumerable set is denumerable.
\item The union of denumerable sets is denumerable.
\end{enumerate}

\begin{example}[{Proof}]
Note that these proofs will not take into consideration empty sets as
the results follow immediately.

\begin{description}
\item[Part (i)] If \(S \sim J_n\) and \(A \subseteq S\) then WLOG
    (since permutations are bijections) we can let \(A\) consist
    of the first \(m\) elements of \(S\). Then clearly
    immediately, \(A\sim J_m\).
\item[Part (ii)] Let \(A \sim J_n\) and \(B \sim J_m\), then we can
    split \(A \cup B\) into the disjoint sets
    \(A\backslash B\), \(B \backslash A\) and
    \(A \cap B\) and instead only consider the case when
    \(A\) and \(B\) are disjoint. In this case, let
    \(f\colon A \to J_n\) and and \(g\colon B \to J_m\)
    be our bijective functions. Then, we can define
    \(h\colon A\cup B \to J_{m+n}\) as \(h(x) = f(x)\)
    if \(x \in A\) and \(h(x) = g(x) + n\) if
    \(x\in B\). Then \(h\) is bijective and
    \(A\cup B \sim J_{m+n}\).
\item[Part (iii)] Let \(A \sim \mathbb{N}\) and \(B \subseteq A\).
    Then, we can label the elements of \(A\) as
    \(x_1, x_2, \ldots\) according to the bijection from
    \(\mathbb{N}\) to \(A\). Then, a subset of
    \(B\) is uniquely defined by a possibly finite strictly
    increasing sequence \(i_1, i_2, \ldots\) of elements in
    \(\mathbb{N}\). If the sequence is finite, there is a maximum
    element \(i_n\) and hence \(B \equiv J_n\). On the
    other hand, if the sequence is infinite our bijection from
    \(h\colon\mathbb{N}\to B\) is defined by
    \(h(k) = x_{i_k}\).
\item[Part (iv)] The proof of this was captured in part (iii) above.
\item[Part (v)] Let \(A \sim J_n\) and \(B \sim \mathbb{N}\). If
    \(A\) and \(B\) are not disjoint we will split
    \(A \cup B\) into disjoint sets and reapply this theorem (note
    that the intersection will be finite). Now that \(A\) and
    \(B\) are disjoint, let \(f\colon A \to J_n\) and
    \(g\colon B \to \mathbb{N}\) then define
    \(h\colon A \cup B \to \mathbb{N}\) as
    \(h(x) = f(x)\) if \(x \in A\) and
    \(h(x) = g(x) + n\) if \(x \in B\). Then,
    \(h\) is our bijection from \(A \cup B\) to
    \(\mathbb{N}\).
\item[Part (vi)] Let \(A \sim B \sim \mathbb{N}\). Then, we will only consider
    the case when they are disjoint since the sets can be split into
    disjoint parts. Let \(f\colon A \to \mathbb{N}\) and
    \(g\colon B \to \mathbb{N}\) be our bijective functions and
    define \(h\colon A \cup B \to \mathbb{N}\) as
    \(h(x) = 2f(x)-1\) if \(x \in A\) and
    \(h(x) = 2g(x)\) if \(x \in B\). Then,
    \(h\) is out bijection from \(A \cup B\) to
    \(\mathbb{N}\).
\end{description}

 ◻
\end{example}

\section{Equivalent definitions countability}
\label{develop--math2277:ROOT:page--countability.adoc---equivalent-definitions-countability}

Let \(A\) be a set, then following definitions are equivalent

\begin{enumerate}[label=\roman*)]
\item \(A\) is countable.
\item \(\exists\) an injection from \(A\) to
    \(\mathbb{N}\).
\item \(\exists\) an surjection from \(\mathbb{N}\) to
    \(A\).
\end{enumerate}

The proof outlined below ignores the possibility that \(A\)
may be empty.

\begin{example}[{Proof}]
Recall that a set is countable if its empty, equivalent to
\(J_n\) or equivalent to \(\mathbb{N}\). Then
immediately, \((i)\implies (ii) \,\land\, (iii)\).

\begin{description}
\item[\((ii) \to (i)\)] Let \(f\colon A\to \mathbb{N}\) be our injective function.
    Then, \(f(A) \subseteq \mathbb{N}\) and hence
    \(f(A)\) is countable. Then, since \(f\) is a
    bijection from \(A\) to
    \(f(A) \subseteq \mathbb{N}\), \(A\) must be
    countable.
\item[\((iii) \to (i)\)] Let \(f\colon \mathbb{N} \to B\) be our surjective function.
    Then, by definition, for all \(b \in B\) there exists an
    element \(i_b \in \mathbb{N}\) such that
    \(f(i_b) = b\). Let \(i_b\) be the minimal such
    element and \(I\) be the set of all such minimal
    \(i_b\). Therefore the function \(g\colon B\to I\)
    defined by \(g(b) = i_b\) is a bijection from \(B\)
    to the countable set \(I\) and hence \(B\) is
    countable.
\end{description}

 ◻
\end{example}

Note that since (ii) is more concise than our original definition, we
will prefer to use it in proofs. Additionally, since equivalence
requires a bijection, \(\mathbb{N}\) in the above statements
can be replaced by any denumerable set.

\section{Minimality of denumerable sets}
\label{develop--math2277:ROOT:page--countability.adoc---minimality-of-denumerable-sets}

Let \(S\) be an infinite set. Then, \(S\) contains a
subset which is denumerable. Below is an outline of the proof and note
that this proof requires the axiom of choice (obviously).

\begin{example}[{Proof}]
Let \(s_1\) be an element in \(S\). Then, since
\(S\) is infinite, \(S\backslash \{s_1\}\) is also
infinite and there exists an element \(s_2\) in it. We can
this process infinitely since \(S\) is infinite and will not
''run out of elements.'' Then, we define our subset as
\(D = \{s_1, s_2, \ldots\}\) and bijective function
\(f\colon \mathbb{N} \to D\) as \(f(n) = s_n\).
Clearly, this function is bijective and since
\(D \subseteq S\), the result follows.

 ◻
\end{example}

Therefore, for all infinite sets \(S\),
\(|\mathbb{N}| = |D| \leq |S|\) and \(\aleph_0\) is
the minimal transfinite cardinal number.

\section{Countability of cartesian product of countable sets}
\label{develop--math2277:ROOT:page--countability.adoc---countability-of-cartesian-product-of-countable-sets}

Let \(A\) and \(B\) be countable sets. Then, we can
define an injection from \(A \times B\) to
\(\mathbb{N} \times \mathbb{N}\) so we will only show that
\(\mathbb{N} \times \mathbb{N}\) is countable.

\begin{example}[{Proof}]
Let \(f\colon \mathbb{N} \times \mathbb{N} \to \mathbb{N}\) be
defined as \(f(m, n) = 2^m 3^n\). Then \(f\) is
injective since

\begin{equation*}
f(m_1, n_1) = f(m_2, n_2)
        \iff 2^{m_1}3^{n_1} = 2^{m_2}3^{n_2}
        \iff 2^{m_1 - m_2} = 3^{n_2 - n_1}
\end{equation*}

Then since the only power of \(2\) that is a power of
\(3\) is \(1\) and hence the result follows.

 ◻
\end{example}

\section{Countable union of countable sets}
\label{develop--math2277:ROOT:page--countability.adoc---countable-union-of-countable-sets}

Let the indexed family of sets \(\mathcal{A}\) indexed by
\(I\) be a countable set of countable sets. That is
\(|I| \leq |\mathbb{N}|\) and
\(\forall i \in I: |A_i| \leq |\mathbb{N}|\). Then

\begin{equation*}
S = \bigcup_{i \in I} A_i
\end{equation*}

is countable.

\begin{example}[{Proof}]
Firstly, we let \(f\colon \mathbb{N} \to I\) be our surjective
function since \(I\) is countable. Also, for each
\(i \in I\), we defined
\(g_i\colon \mathbb{N} \to A_i\) be our surjective function
from each set in \(\mathcal{A}\) (since all are countable).
Finally, we define
\(h\colon \mathbb{N} \times \mathbb{N} \to S\) as

\begin{equation*}
h(n, m) = g_{f(n)}(m)
\end{equation*}

To show that \(h\) is surjective, take \(x \in S\).
Then, by the definition of the union \(\exists i \in I\) such
that \(x \in A_i\). Next, since \(f\) is surjective,
\(\exists n \in \mathbb{N}: f(n) = i\) and and finally since
\(g_i\) is surjective
\(\exists m \in \mathbb{N}: g_i(m) = x\). Then

\begin{equation*}
h(n, m) = g_{f(n)}(m) = g_i(m) = x
\end{equation*}

Therefore, \(S\) is countable.

 ◻
\end{example}
\end{document}
