\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Graphs Further}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Trees}
\label{develop--math2276:ROOT:page--graphs-further.adoc---trees}

A \emph{tree} is a connected simple graph with no cycles.

\begin{admonition-note}[{}]
If a tree has \(p \geq 2\) vertices, then \(T\)
contains at least two pendant vertices.
This is easily proven by considering the longes path in \(T\).
\end{admonition-note}

\subsection{Equivalent definitions}
\label{develop--math2276:ROOT:page--graphs-further.adoc---equivalent-definitions}

Let \(T\) be a simple graph with \(p\) vertices.
Then following statements are equivalent:

\begin{enumerate}[label=\arabic*)]
\item \(T\) is a tree
\item \(T\) has \(p-1\) edges and no cycles.
\item \(T\) has \(p-1\) edges and is connected.
\end{enumerate}

\begin{example}[{Proof}]
We will prove these three in a cyclic manner.

(i) \(\Rightarrow\) (ii) All we have to show is that
the graph has \(p-1\) edges. We will prove this by induction.
This is certainly true when \(p=1\). Suppose it is true for
all trees with \(k \geq 1\) vertices. Then consider a tree
\(T\) with \(k+1\) vertices. Since all trees have at
least two pendant vertices, consider \(T'\) with one of them
removed. Now \(T'\) has \(k\) vertices and
\(k-1\) edges by the inductive hypothesis. So \(T\)
has \(k-1 + 1 = k\) edges as required.

(ii) \(\Rightarrow\) (iii) All we need to show is that
\(T\) is connected. Suppose \(T\) is consists of
\(t \geq 1\) components. Since all of these components are
connected and have no cycles, they are all trees. Let
\(\{p_i\}\) be the number of vertices in each component. Then
\(\sum_i p_i = p\). Also since, each component is a tree, the
total number of edges is \(\sum_i p_i - 1 = p - t = p - 1\).
Therefore, \(t=1\) and \(T\) is connected.

(iii) \(\Rightarrow\) (i) All we need to show is that
\(T\) has no cycles. Suppose \(T\) did have cycles.
Removing an edge from a cycle does not destroy connectedness, so we can
remove edges from cycles until no cycles are left meanwhile preserving
the connectedness. The resulting graph must be a tree with
\(p\) vertices but since edges where removed, it has
\(q < p-1\) edges. However, this contradicts (ii) and hence
\(T\) must be a tree. ◻
\end{example}

\section{Spanning Trees}
\label{develop--math2276:ROOT:page--graphs-further.adoc---spanning-trees}

A \emph{spanning tree} of a connected graph \(G\) is a tree which
contains all the vertices of \(G\) and which is a subgraph of
\(G\). Essentially, a spanning tree is a tree created from
\(G\) using a process similar to (iii)
\(\Rightarrow\) (i) in the above proof.

\section{Perfect Matching theorem}
\label{develop--math2276:ROOT:page--graphs-further.adoc---perfect-matching-theorem}

Roughly speaking, the failure to find a perfect matching is due to the
lack of edges. In fact, if each vertex of a graph of \(2n\)
vertices has a degree of at least \(n\), then the graph has a
perfect matching.

\begin{example}[{Proof}]
This proof is constructive and will, in addition to proving the
theorem, show how to generate such a matching. This proof will show how
to increase the number of edges in the matching as long as the total
number of vertices in the matching is less than \(2n\).

Suppose that \(r < n\) matches have been made so far. If there
exists two adjacent vertices not already paired, add them to the
matching. Hence the total number matches has increased to
\(r + 1\). Otherwise, consider that no such trivial matching
can be made. This means that each vertex not in the matching is only
adjacent to vertices which are already paired.

Take any two vertices, \(a\) and \(b\), not
currently paired. Notice that the sum of their degrees is at least
\(k \geq 2n\). Also, as previously noted, their adjacent
vertices are already paired off. Therefore, if we take all vertices
\(x\) and \(y\) which are already paired and count
the total number of edges from \(a\) and \(b\) to
\(x\) and \(y\), we also arrive at \(k\).
Notice that there are \(r < n\) such pairs \(x, y\)
and \(k \geq 2n\) which implies that \(k > 2r\).
Therefore, by the pigeonhole principle, there exists a pair of already
matched vertices \(x, y\) such that there are three edges from
\(a\) and \(b\) to \(x\) and
\(y\).

Now, note that at least one of \(a\) and \(b\) is
adjacent to both \(x\) and \(y\). Without loss of
generality, let this vertex be \(a\). Additionally, there is
at least one more edge joining \(b\) to \(x\) or
\(y\). Without loss of generality, let this vertex be
\(x\). Now we have at least the following set of edges:
\(xy, ax, ay, bx\).

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math2276/ROOT/perfect-matching-1}
\end{figure}

We can now remove edge \(xy\) and replace it by
\(ay\) and \(bx\). Therefore, the net effect is
increasing the total number of matches to \(r+1\).

We continue this process until \(r = n\). ◻
\end{example}
\end{document}
