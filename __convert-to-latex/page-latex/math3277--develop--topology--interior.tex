\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Interior, Exterior and Boundary Points}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(A \subseteq X\) then \(x\in X\) is an

\begin{description}
\item[Interior Point] If \(\exists r > 0\) such that \(B(x,r) \subseteq A\).
    The set of all interior points is called the \emph{interior} of \(A\) and
    denoted \(int(A)\) or \(A^\circ\)
\item[Boundary Point/Frontier Point] If \(\forall r > 0\) \(B(x,r) \cap A \neq \varnothing\) and
    \(B(x,r) \cap X\backslash A \neq \varnothing\). The set of all boundary
    points is called the \emph{boundary/frontier} of \(A\) and is denoted
    \(bd(A)\), \(\partial A\) or \(Fr(A)\).
\item[Exterior Point] If \(\exists r > 0\) such that \(B(x,r) \subseteq X\backslash A\).
    The set of all exterior points is called the \emph{exterior} of \(A\) and
    denoted \(ext(A)\). Furthermore, notice that \(ext(A) = int(X\backslash A)\).
\end{description}

\begin{admonition-note}[{}]
Some authors add conditions the conditions that interior and exterior points
must be chosen from \(A\) and \(X\backslash A\) respectively, however
notice that this is already implied by the above definition since \(x\) belongs
to the respective balls.
\end{admonition-note}

\section{Partitioning}
\label{develop--math3277:topology:page--interior.adoc---partitioning}

Let \(A \subseteq X\) then

\begin{equation*}
X = int(A) \sqcup \partial A \sqcup ext(A)
\end{equation*}

That is, the interior, boundary and exterior of a set partition the space.

\begin{example}[{Proof}]
Firstly, we would show that each set is disjoint. We already know that \(int(A) \cap ext(A) = \varnothing\)
Then, consider \(X\backslash (int(A) \cup ext(A))\)

\begin{equation*}
\begin{aligned}
x \in X\backslash (int(A) \cup ext(A))
&\iff x \notin int(A) \quad \text{and}\quad x \notin ext(A)
\\&\iff \left[\forall r > 0: B(x,r) \cap X\backslash A \neq \varnothing \right] \quad\text{and}\quad
        \left[\forall r > 0: B(x,r) \cap A \neq \varnothing \right]
\\&\iff \forall r > 0: [ B(x,r) \cap X\backslash A \neq \varnothing \text{ and } B(x,r) \cap A \neq \varnothing ]
\\&\iff x \in \partial A
\end{aligned}
\end{equation*}

Therefore \(X\backslash (int(A)\cup ext(A)) = \partial A\) and hence we are done.
\end{example}

\section{Properties of interiors}
\label{develop--math3277:topology:page--interior.adoc---properties-of-interiors}

Let \(A, B \subseteq X\) then

\begin{enumerate}[label=\arabic*)]
\item \(int(A) \cap int(B) = int(A\cap B)\)
\item \(int(A)\cup int(B) \subseteq int(A\cup B)\)
\item \(A\subseteq B \implies int(A) \subseteq int(B)\)
\item \(int(int(A)) = int(A)\) that is, the interior is an \myautoref[{open}]{develop--math3277:topology:page--open.adoc} set.
\end{enumerate}

Note that similar properties could generated for for the exterior
using the fact that \(ext(A) = int(X\backslash A)\)

\begin{example}[{Proof}]
\begin{example}[{Proof of 1}]
\begin{equation*}
\begin{aligned}
x \in int(A) \cap int(B)
&\iff \exists r_1, r_2 > 0: [B(x,r_1) \subseteq A \text{ and } B(x,r_2) \subseteq B)]
\\&\iff \exists r = min(r_1,r_2) > 0: B(x,r) \subseteq A\cap B
\\&\iff x \in int(A\cap B)
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Proof of 2}]
\begin{equation*}
\begin{aligned}
x \in int(A) \cup int(B)
&\iff [\exists r > 0: B(x,r) \subseteq A] \text{ or } [\exists r>0: B(x,r) \subseteq B]
\\&\iff \exists r > 0: [ B(x,r) \subseteq A \text{ or } B(x,r) \subseteq B]
\\&\implies \exists r > 0: B(x,r) \subseteq A\cup B
\\&\iff x \in int(A \cup B)
\end{aligned}
\end{equation*}

Notice that the one directional implication is not reversible since \(B(x,r)\) may not
necessarily contain elements from only one of the two sets.
For an example of this, consider any set \(A\) where \(\partial A \neq\varnothing\) and
let \(B = X\backslash A\). Then any element of the boundary contains elements from both
sets.
\end{example}

\begin{example}[{Proof of 3}]
\begin{equation*}
x \in int(A)
\implies \exists r > 0: B(x,r) \subseteq A \subseteq B
\implies x \in int(B)
\end{equation*}
\end{example}

\begin{example}[{Proof of 4}]
Consider \(x\in int(A)\), then \(\exists r > 0\) such that \(B(x,r) \subseteq A\).
Now, consider \(y \in B(x,r)\) and \(B(y,r - d(x,y))\). Then

\begin{equation*}
z \in B(y,r-d(x,y))
\implies d(y,z) < r-d(x,y)
\implies d(x,z) \leq d(x,y) + d(y,z) < r
\implies z \in B(x,r)
\end{equation*}

Hence \(B(y,r-d(x,y)) \subseteq B(x,r) \subseteq A\) and \(y\in int(A)\).
Therefore \(B(x,r) \subseteq int(A)\) and \(x\in int(int(A))\). This means that
\(int(A) \subseteq int(int(A))\) however since we know that \(int(B) \subseteq B\),
we get that \(int(int(A)) \subseteq int(A)\) and we are done.
\end{example}
\end{example}

\section{Boundary of complements}
\label{develop--math3277:topology:page--interior.adoc---boundary-of-complements}

Let \(A \subseteq X\) then

\begin{equation*}
\partial A = \partial (X\backslash A)
\end{equation*}

This follows directly from the fact that \(X\backslash(X\backslash A) = A\).

\section{Interior as the largest open subset}
\label{develop--math3277:topology:page--interior.adoc---interior-as-the-largest-open-subset}

Let \(A \subseteq X\) and \(B \subseteq A\) be \myautoref[{open}]{develop--math3277:topology:page--open.adoc}. Then,

\begin{equation*}
B \subseteq int(A)
\end{equation*}

That is, \(int(A)\) is the largest open subset of \(A\).

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
x \in B = int(B)
\implies \exists r > 0: B(x,r) \subseteq B \subseteq A
\implies x \in int(A)
\end{aligned}
\end{equation*}
\end{example}

\section{Relationship with \myautoref[{limit points}]{develop--math3277:topology:page--isolated-limit.adoc}.}
\label{develop--math3277:topology:page--interior.adoc---relationship-with-latex-backslashmyautoref-latex-openbracelimit-points-latex-closebrace-latex-openbracedevelop-math3277topologypage-isolated-limit-adoc-latex-closebrace}
\end{document}
