\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Fun}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
This section is me using the darboux integral to prove some common integration results

\section{Constants}
\label{develop--math3277:riemann-integration:page--fun.adoc---constants}

Consider \(f(x) = c\). Then \(f \in \mathcal{R}[a,b]\) for all \(a, b\)
and

\begin{equation*}
\int_a^b c \ dx = c(b-a)
\end{equation*}

\begin{example}[{Proof}]
Consider arbirary partition \(P \in \mathcal{P}[a,b]\) then

\begin{equation*}
U(P,f) = L(P,f) = \sum_{i=1}^n c \Delta x_i = c(b-a)
\end{equation*}

and we are done
\end{example}

\subsection{Polynomials}
\label{develop--math3277:riemann-integration:page--fun.adoc---polynomials}

We would first show for powers of \(x\) and by the linearity of the integral, the result would follow.
Let \(f(x) = x^k\) where \(k \in \mathbb{Z}^+\), then

\begin{equation*}
\int_a^b x^k = \frac{b^{k+1} - a^{k+1}}{k+1}
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
Yes, I know I could use the first and second fundamental theorems of calculus, but im not.
\end{admonition-note}

First notice that the identity function is reimann integrable since its monotonic (or continuous;
either would work).
Hence, that all polynomials are reimann integrable since
\myautoref[{\(\mathcal{R}[a,b]\) forms a ring}]{develop--math3277:riemann-integration:page--algebra.adoc--ring-of-r-a-b}.

We would then prove the result when \(a=0\) and then when \(b=0\). Then by using
\myautoref[{this result}]{develop--math3277:riemann-integration:page--index.adoc---reimann-integral-over-joint-intervals}, we
would be done.

\begin{example}[{\(a=0\)}]
Consider arbirary \(\varepsilon > 0\) , then,
there exists an \(n\) such that

\begin{equation*}
\left|\frac{\sum_{i=1}^n i^k}{n^{k+1}} - \frac{1}{k+1}\right| < \frac{\varepsilon}{b^{k+1}}
\end{equation*}

\begin{example}[{Old}]
\begin{admonition-important}[{}]
Well well well, what do we have here? Now, I do know of
\href{https://en.wikipedia.org/wiki/Faulhaber\%27s\_formula}{Faulhaber's formula} but I don't really
want to use it. An alternative is to prove that

\begin{equation*}
\lim_{n\to \infty} \frac{\sum_{i=1}^n i^k}{n^{k+1}} = \frac{1}{k+1}
\end{equation*}

But I am yet to do that.
\end{admonition-important}
\end{example}

\begin{admonition-important}[{}]
Look at bottom of proof for the proof of this limit
\end{admonition-important}

hence if we let \(P_n\) be a regular partition in \(\mathcal{P}[0,b]\)
with \(n\) segments,
we get

\begin{equation*}
U(P, x^k)
= \sum_{i=1}^n M_i(P,x^k) \Delta
= \sum_{i=1}^n x_i^k \frac{b}{n}
= \frac{b}{n} \sum_{i=1}^n \left(\frac{bi}{n}\right)^k
= \frac{b^{k+1}}{n^{k+1}} \sum_{i=1}^n i^k
\end{equation*}

and we get that

\begin{equation*}
\left|U(P, x^k) - \frac{a^{k+1}}{k+1}\right|
= \left|\frac{b^{k+1}}{n^{k+1}} \sum_{i=1}^n i^k - \frac{b^{k+1}}{k+1}\right|
= b^{k+1}\left|\frac{1}{n^{k+1}} \sum_{i=1}^n i^k - \frac{1}{k+1}\right|
< \varepsilon
\end{equation*}

hence the limit of \(U(P,x^k)\) where \(P\) is a regular partition is \(\frac{b^{k+1}}{k+1}\).
Now, we cannot conclude that this sequence tends to the upper reimann integral
however, we can conclude that this is an upper bound of the upper reimann integral. That is,

\begin{equation*}
\int_0^{\overline{b}} x^k \ dx \leq \frac{b^{k+1}}{k+1}
\end{equation*}

Consider the lower darboux sum.

\begin{equation*}
L(P_n, x^k)
= \sum_{i=1}^n m_i(P_n,x^k) \Delta
= \sum_{i=1}^n x_{i-1}^k \frac{a}{n}
= \sum_{i=0}^{n-1} x_{i}^k \frac{a}{n}
= \frac{b}{n}\sum_{i=1}^{n-1} \left(\frac{bi}{n}\right)^k
= \frac{b^{k+1}}{n^{k+1}}\sum_{i=1}^{n-1} i^k
\end{equation*}

and

\begin{equation*}
\begin{aligned}
\lim_{n\to \infty} L(P_n, x^k)
&=\lim_{n\to \infty} \left[\frac{b^{k+1}}{n^{k+1}}\sum_{i=1}^{n-1} i^k\right]
\\&=b^{k+1}\lim_{n\to \infty} \left[\frac{1}{n^{k+1}}\sum_{i=1}^{n-1} i^k\right]
\\&=b^{k+1}\left(\lim_{n\to \infty} \frac{n-1}{n}\right)^{k+1}\left(\lim_{n\to \infty}\frac{1}{n^{k+1}}\sum_{i=1}^{n-1} i^k\right)
\\&=b^{k+1}\left(\lim_{n\to \infty} \frac{n-1}{n}\right)^{k+1}\left(\lim_{n\to \infty}\frac{1}{n^{k+1}}\sum_{i=1}^{n} i^k\right)
\\&=b^{k+1}\left(1\right)^{k+1}\left(\frac{1}{k+1}\right)
\\&= \frac{b^{k+1}}{k+1}
\end{aligned}
\end{equation*}

Then, similarly

\begin{equation*}
\int_{\underline{0}}^b x^k \ dx \geq \frac{b^{k+1}}{k+1}
\end{equation*}

and we are done.
\end{example}

\begin{example}[{\(b=0\)}]
let \(P_n\) be the regular partition in \(\mathcal{P}[a,0]\) with \(n\) segments. Then,
the supremum or infimum of each segment is either the left or right endpoint. We would show that
both sums tend to the same value and hence that is the integral (this is to avoid doing the same work twice).
then

\begin{equation*}
r
= \frac{-b}{n}\sum_{i=1}^{n} \left(\frac{-bi}{n} +b\right)^k
= \frac{-b(b)^{k}}{n^{k+1}}\sum_{i=1}^{n} \left(-i+n\right)^k
= \frac{-b^{k+1}}{n^{k+1}}\sum_{i=0}^{n-1} \left(i\right)^k
\end{equation*}

and

\begin{equation*}
l
= \frac{-b}{n}\sum_{i=1}^{n} \left(\frac{-b(i-1)}{n} +b\right)^k
= \frac{-b}{n}\sum_{i=0}^{n-1} \left(\frac{-bi)}{n} +b\right)^k
= \frac{-b^{k+1}}{n^{k+1}}\sum_{i=0}^{n-1} \left(-i +n\right)^k
= \frac{-b^{k+1}}{n^{k+1}}\sum_{i=0}^{n-1} i^k
\end{equation*}

Then, since both sequences tend to \(\frac{-b^{k+1}}{k+1}\), we are done.
\end{example}

\begin{example}[{Proof of limiting arithmetic sum behariour}]
We want to prove that

\begin{equation*}
\lim_{n\to\infty}\frac{\sum_{i=1}^n i^k}{(n+1)^{k+1}} = \frac{1}{k+1}
\end{equation*}

when \(k > -1\)
We have to split up this proof into two cases; they are mostly the same.

Throughout this proof, we use the fact that \(g:[-1,\infty) \to \mathbb{R}\)
defined by

\begin{equation*}
g(x) = (1+x)^\alpha -1-\alpha x
\end{equation*}

has the following properties

\begin{itemize}
\item if \(\alpha \in \{0,1\}\), \(g(x) =0 \)
\item if \(0 < \alpha < 1\), \(g(x) < 0\)
\item if \(\alpha \in (-\infty, 0)\cup (1,\infty)\), \(g(x) > 0\)
\end{itemize}

these results are easy when \(\alpha \in \mathbb{Z}^+\) however, in the other cases,
I'm unsure if their proof (or rather the non-integer exponent) is circular. (relies on the integral).

\begin{example}[{Case 1: \(k \geq 0\)}]
Define

\begin{equation*}
f(n) = \frac{(k+1)\sum_{i=1}^n i^k}{n^{k+1}}
\end{equation*}

then, I claim that \(1 \leq f(n) < \left(\frac{n+1}{n}\right)^{k+1}\). Then, when \(n=1\), we get that

\begin{equation*}
1 \leq f(1) = 1 < \left(\frac{2}{1}\right)^{k+1}
\end{equation*}

And now suppose that this statement is true
for some \(n\). Then

\begin{equation*}
\begin{aligned}
f(n+1)
&= \frac{(k+1)\sum_{i=1}^{n+1} i^k}{(n+1)^{k+1}}
\\&= \frac{(k+1)\sum_{i=1}^{n} i^k + (k+1)(n+1)^k}{(n+1)^{k+1}}
\\&= \frac{f(n)n^{k+1} + (k+1)(n+1)^k}{(n+1)^{k+1}}
\end{aligned}
\end{equation*}

Now,

\begin{equation*}
\begin{aligned}
f(n+1)
&> \frac{n^{k+1} + (k+1)(n+1)^k}{(n+1)^{k+1}}
> \frac{(n+1)^{k+1}}{(n+1)^{k+1}} = 1
\\&\iff
    n^{k+1} + (k+1)(n+1)^k > (n+1)^{k+1}
\\&\iff
    n^{k+1} + k(n+1)^k > n(n+1)^k
\\&\iff
    \left(\frac{n}{n+1}\right)^k + \frac{k}{n} > 1
\\&\iff
    \left(1 - \frac{1}{n+1}\right)^k -1 + \frac{k}{n} > 0
\\&\iff
    \left(1 - \frac{1}{n+1}\right)^k -1 + \frac{k}{n+1} + \frac{k}{n} - \frac{k}{n+1}> 0
\\&\impliedby
    \left(1 - \frac{1}{n+1}\right)^k -1 - \frac{(-k)}{n+1} + \frac{k}{n(n+1)}> 0
\end{aligned}
\end{equation*}

Note that the last inequality is true since by \(g(\frac{-1}{n+1})\)

And on the other hand,

\begin{equation*}
\begin{aligned}
f(n+1)
&< \frac{(n+1)^{k+1} + (k+1)(n+1)^k}{(n+1)^{k+1}}
 < \frac{(n+2)^{k+1}}{(n+1)^{k+1}} = \left(\frac{n+2}{n+1}\right)^{k+1}
\\&\iff
    (n+1)^{k+1} + (k+1)(n+1)^k < (n+2)^{k+1}
\\&\iff
    1 + \frac{k+1}{n+1} < \left(1+\frac{1}{n+1}\right)^{k+1}
\end{aligned}
\end{equation*}

and by sandwitch theorem, we are done.
\end{example}

\begin{example}[{Case 2: \(-1<k<0\)}]
Define

\begin{equation*}
f(n) = \frac{\sum_{i=1}^n i^k -1}{n^{k+1}}
\end{equation*}

Then, we want to show that

\begin{equation*}
\frac{1}{k+1}\left(\frac{n+1}{n}\right)^{k+1} - \frac{2^{k+1}}{(k+1) n^{k+1}} \leq f(n) \leq \frac{1}{k+1} - \frac{1}{(k+1)n^{k+1}}
\end{equation*}

When \(n=0\), we get \(0 \leq 0 \leq 0\). Now suppose the above inequality
holds for some \(n\), then

\begin{equation*}
\begin{aligned}
f(n+1)
&= \frac{\sum_{i=1}^n i^k -1}{(n+1)^{k+1}}
\\&= \frac{\sum_{i=1}^n i^k + (n+1)^k-1}{(n+1)^{k+1}}
\\&= \frac{\sum_{i=1}^n i^k-1}{(n+1)^{k+1}} + \frac{1}{n+1}
\\&= f(n)\left(\frac{n}{n+1}\right)^{k+1} + \frac{1}{n+1}
\end{aligned}
\end{equation*}

Now,

\begin{equation*}
\begin{aligned}
f(n+1)
&\geq \frac{1}{k+1} - \frac{2^{k+1}}{(k+1)(n+1)^{k+1}} + \frac{1}{n+1}
\\&= \frac{(n+1)^{k+1} + (k+1)(n+1)^k}{(k+1)(n+1)^{k+1}} - \frac{2^{k+1}}{(k+1)(n+1)^{k+1}}
\\&> \frac{(n+2)^{k+1}}{(k+1)(n+1)^{k+1}} - \frac{2^{k+1}}{(k+1)(n+1)^{k+1}}
\\&\iff (n+1)^{k+1} + (k+1)(n+1)^k > (n+2)^{k+1}
\\&\iff
    1 + \frac{k+1}{n+1} > \left(1+\frac{1}{n+1}\right)^{k+1}
\end{aligned}
\end{equation*}

and on the other hand

\begin{equation*}
\begin{aligned}
f(n+1)
&\leq \frac{n^{k+1}}{(k+1)(n+1)^{k+1}} - \frac{1}{(k+1)(n+1)^{k+1}} + \frac{1}{n+1}
\\&= \frac{n^{k+1} + (k+1)(n+1)^k}{(k+1)(n+1)^{k+1}} - \frac{1}{(k+1)(n+1)^{k+1}}
\\&< \frac{(n+1)^{k+1}}{(k+1)(n+1)^{k+1}}- \frac{1}{(k+1)(n+1)^{k+1}}
\\&\iff n^{k+1} + (k+1)(n+1)^k < (n+1)^{k+1}
\\&\iff \left(\frac{n}{n+1}\right)^{k+1} + \frac{k+1}{n+1} < 1
\\&\iff \left(1 - \frac{1}{n+1}\right)^{k+1} < 1 - \frac{k+1}{n+1}
\end{aligned}
\end{equation*}

and we are done.
\end{example}

\begin{figure}[H]\centering
\includegraphics[draft,width=8cm,height=5cm]{placeholder.png}
\caption{tfw you thought you couldn't do a proof for years and suddenly figure it out (well, actually me learning and understanding more concepts about limits probably caused this) (omitted external image: https://i.redd.it/gh826bljgaf61.jpg)}
\end{figure}
\end{example}
\end{example}
\end{document}
