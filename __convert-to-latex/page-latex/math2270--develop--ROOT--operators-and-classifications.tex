\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Operators and classifications}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
\section{Del ''operator''}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---del-operator}

The \emph{del operator} is defined as follows

\begin{equation*}
\nabla = \frac{\partial }{\partial x_1} + \frac{\partial }{\partial x_2} +\cdots + \frac{\partial }{\partial x_n}
\end{equation*}

and is used to compute the following operators.

\section{Gradient}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---gradient}

\begin{equation*}
grad(\phi)
    = \nabla \phi
    = \frac{\partial \phi}{\partial x_1}\bm{e}_1 + \frac{\partial \phi}{\partial x_2}\bm{e}_2 +\cdots + \frac{\partial \phi}{\partial x_n}\bm{e}_n
\end{equation*}

where \(\{e_i\}\) are the standard basis vectors. Note that
\(\phi\) is a scalar field while \(\nabla \phi\) is
a vector field.

\section{Divergence}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---divergence}

\begin{equation*}
div(\bm{F})
    =\nabla\bm{\cdot}\bm{F}
    = \frac{\partial F_1}{\partial x_1} + \frac{\partial F_2}{\partial x_2} + \cdots + \frac{\partial F_n}{\partial x_n}
\end{equation*}

where \(\bm{F}_i\) is the component of \(\bm{F}\) in
the direction of \(\bm{e}_i\). Note that \(\bm{F}\)
is a vector field while \(\nabla \bm{\cdot}\bm{F}\) is a
scalar.

\section{Curl}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---curl}

Let \(\bm{F}
    = F_1\bm{e}_1 + F_2\bm{e}_2 + \cdots + F_n\bm{e}_n
    = F_1\bm{dx}_1 + F_2\bm{dx}_2 + \cdots + F_n\bm{dx}_n\) Then

\begin{equation*}
curl(\bm{F})
    = \nabla \times \bm{F}
    = \bm{d}(\bm{F})
    = \sum_{i=1}^n \nabla F_i \wedge \bm{dx}_i
    = \sum_{i=1}^n \sum_{j=1}^n \frac{\partial F_i}{\partial x_j} \bm{dx}_j \wedge \bm{dx}_i
\end{equation*}

where
\(\bm{dx}_j \wedge \bm{dx}_i = - \bm{dx}_i \wedge \bm{dx}_j\)
This abuse of notation but I like it. It includes differential forms (as
far as I understand) and makes it much easier to compute.

\subsection{Update}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---update}

Upon reading, I have come across the concept of bivectors and I believe
that this is what is being done here. In fact, I believe I didn’t write
the curl down properly as I think it is supposed to hold for all
multivectors in general. Furthermore, I should have probably just
written it as the exterior derivative.

\subsection{Three dimensional case}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---three-dimensional-case}

For example in three dimensions, where
\(\bm{F} = P\bm{\hat{\textbf{\i}}} + Q\bm{\hat{\textbf{\j}}} + R\bm{\hat{\textbf{k}}}\)
and

\begin{equation*}
\bm{\hat{\textbf{\i}}} = \bm{dx}
    ,\quad
    \bm{\hat{\textbf{\j}}} = \bm{dy}
    ,\quad
    \bm{\hat{\textbf{k}}} = \bm{dz}
\end{equation*}

and

\begin{equation*}
\bm{\hat{\textbf{\i}}} \wedge \bm{\hat{\textbf{\j}}} = \bm{\hat{\textbf{k}}}
\end{equation*}

\begin{equation*}
\bm{\hat{\textbf{\j}}} \wedge \bm{\hat{\textbf{k}}} = \bm{\hat{\textbf{\i}}}
\end{equation*}

\begin{equation*}
\bm{\hat{\textbf{k}}} \wedge \bm{\hat{\textbf{\i}}} = \bm{\hat{\textbf{\j}}}
\end{equation*}

then

\begin{equation*}
\begin{aligned}
    \nabla \times \bm{F}
    &= \bm{d}(P\bm{\hat{\textbf{\i}}} + Q\bm{\hat{\textbf{\j}}} + R\bm{\hat{\textbf{k}}})\\
    &= \left[
            \frac{\partial P}{\partial x} \bm{dx} \wedge \bm{dx} + \frac{\partial P}{\partial y} \bm{dy} \wedge \bm{dx} + \frac{\partial P}{\partial z} \bm{dz} \wedge \bm{dx}
         \right]\\
    &\quad\quad+ \left[
            \frac{\partial Q}{\partial x} \bm{dx} \wedge \bm{dy} + \frac{\partial Q}{\partial y} \bm{dy} \wedge \bm{dy} + \frac{\partial Q}{\partial z} \bm{dz} \wedge \bm{dy}
         \right]\\
    &\quad\quad+ \left[
            \frac{\partial R}{\partial x} \bm{dx} \wedge \bm{dz} + \frac{\partial R}{\partial y} \bm{dy} \wedge \bm{dz} + \frac{\partial R}{\partial z} \bm{dz} \wedge \bm{dz}
         \right]\\
    &= \left[
            \frac{\partial P}{\partial y} (-\bm{dx} \wedge \bm{dy}) + \frac{\partial P}{\partial z} (\bm{dz} \wedge \bm{dx})
         \right]\\
    &\quad\quad+ \left[
            \frac{\partial Q}{\partial x} (\bm{dx} \wedge \bm{dy})  + \frac{\partial Q}{\partial z} (-\bm{dy} \wedge \bm{dz})
         \right]\\
    &\quad\quad+ \left[
            \frac{\partial R}{\partial x} (-\bm{dz} \wedge \bm{dx}) + \frac{\partial R}{\partial y} (\bm{dy} \wedge \bm{dz})
         \right]\\
    &= \left[  \frac{\partial R}{\partial y} - \frac{\partial Q}{\partial z} \right] \bm{dy} \wedge \bm{dz}
       + \left[  \frac{\partial P}{\partial z} - \frac{\partial R}{\partial x} \right] \bm{dz} \wedge \bm{dx}
       + \left[  \frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y} \right] \bm{dx} \wedge \bm{dy}
       \\
    &= \left[  \frac{\partial R}{\partial y} - \frac{\partial Q}{\partial z} \right] \bm{\hat{\textbf{\i}}}
       + \left[  \frac{\partial P}{\partial z} - \frac{\partial R}{\partial x} \right] \bm{\hat{\textbf{\j}}}
       + \left[  \frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y} \right] \bm{\hat{\textbf{k}}}\end{aligned}
\end{equation*}

The curl of a vector in three dimensions can also be compactly written
as

\begin{equation*}
\nabla \times (P \bm{\hat{\textbf{\i}}}+ Q \bm{\hat{\textbf{\j}}}+ R\bm{\hat{\textbf{k}}})
    = \begin{vmatrix}
        \bm{\hat{\textbf{\i}}}& \bm{\hat{\textbf{\j}}}& \bm{\hat{\textbf{k}}}\\
        \frac{\partial }{\partial x} & \frac{\partial }{\partial y} & \frac{\partial }{\partial z}\\
        P & Q & R
    \end{vmatrix}
\end{equation*}

\section{Laplacian}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---laplacian}

\begin{equation*}
\nabla^2\phi = \nabla \bm{\cdot}(\nabla \phi)
    = \frac{\partial ^2\phi}{\partial x_1^2} + \frac{\partial ^2\phi}{\partial x_2^2} + \cdots + \frac{\partial ^2\phi}{\partial x_n^2}
\end{equation*}

Note that both \(\phi\) and \(\nabla^2\phi\) are
scalar fields.

\section{Classifications}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---classifications}

\subsection{Conservative}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---conservative}

A vector field \(\bm{F}\) is called \emph{conservative} if
\(\exists\) a scalar function \(\phi\) such that

\begin{equation*}
\bm{F} = \nabla \phi
\end{equation*}

\subsection{Incompressible / solenoidal}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---incompressible-solenoidal}

A vector field \(\bm{F}\) is called \emph{incompressible} or
\emph{solenoidal} if

\begin{equation*}
\nabla \bm{\cdot}\bm{F} = 0
\end{equation*}

\subsection{Irrotational}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---irrotational}

A vector field \(\bm{F}\) is called \emph{irrotational} if

\begin{equation*}
\nabla \times \bm{F} = \bm{0}
\end{equation*}

Note that a conservative field with continuous partial derivatives is
necessarily irrotational by Clairaut’s Theorem. That is

\begin{equation*}
\nabla \times (\nabla \phi) = \bm{0}
\end{equation*}

However, if all \(\bm{F}\) has continuous partial derivatives
and \(\nabla \times \bm{F} = \bm{0}\), then
\(\bm{F}\) is conservative.

\subsection{Laplacian (oh how creative)}
\label{develop--math2270:ROOT:page--operators-and-classifications.adoc---laplacian-oh-how-creative}

A scalar field \(\phi\) is called \emph{laplacian} if

\begin{equation*}
\nabla^2 \phi = 0
\end{equation*}
\end{document}
