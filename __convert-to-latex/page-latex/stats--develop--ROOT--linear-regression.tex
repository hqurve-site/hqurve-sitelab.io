\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Linear regression}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Consider the linear model with \(k\) independent variables

\begin{equation*}
Y = \vec{X}^T\vec{\beta} + \varepsilon
\end{equation*}

where \(\varepsilon \sim N(0, \sigma^2)\) is the random error
and \(\vec{\beta} \in \mathbb{R}^{1\times k}\) is the parameter vector.

We can estimate the population parameters as

\begin{equation*}
\hat{\beta}_{MLE} = (\mat{X}^T\mat{X})^{-1}\mat{X}^TY
,\quad\text{and}\quad
\hat{\sigma}_{MLE}^2 = \frac{1}{n} \sum_{i=1}^n\varepsilon_i^2
\end{equation*}

where \(\mat{X}\) is a matrix of rows \(\vec{X}_i^T\),
\(\mat{Y}\) is a column vector of \(Y_i\) and
\(\epsilon_i = Y_i-\vec{X}_i^T\vec{\beta}\).

\section{Application of model to data}
\label{develop--stats:ROOT:page--linear-regression.adoc---application-of-model-to-data}

Suppose we have sample \((\vec{X}_1, Y_1), \ldots (\vec{X}_n, Y_n)\).
In order to use the linear regression model, we need to ensure the following
assumptions hold:

\begin{description}
\item[Linearity] There is a linear relationship between the \(\vec{X}_i\) and \(Y_i\)
\item[Normality of error] The \(\varepsilon_i\) are normally distributed with mean zero
\item[Independence] The \(\varepsilon_i\) are independent
\item[Homoscedasicity] The \(\varepsilon_i\) have constant variance
\end{description}

We can use the output of the \texttt{lm} function to test these assumptions.
The following code generates random data for a linear model and fits it

\begin{listing}[{}]
set.seed(100)
n <- 100
Beta <- c(0.5, 1.2, -1.8, 3.6, 2.1)

X <- cbind(rep(1, n), rnorm(n), rnorm(n), rnorm(n), rnorm(n))
Y <- X %*% Beta + rnorm(n)

p <- ncol(X)

result <- lm(Y ~ X[,2] + X[,3] + X[,4] + X[,5])
\end{listing}

We must first assess the validity of each assumption prior to using the estimated parameters.

\subsection{Assessing assumptions}
\label{develop--stats:ROOT:page--linear-regression.adoc---assessing-assumptions}

In order to assess assumptions, we assess the residuals.
For all assumptions besides normality, we use a plot of residuals vs predicted value

\begin{listing}[{}]
plot(result$fitted.values, result$residuals)
abline(h=0)
\end{listing}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/residuals-vs-fitted}
\end{figure}

\begin{admonition-note}[{}]
The following images were obtained from the math2275 lecture notes in Semester 2 of 2020-2021
and taught by Mr. Brendon Bhagwandeen.
\end{admonition-note}

\begin{description}
\item[Linearity] In the plot, the residuals are evenly distributed about the x-axis.
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/linear}
    \end{figure}
\item[Independence] In the plot, the residuals are evenly distributed about the x-axis.
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/independent}
    \end{figure}
\item[Homoscedasicity] In the plot, the spread of the residuals does not vary with the fitted value
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/constant-variance}
    \end{figure}
\end{description}

Normality is assessed using a qqplot of the residuals.

\begin{listing}[{}]
qqnorm(result$residuals)
qqline(result$residuals)
\end{listing}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/residuals-qq}
\end{figure}

The data should be evenly distributed about the line y=x

\subsection{Interpretting results}
\label{develop--stats:ROOT:page--linear-regression.adoc---interpretting-results}

Once we have assessed the assumptions, we use the following code to produce
the estimated parameters as well and perform corresponding hypothesis tests

\begin{listing}[{}]
summary(result)
\end{listing}

which produces output

\begin{listing}[{}]
Call:
lm(formula = Y ~ X[, 2] + X[, 3] + X[, 4] + X[, 5])

Residuals:
     Min       1Q   Median       3Q      Max
-2.92071 -0.57369  0.09528  0.74823  2.06544

Coefficients:
            Estimate Std. Error t value Pr(>|t|)
(Intercept)   0.3616     0.1080   3.347  0.00117 **
X[, 2]        1.1784     0.1075  10.964  < 2e-16 ***
X[, 3]       -1.8842     0.1421 -13.256  < 2e-16 ***
X[, 4]        3.7264     0.1089  34.230  < 2e-16 ***
X[, 5]        2.0261     0.1011  20.047  < 2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.077 on 95 degrees of freedom
Multiple R-squared:  0.9587,    Adjusted R-squared:  0.957
F-statistic: 551.5 on 4 and 95 DF,  p-value: < 2.2e-16
\end{listing}

The p-values for each parameter is a t-test which tests against the null-hypothesis that the parameter is zero.

The F-statistic is corresponds to commonly referred OMNIBUS F-test
and is testing against the null-hypothesis that all \textbf{coefficient} parameters are zero.
A small p-value indicates to reject the null-hypothesis and hence at least one parameter is non-zero.
Note that when there is only coefficient parameter, this test is equivalent to the t-test for the single parameter.

\begin{admonition-note}[{}]
We can additionally use the \texttt{anova(result)} function to get the F-statistic instead of \texttt{summary(result)}.
The output is the same for the F-statistic.
\end{admonition-note}

\section{Logistic Regression}
\label{develop--stats:ROOT:page--linear-regression.adoc---logistic-regression}

In logistic regression, we wish to predict a probability.
To do this, we use the model

\begin{equation*}
P(.|\vec{x}) = \frac{1}{1+\exp\{-(\beta_0 + \beta_1x_1 + \cdots + \beta_n x_n + \varepsilon)\}}
\end{equation*}

By using function

\begin{equation*}
\mathrm{logit}(p) = \ln\left(\frac{p}{1-p}\right)
\end{equation*}

We can transform the logistic regression problem into a linear regression problem.
\end{document}
