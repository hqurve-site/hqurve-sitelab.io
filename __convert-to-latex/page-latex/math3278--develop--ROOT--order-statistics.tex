\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Order Statistics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

% Title omitted
Up till now, we have concerned ourselves with data sets and statistics from
which order does not matter. Now, consider the set of iid varaibles \(X_1\ldots X_n\)
then, we define \(Y_{i}\) (or \(X_{(i)}\)) as the \emph{\(i\)'th ordered statistic}
defined by

\begin{equation*}
Y_1 \leq Y_2 \leq \cdots \leq Y_{n-1} \leq Y_n
\end{equation*}

where the \(Y_i\) are simply a permuation of the \(X_i\).

\section{Distributions of order statistics}
\label{develop--math3278:ROOT:page--order-statistics.adoc---distributions-of-order-statistics}

Consider \(1 \leq r \leq n\) and we define \(G_r\) to be the df of \(Y_r\).
Then,

\begin{equation*}
\begin{aligned}
P(Y_r \leq y_r)
&= P(\min\{ X_i\} \leq y_r)
\\&= P(\text{at least } r\ X_i \leq y_r)
\\&= P(Z \geq r) \quad\text{where}\ Z \sim Bin(n, F(y_r))
\\&= \sum_{k=r}^n P(Z = r)
\\&= \sum_{k=r}^n \binom{n}{k}F(y_r)^k [1-F(y_r)]^{n-k}
\end{aligned}
\end{equation*}

And if the pdf of the \(X_i\)'s exist and is \(f\), so does of the pdf of \(Y_r\)
which is given by

\begin{equation*}
g_r(y_r)= \frac{n!}{(r-1)!(n-r)!}F(y_r)^{r-1}f(y_r)[1-F(y_r)]^{n-r}
\end{equation*}

In particular,

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {df} & {pdf} \\
\hline
{Minimum, \(y_1\)} & {\(1-[1-F(y_1)]^{n}\)} & {\(nf(y_1)[1-F(y_1)]^{n-1}\)} \\
\hline
{Maximum, \(y_n\)} & {\(F(y_n)^n \)} & {\(nf(y_n)F(y_n)^{n-1}\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{example}[{Proper method}]
\begin{equation*}
\begin{aligned}
g_r(t)
&= \frac{d}{dt}G_r(t)
\\&= \frac{d}{dt}\sum_{k=r}^n \binom{n}{k} F(t)^k[1-F(t)]^{n-k}
\\&= \sum_{k=r}^n \binom{n}{k}f(t)\left[kF(t)^{k-1}[1-F(t)]^{n-k} - (n-k)F(t)^k[1-F(t)]^{n-k-1}\right]
\\&= \sum_{k=r}^n k\binom{n}{k}f(t) F(t)^{k-1}[1-F(t)]^{n-k} - \sum_{k=r}^n(n-k)\binom{n}{k}f(t)F(t)^k[1-F(t)]^{n-k-1}
\\&= \left[r\binom{n}{r}f(t) F(t)^{r-1}[1-F(t)]^{n-r}+  \sum_{k=r+1}^n k\binom{n}{k}f(t) F(t)^{k-1}[1-F(t)]^{n-k}\right]
\\&\quad\quad - \left[\sum_{k=r}^{n-1}(n-k)\binom{n}{k}f(t)F(t)^k[1-F(t)]^{n-k-1}\right]
\\&= \left[r\binom{n}{r}f(t) F(t)^{r-1}[1-F(t)]^{n-r}+  \sum_{k=r}^{n-1} (k+1)\binom{n}{k+1}f(t) F(t)^{k}[1-F(t)]^{n-k-1}\right]
\\&\quad\quad - \left[\sum_{k=r}^{n-1}(n-k)\binom{n}{k}f(t)F(t)^k[1-F(t)]^{n-k-1}\right]
\\&= \left[r\binom{n}{r}f(t) F(t)^{r-1}[1-F(t)]^{n-r}+  \sum_{k=r}^{n-1} n\binom{n-1}{k}f(t) F(t)^{k}[1-F(t)]^{n-k-1}\right]
\\&\quad\quad - \left[\sum_{k=r}^{n-1}n\binom{n-1}{k}f(t)F(t)^k[1-F(t)]^{n-k-1}\right]
\\&= \left[r\binom{n}{r}f(t) F(t)^{r-1}[1-F(t)]^{n-r}+  \sum_{k=r}^{n-1} n\binom{n-1}{k}f(t) F(t)^{k}[1-F(t)]^{n-k-1}\right]
\\&\quad\quad - \left[\sum_{k=r}^{n-1}n\binom{n-1}{k}f(t)F(t)^k[1-F(t)]^{n-k-1}\right]
\\&= r\binom{n}{r}f(t) F(t)^{r-1}[1-F(t)]^{n-r}
\\&= \frac{n!}{(r-1)!(n-r)!}f(t) F(t)^{r-1}[1-F(t)]^{n-r}
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Huristic Method}]
Consider the point \(y_r\) and small difference \(\delta y_r\). Then, consider the following

\begin{equation*}
P(y_r < Y_r < y_r + \delta y_r)
= \int_{y_r}^{y_r+\delta y_r} g_r(t) dt
\approx g_r(y_r) \delta y_r
\end{equation*}

On the other hand, we can claim that \(\delta y_r\) is so small that only one \(X_i\)
exists in the interval \((y_r, y_r  +\delta y_r)\). There are then \(r-1\) \(X_i\)
in the lower interval and \(n-r\) in the upper interval. Therefore, the probability is given by

\begin{equation*}
\begin{aligned}
P(y_r < Y_r < y_r + \delta y_r)
= \binom{n}{(r-1), 1, (n-r)} F(y_r)^{r-1} [F(y_r + \delta y_r) - F(y_r) ][1-F(y_r + \delta y_r)]^{n-r}
\end{aligned}
\end{equation*}

Hence, we get that

\begin{equation*}
g_r(t) \approx \frac{1}{\delta y_r}\binom{n}{(r-1), 1, (n-r)} F(y_r)^{r-1} [F(y_r + \delta y_r) - F(y_r) ][1-F(y_r + \delta y_r)]^{n-r}
\end{equation*}

and as \(\delta y_r \to 0\),

\begin{equation*}
\begin{aligned}
g_r(t)
&= \binom{n}{(r-1), 1, (n-r)} F(y_r)^{r-1} \left[\lim_{\delta y_r \to 0}\frac{F(y_r + \delta y_r) - F(y_r)}{\delta y_r} \right][1-F(y_r)]^{n-r}
\\&= \binom{n}{(r-1), 1, (n-r)} F(y_r)^{r-1} f(y_r)[1-F(y_r)]^{n-r}
\\&= \frac{n!}{(r-1)!(n-r)!} F(y_r)^{r-1} f(y_r)[1-F(y_r)]^{n-r}
\end{aligned}
\end{equation*}
\end{example}

\section{Joint Distributions of order statistics}
\label{develop--math3278:ROOT:page--order-statistics.adoc---joint-distributions-of-order-statistics}

Consider a random sample \(X_1, \ldots X_n\) with pdf \(f\) and distirbution function \(F\).
Further, consider \(1 \leq r_1 < r_2 < \cdots < r_k < n\), and
let  \(g\) be the joint pdf of \((Y_{r_1}, \ldots Y_{r_k})\).
Then

\begin{equation*}
g(y_{r_1}, \ldots y_{r_k})
= n!
    \frac{F(y_{r_1})^{r_1-1}}{(r_1-1)!}f(y_{r_1})\left[\prod_{i=2}^k \frac{[F(y_{r_i}) - F(y_{r_{i-1}})]^{r_i - r_{i-1}-1}}{(r_i-r_{i-1})!} f(y_{r_i})\right]\frac{[1-F(y_{r_k})]^{n-r_k}}{(n-r_k)!}
\end{equation*}

which has support \(y_{r_1} < y_{r_2} < \cdots < y_{r_k}\)

Most notably, the joint pdf reduces to the following when \(r_i=i\) and \(k=n\) (ie the joint pdf of all order statistics)

\begin{equation*}
g(y_1, y_2\ldots y_n) = n! \prod_{i=1}^n f(y_i)
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
Although I initially thought the huristic method was just a huristic and not accurate, it can be manipulated to become
slightly more rigourous.
\end{admonition-note}

Firstly, note that if any two \(X_i\) are equal the joint pdf is \(0\) since they are continuous. Therefore, we
can choose \(\delta y_i\) such that there is only one \(X_i\) in each \((y_i, \delta y_i)\) and each interval is disjoint. Then,
we can partition number line into

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math3278/ROOT/order-stats-1}
\end{figure}

hence

\begin{equation*}
\begin{aligned}
P(y_{r_1} < Y_{r_1}& < y_{r_1} + \delta y_{r_1}, \ldots y_{r_k} < Y_{r_k} < y_{r_k} + \delta y_{r_k})
\\&= \binom{n}{(r_1-1), 1, (r_2-1)\cdots 1,(n-r_k)}
   \\&\hspace{4em}  F(y_{r_1})^{r_1-1}[F(y_{r_1} + \delta y_{r_1}) - F(y_{r_1})]
   \\&\hspace{4em} [F(y_{r_2}) - F(y_{r_1}+\delta y_{r_1})]^{r_2-r_1-1}[F(y_{r_2} + \delta y_{r_2}) - F(y_{r_2})]
   \\&\hspace{4em} \cdots
   \\&\hspace{4em} [F(y_{r_k}) - F(y_{r_{k-1}}+\delta y_{r_{k-1}})]^{r_k-r_{k-1}-1}[F(y_{r_k} + \delta y_{r_k}) - F(y_{r_k})]
   \\&\hspace{4em} [1 - F(y_{r_{k-1}}+\delta y_{r_{k}})]^{n-r_k}
\\&= n!
    \frac{F(y_{r_1})^{r_1-1}}{(r_1-1)!}[F(y_{r_1} + \delta y_{r_1}) - F(y_{r_1})]
    \\&\hspace{4em}\left[\prod_{i=2}^k \frac{[F(y_{r_i}) - F(y_{r_{i-1}})]^{r_i - r_{i-1}-1}}{(r_i-r_{i-1})!} [F(y_{r_i} + \delta y_{r_i}) - F(y_{r_i})]\right]
    \\&\hspace{4em}\frac{[1-F(y_{r_k})]^{n-r_k}}{(n-r_k)!}
\end{aligned}
\end{equation*}

Now, on one hand

\begin{equation*}
\begin{aligned}
\lim_{\|\delta y_{r_1},\ldots \delta y_{r_k} \|\to 0} &
\frac{P(y_{r_1} < Y_{r_1} < y_{r_1} + \delta y_{r_1}, \ldots y_{r_k} < Y_{r_k} < y_{r_k} + \delta y_{r_k})}{\delta y_{r_1} \ldots \delta y_{r_k}}
\\&= \lim_{\|\delta y_{r_1},\ldots \delta y_{r_k} \|\to 0}\frac{G(y_{r_1} + \delta y_{r_1},\ldots y_{r_k} + \delta y_{r_k})  -  G(y_{r_1},\ldots y_{r_k})} {\delta y_{r_1} \ldots \delta y_{r_k}}
\\&= g(y_{r_1},\ldots y_{r_k})
\end{aligned}
\end{equation*}

\begin{admonition-caution}[{}]
That definition of the derivaitive used above is questionable as far as I could tell; I have to verify it.
\end{admonition-caution}

and on the other

\begin{equation*}
\begin{aligned}
\lim_{\|\delta y_{r_1},\ldots \delta y_{r_k} \|\to 0} &
\frac{P(y_{r_1} < Y_{r_1} < y_{r_1} + \delta y_{r_1}, \ldots y_{r_k} < Y_{r_k} < y_{r_k} + \delta y_{r_k})}{\delta y_{r_1} \ldots \delta y_{r_k}}
\\&=\quad\lim_{\|\delta y_{r_1},\ldots \delta y_{r_k} \|\to 0} \left(
        \begin{array}{l}
    n!\frac{F(y_{r_1})^{r_1-1}}{(r_1-1)!}\frac{F(y_{r_1} + \delta y_{r_1}) - F(y_{r_1})}{\delta y_{r_1}}
    \\\hspace{4em}\left[\prod_{i=2}^k \frac{[F(y_{r_i}) - F(y_{r_{i-1}})]^{r_i - r_{i-1}-1}}{(r_i-r_{i-1})!} \frac{F(y_{r_i} + \delta y_{r_i}) - F(y_{r_i})}{\delta y_{r_i}}\right]
    \\\hspace{4em}\frac{[1-F(y_{r_k})]^{n-r_k}}{(n-r_k)!}
        \end{array}
    \right)
\\&= n!\frac{F(y_{r_1})^{r_1-1}}{(r_1-1)!}f(y_{r_1})\left[\prod_{i=2}^k \frac{[F(y_{r_i}) - F(y_{r_{i-1}})]^{r_i - r_{i-1}-1}}{(r_i-r_{i-1})!} f(y_{r_i})\right]\frac{[1-F(y_{r_k})]^{n-r_k}}{(n-r_k)!}
\end{aligned}
\end{equation*}

and by the uniqueness of the limit we are done.
\end{example}
\end{document}
