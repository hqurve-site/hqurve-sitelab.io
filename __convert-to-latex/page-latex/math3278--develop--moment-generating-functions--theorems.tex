\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Proving some nice results}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

% Title omitted
\section{Poisson approximation to binomial}
\label{develop--math3278:moment-generating-functions:page--theorems.adoc---poisson-approximation-to-binomial}

Let \(X_n \sim Bin\left(n, \frac{\lambda}{n}\right)\), then
\(X_n\) converges in distribution to \(Y \sim Possion(\lambda)\).

\begin{example}[{Proof}]
Firstly, recall that the mgf of \(X_n\) is

\begin{equation*}
M_n(t) = \left(1 + \frac{\lambda}{n}(e^t-1)\right)^n
\end{equation*}

where \(t \in \mathbb{R}\)
 [\myautoref[{see here}]{develop--math2274:ROOT:page--random/discrete/binomial.adoc---moment-generating-function}].

Then, since

\begin{equation*}
\begin{aligned}
\lim_{n\to \infty} M_n(t)
= \lim_{n\to \infty}\left(1 + \frac{\lambda}{n}(e^t-1)\right)^n
= \exp\left(\lambda(e^t-1)\right)
\end{aligned}
\end{equation*}

which is the mgf of \(Y\)
 [\myautoref[{see here}]{develop--math2274:ROOT:page--random/discrete/poisson.adoc---moment-generating-function}].
\end{example}

\section{Central limit theorem}
\label{develop--math3278:moment-generating-functions:page--theorems.adoc---central-limit-theorem}

Let \(X_1\ldots X_n\) be a random sample where \(Var(X_i) = \sigma^2 < \infty\)
and \(E(X_i) = \mu \in \mathbb{R}\). Now, let

\begin{equation*}
\overline{X} = \frac{1}{n} \sum_{i=1}^n X_i
\end{equation*}

Then, \(Y_n = \frac{\overline{X}_n - \mu}{\sigma/\sqrt{n}}\) converges in distribution
to a standard normal distribution.

\begin{example}[{Proof}]
We would firstly rewrite \(Y_n\) as

\begin{equation*}
Y_n
= \frac{\overline{X}_n - \mu}{\sigma/\sqrt{n}}
= \frac{\frac{1}{n}\sum_{i=1}^n X_i - \mu}{\sigma/\sqrt{n}}
= \frac{\frac{1}{n}\sum_{i=1}^n (X_i - \mu)}{\sigma/\sqrt{n}}
= \sum_{i=1}^n\frac{\frac{1}{n} (X_i - \mu)}{\sigma/\sqrt{n}}
= \sum_{i=1}^n\frac{X_i - \mu}{\sigma\sqrt{n}}
\end{equation*}

Let \(W_i = \frac{X_i - \mu}{\sigma}\). Then,

\begin{equation*}
E(W_i) = 0
\quad\text{and}\quad
Var(W_i) = \frac{Var(X_i)}{\sigma^2} = 1
\end{equation*}

and hence \(E(X^2) = Var(X_i) + 0 = 1\).
Then, the mgf of \(W_i\) is given by

\begin{equation*}
M_{W_i}(t) = 1 + 0 + \frac{1}{2}t^2 + t^3p_i
\end{equation*}

where \(p_i\) is a power series. Then, the mgf of \(Y_n\) is given by

\begin{equation*}
M_{Y_n}(t)
= \prod_{i=1}^n M_{W_i}\left(\frac{t}{\sqrt{n}}\right)
= \left(1+\frac{1}{2n}t^2 + \frac{1}{n^{3/2}}t^3p_i\right)^n
= \left(1+\frac{1}{n}\left[\frac{t^2}{2} + \frac{1}{\sqrt{n}}t^3p_i\right]\right)^n
\end{equation*}

and as \(n\to \infty\), \(M_{Y_n}(t) \to \exp\left(\frac{t^2}{2}\right)\)
which is the mgf of a normal distribution with mean \(0\) and variance \(1\)
 [\myautoref[{see here}]{develop--math2274:ROOT:page--random/continuous/normal.adoc---moment-generating-function}].
\end{example}

\section{Distribution of squared standard normal}
\label{develop--math3278:moment-generating-functions:page--theorems.adoc---distribution-of-squared-standard-normal}

If \(X\sim N(0,1)\) then, \(X^2\sim \bigchi^2_{1}\)

\begin{example}[{Proof}]
Let \(Y = X^2\), then

\begin{equation*}
\begin{aligned}
M_Y(t) = E(e^{tY}) = E(e^{tX^2})
&= \int_{-\infty}^\infty e^{tx^2}\frac{1}{\sqrt{2\pi}}e^{\frac{-x^2}{2}} \ dx
\\&= \int_{-\infty}^\infty \frac{1}{\sqrt{2\pi}}e^{\frac{-(1-2t)x^2}{2}} \ dx
\\&= \frac{1}{\sqrt{1-2t}}\int_{-\infty}^\infty \frac{1}{\sqrt{2\pi\left(\frac{1}{\sqrt{1-2t}}\right)^2}}e^{\frac{-x^2}{2\left(\frac{1}{\sqrt{1-2t}}\right)^2}} \ dx
\\&= \frac{1}{\sqrt{1-2t}}
\end{aligned}
\end{equation*}

Notice that the integral resolved to the total probability of a \(N\left(0, \frac{1}{1-2t}\right)\) distribution.
Then, by the uniqueness of MGF's we are done. [\myautoref[{mgf of chi-squared}]{develop--math2274:ROOT:page--random/continuous/chi-squared.adoc---moment-generation-function}]
\end{example}
\end{document}
