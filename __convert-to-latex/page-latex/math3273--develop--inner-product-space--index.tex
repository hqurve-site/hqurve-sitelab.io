\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Inner product space}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
We have two definitions for inner products, depending on the scalar field of the vectorspace

\begin{description}
\item[Real inner product space] Let \(V\) be a real vector space, then a \emph{symmetric inner product} is a function
    \(\langle \cdot, \cdot \rangle: V\times V \to \mathbb{R}\) satisfying the following properties
    
    \begin{itemize}
    \item \(\forall \vec{u}, \vec{v} \in V: \abrack{\vec{u}, \vec{v}} = \abrack{\vec{v}, \vec{u}}\)
    \item \(\forall \vec{u}, \vec{v} \in V: \forall \lambda \in \mathbb{R} \abrack{\lambda\vec{u} + \vec{w}, \vec{v}} = \lambda\abrack{\vec{u}, \vec{v}} + \abrack{\vec{w},\vec{v}}\)
    \item \(\forall \vec{v} \in V: \abrack{\vec{v},\vec{v}} \geq 0\) with equality iff \(\vec{v} = \vec{0}\)
    \end{itemize}
\item[Hermatian inner product space] Let \(V\) be a complex vector space, then a \emph{hermatian inner product} is a function
    \(\langle \cdot, \cdot \rangle: V\times V \to \mathbb{C}\) satisfying the following properties
    
    \begin{itemize}
    \item \(\forall \vec{u}, \vec{v} \in V: \abrack{\vec{u}, \vec{v}} = \overline{\abrack{\vec{v}, \vec{u}}}\)
    \item \(\forall \vec{u}, \vec{v} \in V: \forall \lambda \in \mathbb{C} \abrack{\lambda\vec{u} + \vec{w}, \vec{v}} = \overline{\lambda}\abrack{\vec{u}, \vec{v}} + \abrack{\vec{w},\vec{v}}\)
    \item \(\forall \vec{v} \in V: \abrack{\vec{v},\vec{v}} \geq 0\) with equality iff \(\vec{v} = \vec{0}\)
    \end{itemize}
\end{description}

Notice that from the first property, we know that \(\abrack{\vec{v}, \vec{v}} \in \mathbb{R}\)

\begin{admonition-note}[{}]
Functions which are hermatian inner products also are real inner products  since \(\overline{x} = x\) when \(x \in \mathbb{R}\)
Also, notice that hermatian inner products are actually linear in their second argument since

\begin{equation*}
\abrack{\vec{u}, \lambda\vec{w}  + \vec{v}}
= \overline{\abrack{\lambda\vec{w} + \vec{v}}, \vec{u}}
= \overline{\overline{\lambda}\abrack{\vec{w}, \vec{u}}+\abrack{\vec{v}, \vec{u}}}
= \lambda\overline{\abrack{\vec{w}, \vec{u}}}+\overline{\abrack{\vec{v}, \vec{u}}}
= \lambda\abrack{\vec{u}, \vec{w}}+\abrack{\vec{u}, \vec{v}}
\end{equation*}
\end{admonition-note}

\section{Cauchy-Schwartz Inequality}
\label{develop--math3273:inner-product-space:page--index.adoc---cauchy-schwartz-inequality}

The following inequality holds

\begin{equation*}
|\abrack{\vec{u},\vec{v}}|^2 \leq \abrack{\vec{u},\vec{u}}\abrack{\vec{v},\vec{v}}
\end{equation*}

where \(|z|\) is the modulus of \(z \in \mathbb{C}\).
Furthermore, equality holds iff \(\vec{u}\) and \(\vec{v}\) are
linearly dependent.

\begin{admonition-remark}[{}]
Although this was \myautoref[{previously proven}]{develop--math2273:ROOT:page--inner-products.adoc---cauchy-schwarz-inequality}
for real inner product spaces, special care needs to be taken for complex ones.
\end{admonition-remark}

\begin{example}[{Proof}]
Again, clearly if either \(\vec{u} = \vec{0}\) or \(\vec{v}= \vec{0}\),
we get the desired result. Now, let
\(\lambda = -\frac{\abrack{\vec{v}, \vec{u}}}{\abrack{\vec{v},\vec{v}}}\)
and consider

\begin{equation*}
\begin{aligned}
0 \leq \abrack{\vec{u} + \lambda\vec{v}, \vec{u}+\lambda\vec{v}}
&= \abrack{\vec{u},\vec{u}}
+ \lambda\abrack{\vec{u},\vec{v}}
+ \overline{\lambda}\abrack{\vec{v},\vec{u}}
+ \lambda\overline{\lambda}\abrack{\vec{v},\vec{v}}
\\&= \abrack{\vec{u},\vec{u}}
-\frac{\abrack{\vec{v}, \vec{u}}}{\abrack{\vec{v},\vec{v}}} \abrack{\vec{u},\vec{v}}
- \frac{\overline{\abrack{\vec{v}, \vec{u}}}}{\abrack{\vec{v},\vec{v}}}\abrack{\vec{v},\vec{u}}
+
\frac{\abrack{\vec{v}, \vec{u}}}{\abrack{\vec{v},\vec{v}}}
\frac{\overline{\abrack{\vec{v}, \vec{u}}}}{\abrack{\vec{v},\vec{v}}}
\abrack{\vec{v},\vec{v}}
\\&= \abrack{\vec{u},\vec{u}}
-\frac{\overline{\abrack{\vec{u}, \vec{u}}} \abrack{\vec{u},\vec{v}}}{\abrack{\vec{v},\vec{v}}}
- \frac{\abrack{\vec{u}, \vec{v}}\overline{\abrack{\vec{u},\vec{v}}}}{\abrack{\vec{v},\vec{v}}}
+
\frac{\overline{\abrack{\vec{u}, \vec{v}}}\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{v},\vec{v}}}
\\&= \abrack{\vec{u},\vec{u}}
-\frac{\overline{\abrack{\vec{u}, \vec{u}}} \abrack{\vec{u},\vec{v}}}{\abrack{\vec{v},\vec{v}}}
\\&= \abrack{\vec{u},\vec{u}}
-\frac{\left|\abrack{\vec{u}, \vec{u}}\right|^2}{\abrack{\vec{v},\vec{v}}}
\end{aligned}
\end{equation*}

We now rearrange to get
that

\begin{equation*}
|\abrack{\vec{u},\vec{v}}|^2 \leq \abrack{\vec{u},\vec{u}}\abrack{\vec{v},\vec{v}}
\end{equation*}

where equality holds iff

\begin{equation*}
0 = \abrack{\vec{u} + \lambda\vec{v}, \vec{u}+\lambda\vec{v}}
\iff
\vec{0} = \vec{u} + \lambda\vec{v}
\end{equation*}

and hence we are done.
\end{example}

\section{Orthogonality}
\label{develop--math3273:inner-product-space:page--index.adoc---orthogonality}

Let \(\vec{u}, \vec{v} \in V\). Then \(\vec{u}\) and \(\vec{v}\) are orthogonal if \(\abrack{\vec{u}, \vec{v}} = 0\) and we
write \(\vec{u} \perp \vec{v}\).

\begin{description}
\item[Orthagonality of sets] Let \(S, T \subseteq V\), then \(S\) and \(V\) are \emph{orthagonal} if
    
    \begin{equation*}
    \forall \vec{s} \in S: \forall \vec{t} \in T: \vec{s} \perp \vec{t}
    \end{equation*}
    
    and we write, \(S \perp V\)
\item[Orthaonal Subspace] Let \(S \subseteq V\), then we define
    
    \begin{equation*}
    S^\perp = \{\vec{v} \in V \ | \ \forall \vec{s} \in S: \vec{v}\perp \vec{s} \}
    \end{equation*}
    
    then \(S^\perp\) is a subspace of \(V\).
\item[Mutually Orthogonal] Let \(S = \{\vec{v}_1, \ldots\vec{v}_n\}\), then, we say that the vectors in \(S\)
    are \emph{mutually orthognal} if
    
    \begin{equation*}
    \abrack{\vec{v}_i, \vec{v}_j} = \delta_{ij} =
    \begin{cases}
    1 \quad&\text{if } i=j\\
    0 \quad&\text{if } i\neq j
    \end{cases}
    \end{equation*}
    
    where \(\delta_{ij}\) is the
    \href{https://en.wikipedia.org/wiki/Kronecker\_delta}{Kronecker delta function}
\end{description}

\section{Coordinate vector in orthonormal basis}
\label{develop--math3273:inner-product-space:page--index.adoc---coordinate-vector-in-orthonormal-basis}

Let \(\vec{u}_1\ldots \vec{u}_n\) form an orthonormal basis for \(V\). Then
\(\forall \vec{v} \in V\)

\begin{equation*}
\vec{v} = \sum_{i=1}^n \abrack{\vec{u}_i, \vec{v}} \vec{u}_i
\end{equation*}

Notice that this formula arises from the linearity of the second argument.

\subsection{Projections}
\label{develop--math3273:inner-product-space:page--index.adoc---projections}

\begin{admonition-tip}[{}]
You likely also want to read \myautoref[{}]{develop--math3273:inner-product-space:page--projection.adoc} and
\myautoref[{}]{develop--math3273:inner-product-space:page--orthogonal-projections.adoc}.
\end{admonition-tip}

Let \(\vec{u},\vec{v} \in V\), then we define the
\emph{projection of \(\vec{v}\) onto \(\vec{u}\)} as the

\begin{equation*}
P_{\vec{u}}(\vec{v}) = \frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}} \vec{u}
\end{equation*}

Then, the following properties hold for \(\vec{x} = P_{\vec{u}}(\vec{v})\)

\begin{itemize}
\item \(\vec{x} \perp (\vec{v} - \vec{x})\)
\item \(\vec{x} = \vec{x} + (\vec{v} - \vec{x})\)
\item \(P_{\vec{u}}\) is \href{https://en.wikipedia.org/wiki/Idempotence}{idempotent}.
    That is \(P_{\vec{u}}^2 = P_{\vec{u}}\) (repeated function application)
\end{itemize}

\begin{example}[{Proof}]
Firstly,

\begin{equation*}
\abrack{\vec{x}, \vec{v} - \vec{x}}
= \abrack{
    \frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}} \vec{u},
    \vec{v} - \frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}} \vec{u}
}
=
\frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}} \abrack{\vec{u}, \vec{v}}
- \frac{\abrack{\vec{u}, \vec{v}}^2}{\abrack{\vec{u},\vec{u}}^2} \abrack{\vec{u}, \vec{u}}
= 0
\end{equation*}

And, notice that

\begin{equation*}
P_{\vec{u}}(P_{\vec{u}}(\vec{v}))
= \frac{\abrack{\vec{u}, P_{\vec{u}}(\vec{v})}}{\abrack{\vec{u},\vec{u}}} \vec{u}
= \frac{\abrack{\vec{u}, \frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}} \vec{u}}}{\abrack{\vec{u},\vec{u}}} \vec{u}
= \frac{\frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}}\abrack{\vec{u},  \vec{u}}}{\abrack{\vec{u},\vec{u}}} \vec{u}
= \frac{\abrack{\vec{u}, \vec{v}}}{\abrack{\vec{u},\vec{u}}} \vec{u}
= P_{\vec{u}}(\vec{v})
\end{equation*}
\end{example}

Further, we could extend this function to a subspace \(W\) with orthonormal basis
\(\vec{w}_1,\ldots \vec{w}_k\). Then, we define

\begin{equation*}
P_W(\vec{v}) = \sum_{i=1}^k P_{\vec{w}_i}(\vec{v}) = \sum_{i=1}^k \abrack{\vec{w}_i, \vec{v}}\vec{w}_i
\end{equation*}

\section{Linear functionals}
\label{develop--math3273:inner-product-space:page--index.adoc---linear-functionals}

Let \(V\) be a vector space over with scalar
field \(\mathbb{K}\). Then a \emph{(linear) functional} is a
linear transformation in \(\mathcal{L}(V, \mathbb{K})\).
Furthermore, if \(V\) is a finite dimensional inner product space,
\(V\) is isomorphic to \(\mathcal{L}(V, \mathbb{K})\) with isomorphism such that each
\(\vec{v} \in V\) is
mapped to \(f_{\vec{v}}\) defined by

\begin{equation*}
f_{\vec{v}}(\vec{w}) = \abrack{\vec{v},\vec{w}}
\end{equation*}

\begin{admonition-remark}[{}]
Perhaps the fact that this is an isomorphism isnt that important
but rather the fact that it is a bijection.
\end{admonition-remark}

\begin{example}[{Proof}]
Firstly notice that this is homomorphism since it is a linear transformation, the sum
of functionals is the functional of the sum and the scalar multiple of the function
is the functional of the scalar multiple (if \(\mathbb{K} = \mathbb{R}\)). Next,
notice that no two functionals are the same since if

\begin{equation*}
f_{\vec{v}} = f_{\vec{v}'}
\implies \forall \vec{w} \in V:\abrack{\vec{v} - \vec{v}', \vec{w}} = 0
\implies \vec{v} = \vec{v}'
\end{equation*}

by specifically setting \(\vec{w} = \vec{v} - \vec{v}'\). Finally, suppose we have
\(g \in \mathcal{L}\) and orthonormal basis \(\{\vec{u}_1, \ldots \vec{u}_n\}\)
of \(V\). Then,

\begin{equation*}
g(\vec{v})
= g\left(\sum_{i=1}^n \abrack{\vec{u}_i, \vec{v}} \vec{u}_i\right)
= \sum_{i=1}^n \abrack{\vec{u}_i, \vec{v}} g\left( \vec{u}_i\right)
= \sum_{i=1}^n \abrack{\overline{g\left( \vec{u}_i\right)}\vec{u}_i, \vec{v}}
=  \abrack{\sum_{i=1}^n\overline{g\left( \vec{u}_i\right)}\vec{u}_i, \vec{v}}
\end{equation*}

and hence we have found a vector which defines functional \(g\).
\end{example}

\section{Adjoint}
\label{develop--math3273:inner-product-space:page--index.adoc---adjoint}

Let \(V\) an \(W\) be inner product spaces over common
field \(\mathbb{K}\) and \(T \in \mathcal{L}(V,W)\)
then we define the adjoint \(T^* \in \mathcal{L}(W,V)\) of \(T\)
to be such that

\begin{equation*}
\forall \vec{v} \in V: \forall \vec{w} \in W:
\abrack{\vec{w}, T(\vec{v})}
= \abrack{T^*(\vec{w}), \vec{v}}
\end{equation*}

\begin{admonition-caution}[{}]
The above two inner products are not necessarily the same, further,
they operate on two different vector spaces.
\end{admonition-caution}

\begin{example}[{Proof of existence and uniqueness if \(V\) is finite dimensional}]
What we want to show is that for each \(\vec{w} \in W\) there exists
unique \(\vec{w}^* \in V\) such that

\begin{equation*}
\forall \vec{v} \in V:
\abrack{\vec{w}, T(\vec{v})}
= \abrack{\vec{w}^*, \vec{v}}
\end{equation*}

we would then define \(T^*(\vec{w}) = \vec{w}^*\).

Firstly, we fix \(\vec{w}\) and define \(f: V\to\mathbb{K}\)
such that

\begin{equation*}
f(\vec{v}) = \abrack{\vec{w}, T(\vec{v})}
\end{equation*}

Then, \(f \in \mathcal{L}(V, \mathbb{K})\) and hence there exists
unique \(\vec{w}^*\in V\) (see \myautoref[{above}]{develop--math3273:inner-product-space:page--index.adoc---linear-functionals}) such that

\begin{equation*}
\forall \vec{v} \in V: f(\vec{v}) = \abrack{\vec{w}^*, \vec{v}}
\end{equation*}

Next, we want to show that this is a linear transformation.
Consider \(\vec{x} = \vec{w} + \alpha \vec{u} \in W\). Then, \(\exists \vec{u}^* \in V\)
such that

\begin{equation*}
\forall \vec{v} \in V: \abrack{\vec{u}^*, \vec{v}} = \abrack{\vec{u}, T(\vec{v})}
\end{equation*}

and \(\exists \vec{x}^* \in V\) such that

\begin{equation*}
\forall \vec{v} \in V: \abrack{\vec{x}^*, \vec{v}} = \abrack{\vec{x}, T(\vec{x})}
\end{equation*}

Therefore

\begin{equation*}
\begin{aligned}
\forall \vec{v} \in V:
&\abrack{\vec{x}^* - \vec{w}^* - \alpha\vec{u}^*, \vec{v}}
\\&= \abrack{\vec{x}^*, \vec{v}} - \abrack{\vec{w}^*, \vec{v}} - \overline{\alpha}\abrack{\vec{u}^*, \vec{v}}
\\&= \abrack{\vec{x}, T(\vec{v})} - \abrack{\vec{w}, T(\vec{v})} - \overline{\alpha}\abrack{\vec{u}, T(\vec{v})}
\\&= \abrack{\vec{x} - \vec{w} - \alpha \vec{u}, T(\vec{v})}
\\&= \abrack{\vec{0}, T(\vec{v})}
\\&= 0
\end{aligned}
\end{equation*}

and hence \(\vec{x}^* = \vec{w}^* + \alpha\vec{u}^*\) as desired.
\end{example}

\subsection{Operations on adjoints}
\label{develop--math3273:inner-product-space:page--index.adoc---operations-on-adjoints}

Let \(T, S \in \mathcal{L}(V, W)\) then,

\begin{itemize}
\item \((T+S)^* = T^* + S^*\)
\item \((\alpha T)^* = \overline{\alpha}T^*\)
\item If \(V = W\), \((ST)^* = T^* S^*\)
\end{itemize}

\begin{example}[{Proof}]
Consider arbirary \(\vec{w} \in W\) then

\begin{equation*}
\begin{aligned}
\forall \vec{v} \in V:
\abrack{(T+\alpha S)^*(\vec{w}), \vec{v}}
&= \abrack{\vec{w}, (T+\alpha S)(\vec{v})}
\\&= \abrack{\vec{w}, T(\vec{v})}
+ \alpha\abrack{\vec{w}, S(\vec{v})}
\\&=\abrack{T^*(\vec{w}), \vec{v}}
+ \alpha\abrack{S^*(\vec{w}), \vec{v}}
\\&=\abrack{(T^* + \overline{\alpha}S^*)(\vec{w}), \vec{v}}
\end{aligned}
\end{equation*}

Next,

\begin{equation*}
\begin{aligned}
\forall \vec{v} \in V:
\abrack{(ST)^*(\vec{w}), \vec{v}}
&= \abrack{\vec{w}, (ST)(\vec{v})}
\\&= \abrack{\vec{w}, S(T(\vec{v}))}
\\&= \abrack{S^*(\vec{w}), T(\vec{v})}
\\&= \abrack{T^*(S^*(\vec{w})), \vec{v}}
\end{aligned}
\end{equation*}
\end{example}

\subsection{Matricies}
\label{develop--math3273:inner-product-space:page--index.adoc---matricies}

If \(V\) and \(W\) consist of column vectors
and the inner product is the standard dot product, notice that

\begin{equation*}
\forall \vec{v} \in V:
\forall \vec{w} \in W:
\abrack{\vec{w}, T\vec{v}}
= \vec{w}^* T\vec{v}
= (T^*\vec{w})^*\vec{v}
= \abrack{T^*\vec{w}, \vec{v}}
\end{equation*}

Where \(T^*\) is the conjugate transpose of \(T\).
\end{document}
