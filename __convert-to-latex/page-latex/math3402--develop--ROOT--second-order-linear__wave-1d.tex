\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{One dimensional wave equation}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Consider the following second order PDE

\begin{equation*}
u_{tt} = k^2u_{xx}
\end{equation*}

where \(k \neq 0\).
Then, by rearranging the equation, we get

\begin{equation*}
k^2u_{xx}- u_{tt} = 0
\end{equation*}

which is hyperbolic since \(-k^2 -0 < 0\) and hence has general solution

\begin{equation*}
u(x,t) = F(x + \lambda_1 t) + G(x + \lambda_2 t)
\end{equation*}

by using \myautoref[{the perviously derived forumla}]{develop--math3402:ROOT:page--second-order-linear/index.adoc---simple-solving}.
where \(k^2 -\lambda^2 =0\) and hence \(\lambda = \pm k\). That is
the general solution is

\begin{equation*}
u(x,t) = F(x + kt) + G(x-kt)
\end{equation*}

\section{D'Alembert's solution: Initial Value problem}
\label{develop--math3402:ROOT:page--second-order-linear/wave-1d.adoc--dalembert}

This pde is used to describe the vibrations in a string without endpoints. That is
an infinite string given the following initial conditions

\begin{equation*}
u(x,0) = \phi(x)
\quad\text{and}\quad
u_t(x,0) = \psi(x)
\end{equation*}

The solution is

\begin{equation*}
u(x,t)= \frac{1}{2} [\phi(x+kt)+\phi(x-kt)] + \frac{1}{2k}\int_{x-kt}^{x+kt} \psi(s) \ ds
\end{equation*}

\begin{example}[{Derivation}]
Firstly, from above, our general solution is given by

\begin{equation*}
u(x,t) = F(x + kt) + G(x-kt)
\end{equation*}

and by substituting \(t=0\), we get that \(\phi(x) = F(x) + G(x)\).
Also,

\begin{equation*}
u_t(x,t) = kF'(x+kt) - kG'(x-kt)
\end{equation*}

and by substituting \(t=0\), we get that \(\psi(x) = kF'(x) - kG'(x)\)
and by integrating

\begin{equation*}
\int_{\alpha}^x \psi(s) \ ds = kF(x) - k G(x)
\end{equation*}

for some \(\alpha\). We then combine this with the previous equation to get

\begin{equation*}
\begin{aligned}
&F(x) = \frac{1}{2}\phi(x) + \frac{1}{2k}\int_{\alpha}^x \psi(s) \ ds
\\&
G(x) = \frac{1}{2}\phi(x) - \frac{1}{2k}\int_{\alpha}^x \psi(s) \ ds
\end{aligned}
\end{equation*}

Therefore our solution is

\begin{equation*}
\begin{aligned}
u(x,t)
&= F(x  +kt) + G(x-kt)
\\&= \frac{1}{2} \phi(x+kt) + \frac{1}{2k}\int_{\alpha}^{x+kt} \psi(x) \ ds
+ \frac{1}{2} \phi(x-kt) + \frac{1}{2k}\int_{\alpha}^{x-kt} \psi(s) \ ds
\\&= \frac{1}{2} [\phi(x+kt)+\phi(x-kt)] + \frac{1}{2k}\int_{x-kt}^{x+kt} \psi(s) \ ds
\end{aligned}
\end{equation*}
\end{example}

\section{Boundary value problem}
\label{develop--math3402:ROOT:page--second-order-linear/wave-1d.adoc---boundary-value-problem}

Consider instead if the string has fixed end points and length \(L\). That is our boundary conditions are

\begin{equation*}
u(0,t) = u(L,t) = 0
\quad\text{and}\quad
u(x,0) = \phi(x)
\quad\text{and}\quad
u_t(x,0) = \psi(x)
\end{equation*}

then the solution is

\begin{equation*}
u(x,t) = \sum_{n=1}^\infty \sin \frac{n\pi x}{L} \left[a_n\cos\frac{n\pi k t}{L} + b_n \sin\frac{n\pi k t}{L}\right]
\end{equation*}

where

\begin{equation*}
a_n = \frac{2}{L}\int_0^L \phi(x)\sin\left(\frac{n\pi x}{L}\right)\ dx
\quad\text{and}\quad
b_n = \frac{2}{n\pi k}\int_0^L \psi(x)\sin\left(\frac{n\pi x}{L}\right)\ dx
\end{equation*}

\begin{admonition-remark}[{}]
What makes this any different from \myautoref[{D'Alembert's solution}]{develop--math3402:ROOT:page--second-order-linear/wave-1d.adoc--dalembert}? Notice
that in the conditions for D'Alembert's solution, \(\phi\) and \(\psi\) have a domain
of the entire real numbers, however in the boundary value problem, the domain is \( [0,L]\).
Therefore, we would extend \(\phi\) and \(\psi\) to obtain a solution (seen below).
\end{admonition-remark}

\begin{example}[{Derivation using D'Alembert's solution}]
\begin{admonition-warning}[{}]
Throughout this derivation we assume some .... stuff
\end{admonition-warning}

As mentioned before, the domain of the intial functions are different from that in
D'Alembert's solution. Then, lets extend \(\phi\) and \(\psi\) such
that they are both odd and have period \(2L\) and notice that D'Alembert's
solution satisfies our boundary contraint

\begin{equation*}
u(0,t)
= \frac{1}2 [\phi(kt) + \phi(-kt)] + \int_{-kt}^{kt} \psi(s) \ ds
= \frac{1}2 [\phi(kt) - \phi(kt)] + 0
= 0
\end{equation*}

and

\begin{equation*}
\begin{aligned}
u(L,t)
&= \frac{1}2 [\phi(L + kt) + \phi(L-kt)] + \int_{L-kt}^{L+kt} \psi(s) \ ds
\\&= \frac{1}2 [\phi(-L + kt) + \phi(-L-kt)] + \int_{-L-kt}^{-L+kt} \psi(s) \ ds
\\&= \frac{1}2 [-\phi(L - kt) - \phi(L+kt)] + -\int_{L-kt}^{L+kt} \psi(s) \ ds
\\&= -u(L,t)
\end{aligned}
\end{equation*}

hence \(u(L,t) = 0\). Therefore, D'Alembert's solution works.

Now, let's find its fourier series expansion. And .. we assume that the fourier
series expansion of an integral is the integral of the fourier series expansion.
Additionally, lets assume \(\phi\) and \(\psi\) are continuous
so that its less to write.

\begin{equation*}
\phi(s) = \sum_{n=1}^\infty a_n \sin\left(\frac{n\pi s}{L}\right)
\quad\text{where }
a_n = \frac{2}{L}\int_0^L \phi(x) \sin\left(\frac{n\pi x}{L}\right)\ dx
\end{equation*}

and

\begin{equation*}
\psi(s) = \sum_{n=1}^\infty b_n \sin\left(\frac{n\pi s}{L}\right)
\quad\text{where }
b_n = \frac{2}{L}\int_0^L \psi(x) \sin\left(\frac{n\pi x}{L}\right)\ dx
\end{equation*}

since \(\phi\) and \(\psi\) are odd. Next,

\begin{equation*}
\int \phi(s) \ ds
= -\sum_{n=1}^\infty \frac{L}{n\pi} b_n \cos\left(\frac{n\pi s}{L}\right)
\end{equation*}

Therefore

\begin{equation*}
\begin{aligned}
u(x,t)
&= \frac{1}{2} [\phi(x+kt)+\phi(x-kt)] + \frac{1}{2k}\int_{x-kt}^{x+kt} \psi(s) \ ds
\\&= \frac{1}{2} \sum_{n=1}^\infty a_n\left[\sin\left(\frac{n\pi}{L}(x+kt)\right) + \sin\left(\frac{n\pi}{L}(x-kt)\right)\right]
    - \frac{1}{2k}\sum_{n=1}^\infty \frac{L}{n\pi}b_n\left[\cos\left(\frac{n\pi}{L}(x+kt)\right)-\cos\left(\frac{n\pi}{L}(x-kt)\right)\right]
\\&= \frac{1}{2} \sum_{n=1}^\infty a_n\left[2\sin\left(\frac{n\pi x}{L}\right)\cos\left(\frac{n\pi kt}{L}\right)\right]
    - \frac{1}{2k}\sum_{n=1}^\infty \frac{L}{n\pi}b_n\left[-2\sin\left(\frac{n\pi x}{L}\right)\sin\left(\frac{n\pi kt}{L}\right)\right]
\\&= \sum_{n=1}^\infty \sin \frac{n\pi x}{L} \left[a_n\cos\frac{n\pi k t}{L} + \frac{L}{kn\pi}b_n \sin\frac{n\pi k t}{L}\right]
\\&= \sum_{n=1}^\infty \sin \frac{n\pi x}{L} \left[a_n\cos\frac{n\pi k t}{L} + c_n \sin\frac{n\pi k t}{L}\right]
\end{aligned}
\end{equation*}

where

\begin{equation*}
c_n
= \frac{L}{kn\pi} b_n
= \frac{L}{kn\pi} \frac{2}{L}\int_0^L \psi(x) \sin\left(\frac{n\pi x}{L}\right)\ dx
= \frac{2}{kn\pi}\int_0^L \psi(x) \sin\left(\frac{n\pi x}{L}\right)\ dx
\end{equation*}

and we have the expected result.
\end{example}

\begin{example}[{Derivation using superposition}]
\begin{admonition-warning}[{}]
Thoughout this derivation, we assume the uniqueness of a solution for \(u\)
\end{admonition-warning}

Firstly, we are going to find the set of functions which satisfy the pde with only the boundary constraints.
Now, assume that \(u\) is product of a function of \(t\) and \(x\). That is,

\begin{equation*}
u(x,t) = X(x)T(t)
\end{equation*}

hence

\begin{equation*}
\begin{aligned}
&X(x)T''(t) = k^2X''(x)T(t)
\\&\implies
\frac{T''(t)}{k^2T(t)} = \frac{X''(x)}{X(x)}
\end{aligned}
\end{equation*}

then, since each side is a function of a different variable, they most both be constant.
Then, first lets consider when they are both zero. We get that \(X,T \in \mathbb{P}_1\)
and since \(X\) has roots at \(0\) and \(L\), \(X\) must be the zero function.
This is clearly not a solution if either \(\phi\) or \(\psi\) is \(0\).

Next, consider when the constant is positive. Let \(\lambda > 0\), then

\begin{equation*}
T''(t) = \lambda^2k^2 T(t)
\quad\text{and}\quad
X''(x) = \lambda^2 X(x)
\end{equation*}

which has solution

\begin{equation*}
\begin{aligned}
&T(t) = Ae^{\lambda kt} + Be^{-\lambda kt}
\\
&X(x) = Ae^{\lambda x} + Be^{-\lambda x}
\end{aligned}
\end{equation*}

Then notice that the solution diverges to infinity as \(t\to \infty\) and hence this solution
does not make physcall sense.

\begin{admonition-remark}[{}]
I am not certain of how good the above argument is.
\end{admonition-remark}

Finally, consider when the constant is negative. Let \(\lambda > 0\), then

\begin{equation*}
T''(t) = -\lambda^2k^2 T(t)
\quad\text{and}\quad
X''(x) = -\lambda^2 X(x)
\end{equation*}

Then, these are ordinary second order linear equations, with solutions

\begin{equation*}
\begin{aligned}
&T(t) = A\cos(\lambda k t) + B\sin(\lambda k t)
\\&
X(x) = C\cos(\lambda x) + D\sin(\lambda x)
\end{aligned}
\end{equation*}

Now,

\begin{equation*}
u(0,t) = X(0)T(t) = C T(t) = 0 \implies C =0
\end{equation*}

and

\begin{equation*}
u(L,t) = X(L)T(t) = 0 \implies X(L) = D\sin(\lambda L)= 0  \implies \lambda L = n\pi \implies \lambda = \frac{n\pi}{L}
\end{equation*}

where each of these \(\lambda\)'s is called an eigenvalue of the equation. Next, we can add each of associated
solutions for each \(n \in \mathbb{Z}^+\) using the principle of super position to get a general solution

\begin{equation*}
\begin{aligned}
u(x,t)
&= \sum_{n=1}^\infty D_n\sin\left(\frac{n\pi x}{L}\right)\left[
        A_n\cos\left(\frac{n\pi kt}{L}\right) + B_n\sin\left(\frac{n\pi kt}{L}\right)
    \right]
\\&= \sum_{n=1}^\infty \sin\left(\frac{n\pi x}{L}\right)\left[
        a_n\cos\left(\frac{n\pi kt}{L}\right) + b_n\sin\left(\frac{n\pi kt}{L}\right)
    \right]
\end{aligned}
\end{equation*}

we can set \(D=1\) since any variation would be accounted for by \(A\) and \(B\).
Now, lets consider the initial conditions

\begin{equation*}
u(x,0) = \sum_{n=1}^\infty \sin\left(\frac{n\pi x}{L}\right)\left[
        a_n
    \right]
    = \phi(x)
\end{equation*}

Then its immediately seen that \(\phi\) is a fourier sine series. Hence

\begin{equation*}
a_n = \frac{2}{L} \int_0^L \phi(x) \sin\left(\frac{n\pi x}{L}\right) \ dx
\end{equation*}

Next,

\begin{equation*}
u_t(x,t) =
\sum_{n=1}^\infty \sin\left(\frac{n\pi x}{L}\right)\left[
        -a_n\frac{n\pi k}{L}\sin\left(\frac{n\pi kt}{L}\right) + b_n\frac{n\pi k}{L}\cos\left(\frac{n\pi kt}{L}\right)
    \right]
\end{equation*}

and

\begin{equation*}
u_t(x,0) = \sum_{n=1}^\infty \sin\left(\frac{n\pi x}{L}\right)\left[
        b_n\frac{n\pi k}{L}
    \right]
    = \psi(x)
\end{equation*}

and again this is a fourier sine series and hence

\begin{equation*}
b_n\frac{n\pi k}{L} = \frac{2}{L} \int_0^L \psi(x) \sin\left(\frac{n\pi x}{L}\right) \ dx
\implies
b_n = \frac{2}{n\pi k} \int_0^L \psi(x) \sin\left(\frac{n\pi x}{L}\right) \ dx
\end{equation*}

and now we have our solution.
\end{example}
\end{document}
