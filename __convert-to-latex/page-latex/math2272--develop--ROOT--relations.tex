\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Relations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(A\) and \(B\) be sets. Then a subset
\(R \subseteq A \times B\) is called a \emph{relation} from
\(A\) to \(B\). Furthermore, \(a \in A\)
and \(b \in B\) are said to be related (denoted
\(aRb\)) iff \((a, b) \in R\). Inversely,
\(a\) and \(b\) are said to not be related (denoted
\(a\cancel{R}b\)) iff \((a, b) \notin R\).

\section{Acknowledgement}
\label{develop--math2272:ROOT:page--relations.adoc---acknowledgement}

Most of the information from this section was retrieved from Dr.
Farrell’s ''M 20A Lecture Notes.''

\section{Basic Operations}
\label{develop--math2272:ROOT:page--relations.adoc---basic-operations}

\begin{itemize}
\item The \emph{domain} of \(R\), denoted \(\text{dom}(R)\),
    is defined as \(\text{dom}(R) = \{a \mid (a, b) \in R\}\).
\item The \emph{range} of \(R\), denoted \(\text{ran}(R)\),
    is defined as \(\text{ran}(R) = \{b \mid (a, b) \in R\}\).
\item If \(R \subseteq A\times B\) and
    \(S \subseteq B \times C\) are relations, then their
    \emph{composition} is defined as
    \(S \circ R = \{(a, c)\mid \exists b \in B: (a,b)\in R \text{ and } (b, c)\in S\}\).
\item The \emph{inverse} of \(R\) is defined as
    \(R^{-1} = \{(b, a)\mid (a, b) \in R\}\).
\end{itemize}

\section{Relations on \(A\)}
\label{develop--math2272:ROOT:page--relations.adoc---relations-on-latex-backslasha-latex-backslash}

When \(A=B\), \(R\) is called a relation on
\(A\).

\begin{itemize}
\item Reflexive \& Irreflexive
    
    \begin{itemize}
    \item Reflexive: \(\forall a \in A: aRa\).
    \item Irreflexive: \(\forall a \in A: a\cancel{R}a\).
    \end{itemize}
\item Symmetric \& Antisymmetric
    
    \begin{itemize}
    \item Symmetric: \(\forall a, b \in A: aRb \implies bRa\).
    \item Antisymmetric:
        \(\forall a, b \in A: aRb \,\land\, bRa \implies a = b\).
    \end{itemize}
\item Transitive \& Atransitive
    
    \begin{itemize}
    \item Transitive:
        \(\forall a, b, c \in A: aRb \,\land\, bRc \implies aRc\).
    \item Atransitive:
        \(\forall a, b, c \in A: aRb \,\land\, bRc \implies a\cancel{R}c\).
    \end{itemize}
\end{itemize}

\section{Order}
\label{develop--math2272:ROOT:page--relations.adoc---order}

An \emph{order} (or \emph{partial order}) \(R\), on a set
\(A\), is a reflexive, antisymmetric, transitive relation on
\(A\). Additionally, an order is said to be

\begin{itemize}
\item Strict: if it is irreflexive instead of reflexive.
\item Total: \(\forall a, b \in A: aRb \,\lor\, bRa\).
\end{itemize}

Note that an order cannot be both total and strict (as far as I could
tell).

\section{Equivalence Relations}
\label{develop--math2272:ROOT:page--relations.adoc---equivalence-relations}

An \emph{equivalence relation} \(R\), on \(A\), is a
relation that is reflexive, symmetric and transitive.

\subsection{Equivalence class}
\label{develop--math2272:ROOT:page--relations.adoc---equivalence-class}

Let \(x \in A\), then the equivalence class of \(x\)
with respect to \(R\) is defined as

\begin{equation*}
[x] = x/R = \{y \in R: xRy\}
\end{equation*}

\subsection{Partioning property}
\label{develop--math2272:ROOT:page--relations.adoc---partioning-property}

The set of equivalence classes \(X/R\) partitions
\(X\) since

\begin{equation*}
\forall x, y \in A: [x] \cap [y] = \varnothing \,\lor\, [x] = [y]
\end{equation*}

Furthermore,

\begin{itemize}
\item The partition induced by \(R\) itself induces
    \(R\).
\item If \(C\) is a partition of \(X\), it induces an
    equivalence relation whose set of equivalence classes is
    \(C\).
\end{itemize}

These two results assert that an equivalence relation can be
equivalently defined by the partition of \(X\). Or
alternatively, there is a natural bijection from the set of equivalence
relations on \(X\) to the set of partitions of
\(X\).

\section{Functions}
\label{develop--math2272:ROOT:page--relations.adoc---functions}

A function, \(f\), from \(A\) to \(B\) is
a relation such that

\begin{itemize}
\item \(dom(f) = A\).
\item \(\forall x \in A: (x, y) \in f \,\land\, (x, z) \in f \implies x = z\).
\end{itemize}

The set of all functions from \(A\) to \(B\) is
denoted \(B^A\) or \(A\to B\).

\subsection{Special functions}
\label{develop--math2272:ROOT:page--relations.adoc---special-functions}

\begin{itemize}
\item The function \(f: A\times B \to B\) defined by
    \(f(x, y) = y\) is called a \emph{projection} from
    \(A\times B\) onto \(B\).
\item The function \(f: A \to A/R\) defined by
    \(f(x) = [x]\) is called a \emph{canonical map} from
    \(A\) to \(A/R\).
\item Let \(D\) be a subset of \(A\), then the function
    \({\Large\chi}_D :A \to \{0, 1\}\) defined by
    
    \begin{equation*}
    {\Large \chi}_D(x) = \begin{cases}
                        1 \text{ if } x\in D \\
                        0 \text{ if } x\notin D
                    \end{cases}
    \end{equation*}
    
    is called the \emph{characteristic function} of \(D\).
\item The function \(f: P(A) \to 2^A\) - where \(P(A)\)
    is the power set of \(A\) and \(2^A\) is the set of
    characteristic functions of \(A\) - defined by
    \(f(D) = {\Large \chi}_D\) (need to find the name)
\end{itemize}
\end{document}
