\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Chapter 2: Interesting non-linear ODE}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
Consider the ODE

\begin{equation*}
x' = |x|^{\frac12} \quad\forall t \in \mathbb{R}
\end{equation*}

This ODE has a very interesting general solution.

Suppose that \(\phi(t)\) is a solution for the above ODE.
The first thing to note is that \(\phi'(t) \geq 0\) and hence \(\phi\) is monotonic increasing.
Also notice that \(\phi'(t)\) is continuous since \(|\phi(t)|\) is continuous.

\begin{admonition-remark}[{}]
On this page we assume that \(x\) is a real valued function.
If it is complex, let \(x = u + wi\) where \(u\) and \(w\) are real valued.
Since \(x' = u' + w'i\) is real, we have that \(w'=0\).
Therefore, for any constant valued solution, there exists a real valued solution such that the difference is a
constant imaginary number.
\end{admonition-remark}

\begin{admonition-remark}[{}]
I split this section into lemmas but they are not formatted overly well due to limitations of asciidoc (AKA my laziness).
\end{admonition-remark}

\section{Lemma 1: Splitting domain}
\label{develop--math6120:exercises:page--chapter2-interesting-1.adoc---lemma-1-splitting-domain}

Define

\begin{equation*}
I^- = \{t \in \mathbb{R} \ | \ \phi(t) < 0\}
\quad
I^0 = \{t \in \mathbb{R} \ | \ \phi(t) = 0\}
\quad
I^+ = \{t \in \mathbb{R} \ | \ \phi(t) > 0\}
\end{equation*}

Then, these three are intervals due to the monotonicity of \(\phi\).
Furthermore, due to the continuity of \(\phi\), \(I^ + \) and \(I^-\) are open.
Since \(I^ + \) and \(I^ - \) are open, it is possible to use standard techniques on these intervals.

\begin{admonition-note}[{}]
It is possible for \(I^-\) and/or \(I^+\) to be empty
\end{admonition-note}

\section{Lemma 2: Form of solution}
\label{develop--math6120:exercises:page--chapter2-interesting-1.adoc---lemma-2-form-of-solution}

If \(I^- \neq \varnothing\) consider arbitrary \(t, t_0 \in I^-\).
Then \(\frac{\phi'(t)}{\sqrt{-\phi(t)}}\) is continuous on \(I^-\) and

\begin{equation*}
2\sqrt{-\phi(t)} - 2\sqrt{-\phi(t_0)}
= \int_{t_0}^t \frac{\phi'(t)}{\sqrt{-\phi(t)}} \ dt
= \int_{t_0}^t 1 \ dt
= t-t_0
\end{equation*}

This implies that there exists a constant \(c_- \in \mathbb{R}\) such that

\begin{equation*}
\forall t \in I^-: \phi(t) = -\frac14 (t+c_-)^2
\end{equation*}

This process can be repeated for \(I^] to determine that there exists stem:[c\_ \in \mathbb{R}\) such that

\begin{equation*}
\forall t \in I^+: \phi(t) = \frac14 (t+c_+)^2
\end{equation*}

\subsection{Possible solutions}
\label{develop--math6120:exercises:page--chapter2-interesting-1.adoc---possible-solutions}

The function \(\phi\) has one of the following forms

\begin{equation*}
\phi(t) = 0
\end{equation*}

\begin{equation*}
\phi(t) = \begin{cases}
\frac14 (t+c_+)^2 &\quad\text{if } t \in I^+\\
0 &\quad\text{otherwise}\\
\end{cases}
\end{equation*}

\begin{equation*}
\phi(t) = \begin{cases}
-\frac14 (t+c_-)^2 &\quad\text{if } t \in I^-\\
0 &\quad\text{otherwise}\\
\end{cases}
\end{equation*}

\begin{equation*}
\phi(t) = \begin{cases}
\frac14 (t+c_+)^2 &\quad\text{if } t \in I^+\\
-\frac14 (t+c_-)^2 &\quad\text{if } t \in I^-\\
0 &\quad\text{otherwise}\\
\end{cases}
\end{equation*}

\section{Lemma 3: More specific forms}
\label{develop--math6120:exercises:page--chapter2-interesting-1.adoc---lemma-3-more-specific-forms}

From the above notice that \(I^] must be bounded below since stem:[{\textbackslash}phi'(t) = {\textbackslash}frac12(t+c\_) \geq 0\).
Likewise, \(I^-\) must be bounded above.

Therefore let \(I^+ = (b,\infty)\) (if it is non-zero) and \(I^- = (-\infty, a)\) (if it is non-zero).

Now, since \(\phi\) is continuous, we have the following

\begin{itemize}
\item If \(I^+ \neq \varnothing\), then \(0 = \phi(b) = \phi(b^) = {\textbackslash}frac14 (b+c\_)^2\) implies
    that \(b=-c^+\)
\item If \(I^- \neq \varnothing\) and \(I^+ = \varnothing\), then \(0 = \phi(a) = \phi(a^-) = -\frac14 (a+c_-)^2\) implies
    that \(a=-c^-\)
\end{itemize}

Since \(a,b\in I^0\) (if they exists).

\section{Solutions}
\label{develop--math6120:exercises:page--chapter2-interesting-1.adoc---solutions}

Therefore, the final forms of \(\phi\) are as below

\begin{equation*}
\phi(t) = 0
\end{equation*}

\begin{equation*}
\phi(t) = \begin{cases}
\frac14 (t-b)^2 &\quad\text{if } t > b\\
0 &\quad\text{otherwise}\\
\end{cases}
\end{equation*}

\begin{equation*}
\phi(t) = \begin{cases}
-\frac14 (t-a)^2 &\quad\text{if } t < a\\
0 &\quad\text{otherwise}\\
\end{cases}
\end{equation*}

\begin{equation*}
\phi(t) = \begin{cases}
-\frac14 (t-a)^2 &\quad\text{if } t < a\\
\frac14 (t-b)^2 &\quad\text{if } t > b\\
0 &\quad\text{otherwise}\\
\end{cases}
\end{equation*}

It is easily verified that each of these forms of \(\phi\) are valid solutions of the ODE.

\section{Generalization}
\label{develop--math6120:exercises:page--chapter2-interesting-1.adoc---generalization}

Consider the ODE

\begin{equation*}
x' = |x|^{\alpha} \quad\forall t \in \mathbb{R}
\end{equation*}

where \(0 < \alpha < 1\).
Then, we can use the above process to solve this ODE in a similar manner.
However, one must take care when rationalizing why the intervals are bounded above/below.

\begin{admonition-caution}[{}]
I am not too sure if there are additional solutions other than the 4 forms.
\end{admonition-caution}
\end{document}
