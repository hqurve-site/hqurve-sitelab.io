\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Systems of Linear Equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Definition}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---definition}

Let \(\mathbb{F}\) be a field. A system of \(m\)
linear equations in \(n\) unknowns,
\(x_1, x_2, \ldots, x_n\in \mathbb{F}\) is a set of equations
of the form

\begin{equation*}
a_{i1}x_1 + a_{i2}x_2 + \cdots+ a_{in}x_n + =b_i
\end{equation*}

for \(1 \leq i \leq m\). By creating matrix
\(A = (a_{ij})\) and column vectors
\(\underline{x} = (x_1, \ldots, x_n)^T \in \mathbb{F}^n\) and
\(\underline{b} = (b_1, \ldots, b_m)^T \in \mathbb{F}^m\), the
above system can be compactly written as

\begin{equation*}
A \underline{x} = \underline{b}
\end{equation*}

Furthermore,

\begin{itemize}
\item the system is called \emph{homogeneous} if
    \(\underline{b} = \underline{0}\) and \emph{non-homogeneous}
    otherwise.
\item the set of all solutions is called the \emph{solution set} of the system.
\item the system is called \emph{consistent} if the solution set is non-empty and
    \emph{inconsistent} otherwise.
\end{itemize}

\section{Elementary row operations}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---elementary-row-operations}

Let \(A\) be an \(m\times n\) matrix with field (or
commutative ring) \(\mathbb{F}\). Then the \emph{elementary row
operations} on \(A\) are

\begin{enumerate}[label=\arabic*)]
\item Interchange rows \(i\) and \(j\) (written as
    \(R_i \leftrightarrow R_j\)).
\item Multiply row \(i\) by \(c \neq 0\) (written as
    \(R_i \rightarrow cR_i\)).
\item Add a non-zero multiple of row \(j\) to row \(i\)
    (written as \(R_i \rightarrow R_i + cR_j\)).
\end{enumerate}

\subsection{Matrix representation}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---matrix-representation}

Notice that all three operations are linear in their row arguments and
are hence linear transformations. Then these operations can be written
as matrices called \emph{elementary matrices}. They are simply constructed by
applying the operation to the \(m\times m\) identity matrix
\(I_m\). Furthermore, since each of the operations are
invertible, their matrix representations are also invertible (with
respect to matrix multiplication).

\subsection{Matrix equivalence}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---matrix-equivalence}

Two \(m\times n\) matrices \(A\) and \( B \)
are equivalent, denoted \(A\cong B\) if \( B \) can be
obtained from \(A\) by a finite number of elementary row
operations. That is

\begin{equation*}
A \cong B \Longleftrightarrow E_1E_2\cdots E_k A = B
\end{equation*}

This equivalence is in fact an equivalence relation on
\(\mathbb{R^{m\times n}}\) since elementary row operations are
invertible and their inverse is an elementary row operation.

\section{Augmented matrix}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---augmented-matrix}

Consider the linear system of equations
\(A\underline{ x} = \underline{ b}\). Then the block matrix
\([ A, \underline{ b}]\) is called the \emph{augmented matrix} of the
system. This augmented matrix allows for easy computation of the
solution set of the system. In fact, if
tem:[[ A, {\textbackslash}underline\{ b \}{\textbackslash}] {\textbackslash}cong [ C, {\textbackslash}underline\{ d\}{\textbackslash}]] have the same

\begin{example}[{Proof}]
Since \([ A, \underline{ b}] \cong [ C, \underline{ d}]\), there
exists elementary matrices \(E_1, \ldots, E_n\) such that

\begin{equation*}
E_1E_2\cdots E_n [A, \underline{b}] = [C, \underline{d}]
        \implies
        C = E_1E_2\cdots E_n A
        \quad\text{and}\quad
        \underline{d} = E_1E_2\cdots E_n \underline{b}
\end{equation*}

Now consider any \(\underline{u}\in \mathbb{F}^n\) such that
\(A\underline{u} = \underline{b}\). Then

\begin{equation*}
C\underline{u}
        = E_1E_2\cdots E_n A
        = E_1E_2\cdots E_n \underline{b}
        = \underline{d}
\end{equation*}

and the converse also follows by the symmetry of matrix equivalence.

 ◻
\end{example}

\section{Row echelon form}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---row-echelon-form}

Consider a \(m\times n\) matrix, \(A\). Then the
pivot of any row is the first non-zero entry in the row. Then
\(A\) is \emph{row echelon form} (REF) if

\begin{itemize}
\item All zero rows (rows filled with zeros) are at the bottom
\item The pivot in row \(j\) is to the right of all pivots in rows
    \(i < j\).
\end{itemize}

Furthermore, \(A\) is in \emph{reduced row echelon form} (RREF) if
it is in REF and

\begin{itemize}
\item the pivots are the only non-zero entries in their columns
\item all the pivots are \(1\) (multiplicative identity)
\end{itemize}

The process of computing the REF of a matrix is called \emph{Gaussian
Elimination} while that of computing the RREF is called \emph{Gauss-Jordan
Elimination}, both of which consists of elementary row operations.
Therefore, two matrices are similar iff they share a row echelon form.
Furthermore, the reduced row echelon form is unique and hence
equivalence classes of RREFs partition the set of matrices.

\section{Row and column space}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---row-and-column-space}

Let \(A\) be an \(m\times n\) matrix. Then the
subspace spanned by the rows of \(A\) is called the \emph{row
space} of \(A\) and is denoted by
\(RS(A) \subseteq \mathbb{F}^n\). Similarly, the subspace
spanned by the columns of \(A\) is called the \emph{column space}
of \(A\) and is denoted
\(CS(A) \subseteq \mathbb{F}^m\). Then

\begin{itemize}
\item The \(CS(A)\) is the range of
    \(\underline{x}\mapsto A\underline{x}\) where
    \(\underline{x} \in \mathbb{F}^n\).
\item The \(RS(A)\) is the range of
    \(\underline{y}\mapsto \underline{y}A = (A^T\underline{y}^T)^T\)
    where \(\underline{y} \in \mathbb{F}^m\).
\end{itemize}

\subsection{Rank}
\label{develop--math2273:ROOT:page--systems-of-linear-equations.adoc---rank}

Notice that the rows in a REF matrix are independent and hence form a
basis for the row space. Then, the rank of
\(\underline{x}\mapsto A\underline{x}\) is the number of
non-zero rows of a row echelon form of matrix \(A\). This rank
is called the \emph{column rank} and is written as

\begin{equation*}
\dim(CS(A))
\end{equation*}

While the \emph{row rank} is the number of non-zero rows of a row echelon
form of \(A^T\). Furthermore, there is no reason to
distinguish between the two since they are both equal.
\end{document}
