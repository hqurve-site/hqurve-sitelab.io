\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Branching Processes}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-tip}[{}]
See An introduction to stochastic processes with applications to biology by Linda J. S. Allen
for a nice discussion of branching processes. It has been the best resource that I've seen.
\end{admonition-tip}

A branching process is a discrete-time markov chain.
At time \(t\), we have a population of \(Z_n\) and

\begin{equation*}
Z_{n+1} = \sum_{i=1}^{Z_n} Y_i^{(n)}
\end{equation*}

where the \(Y_i^{(n)}\) are iid random variables, called the \emph{family size distribution}.
Note that \(Z_{n+1}\) is a random sum.

Alternatively, we could have defined a branching process as follows

\begin{custom-quotation}[{}][{}]
Let \(Z_n\) be the number of individuals at stage \(n\).
Then the sequence \(Z_0, Z_1, \ldots\) forms a branching process if

\begin{itemize}
\item There is a single individual at stage \(n=0\)
\item Every individual lives for exactly one unit of time. At the end of their life,
    the produce offspring
\item The number of offspring \(Y\) is a random variable which takes values \(0,1,2,3,\ldots\).
    If the offspring are represented by \(Y_1, \ldots Y_k\), each of them have the same distribution
    for their number of offspring
\end{itemize}

We assume that

\begin{itemize}
\item All individuals reproduce independently of each other
\item The family size of different individuals are iid random variables
\end{itemize}
\end{custom-quotation}

\section{Nice properties}
\label{develop--math6180:ROOT:page--branching-processes.adoc---nice-properties}

If \(Z_0 = 1\), we have that

\begin{equation*}
E[Z_n] = \mu^n \quad\text{and}\quad
Var[Z_n] = \begin{cases}
n\sigma^2,\quad&\text{if } \mu=1\\
\sigma^2\mu^{n-1}\left(\frac{1-\mu^n}{1-\mu}\right),\quad&\text{if } \mu\neq 1\\
\end{cases}
\end{equation*}

where \(\mu = E[Y]\) and \(\sigma^2 = Var[Y]\).

\begin{proof}[{}]
We will prove both results inductively.
Note that the base case is trivially true.
So, suppose that it is true for the case when \(n=k\).
Then,

\begin{equation*}
E[Z_{k+1}]
= E\left[\sum_{i=1}^{Z_k} Y_i\right]
= E_{Z_k}\left[E_{\vec{Y}}\left[\sum_{i=1}^{Z_k} Y_i\right | Z_k]\right]
= E_{Z_k}\left[Z_k \mu\right]
= \mu^k \mu
= \mu^{k+1}
\end{equation*}

Also, we would rewrite the variance as

\begin{equation*}
Var[Z_k] = \sigma^2\mu^{k-1}\sum_{i=0}^{k-1}\mu^i
\end{equation*}

When written like this, the case when \(\mu = 1\) and \(\mu\neq 1\) agree.
So, we have that

\begin{equation*}
\begin{aligned}
Var[Z_{k+1}]
&= Var\left[\sum_{i=1}^{Z_k} Y_i\right]
\\&=
Var_{Z_k}\left[E_{\vec{Y}}\left[\sum_{i=1}^{Z_k} Y_i | Z_k\right]\right]
+ E_{Z_k}\left[Var_{\vec{Y}}\left[\sum_{i=1}^{Z_k} Y_i | Z_k\right]\right]
\\&=
Var_{Z_k}\left[\mu Z_k\right]
+ E_{Z_k}\left[\sigma^2Z_k\right]
\quad\text{since the } Y_i \text{ are independent}
\\&=
\mu^2 Var[Z_k]
+ \sigma^2 \mu^k
\\&=
\mu^2 \sigma^2\mu^{k-1}\sum_{i=0}^{k-1}\mu^i
+ \sigma^2 \mu^k
\\&=
\sigma^2\mu^{k}\sum_{i=1}^{k}\mu^i
+ \sigma^2 \mu^k
\\&=
\sigma^2\mu^{(k+1) -1}\sum_{i=0}^{(k+1)-1}\mu^i
\end{aligned}
\end{equation*}

as desired
\end{proof}

\section{Probability of extinction}
\label{develop--math6180:ROOT:page--branching-processes.adoc---probability-of-extinction}

We will like to know

\begin{itemize}
\item The probability that the population is extinct at stage \(n\)
\item The probability that the population eventually goes extinct;
    the probability of \emph{ultimate extinction}.
\end{itemize}

Let \(E_n\) be the event that the population is extinct at stage \(n\).
Then,

\begin{equation*}
E_n = \{\vec{x} \in \Omega \ | \ Z_n(\vec{x}) = 0\}
\end{equation*}

where \(\Omega\) is our sample space.

\begin{admonition-tip}[{}]
Recall that a random variable is just a function from our sample space to the real numbers.
\end{admonition-tip}

Also note that

\begin{equation*}
E_0 \subseteq E_1 \subseteq E_2 \subseteq \cdots \subseteq E_n \subseteq \cdots
\end{equation*}

since \(Z_n(\vec{x}) = 0\).
So, the probability of ultimate extinction is given by

\begin{equation*}
P(\text{ultimate extinction})
= P\left(\bigcup_{n=0}^\infty E_n\right)
= P\left(\lim_{n\to \infty} E_n\right)
\end{equation*}

Note that the latter limit only makes sense if somehow use our probability measure \(P\)
to determine a metric for the subsets of our sample space \(\Omega\).
For my own sanity, we'll ignore the last equality for now.
Let \(E\) be the event that ultimate extinction occurs.
So,

\begin{equation*}
E = \bigcup_{n=0}^\infty E_n
\end{equation*}

\begin{theorem}[{}]
\begin{admonition-note}[{}]
That this is a special case of the continuous mapping theorem.
\url{https://en.wikipedia.org/wiki/Continuous\_mapping\_theorem}
\end{admonition-note}

Let \(E_0 \subseteq E_1 \subseteq \cdots\) be a collection of nested
events with \(E = \bigcup_{i=0}^\infty E_n\).
Then,

\begin{equation*}
P(E)
% = P\left(\bigcup_{n=0}^\infty E_n\right)
= \lim_{n\to\infty} P(E_n)
\end{equation*}

\begin{proof}[{}]
Recall that since \((\Omega, \mathcal{F}, P)\) is a probability space
(\url{https://en.wikipedia.org/wiki/Probability\_space\#Definition} ),

\begin{itemize}
\item \(\mathcal{F} \subseteq 2^\Omega\)
\item \(\mathcal{F}\) is closed under complements,
    countable unions (and hence also countable intersections)
\item \(P: \mathcal{F}\to [0,1]\)
\item If \(\{A_i\}_{i\in I}\) is a countable collection of events
    which are pairwise disjoint,
    
    \begin{equation*}
    P\left(\bigcup_{i\in I}A_i\right) = \sum_{i \in I}P(A_i)
    \end{equation*}
\end{itemize}

This is all that we need to prove our result.

Let \(F_n = E_n - E_{n-1}\) for \(n\geq 1\) and \(F_0 = E_0\).
Note that \(F_n \in \mathcal{F}\) and
the \(F_n\) are pairwise disjoint and

\begin{equation*}
E = \cup_{i=p}^\infty E_n = \cup_{i=0}^\infty F_n
\end{equation*}

So, we have that

\begin{equation*}
P\left(E\right)
= P\left(\cup_{i=0}^\infty E_n\right)
= P\left(\cup_{i=0}^\infty F_n\right)
= \sum_{i=0}^\infty P(F_n)
\end{equation*}

Also,

\begin{equation*}
P(E_n) = P\left(\cup_{i=0}^n F_n\right)
= \sum_{i=0}^n P(F_n)
\end{equation*}

So, we must have that

\begin{equation*}
\lim_{n\to\infty} P(E_n) = \lim_{n\to\infty} \sum_{i=0}^n P(F_n)
= P(E)
\end{equation*}

as desired.
\end{proof}
\end{theorem}

Therefore, the probability of ultimate extinction can be determined by the limit
of probabilities of extinction by stage \(n\).
Although this is a nice result, it is not very feasible to compute.
So, instead we use the following theorem to find the probability
of ultimate extinction

\begin{theorem}[{Probability of total extinction}]
Let \(\gamma\) be the probability of ultimate extinction.
Then \(\gamma\) is the smallest non-negative solution to the equation

\begin{equation*}
G(s) = s
\end{equation*}

where \(G(s)\) is the probability generating function of the family size
distribution \(Y\).

\begin{admonition-note}[{}]
The solution \(s=1\) exists by the definition of the PGF,
so this theorem always holds.
\end{admonition-note}
\end{theorem}

The above theorem is an immediate result of the two following lemmas

\begin{lemma}[{PGF of stage \(n\)}]
Let \(G(s)\) be the probability generating function for the family size distribution \(Y\).
Then, if \(H_n\) is the probability generating function of \(Z_n\),

\begin{equation*}
H_n(s) = G^{n}(s)
\end{equation*}

and hence

\begin{equation*}
P(E_n) = P(Z_n=0) = H_n(0) = G^n(0)
\end{equation*}

\begin{proof}[{}]
\begin{admonition-tip}[{}]
This could have alternatively been proven using the theorem that states
\(G_T(s) = G_N(G_X(s))\) where \(T = \sum_{i=1}^N X_i\).
\end{admonition-tip}

Note that the result clearly holds when \(n=0\) and \(n=1\).

Consider \(H_{n+1}(s)\)

\begin{equation*}
\begin{aligned}
H_{n+1}(s)
&= \sum_{z_{n+1}=0}^\infty s^{z_{n+1}}P(Z_{n+1} = z_{n+1})
\\&= \sum_{z_{n+1}=0}^\infty s^{z_{n+1}}\sum_{z_{n}=0}^\infty P(Z_{n+1} = z_{n+1} | Z_{n}=z_n)P(Z_n=z_n)\quad\text{ by conditioning on }Z_{n}
\\&= \sum_{z_{n}=0}^\infty\sum_{z_{n+1}=0}^\infty s^{z_{n+1}} P(Z_{n+1} = z_{n+1} | Z_{n}=z_n)P(Z_n=z_n)\quad \text{ by absolute convergence}
\\&= \sum_{z_{n}=0}^\infty\sum_{z_{n+1}=0}^\infty s^{z_{n+1}} P\left(\sum_{i=1}^{z_n} Y_{i} = z_{n+1} \right)P(Z_n=z_n)\quad \text{ by definition of } Z_{n+1}
\\&= \sum_{z_{n}=0}^\infty\sum_{z_{n+1}=0}^\infty s^{z_{n+1}} \left(z_{n+1}\text{'th coefficient of PGF of } \sum_{i=1}^{z_n} Y_{i} \right)P(Z_n=z_n)\quad \text{ by definition of PGF}
\\&= \sum_{z_{n}=0}^\infty\sum_{z_{n+1}=0}^\infty s^{z_{n+1}} \left(z_{n+1}\text{'th coefficient of } G(s)^{z_{n}} \right)P(Z_n=z_n)\quad \text{ since the }Y_i \text{ are iid}
\\&= \sum_{z_{n}=0}^\infty\sum_{z_{n+1}=0}^\infty \left(z_{n+1}\text{'th term of }G(s)^{z_{n}}\right) P(Z_n=z_n)
\\&= \sum_{z_{n}=0}^\infty G(s)^{z_{n}} P(Z_n=z_n)
\\&= H_n(G(s)) \quad \text{ by definition of PGF}
\\&= G^{n}(G(s))
\\&= G^{n+1}(s)
\end{aligned}
\end{equation*}
\end{proof}
\end{lemma}

\begin{lemma}[{}]
Let \(G(s)\) be probability generating function

\begin{equation*}
\lim_{n\to \infty} G^n(0)
\end{equation*}

is the smallest non-negative root of

\begin{equation*}
G(s) = s
\end{equation*}

\begin{proof}[{}]
Let \(\alpha\) be a non-negative root of \(G(s) = s\) and let

\begin{equation*}
\gamma = \lim_{n\to\infty} G^n(0)
\end{equation*}

Then, it is sufficient to show that \(\gamma \leq \alpha\).
Suppose BWOC that \(\gamma > \alpha\).
Then, there exists \(n \geq 0\) with

\begin{equation*}
G^{n}(0) < \alpha < G^{n+1}(0)
\end{equation*}

Since \(G\) is a probability generating function, it is strictly increasing and
applying \(G\) to the above inequality preserves ordering. So

\begin{equation*}
G^{n+1}(0) < G(\alpha) = \alpha
\end{equation*}

This is our contradiction.

So, we must have that \(\gamma\) is less than or equal to any other non-negative root of \(G(s)=s\).
Since \(\gamma\) is itself a root of \(G(s)= s\), it must be the least non-negative root.

\begin{admonition-remark}[{}]
A simpler way to prove this is just noting that \(G(0) \leq \alpha\) and hence
\(\gamma \leq \alpha\) in the limit. From \url{https://www.stat.auckland.ac.nz/~fewster/325/notes/ch7blank.pdf}
\end{admonition-remark}
\end{proof}
\end{lemma}

\subsection{Examples}
\label{develop--math6180:ROOT:page--branching-processes.adoc---examples}

In exams, there are three steps:

\begin{enumerate}[label=\arabic*)]
\item Quote the theorem to be used
\item Find the PGF of the family size distribution
\item Solve \(G(s)=s\)
\end{enumerate}

\begin{admonition-tip}[{}]
When solving \(G(s) = s\), we already know that \(s=1\) is a solution,
so we can factor it out of \(G(s) -s\).
\end{admonition-tip}

\subsubsection{Example with binomial}
\label{develop--math6180:ROOT:page--branching-processes.adoc---example-with-binomial}

Let \(\{Z_0, Z_1, \ldots\}\) be a branching process
with a family size distribution \(Y\sim Binomial\left(2,\frac14\right)\).
Find the probability that the process will eventually die out.

We will find the general result for some arbitrary probability.

Recall that the pgf is given by

\begin{equation*}
G(s) = ((1-p) + ps)^2
\end{equation*}

So,

\begin{equation*}
\begin{aligned}
G(s) &= s
\\\iff \quad ((1-p) + ps)^2 &= s
\\\iff \quad (1-p)^2 + 2p(1-p)s + p^2s^2 &= s
\\\iff \quad (1-p)^2 + (2p - 2p^2 - 1)s + p^2s^2 &= 0
\end{aligned}
\end{equation*}

By using the quadratic formula, we get

\begin{equation*}
\begin{aligned}
s
&= \frac{(1+2p^2-2p) \pm \sqrt{(2p - 2p^2 - 1)^2 -4p^2(1-p)^2}}{2p^2}
\\&= \frac{(1+2p^2-2p) \pm \sqrt{[4p^2 + 4p^4 + 1 - 8p^3 -4p+4p^2] - [4p^4 -8p^3 + 4p^2]}}{2p^2}
\\&= \frac{(1+2p^2-2p) \pm \sqrt{1 -4p+4p^2}}{2p^2}
\\&= \frac{(1+2p^2-2p) \pm \sqrt{(1-2p)^2}}{2p^2}
\\&= \frac{(1+2p^2-2p) \pm (1-2p)}{2p^2}
\\&= \frac{2p^2 \text{ or } (2 + 2p^2 -4p)}{2p^2}
\\&= \frac{2p^2 \text{ or } 2(1-p)^2}{2p^2}
\\&= 1 \text{ or } \left(\frac{1-p}{p}\right)^2
\end{aligned}
\end{equation*}

Note that

\begin{equation*}
\frac{1-p}{p} < 1 \iff p > \frac{1}{2}
\end{equation*}

This is really nice. It says that the process will go extinct with probability \(1\) as long as \(p \leq \frac{1}{2}\).
This makes sense.

\subsubsection{Geometric (inclusive)}
\label{develop--math6180:ROOT:page--branching-processes.adoc---geometric-inclusive}

Let \(Y\sim Geometric(p)\) where \(P(Y=y) = p(1-p)^{y-1}\) for \(y=1,2,\ldots\).

Recall that

\begin{equation*}
G(s) = \frac{ps}{1-(1-p)s}
\end{equation*}

So,

\begin{equation*}
G(s) = s
\iff ps = s-(1-p)s^2
\end{equation*}

There is no need to go further since \(s=0\) is a root.
This states that the process will go extinct with probability 0.
We can actually be certain that it will never go extinct since \(Y > 0\).

\subsubsection{Geometric (exclusive)}
\label{develop--math6180:ROOT:page--branching-processes.adoc---geometric-exclusive}

Let \(Y\sim Geometric(p)\) where \(P(Y=y) = p(1-p)^{y}\) for \(y=0,1,2,\ldots\).

Recall that

\begin{equation*}
G(s) = \frac{p}{1-(1-p)s}
\end{equation*}

So,

\begin{equation*}
\begin{aligned}
&G(s) = s
\\&\iff \frac{p}{1-(1-p)s} = s
\\&\iff p = s - (1-p)s^2
\\&\iff p = s - (1-p)s^2
\\&\iff (1-p)s^2 -s + p =0
\\&\iff (1-p)(s-1)s + (1-p)s -s + p =0
\\&\iff (1-p)(s-1)s -ps + p =0
\\&\iff (1-p)(s-1)s - p(s-1) =0
\\&\iff ((1-p)s - p)(s-1) =0
\end{aligned}
\end{equation*}

So either \(s=1\) or \(s = \frac{p}{1-p}\).
Note that the latter is the extinction probability iff

\begin{equation*}
\frac{p}{1-p} < 1 \iff p < 1-p \iff p < \frac{1}{2}
\end{equation*}

So, we have certain extinction with probability \(1\) iff the probability of success (probability of geometric process stopping),
\(p \geq \frac{1}{2}\).
Otherwise if \(p < \frac{1}{2}\), the probability of ultimate extinction of

\begin{equation*}
\frac{p}{1-p} = \frac{1}{E[Y]}
\end{equation*}

\subsection{Interesting observation}
\label{develop--math6180:ROOT:page--branching-processes.adoc---interesting-observation}

\begin{admonition-tip}[{}]
This is also documented in the Wikipedia page \url{https://en.wikipedia.org/wiki/Branching\_process} .
\end{admonition-tip}

Consider \(E[Y]\). Intuitively, if this number is greater than \(1\), we would
expect that we don't have certain (probability \(1\)) ultimate extinction.
Conversely, if it is less than or equal to \(1\), we have certain (probability \(1\)) ultimate extinction.
This intuition can actually be proven.

Consider the graph of \(G(s)-s\) and \(\frac{d}{ds} [G(s) - s]_{s=1} = E[Y] - 1\).

\begin{itemize}
\item If \(\frac{d}{ds} [G(s) - s]_{s=1} > 0\), we must have had another small root since
    
    \begin{itemize}
    \item the graph of \(G(s) -s \) is negative in the left neighbourhood of \(s=1\)
    \item \(G(0)-0\geq 0\)
    \end{itemize}
    
    So if \(E[Y] > 1\), the probability of ultimate extinction is less than \(1\) (by the intermediate value theorem)
\item If \(\frac{d}{ds} [G(s) -s]_{s=1} < 0\), this must have been the only root since the graph of \(G(s)\) is convex.
    To see this, note that
    
    \begin{equation*}
    \begin{aligned}
    G(\lambda s_1 + (1-\lambda)s_2)
    &= \sum_{y=0}^\infty (\lambda s_1 + (1-\lambda)s_2)^y P(Y=y)
    \\&\leq \sum_{y=0}^\infty (\lambda s_1^y + (1-\lambda)s_2^y) P(Y=y)
    \\&= \lambda G(s_1) + (1-\lambda)G(s_2)
    \end{aligned}
    \end{equation*}
    
    from the convexity of \(t^y\). From the properties of convex functions
    
    \begin{equation*}
    G(x) \geq 1 + G'(1)(x-1) = x + (1-G'(1))(1-x) > x
    \end{equation*}
    
    for all \(x \in [0,1)\). So \(s=1\) is the smallest root of \(G(s)=s\).
    Therefore if \(E[Y] < 1\), the probability of ultimate extinction is \(1\).
\end{itemize}

Note that if \(E[Y] = 1\), no conclusion can be made.
\end{document}
