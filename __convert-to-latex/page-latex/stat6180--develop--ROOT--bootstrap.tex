\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Bootstrap}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
The bootstrap is a resampling procedure used
to estimate population parameters and conduct statistical inference
when the distribution is unknown.
This method relies on the empirical CDF and
the fact that it converges to the true CDF.

Suppose that we have a random sample \(X_1, \ldots X_n\)
which is representative of the population.
Also suppose that we want to estimate some parameter \(\tau(\theta)\)
which can be estimated by estimator \(W_n(\vec{X})\).
To estimate \(\tau(\theta)\), we

\begin{enumerate}[label=\arabic*)]
\item Produce \(B\geq 10000\) bootstrapped samples.
    These are resamples of the original sample of size \(n\) with possible repetitions.
    
    \begin{equation*}
    \begin{array}{cccc}
    x_{11}^*, & x_{12}^*, & \cdots & x_{1n}^*\\
    x_{21}^*, & x_{22}^*, & \cdots & x_{2n}^*\\
    \vdots & \vdots & \ddots & \vdots\\
    x_{B1}^*, & x_{B2}^*, & \cdots & x_{Bn}^*\\
    \end{array}
    \end{equation*}
\item For each bootstrapped sample, compute the estimate \(\hat{\tau}(\theta)_j^* = W_n(\vec{x}_j^*)\)
\item Compute estimate \(\hat{\tau}(\theta)^* = \frac{1}{B}\sum_{j=1}^B W_n(\vec{x}_j^*)\)
\end{enumerate}

We can also compute confidence intervals by taking the appropriate quartiles of the
\(\hat{\tau}(\theta)_j^*\). The interpretation of the confidence intervals is the same
as the frequentist approach.

\section{Parametric boostrap}
\label{develop--stat6180:ROOT:page--bootstrap.adoc---parametric-boostrap}

In the parametric bootstrap, we

\begin{enumerate}[label=\arabic*)]
\item assume a family
    of distributions which the sample came from.
\item using the original data, produce estimates of the population parameters.
\item take \(B\) bootstrapped samples by sampling \(n\) values from the estimated
    distribution
\end{enumerate}

Then, we conduct the bootstrapping procedure as in the non-parametric bootstrap.

\section{Bootstrap estimation of hypotheses}
\label{develop--stat6180:ROOT:page--bootstrap.adoc---bootstrap-estimation-of-hypotheses}

Suppose that we have \(X_1, \ldots X_{n_X}\) and \(Y_1, \ldots Y_{n_Y}\)
and we want to assess the hypotheses

\begin{description}
\item[\(H_0:\)] \(\mu_X = \mu_Y\)
\item[\(H_1:\)] \(\mu_X > \mu_Y\)
\end{description}

To do this, we produce \(B\) bootstrapped  samples
of the form \(x^*_{k1},\ldots x^*_{kn_X}\) and \(y^*_{k1},\ldots y^*_{kn_Y}\).
And we produce an estimate for \(P(\overbar{x}^* > \overbar{y}^*)\).
This estimate will be the p-value.

\section{Studentized bootstrap/Bootstrap t}
\label{develop--stat6180:ROOT:page--bootstrap.adoc---studentized-bootstrapbootstrap-t}

The studentized bootstrap gives us a way to estimate the equivalent of \(t_{n-1}(\alpha/2)\)
for any distribution.
We perform the following steps

\begin{enumerate}[label=\arabic*)]
\item Estimate mean, \(\hat{\mu}\), and the standard deviation, \(\hat{\sigma}\), using the original sample.
\item Produce \(B\) bootstrapped resamples from the original sample
    and for each compute the mean and standard deviation
    \((\overbar{x}^*_k, s^*_k)\).
    The mean and standard deviation should be computed using the same method as \(\hat{mu}\)
    and \(\hat{\sigma}\) but instead using the bootstrapped sample
\item Compute
    
    \begin{equation*}
    z^*_k = \frac{\overbar{x}^*_k - \hat{\mu}}{s^*_k}
    \end{equation*}
\item Produce a confidence interval as
    
    \begin{equation*}
    \left(
    \hat{\mu} + z^*(\alpha/2)\hat{\sigma}
    ,\
    \hat{\mu} + z^*(1- \alpha/2)\hat{\sigma}
    \right)
    \end{equation*}
    
    where \(z^*(p)\) is the \(p\)'th quantile of the \(z^*\) values.
\end{enumerate}

The studentized bootstrap produces estimates with less bias and smaller variance
than the non-parametric bootstrap.

\begin{proposition}[{Multivariate delta-method}]
Let \(\vec{W}_n \sim AMVN\left(\vec{\theta}, \Sigma(\vec{\theta})\right)\).
Then if \(g\) is a continuous function satisfying \(\nabla g(\theta)\neq \vec{0}\).
Then,

\begin{equation*}
g(\vec{W}_n) \sim AN\left(g(\vec{\theta}),
\nabla g(\theta)^T \Sigma(\vec{\theta}) \nabla g(\theta)
\right)
\end{equation*}
\end{proposition}

\subsection{Studentized for weird statistic}
\label{develop--stat6180:ROOT:page--bootstrap.adoc---studentized-for-weird-statistic}

Suppose we have a linear model \(Y = \beta_0 + \beta_1 X + \varepsilon\)
and we want a confidence interval for \(\theta=\frac{\beta_1}{\beta_0}\).
We can perform the usual non-parametric bootstrap and produce a sequence
of estimates for \(\frac{\beta_1}{\beta_0}\) and produce a confidence interval.
However, it is also possible to use the studentized bootstrap.

To perform the studentized bootstrap, we need a way to estimate
the variance of \(\theta\).
Assuming that \((\hat{\beta}_0,\hat{\beta}_1)\) us asymptotically normal
with some covariance. Then

\begin{equation*}
\begin{bmatrix}\hat{\beta}_0\\\hat{\beta}_1\end{bmatrix}
\sim AMVN
\left(
\begin{bmatrix}\hat{\beta}_0\\\hat{\beta}_1\end{bmatrix}
,\
\begin{bmatrix}
Var(\hat{\beta}_0) & Cov(\hat{\beta}_0, \hat{\beta}_1)\\
Cov(\hat{\beta}_0, \hat{\beta}_1) & Var(\hat{\beta}_1)
\end{bmatrix}
\right)
\end{equation*}

If we define \(g(x,y) = \frac{y}{x}\),
\(\nabla g = \begin{bmatrix}\frac{-y}{x^2}\\ \frac{1}{x}\end{bmatrix}\).
Then,

\begin{equation*}
\begin{aligned}
Var\left(\frac{\hat{\beta}_1}{\hat{\beta}_0}\right)
&\approx
\begin{bmatrix}\frac{-\hat{\beta}_1}{\hat{\beta}_0^2}& \frac{1}{\hat{\beta}_0}\end{bmatrix}
\begin{bmatrix}
Var(\hat{\beta}_0) & Cov(\hat{\beta}_0, \hat{\beta}_1)\\
Cov(\hat{\beta}_0, \hat{\beta}_1) & Var(\hat{\beta}_1)
\end{bmatrix}
\begin{bmatrix}\frac{-\hat{\beta}_1}{\hat{\beta}_0^2}\\ \frac{1}{\hat{\beta}_0}\end{bmatrix}
\\&=
\frac{\hat{\beta}_1^2}{\hat{\beta}_0^4}Var(\hat{\beta}_0)
- 2\frac{\hat{\beta}_1}{\hat{\beta}_0^3}Cov(\hat{\beta}_0, \hat{\beta}_1)
+ \frac{1}{\beta_0^2}Var(\hat{\beta}_1)
\\&=
\left(\frac{\beta_1}{\beta_0}\right)^2
\left(
\frac{Var(\hat{\beta}_0)}{\beta_0^2}
- 2\frac{Cov(\hat{\beta}_0, \hat{\beta}_1)}{\beta_0\beta_1}
+ \frac{Var(\hat{\beta}_1)}{\beta_1^2}
\right)
\end{aligned}
\end{equation*}

The variances and covariances can be estimated using the \texttt{lm} and \texttt{vcov} functions.
The code is as follows

\begin{listing}[{}]
# input data
x <- c(0.01, 0.48, 0.71, 0.95, 1.19, 0.01, 0.48, 1.44, 0.71, 1.96, 0.01, 1.44, 1.96)
y <- c(127.6, 124.0, 110.8, 103.9, 101.5, 130.1, 122.0, 92.3, 113.1, 83.7, 128.0, 91.4, 86.2)

B <- 10000

# get initial estimates
reg0 <- lm(y ~ x)
b <- reg0$coefficients
V <- vcov(reg0)
theta.hat <- as.numeric(b[2] / b[1])
v.theta <- as.numeric(theta.hat**2 * (V[2,2] / b[2]**2 + V[1,1] / b[1]**2 - 2 * V[1,2] / b[1] / b[2]))

# get t-values
T.star <- rep(0, B)
for(j in 1:B) {
    iz <- sample(1:n, n, replace=TRUE)
    xx <- x[iz]
    yy <- y[iz]
    reg <- lm(yy ~ xx)
    bb <- reg$coefficients
    VV <- vcov(reg)
    tt <- as.numeric(bb[2] / bb[1])
    vv <- as.numeric(tt**2 * (VV[2,2] / bb[2]**2 + VV[1,1] / bb[1]**2 - 2 * VV[1,2] / bb[1] / bb[2]))
    T.star[j] = (tt - theta.hat) / sqrt(vv)
}

#produce CI
alpha <- 0.05
Q <- as.numeric(quantile(T.star, c(alpha / 2 , 1 - alpha / 2)))
CI.t <- theta.hat + Q * sqrt(v.theta) # bootstrap t interval
print(CI.t)
\end{listing}

\section{Bootstrapping dependent data}
\label{develop--stat6180:ROOT:page--bootstrap.adoc---bootstrapping-dependent-data}

Bootstrapping depends on the data being iid.
However, we sometimes would like to produce bootstraps for dependent data.

In the case of time-series,
we can split the data into blocks which we assume are iid.
For example, if we are looking at irradiance data, the blocks may be a single day each.
Then, we bootstrap the blocks (ie rebuild a timeseries using the blocks).

\section{Permutation test}
\label{develop--stat6180:ROOT:page--bootstrap.adoc---permutation-test}

Suppose we have data \(X_1,\ldots X_{n_X}\) and \(Y_1, \ldots Y_{n_Y}\)
and we want to test

\begin{description}
\item[\(H_0:\)] \(\mu_X = \mu_Y\)
\item[\(H_1:\)] \(\mu_X > \mu_Y\)
\end{description}

An alternative to bootstrapping is the \textbf{permutation test}.

It is conducted as follows

\begin{enumerate}[label=\arabic*)]
\item Compute \(diff = \overbar{x} - \overbar{y}\)
\item Create a single vector \(z = (x_1, \ldots x_{n_X}, y_1, \ldots y_{n_Y})\)
\item Repeat the following process \(B\) times
    
    \begin{enumerate}[label=\alph*)]
    \item Randomly partition \(z\) into \(x_1^*, \ldots x_{n_X}^*\) and \(y_1^*, \ldots y_{n_Y}^*\).
        This can either be done by
        
        \begin{itemize}
        \item sampling \(n_X\) of the indices \(1, \ldots (n_X+n_Y)\) for \(x\) and use the remainder for \(y\).
        \item shuffling \(z\) and selecting the first \(n_X\) as \(x\) and the last \(n_Y\) for \(y\)
        \end{itemize}
    \item Compute \(\hat{\mu}_X^*\) and \(\hat{\mu}_Y^*\)
    \item Record whether \(\hat{\mu}_X^* - \hat{\mu}_Y^* > diff\)
    \end{enumerate}
\item Set the p-value to be the proportion that \(\hat{\mu}_X^* - \hat{\mu}_Y^* > diff\)
\end{enumerate}

We can also perform a similar permutation test for ANOVA, but with a few key differences

\begin{itemize}
\item Instead of a difference, we us the F-statistic of the initial sample
\item The vector \(z\) now created from \(g\) sets of data.
    So, care needs to be taken when producing the permuted data
\item The F-statistic is compared against the initial F-statistic
\item The p-value is the proportion of permuted F-statistics that are greater than the initial F-statistic
\end{itemize}
\end{document}
