\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Rings}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \( (R, + , \cdot)\) be a non-empty set with two binary
operations. We call \( + \) \emph{addition} and \(\cdot\)
\emph{multiplication}. Then \((R, + , \cdot)\) is called a \emph{ring} if

\begin{enumerate}[label=\arabic*)]
\item \(\forall a, b \in R: a + b = b + a\)
\item \(\forall a, b, c, \in R: (a+b) + c = a+ (b+c)\)
\item \(\exists z \in R: \forall a \in R: a  +z = a\)
\item \(\forall a \in R: \exists (-a) \in R: a + (-a) = z\)
\item \(\forall a, b, c \in R: (a\cdot b) \cdot c = a \cdot (b \cdot c)\)
\item \(\forall a, b, c \in R: a\cdot (b+c) = (a\cdot b) + (a \cdot c) \text{ and } (b+c) \cdot a = (b \cdot a) + (c\cdot a)\)
\end{enumerate}

Alternatively, we could simplify the above definition as follows.
\((R, +, \cdot)\) is a ring iff

\begin{enumerate}[label=\arabic*)]
\item \((R, +)\) forms an abelian group.
\item \((R, \cdot)\) forms a semigroup.
\item \(\cdot\) distributes over \(+\) in
    \(R\).
\end{enumerate}

Furthermore, \((R, + , \cdot)\) is called

\begin{itemize}
\item a \emph{commutative ring} if \(\cdot\) is commutative
\item a \emph{ring with unity} if
    \(\exists u \in R: \forall a \in R: a\cdot u = u \cdot a\).
    That is, \((R, \cdot)\) forms a monoid.
\end{itemize}

We call \(z\) the \emph{zero element} and \(u\) \emph{unity}.

\section{Nice properties}
\label{develop--math2272:ROOT:page--rings/index.adoc---nice-properties}

Let \((R, +, \cdot)\) be a ring and
\(a, b, c \in R\). Then we have the following properties

\begin{enumerate}[label=\arabic*)]
\item \(a + b = a \implies b = z\) (uniqueness of identity).
\item \(-(-a) = a\)
\item \(-(a+b) = (-a) + (-b)\)
\item \((a + (-b)) + (-c) = a + (-(b+c))\)
\item \(a\cdot z = z\cdot a = z\)
\item \(a\cdot( -b) = (-a)\cdot b = - (a\cdot b)\)
\end{enumerate}

\section{Convensions}
\label{develop--math2272:ROOT:page--rings/index.adoc---convensions}

Let \((R, + , \cdot )\) be a ring and \(a, b \in R\)
and \(m, n \in \mathbb{Z}^+ \), then we define the following
convensions

\begin{enumerate}[label=\arabic*)]
\item \(a -b := a + (-b)\)
\item \(ab := a\cdot b\)
\item \(-ab := -(ab) = (-a)b = a(-b)\)
\item \(ma := (m-1)a + a\) where \(1a := a\).
\item \((-m)a = m(-a)\)
\item \(a^m = a^{m-1}a\) where \(a^1 = a\).
\item \(a^0 = u\) if \(u\) exists.
\item \(a^{-m} = (a^{-1})^m\) if \(u\) and
    \(a^{-1}\) exist.
\end{enumerate}

\section{Zero divisors}
\label{develop--math2272:ROOT:page--rings/index.adoc---zero-divisors}

Let \(R\) be a ring, then \(a \in R - \{z\}\) is a
\emph{zero divisior} iff

\begin{equation*}
\exists b \in S - \{z\}: ab = z
\end{equation*}

\section{Subrings}
\label{develop--math2272:ROOT:page--rings/index.adoc---subrings}

Let \(R\) be a ring. Then, a \emph{subring} \(S\) of
\(R\) is a subset of \(R\) which is also a ring with
the same operations.

\subsection{Two step subring test}
\label{develop--math2272:ROOT:page--rings/index.adoc---two-step-subring-test}

Let \(R\) be a ring and \(S\) be a non-empty subset
of \(R\), then \(S\) is a subring of \(R\)
iff \(\forall a,b \in S: a-b \in S \ \land \ ab \in S\).

\subsection{Three step subring test}
\label{develop--math2272:ROOT:page--rings/index.adoc---three-step-subring-test}

Let \(R\) be a ring and \(S\) be a non-empty subset
of \(R\), then \(S\) is a subring iff
\(\forall a, b \in S: a+b \in S\ \land\ ab \in S\ \land\ -a \in S\).

\section{Characteristic}
\label{develop--math2272:ROOT:page--rings/index.adoc---characteristic}

Let \(R\) be a ring then

\begin{itemize}
\item if \(\exists n \in \mathbb{Z}^+: \forall r \in R: nr = z\),
    the smallest such \(n\) is called the \emph{characteristic of}
    \(R\).
\item otherise, \(R\) has characteristic \(0\).
\end{itemize}

\section{Integral domain}
\label{develop--math2272:ROOT:page--rings/index.adoc---integral-domain}

Let \(R\) be a commutative ring with unity, then
\(R\) is called an \emph{integral domain} iff \(R\)
contains no zero divisors. That is,

\begin{equation*}
\forall a, b \in R: ab = z \iff a = z \text{ or } b = z
\end{equation*}

then,

\begin{itemize}
\item the cancellation law for multiplication holds. That is
    
    \begin{equation*}
    \forall a \in R -\{z\}: \forall b,c \in R: ab = ac \ \lor \ ba = ca \implies b = c
    \end{equation*}
\end{itemize}

\subsection{Integers modolo n}
\label{develop--math2272:ROOT:page--rings/index.adoc---integers-modolo-n}

Consider the ring of \(\mathbb{Z}_n\) and notice that it is a
commutative ring with unity. Then, we have the following theorem

\begin{equation*}
\mathbb{Z}_n \text{ is an integral domain} \iff n \text{ is prime}
\end{equation*}

\section{Division ring}
\label{develop--math2272:ROOT:page--rings/index.adoc---division-ring}

Let \(R\) be a ring. Then, \(R\) is called a
\emph{division ring} iff \((R -\{z\}, \cdot)\) forms a group.

\section{Ideals}
\label{develop--math2272:ROOT:page--rings/index.adoc---ideals}

Let \(R\) be a ring. Then, \(I \subseteq R\) is a

\begin{itemize}
\item \emph{right ideal} of \(R\) if \((I, +)\) is a subgroup
    of \((R, +)\) and
    
    \begin{equation*}
    \forall r \in R: \forall a \in I: ar \in I
    \end{equation*}
\item \emph{left ideal} of \(R\) if \((I, +)\) is a subgroup
    of \((R, +)\) and
    
    \begin{equation*}
    \forall r \in R: \forall a \in I: ra \in I
    \end{equation*}
\end{itemize}

Furthermore \(I\) is simply called a \emph{ideal} of
\(R\) if it is both a right and left ideal. Also, we call
\(I = \{z\}\) and \(I = R\) \emph{trivial ideals}.

\begin{admonition-note}[{}]
This is a stronger condition than closure of multiplication which is
used in subrings. Also, ideals are said to \emph{absorb} multiplication (from
the left and/or right).
\end{admonition-note}

\subsection{The only ideal to contain unity is the ring itself}
\label{develop--math2272:ROOT:page--rings/index.adoc---the-only-ideal-to-contain-unity-is-the-ring-itself}

Let \(R\) be a ring with unity. Then if
\(I \subseteq R\) is an ideal (left or right)

\begin{equation*}
e \in I \iff I = R
\end{equation*}

\begin{example}[{Proof}]
Notice that the reverse direction is trivial. Now, suppose WLOG
that \(I\) is a left ideal of \(R\) where \(e\in I\). Then

\begin{equation*}
\forall r \in R: re = r \in I
\end{equation*}

and we are done.
\end{example}

\subsection{Principal Ideals}
\label{develop--math2272:ROOT:page--rings/index.adoc---principal-ideals}

Let \(R\) be a commutative ring with unity, then

\begin{equation*}
(a) = \{ar: r \in R\}
\end{equation*}

is called a \emph{principal ideal} where \(a \in \mathbb{R}\).
Then, \((a)\) is also a ideal.

\begin{example}[{Proof}]
Firstly, note that by the one step subgroup test, \(((a), + )\) is a
subgroup of \((R, + )\) since

\begin{equation*}
    \forall ar, as \in (a): ar - as = a(r-s) \in (a)
\end{equation*}

since \(r-s \in R\). Finally, notice that \((a)\) is an ideal since

\begin{equation*}
    \forall t \in R: \forall ar \in (a): t(ar) = (ar)t  = a(rt) \in (a)
\end{equation*}
\end{example}

\subsection{One sided principal ideals}
\label{develop--math2272:ROOT:page--rings/index.adoc---one-sided-principal-ideals}

Let \(R\) be a ring with ideal \(I\). Then,
\(I\) is called a

\begin{itemize}
\item \emph{right principal ideal} if \(\exists a \in I\) such that
    \(I = \{ar : r \in R\}\)
\item \emph{left principal ideal} if \(\exists a \in I\) such that
    \(I = \{ra : r \in R\}\)
\end{itemize}

If \(I\) is both a left and right principal ideal,
\(I\) is also a principal ideal. Note that the condition that
\(I\) be an ideal is necessary otherwise absorbtion from both
sides would not be guaranteed.

\subsection{All ideals in the integers are principal ideals}
\label{develop--math2272:ROOT:page--rings/index.adoc---all-ideals-in-the-integers-are-principal-ideals}

Consider the ring of integers with ideal \(I\), then
\(I\) is a principal ideal.

\begin{example}[{Proof}]
Let \(I \subseteq \mathbb{Z}\) be an ideal and let \(a\) be the least positive integer
in \(I\).

\begin{admonition-note}[{}]
If \(I = \{0\}\) it is automatically principal. Otherwise, there exists \(x \in I \backslash \{0\}\).
Then either \(x\) or \(-x \in I\), one of which must be positive. Therefore \(a\) exists.
\end{admonition-note}

Now, consider any integer \(b \in I\) and perform the division algorithm on
it with respect to \(a\). That is, we write \(b = ka + r\) where \(k \in \mathbb{Z}\)
and \(0 \leq r < a\). Then, since \(I\) is an ideal, \(ka \in I\) and hence
\(r = b-ka \in I\). Notice that since \(a\) is minimal, \(r = 0\) and \(b = ka\) and
we are done.
\end{example}

\section{Quotient Rings}
\label{develop--math2272:ROOT:page--rings/index.adoc---quotient-rings}

Let \(R\) be a ring with ideal \(J\). Then, the set
of additive cosets of \(J\) form a ring where

\begin{equation*}
\forall (a + J), (b + J) \in R/J: (a+J) + (b + J) = (a+b) + J
\end{equation*}

and

\begin{equation*}
\forall (a + J), (b + J) \in R/ J: (a + J)(b + J) = ab + J
\end{equation*}

both of these operations are well defined satisfy the ring axioms.
We call this ring the \emph{quotient ring of} \(J\) in
\(R\).

\begin{example}[{Proof of well defined}]
Let \(a, a', b, b' \in R\) such that

\begin{equation*}
a + J = a' + J \quad\text{and}\quad b + J = b' + J
\end{equation*}

Then, \(a - a' \in J\) and \(b - b' \in J\). Next, notice that
since \(J\) is an ideal

\begin{equation*}
(a-a') + (b - b') = (a+b) - (a' + b') \in J
\implies (a+b) + J = (a' + b') + J
\end{equation*}

Therefore, addition is well defined.

Next, notice that

\begin{equation*}
\begin{aligned}
&(a-a') b = ab - a'b \in J\quad\text{and}\quad a'(b-b') = a'b - a'b' \in J
\\&\quad\implies ab - a'b' \in J
\\&\quad\implies ab + J = a'b'+ J
\end{aligned}
\end{equation*}

Therefore, multiplication is well defined.
\end{example}

\begin{example}[{Proof of ring axioms}]
We would do this in two parts.

Firstly, lets prove that \((R/J, + )\) is an abelian group.
Notice that since \((R, + )\) is an abelian group, \((J, + )\) is a normal subgroup,
and hence \((R/J, + )\) is a \myautoref[{quotient group}]{develop--math2272:ROOT:page--normal/index.adoc---quotient-groups}. Furthermore,
it is abeliean.

Next, we need to show that \((R/J, \cdot)\) forms a \myautoref[{semigroup}]{develop--math2272:ROOT:page--groups/index.adoc}.

\begin{description}
\item[Closure] Consider \((a + J), (b + J) \in R/J\) then by definition of its operation \(ab + J \in R/J\)
\item[Associative] Since \((R, \cdot)\) forms a semigroup, \(\cdot\) is associative and hence if \((a + J), (b + J), (c+ J) \in R/J\)
    
    \begin{equation*}
    \begin{aligned}
    [(a+J)(b+J)](c+J)
    &= (ab + J)(c+J)
    \\&= (ab)c + J
    \\&= a(bc) + J
    \\&= (a + J)(bc + J)
    \\&= (a + J)[ (b + J)(c + J) ]
    \end{aligned}
    \end{equation*}
\end{description}
\end{example}

\section{Ring homomorphisms}
\label{develop--math2272:ROOT:page--rings/index.adoc---ring-homomorphisms}

Let \(R\) and \(S\) be rings (not necessarily with
the same operations, then \(f\colon R \to S\) is called a
\emph{ring homomorphism} if

\begin{equation*}
\forall a, b \in R: f(a + b) = f(a) + f(b) \text{ and } f(a\cdot b) = f(a) \cdot f(b)
\end{equation*}

Furthermore, if \(f\) is bijective, \(f\) is called
a \emph{isomorphism} and \(R\) is said to be \emph{isomorphic} to
\(S\).

\subsection{Nice properties}
\label{develop--math2272:ROOT:page--rings/index.adoc---nice-properties-2}

Let \(R\) and \(S\) be rings with homomorphism
\(f \colon R \to S\), then

\begin{itemize}
\item \(f(z_R) = z_S\)
\item \(f(e_R) = e_S\) if \(R\) is a ring with unity and
    \(f\) is surjective
\end{itemize}

\begin{example}[{Proof}]
The first property follows directly from the fact that \((R, + )\) and \((S, + )\)
are groups and hence \(f\) is also a group homomorphism from \(R\) to \(S\).

For the second. Consider \(s \in S\). Then since \(f\) is surjective, there exists
\(r \in R\) such that \(f(r) = s\). Then

\begin{equation*}
s f(e_R) = f(r)(e_R) = f(re_R) = f(r) = s = f(r) = f(e_R r ) = f(e_R) f(r) = f(e_R) s
\end{equation*}

and hence \(f(e_R)\) is the unity within \(S\).
\end{example}

\subsection{Kernel}
\label{develop--math2272:ROOT:page--rings/index.adoc---kernel}

Let \(R\) and \(S\) be rings with homomorphism
\(f\colon R\to S\), then

\begin{equation*}
\ker f = \{r \in R: f(r) = z \in S\}
\end{equation*}

Then, \(\ker f = \{z\} \iff f\) is injective.

\begin{admonition-note}[{}]
This directly follows from the fact that \((R, + )\) and \((S, + )\) are groups
and hence \(f\) is a group homomorphism between the two.
\end{admonition-note}
\end{document}
