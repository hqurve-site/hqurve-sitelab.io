\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Number of topologies on a finite set}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Consider the set \(J_n = \{1, \ldots n\}\). Then, an obvious question is ``How many topologies are there on \(J_n\)?''.
In this page, we seek to find an answer.

First notice that a topology is a subset of \(\mathcal{P}(J_n)\).
Since there are only finitely many subsets (\(2^{2^n}\)), the number of topologies on \(J_n\) must also be finite.

Before counting, let us establish a few results

\section{The smallest basis}
\label{develop--math6620:appendix:page--counting-finite.adoc---the-smallest-basis}

In general, there is no such thing as the smallest basis. However, in finite sets, this concept exists.
Let \(\mathcal{T}\) be a topology on \(J_n\) and let \(\{\mathcal{B}_\alpha\}_{\alpha \in I}\) be the set
of bases that generate \(\mathcal{T}\). Then,

\begin{equation*}
\bigcap_{\alpha \in I}\mathcal{B}_\alpha
\end{equation*}

is also a basis which generates \(\mathcal{T}\). Since \(I\) must be finite, it is sufficient to prove
that the intersection of two bases which generate \(\mathcal{T}\) is also a basis which generates \(\mathcal{T}\).

\begin{proof}[{}]
Let \(\mathcal{B}_1\) and \(\mathcal{B}_2\) be basis which generate \(\mathcal{T}\).
Consider arbitrary open \(U \in \mathcal{T}\). Then, we want to show that there exists \(B_1, \ldots, B_k \in \mathcal{B}_1 \cap \mathcal{B}_2\)
such that \(U = \bigcup_{i=1}^k B_k\).

\begin{admonition-note}[{}]
This claim would prove that \(\mathcal{B}_1 \cap \mathcal{B}_2\) is (1) a basis on \(J_n\) and (2) generates \(\mathcal{T}\)
\end{admonition-note}

To prove this, we need a bit extra machinery. Suppose that \(U = B_1 \cup \cdots B_k\).
Then we define \(f(\{B_1, \ldots B_k\})\) to be the vector in \(\mathbb{N}^n\) such that
the \(i\)'th entry of the vector is the number of \(\{B_1, \ldots B_k\}\) with \(i\) elements.
We may then order these vectors using the lexicographical order of the reversed vectors.
Therefore, the ordering is a total order.

Let \(U = B_1 \cup \cdots B_k\) where \(B_1, \ldots B_k \in \mathcal{B}_1\) which minimizes
\(f(\{B_1, \ldots B_k\})\). Note that a minimum exists since \(\mathcal{B}_1\).
However, we do not know whether this minimum is uniquely obtained as yet.
We do not need this minimum to be uniquely determined for the remainder of the proof.

Since \(B_1, \ldots B_k\) are open in \(\mathcal{T}\).
Each are representable using \(\mathcal{B}_2\) and hence we have
an alternative representation for \(U = B_1' \cup \cdots B_{k'}'\)
where \(B_1' \ldots B_{k'}' \in \mathcal{B}_2\).
Notice that \(f(\{B_1', \ldots, B_{k'}'\}) \leq f(\{B_1, \ldots, B_{k}\})\)
since we are representing the \(B_i\) using the union of smaller elements in \(\mathcal{B}_2\).

Since \(B_1' \ldots B_{k'}' \in \mathcal{T}\), we can construct a third representation
of \(U\) using elements \(B_1'', \ldots B_{k''}'' \in \mathcal{B}_1\) by representing each of the \(B_j'\) as unions
of elements in \(\mathcal{B}_1\).
From similar reasoning as above, we obtain that

\begin{equation*}
f(\{B_1'', \ldots, B_{k''}''\}) \leq f(\{B_1', \ldots, B_{k'}'\}) \leq f(\{B_1, \ldots, B_{k}\})
\end{equation*}

and due to the minimality of \(B_1\cup \cdots B_k\), we obtain that
\(f(\{B_1', \ldots, B_{k'}'\}) = f(\{B_1, \ldots, B_{k}\})\).
This implies that the representations in \(\mathcal{B}_1\) and \(\mathcal{B}_2\) must be the same.
This actually needs to be proven in \(n\) steps starting with \(k=n\) and going to \(k=1\).

\begin{itemize}
\item Note that the number of \(B_j\) and \(B_j'\) with cardinality \(n\) are the same. This implies that
    each of the \(B_j\) corresponds with a \(B_j'\) since the \(B_j'\) were constructed by representing
    the \(B_j\) using \(\mathcal{B}_2\).
\item For step \(k < n\), the number of \(B_j\) and \(B_j'\) with cardinality \(k\) are the same.
    Since the \(B_j\) with cardinality greater than \(k\) were represented by a single element in \(\mathcal{B}_2\),
    there these \(B_j'\) with cardinality \(k\) must be used only to represent \(B_j\) with cardinality \(k\).
    Therefore, each of the \(B_j'\) with cardinality \(k\) corresponds with exactly one \(B_j\) with cardinality \(k\).
\end{itemize}

Since the representations in \(\mathcal{B}_1\) and \(\mathcal{B}_2\) are the same,
there exists elements in \(\mathcal{B}_1 \cap \mathcal{B}_2\) whose union is equal to \(U\).
Therefore, we have proven that \(\mathcal{B}_1 \cap \mathcal{B}_2\) is a basis which generates
\(\mathcal{T}\)
\end{proof}

Since we know that a smallest basis exists for each topology in \(J_n\), it suffices to count the number of smallest bases.
Using smallest bases is preferable since it allows us to know that two topologies are equal iff their smallest bases are equal.
However, we still need a way to know whether a given collection of subsets of \(J_n\) is a smallest basis.

\subsection{Characterization of smallest basis}
\label{develop--math6620:appendix:page--counting-finite.adoc---characterization-of-smallest-basis}

Let \(\mathcal{B}\) be a basis in \(J_n\). Then the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(\mathcal{B}\) is a smallest basis
\item For each \(\mathcal{B}'\) which generates the same topology as \(\mathcal{B}\), \(\mathcal{B} \subseteq \mathcal{B}'\).
\item For each \(B \in \mathcal{B}\), there does not exist \(B_1, \ldots B_k \in \mathcal{B}\)
    such that \(B = B_1 \cup \cdots B_k\)
\item For each \(B \in \mathcal{B}\), there exists \(x \in B\) such that
    \(\forall B' \in \mathcal{B}: B' \subseteq B \implies x \notin B'\)
\end{enumerate}

WIP : prove and use to count
\end{document}
