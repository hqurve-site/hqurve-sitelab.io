\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Multivariate normal distribution}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

% Title omitted
Let \(\vec{X} = (X_1, \ldots X_n)\) be a random vector. Then, \(\vec{X}\)
has a \emph{multivariate normal distribution} iff one of the following equivalent
conditions

\begin{enumerate}[label=\arabic*)]
\item There exists a real vector \(\vec{\mu}\) and
    \href{https://en.wikipedia.org/wiki/Definite\_matrix}{positive definite} matrix \(\Sigma\)
    such that \(\vec{X}\) has joint pdf
    
    \begin{equation*}
    f_{\vec{X}}(\vec{x}) = \frac{1}{\sqrt{(2\pi)^n |\Sigma|}}\exp\left\{
    -\frac{(\vec{x} - \vec{\mu})^T \Sigma^{-1}(\vec{x} - \vec{\mu})}{2}
    \right\}
    \end{equation*}
\item There is a real vector \(\vec{\mu}\) and
    real matrix \(D\) such that
    
    \begin{equation*}
    \vec{X} = D\vec{W} + \vec{\mu}
    \end{equation*}
    
    where \(\vec{W}\) consists of independent \(N(0,1)\)
    random variables
\item For each vector \(\vec{a} \in \mathbb{R}^n\), \(\vec{a}^T\vec{X}\)
    has a normal distribution.
\end{enumerate}

We then let the covariance matrix of \(\vec{X}\) be denoted by \(\Sigma\) where
\(\Sigma_{ij} = Cov(X_i, X_j)\) and the mean of \(\vec{X}\) be \(\vec{\mu}\).
We then write \(\vec{X} \sim MVN(\vec{\mu}, \Sigma)\) and notice that a multivariate
normal distribution is completely defined by its mean vector and covariance matrix.
Furthermore,

\begin{itemize}
\item \(\Sigma\) in definition 1 is consistent with \(\Sigma = Cov(\vec{X},\vec{X})\).
\item \(\vec{\mu}\) in definitions 1 and 2 is consistent with \(\vec{\mu} = E(\vec{X})\)
    (this is not hard to see)
\item \(\Sigma = DD^T\)
\item The matrix \(D\) is not unique.
\item If \(\Sigma\) is diagonal, there exists diagonal \(D\) such that \(DD^T = \Sigma\) and hence
    the components of \(\vec{X}\) are independent (the converse is also true).
\item Some authors prefer to write \(\vec{X} \sim \mathcal{N}(\vec{\mu},\Sigma)\)
\end{itemize}

\begin{admonition-important}[{}]
Most of the information for these definitions came from

\begin{itemize}
\item Dr. Aimin Huang's Mathbook \url{http://math.7starsea.com/post/274}
    (\href{https://web.archive.org/web/20201127235124/http://math.7starsea.com/post/274}{wayback archive})
    (also see his
    \href{https://web.archive.org/web/20190718201757/http://career.7starsea.com/}{career page})
\item MIT 6.436J/15.085J Fundamentals of Probability Fall 2008 Lectures
    \href{https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-436j-fundamentals-of-probability-fall-2008/lecture-notes/MIT6\_436JF08\_lec15.pdf}{15} \&
    \href{https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-436j-fundamentals-of-probability-fall-2008/lecture-notes/MIT6\_436JF08\_lec16.pdf}{16}
    (wayback archive
    \href{https://web.archive.org/web/20170830024551mp\_/https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-436j-fundamentals-of-probability-fall-2008/lecture-notes/MIT6\_436JF08\_lec15.pdf}{15} \&
    \href{https://web.archive.org/web/20170830081431/https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-436j-fundamentals-of-probability-fall-2008/lecture-notes/MIT6\_436JF08\_lec16.pdf}{16})
\end{itemize}
\end{admonition-important}

\begin{example}[{Proof of equivalence of 1 and 2}]
\begin{admonition-important}[{}]
This proof is taken from the MIT lecture 16.
\end{admonition-important}

Let \(\vec{X}\) be defined by the first definition. Since \(\Sigma\) is positive definite, there exists invertible \(D\)
such that \(DD^T = \Sigma\). Now, we let

\begin{equation*}
\vec{W} = D^{-1}(\vec{X}-\vec{\mu})
\end{equation*}

Then, the \(ij\) entry in the jacobian of transformation is given by

\begin{equation*}
\frac{\partial x_i}{w_j} = \frac{\partial}{\partial w_j}(\sum_{k=1}^n d_{ik}w_k + \mu_i)
= \frac{\partial}{\partial w_j}d_{ij}w_j = d_{ij}
\end{equation*}

Then, the jacobian of transformation is given
by \(|D|\) and hence
given by

\begin{equation*}
\begin{aligned}
f_{\vec{W}}(\vec{w})
&= f_{\vec{X}}(D\vec{w} + \vec{\mu}) |D|
\\&= \frac{1}{\sqrt{(2\pi)^n |\Sigma|}}\exp\left\{
    -\frac{\vec{w}^T D^T \Sigma^{-1} D\vec{w}}{2}
\right\}
\ |D|
\\&= \frac{1}{\sqrt{(2\pi)^n |D|^2}}\exp\left\{
    -\frac{\vec{w}^T\vec{w}}{2}
\right\}
\ |D|
\\&= \frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\vec{w}^T\vec{w}}{2}
\right\}
\end{aligned}
\end{equation*}

Which is clearly the joint pdf of \(n\) independent \(N(0,1)\) random variables.

For the reverse direction, is again a transformation of random variables and we basically run this proof in reverse.
\end{example}

\begin{example}[{Proof of equivalence of 2 and 3}]
\begin{admonition-important}[{}]
This proof is taken from the MIT lecture 16.
\end{admonition-important}

For the forward direction, \(\vec{a}^T \vec{X}\) is a linear combination of independent \(N(0,1)\)
distributions and hence is also normal.

\begin{admonition-important}[{}]
If the \(\Sigma\) is not positive definite, this means that there is \(\vec{v} \in null(\Sigma)\)
and hence a linear combination which leads to \(0\). In such a case, one of the variables could be written
as a linear combination of the others. We do this until we get \(\Sigma\) to be positive definite. We
then conduct the proof with the remaining set of random variables and then reconstruct our original set of variables.
\end{admonition-important}

For the reverse direction, let \(\Sigma = Cov(\vec{X},\vec{X})\) and \(\vec{\mu} = E(X)\). Then,
\(\Sigma\) is positive definite since

\begin{equation*}
\begin{aligned}
\forall \vec{v} \in \mathbb{R}^n&:
\vec{v}^T Cov(\vec{X}, \vec{X}) \vec{v}
\\&= \vec{v}^T E[(\vec{X} -\vec{\mu})(\vec{X} - \vec{\mu})^T] \vec{v}
\\&=  E[\vec{v}^T(\vec{X} -\vec{\mu})(\vec{X} - \vec{\mu})^T\vec{v}]
\\&=  E[(\vec{v}^T\vec{X} -\vec{v}^T\vec{\mu})(\vec{v}^T\vec{X} - \vec{v}^T\vec{\mu})^T]
\\&= Cov(\vec{v}^T\vec{X}, \vec{v}^T\vec{X})
\\&= Var(\vec{v}^T\vec{X}) > 0
\end{aligned}
\end{equation*}

since \(E(\vec{v}^T\vec{X}) = \vec{v}^T\vec{\mu}\).
Hence there exists invertible \(D\) such that \(\Sigma = DD^T\). We now
define \(\vec{W} = D^{-1}(\vec{X} - \vec{\mu})\). Then,
\(E(\vec{W}) = \vec{0}\) and

\begin{equation*}
\begin{aligned}
Cov(\vec{W}, \vec{W})
&= Cov(D^{-1}(\vec{X} - \vec{\mu}),D^{-1}(\vec{X} - \vec{\mu}))
\\&= E(D^{-1}(\vec{X} - \vec{\mu})(\vec{X} - \vec{\mu})^T(D^{-1})^T)
\\&= D^{-1}E((\vec{X} - \vec{\mu})(\vec{X} - \vec{\mu})^T)(D^{-1})^T
\\&= D^{-1}Cov(\vec{X},\vec{X})(D^{-1})^T
\\&= D^{-1}\Sigma(D^{-1})^T
\\&= D^{-1}DD^T(D^{-1})^T
\\&= I
\end{aligned}
\end{equation*}

Furthermore, \(\vec{W}\) is also multivariate normal (definition 3) since for each \(\vec{s}^T\),

\begin{equation*}
\vec{s}^T\vec{W} = \vec{s}^TD^{-1}\vec{X} - \vec{s}^TD^{-1}\vec{\mu}
\end{equation*}

which is a linear combination of each of the entries in \(\vec{X}\) and hence is normally
distributed. Furthermore, \(E(\vec{s}^T\vec{W}) = \vec{s}^TE(\vec{W}) = 0\) and

\begin{equation*}
\begin{aligned}
Var(\vec{s}^T\vec{W})
&= Cov(\vec{s}^T\vec{W}, \vec{s}^T\vec{W})
\\&= E(\vec{s}^T\vec{W}\vec{W}^T\vec{s})
\\&= \vec{s}^TE(\vec{W}\vec{W}^T)\vec{s}
\\&= \vec{s}^TCov(\vec{W},\vec{W})\vec{s}
\\&= \vec{s}^TI\vec{s}
\\&= \vec{s}^T\vec{s}
\end{aligned}
\end{equation*}

Now, we compute the mgf of \(W\) as

\begin{equation*}
M_{\vec{W}}(\vec{t})
= E[e^{\vec{t}^T\vec{W}}]
= M_{\vec{t}^T\vec{W}}(1)
= \exp\left\{\frac{Var(\vec{t}^T\vec{W})}{2}\right\}
= \exp\left\{\frac{\vec{t}^T\vec{t}}{2}\right\}
= \exp\left\{\frac{1}{2}\sum_{i=1}^nt_i^2\right\}
\end{equation*}

since \(\vec{t}^T\vec{W}\) is normally distributed with mean \(0\).
This is precisely the mgf of \(n\) independent \(N(0,1)\) random variables
and by the uniqueness of mgfs, \(\vec{W} = D^{-1}(\vec{X}-\vec{\mu})\)
has is the joint distribution of \(n\) independent \(N(0,1)\) random variables.
Hence \(\vec{X}\) satisfies definition 2
\end{example}

\begin{example}[{Proof of consistency of definition for \(\Sigma\)}]
Firstly, since \(\Sigma\) is positive definite, there exists invertible
matrix \(D\) such that \(\Sigma = DD^T\). Then, we want to compute the following integral

\begin{equation*}
\begin{aligned}
Cov(\vec{X}, \vec{X})
&= E[(\vec{X} - \vec{\mu})(\vec{X} - \vec{\mu})^T]
\\&= \int_{\mathbb{R}^n} (\vec{y} - \vec{\mu})(\vec{y} - \vec{\mu})^T
\frac{1}{\sqrt{(2\pi)^n |\Sigma|}}\exp\left\{
    -\frac{(\vec{x} - \vec{\mu})^T \Sigma^{-1}(\vec{x} - \vec{\mu})}{2}
\right\}
\ d\vec{x}
\end{aligned}
\end{equation*}

Now, we would use the change of variables defined by \(\vec{Y} = D^{-1}(\vec{X} - \vec{\mu})\).
Then \(\vec{X} = D\vec{Y} + \vec{\mu}\) and

\begin{equation*}
\frac{\partial x_i}{y_j} = \frac{\partial}{\partial y_j}(\sum_{k=1}^n d_{ik}y_k + \mu_i)
= \frac{\partial}{\partial y_j}d_{ij}y_j = d_{ij}
\end{equation*}

Therefore the jacobian of transformation is given by \(D\) and hence

\begin{equation*}
\begin{aligned}
&\int_{\mathbb{R}^n} (\vec{X} - \vec{\mu})(\vec{X} - \vec{\mu})^T
\frac{1}{\sqrt{(2\pi)^n |\Sigma|}}\exp\left\{
    -\frac{(\vec{x} - \vec{\mu})^T \Sigma^{-1}(\vec{x} - \vec{\mu})}{2}
\right\}
\ d\vec{x}
\\&=
\int_{\mathbb{R}^n} D\vec{y}\vec{y}^TD^T
\frac{1}{\sqrt{(2\pi)^n |\Sigma|}}\exp\left\{
    -\frac{\vec{y}^T D^T \Sigma^{-1} D\vec{y}}{2}
\right\}
\ |D|\ d\vec{y}
\\&=
D\left[\int_{\mathbb{R}^n} \vec{y}\vec{y}^T
\frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\vec{y}^T D^T ((D^{-1})^T D^{-1}) D\vec{y}}{2}
\right\}
\ d\vec{y}\right] \frac{|D|}{\sqrt{|\Sigma|}}D^T
\\&=
D\left[\int_{\mathbb{R}^n} \vec{y}\vec{y}^T
\frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\vec{y}^T\vec{y}}{2}
\right\}
\ d\vec{y}\right] \frac{|D|}{\sqrt{|\Sigma|}}D^T
\\&=
D\left[\int_{\mathbb{R}^n} \vec{y}\vec{y}^T
\frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\sum_{i=k}^n y_k^2}{2}
\right\}
\ d\vec{y}\right] \frac{|D|}{\sqrt{|\Sigma|}}D^T
\end{aligned}
\end{equation*}

Now, consider \(y_iy_j\) in the product \(\vec{y}\vec{y}^T\). Then, if \(i=j\),
we get

\begin{equation*}
\begin{aligned}
&\int_{\mathbb{R}^n} y_i^2
\frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\sum_{i=k}^n y_k^2}{2}
\right\}
\\&= \int_\mathbb{R} y_i^2\frac{1}{\sqrt{2\pi}}\exp\left\{\frac{-y_i^2}{2}\right\} \ dy_i
\prod_{k\neq i} \left(\int_\mathbb{R}\frac{1}{\sqrt{2\pi}} \exp\left\{\frac{-y_k^2}{2}\right\} \ dy_k\right)
\\&= 1 (1)^{n-1} = 1
\end{aligned}
\end{equation*}

Since the first integral is simply the integral for the variance of a \(N(0,1)\) distribution
while the second is the integral for total probability of a \(N(0,1)\) distribution.
Otherwise if \(i\neq j\), we get

\begin{equation*}
\begin{aligned}
&\int_{\mathbb{R}^n} y_iy_j
\frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\sum_{i=k}^n y_k^2}{2}
\right\}
\\&= \int_\mathbb{R} y_i\frac{1}{\sqrt{2\pi}}\exp\left\{\frac{-y_i^2}{2}\right\} \ dy_i
     \int_\mathbb{R} y_j\frac{1}{\sqrt{2\pi}}\exp\left\{\frac{-y_j^2}{2}\right\} \ dy_j
\prod_{k\notin \{i,j\}} \left(\int_\mathbb{R}\frac{1}{\sqrt{2\pi}} \exp\left\{\frac{-y_k^2}{2}\right\} \ dy_k\right)
\\&= (0)(0) (1)^{n-2} = 0
\end{aligned}
\end{equation*}

Since the first two integrals are the integrals for the expectation of a \(N(0,1)\) distribution. Therefore.

\begin{equation*}
\begin{aligned}
&\int_{\mathbb{R}^n} (\vec{X} - \vec{\mu})(\vec{X} - \vec{\mu})^T
\frac{1}{\sqrt{(2\pi)^n |\Sigma|}}\exp\left\{
    -\frac{(\vec{x} - \vec{\mu})^T \Sigma^{-1}(\vec{x} - \vec{\mu})}{2}
\right\}
\ d\vec{x}
\\&=
D\left[\int_{\mathbb{R}^n} \vec{y}\vec{y}^T
\frac{1}{\sqrt{(2\pi)^n}}\exp\left\{
    -\frac{\sum_{i=k}^n y_k^2}{2}
\right\}
\ d\vec{y}\right] \frac{|D|}{\sqrt{|\Sigma|}}D^T
\\&=
DI \frac{|D|}{\sqrt{|\Sigma|}}D^T
\\&= D \frac{|D|}{\sqrt{|D|\,|D^T|}}D^T
\\&= D \frac{|D|}{\sqrt{|D|^2}}D^T
\\&= DD^T
\\&= \Sigma
\end{aligned}
\end{equation*}
\end{example}

\section{Transformations}
\label{develop--math3278:multivariate-normal:page--index.adoc---transformations}

Let \(\vec{X} \sim MVN(\vec{\mu},\Sigma)\) of \(n\) variables and \(A\in \mathbb{R}^{m\times n}\)
and \(\vec{b} \in \mathbb{R}^m\). Then, \(\vec{Y} = A\vec{X} + \vec{b}\). Then

\begin{equation*}
\vec{Y} \sim MVN(A\vec{\mu} + \vec{b}, A\Sigma A^T)
\end{equation*}

This is not hard to see since clearly, \(\vec{Y}\) is multivariate normal in the sense of the third definition and
hence is completely described by its mean vector and covariance matrix.

\begin{equation*}
E(\vec{Y})
= E(A\vec{X} + \vec{b})
= AE(\vec{X}) + \vec{b}
= A\vec{\mu} + \vec{b}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
Cov(\vec{Y})
&= E[(\vec{Y}-  A\vec{\mu} - \vec{b})(\vec{Y} - A\vec{\mu} - \vec{b})^T]
\\&= E[(A\vec{X} + \vec{b}-  A\vec{\mu} - \vec{b})(A\vec{X} + \vec{b} - A\vec{\mu} - \vec{b})^T]
\\&= E[(A\vec{X}  - A\vec{\mu} )(A\vec{X} - A\vec{\mu})^T]
\\&= E[A(\vec{X} - \vec{\mu} )(\vec{X} - \vec{\mu})^TA^T]
\\&= A E[(\vec{X} - \vec{\mu} )(\vec{X} - \vec{\mu})^T] A^T
\\&= A\Sigma A^T
\end{aligned}
\end{equation*}

\section{Moment generating function}
\label{develop--math3278:multivariate-normal:page--index.adoc---moment-generating-function}

Let \(\vec{X} \sim MVN(\vec{\mu},\Sigma)\) of \(n\) variables. Then, the MGF of \(\vec{X}\) is

\begin{equation*}
\begin{aligned}
M_{\vec{X}}(\vec{t})
&= E[e^{\vec{t}^T\vec{X}}]
\\&= M_{\vec{t}^T\vec{X}}(1)
\\&= \exp\left\{E(\vec{t}^T\vec{X}) + \frac12 Var(\vec{t}^T\vec{X})\right\}
\\&= \exp\left\{ \vec{t}^T\vec{\mu} + \frac12 \vec{t}^T \Sigma \vec{t}\right\}
\end{aligned}
\end{equation*}

\section{Bivariate Normal Distribution}
\label{develop--math3278:multivariate-normal:page--index.adoc---bivariate-normal-distribution}

Let \((X,Y)\) have a multivariate normal distribution. Then, we need only to specify the
mean vector and covariance matrix. Let

\begin{equation*}
\begin{array}{cc}
E(X) = \mu_x & E(Y) = \mu_y\\
Var(X) = \sigma_x^2 & Var(Y) = \sigma_y^2
\end{array}
\quad Cov(X,Y) = \rho\sigma_x\sigma_y
\end{equation*}

where \(\rho \in (-1,1)\). Then the pdf of \((X,Y)\) is given by

\begin{equation*}
f(x,y) = \frac{1}{2\pi\sigma_x\sigma_y\sqrt{1-\rho^2}}
    \exp\left\{-\frac{
        \left(\frac{x-\mu_x}{\sigma_x}\right)^2
        - 2\rho\left(\frac{x-\mu_x}{\sigma_x}\right)\left(\frac{y-\mu_y}{\sigma_y}\right)
        + \left(\frac{y-\mu_y}{\sigma_y}\right)^2
        }{2(1-\rho^2)}
    \right\}
\end{equation*}

and

\begin{equation*}
\begin{pmatrix}X\\ Y\end{pmatrix}
=
\begin{pmatrix}
\sigma_x & 0 \\
\rho\sigma_y & \sigma_y\sqrt{1-\rho^2}
\end{pmatrix}
\begin{pmatrix}Z_1\\ Z_2\end{pmatrix}
+
\begin{pmatrix}\mu_x \\ \mu_y\end{pmatrix}
\end{equation*}

where \(Z_1,Z_2 \sim N(0,1)\) and \(Z_1\perp Z_2\).

\begin{admonition-note}[{}]
To prove that \((X,Y)\) can be written
like this we need only verify that the matrix times its transpose is the covariance
matrix. Also this matrix is clearly not unique as
\(\begin{pmatrix}
\sigma_x\sqrt{1-\rho^2} & \rho\sigma_x \\
0 & \sigma_y
\end{pmatrix}\) also works.
\end{admonition-note}

\begin{example}[{Proof}]
Since the
covariance is an inner product, by the cauchy schwatz inequality

\begin{equation*}
Cov(X,Y)^2 \leq Cov(X,X) Cov(Y,Y) = \sigma_x^2\sigma_y^2
\end{equation*}

where equality holds iff \(aX+bY =0\) for some \(a\) and \(b\).
This equality case is degenerate, so we wont consider it.
Therefore \(Cov(X,Y) = \rho \sigma_x\sigma_y\) for some
\(\rho \in (-1,1)\) and hence

\begin{equation*}
\vec{\mu} = \begin{pmatrix}\mu_x \\ \mu_y\end{pmatrix}
\quad\text{and}\quad
\Sigma = \begin{pmatrix}
\sigma_x^2 & \rho\sigma_x\sigma_y\\
\rho\sigma_x\sigma_y & \sigma_y^2
\end{pmatrix}
\end{equation*}

Then, \(|\Sigma| = (1-\rho^2)\sigma_x^2\sigma_y^2\) and

\begin{equation*}
\Sigma^{-1}
= \frac{1}{(1-\rho^2)\sigma_x^2\sigma_y^2}
\begin{pmatrix}
\sigma_y^2 & -\rho\sigma_x\sigma_y\\
-\rho\sigma_x\sigma_y & \sigma_x^2
\end{pmatrix}
= \frac{1}{(1-\rho^2)\sigma_x^2\sigma_y^2}
\begin{pmatrix}
\sigma_y^2 & -\rho\sigma_x\sigma_y\\
-\rho\sigma_x\sigma_y & \sigma_x^2
\end{pmatrix}
\end{equation*}

and hence the pdf of \((X,Y)\) is given by

\begin{equation*}
\begin{aligned}
f(x,y)
&= \frac{1}{2\pi\sqrt{(1-\rho^2)\sigma_x^2\sigma_y^2 }}
    \exp\left\{-\frac{
        \begin{pmatrix}x - \mu_x & y-\mu_y\end{pmatrix}
        \begin{pmatrix}
            \sigma_y^2 & -\rho\sigma_x\sigma_y\\
            -\rho\sigma_x\sigma_y & \sigma_x^2
        \end{pmatrix}
        \begin{pmatrix}x - \mu_x \\ y-\mu_y\end{pmatrix}
        }{2(1-\rho^2)\sigma_x^2\sigma_y^2}
    \right\}
\\&= \frac{1}{2\pi\sigma_x\sigma_y\sqrt{1-\rho^2}}
    \exp\left\{-\frac{
        (x-\mu_x)^2\sigma_y^2 - 2(x-\mu_x)(y-\mu_y)\rho\sigma_x\sigma_y + (y-\mu_y)^2\sigma_X^2
        }{2(1-\rho^2)\sigma_x^2\sigma_y^2}
    \right\}
\\&= \frac{1}{2\pi\sigma_x\sigma_y\sqrt{1-\rho^2}}
    \exp\left\{-\frac{
        \left(\frac{x-\mu_x}{\sigma_x}\right)^2
        - \left(\frac{x-\mu_x}{\sigma_x}\right)\left(\frac{y-\mu_y}{\sigma_y}\right)
        + \left(\frac{y-\mu_y}{\sigma_y}\right)^2
        }{2(1-\rho^2)}
    \right\}
\end{aligned}
\end{equation*}
\end{example}
\end{document}
