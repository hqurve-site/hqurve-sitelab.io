\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Discrimination}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
Discrimination analysis allows us to explore
and investigate observed differences and relationships
that are not well understood.
We aim to graphically or algebraically describe the different
features of objects from several \textbf{known} populations.
This is done by assigning \textbf{discriminants} (numerical values) to the objects
in such a way that objects from different populations are assigned very different
values.

There is a slight difference between discrimination and classification

\begin{description}
\item[Discrimination] Given existing data already assigned to specified groups,
    we wish to determine a rule which can be used to reproduce the groups.
\item[Classification] Assigning new observations each to some set of fixed groups
    using a discrimination rule.
\end{description}

Discrimination operates in similarly to Bayesian hypothesis testing.
Suppose that we have populations \(\pi_1,\ldots \pi_g\),
with associated densities \(f_1, \ldots f_g\),
and we want to classify \(\vec{x} \in \chi\) to one of these populations.
To do this, we partition \(\chi\) into regions \(R_1, \ldots R_g\)
where we assign \(\vec{x}\) to population \(i\) iff it lies in region \(R_i\).
The problem is now determining the regions \(R_1, \ldots R_g\).
To do this, we consider the following sets of quantities

\begin{description}
\item[Prior probabilities] \(p_1, \ldots p_g\). The value \(p_i\) is the probability
    that an object belongs to population \(\pi_i\).
    Also, \(\sum_{i=g}^k p_i = 1\).
\item[Density functions] \(f_1, \ldots f_g\).
    The value \(f_i(\vec{x})\) is the density of observing
    \(\vec{x}\) given that it is from \(\pi_i\).
\item[Conditional assignment probabilities] \(P(j|i) = P(\vec{X} \in R_j| \pi_i)\).
    The probability that we assign \(\vec{X}\) to \(\pi_j\)
    given that \(\vec{x}\) is from \(\pi_i\).
\item[Cost functions] \(c(j|i)\). The cost of (mis)classifying an object from \(\vec{x}_i\) as \(\vec{x}_j\).
\end{description}

\section{Determining regions}
\label{develop--stats:ROOT:page--analysis/discrimination.adoc---determining-regions}

We seek to minimize the \textbf{expected cost of misclassification} (ECM)
which is computed as

\begin{equation*}
\begin{aligned}
ECM
&= \sum_{i=1}^g E[\text{cost of misclassifying}\ |\ \pi_i]p_i
\\&= \sum_{i=1}^g \sum_{j=1}^g c(j|i)P(j|i)p_i
\end{aligned}
\end{equation*}

This can only be achieved by adjusting the regions \(R_1, \ldots R_k\) since
everything else is considered constant.

\begin{proposition}[{}]
For a given observation \(\vec{x}\),
we assign \(\vec{x}\) to
population which minimizes the expected cost of misclassification.
That is, we assign \(\vec{x}\) to population \(\pi_k\)
if the following is minimized

\begin{equation*}
E[\text{cost of misclassified }\vec{x}\text{ as k}]
= \sum_{i=1}^g c(k|i)P(\vec{x} \text{ is from} \pi_i)
= \sum_{i=1}^g c(k|i)f_i(\vec{x})p_i
\end{equation*}

This assignment rule minimizes the ECM.
\end{proposition}

In the absence of cost functions,
we can also seek to minimize the total probability
of misclassification.

\begin{equation*}
\begin{aligned}
TPM
&= \sum_{i=1}^g P[\text{misclassifying}\ |\ \pi_i]p_i
\\&= \sum_{i=1}^g \sum_{j=1, j\neq i}^g P(j|i)p_i
\end{aligned}
\end{equation*}

Note that this is equivalent to having cost functions
all equal to one.

\section{Discrimination between two populations}
\label{develop--stats:ROOT:page--analysis/discrimination.adoc---discrimination-between-two-populations}

Suppose we have two populations \(\pi_1\) and \(\pi_2\).
Then, the cost can be represented as

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {Classify as \(\pi_1\)} & {Classify as \(\pi_2\)} \\
\hline
{From \(\pi_1\)} & {0} & {\(c(2|1)\)} \\
\hline
{From \(\pi_2\)} & {\(c(1|2)\)} & {\(0\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Se the ECM is

\begin{equation*}
ECM = c(2|1)P(2|1)p_1 + c(1|2)P(1|2)p_2
\end{equation*}

and the assignment regions are

\begin{equation*}
\begin{aligned}
R_1:\quad
\frac{f_1(\vec{x})}{f_2(\vec{x})}
&\geq
\left(\frac{c(1|2)}{c(2|1)}\right)
\left(\frac{p_2}{p_1}\right)
\\
R_2:\quad
\frac{f_1(\vec{x})}{f_2(\vec{x})}
&<
\left(\frac{c(1|2)}{c(2|1)}\right)
\left(\frac{p_2}{p_1}\right)
\end{aligned}
\end{equation*}

where the terms are referred to as

\begin{itemize}
\item density ratio \(\frac{f_1(\vec{x})}{f_2(\vec{x})}\)
\item cost ratio \(\frac{c(1|2)}{c(2|1)}\)
\item prior probability ratio \(\frac{p_2}{p_1}\)
\end{itemize}

Also note that \(R_1\) is the region
where the expected cost of misclassifying
as \(\pi_1\) is less than the expected
cost of misclassifying as \(\pi_2\).

\begin{example}[{Example 11.2 from the book}]
Suppose that we have two populations
\(\pi_1\) and \(\pi_2\) with density functions
\(f_1(\vec{x})\) and \(f_2(\vec{x})\).
Suppose that \(c(2|1)=5\)
and \(c(1|2)=10\)
and we know that \(20\%\) of all
objects belong to \(\pi_2\).
So \(p_1 = 0.8\) and \(p_2=0.2\).

So, the classification regions are

\begin{equation*}
\begin{aligned}
R_1:
\frac{f_1(\vec{x})}{f_2(\vec{x})}
&\geq
\left(\frac{10}{5}\right)
\left(\frac{0.2}{0.8}\right)
= 0.5
\\
R_2:
\frac{f_1(\vec{x})}{f_2(\vec{x})}
&<
\left(\frac{10}{5}\right)
\left(\frac{0.2}{0.8}\right)
= 0.5
\end{aligned}
\end{equation*}

So, if we have an observation \(\vec{x}_0\)
with \(f_1(\vec{x}_0) = 0.3\)
and \(f_2(\vec{x}_0 = 0.4\).
So the density ratio is \(0.75\)
and \(\vec{x}_0 \in R_1\).
Hence we assign \(\vec{x}_0\) as belonging \(\pi_1\).

Note that we could have alternatively computed the cost of misclassification

\begin{itemize}
\item The cost of misclassifying as \(\pi_1\) is \(c(1|2)f_2(\vec{x})p_2 = 10(0.4)(0.2) = 0.8\)
\item The cost of misclassifying as \(\pi_2\) is \(c(2|1)f_1(\vec{x})p_1 = 5(0.3)(0.8) = 1.2\)
\end{itemize}

and hence we classify \(\vec{x}_0\) as belonging to \(\pi_1\) since it has a lower cost
of misclassification.
\end{example}

\subsection{Normally distributed with equal covariance}
\label{develop--stats:ROOT:page--analysis/discrimination.adoc---normally-distributed-with-equal-covariance}

Suppose that we have two \textbf{normally distributed} datasets
with respective means \(\vec{\mu}_1\) and \(\vec{\mu}_2\)
and \textbf{equal covariance} matrix \(\mat{\Sigma}\).
Then, the assignment rule which minimizes the ECM
assigns a point \(\vec{x}_0\) to \(\pi_1\)
if

\begin{equation*}
(\vec{\mu}_1-\vec{\mu}_2)^T\mat{\Sigma}^{-1}\vec{x}_0
-\frac12 (\vec{\mu}_1-\vec{\mu}_2)^T\mat{\Sigma}^{-1}(\vec{\mu}_1+\vec{\mu}_2)
\geq
\ln\left[\left(\frac{c(1|2)}{c(2|1)}\right)\left(\frac{p_2}{p_1}\right)\right]
\end{equation*}

Typically, we will estimate \(\vec{\mu}_i\) with \(\overbar{\vec{x}}_i\)
and estimate \(\mat{\Sigma}\) with the pooled sample variance.

Furthermore, if we have \textbf{equal costs} (\(c(1|2)=c(2|1)\)) and \textbf{equal priors} (\(p_1=p_2\)),
the rule assigns \(\vec{x}_0\) to \(\pi_1\)
if

\begin{equation*}
\begin{aligned}
\hat{y_0}
&\geq \hat{m}
\\
(\overbar{\vec{x}}_1 - \overbar{\vec{x}}_2)^T \mat{S}^{-1}_{pooled}\vec{x}_0
&\geq
(\overbar{\vec{x}}_1 - \overbar{\vec{x}}_2)^T \mat{S}^{-1}_{pooled}
\left(\frac{\overbar{\vec{x}}_1 + \overbar{\vec{x}}_2}{2}\right)
\end{aligned}
\end{equation*}

The above is also called \textbf{Fisher's Classification Criteria}.

\section{Implementation in R}
\label{develop--stats:ROOT:page--analysis/discrimination.adoc---implementation-in-r}

To perform discrimination in R,
we can use the \texttt{lda} function from the \texttt{MASS} package
to perform \textbf{linear discrimination analysis} (ie similar to fisher's discrimination rule).

\begin{listing}[{}]
# define the data with a column of discriminants `group`
data = ..

library(MASS)
# use CV=FALSE to get a discriminator
# use CV=TRUE to perform cross validation and obtain the results.
fit <- lda(group ~ x1+...+ xp, data=data, CV=FALSE)

# perform classification on new data
predict(fit, new_data)
\end{listing}
\end{document}
