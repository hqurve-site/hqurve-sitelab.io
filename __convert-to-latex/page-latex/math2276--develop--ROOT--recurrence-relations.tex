\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Recurrence Relations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A \emph{recurrence relation} defines a sequence, \(a_n\), in terms
of its index, \(n\), and its preceding terms. Additionally, we
say that a recurrence relation is of order \(k\) if the
relation only depends on \(k\) preceding terms. As stated
previously there is no one way to solve recurrence relations. In this
section, several methods will be discussed.

\section{Guess and check}
\label{develop--math2276:ROOT:page--recurrence-relations.adoc---guess-and-check}

Perhaps the simplest method of solving a recurrence relations is to
guess the general formula for the relation then prove its validity using
mathematical induction. Typically, in order to determine a formula,
several small cases are examined and an educated guess is made.
Additionally, when sufficient boundary conditions are supplied, there is
only one solution and hence this method works. However, if there is not
enough information to determine the solution exactly, it is possible to
miss solutions.

For example, consider \(\{a_n\}\) where
\(a_n = 2a_{n-1} + 1\) and \(a_1 = 1\). Here are the
first few terms

\begin{equation*}
\begin{aligned}
    a_1 &= 1\\
    a_2 &= 2a_1 + 1 = 3\\
    a_3 &= 2a_2 + 1 = 7\\
    a_4 &= 2a_3 + 1 = 15\end{aligned}
\end{equation*}

From this, a reasonable guess would be \(a_n = 2^n -1\). This
clearly satisfies the base case. Additionally, it does satisfy the
recurrence relation as shown below

\begin{equation*}
2^n - 1 = a_n
    = 2a_{n-1} + 1
    = 2(2^{n-1} - 1) + 1
\end{equation*}

Hence the guess was correct.

\section{Iteration}
\label{develop--math2276:ROOT:page--recurrence-relations.adoc---iteration}

When the recurrence relation is simple, it is possible to back propagate
the relation until its base case. Upon doing so, the formula would begin
to appear.

For example, take the previous \(\{a_n\}\)

\begin{equation*}
\begin{aligned}
    a_n
    =& 2a_{n-1} + 1\\
    =& 1 + 2a_{n-1}\\
    =& 1 + 2(2a_{n-2} + 1)\\
    =& 1 + 2 + 4a_{n-2}\\
    =& 1 + 2 + 4(2a_{n-3} + 1)\\
    =& 1 + 2 + 4 + 8a_{n-3}\\
    &\vdots\\
    =& 1 + 2 + 4 + \ldots 2^{k-1} + 2^k a_{n-k}\\
    =& 1 + 2 + 4 + \ldots 2^{n-1-1} + 2^{n-1} a_{1}\\
    =& 1 + 2 + 4 + \ldots 2^{n-1-1} + 2^{n-1}\\
    =& 2^{n} - 1\end{aligned}
\end{equation*}

As seen, we do indeed get the correct result.

\section{Generating functions}
\label{develop--math2276:ROOT:page--recurrence-relations.adoc---generating-functions}

Although not the simplest, generating functions provide a direct method
for computing recurrence relations. Suppose we had the sequence
\(\{a_n\}\), then consider the following function

\begin{equation*}
f(x) = \sum_{i=1}^\infty a_ix^i
\end{equation*}

which is a formal power series. Therefore, upon solving for
\(f\), the formula for \(a_n\) is simply reading off
the coefficient of \(x^n\) in \(f\).

For example, consider \(\{a_n\}\) such that
\(a_n = 2a_{n-1} +1\) with \(a_1 = 1\). Then,

\begin{equation*}
\begin{aligned}
    f(x)
    &= \sum_{i=1}^\infty a_i x^i\\
    &= a_1 x + \sum_{i=2}^\infty a_i x^i\\
    &= x + \sum_{i=2}^\infty (2a_{i-1} +1) x^i\\
    &= x + 2x\sum_{i=1}^\infty a_{i} x^i + \sum_{i=2}^\infty x^i\\
    &= x + 2xf(x) + \sum_{i=2}^\infty x^i\\
    &= 2xf(x) + \sum_{i=1}^\infty x^i\\
    &= 2xf(x) + \frac{x}{1-x}\end{aligned}
\end{equation*}

Therefore,

\begin{equation*}
\begin{aligned}
    f(x)
    &= \frac{1}{1-2x}\frac{x}{1-x}\\
    &= \frac{x}{(1-2x)(1-x)}\\
    &= \frac{1}{1-2x} - \frac{1}{1-x}\\
    &= \sum_{i=0}^\infty (2x)^i - \sum_{i=0}^\infty x^i\end{aligned}
\end{equation*}

Therefore the coefficient of \(x^n\) is \(2^n - 1\)
as expected.

\section{Auxiliary equation method}
\label{develop--math2276:ROOT:page--recurrence-relations.adoc---auxiliary-equation-method}

If the recurrence relation is linear and homogenous, that is, of the
following form

\begin{equation*}
A_0a_n + A_1a_{n-1} + \ldots A_ka_{n-k} = 0
\end{equation*}

Then there is a simple way to find the general formula. Firstly,
consider the \(m\) distinct roots of the following polynomial,
called the \emph{characteristic equation},

\begin{equation*}
A_0 z^k + A_1 z^{k-1} + \ldots A_k = 0
\end{equation*}

where the distinct roots are
\(\alpha_1, \alpha_2, \ldots \alpha_m\). Where each of the
roots, \(a_i\) has multiplicity \(p_i\). Then the
general formula is as follows

\begin{equation*}
a_n = \sum_{i=1}^m \; \sum_{j=0}^{p_i -1} B_{i, j}\, n^j\, \alpha_i ^ n
\end{equation*}

where \(B_{i, j}\) are constants.

\begin{admonition-note}[{}]
\begin{itemize}
\item the boundary conditions must be used to evaluate each of the
    \(B_{i, j}\).
\item the solutions to a linear homogenous recurrence relation form a vector
    space (usually with a complex field).
\end{itemize}
\end{admonition-note}

\subsection{Non-homogenous recurrence relations}
\label{develop--math2276:ROOT:page--recurrence-relations.adoc---non-homogenous-recurrence-relations}

That is of the following form

\begin{equation*}
A_0a_n + A_1a_{n-1} + \ldots A_ka_{n-k} = p(n)
\end{equation*}

Then, firstly solve for \(a_n\) as if it the recurrence
relation was homogenous to find the general solution. After, make
educated guesses for particular solution. I don’t know how to help here.
\end{document}
