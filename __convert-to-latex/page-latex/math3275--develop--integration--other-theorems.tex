\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Other Theorems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Ln}{Ln}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\Res}{Res}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\section{Taylor's Theorem}
\label{develop--math3275:integration:page--other-theorems.adoc---taylors-theorem}

Let \(C\) be a circle of radius \(R\) centered at \(z=a\). Then
if \(f(z)\) is analytic on \(C \cup I(C)\), then

\begin{equation*}
f(z) = \sum_{n=0}^\infty \frac{f^{(n)}(a)}{n!}(z-a)^n
\end{equation*}

for all \(z \in I(C)\).

\begin{example}[{Proof}]
Let \(n > 0\) and
consider the difference

\begin{equation*}
\begin{aligned}
&f(z) - \sum_{k=0}^n \frac{f^{(k)}}{k!}(z-a)^k
\\&= \frac{1}{2\pi i}\oint_{C} \frac{f(w)}{w-z}\ dw - \frac{1}{2\pi i} \sum_{k=0}^n \frac{(z-a)^k}{k!}\frac{k!}{1}\oint_{C} \frac{f(w)}{(w-a)^{k+1}} \ dw
\\&= \frac{1}{2\pi i}\oint_{C} f(w) \left[\frac{1}{w-z} - \sum_{k=0}^n \frac{(z-a)^k}{(w-a)^{k+1}}\right] \ dw
\\&= \frac{1}{2\pi i}\oint_{C} f(w) \left[\frac{1}{w-z} - \frac{1}{w-a}\frac{\left(\frac{z-a}{w-a}\right)^{n+1} - 1}{\left(\frac{z-a}{w-a}\right) - 1}\right] \ dw
\\&= \frac{1}{2\pi i}\oint_{C} f(w) \left[\frac{1}{w-z} - \frac{\left(\frac{z-a}{w-a}\right)^{n+1} - 1}{z-a - (w-a)}\right] \ dw
\\&= \frac{1}{2\pi i}\oint_{C} f(w) \frac{1}{w-z}\left(\frac{z-a}{w-a}\right)^{n+1} \ dw
\end{aligned}
\end{equation*}

Then, by applying the modulus,

\begin{equation*}
\begin{aligned}
&\left|f(z) - \sum_{k=0}^n \frac{f^{(k)}}{k!}(z-a)^k\right|
\\&\leq \frac{1}{2\pi} \oint_{C} |f(w)| \frac{1}{|w-z|} \left|\frac{z-a}{w-a}\right|^{n+1} \ |dw|
\\&\leq \frac{1}{2\pi} \oint_{C} |f(w)| \frac{1}{|w-a| - |z-a|} \left|\frac{z-a}{w-a}\right|^{n+1} \ |dw|
\\&= \frac{1}{2\pi} \oint_{C} |f(w)| \frac{1}{r - |z-a|} \frac{|z-a|^{n+1}}{r^{n+1}} \ |dw|
\\&\leq \frac{1}{2\pi} M \frac{1}{r-|z-a|}\frac{|z-a|^{n+1}}{r^{n+1}} 2\pi r
\\&= M \frac{1}{r-|z-a|}\frac{|z-a|^{n+1}}{r^n}
\\&= M \frac{1}{r-|z-a|}\frac{|z-a|^{n+1}}{r^n}
\end{aligned}
\end{equation*}

where \(M = \sup_{w \in C} |f(w)|\). Then this tends to \(0\) as \(n\to \infty\) since \(|z-a| < r\).
\end{example}

\section{Laurent's Theorem}
\label{develop--math3275:integration:page--other-theorems.adoc---laurents-theorem}

Let

\begin{equation*}
A = \{z: r_1 \leq |z - a| \leq r_2\}
\end{equation*}

Then if \(f(z)\) is analytic on \(A\), then

\begin{equation*}
f(z) = \sum_{n=0}^\infty a_n (z-a)^n + \sum_{n=1}^\infty \frac{b_n}{(z-a)^n}
\end{equation*}

for all \(z \in I(A)\) where

\begin{equation*}
a_n = \frac{1}{2\pi i}\oint_{C_2} \frac{f(w)}{w^{n+1}} \ dw
\quad\text{and}\quad
b_n = \frac{1}{2\pi i}\oint_{C_1} \frac{f(w)}{w^{-n+1}} \ dw
\end{equation*}

and \(C_1\) is the circle \(|z -a| = r_1\) and \(C_2\)
is the circle \(|z-a| = r_2\).

\begin{figure}[H]\centering
\includegraphics[width=0.5\linewidth]{images/develop-math3275/integration/laurent-theorem}
\end{figure}

\begin{admonition-tip}[{}]
We usually call the series \(\sum_{n=0}^\infty a_n (z-a)^n\) the \emph{principal part}
of the Laurent series.
\end{admonition-tip}

\begin{example}[{Proof}]
Firstly, since \(f(z)\) is analytic in \(I(A)\) by
\myautoref[{Cauchy's multiple integral formula}]{develop--math3275:integration:page--cauchy.adoc---cauchys-multiple-integral-formula},

\begin{equation*}
f(z) =
\frac{1}{2\pi i}\oint_{C_2} \frac{f(w)}{w-z} \ dw
- \frac{1}{2\pi i}\oint_{C_1} \frac{f(w)}{w-z} \ dw
\end{equation*}

and lets focus on each of the integrals separately. Then

\begin{equation*}
\begin{aligned}
\frac{1}{2\pi i}\oint_{C_2} \frac{f(w)}{w-z} \ dw
&\frac{1}{2\pi i}\oint_{C_2} \frac{f(w)}{w-a-(z-a)} \ dw
\\&= \frac{1}{2\pi i}\oint_{C_2} \frac{1}{w-a}\frac{f(w)}{1-\frac{z-a}{w-a}} \ dw
\\&= \frac{1}{2\pi i}\oint_{C_2} \frac{1}{w-a}f(w) \sum_{n=0}^\infty\left(\frac{z-a}{w-a}\right)^n \ dw
\\&= \sum_{n=0}^\infty (z-a)^n\frac{1}{2\pi i}\oint_{C_2} \frac{f(w)}{(w-a)^{n+1}} \ dw
\end{aligned}
\end{equation*}

since \(|frac{z-a}{w-a}| < 1\) since \(w \in C_2\) and the series is uniformly convergent, hence we can interchange integration and summation.

For the other integral,

\begin{equation*}
\begin{aligned}
\frac{1}{2\pi i}\oint_{C_1} \frac{f(w)}{w-z} \ dw
&\frac{1}{2\pi i}\oint_{C_1} \frac{f(w)}{w-a-(z-a)} \ dw
\\&= \frac{1}{2\pi i}\oint_{C_1} \frac{1}{z-a}\frac{f(w)}{\frac{w-a}{z-a} - 1} \ dw
\\&= -\frac{1}{2\pi i}\oint_{C_1} \frac{1}{z-a}f(w) \sum_{n=0}^\infty\left(\frac{w-a}{z-a}\right)^n \ dw
\\&= \sum_{n=0}^\infty (z-a)^{-n-1}\frac{1}{2\pi i}\oint_{C_1} \frac{f(w)}{(w-a)^{-n}} \ dw
\\&= \sum_{n=1}^\infty (z-a)^{-n}\frac{1}{2\pi i}\oint_{C_1} \frac{f(w)}{(w-a)^{-n+1}} \ dw
\end{aligned}
\end{equation*}

since \(|\frac{w-a}{z-a}| < 1\) since \(w \in C_1\) and similar reasoning.
\end{example}

\section{Argument Principle}
\label{develop--math3275:integration:page--other-theorems.adoc---argument-principle}

Suppose that \(f(z)\) is meromorphic on \(C \cup I(C)\) such that

\begin{itemize}
\item \(f(z)\) has \(P\) poles
\item \(f(z)\) has \(N\) zeros, none of which lie on \(C\)
\end{itemize}

both including multiplicity (ie order).
Then,

\begin{equation*}
N - P
= \frac{1}{2\pi i } \oint_C \frac{f'(z)}{f(z)} \ dz
= \frac{1}{2\pi} \Delta_C \arg f(z)
\end{equation*}

Where \(\Delta_C u\) is the \emph{variation} of function \(u\)
along \(C\). Essentially the contour integral of the derivative of \(u\).

\begin{example}[{Proof}]
\begin{admonition-important}[{}]
Proof taken from \href{https://en.wikipedia.org/wiki/Argument\_principle}{the wiki page}.
\end{admonition-important}

\begin{admonition-note}[{}]
Only the first equality would be proven. See \myautoref[{here for second equality}]{develop--math3275:integration:page--other-theorems.adoc---understanding-the-integral}.
\end{admonition-note}

Consider the integral

\begin{equation*}
\frac{1}{2\pi i}\oint_C \frac{f'(z)}{f(z)} \ dz
\end{equation*}

Then, since we have zeros an poles, we would use the
\myautoref[{cauchy multiple region theorem}]{develop--math3275:integration:page--cauchy.adoc---multiply-connected-regions},
to calculate the integral by selecting curves \(C_1, \ldots C_n\)
such that each curve encloses exactly one pole or zero of \(f(z)\).
We have two cases

If \(C_i\) encloses a pole of order \(k\) at \(z_0\), then
by definition of pole, we
write \(f(z) = (z-z_0)^{-k}g(z)\) where \(g(z)\) is analytic
on \(C_i \cup I(C_i)\). Then

\begin{equation*}
f'(z) = -k(z-z_0)^{-k-1}g(z) + (z-z_0)^{-k}g'(z)
\end{equation*}

Furthermore, notice that \(g(z)\) has no zeros nor
poles on \(C_i \cup I(C_i)\).
Then

\begin{equation*}
\frac{1}{2\pi i}\oint_{C_i} \frac{f'(z)}{f(z)} \ dz
=\frac{1}{2\pi i}\oint_{C_i} \frac{-k(z-z_0)^{-k-1} g(z) + (z-z_0)^{-k}g'(z)}{(z-z_0)^{-k}g(z)} \ dz
=\frac{1}{2\pi i}\oint_{C_i} \frac{-k}{z-z_0} + \frac{g'(z)}{g(z)} \ dz
= -k
\end{equation*}

by Cauchy's integral formula since \(\frac{g'(z)}{g(z)}\) is analytic on \(C_i \cup I(C_i)\).

On the other hand, if \(C_i\) encloses a hole of order \(k\) at \(z_0\),
then by definition of a hole, \(f(z) = (z-z_0)^kg(z)\) where \(g(z)\)
is analytic on \(C_i \cup I(C_i)\). Then

\begin{equation*}
f'(z) = k(z - z_0)^k g(z) + (z-z_0)^k g'(z)
\end{equation*}

Furthermore, notice that \(g(z)\) has no zeros nor poles on \(C_i \cup I(C_i)\),
hence \(\frac{g'(z)}{g(z)}\) is analytic on that region. Then by
Cauchy's integral formula.

\begin{equation*}
\frac{1}{2\pi i}\oint_{C_i} \frac{f'(z)}{f(z)} \ dz
=\frac{1}{2\pi i}\oint_{C_i} \frac{k(z-z_0)^{k-1} g(z) + (z-z_0)^{k}g'(z)}{(z-z_0)^{k}g(z)} \ dz
=\frac{1}{2\pi i}\oint_{C_i} \frac{k}{z-z_0} + \frac{g'(z)}{g(z)} \ dz
= k
\end{equation*}

Therefore, zeros contribute their positive order while poles contribute their negative
order, hence the formula follows.

\begin{admonition-remark}[{}]
Note that the hole and pole section were virtually identical. Are zeros
and poles the same construct? Their definitions are very similar and basically identical
where we write \(f(z) = (z-z_0)^ng(z)\) such that \(g(z)\) has no zeros
nor poles at \(z=z_0\).
\end{admonition-remark}
\end{example}

\subsection{Understanding the integral}
\label{develop--math3275:integration:page--other-theorems.adoc---understanding-the-integral}

\begin{admonition-important}[{}]
Explanation taken from \href{https://en.wikipedia.org/wiki/Argument\_principle}{the wiki page}.
\end{admonition-important}

Firstly, since \(f(z)\) is analytic, the path followed by \(f(z)\) on \(C\)
should also be a closed contour. However, it may not be simple. Then,

\begin{equation*}
\frac{1}{2\pi i } \oint_C \frac{f'(z)}{f(z)} \ dz = \frac{1}{2\pi i} \oint_{f(C)} \frac{1}{w} \ dw
\end{equation*}

where \(w = f(z)\). We may be tempted to immediately apply Cauchy's integral formula,
however that requires that \(f(C)\) is simple, which it may not be. However,
notice that for each double point, we can split the curve in two. Then, applying
the integral formula on each of these curves yields the result by also taking into
account

\begin{itemize}
\item the orientation of the curve
\item Whether the curve actually encloses the origin
\end{itemize}

We then make the following observations

\begin{itemize}
\item For each positive loop around the origin, the integral contributes \( + 1\)
\item For each negative loop around the origin, the integral contributes \(-1\)
\item For each loop which does not enclose the origin, the integral contributes \(0\).
\end{itemize}

Therefore, the integral yields the net number of positive loops of \(f(C)\) around the origin.
Alternatively, this is precisely \(\frac{1}{2\pi}\) times how much the argument of \(f(z)\)
changes as \(z\) follows \(C\). Therefore, the two equations are equal.

\begin{admonition-tip}[{}]
This number is known as the \emph{winding number} of \(f(C)\) about the origin.
\href{https://en.wikipedia.org/wiki/Winding\_number}{[wiki article]}
\end{admonition-tip}

Alternatively, if we were to assume that an ``anti-derivative'' exists, then

\begin{equation*}
\frac{1}{2\pi i } \oint_C \frac{f'(z)}{f(z)} \ dz
= \left.\frac{1}{2\pi i}\ln(f(z))\right|_C
= \left[\frac{1}{2\pi i}  \ln(|f(z)|) + i\arg(f(z)) \right|_C
= \left.\frac{1}{2\pi i} \ln|f(z)| \right|_C + \left.\frac{1}{2\pi}\arg(f(z))\right|_C
\end{equation*}

Then since \(|f(z)|\) has no branch points, its variation over \(C\) amounts to \(0\).
However, \(\arg(f(z))\) has a branch point at the origin, so we much account for it.
Therefore, we see that the integral is again, precisely, the net number of positive loops
of \(f(C)\) around the origin.

\section{Rouche's Theorem}
\label{develop--math3275:integration:page--other-theorems.adoc---rouches-theorem}

Let \(f(z)\) and \(g(z)\) be analytic in \(C \cup I(C)\).
Then if,

\begin{equation*}
\forall z \in C: |g(z)| < |f(z)|
\end{equation*}

\(f(z)\) and \(f(z)+g(z)\) have the same number of zeros inside \(C\) (including order).

\subsection{Informal understanding}
\label{develop--math3275:integration:page--other-theorems.adoc---informal-understanding}

\begin{admonition-important}[{}]
Taken from
\href{https://en.wikipedia.org/wiki/Rouch\%C3\%A9\%27s\_theorem\#Geometric\_explanation}{the wiki page}.
\end{admonition-important}

Consider the curves \(f(C)\) and \((f+g)(C)\). Then, since \(|g(z)| < |f(z)|\),
both curves must wind about the origin the same number of times.
Then since both \(f(z)\) and \(f(z) + g(z)\) are analytic,
by the argument principle, the have the same number of zeros.

To see this, let the origin be a tree,
\(f(C)\) is the path taken by a person and \(g(z)\) is the offset
of their dog on a leash. Then \((f + g)(C)\) is the path taken by the dog.
Then since \(|g(z)| < |f(z)|\), the dog must wind around the tree the same number
of times as the person.

\section{Fundamental Theorem of Algebra}
\label{develop--math3275:integration:page--other-theorems.adoc---fundamental-theorem-of-algebra}

Every polynomial with degree at least \(1\) has at least one root.
Hence, by induction, each polynomial of degree \(n\) has exactly \(n\)
roots.
\end{document}
