\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Matrix of Linear Transformation}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Consider \(T \in \mathcal{L}(V, W)\) where \(V\) and \(W\) are over common field \(\mathbb{K}\)
and have finite dimensions \(m\) and \(n\) and
basis \(\mathcal{B}\) and \(\mathcal{B}'\). Then, the matrix of \(T\) with respect to basis
\(\mathcal{B}\) and \(\mathcal{B}'\), denoted by
\({}_{\mathcal{B}'}[T]_\mathcal{B}\) is the linear transformation in \(\mathcal{L}(\mathbb{K}^m, \mathbb{K}^n)\)
such that the following diagram commutes

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math3273/linear-transformations/matrix}
\end{figure}

That is,

\begin{equation*}
({}_{\mathcal{B}'}[T]_\mathcal{B}) ([\ \cdot\ ]_{\mathcal{B}}) = ([\ \cdot\ ]_{\mathcal{B'}})(T)
\end{equation*}

Where \([\ \cdot\ ]_{\mathcal{B}}\) and \([\ \cdot\ ]_{\mathcal{B}'}\) are the transformations which
take vectors from \(V\) and \(W\) and convert them into coordinate vectors with respect to the
respective basis.

By having a matrix of transformation, computation of the transformation \(T\) gets reduced to matrix
multiplication since the diagram commutes.

\section{Theorem: The columns of \({}_{\mathcal{B}'}[T]_\mathcal{B}\) are given by \([T(\vec{b}_j)]_{\mathcal{B}'}\)}
\label{develop--math3273:linear-transformations:page--matrix.adoc--columns-of-matrix}

The \(j\)'th column of \({}_{\mathcal{B}'}[T]_\mathcal{B}\) is given by \([T(\vec{b}_j)]_{\mathcal{B}'}\)
where \(\vec{b}_j\) is the \(j\)'th basis vector of \(\mathcal{B}\). That is

\begin{equation*}
{}_{\mathcal{B}'}[T]_\mathcal{B}
= \begin{bmatrix}[
    T(\vec{b}_1)]_{\mathcal{B}'}
    & [T(\vec{b}_2)]_{\mathcal{B}'}
    &\cdots
    & [T(\vec{b}_m)]_{\mathcal{B}'}
\end{bmatrix}
\end{equation*}

This theorem not only shows that the matrix of transformation exists, but also that it is unique.

\begin{admonition-note}[{}]
This could either be a theorem or a definition, during class it was introduced as a defintion, but it doesnt
really matter.
\end{admonition-note}

\begin{example}[{Proof}]
Consider the vector \(\mathcal{b}_j\), then \([\vec{b}_j]_\mathcal{B}\) is the column vector with
a \(1\) in the \(j\)'th position and \(0\)s elsewhere. Then
\({}_{\mathcal{B}'}[T]_\mathcal{B}([\vec{b}_j]_\mathcal{B})\)
is the \(j\)'th column of the matrix. Then, since the diagram commutes

\begin{equation*}
{}_{\mathcal{B}'}[T]_\mathcal{B}([\vec{b}_j]_\mathcal{B}) = [\vec{b}_j]_{\mathcal{B}'}
\end{equation*}

and we have shown the uniqueness of the matrix. However, we need to show that such a matrix causes the diagram
to commute (ie existance of such a matrix).

Consider arbitrary \(\vec{v} \in V\), then since \(\mathcal{B}\) is a basis, \(\exists c_1, c_2, \ldots c_m\}\)
such that

\begin{equation*}
\vec{v} = \sum_{j=1}^m c_j\vec{b}_j
\end{equation*}

Then,

\begin{equation*}
T(\vec{v})
= T\left(\sum_{j=1}^m c_j\vec{b}_j\right)
= \sum_{j=1}^m c_jT(\vec{b}_j)
\end{equation*}

and

\begin{equation*}
[T(\vec{v})]_{\mathcal{B}'}
=\left[\sum_{j=1}^m c_jT(\vec{b}_j)\right]_{\mathcal{B}'}
=\sum_{j=1}^m c_j[T(\vec{b}_j)]_{\mathcal{B}'}
\end{equation*}

since \myautoref[{the coordinate vector is linear}]{develop--math3273:ROOT:page--direct-sums.adoc---coordinate-vector}.

On the other hand

\begin{equation*}
[\vec{v}]_{\mathcal{B}} =
\begin{bmatrix}
c_1\\ c_2\\\vdots \\ c_m
\end{bmatrix}
\end{equation*}

and

\begin{equation*}
{}_{\mathcal{B}'}[T]_\mathcal{B}([\vec{v}]_{\mathcal{B}})
= \begin{bmatrix}[
    T(\vec{b}_1)]_{\mathcal{B}'}
    & [T(\vec{b}_2)]_{\mathcal{B}'}
    &\cdots
    & [T(\vec{b}_m)]_{\mathcal{B}'}
\end{bmatrix}
\begin{bmatrix}
c_1\\ c_2\\\vdots \\ c_m
\end{bmatrix}
=
\sum_{j=1}^m c_j[T(\vec{b}_j)]_{\mathcal{B}'}
= [T(\vec{v})]_{\mathcal{B}'}
\end{equation*}

and we are done since the diagram commutes.
\end{example}

\section{Operations on matricies}
\label{develop--math3273:linear-transformations:page--matrix.adoc---operations-on-matricies}

Let \(U,V,W\) be vector spaces with common field
\(\mathbb{K}\) and with bases \(\mathcal{B}_1\)
\(\mathcal{B}_2\) and \(\mathcal{B}_3\) respectively. Then

\begin{equation*}
\begin{aligned}
& \forall T,S \in \mathcal{L}(U, V):
    {}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
    + {}_{\mathcal{B}_2}[S]_{\mathcal{B}_1}
    =
    {}_{\mathcal{B}_2}[T+S]_{\mathcal{B}_1}
\\[5pt]
& \forall T \in \mathcal{L}(U,V):
    \forall c \in \mathbb{K}:
    c\ {}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
    =
    {}_{\mathcal{B}_2}[cT]_{\mathcal{B}_1}
\\[5pt]
& \forall T \in \mathcal{L}(U,V):
    \forall R \in \mathcal{L}(V,W):
    {}_{\mathcal{B}_3}[R]_{\mathcal{B}_2}\
    {}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
    =
    {}_{\mathcal{B}_3}[R\circ T]_{\mathcal{B}_1}
\end{aligned}
\end{equation*}

where the above operations are matrix and scalar muliplication.

\begin{example}[{Proof}]
Addition and scalar multiplication follow directly from the fact that
the
\myautoref[{columns of a matrix are given by the coordinate vectors of the basis}]{develop--math3273:linear-transformations:page--matrix.adoc--columns-of-matrix}
and that
\myautoref[{coordinate vectors are linear}]{develop--math3273:ROOT:page--direct-sums.adoc---coordinate-vector}.

Consider the following diagram
Let
\(\mathcal{B}_1 = \{\vec{a}_1\ldots \vec{a}_m\}\),
\(\mathcal{B}_2 = \{\vec{b}_1\ldots \vec{b}_n\}\)
and
\(\mathcal{B}_3 = \{\vec{c}_1\ldots \vec{c}_p\}\).
Now, to show that

\begin{equation*}
    {}_{\mathcal{B}_3}[R]_{\mathcal{B}_2}\
    {}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
    =
    {}_{\mathcal{B}_3}[R\circ T]_{\mathcal{B}_1}
\end{equation*}

we need to show that the \(j\)'th column of the left
is the \(j\)'th column of the right,
which is simply \([\vec{a}_j]_{\mathcal{B}_3}\).
Consider the following diagram

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math3273/linear-transformations/matrix-product}
\end{figure}

Consider arbirary \(\vec{u} \in U\). Then,

\begin{equation*}
{}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
[\vec{u}]_{\mathcal{B}_1}
= [T(\vec{u})]_{\mathcal{B}_2}
\end{equation*}

Also,

\begin{equation*}
{}_{\mathcal{B}_3}[R]_{\mathcal{B}_2}
[T(\vec{u})]_{\mathcal{B}_2}
= [R(T(\vec{u}))]_{\mathcal{B}_3}
\end{equation*}

Then, we get that

\begin{equation*}
{}_{\mathcal{B}_3}[R]_{\mathcal{B}_2}
{}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
[\vec{u}]_{\mathcal{B}_1}
=
{}_{\mathcal{B}_3}[R]_{\mathcal{B}_2}
[T(\vec{u})]_{\mathcal{B}_2}
= [R(T(\vec{u}))]_{\mathcal{B}_3}
\end{equation*}

Finally since \(R\circ T \in \mathcal{L}(U,W)\)

\begin{equation*}
{}_{\mathcal{B}_3}[R\circ T]_{\mathcal{B}_1}
[\vec{u}]_{\mathcal{B}_1}
= [R(T(\vec{u}))]_{\mathcal{B}_3}
=
{}_{\mathcal{B}_3}[R]_{\mathcal{B}_2}
{}_{\mathcal{B}_2}[T]_{\mathcal{B}_1}
[\vec{u}]_{\mathcal{B}_1}
\end{equation*}

and since the choice of \(\vec{u}\) was arbirary, the matricies must be the
same. (we only needed to show it for basis vectors though)
\end{example}

\subsection{Corollary: Polynomials of transformations}
\label{develop--math3273:linear-transformations:page--matrix.adoc---corollary-polynomials-of-transformations}

Let \(p \in \mathbb{K}[X]\), then if \(T \in \mathcal{L}(V)\),

\begin{equation*}
{}_{\mathcal{B}_1}[p(T)]_{\mathcal{B}_1} =
p\left({}_{\mathcal{B}_1}[T]_{\mathcal{B}_1}\right)
\end{equation*}

where \(T^n\) is repeated application of \(T\).

\subsection{Corollary: Similar matricies}
\label{develop--math3273:linear-transformations:page--matrix.adoc---corollary-similar-matricies}

Let \(P = {}_{\mathcal{B}_2}[I]_{\mathcal{B}_1}\), then

\begin{equation*}
{}_{\mathcal{B}_2}[T]_{\mathcal{B}_2}
=
P{}_{\mathcal{B}_1}[T]_{\mathcal{B}_1}P^{-1}
\end{equation*}

\section{Determinants}
\label{develop--math3273:linear-transformations:page--matrix.adoc---determinants}

We have three ways to the determinant of
an \(n\times n\) matrix, \(A\), all of which are equivalent.

\begin{enumerate}[label=\arabic*)]
\item Laplace expansion
    
    \begin{equation*}
    \det(A) = \sum_{i=1}^n (-1)^{i+j} a_{ij} \det(A_{ij})
    \end{equation*}
    
    where \(1\leq j \leq n\) and \(A_{ij}\) is minor of \(A\) at \((i,j)\).
\item Leibnix formula
    
    \begin{equation*}
    \det(A) = \sum_{\sigma \in S_n} sgn(\sigma) \prod_{i=1}^n a_{i\sigma(i)}
    \end{equation*}
    
    where \(sgn(\sigma)\) is the
    \myautoref[{parity}]{develop--math2272:ROOT:page--permutations/index.adoc---parity-of-a-permutation} of \(\sigma\).
\item The unique function \(\det: \mathbb{K}^{n\times n} \to \mathbb{K}\) which is
    
    \begin{itemize}
    \item \(\det(I) = 1\)
    \item linear in each entry:
        
        \begin{equation*}
        \det(\vec{v}_1\ldots (a\vec{v}_i+\vec{v}')\ldots\vec{v}_n)
        =
        a\det(\vec{v}_1\ldots \vec{v}_i\ldots\vec{v}_n)
        + \det(\vec{v}_1\ldots \vec{v}'\ldots\vec{v}_n)
        \end{equation*}
    \item alternating:
        
        \begin{equation*}
        \det(\vec{v}_1\ldots \vec{v}_i\ldots \vec{v}_j\ldots\vec{v}_n)
        =
        -\det(\vec{v}_1\ldots \vec{v}_j\ldots \vec{v}_i\ldots\vec{v}_n)
        \end{equation*}
    \end{itemize}
\end{enumerate}

\subsection{Determinant of product}
\label{develop--math3273:linear-transformations:page--matrix.adoc---determinant-of-product}

The determinant of a matrix is a homomorphism from \(\mathbb{K}^{n\times n}\) to \(\mathbb{K}\)
where the group operation is multiplication. That is,

\begin{equation*}
\det(AB) = \det(A) \det(B)
\end{equation*}

\begin{example}[{Proof}]
Let \(A = [a_{ij}]\), \(B = [b_{ij}]\) and \(AB = [c_{ij}]\).
Then, firstly note that

\begin{equation*}
c_{ij} = \sum_{k=1}^n a_{ik}b_{kj}
\end{equation*}

Then,

\begin{equation*}
\begin{aligned}
\det(AB)
= \sum_{\sigma \in S_n} sgn(\sigma) \prod_{i=1}^n c_{i\sigma(i)}
= \sum_{\sigma \in S_n} sgn(\sigma) \prod_{i=1}^n \left[\sum_{k=1}^n a_{ik}b_{k\sigma(i)}\right]
\end{aligned}
\end{equation*}

Now, consider one term of the expansion of the product. That is,

\begin{equation*}
\prod_{i=1}^n a_{ik_i}b_{k_i\sigma(i)}
\end{equation*}

Now, suppose that two of the \(k_i\) were the same, that is, \(k_i = k_j\),
then, we could get the above would appear a second time when \(\sigma' = (i\ j)\sigma\).
Notice that \(sgn(\sigma') = -sgn(\sigma)\), and hence the two terms would cancel out.
Furthermore, notice that these items appear in pairs since each \(\sigma'\) uniquely defines
\(\sigma\) and they are not equal (we are assuming \(n > 1\) otherwise the inital result is trivial)

Therefore, we are left with the sum of terms with unique \(k_i\). We can enumerate this
by using \(S_n\) and we get the following sum

\begin{equation*}
\begin{aligned}
\det(AB)
&= \sum_{\sigma \in S_n} sgn(\sigma) \prod_{i=1}^n \left[\sum_{k=1}^n a_{ik}b_{k\sigma(i)}\right]
\\&= \sum_{\sigma \in S_n} sgn(\sigma) \sum_{\tau \in S_n} \prod_{i=1}^n a_{i\tau(i)}b_{\tau(i)\sigma(i)}
\\&= \sum_{\tau \in S_n} \sum_{\sigma \in S_n} sgn(\sigma) \prod_{i=1}^n a_{i\tau(i)}b_{\tau(i)\sigma(i)}
\end{aligned}
\end{equation*}

Now since \(S_n\) is a group \(S_n  = S_n \tau^{-1}\). So
where each of the \(k_i\) are unique. Hence, we can let them

\begin{equation*}
\begin{aligned}
\det(AB)
&= \sum_{\tau \in S_n} \sum_{\sigma\tau \in S_n} sgn(\sigma\tau) \prod_{i=1}^n a_{i\tau(i)}b_{\tau(i)\sigma\tau(i)}
\\&= \sum_{\tau \in S_n} \sum_{\sigma\tau \in S_n} sgn(\sigma\tau) \left[\prod_{i=1}^n a_{i\tau(i)}\right]\left[\prod_{i=1}^n b_{\tau(i)\sigma\tau(i)}\right]
\end{aligned}
\end{equation*}

and we can reorder the second product so that the \(\tau(i)\) appear in acending order

\begin{equation*}
\begin{aligned}
\det(AB)
&= \sum_{\tau \in S_n} \sum_{\sigma\tau \in S_n} sgn(\sigma\tau) \left[\prod_{i=1}^n a_{i\tau(i)}\right]\left[\prod_{i=1}^n b_{i\sigma(i)}\right]
\\&= \sum_{\tau \in S_n} \sum_{\sigma \in S_n} sgn(\sigma\tau) \left[\prod_{i=1}^n a_{i\tau(i)}\right]\left[\prod_{i=1}^n b_{i\sigma(i)}\right]
\end{aligned}
\end{equation*}

Note that that last step is different from when we replace \(\sigma\to \sigma\tau\). In this case,
we replaced \(S_n\) with \(S_n \tau^{-1}\). Then

\begin{equation*}
\begin{aligned}
\det(AB)
&=
    \left[\sum_{\tau \in S_n} sgn(\tau) \prod_{i=1}^n a_{i\tau(i)}\right]
    \left[\sum_{\sigma \in S_n} sgn(\sigma) \prod_{i=1}^n a_{i\sigma(i)}\right]
\\&=\det(A)\det(B)
\end{aligned}
\end{equation*}

And we are done!
\end{example}
\end{document}
