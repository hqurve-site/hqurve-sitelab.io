\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Jordan Canonical Forms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Firstly, we define the \(J_m\) to be the \(m\times m\) matrix with
ones on the \emph{super diagonal}. That is

\begin{equation*}
J_m = \begin{pmatrix}
0 & 1 & 0 & 0 & \cdots & 0 & 0\\
0 & 0 & 1 & 0 & \cdots & 0 & 0\\
0 & 0 & 0 & 1 & \cdots & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
0 & 0 & 0 & 0 & \cdots & 1 & 0\\
0 & 0 & 0 & 0 & \cdots & 0 & 1\\
0 & 0 & 0 & 0 & \cdots & 0 & 0
\end{pmatrix}
\end{equation*}

and we define the \emph{jordan block} \(J_m(\lambda) = \lambda I + J_m\). That is

\begin{equation*}
J_m(\lambda) = \begin{pmatrix}
\lambda & 1 & 0 & 0 & \cdots & 0 & 0\\
0 & \lambda & 1 & 0 & \cdots & 0 & 0\\
0 & 0 & \lambda & 1 & \cdots & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
0 & 0 & 0 & 0 & \cdots & 1 & 0\\
0 & 0 & 0 & 0 & \cdots & \lambda & 1\\
0 & 0 & 0 & 0 & \cdots & 0 & \lambda
\end{pmatrix}
\end{equation*}

Then define a \emph{jordan matrix} as a
\myautoref[{block diagonal matrix}]{develop--math3273:linear-transformations:page--block-matrix.adoc---diagonal-block-matrix}
where each of the blocks are jordan blocks.

Finally,
we say that the \emph{jordan form} of \(A \in \mathbb{F}^{n\times n}\) is jordan matrix
\(J\) iff it is similar to \(J\). Furthermore, the jordan form of \(A\)
is unique up to permutation of the jordan blocks.

\section{Writing \(\mathbb{F}^n\) as a direct sum of kernels}
\label{develop--math3273:eigenvectors:page--jordan.adoc---writing-latex-backslash-latex-backslashmathbb-latex-openbracef-latex-closebrace-latex-caretn-latex-backslash-as-a-direct-sum-of-kernels}

Let \(V\) be a vector space and let \(T \in \mathcal{L}(V)\)
which has all of its eigenvalues. That is, the \myautoref[{characteristic polynomial}]{develop--math3273:eigenvectors:page--index.adoc---characteristic-polynomial}
of \(T\), \(p_T\), can be factorized into a product of powers of degree 1 polynomials
as follows

\begin{equation*}
p_T(z) = \prod_{i=1}^k (z - \lambda_i)^{m_i}
\end{equation*}

where \(m_i\) is the algebraic multiplicity of eigenvalue \(\lambda_i\). Then
since each of the factors \((z-\lambda_i)^{m_i}\) have a pairwise gcd of \(1\),
we get that

\begin{equation*}
V =
\ker\left( (T - \lambda_1 I )^{m_1} \right) \oplus \cdots \oplus \ker\left((T -\lambda_k I)^{m_k}\right)
\end{equation*}

as discussed \myautoref[{previously}]{develop--math3273:linear-transformations:page--index.adoc---partitioning-the-space}.
Furthermore, each of the \(\ker\left((T-\lambda_i I)^{m_i}\right)\) are invariant wrt
\(T\). Then, we can restrict our discussion of \(T\) to \(\ker\left((T - \lambda_i)^{m_i}\right)\)
or rather

\begin{equation*}
S \in \mathcal{L}\left(\ker\left((T - \lambda_i)^{m_i}\right)\right)
\end{equation*}

such that \(S^{m_i} = \theta\). Notice that \(S = T-\lambda_i I\) satisfies this condition.

\section{Nilpotent operators and cyclic subspaces}
\label{develop--math3273:eigenvectors:page--jordan.adoc---nilpotent-operators-and-cyclic-subspaces}

Let \(T \in \mathcal{L}(V)\). Then we define the following

\begin{description}
\item[Nilpotent] We say that \(T\) is \emph{nilpotent} if
    there exists \(m \geq 1\) such that \(T^m = \theta\).
\item[Cyclic Subspace] Let \(\vec{v} \in V\) such that
     \(\exists m \geq 1\) such that \(T^m(\vec{v}) = \vec{0}\) and
    \(T^{m-1}(\vec{v}) \neq \vec{0}\). Then we call \(\vec{v}\) a \emph{cyclic vector}
    and define the \emph{cyclic subspace} \(C(\vec{v})\) of \(V\) as
    
    \begin{equation*}
    C(\vec{v}) = span\{\vec{v}, T(\vec{v}), \ldots T^{m-1}(\vec{v})\}
    = \{p(T)(\vec{v}): p \in \mathbb{F}[X]\}
    \end{equation*}
\end{description}

Then we claim that \(\vec{v}, \ldots T^{m-1}(\vec{v})\) are linearly independent (and hence
form a basis for \(C(\vec{v})\)).

\begin{example}[{Proof}]
Consider a linear combination of \(\vec{v}, \ldots T^{m-1}(\vec{v})\) which is \(\vec{0}\). That is

\begin{equation*}
a_0 \vec{v} + a_1 T(\vec{v}) + \cdots a_{m-1}T^{m-1}(\vec{v}) = \vec{0}
\end{equation*}

Then, we would prove by induction that all \(a_{m-1} = 0\)  (overkill).
We have the following statement

\begin{equation*}
P(n): \forall i < n: a_i = 0
\end{equation*}

Then this is true for \(P(0)\). Now, suppose that it is true for some \(n\geq 0\), we get that

\begin{equation*}
\sum_{i=n}^{m-1}a_iT^i(\vec{v}) = \vec{0}
\end{equation*}

Then

\begin{equation*}
\vec{0}
= T^{m-n-1}\left(\sum_{i=n}^{m-1}a_iT^i(\vec{v})\right)
= \sum_{i=n}^{m-1}a_iT^{i+m-n-1}(\vec{v})
= \sum_{i=m-1}^{m-1 + m-n-1}a_{i-m+n+1}T^{i}(\vec{v})
\end{equation*}

Then since \(T^i(\vec{v}) = \vec{0}\) for all \(i\geq m\) we get that

\begin{equation*}
\vec{0}
= \sum_{i=m-1}^{m-1 + m-n-1}a_{i -m +n +1}T^{i}(\vec{v})
= \sum_{i=m-1}^{m-1}a_{i-m+n+1}T^{i}(\vec{v})
= a_n T^{m-1}(\vec{v}) \implies a_n = \vec{0}
\end{equation*}

and we get that \(P(n+1)\) is true.
\end{example}

\subsection{Intersection of cyclic subspaces}
\label{develop--math3273:eigenvectors:page--jordan.adoc---intersection-of-cyclic-subspaces}

Let \(\vec{v}, \vec{w} \in V\) be cyclic vectors. Then \(\exists j,k \geq 0\) such that

\begin{equation*}
C(\vec{v}) \cap C(\vec{w}) = C(T^j(\vec{v})) = C(T^k(\vec{w}))
\end{equation*}

Therefore, there exists minimal \(j, k\) such that

\begin{equation*}
C(T^j(\vec{v})) = C(T^k(\vec{w}))
\end{equation*}

\begin{admonition-important}[{}]
Sometimes \(j\) and \(k\) may be the values such that \(T^j(\vec{v}) = \vec{0}\)
in which case.
\end{admonition-important}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
I am only going to prove that \(C(T^j(\vec{v})) = C(\vec{v}) \cap C(\vec{w})\) as the proof
for the other holds by symmetry.
\end{admonition-note}

Firstly, let \(m, n\) be the least powers of \(T\) such that \(T^m(\vec{v}) = \vec{0}\) and \(T^n(\vec{w}) = \vec{0}\).

Let \(A\) be the set of polynomials such that \(p(T)(\vec{v}) \in C(\vec{w})\). That is

\begin{equation*}
A = \{p\in \mathbb{F}[X]: p(T)(\vec{v}) \in C(\vec{w})\}
\end{equation*}

Then \(A\) is an ideal since

\begin{equation*}
p,q \in A \implies (p+q)(T)(\vec{v}) = p(T)(\vec{v}) + q(T)(\vec{v}) \in C(\vec{w}) \implies p+q \in A
\end{equation*}

\begin{equation*}
p\in A, q\in \mathbb{F}[X]: (qp)(T)(\vec{v}) = q(T)[p(T)(\vec{v})] \in C(\vec{w})
\end{equation*}

since \(C(\vec{w})\) is closed upon repeated application of \(T\). Then since
all polynomial ideals are principal, let \((a) = A\) [\myautoref[{see here}]{develop--math3273:linear-transformations:page--polynomials.adoc---all-ideals-are-principal}].
Now, let \(j\) be the degree of the least term in \(a\).
Then, notice that \(j\leq m\) since \(z^m \in A\) as \(T^m(\vec{v}) = \vec{0} \in C(\vec{w})\). Hence,
\(gcd(a, z^m) = z^j\) and by Bezout's lemma, \(\exists a', b' \in \mathbb{F}[X]\) such that

\begin{equation*}
a'a + b'z^m = z^j
\end{equation*}

and hence \(z^j \in A\). Therefore, \((z^j) = A\) (in fact, \(a\) was a scalar multiple of \(z^j\)).
Then since

\begin{equation*}
\vec{u} \in C(\vec{v}) \cap C(\vec{w})
\iff \exists p \in \mathbb{F}[X]: p(T)(\vec{v}) = \vec{u} \in C(\vec{w})
\iff \exists p \in A: \vec{u} = p(T)(\vec{v})
\end{equation*}

we have proven our result.
\end{example}

\subsection{Writing the vector space as a direct sum of cyclic subspaces}
\label{develop--math3273:eigenvectors:page--jordan.adoc---writing-the-vector-space-as-a-direct-sum-of-cyclic-subspaces}

Let \(T \in \mathcal{L}(V)\) be nilpotent. Then we could write \(V\)
as a direct sum of cyclic subspaces.

\begin{example}[{Proof}]
\begin{admonition-important}[{}]
This is essentially the proof outlined in the video
\url{https://www.youtube.com/watch?v=9wWYAo-7LYc} however, we would outline the
algorithm since the recursiveness of induction does not
\href{https://idioms.thefreedictionary.com/do+it+justice}{do it justice}.
\end{admonition-important}

Let \(n\) be such that \(T^n =\theta\) but \(T^{n-1} \neq \theta\). Then
notice the following result.

\begin{sidebar}[{Lemma 1}]
Let \(\vec{u}_1, \ldots \vec{u}_k\) be a basis for \(U \subseteq \ker(T) \cap T^{m}(V)\)
where \(1\leq m < n\). Then there exists \(\vec{w}_1, \ldots \vec{w}_k\) where
\(T^m(\vec{w}_i) = \vec{w}_i\) and

\begin{equation*}
W = C(\vec{w}_1) \oplus C(\vec{w}_2) \oplus \cdots C(\vec{w}_k)
\end{equation*}

Furthermore, \(\ker(T) \cap W = U\).

\begin{example}[{Proof}]
By definition if \(\vec{u}_i\in T^m(V)\), there exists \(\vec{w}_i \in V\) such that
\(T^m(\vec{w}_i) = \vec{u}_i\). Now, consider

\begin{equation*}
\vec{0} = p_1(T)(\vec{w}_1) + p_2(T)(\vec{w}_2) + \cdots + p_k(T)(\vec{w}_k)
\in C(\vec{w}_1) + C(\vec{w}_2) + \cdots + C(\vec{w}_k)
\end{equation*}

where \(p_i \in \mathbb{F}[X]\). We want to show that each of the \(p_i = \theta\).
Suppose that at least one isn't. Then there exists minimal \(j \geq 0\) such
that one of the \(p_i\) has a term of degree
\(j\) with non-zero coefficient. Then consider

\begin{equation*}
\vec{0}
= T^{m-j}(\vec{0})
= p_1(T)T^{m-j}(\vec{w}_1)
+ p_2(T)T^{m-j}(\vec{w}_2)
+ \cdots
+ p_k(T)T^{m-j}(\vec{w}_k)
\end{equation*}

Then each of the \(p_k(T)T^{m-j}\) contain powers of \(T\) at least \(m\)
and since \(\vec{u}_i = T^m(\vec{w}_i) \in \ker(T)\), all powers greater than \(m\)
result in \(0\). Therefore, we are dealing with a linear combination of the \(\vec{w}_i\)
which is equal to zero. Then since they form a basis, each of the coefficients must be zero which contradicts
the assertion that the \(j\)'th coefficient of at least one of the \(p_i\) was non-zero.

Therefore, we get that each of the \(p_i = \theta\) and the following is indeed a direct sum

\begin{equation*}
W = C(\vec{w}_1) \oplus C(\vec{w}_2) \oplus \cdots C(\vec{w}_k)
\end{equation*}

Next, suppose that \(\vec{w} \in W \cap \ker(T)\). Then we write

\begin{equation*}
\vec{w} = p_1(T)(\vec{w}_1) + p_2(T)(\vec{w}_2) + \cdots + p_k(T)(\vec{w}_k)
\end{equation*}

We can disregard and terms in the \(p_i\) with degree greater than \(m\) as it
adds zero contribution. Then suppose one of the \(p_i\) had a term with degree
less than \(m\) with non-zero coefficient. Then

\begin{equation*}
\vec{0} = T(\vec{w}) = p_1(T)T(\vec{w}_1) + p_2(T)T(\vec{w}_2) + \cdots + p_k(T)T(\vec{w}_k)
\end{equation*}

and notice that at least one of the \(p_i(T)T(\vec{w}_i)\) is non zero which is a contradiction.
Therefore, \(\vec{w}\) is a linear combination of \(T^m(\vec{w}_i) = \vec{u}_i\)
and we are done.
\end{example}
\end{sidebar}

\begin{sidebar}[{Lemma 2}]
Let \(W \subseteq V\) be a subspace which can be written as a direct sum of
cyclic subspaces and \(U \subseteq \ker(T) \cap T^{m}(V)\). Then if \(U \cap W = \{\vec{0}\}\),
there exists \(U' \supseteq U\) which can be written as a direct sum of cyclic subspaces where
\(U' \cap W = \{\vec{0}\}\).

\begin{example}[{Proof}]
Firstly, from lemma 1, \(U'\) exists.
Next, consider \(\vec{v} \in W \cap U'\). Suppose that \(\vec{v} \neq \vec{0}\).
Then there exists a \(i \geq 0\)
such that \(T^i(\vec{v}) \neq \vec{0}\) and \(T^{i+1}(\vec{v}) = \vec{0}\).
Then \(T^i(\vec{v}) \in \ker(T) \cap U'\) implies that \(T^i(\vec{v}) \in U \cap W\)
which implies that \(T^i(\vec{v}) = \vec{0}\). This is a contradiction and hence
\(W \cap U' = \{\vec{0}\}\)
\end{example}
\end{sidebar}

Notice that

\begin{equation*}
\varnothing \subset T^{n-1}(V) \subset T^{n-2}(V)\subset \cdots T(V) \subset V
\end{equation*}

Since if \(T^{i}(V) = T^{i+1}(V)\), \(T\) would no longer be nilpotent.

Now, we would outline the algorithm for writing \(V\) as a direct sum of cyclic subspaces.

\begin{sidebar}[{Algorithm}]
\begin{enumerate}[label=\arabic*)]
\item Let \(V_n = \{0\}\) and \(i = 1\). Then clearly \(V_n\)
    is written as a direct sum of cyclic subspaces.
\item Consider \(U \subseteq \ker(T) \cap T^{n-i}(V)\) such that
    \(U_{n-i} \cap V_{n-(i-1)} = \{\vec{0}\}\) and
    \(\ker(T) \cap T^{n-i}(V) \subseteq U_{n-i} + V_{n-(i-1)}\). We can perhaps
    construct \(U_{n-i}\) by extending a basis for \(V_{n-(i-1)}\). Then by
    lemma 2, there exists \(U_{n-i}'\) such that \(U_{n-i} \subseteq U_{n-i}'\)
    which can be written as a direct sum of cyclic subspaces
    and \(U_{n-i}' \cap V_{n-(i-1)} = \{\vec{0}\}\). We then define \(V_{n-i} = U_{n-i}' \oplus V_{n-(i-1)}\)
\item If \(i < n\), increment \(i\) by \(1\) and go back to step 2
\end{enumerate}
\end{sidebar}

Then what does this algorithm do? We want to show that \(T^{n-i}(V) \subseteq V_{n-i}\).
Then by the time we reach \(V_0\), we get \(T^0(V) = V \subseteq V_{0}\).

\begin{example}[{Proof}]
Firstly, each of the \(V_{n-i}\) can be written as the direct sum of cyclic subgroups \(C(\vec{v}_j)\)
where \(T^{n-i}(\vec{v}_j) \neq \vec{0}\) by lemma 1. Then clearly \(V_n = \{\vec{0}\} = \theta(V) = T^{n}(V)\).

Suppose that \(T^{n-i}(V) \subseteq V_{n-i}\) and consider \(\vec{v} \in T^{n-(i+1)}(V) \backslash \{0\}\).
Then \(T(\vec{v}) \in T^{n-i}(V) \subseteq V_{n-i}\). Let

\begin{equation*}
T(\vec{v}) = \sum_{j=1}^k p_j(T)(\vec{v}_j)
\end{equation*}

where \(V_{n-i} = C(\vec{v}_1) \oplus \cdots \oplus C(\vec{v}_k)\). Suppose that one of the \(p_j\) has a term
with degree less than to \(n-i+1\), then

\begin{equation*}
\sum_{j=1}^k p_j(T)T^{i-1}(\vec{v}_j) = T^{i}(\vec{v}) \in T^{n}(\vec{v}) = \{\vec{0}\}
\end{equation*}

which is a contradiction. Then since \(p_j\) all have terms of degree greater than or equal to \(n-i\), there exists
\(\vec{w} \in V_{n-i}\) such that \(T^{n-i}(\vec{w}) = T(\vec{v})\). Therefore, \(T^{n-(i+1)}(\vec{w}) - \vec{v} \in \ker(T)\).
Furthermore, \(T^{n-(i+1)}(\vec{w}) - \vec{v} \in T^{n-(i+1)}\). Therefore, by construction of
\(V_{n-(i+1)}\), \(T^{n-(i+1)}(\vec{w}) - \vec{v}\in V_{n-(i+1)}\). Then since \(\vec{w} \in V_{n-i} \subseteq V_{n-(i+1)}\),
we get that \(\vec{v} \in V_{n-(i+1)}\).
\end{example}
\end{example}

\subsection{Matrix of nilpotent operator}
\label{develop--math3273:eigenvectors:page--jordan.adoc---matrix-of-nilpotent-operator}

If \(T^n = \theta\), then \(V\) can be written as follows

\begin{equation*}
V = C(\vec{v}_1) \oplus C(\vec{v}_2) \oplus \cdots \oplus C(\vec{v}_k)
\end{equation*}

Then we define \(m_i\) be such that
\(T^{m_i-1}(\vec{v}_i) \neq \vec{0}\) but \(T^{m_i}(\vec{v}_i) = \vec{0}\).
Now, focus on any one of the \(C(\vec{v}_i)\), then

\begin{equation*}
C(\vec{v}_i) = span \{
T^{m_i -1}(\vec{v}_i),\
T^{m_i -2}(\vec{v}_i),\ \ldots, \
T(\vec{v}_i),\
\vec{v}
\}
\end{equation*}

Then if we restrict \(T\) to \(C(\vec{v}_i)\) we can write the matrix
as \(J_{m_i}\). Hence

\begin{equation*}
[T] = J_{m_1} \oplus J_{m_2} \oplus \cdots \oplus J_{m_k}
\end{equation*}

Furthermore, notice that \(\vec{v}\) is an eigenvector of \(T\) iff
it is a linear combination of the \(T^{m_i -1}\). Therefore, the number of
jordan blocks is equal to the dimension of the \(\ker(T)\).

\section{Jordan Normal Form}
\label{develop--math3273:eigenvectors:page--jordan.adoc---jordan-normal-form}

Let \(T\) be a linear operator. Then, from before, we
can write

\begin{equation*}
V =
\ker\left( (T - \lambda_1 I )^{m_1} \right) \oplus \cdots \oplus \ker\left((T -\lambda_k I)^{m_k}\right)
\end{equation*}

Then since each of \(T-\lambda_i I\) are a nilpotent operator when restricted to
\(\ker\left( (T - \lambda_i I )^{m_i} \right)\) we can write
the matrix of \([T-\lambda_i I]\) as a block diagonal of jordan blocks.
Hence \(T\) (when restricted) can be written as a block diagonal of jordan blocks
with \(\lambda_i\) on the diagonal.

Finally, the matrix of \(T\) on \(V\) can be written as a block diagonal of jordan blocks.
This is the jordan normal form of a matrix.
\end{document}
