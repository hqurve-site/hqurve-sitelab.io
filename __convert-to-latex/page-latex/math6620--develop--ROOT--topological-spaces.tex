\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Topological Spaces}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(X\neq\varnothing\) be a set and \(\mathcal{T} \subseteq \mathcal{P}(X)\)
such that

\begin{enumerate}[label=\arabic*)]
\item \(\varnothing, X \in \mathcal{T}\)
\item If \(U \subseteq \mathcal{T}\), then \(\bigcup_{u\in U}u \in \mathcal{T}\)
\item If \(u_1, \ldots u_n \in \mathcal{T}\), then \(\bigcap_{i=1}^n u \in \mathcal{T}\)
\end{enumerate}

Then, we call \((X, \mathcal{T})\) is a \emph{topological space} where \(\mathcal{T}\) is called the \emph{topology}
and any \(u \in \mathcal{T}\) is called an \emph{open set}.
We may also simply refer to \(X\) as a topological space when the topology is obvious.

\begin{admonition-tip}[{}]
The third property is equivalent to the case when \(n=2\).
Therefore, in proofs, it may be simpler to prove that \(u, v\in \mathcal{T} \implies u\cap v \in \mathcal{T}\).
\end{admonition-tip}

\section{Closed sets}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---closed-sets}

Let \((X, \mathcal{T})\) be a topology and \(F \subseteq X\). Then we say that \(F\) is \emph{closed}
if \(F^c \in \mathcal{T}\).
Using De Morgan's laws, we obtain the following properties

\begin{enumerate}[label=\arabic*)]
\item \(\varnothing\) and \(X\) are closed.
\item If \(U\) is a collection of closed sets, then \(\bigcap_{u\in U}u\) is closed.
\item If \(u_1, \ldots u_n\) are closed, then \(\bigcup_{i=1}^n u \in \mathcal{T}\)
\end{enumerate}

Note that it is possible to do topology without explicitly specifying closed sets; however, it is more convenient
to have a word for the complement of open sets.

\begin{admonition-tip}[{}]
If \(X\) is finite, then the set of closed sets is also a topologies.
\end{admonition-tip}

\begin{admonition-tip}[{}]
The letter \(F\) is usually used for closed spaces since the french word for closed starts with `F',
    \href{https://translate.google.com/?sl=auto\&tl=fr\&text=close\&op=translate}{fermer}.
\end{admonition-tip}

\section{Fine and Coarse}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---fine-and-coarse}

Let \(X\) be a set with topologies \(T\) and \(T'\).
If \(T \subseteq T'\), we say that \(T'\) is \emph{finer} than \(T\).

\begin{admonition-tip}[{}]
(from Dr. Tweedle) Consider a big rock being broken into gravel. The more we break it up, the finer it is.
\end{admonition-tip}

\section{Basis}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---basis}

As in other subjects, a basis allows for easy understanding of a given structure.
This is discussed separately in \myautoref[{}]{develop--math6620:ROOT:page--basis.adoc}.

\section{Examples}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---examples}

Some special examples are given in \myautoref[{}]{develop--math6620:special-topologies:page--index.adoc}.

\subsection{Discrete and indiscrete topologies}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---discrete-and-indiscrete-topologies}

Let \(X\) be a non-empty set, then \((X, \mathcal{P}(X))\) is a topology.
We call this topology the discrete topology since for each element, there is a set containing only it.
This allows us to ``tell the difference'' between elements (similar to \(\mathbb{Z}\)).

The set \((X, \{(\varnothing, X)\})\) is also a topology called the indiscrete topology
since all the elements are ``lumped together''.

\subsection{Metric spaces}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---metric-spaces}

Let \((X, d)\) be a metric space (see \myautoref[{}]{develop--math3277:metrics:page--index.adoc}). Then,
the set \(\mathcal{T}\) defined by the following property is a topology on \(X\).

\begin{equation*}
U \in \mathcal{T} \quad\iff\quad \forall x \in U: \exists \varepsilon > 0: B(x, \varepsilon) \subseteq U
\end{equation*}

This is proven in \myautoref[{}]{develop--math6620:ROOT:page--basis.adoc}

\subsection{Finite sets}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---finite-sets}

\begin{admonition-note}[{}]
We use \(J_n\) but this is general to any finite set since a set is finite iff there is a bijection with one \(J_n\).
\end{admonition-note}

Consider the set \(J_3 = \{1,2,3\}\). Then, the following are some of the possible topologies on \(J_3\)

\begin{itemize}
\item \(\{\varnothing, \{1,2\}, \{1,2,3\}\}\)
\item \(\{\varnothing, \{1\}, \{1,2\}, \{1,2,3\}\}\)
\item \(\{\varnothing, \{1\}, \{1,2\}, \{1,3\}, \{1,2,3\}\}\)
\item \(\{\varnothing, \{1\}, \{2\}, \{3\}, \{1,2\}, \{1,3\}, \{2,3\}, \{1,2,3\}\}\)
\end{itemize}

Note that only the last topology corresponds with a metric.
We prove that each \(J_n\) has a single topology which corresponds to a metric (the discrete metric).

\begin{proof}[{Any topology generated by a metric space on a finite set is the powerset}]
Consider \(J_n\) with metric \(d\) and topology \(\mathcal{T}\).
We would prove that \(\mathcal{T} = \mathcal{P}(J_n)\)
Let

\begin{equation*}
\varepsilon = \frac{1}{2} \min \left\{d(x,y)\ \middle|\ x,y \in J_n, \ x\neq y\ \right\}
\end{equation*}

Note that a minimum exists since \(J_n\) is finite
Also, since \(d\) is a metric \(\varepsilon > 0\).

We would prove that for each \(x \in J_n\), \(\{x\} \in \mathcal{T}\).
Consider arbitrary \(x \in J_n\) and \(B(x, \varepsilon)\).
Since \(\varepsilon\) is less than each \(d(x, y)\) where \(y\neq x\),
\(B(x, \varepsilon) = \{x\}\).

Consider \(U \in \mathcal{P}(J_n)\). We may instead represent \(U\)
as the union of the singletons.
Since the singletons are all in \(\mathcal{T}\), their union, \(U\), must also be in \(\mathcal{T}\).
Therefore \(\mathcal{P}(J_n) \subseteq \mathcal{T} \subseteq \mathcal{P}(J_n)\).

Therefore, all topologies generated by a metric space of \(J_n\) are the same \(\mathcal{P}(J_n)\).
\end{proof}

Another question about \(J_n\) is how many topologies exist.
This number is finite since it is bounded above by \(2^{2^n}\) but it seems like a hard question.
This question is answered in \myautoref[{}]{develop--math6620:appendix:page--counting-finite.adoc}.

\subsection{Cofinite topology}
\label{develop--math6620:ROOT:page--topological-spaces.adoc---cofinite-topology}

Let \(X\) be a non-empty set. Then, the set \(\mathcal{T}\) defined by the following property
is a topology on \(X\)

\begin{equation*}
U \in \mathcal{T}
\quad\iff\quad
U = \varnothing \text{ or } X -U \text{ is finite}
\end{equation*}
\end{document}
