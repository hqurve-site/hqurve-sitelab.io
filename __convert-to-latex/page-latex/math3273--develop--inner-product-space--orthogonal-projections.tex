\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Orthogonal Projections}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Let \(V\) be a \myautoref[{inner product space}]{develop--math3273:inner-product-space:page--index.adoc} and \(U, W\) be subspaces such that \(U\perp W\) and
\(U \oplus W = V\), then \(P_{U, W}\) is an \emph{orthogonal projection}.
We first want to show that \(W\) is merely \(U^\perp\) and that it \(W = U^\perp\) satisfies
the above conditions.

\begin{description}
\item[Necessary (\(W\) must be \(U^\perp\))] Clearly, \(W \subseteq U^\perp\), now consider arbitrary \(\vec{v} \in U^\perp\). Then,
    we write \(\vec{v} = \vec{u} + \vec{w}\). Then, \(\vec{u} = \vec{v} - \vec{w} \in U^\perp\)
    and hence \(\vec{u} = \vec{0}\). Therefore, \(\vec{v} \in W\) and \(W = U^\perp\).
\end{description}

\begin{admonition-note}[{}]
for sufficiency,
\href{https://en.wikipedia.org/wiki/Orthogonal\_complement\#Inner\_product\_spaces}{the wikipeidia article}
outlines that
\(U\) must be a closed subset of \(V\) which is a hilbert space. However, this result is true
if \(V\) is finite as we could write \(U^\perp\) as the span of the remaining orthonormal basis.
We would continue the rest of this page assuming that \(V\) is finite dimensional.
\end{admonition-note}

We are now able to give an expression for \(P_U\). Let \(\vec{u}_1, \ldots \vec{u}_k\) be an orthonormal
basis for \(U\). Then

\begin{equation*}
P_U(\vec{v})
= \sum_{i=1}^k \abrack{\vec{u}_i, \vec{v}}\vec{u}_i
\end{equation*}

Then \(P_U(\vec{u}_i)= \vec{u}_i\) and hence \(P_U^2 = P\).

Furthermore, if \(V = \mathbb{C}^n\), we could write \(P_U\) as a matrix as follows

\begin{equation*}
P_U(\vec{v})
= \sum_{i=1}^k \abrack{\vec{u}_i, \vec{v}}\vec{u}_i
= \sum_{i=1}^k (\vec{u}_i^*\vec{v})\vec{u}_i
= \sum_{i=1}^k \vec{u}_i(\vec{u}_i^*\vec{v})
= \sum_{i=1}^k (\vec{u}_i\vec{u}_i^*)\vec{v}
= \left(\sum_{i=1}^k \vec{u}_i\vec{u}_i^*\right)\vec{v}
\end{equation*}

Then by thinking in terms of block matrices
 [\myautoref[{see here}]{develop--math3273:linear-transformations:page--block-matrix.adoc---understanding-matrix-multiplication-using-block-matrices}]

\begin{equation*}
P_U = \sum_{i=1}^k \vec{u}_i\vec{u}_i^* = RR^*
\end{equation*}

where

\begin{equation*}
R = \begin{bmatrix}\vec{u}_1 & \cdots & \vec{u}_k \end{bmatrix}
\end{equation*}

is an \(n\times k\) matrix.
From this we immediately see that \(P_U^* = P_U\).

\section{Gram-Schmidt Orthogonalization}
\label{develop--math3273:inner-product-space:page--orthogonal-projections.adoc---gram-schmidt-orthogonalization}

Consider any step of the Gram-Schmidt orthogonalization process.

\begin{equation*}
\vec{u}_k = \vec{v}_k - \sum_{i=1}^{k-1} \abrack{\frac{\vec{u}_i}{||\vec{u}_i||}, \vec{v}_k} \frac{\vec{u}_i}{||\vec{u}_i||}
\end{equation*}

Then, we see that we are writing \(\vec{v}_k\) has the sum of \(\vec{u}_k\) and the projection
of \(\vec{v}_k\) onto the subspace formed by \(\vec{u}_1\ldots \vec{u}_{k-1}\).

\section{QR Factorization}
\label{develop--math3273:inner-product-space:page--orthogonal-projections.adoc---qr-factorization}

Previously, when transforming a matrix \(A\) into reduced row echelon
form using Gauss-Jordan elimination, we were really finding
an invertible matrix \(P\) and row reduced echelon matrix \(R\) such that

\begin{equation*}
PA = R
\end{equation*}

Now, we want unitary matrix \(V\) and upper triangular matrix \(R\) with real, non-negative diagonal entries such that

\begin{equation*}
A = V \begin{bmatrix}R \\ 0\end{bmatrix}
\end{equation*}

where we have the following dimensions

\begin{itemize}
\item \(A\) is \(m\times n\) where \(m\geq n\)
\item \(V\) is \(m\times m\)
\item \(R\) is an \(n\times n\) matrix
\end{itemize}

Then, if we write \(V = \begin{bmatrix}Q & Q'\end{bmatrix}\) where \(Q\) has \(n\) columns,
\(A = QR\).

We would show that such matrices \(V\) and \(R\) exists and would later
show that if \(rank(A) = n\) (\(A\) is full rank), the matrices
\(Q\) and \(R\) are unique and the diagonal of \(R\) consists of positive entries.

\begin{example}[{Proof}]
Let \(A = \begin{bmatrix}\vec{a}_1 & \cdots & \vec{a}_m\end{bmatrix}\) and
\(V = \begin{bmatrix}\vec{u}_1 & \cdots & \vec{u}_m\end{bmatrix}\) and

\begin{equation*}
R = \begin{bmatrix}
r_{11} & r_{12} & r_{13} & \cdots & r_{1n}\\
       & r_{22} & r_{23} & \cdots & r_{2n}\\
       &        & r_{33} & \cdots & r_{3n}\\
       &        &        & \ddots & \vdots\\
       &        &        &        & r_{nn}
\end{bmatrix}
\end{equation*}

hence

\begin{equation*}
\vec{a}_k = \sum_{i=1}^k r_{ik}\vec{u}_i
\end{equation*}

and since the \(\vec{u}_i\) form an orthonormal basis, \(r_{ik} = \abrack{\vec{u}_i, \vec{a}_k}\).
That is

\begin{equation*}
\vec{a}_k
 = \sum_{i=1}^k \abrack{\vec{u}_i, \vec{a}_k}\vec{u}_i
\end{equation*}

Hence,

\begin{equation*}
span\{\vec{a}_1,\ldots \vec{a}_k\} \subseteq span\{\vec{u}_1, \ldots \vec{u}_k\}
\end{equation*}

where equality holds iff \(\vec{a}_1, \ldots \vec{a}_k\) are independent.

Now, when \(k=1\), we get that \(\vec{a}_1 = r_{11}\vec{u}_1\) and hence
\(\|\vec{a}_1\| = r_{11}\|\vec{u}_1\| = r_{11}\). If \(\vec{a}_1 \neq \vec{0}\),
\(\vec{u}_1\) is uniquely determined and \(r_{11} \geq 0\). However,
if \(\vec{a}_1 = \vec{0}\), we let \(\vec{u}_1\) be any normalized vector
(perhaps \(\vec{e}_1\)) and \(r_{11} = 0\).

Now, for \(k > 1\), we manipulate the formula a little bit

\begin{equation*}
r_{kk}\vec{u}_k = \abrack{\vec{u}_k, \vec{a}_k}\vec{u}_k = \vec{a}_k - \sum_{i=1}^{k-1} \abrack{\vec{u}_i, \vec{a}_k}\vec{u}_i
\end{equation*}

Then, we have two cases

\begin{itemize}
\item If \(\vec{k} \in span\{\vec{u}_1 \ldots \vec{u}_{k-1}\}\), we would get that the sum on the right
    is \(\vec{0}\) and hence we need that \(r_{kk} = 0\) and \(\vec{u}_k\) be any normalized
    vector which extends the independent set \(\{\vec{u}_1, \ldots \vec{u}_{k-1}\}\) (perhaps by doing the
    Gram-Schmidt orthogonalization process on \(\{\vec{u}_1, \ldots \vec{u}_{k-1}, \vec{e}_1\ldots \vec{e}_k\}\)).
\item If \(\vec{k} \notin span\{\vec{u}_1,\ldots \vec{u}_{k-1}\}\), the sum on the right is non-zero and hence
    \(r_{kk}\) is its norm. Hence, \(\vec{u}_k\) is uniquely determined.
\end{itemize}

And hence we do this iteratively until we reach \(\vec{a}_n\) at which point we extend the orthonormal basis to
get \(m\) vectors which form \(V\).

\begin{admonition-note}[{}]
This is essentially conducting the Gram-Schmidt orthogonalization process on the columns of \(A\)
however, we insert an independent vector if ever we get \(\vec{0}\) instead of ignoring it.
\end{admonition-note}

Now, lets consider uniqueness. Clearly if \(A\) is not full rank, its columns (\(\vec{a}_i\)) are not
linearly independent and at some point we would get that we have to make a choice for \(\vec{u}_k\) and the
associated \(r_{kk} = 0\). Therefore we get that \(Q\) (the first \(n\) columns of \(V\))
is not uniquely determined and the diagonal of \(R\)
may contain zeros (but is still non-negative).

Otherwise, if \(A\) has full rank, its columns are linearly independent and hence each of the \(\vec{u}_k\)
are uniquely determined. Hence each of the \(r_{ij}\) are uniquely determined. Furthermore, since
\(r_{kk}\) is the norm of a non-zero vector, we get that it is positive. Therefore, we get that \(Q\)
(the first \(n\) columns of \(V\))
and \(R\) are uniquely determined where the diagonal of \(R\) is strictly positive.
\end{example}

\section{Householder transformations}
\label{develop--math3273:inner-product-space:page--orthogonal-projections.adoc---householder-transformations}

Let \(\vec{w} \neq 0\), then we define the \emph{householder transformation} of \(\vec{w}\) as

\begin{equation*}
U_{\vec{w}} = I - 2P_{\vec{w}}
\end{equation*}

Then, notice that if we write \(\vec{x} = \vec{v} + k\vec{w}\) where \(\vec{v} \perp \vec{w}\),
\(U_{\vec{w}}(\vec{x}) = \vec{v} -k\vec{w}\). That is \(U_{\vec{w}}\) is a reflection
about the space orthogonal to \(\vec{w}\).

\begin{admonition-note}[{}]
In fact, notice that

\begin{equation*}
-\vec{v} = U_{\vec{w}} (\vec{v}) = \vec{v} -2 P_{\vec{w}}(\vec{v})
\iff P_{\vec{w}}(\vec{v}) = \vec{v} \iff \vec{v} = k\vec{w}
\end{equation*}
\end{admonition-note}

Next, notice that

\begin{equation*}
U_{\vec{w}}^2 = (I - 2P_{\vec{w}})(I - P_{\vec{w}})
= I - 4P_{\vec{w}} + 4P_{\vec{w}}^2
= I - 4P_{\vec{w}} + 4P_{\vec{w}}
= I
\end{equation*}

since \(P_{\vec{w}}\) is a projection. Also, notice that
\(U_{\vec{w}}\) is an isometry and \(U_{\vec{w}}^* U_{\vec{w}} = I\)

\begin{example}[{}]
\begin{equation*}
\begin{aligned}
\|U_{\vec{w}}(\vec{v})\|^2
&= \abrack{U_{\vec{w}}(\vec{v}), U_{\vec{w}}(\vec{v}) }
\\&= \abrack{\vec{v}- 2P_{\vec{w}}(\vec{v}), \vec{v} - 2P_{\vec{w}}(\vec{v}) }
\\&= \abrack{\vec{v}- P_{\vec{w}}(\vec{v}) - P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v}) - P_{\vec{w}}(\vec{v}) }
\\&=
\abrack{\vec{v}- P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v})}
- \abrack{\vec{v}- P_{\vec{w}}(\vec{v}), P_{\vec{w}}(\vec{v})}
- \abrack{P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v})}
+ \abrack{P_{\vec{w}}(\vec{v}), P_{\vec{w}}(\vec{v})}
\\&=
\abrack{\vec{v}- P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v})}
+ \abrack{P_{\vec{w}}(\vec{v}), P_{\vec{w}}(\vec{v})}
\\&=
\abrack{\vec{v}- P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v})}
+ \abrack{\vec{v}- P_{\vec{w}}(\vec{v}), P_{\vec{w}}(\vec{v})}
+ \abrack{P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v})}
+ \abrack{P_{\vec{w}}(\vec{v}), P_{\vec{w}}(\vec{v})}
\\&= \abrack{\vec{v}- P_{\vec{w}}(\vec{v}) + P_{\vec{w}}(\vec{v}), \vec{v} - P_{\vec{w}}(\vec{v}) + P_{\vec{w}}(\vec{v}) }
\\&= \abrack{\vec{v}, \vec{v}}
\\&= \|\vec{v}\|^2
\end{aligned}
\end{equation*}

Since \(\vec{v} - P_{\vec{w}}(\vec{v}) \in ker(P_{\vec{w}})\).
\end{example}

Furthermore, since \(U^2 = I\) and the inverse is unique, \(U^* = U = U^{-1}\).

Let \(\vec{x}\) and \(\vec{y}\) be arbitrary vectors of equal length. Then we want a
transformation that maps \(\vec{x}\) to \(\vec{y}\) and vice versa.
Now in the real world, \(\vec{x} -\vec{y}\) and \(\vec{y} + \vec{x}\)
are orthogonal and hence \(U_{\vec{x} - \vec{y}}\) would suffice. However,
in the complex world lets first try \(\vec{w} = \vec{x} - \sigma\vec{y}\)
where

\begin{equation*}
\sigma = \begin{cases}
1 &\quad\text{if } \vec{x} \perp \vec{y}\\
\frac{\overline{\abrack{\vec{x}, \vec{y}}}}{\left|\abrack{\vec{x}, \vec{y}}\right|} &\quad\text{if } \vec{x} \not\perp \vec{y}
\end{cases}
\end{equation*}

\begin{example}[{Discussion}]
Lets leave \(\sigma \in \mathbb{C}\) yet to be specified. Then

\begin{equation*}
U_{\vec{w}}(\vec{w}) = - \vec{w}
\implies
U_{\vec{w}}(\vec{x}) - \sigma U_{\vec{w}}(\vec{y}) = -\vec{x} + \sigma\vec{y}
\end{equation*}

Now, recall that \(U_{\vec{w}}(\vec{x}) = -\vec{x} \iff \vec{x} = k\vec{w}\) which would imply
that \(\vec{x} = k'\vec{y}\) which would be boring. Instead, we want \(U_{\vec{w}}(\vec{x}) = \sigma\vec{y}\).
We would now work backwards to get a value for \(\sigma\).

\begin{equation*}
\begin{aligned}
&\sigma\vec{y}
 = U_{\vec{w}}(\vec{x})
 = \vec{x} - 2P_{\vec{w}}(\vec{x})
 = \vec{x} - 2\frac{\abrack{\vec{w}, \vec{x}}}{\abrack{\vec{w}, \vec{w}}} (\vec{x} - \sigma\vec{y})
\\&\iff
 \vec{0} = \left(1 - 2\frac{\abrack{\vec{w}, \vec{x}}}{\abrack{\vec{w}, \vec{w}}}\right)(\vec{x} - \sigma\vec{y})
\end{aligned}
\end{equation*}

Again, we want to study the non-degenerate case when \(\vec{x} \neq k \vec{y}\). So

\begin{equation*}
\begin{aligned}
&2\frac{\abrack{\vec{w}, \vec{x}}}{\abrack{\vec{w}, \vec{w}}} = 1
\\&\iff 2\abrack{\vec{w}, \vec{x}} = \abrack{\vec{w}, \vec{w}}
\\&\iff 2\abrack{\vec{x} - \sigma\vec{y}, \vec{x}} = \abrack{\vec{x} - \sigma\vec{y}, \vec{x} - \sigma\vec{y}}
\\&\iff 2\abrack{\vec{x}, \vec{x}} - 2\overline{\sigma}\abrack{\vec{y}, \vec{x}} =
    \abrack{\vec{x},\vec{x}} - \overline{\sigma}\abrack{\vec{y}, \vec{x}}
    - \sigma\abrack{\vec{x}, \vec{y}} + \sigma\overline{\sigma}\abrack{\vec{y}, \vec{y}}
\end{aligned}
\end{equation*}

Now, we would restrict \(|\sigma| = 1\), then

\begin{equation*}
\begin{aligned}
&2\frac{\abrack{\vec{w}, \vec{x}}}{\abrack{\vec{w}, \vec{w}}} = 1
\\&\iff 2\abrack{\vec{x}, \vec{x}} - 2\overline{\sigma}\abrack{\vec{y}, \vec{x}} =
    \abrack{\vec{x},\vec{x}} - \overline{\sigma}\abrack{\vec{y}, \vec{x}}
    - \sigma\abrack{\vec{x}, \vec{y}} + \abrack{\vec{y}, \vec{y}}
\\&\iff -\overline{\sigma}\abrack{\vec{y}, \vec{x}} = -\sigma\abrack{\vec{x}, \vec{y}} \in \mathbb{R}
\end{aligned}
\end{equation*}

Then, \(\sigma = \frac{\overline{\abrack{\vec{x}, \vec{y}}}}{\left|\abrack{\vec{x}, \vec{y}}\right|}\) since
we want its modulus to be \(1\). Also, notice that if \(\vec{x} \perp \vec{y}\)
we can choose \(\sigma\) to be any complex number on the unit circle.
And we get that

\begin{equation*}
U_{\vec{x} - \sigma\vec{y}}(\vec{x}) = \sigma \vec{y}
\end{equation*}

\begin{admonition-important}[{}]
During class sir took the negation of \(\sigma\) taken here. I am unsure as to why he did that.
\end{admonition-important}
\end{example}

\subsection{Cool}
\label{develop--math3273:inner-product-space:page--orthogonal-projections.adoc---cool}

Suppose that our vector space was \(\mathbb{C}^n\) and we have normalized vector \(\vec{u}\).
Then, what if we wanted an orthonormal basis containing \(\vec{u}\). We can recall
the following two facts

\begin{itemize}
\item If a matrix \(U\) is unitary, its columns give an orthonormal basis
\item If \(U\) is a householder matrix, its an isometry (and hence also unitary) and \(U^* = U\).
\end{itemize}

Then, using this information, it would suffice to find a vector \(\vec{w}\) such that \(U_{\vec{w}}\)
contains \(\vec{u}\). Now, suppose that it is the first vector column in \(U_{\vec{w}}\),
then

\begin{equation*}
U_{\vec{w}} \vec{u} = U_{\vec{w}}^*\vec{u} = \overline{e}_1
\end{equation*}

Now, doing this is a bit hard, but we actually have a very similar result. If we let
\(\sigma = \frac{\overline{\abrack{\vec{u}, \vec{e}_1}}}{\left|\abrack{\vec{u}, \vec{e}_1}\right|}\) and
\(\vec{w} = \vec{u} - \sigma \vec{e}_1\), we get that

\begin{equation*}
U_{\vec{w}}\vec{u} = \sigma \vec{e}_1
\end{equation*}

Therefore, since multiplication by \(\overline{\sigma}\) does not affect the fact that the matrix is unitary,
we can let our orthonormal basis be the columns of

\begin{equation*}
\sigma U_{\vec{w}}
\end{equation*}
\end{document}
