\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Groups}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
The following table show the properties of the algebraic structures:
groupoids, semigroups, monoids, groups. Let \(G\) be a
non-empty set

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Property} & {Definition} & {Groupoid} & {Semigroup} & {Monoid} & {Group} \\
\hline[\thicktableline]
{Closure} & {\(\star:G \times G \to G\)} & {{\checkmark}} & {{\checkmark}} & {{\checkmark}} & {{\checkmark}} \\
\hline
{Associative} & {\((a\star b)\star c = a\star(b\star c)\)} & {} & {{\checkmark}} & {{\checkmark}} & {{\checkmark}} \\
\hline
{Identity} & {\(\exists e\in G: \forall a \in G: a\star x = e\star a = a \)} & {} & {} & {{\checkmark}} & {{\checkmark}} \\
\hline
{Inverse} & {\(\forall a \in G: \exists a^{-1} \in G: a\star a^{-1} = a^{-1}\star a = e \)} & {} & {} & {} & {{\checkmark}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Furthermore, a group is called

\begin{itemize}
\item \emph{abelian} iff \(\star\) is commutative.
\item \emph{trivial} iff \(G = \{e\}\) (the singleton of the identity).
    Additionally, this is the smallest subgroup.
\item \emph{finite} iff \(|G|\) is finite and \emph{infinite} otherwise.
\end{itemize}

Note: for the rest of this section, groups are used unless otherwise
stated. Also, for brevity sake, \(a\star b\) would be written
as \(ab\) instead. Additionally, we we define powers of an
element as

\begin{equation*}
a^n = \begin{cases}
        a \star a^{n-1} \quad&\text{if } n > 0\\
        e \quad&\text{if } n = 0\\
        a^{-1} \star a^{n+1} \quad&\text{if } n < 0
    \end{cases}
\end{equation*}

Notice that powers are well defined since \(\star\) is
associative and both the inverse and identity are unique (as discussed
below). Furthermore, it is not hard to show that

\begin{itemize}
\item \((a^{n})^{-1} = a^{-n}\)
\item \((a^n)^m = a^{nm}\)
\item \(a^na^m = a^{n+m}\)
\end{itemize}

\section{Nice properties}
\label{develop--math2272:ROOT:page--groups/index.adoc---nice-properties}

\subsection{Uniquness of Identity}
\label{develop--math2272:ROOT:page--groups/index.adoc---uniquness-of-identity}

\begin{example}[{Proof}]
Let \(e_1\) and \(e_2\) be identity elements in
\(G\), then

\begin{equation*}
e_1 = e_2 e_1 = e_2
\end{equation*}

 ◻
\end{example}

\subsection{Uniquness of Inverse}
\label{develop--math2272:ROOT:page--groups/index.adoc---uniquness-of-inverse}

\begin{example}[{Proof}]
Suppose, \(b\) and \(c\) are inverses of
\(a \in G\), then

\begin{equation*}
b = be = b(ac) = (ba)c = ec = c
\end{equation*}

 ◻
\end{example}

\subsection{Special cases of inverse}
\label{develop--math2272:ROOT:page--groups/index.adoc---special-cases-of-inverse}

Let \(a,b \in G\), then

\begin{itemize}
\item \((a^{-1})^{-1} = a\).
\item \((ab)^{-1} = b^{-1}a^{-1}\)
\end{itemize}

Both of these could be verified using the definition of the inverse and
its uniqueness.

\subsection{Cancellation property}
\label{develop--math2272:ROOT:page--groups/index.adoc---cancellation-property}

Let \(a, b, c \in G\) where \(ab = ac\) or
\(ba=ca\), then \(b=c\). This property is proven by
either pre or post multiplying by \(a^{-1}\).

\section{Order}
\label{develop--math2272:ROOT:page--groups/index.adoc---order}

The \emph{order} of a group \(G\) is simply its cardinality.

The \emph{order} of an element \(g \in G\) is the least positive
power of \(g\) that is equal to the identity. If no such power
exists, \(g\) is said to have infinite order.

\subsection{Order of product}
\label{develop--math2272:ROOT:page--groups/index.adoc---order-of-product}

\begin{admonition-important}[{}]
The information presented here is from
\href{https://kconrad.math.uconn.edu/blurbs/grouptheory/order.pdf}{"Orders of Elements in a Group" by Keith Conrad}
\end{admonition-important}

What is the order of the product of two elements in a group? In general, not much could be said. In fact,
the order of two elements may be finite but the order of their product could be infinite. Additionally,
two elements could both have order \(p\) but their product has order \(q\) (both prime).

However, if the two elements commute, we can place a trivial upper bound on the order of their product.
That is, \(ord(ab) | ord(a)ord(b)\). Furthermore, if the orders of the two elements are relatively prime,
we can conclude that \(ord(ab) = ord(a)ord(b)\)

\section{Subgroups}
\label{develop--math2272:ROOT:page--groups/index.adoc---subgroups}

A \emph{subgroup} of \((G, \star)\) is a pair
\((H, \star)\) that is also a group where
\(H \subseteq G\). Note that \(H \neq \varnothing\).

\subsection{One step test}
\label{develop--math2272:ROOT:page--groups/index.adoc---one-step-test}

\((H, \star)\) is a subgroup of \((G, \star)\) iff
\(\forall a, b \in H: a b^{-1} \in H\).

\begin{example}[{Proof}]
Clearly, the forward direction is true by the closure and existence of
an inverse. Then, suppose
\(\forall a, b \in H: a b^{-1} \in H\).

\begin{itemize}
\item Let \(b=a\). Then by the definition of the inverse,
    \(aa^{-1} = e \in H\).
\item Let \(a=e\). Then \(eb^{-1} = b \in H\).
\item Inherited from \(G\).
\item Let \(c \in G\) and \(b = c^{-1}\). Then
    \(a(c^{-1})^{-1} = ac \in H\).
\end{itemize}

 ◻
\end{example}

\subsection{Two step test}
\label{develop--math2272:ROOT:page--groups/index.adoc---two-step-test}

\((H, \star)\) is a subgroup of \((G, \star)\) iff
\(\forall a, b \in H: a b \in H \land a^{-1} \in H\).

\begin{example}[{Proof}]
The forward direction is clearly true by closure and existence of an
inverse. Also, the reverse direction follows from the one step test
since

\begin{equation*}
b^{-1} \in H \implies ab^{-1} \in H
\end{equation*}

 ◻
\end{example}

\subsection{Finite equality subgroup test}
\label{develop--math2272:ROOT:page--groups/index.adoc---finite-equality-subgroup-test}

If \(H\) is a (non-empty) finite subset of \(G\),
then \((H, \star)\) is a subgroup of \((G, \star)\)
iff \(H \star H = H\), where

\begin{equation*}
A\star B = \{a \star b\mid a \in A, b \in B\}
\end{equation*}

NB: I came up with that name on my own.

\begin{example}[{Proof}]
For the forward direction, notice that
\(H\star H \subseteq H\) by closure of \(\star\),
while \(H \subseteq H\star H\) since,

\begin{equation*}
\forall h \in H: h = h\star e \in H \star H
\end{equation*}

where \(e\) is the identity.

For the reverse direction, firstly, notice that \(H\) is
closed under star since \(H \star H = H\). Also, it inherits
the associative from \(G\). Now, let \(n= |H|\)
(since \(H\) is finite). Then, we can write
\(H = \{h_1, h_2, \ldots, h_n\}\) and consider the following
sequence,

\begin{equation*}
h_i\star h_1, h_i\star h_2, \ldots h_i\star h_n \in H
\end{equation*}

where \(h_i\) is an arbitrary element in \(H\).
Then, by the cancellation property, all of these \(n\)
elements must be unique. Furthermore, since there are exactly,
\(n\) elements in \(H\), at least one of these must
be equal to \(h_i\). That is
\(\exists h_j \in H: h_i\star h_j = h_i\). Again, by the
cancellation property, since \(h_ih_j = h_i = h_ie\) (where
\(e \in G\)), then \(e = h_j\) and
\(e\in H\).

Finally, consider the same sequence. Then, by the same logic,
\(\exists h_k \in H:
    h_i \star h_k = e\). Then, by the cancellation property,
\(h_k = h_i^{-1} \in H\).

 ◻
\end{example}

\subsection{Finite subgroup test}
\label{develop--math2272:ROOT:page--groups/index.adoc---finite-subgroup-test}

If \(H\) is a (non-empty) finite subset of \(G\),
then \((H, \star)\) is a subgroup of \((G, \star)\)
iff \(\star\) is closed under \(\star\) (ie
\(H\star H \subseteq H\)).

NB: I believe this is equivalent to the `finite equality subgroup test'
and the same proof can be applied but I saw the proof as presented
below.

\begin{example}[{Proof}]
Note that the forward direction is assured by the `finite equality
subgroup test.' Now, for the reverse direction, we only need to show
that \(\forall h \in H: h^{-1} \in H\) and the result will
follow by the `two step test.' Now, consider an arbitrary
\(h \in H\). Then,

\begin{itemize}
\item If \(a = e\), \(a^{-1} = e\in H\).
\item If \(a \neq e\) consider the sequence
    \(a, a^2, \ldots\). Then, by closure this sequence is a subset
    of \(H\) and since \(H\) is finite, not all of the
    elements of this sequence can be distinct. Say
    \(a^{i} = a^{j}\) where \(i > j\) then
    
    \begin{equation*}
    a^{i} = a^{i-j}a^{j} = a^j = e a^j
    \end{equation*}
    
    and by the cancellation property, \(a^{i-j} = e\) and since
    \(a \neq e\), \(i-j > 1 \implies a-j-1 \geq 1\).
    Then \(a^{i-j} = a^{i-j-1}a = e = a^{-1}a\) and again by the
    cancellation property, \(a^{-1} = a^{i-j-1} \in H\) since
    \(a-j-1 \geq 1\) and we are done.
\end{itemize}

 ◻
\end{example}

\section{Cayley’s Theorem}
\label{develop--math2272:ROOT:page--groups/index.adoc---cayleys-theorem}

\begin{admonition-note}[{}]
This section is to be moved.
\end{admonition-note}

Cayley’s theorem asserts that every
group \(G\) is isomorphic to a subgroup of its symmetric group
\(S(G)\).

\begin{example}[{Proof}]
We simply need to find an injective function \(f: G \to S(G)\)
such that its image forms a subgroup of \(S(G)\) and group
operations are ''preserved''. We define

\begin{equation*}
f(x) = g_x \quad\text{where }g_x: G \to G \text{ defined by } g_x(t) = xt
\end{equation*}

Clearly, \(f\) is injective and \(g_x\) is
bijective. Additionally

\begin{equation*}
f(xy)(t) = g_{xy}(t) = xyt = g_x(yt) = g_x(g_y(t)) = (f_x \circ f_y)(t)
\end{equation*}

so \(G\) is isomorphic to the range of \(S(G)\)
which forms a subgroup since group operations are preserved.

 ◻
\end{example}
\end{document}
