\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Quadratic residues}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

{} \def\leg#1#2{\left(\frac{#1}{#2}\right)}
% Title omitted
Let \(p \in \mathbb{Z}^+\) and \(a \in \mathbb{Z}\) such that \(p \nmid a\). Then, \(a\)
is a \emph{quadradic residue} modulo \(p\) iff \(\exists b \in \mathbb{Z}\) such that
\(a \equiv b^2 \mod p\). Furthermore we define the \emph{legendre symbol} as

\begin{equation*}
\leg{a}{p} =
\begin{cases}
-1, &\text{if } a \text{ is not a quadratic residue}\\
\ 1, &\text{if } a \text{ is a quadratic residue}
\end{cases}
\end{equation*}

Now, for the rest of this page we would let \(p\) be an odd prime (no fun when \(p=2\)).
Then, their are exactly \(\frac{p-1}{2}\) quadratic residues modulo \(p\)
which can be generated by the set \(\left\{1,2\ldots\frac{p-1}{2}\right\}\).

\begin{example}[{Proof}]
Firstly, notice that if \(b^2 \equiv a \mod p\), \((p-b)^2 \equiv a \mod p\)
and hence there are at most \(\frac{p-1}{2}\) quadratic residues since \(b \neq p-b\)
since \(p\) is odd. Next, consider

\begin{equation*}
x,y \in \left\{1,2,\ldots \frac{p-1}{2}\right\}
\end{equation*}

then

\begin{equation*}
x^2 \equiv y^2 \mod p
\implies (x+y)(x-y) \equiv 0 \mod p
\implies p | (x+y)(x-y)
\implies p | x-y \ \text{or} \ p | x+y
\end{equation*}

Notice that \(2 \leq x+y \leq p-1\) so \(p \nmid x+y\)
and hence \(p | x-y\). Therefore \(x \equiv y \mod p\)
and \(x = y\). Therefore the number of quadratic residues is
at least \(\frac{p-1}{2}\) and hence there are exactly \(\frac{p-1}{2}\) quadratic residues which
are generated by the aforementioned set.
\end{example}

Next, we can actually compute \(\leg{a}{p}\) as

\begin{equation*}
\leg{a}{p} \equiv a^{\frac{p-1}{2}} \mod p
\end{equation*}

and as a result \(a\to \leg{a}{p}\) is multiplicative for all \(a,b \in U_p\).

\begin{example}[{Proof}]
Let \(b\)
be a primitive root of \(\mathbb{Z}/p\mathbb{Z}\) and firstly note
that

\begin{equation*}
b^{p-1} \equiv 1 \mod p
\implies \left(b^{\frac{p-1}{2}} - 1\right)\left(b^{\frac{p-1}{2}} + 1\right) \equiv 0 \mod p
\implies
b^{\frac{p-1}{2}} \equiv -1 \mod p
\end{equation*}

otherwise \(b\) would not have order \(p-1\).
Additionally,
\(\{1, b^2, b^4, \ldots b^{p-1}\}\) are the quadratic residues since there
are exactly \(p-1\) quadratic residues.

Let \(a = b^n\), then we have two cases.

\begin{itemize}
\item Case 1: \(n = 2k\). Then \(\leg{a}{p} = 1\) and
    
    \begin{equation*}
    a^{\frac{p-1}{2}} \equiv b^{2k\frac{p-1}{2}} \equiv (b^{p-1})^k \equiv 1^k \equiv 1 \mod p
    \end{equation*}
\item Case 2: \(n = 2k+1\). Then, \(\leg{a}{p} = -1\) and
    
    \begin{equation*}
    a^{\frac{p-1}{2}} \equiv b^{(2k+1)\frac{p-1}{2}}
    \equiv (b^{p-1})^k b^{\frac{p-1}{2}} \equiv 1^k (-1) \equiv -1 \mod p
    \end{equation*}
\end{itemize}

In both cases, we get the desired result.
\end{example}

\section{Gauss's Lemma}
\label{develop--math2400:modular-arithmetic:page--quadratic-residues.adoc---gausss-lemma}

Firstly let

\begin{equation*}
S = \left\{1,2, \ldots, \frac{p-1}{2}\right\}
\end{equation*}

then there is a bijection from \(S \times \{1,-1\}\) to \(U_p\) defined by

\begin{equation*}
\forall (s, \varepsilon) \in S\times\{1,-1\}: f(s,\varepsilon) = \varepsilon s
\end{equation*}

This is
easily seen since both sets have the same number of elements and the function is injective.

Next, we fix some \(b \in U_p\) and define

\begin{equation*}
S' = \{b s\ : \ s \in S\}
\end{equation*}

Again, there is a bijection from \(S'\times \{1,-1\}\) to \(U_p\) defined by

\begin{equation*}
\forall (s', \varepsilon) \in S'\times\{1,-1\}: g(s',\varepsilon) = \varepsilon s'
\end{equation*}

Again, both sets have the same number of elements, but this time seeing injectivity is a
bit harder. Suppose that \(\varepsilon_1s_1' = \varepsilon_2 s_2'\)

\begin{itemize}
\item If \(\varepsilon_1 \neq \varepsilon_2\), we get \(s_1' = -s_2'\) hence \(-s' \in S'\) and \(-s \in S\)
    which is a contradiction
\item If \(\varepsilon_1 = \varepsilon_2\), we get that \(s_1' = s_2'\) and hence \(s_1 = s_2\) (since \(b \in U_p\))
\end{itemize}

Now, we define \(br = \varepsilon_rs_r\) where \(r \in S\). We want to show that \(\{s_r\} = S\),
ie \(s_1,\ldots s_{\frac{p-1}{2}}\) is a rearrangement of \(1\ldots \frac{p-1}{2}\).
Firstly, notice that

\begin{equation*}
\forall r \in S: \{br, -br\} = \{s_r, -s_r\}
\end{equation*}

Now, if any of the \(s_r\) are equal, we would get that \(\{br_1, -br_1\} = \{br_2, -br_2\}\)
and hence \(r_1 = \pm r_2\). However, \(-r_2 \notin S\) and hence \(r_1 = r_2\). Therefore
each of the \(s_r\) are unique and
\(\{s_r\} = S\).
Furthermore,
\(\{\{s_r, -s_r\}\ :\ r \in S \}\) partition \(U_p\)
and hence

\begin{equation*}
U_p = \{br,-br \ : \ r \in S\} = \{s_r, -s_r \ : \ r \in S\}
\end{equation*}

Now, we are ready. Consider the following product

\begin{equation*}
\begin{aligned}
\left(\frac{p-1}{2}\right)!b^{\frac{p-1}{2}}
&\equiv (1b)(2b)\cdots \left(\frac{p-1}{2}b\right)
    \mod p
\\&\equiv (\varepsilon_1s_1)(\varepsilon_2s_2)\cdots \left(\varepsilon_{\frac{p-1}{2}}s_{\frac{p-1}{2}}\right)
    \mod p
\\&\equiv \left(\prod_{i=1}^{\frac{p-1}{2}} \varepsilon_i\right) \left(\prod_{i=1}^{\frac{p-1}{2}} s_i\right)
    \mod p
\\&\equiv \left(\prod_{i=1}^{\frac{p-1}{2}} \varepsilon_i\right) \left(\frac{p-1}{2}\right)!
    \mod p
\end{aligned}
\end{equation*}

and hence

\begin{equation*}
\leg{b}{p} \equiv b^{\frac{p-1}{2}} \equiv \prod_{i=1}^{\frac{p-1}{2}} \varepsilon_i \mod p
\end{equation*}

This is \emph{Gauss' Lemma}.

\subsection{Understanding}
\label{develop--math2400:modular-arithmetic:page--quadratic-residues.adoc---understanding}

Firstly, lets consider a infinite (one-dimensional array) indexed by the integers.
Then, we could color (label) the \(r\)'th cell of this array as follows. Let \(r = kp + r'\)
and colour the cell

\begin{itemize}
\item black: if \(r =0\)
\item blue: if \(1\leq r \leq \frac{p-1}{2}\)
\item red: if \(\frac{p-1}{2} + 1 \leq r \leq p-1\)
\end{itemize}

Next, lets consider the cells used if we place balls into \(\{br: r\in S\}\) and notice
that none of the cells used are black. Then

\begin{equation*}
\varepsilon_r = \begin{cases}
1 \quad&\text{ if the } br \text{ ball is in a blue cell}\\
-1 \quad&\text{ if the } br \text{ ball is in a red cell}
\end{cases}
\end{equation*}

since if \(br\) lies in a blue cell, it is congruent to \(s_r \mod p\). Otherwise,
if \(br\) is in a red cell, it is congruent to \(p-s_r \mod p\).

Hence, to determine \(\leg{b}{p}\) we only need to know the parity of the number
of elements in red cells.

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math2400/modular-arithmetic/gauss-1}
\caption{Example where \(p=13\) and \(b=4\)}
\end{figure}

\section{Quadratic Reciprocity}
\label{develop--math2400:modular-arithmetic:page--quadratic-residues.adoc---quadratic-reciprocity}

Let \(p\) and \(q\) be odd primes. Then

\begin{equation*}
\leg{p}{q}\leg{q}{p} = (-1)^{\left(\frac{p-1}{2}\right)\left(\frac{q-1}{2}\right)}
\end{equation*}

\begin{admonition-caution}[{}]
The exponent of \(-1\) is simply division, not the legendre symbol.
\end{admonition-caution}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
This is the proof outlined in class
\end{admonition-note}

WLOG let \(q < p\)
Firstly, we define

\begin{equation*}
W = \left\{(a,b) \ | \ 1\leq a \leq \frac{q-1}{2}, \ 1 \leq b \leq \frac{p-1}{2}\right\}
\end{equation*}

Then \(|W| = \left(\frac{p-1}{2}\right)\left(\frac{q-1}{2}\right)\). Next, we define
\(f : W \to \mathbb{Z}\) defined by

\begin{equation*}
f(a,b) = ap - bq
\end{equation*}

Then, notice that

\begin{equation*}
\begin{aligned}
f(a,b) = ap - bq = a'p - b'q = f(a',b')
&\implies (a-a')p - (b-b')q = 0
\\&\implies a\equiv a' \mod q \text{ and } b\equiv b' \mod p
\\&\implies (a,b) = (a',b')
\end{aligned}
\end{equation*}

since \(\gcd(p,q) = 1\) and hence each of \(ap - bq\) is unique.

Next, we define the following sets

\begin{equation*}
P = \left\{(a,b) \in W \ : -\frac{q-1}{2} \leq ap-bq \leq -1\right\}
\end{equation*}

\begin{equation*}
Q = \left\{(a,b) \in W \ : 1 \leq ap - bq \leq \frac{p-1}{2} \right\}
\end{equation*}

Notice that

\begin{equation*}
\begin{aligned}
ap \equiv -a' \mod q
&\iff \exists b \in \left\{1\ldots \frac{p-1}{2}\right\}: ap = -a' + bq
\\&\iff \exists b\in \left\{1\ldots \frac{p-1}{2}\right\}: ap -bq = -a'
\\&\iff \exists b\in \left\{1\ldots \frac{p-1}{2}\right\}: -\frac{q-1}{2} \leq ap - bq \leq -1
\\&\iff (a,b) \in P
\end{aligned}
\end{equation*}

where \(a' \in \{1,2,\ldots\frac{q-1}{2}\}\).
Also, notice that such a \(b\) would be unique. Hence,
by gauss' lemma \(\leg{p}{q} = (-1)^{|P|}\). However, we actually
need to show that such a \(b\) exists in our desired range.

\begin{example}[{Details}]
Notice that

\begin{equation*}
-\frac{q-1}{2} \leq ap - bq \leq -1
\iff 1 \leq bq -ap \leq \frac{q-1}{2}
\iff \frac{1 +ap}{q} \leq b \leq \frac{\frac{q-1}{2} + ap}{q}
\end{equation*}

Also

\begin{equation*}
\begin{aligned}
p > q
&  \implies pq -q -2ap -2 \geq pq - q -(q-1)p -2 = p-q - 2 \geq 0
\\& \implies 2ap + 2 \leq pq-q
\\& \implies \frac{1+ap}{q} \leq \frac{p-1}{2}
\end{aligned}
\end{equation*}

also

\begin{equation*}
\begin{aligned}
p > q
&\implies p + p = 2p > q + 1
\\&\implies ap \geq p > \frac{q+1}{2}
\\&\implies \frac{q-1}{2} +ap > q
\\&\implies \frac{\frac{q-1}{2} + ap}{q} > 1
\end{aligned}
\end{equation*}

Therefore, \(\exists b\) such that

\begin{equation*}
1 \leq b \leq \frac{\frac{q-1}{2} +ap}{q}
\end{equation*}

and \(\exists b'\) such that

\begin{equation*}
\frac{1+ap}{q} \leq b' \leq \frac{p-1}{2}
\end{equation*}

Then since \(\frac{1+ap}{q} \leq \frac{\frac{q-1}{2} + ap}{q}\)
there must exist a \(b\) which lies between them.
\end{example}

Next,

\begin{equation*}
\begin{aligned}
bq \equiv -b' \mod p
&\iff \exists a \in \left\{1,\ldots \frac{q-1}{2}\right\}: bq = -b' +ap
\\&\iff \exists a \in \left\{1,\ldots \frac{q-1}{2}\right\}: ap-bq = b'
\\&\iff \exists a \in \left\{1,\ldots \frac{q-1}{2}\right\}: 1 \leq ap-bq \leq \frac{p-1}{2}
\\&\iff (a,b) \in Q
\end{aligned}
\end{equation*}

where \(b' \in \{1,2,\ldots \frac{p-1}{2}\}\). Again, such a
\(a\) would be unique and by gauss's lemma \(\leg{q}{p} = (-1)^{|Q|}\).
We need to actually show that such an \(a\) exists in our desired range.

\begin{example}[{Details}]
Notice that

\begin{equation*}
1 \leq ap-bq \leq \frac{p-1}{2}
\iff
\frac{1+bq}{p} \leq a \leq \frac{\frac{p-1}{2} + bq}{p}
\end{equation*}

Also,

\begin{equation*}
0\leq (p+1)(q-2) = pq +q -2p -2
\implies 2 + pq-q \leq 2pq -2p
\implies 1 + bq \leq 1 + \frac{p-1}{2}q \leq pq - p
\implies \frac{1+bq}{p} \leq \frac{q-1}{2}
\end{equation*}

Also, suppose that \(\frac{p-1}{2} +bq < p\), then

\begin{equation*}
q \leq bq < \frac{p+1}{2} \implies 1\leq bq \leq \frac{p-1}{2}
\implies bq \not\equiv -b' \mod p
\end{equation*}

Therefore, \(\frac{\frac{p-1}{2} + bq}{p} \geq 1\). Hence, in a similar manner
as before, we are done.
\end{example}

Then, since \(P \cap Q = \varnothing\), all that remains is to show that \(|P\cup Q| \equiv |W| \mod 2\)
which we would use to show that

\begin{equation*}
\leg{p}{q}\leg{q}{p} = (-1)^{|P|}(-1)^{|Q|} = (-1)^{|P\cup Q|} = (-1)^{|W|}
= (-1)^{\left(\frac{p-1}{2}\right)\left(\frac{q-1}{2}\right)}
\end{equation*}

Then notice that

\begin{equation*}
P \cup Q = \left\{(a,b) \in W \ : \ -\frac{q-1}{2} \leq ap-bq \leq \frac{p-1}{2}\right\}
\end{equation*}

since there is no \((a,b) \in W\) where \(ap-bq = 0\). Now, consider arbitrary
\((a,b) \in W\) and let \((a',b') = \left(\frac{q-1}{2} -a + 1, \frac{p-1}{2} - b + 1\right)\)
which we see is in \(W\). Then

\begin{equation*}
a'p - b'q
= \frac{pq - p}{2} -ap +p - \frac{pq-q}{2} +bq -q
= \frac{p-q}{2} - (ap-bq)
\end{equation*}

Then

\begin{equation*}
\begin{aligned}
(a,b) \in P \cup Q
&\iff -\frac{q-1}{2} \leq ap-bq \leq \frac{p-1}{2}
\\&\iff -\frac{q-1}{w} = \frac{p-q}{2} - \frac{p-1}{2} \leq  a'p-b'q \leq \frac{p-q}{2} + \frac{q-1}{2} = \frac{p-1}{2}
\\&\iff (a',b') \in P\cup Q
\end{aligned}
\end{equation*}

And as a result \((a,b) \notin P\cup Q \iff (a',b')\notin P\cup Q\). Furthermore

\begin{equation*}
\begin{aligned}
(a,b) = (a',b')
&\implies a = \frac{q+1}{4} \text{ and } b = \frac{p+1}{4}
\\&\implies ap-bq = \frac{p-q}{4} < \frac{p-1}{2}
\\&\implies (a,b) \in P \cup Q
\end{aligned}
\end{equation*}

and as a result \((a,b) \notin P \cup Q\) implies that \((a,b) \neq (a',b')\).
Therefore, we have paired the elements outside of \(P \cup Q\) since the transformation
is an involution and not equal to its image since its outside of \(P\cup Q\).
Therefore \(|W \cap (P\cup Q)^c| \equiv 0 \mod 2\) and we are done.
\end{example}

\section{Determining for which \(p\), \(a\) is a quadratic residue}
\label{develop--math2400:modular-arithmetic:page--quadratic-residues.adoc---determining-for-which-latex-backslashp-latex-backslash-latex-backslasha-latex-backslash-is-a-quadratic-residue}

From quadratic reciprocity, if \(a\) is the product of odd primes, we
can determine \(\leg{a}{p}\) as

\begin{equation*}
\leg{a}{p}
= \prod_i \leg{q_i}{p}
= \prod_{i} (-1)^{\left(\frac{p-1}{2}\right)\left(\frac{q_i-1}{2}\right)} \leg{p}{q_i}
= \left((-1)^{\sum_{i} \frac{q_i-1}{2}}\right)^{\frac{p-1}{2}} \prod_{i} \leg{p}{q_i}
\end{equation*}

Then, for any fixed \(a\), \(\leg{a}{p}\) depends on \(\frac{p-1}{2} \mod 2\)
and \(p \mod q_i\) for each \(i\). Then, since \(lcm\{q_i\} | a\), and \(\frac{p-1}{2} \mod 2\)
depends on \(p \mod 4\), we get that \(\leg{a}{p}\) simply depends on
\(p \mod 4a\).

We still have two more things to discuss. If \(a\) is negative or a multiple of \(2\). If \(a\)
is negative, since \(\leg{-1}{p}\) depends on \(p\mod 4\), this is already captured above. However, we need to
determine what \(\leg{2}{p}\) is. Notice that we get our necessary negative coefficient
iff

\begin{equation*}
\frac{p-1}{2} +1 \leq 2r \leq p-1
\end{equation*}

Let \(p = 4m+1\), then

\begin{equation*}
2m + 1 \leq 2m+2 \leq 2r \leq 4m \iff m+1 \leq r \leq 2m
\end{equation*}

Hence, we have \(\leg{2}{p} = (-1)^m = (-1)^{\frac{p-1}{4}}\). Otherwise, if \(p=4m+3\),

\begin{equation*}
2m +1 \leq 2m+2 \leq 2r \leq 4m \iff m+1 \leq r \leq 2m
\end{equation*}

Hence, we get that \(\leg{2}{p} = (-1)^m = (-1)^{\frac{p-3}{4}}\)

Then, consider our table \(\mod 8\)

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(p\mod 8\)} & {\(\leg{2}{p}\)} \\
\hline[\thicktableline]
{\(1\)} & {\(1\)} \\
\hline
{\(3\)} & {\(-1\)} \\
\hline
{\(5\)} & {\(-1\)} \\
\hline
{\(7\)} & {\(1\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Hence,

\begin{equation*}
\leg{2}{p} = (-1)^{\frac{p^2-1}{8}}
\end{equation*}

\begin{admonition-important}[{}]
Yes, I too am unable to manually come up with this equation. But it does work.
\end{admonition-important}

Therefore, we could now have a complete solution for \(\leg{a}{p}\) where \(a = \pm 2^k \prod_{i=1}^n q_i^{\alpha_i}\)
where the \(q_i\) are odd primes.

\begin{equation*}
\begin{aligned}
\leg{a}{p}
&= \leg{a/|a|}{p} \leg{2}{p}^k \prod_{i=1}^n \leg{q_i}{p}^{\alpha_i}
\\&= \left((-1)^{\frac{p-1}{2}}\right)^{\frac{1 - a/|a|}{2}}
    \left((-1)^{\frac{p^2-1}{8}}\right)^k
    \prod_{i=1}^n \left((-1)^{\left(\frac{p-1}{2}\right)\left(\frac{q_i -1}{2}\right)}\right)^{\alpha_i} \leg{p}{q_i}^{\alpha_i}
\\&= (-1)^{\frac{p-1}{2}\left(\frac{1 - a/|a|}{2} + k\frac{p+1}{4} + \sum_{i=1}^n \alpha_i \frac{q_i-1}{2}\right)} \prod_{i=1}^n \leg{p}{q_i}^{\alpha_i}
\end{aligned}
\end{equation*}

Therefore \(\leg{a}{p}\) depends on \(p \mod \prod_{i=1}^n q_i^{\alpha_i}\) and \(p \mod 4\) and if \(a\) is even \(p \mod 8\).
Therefore, \(\leg{a}{p}\) depends on \(p \mod 4|a|\) just as before. However, we can greatly optimize this as we only need to
care if \(\alpha_i\) and \(k\) is \(0\) or \(1\). That is, we can exclude the terms of \(k\) or \(\alpha_i\)
if they are even. Then, say \(a'\) is the product of distinct primes (and negative one) which have odd power in \(a\),
we get that \(\leg{a}{p}\) depends only on \(p\mod 4|a'|\). We can then easily compute the necessary symbols \(\leg{p}{q_i}\)
to determine our result for odd \(p \mod 4|a'|\).
\end{document}
