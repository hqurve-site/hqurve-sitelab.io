\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Heat Flow Problems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In this section, we will study models by looking at the flow of heat in the system

\section{Laws}
\label{develop--math6192:ROOT:page--heat-flow.adoc---laws}

\begin{theorem}[{Convection diffusion equation}]
\begin{equation*}
(\vec{v}\cdot\nabla) T + \frac{\partial T}{\partial t} = \kappa \nabla^2 T
\end{equation*}

where \(T\) is the temperature of the medium, \(\vec{v}\)
is the velocity and \(\kappa\) is the thermal diffusivity of the medium.
\end{theorem}

\subsection{Derivation of convection diffusion equation}
\label{develop--math6192:ROOT:page--heat-flow.adoc---derivation-of-convection-diffusion-equation}

Let \(\vec{q}_n\) be the heat flux in a given direction
and \(\lambda\) be the heat conductivity of the medium
then,

\begin{equation*}
\vec{q}_n = -\lambda \nabla T
\end{equation*}

where \(T\) is the temperature.
Also, the total heat \(Q\) in a volume can be modeled as

\begin{equation*}
Q = \iiint_V \rho C_p T\ dV
\end{equation*}

where \(\rho\) is the density and \(C_p\) is the heat capacity of the medium.

Then, we can determine the change in total heat \(Q\) flowing through volume \(V\) in two ways,

\begin{equation*}
\begin{aligned}
\iiint_{V} \frac{d}{d t}(\rho C_p T) \ dV
=
\frac{d Q}{d t}
&= -\iint_S \vec{q}_n \cdot dS
\\&= \iint_S \lambda \nabla T \cdot dS
\\&= \iiint_V \nabla \cdot(\lambda \nabla T)\ dV
\end{aligned}
\end{equation*}

by the Gauss divergence theorem.
Therefore, since the above must hold for all volumes \(V\),

\begin{equation*}
\frac{d}{d t}(\rho C_p T) = \nabla\cdot (\lambda \nabla T)
\end{equation*}

When \(\rho\), \(C_p\) and \(\lambda\) are all constant, this reduces to

\begin{equation*}
\frac{d T}{dt} = \kappa \nabla^2 T
\end{equation*}

where \(\kappa = \frac{\lambda}{\rho C_p}\) is the thermal diffusivity of the material.
By expanding the left hand side using the chain rule, we get

\begin{equation*}
(\vec{v}\cdot\nabla) T + \frac{\partial T}{\partial t} = \kappa \nabla^2 T
\end{equation*}

\section{Application: heat sink on rod}
\label{develop--math6192:ROOT:page--heat-flow.adoc---application-heat-sink-on-rod}

Suppose we have an infinite rod at initial constant temperature \(T_0\)
and we attach a heat sink to one end.
The heat sink is kept at constant temperature \(T_1\).
We wish to determine the temperature of the rod.

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/heat-sink}
\end{figure}

Assumptions:

\begin{itemize}
\item The temperature of the rod is constant at each cross section
    and hence the temperature can be modelled as a function of \(x\)
    and \(t\) only.
\item The heat sink at the boundary is kept at temperature \(T_1\)
    for all time.
\end{itemize}

Since we are modelling \(x\) only and there is no fluid flow,
the convection-diffusion equation reduces to

\begin{equation*}
\frac{\partial T}{\partial t} = \kappa \frac{\partial^2 T}{\partial x^2}
\end{equation*}

Also, we have the following boundary conditions

\begin{itemize}
\item \(T(0,t) = T_1\)
\item \(T(x,0) = T_0\).
\end{itemize}

We will assume that \(T = f(\eta)\) where \(\eta = \frac{x}{\sqrt{\kappa t}}\).
Then, the governing equation reduces to

\begin{equation*}
f'(\eta) \frac{-x\kappa}{2(\kappa t)^{3/2}}
= \kappa \frac{\partial}{\partial x}\left[f'(\eta) \frac{1}{\sqrt{kt}}\right]
= \kappa f''(\eta) \frac{1}{kt}
\implies f''(\eta) = -\frac{1}{2}\eta f'(\eta)
\end{equation*}

with boundary conditions \(f(0) = T_1\) and \(f(\infty) = T_0\).
We solve the ODE to get

\begin{equation*}
f(\eta) = A + B\int_0^{\eta /2} \exp\left(-s^2\right) \ ds
\end{equation*}

By applying the boundary conditions, we get that

\begin{equation*}
f(\eta)
= T_1 + (T_0 - T_1) \frac{2}{\sqrt{\pi}} \int_0^{\eta/2} \exp\left(-s^2\right) \ ds
= T_1 + (T_0 - T_1) \erf\left(\frac{\eta}{2}\right)
\end{equation*}

Therefore,

\begin{equation*}
T(x,t) = T_1 + (T_0 - T_1) \erf\left(\frac{x}{2\sqrt{\kappa t}}\right)
\end{equation*}

\subsection{Heat sink at oscillating temperature}
\label{develop--math6192:ROOT:page--heat-flow.adoc---heat-sink-at-oscillating-temperature}

Suppose instead that \(T_1(t) = T_0\exp(-i\omega t)\).
Then, we instead assume a form of

\begin{equation*}
T(x,t) = \exp(-i\omega t)f(x)
\end{equation*}

By substituting into the governing equation, we get

\begin{equation*}
-i\omega f = \kappa f''
\end{equation*}

which has solution

\begin{equation*}
\begin{aligned}
f
&=
A\exp\left(x\sqrt{\frac{\omega}{\kappa}}\sqrt{-i}\right)
+ B\exp\left(-x\sqrt{\frac{\omega}{\kappa}}\sqrt{-i}\right)
\\
&= A\exp\left(x(1-i)\sqrt{\frac{\omega}{2\kappa}}\right)
+ B\exp\left(x(i-1)\sqrt{\frac{\omega}{2\kappa}}\right)
\end{aligned}
\end{equation*}

Since \(T\) should be bounded as \(x\to \infty\), \(A = 0\).
Also, at \(x=0\), \(f=1\).
So,

\begin{equation*}
T(x,t)
= T_0\exp(-i\omega t)\exp\left(x(i-1)\sqrt{\frac{\omega}{2\kappa}}\right)
= T_0\exp\left(-x\sqrt{\frac{\omega}{2\kappa}}\right)\exp\left[i\left(x\sqrt{\frac{\omega}{2\kappa}} - \omega t\right)\right]
\end{equation*}

This is a travelling thermal wave with decaying amplitude
\(T_0\exp\left(-x\sqrt{\frac{\omega}{2\kappa}}\right)\)
and speed

\begin{equation*}
v = \frac{\omega}{\sqrt{\frac{\omega}{2\kappa}}} = \sqrt{2\omega\kappa}
\end{equation*}

\section{Rayleigh-Bénard Problem}
\label{develop--math6192:ROOT:page--heat-flow.adoc---rayleigh-bénard-problem}

When there is a thin fluid which is heated from bellow,
convection currents form.
Depending on the conditions of the system, \emph{Bénard cells} form.
See \url{https://www.youtube.com/watch?v=gSTNxS96fRg}.

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-math6192/ROOT/rayleigh-benard}
\caption{Illustration of convection current}
\end{figure}

We will assume the following

\begin{itemize}
\item Everything happens in the x-z plane only
\item \(\rho\) is constant in the continuity equation
\item \(\rho\) is a linear function of temperature.
    \(\rho = \rho_0[1-\beta(T-T_0)]\)
\end{itemize}

We also use free-free conditions at the top and bottom surfaces.
That is

\begin{itemize}
\item The derivative of the x-component of the velocity wrt the upward direction
    is zero at the at \(z=0\) and \(z=h\).
\item The y-component of the velocity is zero at \(z=0\) and \(z=h\)
\end{itemize}

Our governing equations consist of the Naiver-Stokes equation,
the convection-diffusion equation
and the continuity equation.

\begin{equation*}
\begin{aligned}
\frac{\partial \vec{v}}{\partial t} + (\vec{v}\cdot\nabla)\vec{v}
&= -\frac{1}{\rho_0}\nabla P + \nu \nabla^2\vec{v} + [1-\beta(T-T_0)] \vec{g}
\\
\frac{\partial T}{\partial t} + (\vec{v}\cdot\nabla)T
&= \kappa \nabla^2 T
\\
\nabla\cdot\vec{v} = 0
\end{aligned}
\end{equation*}

We are interested in the case of neutral stability;
When the time derivatives of the normal modes form is zero.

\subsection{Steady state}
\label{develop--math6192:ROOT:page--heat-flow.adoc---steady-state}

At steady state, time derivatives are zero.
From the Naiver-Stokes, we have that

\begin{equation*}
\frac{\partial P}{\partial x} = 0
\text{ and }
\frac{\partial P}{\partial y} = -g\rho_0[1-\beta(T-T_0)]
\end{equation*}

Also, from the convection-diffusion equation

\begin{equation*}
\frac{\partial^2 T}{\partial x^2} + \frac{\partial^2 T}{\partial y^2} = 0
\end{equation*}

We assume that in steady state, \(\frac{\partial T}{\partial x} = 0\);
that is, the temperature is not a function of the \(x\) position.
So, after applying our boundary conditions, we get

\begin{equation*}
T = (T_1-T_0)\frac{z}{h} + T_0
\end{equation*}

and hence

\begin{equation*}
\begin{aligned}
&
\frac{\partial P}{\partial y}
= -g\rho_0[1-\beta(T-T_0)]
= -g\rho_0\left[1-\beta(T_1-T_0)\frac{z}{h}\right]
\\&\implies
P = -g\rho_0\left[z-\beta(T_1-T_0)\frac{z^2}{2h} + \text{constant}\right]
\end{aligned}
\end{equation*}

We call these two solutions \(T_{ss}\) and \(P_{ss}\).

So, after applying a perturbation, we get

\begin{equation*}
\begin{aligned}
\vec{v} &= \tilde{\vec{v}}\\
T &= T_{ss} + \tilde{T}\\
P &= P_{ss} + \tilde{P}\\
\end{aligned}
\end{equation*}

\subsection{Linearization}
\label{develop--math6192:ROOT:page--heat-flow.adoc---linearization}

Note that the boundary conditions and continuity equation is already linearized.

\subsubsection{Navier-Stokes}
\label{develop--math6192:ROOT:page--heat-flow.adoc---navier-stokes}

\begin{equation*}
\begin{aligned}
\frac{\partial \vec{v}}{\partial t} + (\vec{v}\cdot\nabla)\vec{v}
&= -\frac{1}{\rho_0}\nabla P + \nu \nabla^2\vec{v} + [1-\beta(T-T_0)] \vec{g}
\\
\frac{\partial \tilde{\vec{v}}}{\partial t} + (\tilde{\vec{v}}\cdot\nabla)\tilde{\vec{v}}
&= -\frac{1}{\rho_0}\nabla (P_{ss}+\tilde{P})
    + \nu \nabla^2\tilde{\vec{v}} + [1-\beta(T_{ss}+\tilde{T}-T_0)] \vec{g}
\\
\frac{\partial \tilde{\vec{v}}}{\partial t}
&=
    -\frac{1}{\rho_0}\nabla P_{ss}
    -\frac{1}{\rho_0}\nabla \tilde{P}
    + \nu \nabla^2\tilde{\vec{v}}
    + [1-\beta(T_{ss}+-T_0)] \vec{g}
    - \beta\tilde{T}\vec{g}
\\
\frac{\partial \tilde{\vec{v}}}{\partial t}
&=
    -\frac{1}{\rho_0}\nabla \tilde{P}
    + \nu \nabla^2\tilde{\vec{v}}
    - \beta\tilde{T}\vec{g}
\\
\end{aligned}
\end{equation*}

Note that the final step is since \(P_{ss}\) and \(T_{ss}\) satisfy
the equation under steady state (or you can just insert it manually).

\subsubsection{Convection-diffusion}
\label{develop--math6192:ROOT:page--heat-flow.adoc---convection-diffusion}

\begin{equation*}
\begin{aligned}
\frac{\partial T}{\partial t} + (\vec{v}\cdot\nabla)T
&= \kappa \nabla^2 T
\\
\frac{\partial \tilde{T}}{\partial t}
+ (\vec{v}\cdot\nabla)T_{ss}
+ (\vec{v}\cdot\nabla)\tilde{T}
&= \kappa \nabla^2 \tilde{T}
\\
\frac{\partial \tilde{T}}{\partial t}
+ \vec{v}\cdot\left(0, \frac{T_1-T_0}{h}\right)
&= \kappa \nabla^2 \tilde{T}
\\
\end{aligned}
\end{equation*}

\subsection{Stability}
\label{develop--math6192:ROOT:page--heat-flow.adoc---stability}

We assume the following form of normal modes

\begin{equation*}
\begin{aligned}
\tilde{\vec{v}} &= (U(z), V(z))\exp(ikx + \sigma t)\\
\tilde{P} &= M(z)\exp(ikx + \sigma t)\\
\tilde{T} &= \Theta(z)\exp(ikx + \sigma t)\\
\end{aligned}
\end{equation*}

As stated before, we are interested in the case when \(\sigma = 0\) (neutral stability).
We will also let \(\Delta T = T_0 - T_1\) (typically positive).

\subsubsection{Getting initial equations}
\label{develop--math6192:ROOT:page--heat-flow.adoc---getting-initial-equations}

\begin{admonition-note}[{}]
We use the notation that \(D = \frac{\partial}{\partial z}\)
\end{admonition-note}

From the convection diffusion equation

\begin{equation*}
0 - V\frac{\Delta T}{h} = \kappa (-k^2\Theta + \Theta'')
\implies V = -\frac{\kappa h}{\Delta T}(D^2-k^2)\Theta
\end{equation*}

From the continuity equation

\begin{equation*}
\begin{aligned}
&0 = ik U + V'
\\&\implies U = \frac{i}{k} DV = -\frac{\kappa hi}{k\Delta T} (D^2-k^2)D\Theta
\end{aligned}
\end{equation*}

From the Navier-Stokes equation,

\begin{equation*}
\begin{aligned}
(0,0)
&=
\left(-\frac{1}{\rho_0}(ik)M, -\frac{1}{\rho_0}M'\right)
+ \left(\nu (- k^2 U + U''), \nu (-k^2V + V'')\right)
+ (0, g\beta \Theta)
\\&=\left(
-\frac{1}{\rho_0}(ik)M + \nu (- k^2 U + U'')
,
-\frac{1}{\rho_0}M'+ \nu (-k^2V + V'') + g\beta \Theta
\right)
\\&=\left(
-\frac{ik}{\rho_0}M + \nu (D^2 - k^2)U
,
-\frac{1}{\rho_0}DM+ \nu (D^2-k^2)V + g\beta \Theta
\right)
\end{aligned}
\end{equation*}

From the first component,

\begin{equation*}
M
= \frac{\rho_0 \nu}{ik} (D^2-k^2)U
= -\frac{\rho_0 \nu \kappa h}{k^2\Delta T} (D^2-k^2)^2 D\Theta
\end{equation*}

and from the second component

\begin{equation*}
\begin{aligned}
0
&= -\frac{1}{\rho_0}DM+ \nu (D^2-k^2)V + g\beta \Theta
\\&= \frac{\nu \kappa h}{k^2\Delta T} (D^2-k^2)^2 D^2\Theta
- \frac{\nu\kappa h}{\Delta T} (D^2-k^2)^2\Theta + g\beta \Theta
\\&\implies
\begin{aligned}[t]
0&= (D^2-k^2)^2 D^2\Theta - k^2 (D^2-k^2)^2\Theta + \frac{g\beta k^2\Delta T}{\nu \kappa h} \Theta
\\&= (D^2-k^2)^3 \Theta + \frac{g\beta k^2\Delta T}{\nu \kappa h} \Theta
\end{aligned}
\end{aligned}
\end{equation*}

\subsubsection{Dimensionless}
\label{develop--math6192:ROOT:page--heat-flow.adoc---dimensionless}

In dimensionless form, we use the following two substitutions

\begin{equation*}
z = h\hat{z}, \ k = \frac{\hat{k}}{h}
\end{equation*}

The first is so that \(\hat{z} \in [0,1]\).
The second is used to simplify the equation.
Upon substitution, we get

\begin{equation*}
(\Delta^2 - \hat{k}^2)^3\Theta + \frac{g\beta h^3\Delta T}{\nu \kappa}\hat{k}^2\Theta
= (\Delta^2 - \hat{k}^2)^3\Theta + Ra\hat{k}^2\Theta
=0
\end{equation*}

where \(\Delta = \frac{\partial}{\partial \hat{z}}\).
We call the constant \(Ra = \frac{g\beta \Delta T}{\nu \kappa}h^3\) Rayleigh's number.
From our boundary conditions we have that

\begin{itemize}
\item \(\Theta(\hat{z}=0) = \Theta(\hat{z}=1) = 0\)
    Since \(T\) is fixed at the boundaries
\item From the x-component of the free-free conditions \(U' = 0\).
    So, \((\Delta^2 -k^2)\Delta^2 \Theta =0\) and \(\Delta^4 \Theta =k^2\Delta^2 \Theta\)
    and \(\hat{z}=0\) and \(\hat{z}=1\).
\item From the y-component of the free-free conditions \(V = 0\).
    So \((\Delta^2-k^2)\Theta = 0\)
    and \(\Delta^2\Theta = k^2\Theta\) at \(\hat{z} = 0\)
    and \(\hat{z}=1\).
\end{itemize}

Therefore, at \(\hat{z}=0\) and \(\hat{z}=1\)

\begin{equation*}
\Delta^4\Theta = \Delta^2\Theta = \Theta = 0
\end{equation*}

This implies that \(\Theta(\hat{z}) = A\sin(n\pi \hat{z})\) for some
\(n \in \mathbb{Z}\) and \(A \in \mathbb{R}\).
We are interested in the case where there is only one cycle; ie \(n=1\).

\begin{admonition-todo}[{}]
Determine why this is the only form.
\end{admonition-todo}

If we plug this into our differential equation,
we get

\begin{equation*}
(-\pi^2-\hat{k}^2)^3 + Ra \hat{k}^2 =0
\implies Ra(\hat{k}) = \frac{(\pi^2 + \hat{k}^2)^3}{\hat{k}^2}
\end{equation*}

We call the minimum value of the Rayleigh number,
the critical Rayleigh number \(Ra_{cr}\) and it is

\begin{equation*}
Ra_{cr} = Ra\left(\hat{k}^2 = \frac{\pi^2}{2}\right) = \frac{27\pi^4}{4} \approx 660
\end{equation*}

It is the value of \(Ra\) when the first convection current occurs.
If \(Ra < Ra_{cr}\), the system is stable.

\section{Marangoni Problem}
\label{develop--math6192:ROOT:page--heat-flow.adoc---marangoni-problem}

Instead of gravity governing the convection currents,
we instead focus on the surface tension.
Note that this is unrealistic as we would assume that both gravity
and surface tension play pivotal roles in causing the convection currents.
Regardless, we will perform the analysis under these conditions.

\begin{figure}[H]\centering
\includegraphics[width=0.7\linewidth]{images/develop-math6192/ROOT/marangoni}
\end{figure}

We will assume the following

\begin{itemize}
\item Everything happens in the x-z plane only
\item \(\rho\) is constant in the continuity equation
\item \(\rho\) is a linear function of temperature \(\rho = \rho_0[1-\beta(T-T_0)]\)
\end{itemize}

We also assume the following conditions at the boundaries

\begin{itemize}
\item At the top surface (\(z=h\))
    
    \begin{itemize}
    \item The shape is due to surface tension and satisfies
        \(\eta \frac{\partial U}{\partial z}=-\alpha \frac{\partial T}{\partial x}\)
        where
        
        \begin{itemize}
        \item \(U\) is the x-component of the velocity
        \item \(\alpha = -\frac{\partial \gamma}{\partial T}\) is the rate of change of surface tension with temperature
        \item \(\eta\) is the dynamic viscosity
        \end{itemize}
    \item The z-component of the velocity is zero
    \item Newton's law of cooling is followed.
        \(-\frac{\partial T}{\partial z} = \beta\left[\Delta T \frac{z}{h}\right]\)
        where \(\Delta T = T_0-T_1\).
    \end{itemize}
\item At the bottom surface (\(z=0\))
    
    \begin{itemize}
    \item \(U'=0\)
    \item The z-component of the velocity is zero
    \end{itemize}
\end{itemize}

\subsection{Stability Analysis}
\label{develop--math6192:ROOT:page--heat-flow.adoc---stability-analysis}

We can skip straight to the stability analysis since
the derivation is exactly the same as the Rayleigh-Bénard
problem but with \(g=0\).
So, we have that

\begin{equation*}
\begin{aligned}
\tilde{\vec{v}} &= (U(z), V(z))\exp(ikx + \sigma t)\\
\tilde{P} &= M(z)\exp(ikx + \sigma t)\\
\tilde{T} &= \Theta(z)\exp(ikx + \sigma t)\\
\end{aligned}
\end{equation*}

and we find that

\begin{equation*}
\begin{aligned}
V(z) &= - \frac{\kappa h}{\Delta T}(D^2-k^2)\Theta\\
U(z) &= -\frac{\kappa hi}{k\Delta T}(D^2-k^2)D\Theta\\
M(z) &= -\frac{\rho_0\nu\kappa h}{k^2\Delta T} (D^2-k^2)^2D\Theta\\
(D^2-k^2)^3\Theta &= 0
\end{aligned}
\end{equation*}

We then apply the transformations \(z = h\hat{z}\) and
\(k = \frac{\hat{k}}{h}\) to see that

\begin{equation*}
(\Delta^2-\hat{k}^2)^3\Theta = 0
\end{equation*}

where \(\Delta = \frac{\partial}{\partial \hat{z}}\).
So,

\begin{equation*}
\Theta(\hat{z}) =
(A_0 + A_1\hat{z} + A_2\hat{z}^2)\sinh(\hat{k}\hat{z})
+ (B_0 + B_1\hat{z} + B_2\hat{z}^2)\cosh(-\hat{k}\hat{z})
\end{equation*}

\subsection{Applying boundary conditions}
\label{develop--math6192:ROOT:page--heat-flow.adoc---applying-boundary-conditions}

\subsubsection{At bottom surface}
\label{develop--math6192:ROOT:page--heat-flow.adoc---at-bottom-surface}

Since \(T\) is fixed at \(\hat{z}=0\),
\(\Theta =0 \) at \(\hat{z}=0\).

Since the \(z\)-derivative of the x-component of \(\vec{v}\) is 0
at \(\hat{z}=0\),

\begin{equation*}
0 = U' = -\frac{\kappa hi}{k\Delta T}(D^2-k^2)D^2\Theta
\implies \Delta^4\Theta = \Delta^2\Theta=0
\end{equation*}

Since the y-component of \(\vec{v}\) is zero at \(\hat{z}=0\),

\begin{equation*}
0 = V = - \frac{\kappa h}{\Delta T}(D^2-k^2)\Theta
\implies \Delta^2\Theta = \Theta =0
\end{equation*}

In summary, at \(\hat{z}=0\),

\begin{equation*}
\Delta^4\Theta = \Delta^2\Theta = \Theta = 0
\end{equation*}

Therefore, \(B_0=B_1=B_2=0\)
and

\begin{equation*}
\Theta(\hat{z}) =
(A_0 + A_1\hat{z} + A_2\hat{z}^2)\sinh(\hat{k}\hat{z})
\end{equation*}

\begin{admonition-todo}[{}]
Complete
\end{admonition-todo}
\end{document}
