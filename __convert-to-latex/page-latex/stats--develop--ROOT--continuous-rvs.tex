\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Continuous random variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
A random variable is continuous if it has no atoms of probability.

\section{Uniform}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---uniform}

\(X \sim Uniform[a,b]\)

\begin{description}
\item[pdf] \(f(x) = \frac{1}{b-a}\) for \(x \in (a,b)\)
\item[expectation] \(E[X] = \frac{a+b}{2}\)
\item[variance] \(Var[X] = \frac{(b-a)^2}{12}\)
\item[MGF] \(M(t) = \frac{e^{bt}-e^{at}}{t(b-a)}\)
\end{description}

\section{Normal}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---normal}

\(X \sim N(\mu,\sigma^2)\)

\begin{description}
\item[pdf] \(f(x) = \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right)\) for \(x \in \mathbb{R}\)
\item[expectation] \(E[X] = \mu\)
\item[variance] \(Var[X] = \sigma^2\)
\item[MGF] \(M(t) = \exp\left(t\mu + \frac{t^2\sigma^2}{2}\right)\)
\end{description}

\section{Exponential}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---exponential}

We denote \(\lambda > 0\) to
be the rate.

\(X \sim Exp(\lambda)\)

\begin{description}
\item[pdf] \(f(x) = \lambda e^{-\lambda x}\) for \(x\in\mathbb{R}\)
\item[expectation] \(E[X] = \frac{1}{\lambda}\)
\item[variance] \(Var[X] = \frac{1}{\lambda^2}\)
\item[MGF] \(M(t) = \frac{\lambda}{\lambda-t}\)
\end{description}

\section{Gamma}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---gamma}

We denote \(\alpha > 0\) to be the shape parameter and \(\beta > 0\) to
be the scale parameter.
The gamma distribution is the sum of \(\alpha\) iid exponential random variables.

\(X \sim Gamma(\alpha,\beta)\)

\begin{description}
\item[pdf] \(f(x) = \frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-\beta x}\) for \(x\in\mathbb{R}\)
\item[expectation] \(E[X] = \frac{\alpha}{\beta}\)
\item[variance] \(Var[X] = \frac{\alpha}{\beta^2}\)
\item[MGF] \(M(t) = \left(\frac{\beta}{\beta-t}\right)^\alpha\)
\end{description}

\section{Chi-Squared}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---chi-squared}

Let \(n\) be the number of the degrees of freedom

\(X \sim \chi^2_{(n)}\)

\begin{description}
\item[pdf] \(f(x) = \frac{1}{2^{-2/n}\Gamma(n/2)}x^{\frac{n}{2}-1}e^{-\frac{x}{2}}\) for \(x\in\mathbb{R}\)
\item[expectation] \(E[X] = n\)
\item[variance] \(Var[X] = 2n\)
\item[MGF] \(M(t) = \frac{1}{(1-2t)^{n/2}}\)
\end{description}

\begin{admonition-note}[{}]
the chi-squared is a special case of the gamma with \(\alpha=\frac{n}2\) and
\(\beta = \frac12\)
\end{admonition-note}

\begin{admonition-note}[{}]
If \(Z_1,\ldots Z_n \sim N(0,1)\) are iid,
then \(\sum_{i=1}^n Z_i^2 \sim \chi^2_n\)
\end{admonition-note}

\section{Beta}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---beta}

The beta distribution if often used to model probabilities.

\(X \sim Beta(\alpha,\beta)\)

\begin{description}
\item[pdf] \(f(x) = \frac{\Gamma(\alpha+\beta)}{\Gamma(\alpha)\Gamma(\beta)}x^{\alpha-1}(1-x)^{\beta-1}\) for \(x \in \mathbb{R}\)
\item[expectation] \(E[X] = \frac{\alpha}{\alpha+\beta}\)
\item[variance] \(Var[X] = \frac{\alpha\beta}{(\alpha+\beta+1)(\alpha+\beta)^2}\)
\item[MGF] \(M(t) = \sum_{n=0}^\infty \left(\prod_{r=0}^{n-1} \frac{\alpha+r}{\alpha+\beta+r}\right)\frac{t^n}{n!}\)
\end{description}

\section{Student's t}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---students-t}

Student's t-distribution is defined as

\begin{equation*}
t_n = \frac{Z}{\sqrt{V/ n}}
\end{equation*}

where \(Z\sim (0,1)\) and \(V\sim \chi^2_n\).

\(X \sim t_\nu\)

\begin{description}
\item[pdf] \(f(x) = \frac{\Gamma\left(\frac{\nu+1}{2}\right)}{\sqrt{\pi\nu}\Gamma\left(\frac{\nu}{2}\right)}
    \left(1+\frac{x^2}{\nu}\right)^{-\frac{\nu +1}{2}}\) for \(x \in \mathbb{R}\)
\item[expectation] \(E[X] = 0\) if \(\nu >1\)
\item[variance] \(Var[X] = \frac{\nu}{\nu-2}\) if \(\nu > 2\)
\item[MGF] does not exist
\end{description}

\section{F-dstribution}
\label{develop--stats:ROOT:page--continuous-rvs.adoc---f-dstribution}

The F-distribution may be defined as

\begin{equation*}
F = \frac{U/n}{V/m}
\end{equation*}

where \(U\sim \chi^2_n\) and \(V\sim \chi^2_m\).

\(X \sim F_{\nu_1, \nu_2}\)

\begin{description}
\item[expectation] \(E[X] = \frac{\nu_2}{\nu_2-2}\) if \(\nu_2 >2\)
\item[variance] \(Var[X] = \left(\frac{\nu_2}{\nu_2-2}\right)^2 \frac{\nu_1+\nu_2-2}{\nu_1(\nu_2-4)}\) if \(\nu_2 > 4\)
\item[MGF] does not exist
\end{description}
\end{document}
