\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Negative Binomial}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A \emph{negative binomial} experiment is a statistical experiment consisting
of independent bernoulli trials, each with probability of success
\(p\), repeated until \(r\) success are obtained.
Let \(X\) be the number of trials required to obtain
\(r\) successes each with probability \(p\). Then

\begin{equation*}
X \sim NegativeBinomial(r, p)
\end{equation*}

Furthermore, the probability function of \(X\) is

\begin{equation*}
\begin{aligned}
    f(x)
    &= P(X = x) \\
    &= P(\text{$x$'th trial is a success and there are $r-1$ successes in the first $x-1$ trials})\\
    &= P(\text{$x$'th trial is a success}) P(\text{exactly $r-1$ success in the first $x-1$ trials})\\
    &= p \binom{x-1}{r-1}p^{r-1}(1-p)^{(x-1)-(r-1)}\\
    &= \binom{x-1}{r-1}p^{r}(1-p)^{x-r}\end{aligned}
\end{equation*}

for \(x = r, r+1, \ldots\).

Alternatively, a negative binomial distribution is the sum of
\(r\) independent random variables with a geometric
distribution. This is intuitive since \(i\)’th of these
experiments consists of the the trials following the
\((i-1)\)’th success up to the \(i\)th success. That
is the \(i\)’th sub-experiment counts the number of trials
until the next (\(i\)’th) success is obtained.

\begin{admonition-note}[{}]
In a way, this is the 'inverse' of the binomial distribution since the
number of success are fixed instead of the number of trials.
\end{admonition-note}

\begin{admonition-caution}[{}]
Some books define this very differently, but this definition would be
the one used in this course.
\end{admonition-caution}

\section{Validity of probability function}
\label{develop--math2274:ROOT:page--random/discrete/negative-binomial.adoc---validity-of-probability-function}

Let

\begin{equation*}
S_r(t) = \sum_{x=0}^\infty \binom{r+x}{r}t^x
\end{equation*}

Then

\begin{equation*}
\begin{aligned}
    S_r(t)
    &= \sum_{x=0}^\infty \binom{r+x}{r}t^x\\
    &= \frac{1}{t^{r-1}}\sum_{x=0}^\infty \binom{r+x}{r}t^{x+r-1}\\
    &= \frac{1}{rt^{r-1}}\frac{d}{dt}\left[ \sum_{x=0}^\infty \binom{r+x-1}{r-1}t^{x+r} \right]\\
    &= \frac{1}{rt^{r-1}}\frac{d}{dt}\left[ t^rS_{r-1}(t) \right]\end{aligned}
\end{equation*}

Notice that \(S_0(t) = \frac{1}{1-t}\) and hence we claim that
\(S_r(t) = \frac{1}{(1-t)^{r+1}}\) whenever
\(|t| < 1\). Then

\begin{equation*}
S_r(t)
    = \frac{1}{rt^{r-1}}\frac{d}{dt}\left[ t^rS_{r-1}(t) \right]
    = \frac{1}{rt^{r-1}}\frac{d}{dt}\left( \frac{t}{1-t} \right)^r
    = \frac{1}{rt^{r-1}}\cdot r \cdot \frac{1}{(1-t)^2} \cdot\left( \frac{t}{1-t} \right)^{r-1}
    = \frac{1}{(1-t)^{r+1}}
\end{equation*}

Alternatively, this formula can be derived as the number of ways to sum
\(r+1\) non-negative integers to \(x\) since
\(\frac{1}{(1-t)^{r+1}} = \left( 1 + t+  t^2 + t^3 + \cdots \right)^{r+1}\).
Either way, it is now clear that the probability function forms a valid
probability space since

\begin{equation*}
\sum_{x=r}^\infty \binom{x-1}{r-1}p^{r}(1-p)^{x-r}
    =\sum_{x=0}^\infty \binom{x+r-1}{r-1}p^{r}(1-p)^{x}
    =p^{r} S_{r-1}(1-p)
    =1
\end{equation*}

\section{Moment}
\label{develop--math2274:ROOT:page--random/discrete/negative-binomial.adoc---moment}



\begin{equation*}
\begin{aligned}
    E[X^k]
    &= \sum_{x=r}^{\infty} x^k\binom{x-1}{r-1}p^{r}(1-p)^{x-r}\\
    &= r\sum_{x=r}^{\infty} x^{k-1}\binom{x}{r}p^{r}(1-p)^{x-r}\\
    &= \frac{r}{p}\sum_{x=r}^{\infty} x^{k-1}\binom{x + 1 - 1}{r + 1 - 1}p^{r + 1}(1-p)^{(x+1)-(r+1)}\\
    &= \frac{r}{p}\sum_{x=r+1}^{\infty} (x-1)^{k-1}\binom{x - 1}{r + 1 - 1}p^{r + 1}(1-p)^{x-(r+1)}\\
    &= \frac{r}{p} E[(Y-1)^{k-1}]\end{aligned}
\end{equation*}

where \(Y \sim NegativeBinomial(r+1, p)\)

\section{Expectation}
\label{develop--math2274:ROOT:page--random/discrete/negative-binomial.adoc---expectation}

\begin{equation*}
E[X] = \frac{r}{p} E[(Y-1)^0] = \frac{r}{p}
\end{equation*}

\section{Variance}
\label{develop--math2274:ROOT:page--random/discrete/negative-binomial.adoc---variance}

\begin{equation*}
E[X^2]
    = \frac{r}{p} E[(Y-1)^1]
    = \frac{r}{p}\left[\frac{r+1}{p} - 1\right]
    = \frac{r^2 + r - rp}{p^2}
\end{equation*}

Therefore

\begin{equation*}
Var[X] = E[X^2] - E[X]^2 = \frac{r^2 + r - rp}{p^2} - \frac{r^2}{p^2} = \frac{r(1-p)}{p^2}
\end{equation*}

\section{Moment Generating Function}
\label{develop--math2274:ROOT:page--random/discrete/negative-binomial.adoc---moment-generating-function}

\begin{equation*}
\begin{aligned}
    M_X(t)
    &= E[e^{Xt}]\\
    &= \sum_{x=r}^{\infty} \binom{x-1}{r-1}e^{xt} p^{r}(1-p)^{x-r}\\
    &= \sum_{x=0}^{\infty} \binom{x+r-1}{r-1}e^{xt+rt} p^{r}(1-p)^{x}\\
    &= p^re^{rt}\sum_{x=0}^{\infty} \binom{x+r-1}{r-1}\left( (1-p)e^t \right)^{x}\\
    &= \left( pe^t \right)^{r}S_{r-1}\left( (1-p)e^t \right)\\
    &= \frac{\left( pe^t \right)^{r}}{\left( 1-(1-p)e^t \right)^r}\\
    &= \left( \frac{pe^t}{1-(1-p)e^t} \right)^r\end{aligned}
\end{equation*}

for
\((1-p)e^t < 1 \Longleftrightarrow t < \ln\left( \frac{1}{1-p} \right)\).
Also, notice that the moment generating function shows that the negative
binomial can be seen as the sum of \(r\) independent geometric
distributions.
\end{document}
