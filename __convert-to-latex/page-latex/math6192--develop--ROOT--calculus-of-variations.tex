\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Calculus of Variations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In this section, we will will try to find a function \(y(x)\) which maximizes (or minimizes)
the following operator

\begin{equation*}
I(y) = \int_a^b F(x,y,y') \ dx
\end{equation*}

where \(F(x,y,y')\) is a function such that \(I(y)\) will, for example,

\begin{itemize}
\item Compute the length of the curve of \(y\). \(F(x,y,y') = \sqrt{1+(y')^2}\)
\item Compute the amount of time for an object to follow the path when acted on by only gravity;
    ie the Brachistochrone problem
\item Compute the surface area of a region. For example when trying to convert a point set
    into a mesh \url{https://www.cs.jhu.edu/~misha/MyPapers/ToG13.pdf}
\end{itemize}

We also have boundary conditions on \(y\): \(y(a) = y_a\) and \(y(b) = y_b\).

To find the function \(y(x)\) which maximizes/minimizes \(I(y)\),
we will consider the Taylor expansion of \(F\) as follows

\begin{equation*}
\begin{aligned}
I(y+\delta y)
&= \int_a^b F(x,y+\delta y, y' + \delta y') \ dx
\\&= \int_a^b \left[
    F(x,y,y')
    + \frac{\partial F}{\partial y}\delta y
    + \frac{\partial F}{\partial y'}\delta y'
    + \cdots
\right] \ dx
\\&= I(y) + \int_a^b \left[
    \frac{\partial F}{\partial y}\delta y
    + \frac{\partial F}{\partial y'}\delta y'
    + \cdots
\right] \ dx
\end{aligned}
\end{equation*}

\begin{admonition-note}[{}]
Here \(\delta y\) is not an actual value but rather a small change in the function \(y\).
So, for example, if \(y=x^2\), \(\delta y\) may be \(\frac{x}{10000}\).
\end{admonition-note}

Define

\begin{equation*}
\delta I = \int_a^b \left[
    \frac{\partial F}{\partial y}\delta y
    + \frac{\partial F}{\partial y'}\delta y'
\right] \ dx
\end{equation*}

as the \emph{first variation} of \(I\).
At the maximum/minimum of \(I\), \(\delta I = 0\) (think of the Taylor series expansion about extremum \(y\))
and hence

\begin{equation*}
\begin{aligned}
0
&=
\int_a^b \frac{\partial F}{\partial y} \delta y \ dx
+ \int_a^b \frac{\partial F}{\partial y'} \delta y' \ dx
\\&=
\int_a^b \frac{\partial F}{\partial y} \delta y \ dx
+ \left[
\left. \frac{\partial F}{\partial y'} \delta y \right|_a^b
- \int_a^b \frac{d}{dx}\left(\frac{\partial F}{\partial y'}\right) \delta y \ dx
\right]
\\&=
\int_a^b \frac{\partial F}{\partial y} \delta y \ dx
- \int_a^b \frac{d}{dx}\left(\frac{\partial F}{\partial y'}\right) \delta y \ dx
\\&=
\int_a^b \left[\frac{\partial F}{\partial y} - \frac{d}{dx}\left(\frac{\partial F}{\partial y'}\right) \right] \delta y \ dx
\end{aligned}
\end{equation*}

since the endpoints are fixed (by the boundary conditions) and hence the variation in \(y\), \(\delta y\), at the boundary conditions
must be zero.
Now, since the above must hold regardless of the choice of \(\delta y\), we must have that

\begin{equation*}
\frac{\partial F}{\partial y} - \frac{d}{dx}\left(\frac{\partial F}{\partial y'}\right) = 0
\end{equation*}

This is called the \emph{Euler-Lagrange equation} and our function \(y\)
must satisfy it.

We may also extend this derivation to multiple dependent variables.
So, if we have \(y_1(x), \ldots y_n(x)\) and \(F(x, \vec{y}, \vec{y}')\),
we obtain the following Euler-Lagrange equations

\begin{equation*}
\frac{\partial F}{\partial y_i} - \frac{d}{dx}\left(\frac{\partial F}{\partial y'_i}\right) = 0
\quad\text{for }i=1,\ldots n
\end{equation*}

which must all be satisfied for our \(\vec{y}\) to maximize/minimize
the functional

\begin{equation*}
I(\vec{y}) = \int_a^b F(x, \vec{y},\vec{y}')\ dx
\end{equation*}

\section{Alternative derivation}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---alternative-derivation}

Let \(\eta(x)\) be a non-zero function with \(\eta(a) = \eta(b) = 0\) and consider

\begin{equation*}
S(\alpha) = I(y + \alpha \eta)
\end{equation*}

Then, if \(y\) attains an extremum of \(I\),
\(S\) must attain an extremum at \(\alpha = 0\).
So, we have that

\begin{equation*}
\begin{aligned}
0 = S'(0)
&= \left.\frac{d}{d\alpha} \int_a^b F(x,y +\alpha\eta, y' + \alpha\eta') dx \right|_{\alpha=0}
\\&= \left.\int_a^b
    \eta\left.\frac{\partial F}{\partial y} \right|_{(x,y +\alpha\eta, y' + \alpha\eta')}
    + \eta'\left.\frac{\partial F}{\partial y'} \right|_{(x,y +\alpha\eta, y' + \alpha\eta')}
dx \right|_{\alpha=0}
\\&= \left.\int_a^b
    \eta\frac{\partial F}{\partial y}
    + \eta'\frac{\partial F}{\partial y'}
dx \right|_{\alpha=0}
\end{aligned}
\end{equation*}

Then, we do the same manipulations as previously to obtain that

\begin{equation*}
\int_a^b \left[\frac{\partial F}{\partial y} - \frac{d}{dx}\left(\frac{\partial F}{\partial y'}\right) \right] \eta \ dx
= 0
\end{equation*}

So, since the choice of \(\eta\) was not restricted, this holds for all \(\eta\).
So, we must have that the coefficient of \(\eta\) is zero.

\section{No dependence on \(x\)}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---no-dependence-on-latex-backslashx-latex-backslash}

\begin{admonition-note}[{}]
We use the notation \(F_y = \frac{\partial F}{\partial y}\)
and \(F_{y'} = \frac{\partial F}{\partial y'}\)
\end{admonition-note}

Suppose that \(F(y,y')\) with no direct dependence on \(x\) (except through \(y\) and \(y'\)).
Then, the Euler-Lagrange equation reduces to

\begin{equation*}
\begin{aligned}
0
&= F_y - \frac{d}{dx}F_{y'}
\\&= F_y - (F_{y'y}y' + F_{y'y'}y'') \quad\text{by the chain rule}
\end{aligned}
\end{equation*}

However, if we multiply the Euler-Lagrange equation by \(y'\),
we get

\begin{equation*}
\begin{aligned}
0
&= y'F_y - y'\frac{d}{dx}F_{y'}
\\&= \left(\frac{dF}{dx} - F_{y'}y''\right) - y'\frac{d}{dx}F_{y'} \quad\text{by chain rule}
\\&= \frac{dF}{dx} - \frac{d}{dx}\left[F_{y'}y'\right] \quad\text{by product rule}
\\&= \frac{d}{dx}\left[F - F_{y'}y'\right] \quad\text{by product rule}
\end{aligned}
\end{equation*}

So, \(F - F_{y'}y'\) is constant.

\begin{example}[{Minimizing the length of curve}]
\begin{admonition-caution}[{}]
I wonder if the Euler-Lagrange equation can be derived without using
this property. I doubt it needs it. Regardless, this proof is valid as long as you can
get the prerequisites.
\end{admonition-caution}

Suppose that we want to find the curve \(y(x)\) which joins
\((a,y_a)\) and \((b,y_b)\) and has minimum length

\begin{equation*}
L(y) = \int_a^b \sqrt{1+(y')^2} \ dx
\end{equation*}

We expect that \(y\) to be a straight line; but we have to prove it.
Let \(F(y,y') = \sqrt{1+(y')^2}\).
So

\begin{equation*}
F_y = 0,\quad\text{and}\quad F_{y'} = \frac{y'}{\sqrt{1+(y')^2}}
\end{equation*}

So, we have that there exists a constant \(A\) such that

\begin{equation*}
A = F-y' F_{y'} = \sqrt{1+(y')^2} - \frac{(y')^2}{\sqrt{1+(y')^2}}
\implies y' = \sqrt{\frac{1}{A^2} - 1}
\end{equation*}

and hence \(y(x) = mx + c\) for some constants \(m\) and \(c\).

\begin{admonition-important}[{}]
We have shown that this is the point of the extremum.
We need to prove that this extremum is a minimum, which should be plain to see.
\end{admonition-important}
\end{example}

\begin{example}[{Brachistochrone problem}]
Consider the we are to build a track such that when a ball is released at \((a, y_a)\),
it reaches \((b,y_b)\). There are many ways to do this, such as having a simple slope
between the two points.
What function \(y(x)\) should the slope be such that the time taken is minimized?

We assume that there is no rolling resistance and the only force which acts on the ball
is gravity.
First, we must model the time taken. This is simply the integral
\(\int_a^b \frac{ds}{v}\) where \(v\) is the velocity of the ball.
Note that the velocity can be easily modeled using conservation
of energy to obtain \(v = \sqrt{2g(y_a-y)}\) and the speed
is a well known to be \(dx\sqrt{1+(y')^2}\).
So,

\begin{equation*}
I(y) = \int_a^b \frac{\sqrt{1+(y')^2}}{\sqrt{2g(y_a-y)}} \ dx
\end{equation*}

So, from the simplified version of the Euler-Lagrange equation,
there exists a constant \(A\) such that

\begin{equation*}
A = F - y'F_{y'}
\end{equation*}

where

\begin{equation*}
F(y,y') = \frac{\sqrt{1+(y')^2}}{\sqrt{2g(y_a-y)}}
\quad\text{and}\quad
F_{y'} = \frac{y'}{1+(y')^2}F
\end{equation*}

So, we have that

\begin{equation*}
\begin{aligned}
&A = F\frac{1}{1+(y')^2}
\\&\implies A\sqrt{2g(y_a-y)}\sqrt{1+(y')^2} = 1
\\&\implies 2gA^2(y_a-y)(1+(y')^2) = 1
\end{aligned}
\end{equation*}

If we let \(z=y_a-y\) and \(z' = \cot t\) where
\(y(t)\) and \(x(t)\), we get that

\begin{equation*}
2gA^2 z \frac{1}{\sin^2 t} = 1
\implies z = \frac{1}{2gA^2} \sin^2 t
\end{equation*}

Therefore,

\begin{equation*}
\cot t = z' = \frac{1}{gA^2} \sin t\cos t \frac{dt}{dx}
\implies \frac{dx}{dt} = \frac{1}{gA^2}\sin^2 t
\implies x = \frac{1}{2gA^2}\left(t-\frac{1}{2}\sin(2t)\right) + C
\end{equation*}

where \(C\) is a constant of integration,
and

\begin{equation*}
y = y_a - z
= y_a - \frac{1}{2gA^2} \sin^2 t
= y_a - \frac{1}{2gA^2} \left(1-\frac{1}{2}\cos (2t)\right)
\end{equation*}

So, since the equations of a cycloid are

\begin{equation*}
x = r(t-\sin t), \quad\text{and}\quad y = r(1-\cos t)
\end{equation*}

we have that the path of the ball must follow the path of a cycloid.

Note that we still have to determine the constants \(A\) and \(C\)
as well as the limits for \(t\) based on
our boundary conditions.
\end{example}

\begin{example}[{Surface area of bubble film}]
Suppose we have two rings of radius \(r\) separated by distance \(L\)
and there is a soap film joining the two rings.
Given that the surface area of the soap is minimized, we need to determine the shape
of the film.
We will assume that there is circular symmetry.

Let \(y(x)\) be the radius of the film at position \(x\).
Then, the surface area is given by

\begin{equation*}
S(y) = \int_a^b (\text{perimeter}) \ ds = \int_a^b Cy\sqrt{1+(y')^2} \ dx
\end{equation*}

where \(C=2\pi\).
So,

\begin{equation*}
F(y,y') = Cy\sqrt{1+(y')^2}
\implies F_{y'} = \frac{Cyy'}{\sqrt{1+(y')^2}}
\end{equation*}

So, from the Euler-Lagrange equation, there exists constant \(A\) such that

\begin{equation*}
A = F-y'F_{y'}
= Cy\sqrt{1+(y')^2} - \frac{Cy(y')^2}{\sqrt{1+(y')^2}}
= \frac{Cy(1+(y')^2) - Cy(y')^2}{\sqrt{1+(y')^2}}
= \frac{Cy}{\sqrt{1+(y')^2}}
\end{equation*}

So, by rearranging, we get that

\begin{equation*}
y' = \sqrt{\left(\frac{C}{A}\right)^2y^2 - 1}
\end{equation*}

Therefore, \(y = \frac{A}{C}\cosh \left(\frac{C}{A}(x+B)\right)\)
where \(B\) is a constant to of integration.
We can then solve for constants \(A\) and \(B\)
using the distances between the rings and their respective sizes.
\end{example}

\begin{example}[{Application in Optics}]
Fermat's principle states that the path taken
between two points by a ray of light is the path that takes the least
time.

Recall that the speed of light depends on the medium of propagation.
We define \(c_0\) to be the speed of light in a vacuum
and we define the \emph{refraction index} to be

\begin{equation*}
n = \frac{c_0}{c}
\end{equation*}

where \(c\) is the speed of light in another medium.
Suppose that \(n = n(y)\).
We wish to find determine when it is possible for light
to travel from points \((a,y_a)\) to \((b,y_b)\).

First, note that

\begin{equation*}
\frac{c_0}{n(y)} = c = \frac{ds}{dt} = \frac{\sqrt{1 + (y')^2} \ dx}{dt}
\end{equation*}

So, we have that

\begin{equation*}
T = I(y) = \int_a^b \frac{\sqrt{1 + (y')^2}}{c_0/n(y)} \ dx
\end{equation*}

We want to find \(y(x)\) that minimizes \(I(t)\).
Let \(F(y,y') = \frac{\sqrt{1 + (y')^2}}{c_0/n(y)}\);
and recall that the optimal path \(y(x)\) satisfies the
Euler-Lagrange equation

\begin{equation*}
A = F - y'F_{y'}
= \frac{\sqrt{1 + (y')^2}}{c_0/n(y)} - y' \frac{y'}{(c_0/n(y))\sqrt{1 + (y')^2}}
= \frac{1}{(c_0/n(y))\sqrt{1 + (y')^2}}
\end{equation*}

where \(A\) is a constant.
Then, after rearranging, we get that

\begin{equation*}
y' = \pm\sqrt{\left(\frac{n(y)}{Ac_0}\right) - 1}
\end{equation*}

We only get a valid solution if \(n(y) > Ac_0\).
This means that the refractive index must be above some value in order
for light to propagate through it.
\end{example}

\section{Lagrange Multipliers}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---lagrange-multipliers}

Suppose we have a function \(F(\vec{x})\) which is subject
to the constraints \(G_i(\vec{x})=0\).
Then, to find the stationary points of \(F\) in region of validity
of the \(G_i\), we construct the function

\begin{equation*}
H(\vec{x}) = F(\vec{x}) + \sum_i \lambda_i G_i(\vec{x})
\end{equation*}

and find the stationary points of this new function with the same
constraints \(G_i\)

\begin{proof}[{``Proof''}]
\begin{admonition-remark}[{}]
I don't like this proof as it fails when \(F=xy\) and \(G=x+y\).
Also, what on earth is happening with \(x\) and \(y\).
How can \(y\) be a function of \(x\). In such a case,
this is a usual univariate problem.
\end{admonition-remark}

Suppose we only have \(F(x,y(x))\) and \(G(x,y(x))=0\).
Then, at the stationary point,

\begin{equation*}
0 = \frac{d}{dx}F = \frac{\partial F}{\partial x} + \frac{\partial F}{\partial y} \frac{dy}{dx}
\end{equation*}

\begin{admonition-tip}[{}]
This is not necessarily true, \(F\) need not be stationary relative to the entire \(\mathbb{R}^2\)
\end{admonition-tip}

Also, we can differentiate \(G\) to get a similar equation

\begin{equation*}
0 = \frac{d}{dx} 0 =  \frac{d}{dx}G
= \frac{\partial G}{\partial x} + \frac{\partial G}{\partial y} \frac{dy}{dx}
\end{equation*}

by combining both equations, we get

\begin{equation*}
\frac{F_x}{G_x} = \frac{F_y}{G_y}
\end{equation*}

which must be a constant (\(\lambda\)). So, if we had instead used

\begin{equation*}
H(x,y) = F - \lambda G
\end{equation*}

we would have gotten the same result.
\end{proof}

We don't really care about this for our case as we do not want to find
the point of the maximum of a function, but rather a function
which maximizes a functional.
To do this, we just replace our functions \(F\) and \(G\)
with functionals.

Suppose we have a functional

\begin{equation*}
I(y) = \int_a^b F(x,y,y') \ dx
\end{equation*}

We want to find a function \(y\) which obtains an extremum
for this functional where \(y\) satisfies

\begin{equation*}
\int_a^b G(x,y,y') \ dx = k
\end{equation*}

Then, by using the theory of Lagrange multipliers, we can instead
find the extrema of

\begin{equation*}
J(y) = \int_a^b F(x,y,y') \ dx - \lambda \int_a^b G(x,y,y') \ dx
= \int_a^b (F- \lambda G) \ dx
\end{equation*}

\begin{example}[{Shape of string with maximum area}]
Suppose we have a string of length \(L\)
and we use it to create a curve from \((a,0)\) to \((b,0)\)
which maximizes the area between the curve and the x-axis.
What is the shape of the curve \(y\).

The area is given by \(\int_a^b y \ dx\),
so \(F(x,y,y') = y\) and the constraint is given
by

\begin{equation*}
L = \int_a^b \sqrt{1+(y')^2} \ dx
\end{equation*}

So, we need to maximizes

\begin{equation*}
J(y) = \int_a^b y - \lambda \sqrt{1+(y')^2} \ dx
\end{equation*}

We call the inner part \(G\) and note that

\begin{equation*}
G(y,y') = y - \lambda \sqrt{1+(y')^2}
\end{equation*}

Since this is only a function of \(y\) and \(y'\),
we can use the following form of the Euler-Lagrange equation

\begin{equation*}
A = G - y'G_{y'}
= y - \lambda \sqrt{1+(y')^2} - y' \frac{-\lambda y'}{\sqrt{1+(y')^2}}
= y + \frac{-\lambda}{\sqrt{1+(y')^2}}
\end{equation*}

After rearranging, we can obtain that

\begin{equation*}
\lambda^2 = (x+C)^2 + (y-A)^2
\end{equation*}

So, it is the segment of a circle.

This makes sense when \(L\) is not too long.
But what happens when \(L\) is very big?
My conjecture is that it is still a circle, but it can extend backward
\end{example}

\begin{example}[{Shape of string with maximum area (generalized)}]
We have the same setup as the previous problem, but instead we will
use a general curve \(\vec{r}(t) = x + iy\) where \(r(0) = (a,0)\)
and \(r(1) = (b,0)\).
So, the length is now given by

\begin{equation*}
\int_0^1 \sqrt{(x')^2 + (y')^2} \ dt
\end{equation*}

and from greens theorem (with \(P=0\) and \(Q=x\)),
the area is given by

\begin{equation*}
Area = \int_0^1 xy' \ dt + \int_1^0 (0,x)\cdot\vec{r}_2(t) \ dt
\end{equation*}

where \(\vec{r}_2\) is the path from \((a,0)\) to \((b,0)\).
We can ignore the second term since it is constant

\begin{admonition-remark}[{}]
I am not too sure if this is the negated area or positive area. Regardless, it is
a linear function of the area and we can use it to find the extremum of the area.
\end{admonition-remark}

So, we wish to maximize

\begin{equation*}
J(x,y) = \int_0^1 xy' -\lambda \sqrt{(x')^2 + (y')^2} \ dt
\end{equation*}

and our function is

\begin{equation*}
F(x,y,x',y') = xy' -\lambda \sqrt{(x')^2 + (y')^2}
\end{equation*}

By following the same derivation as the beginning of this section,
we can obtain the same Euler-Lagrange equations of

\begin{equation*}
F_x - \frac{d}{dt} \left(F_{x'}\right) = 0
\quad\text{and}\quad
F_y - \frac{d}{dt} \left(F_{y'}\right) = 0
\end{equation*}

Therefore, we get

\begin{equation*}
y' - \frac{d}{dt}\left(\frac{-\lambda x'}{\sqrt{(x')^2 + (y')^2}}\right)
= 0
% \implies y + \frac{\lambda x'}{\sqrt{(x')^2 + (y')^2}} = A
\implies A-y = \frac{\lambda x'}{\sqrt{(x')^2 + (y')^2}}
\end{equation*}

and

\begin{equation*}
0 - \frac{d}{dt}\left(x - \frac{\lambda y'}{\sqrt{(x')^2 + (y')^2}}\right) = 0
% \implies x- \frac{\lambda y'}{\sqrt{(x')^2 + (y')^2}}= B
\implies x-B =  \frac{\lambda y'}{\sqrt{(x')^2 + (y')^2}}
\end{equation*}

where \(A\) and \(B\) are constants.
By dividing the two equations, we get

\begin{equation*}
\frac{A-y}{x-B} = \frac{x'}{y'}
\implies (A-y)y' = (x-B)x'
\end{equation*}

and by integrating, we get the equation of a circle.
\end{example}

\section{Least Action Principle}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---least-action-principle}

We define an \emph{action} \(S\) as a functional of the trajectory \(x\) (as well as its derivatives).

\begin{equation*}
S(x) = \int_{t_1}^{t_2} \mathcal{L}\ dt
\end{equation*}

Note that \(S\) has units equivalent to work and hence \(\mathcal{L}\), the Lagrangian,
has units of energy.
In classical mechanics, we define the Lagrangian as

\begin{equation*}
\mathcal{L} = T-U
\end{equation*}

where \(T\) and \(U\) are the kinetic and potential energies of the system.
I am not sure why this is chosen, but upon applying the Euler-Lagrange equation,
and rearranging, we get the time differentiated form of the conservation of energy.
Alternatively, we could have used the fact that force is the negated spatial derivative
of potential energy and used \(F = m\ddot{x}\).

Now, since \(E=T+U\) is constant, we can rewrite the action as

\begin{equation*}
S(x)
= \int_{t_1}^{t_2} 2T-E \ dt
= \int_{t_1}^{t_2} m \vec{v}^2 \ dt - E(\delta t)
= \int_{t_1}^{t_2} m \vec{p}\cdot \ d\vec{r} - E(\delta t)
\end{equation*}

If we operate under the least action principle, we can determine the trajectory
of a system.

\subsection{Example: Spring pendulum}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---example-spring-pendulum}

Suppose that we have a pendulum consisting of an elastic string (or spring) and mass \(m\).
We use the coordinate system \((L_e, \theta)\) where \(L_e\) is the length of the string
and \(\theta\) is the angle to the vertical (both are functions of time).
Let \(L_0\) be the ``rest'' length of the elastic string (under no load) and
\(k\) be the spring constant.
Our first goal is to find the Lagrangian of the system.

So, by using the usual Cartesian coordinates, we have that

\begin{equation*}
x = L_e\sin\theta, \quad\text{and}\quad y = L_e\cos\theta
\end{equation*}

where \(y\) increases as we move downward.
So

\begin{equation*}
\begin{aligned}
\dot{x} &= \dot{L}_e\sin\theta + \dot{\theta} L_e\cos\theta\\
\dot{y} &= \dot{L}_e\cos\theta - \dot{\theta} L_e\sin\theta\\
\end{aligned}
\end{equation*}

and hence the total kinetic energy is given by

\begin{equation*}
T
= \frac{m}{2}(\dot{x}^2 + \dot{y}^2)
= \frac{m}{2}\left[\dot{L}_e^2 + \dot{\theta}^2 L_e^2\right]
\end{equation*}

Also, the potential energy is given by

\begin{equation*}
U
= -mgy + \frac{k}{2}(L_e-L_0)^2
= -mgL_e\cos\theta + \frac{k}{2}(L_e-L_0)^2
\end{equation*}

Note that the usual form for gravitational potential energy is negated since \(y\) points
downwards.
So, the Lagrangian of the system is

\begin{equation*}
\mathcal{L} = T-U
= \frac{m}{2}\left[\dot{L}_e^2 + \dot{\theta}^2 L_e^2\right]
 +mgL_e\cos\theta - \frac{k}{2}(L_e-L_0)^2
\end{equation*}

By applying the Euler-Lagrange equation with respect to \(L_e\), we get

\begin{equation*}
\begin{aligned}
0
&= \frac{\partial \mathcal{L}}{\partial L_e} - \frac{d}{dt}\frac{\partial \mathcal{L}}{\partial \dot{L}_e}
\\&=
    \left(m\dot{\theta}^2 L_e +mg \cos\theta -k(L_e-L_0)\right)
    -\frac{d}{dt}\left(m\dot{L}_e\right)
\\&\implies\quad
    0 = m\dot{\theta}^2 L_e +mg \cos\theta -k(L_e-L_0) - m\ddot{L}_e
\end{aligned}
\end{equation*}

By applying the Euler-Lagrange equation with respect to \(\theta\), we get

\begin{equation*}
\begin{aligned}
0
&= \frac{\partial \mathcal{L}}{\partial \theta} - \frac{d}{dt}\frac{\partial \mathcal{L}}{\partial \dot{\theta}}
\\&=
    \left(-mgL_e\sin\theta\right)
    -\frac{d}{dt}\left(m\dot{\theta}L_e^2\right)
\\&=
    -mgL_e\sin\theta -m\ddot{\theta}L_e^2 - 2m\dot{\theta}L_e\dot{L}_e
\\&\implies\quad
    0 = g\sin\theta +\ddot{\theta}L_e + 2\dot{\theta}\dot{L}_e
\end{aligned}
\end{equation*}

\subsubsection{Steady state}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---steady-state}

Suppose we have a resting position for our pendulum. Then,
all the time derivatives are equal to zero and we obtain that

\begin{equation*}
\begin{aligned}
& mg\cos\theta - k(L_e-L_0) = 0
\\& g\sin\theta = 0
\end{aligned}
\end{equation*}

So, \(\theta = 0\) and \(L_e = \frac{mg}{k} +L_0\).
This is as expected

\subsubsection{Linearization}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---linearization}

Next, suppose that \(L_e = L_{equilibrium} + q\) and \(q\) and \(\theta\)
are both small (where \(L_{equilibrium} = \frac{mg}{k} + L_0\)).
When linearizing, we drop all non-linear terms.
Non-linear terms are any terms with degree greater than 1 which
can either be higher order terms in the Taylor series or products of variables.
So, upon linearization, we obtain that

\begin{equation*}
\begin{aligned}
& m (0) + mg(1) -k\left(\frac{mg}{k} + q\right) - m\ddot{q} =0
\implies kq + m\ddot{q} =0
\\
& g\theta + \ddot{\theta}(\frac{mg}{k} + L_0) + 2(0) =0
\implies g\theta + \left(\frac{mg}{k} + L_0\right)\ddot{\theta} =0
\end{aligned}
\end{equation*}

So, we have that both \(q\) and \(\theta\) oscillate with frequencies
\(\omega_0 = \sqrt{\frac{k}{m}}\) and \(\omega_1 = \sqrt{\frac{g}{\frac{mg}{k} + L_0}}\)
respectively.

\subsubsection{Resonance}
\label{develop--math6192:ROOT:page--calculus-of-variations.adoc---resonance}

\begin{admonition-important}[{}]
I skipped the part where we perform regular pertubation expansion.
But in this case, we get the same first order equations but with
\(\theta_0\) and \(q_0\) instead.
\end{admonition-important}

If we just substitute our definition of \(L_e\), we obtain that

\begin{equation*}
q + \frac{k}{m}\ddot{q} = \dot{\theta}^2 L_{equilibrium}- \frac{g}{2}\theta^2
\end{equation*}

and

\begin{equation*}
L_{equilibrium} \ddot{\theta} + g\theta - 2\dot{q}\dot{\theta} - q^2\ddot{\theta}
\end{equation*}

Consider the first of these equations.
From our linearization solution, we see that the right hand side
has frequency of \(2\omega_1\) (since \(\theta \sim e^{i\omega_1 t}\))
while the left has a frequency of \(\omega_0\).
So, we get resonance if these two frequencies are equal.

\begin{equation*}
2\sqrt{\frac{g}{\frac{mg}{k} + L_0}} = 2\omega_1 = \omega_0=\sqrt{\frac{k}{m}}
\end{equation*}

and by rearranging, we get that \(\frac{L_0k}{mg}=3\).
\end{document}
