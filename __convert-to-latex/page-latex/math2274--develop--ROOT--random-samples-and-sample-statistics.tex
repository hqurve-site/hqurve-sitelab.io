\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Random Samples and Sample Statistics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Preface}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---preface}

Sampling is a technique used to obtain information about a population by
extrapolating results from a subset of the population. It is important
to ensure that gathered information is accurate or better
\emph{representative} of the entire population and hence a \emph{random sample}
must be taken. From such a sample, the information obtained can be
condensed into \emph{sample statistics} which are then used to gather
information about the population.

\subsection{Assumptions for this section}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---assumptions-for-this-section}

The theory of sampling is complex and hard to understand without first
understanding ideal cases. Hence, in this section, the sample would be
taken an infinite population otherwise, the random variables from the
sample would not be independent despite being identically distributed.

The explanation for this is quite simple. Although the each of the
distributions are random, the information about the population as a
whole is fixed. Therefore, knowledge of one random variable affects
possible knowledge about another random variable. That is, the
probability of the conditional is not equal to the probability of the
unconditional.

\subsection{Random Sample from a distribution}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---random-sample-from-a-distribution}

A \emph{random sample} of size \(n\) from a population with
distribution \(F\) is a sequence \(\{X_i\}\) (or
\(n\)-tuple) of independent and identically distributed random
variables such that \(X_i \sim F\).

\section{Sample Statistic}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---sample-statistic}

A \emph{sample statistic} of a sample with size \(n\) is simply a
function \(T\) (or transformation) of the
\(\{X_i\}\). Note that \(T\) is itself a random
variable and is used to summarize information about the sample and its
distribution is referred to as a \emph{sampling distribution} as it is the
distribution of the function \(T\) applied to each of the
samples.

\subsection{Note}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---note}

there are no restrictions on the function \(T\) and hence the
information obtained from such a transformation is sometimes useless but
are regardless valid.

\subsection{Unbiased Estimators}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---unbiased-estimators}

A sample statistic, \(\beta^*\), of a population statistic,
\(\beta\), is called \emph{unbiased} if the expectation of the
sample statistic is the population statistic. That is

\begin{equation*}
E[\beta^*] = \beta
\end{equation*}

\section{Sample mean}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---sample-mean}

The average of a sample of \(n\) samples \(\{X_i\}\)
is called the \emph{sample mean} and is defined as

\begin{equation*}
\bar{X}_n = \frac{1}{n} \sum_{i=1}^n X_i
\end{equation*}

\subsection{Mean}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---mean}

Let the population mean be \(\mu\), then

\begin{equation*}
\begin{aligned}
    E\left[ \bar{X} \right]
    = \frac{1}{n}E\left[ \sum_{i=1}^n X_i \right]
    = \frac{1}{n}\sum_{i=1}^nE\left[  X_i \right]
    = \frac{1}{n}\sum_{i=1}^n \mu
    = \frac{1}{n} n \mu
    =\mu\end{aligned}
\end{equation*}

Therefore \(\bar{X}\) is an unbiased estimator for the
population mean.

\subsection{Variance}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---variance}

Let the population mean and variance be \(\mu\) and
\(\sigma^2\) respectively, then

\begin{equation*}
\begin{aligned}
    Var\left[ \bar{X} \right]
    = \frac{1}{n^2}Var\left[ \sum_{i=1}^n X_i \right]
    = \frac{1}{n^2}\left[ \sum_{i=1}^n Var\left[  X_i \right] + \sum_{i < j} Cov\left[ X_i, X_j \right] \right]
    = \frac{1}{n^2}\sum_{i=1}^n \sigma^2
    % = \frac{1}{n^2}n \sigma^2
    = \frac{\sigma^2}{n}\end{aligned}
\end{equation*}

Therefore, as the sample size increases, \(\bar{X}\) becomes a
better and better estimator for the population mean.

\section{Sample Variance}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---sample-variance}

The \emph{sample variance} of \(n\) samples \(\{X_i\}\)
is defined as

\begin{equation*}
S^2_n = \frac{1}{n-1}\sum_{i=1}^n{\left( X_i - \bar{X}_n \right)^2}
\end{equation*}

where \(\bar{X}_n\) is the sample mean. Additionally, the
above expression can be simplified as follows

\begin{equation*}
\begin{aligned}
    S^2_n
    &= \frac{1}{n-1}\sum_{i=1}^n\left( X_i - \bar{X}_n \right)^2\\
    &= \frac{1}{n-1}\sum_{i=1}^n\left[ X_i^2 - 2X_i\bar{X}_n + \bar{X}_n^2 \right]\\
    &= \frac{1}{n-1}\left[ \sum_{i=1}^n X_i^2 - 2\bar{X}_n\sum_{i=1}^n X_i + n\bar{X}_n^2 \right]\\
    &= \frac{1}{n-1}\left[ \sum_{i=1}^n X_i^2 - 2n\bar{X}_n^2 + n\bar{X}_n^2 \right]\\
    &= \frac{1}{n-1}\left[ \sum_{i=1}^n X_i^2 - n\bar{X}_n^2 \right]\end{aligned}
\end{equation*}

\subsection{Note}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---note-2}

the sample variance is not simply the variance of the sample as the
divisor of \((n-1)\) is used instead of \(n\). This
is to ensure that the sample variance is an unbiased estimate for the
population variance.

\subsection{Mean}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---mean-2}

Let the population mean and variance be \(\mu\) and
\(\sigma^2\) respectively, then

\begin{equation*}
\begin{aligned}
    E\left[ S^2_n \right]
    &= \frac{1}{n-1} E\left[ \sum_{i=1}^n X_i^2 - n\bar{X}_n^2 \right]\\
    &= \frac{1}{n-1} \left[ \sum_{i=1}^n E\left[  X_i^2 \right] - nE\left[ \bar{X}_n^2 \right] \right]\\
    &= \frac{1}{n-1} \left[ \sum_{i=1}^n (\sigma^2 + \mu^2) - n\left( \frac{\sigma^2}{n} + \mu^2 \right) \right]\\
    &= \frac{1}{n-1} \left[ n(\sigma^2 + \mu^2) - \left( \sigma^2 + n\mu^2 \right) \right]\\
    &= \frac{1}{n-1} (n-1)\sigma^2\\
    &= \sigma^2\end{aligned}
\end{equation*}

Therefore, \(S^2_n\) is an unbiased estimator for the
population variance.

\section{Sample mean and variance from normal distribution}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---sample-mean-and-variance-from-normal-distribution}

If each of the \(\{X_i\}\) of the sample are independent and
identically distributed with distribution
\(N(\mu, \sigma^2)\), then

\begin{equation*}
\bar{X}_n \sim N\left( \mu, \frac{\sigma^2}{n} \right)
\end{equation*}

and

\begin{equation*}
\frac{(n-1)S^2_n}{\sigma^2} \sim \chi^2_{n-1}
\end{equation*}

The first of these two facts are clear to see however the later is has
not yet been discussed and requires the use of MGFs to easily prove.
Furthermore, the two sample statistics are independent, that is

\begin{equation*}
S^2_n \perp \bar{X}_n
\end{equation*}

\subsection{Remark}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---remark}

I have no idea why this is true or how to easily prove it at this point.

\subsection{Variance of sample variance}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---variance-of-sample-variance}

In this spacial case, the variance of \(S_n^2\) can be written
as

\begin{equation*}
Var\left[ S_n^2 \right] = \frac{2\sigma^4}{n-1}
\end{equation*}

since we know that \(Var[\chi^2_{n-1}] = 2(n-1)\).

\section{Central Limit theorem}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---central-limit-theorem}

The \emph{central limit theorem} (CLT) allows for the approximation of the
distribution of the sample mean of independent and identically
distributed random variables. The theorem is stated below.

Let \(\{X_i\}_1^n\) be a sequence of iid random variables with
finite mean \(\mu\) and variance \(\sigma^2\). Let
\(\bar{X}_n = \frac{1}{n}\sum_{i=1}^n X_i\), then

\begin{equation*}
\frac{\bar{X}_n - \mu}{\sigma / \sqrt{n}} \xrightarrow{d} Z
        \quad\quad\text{where } Z \sim N(0,1)
\end{equation*}

That is, \(\frac{\bar{X}_n - \mu}{\sigma / \sqrt{n}}\)
converges in distribution to the standard normal.

It should be noted that the CLT does not require that the random
variables have a normal distribution, in fact, its main appeal is that
it does not have any constraints on the type of distribution of each of
the \(X_i\).

\subsection{Corollaries}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---corollaries}

Instead of stating that
\(\frac{\bar{X}_n - \mu}{\sigma / \sqrt{n}} \xrightarrow{d} Z\),
the central limit theorem applied as to prove that

\begin{equation*}
\bar{X}_n \approx N\left( \mu, \frac{\sigma^2}{n} \right)
\end{equation*}

or that

\begin{equation*}
\sum_{i=1}^n X_i \approx N\left( n\mu, n\sigma^2 \right)
\end{equation*}

for ''sufficiently large \(n\).''

\subsection{Remark on Binomial approximation to normal}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---remark-on-binomial-approximation-to-normal}

The binomial approximation to the normal relies on the CLT. As the
binomial distribution is just the sum of idd Bernoulli RVs, it is clear
that for sufficiently large \(n\),

\begin{equation*}
X \approx N\left( np, np(1-p) \right)
\end{equation*}

where \(X \sim Binomial(n, p)\).

\subsection{Convergence in distribution}
\label{develop--math2274:ROOT:page--random-samples-and-sample-statistics.adoc---convergence-in-distribution}

Let \(\{Y_i\}\) be a infinite sequence of random variables
where each of the \(Y_i\) have probability function
\(F_i\). Let \(Y\) be a random variable with
probability function \(F\), then \(\{Y_i\}\)
converges to distribution to \(Y\) iff

\begin{equation*}
\lim_{n\to \infty} F_i(x) = F(x)
\end{equation*}

for all \(x \in \mathbb{R}\) at which \(F\) is
continuous.
\end{document}
