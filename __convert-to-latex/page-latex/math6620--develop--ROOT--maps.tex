\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Continuous Maps}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \((X, \mathcal{T}_X)\) and \((Y, \mathcal{T}_Y)\) be topological spaces.
Then, the function \(f: X\to Y\) is continuous if

\begin{equation*}
\forall V \in T_Y: f^{-1}(V) = \{x \in X \ | \ f(x) \in V\} \in T_X
\end{equation*}

\begin{admonition-remark}[{}]
It is only necessary that to prove that the preimage of (sub)basis elements in \(Y\) are open
from property 3 below.
\end{admonition-remark}

\begin{admonition-tip}[{}]
It is important to remember a few properties of images and pre-images.
Namely

\begin{enumerate}[label=\arabic*)]
\item \(f(A \cap B) \subseteq f(A) \cap f(B)\) (also holds for arbitrary intersections)
\item \(f(A \cup B) = f(A) \cup f(B)\) (also holds for arbitrary unions)
\item \(f^{-1}(A \cup B) = f^{-1}(A) \cup f^{-1}(B)\) and \(f^{-1}(A \cap B) = f^{-1}(A) \cap f^{-1}(B)\)
    (also holds for arbitrary unions and intersections)
\item \(f^{-1}(Y - B) = X - f^{-1}(B)\)
\end{enumerate}
\end{admonition-tip}

\section{Equivalent conditions}
\label{develop--math6620:ROOT:page--maps.adoc---equivalent-conditions}

Let \((X, \mathcal{T}_X)\) and \((Y, \mathcal{T}_Y)\) be topological spaces and \(f: X\to Y\).
Then, the following are equivalent

\begin{enumerate}[label=\arabic*)]
\item \(f\) is continuous
\item \(f(\closure_X(A)) \subseteq \closure_Y(f(A))\) for all \(A \subseteq X\).
\item For all closed \(F \subseteq Y\), \(f^{-1}(F)\) is closed in \(X\).
\item For each \(x \in X\) and neighbourhood \(V\) of \(f(x)\), there exists a neighbourhood \(U\)
    of \(x\) such that \(f(U) \subseteq V\)
\end{enumerate}

We say that \(f\) is continuous at \(x\) if statement 4 holds for \(x\).
Therefore, topological continuity is equivalent to continuity in metric spaces.

\begin{proof}[{Equivalence of 1 and 3}]
\begin{admonition-note}[{}]
This proof was obtained from Dr Tweedle's notes.
\end{admonition-note}

Also note that the property that \(f^{-1}(Y-B) = X - f^{-1}(B)\) immediately proves the equivalence of statements 1 and 3.
\end{proof}

\begin{proof}[{Equivalence of 1 and 2}]
\begin{admonition-note}[{}]
This proof was obtained from the book by Munkres
\end{admonition-note}

Suppose that \(f\) is continuous and consider \(f(x) \in f(\closure_X(A))\) (where \(x \in \closure_X(A)\)) and consider neighbourhood \(V\) of \(f(x)\).
Then, \(f^{-1}(V)\) is open and contains \(x\) and hence \(f^{-1}(V) \cap A\) contains
at least one point \(x'\). Since \(f(x') \in V\), \(V\cap f(A)\) is non-empty and
therefore \(f(x) \in \closure_Y(f(A))\).
It follows that \(f(\closure_X(A)) \subseteq \closure_Y(f(A))\).

Now, suppose that \(2\) is true and consider arbitrary open \(V \subseteq Y\).
Suppose BWOC that \(X-f^{-1}(V)\) is not closed.
Then, there exists \(x \in X\) such that \(x \in \closure_X(X-f^{-1}(V))\) and \(x \notin X - f^{-1}(V)\).
This implies that

\begin{equation*}
\begin{aligned}
f\left(\closure_X(X-f^{-1}(V)\right)
&\supset f(X-f^{-1}(V))
\\&= f(f^{-1}(Y-V))
\\&= Y - V
\\&= \closure_Y(Y-V) \quad \text{since } V \text{ is open}
\\&= \closure_Y(f(f^{-1}(Y-V)))
\\&\supseteq f(\closure_X(f^{-1}(Y-V))) \quad\text{ by statement 2}
\\&= f(\closure_X(X - f^{-1}(V)))
\end{aligned}
\end{equation*}

This is a contradiction. Therefore \(X - f^{-1}(V)\) is closed and \(f^{-1}(V)\) is open.
Therefore \(f\) is continuous.
\end{proof}

\begin{proof}[{Equivalence of 1 and 4}]
Suppose that \(f\) is continuous and consider arbitrary \(x \in X\) and neighbourhood \(V\) of \(f(x)\).
Then, since \(f\) is continuous, \(f^{-1}(V)\) is open and contains \(x\) as desired.
Therefore statement 4 is true.

Conversely, suppose that statement 4 is true and consider open \(V \subseteq Y\).
Then, for each \(x \in f^{-1}(V)\) define neighbourhood \(U_x\) of \(x\) such that \(f(U_x) \subseteq f(f^{-1}(V)) = V\).
Then,

\begin{equation*}
f^{-1}(V) = \bigcup_{x \in f^{-1}(V)} U_x
\end{equation*}

is open and \(f\) is continuous.
\end{proof}

\section{Homeomorphisms}
\label{develop--math6620:ROOT:page--maps.adoc---homeomorphisms}

Let \(X\) and \(Y\) be topological spaces.
Then we say that the bijective function \(f: X\to Y\) is a \emph{homeomorphism}
if \(f\) and \(f^{-1}\) are continuous.
Furthermore, since \(f\) is bijective, we see that the open sets in \(X\) have
a on-to-one correspondence with the open sets in \(Y\) since
\((f^{-1})^{-1}(U) = f(U)\) is open and \(f^{-1}(f(U)) = U\).
Therefore, we can equivalently define a homeomorphism to be a bijective function such that
\(f(U)\) is open iff \(U \subseteq X\) is open.

In the case where \(f\) is only injective, but both \(f\) and \(f^{-1}\) are continuous (when restricted
to the image of \(f\)), we say that \(f\) is an \emph{imbedding} of \(X\) in \(Y\).

\begin{admonition-remark}[{}]
Homeomorphisms and imbeddings are analogous to isomorphisms and embeddings in other structures.
\end{admonition-remark}

\section{More results}
\label{develop--math6620:ROOT:page--maps.adoc---more-results}

Let \(X\), \(Y\) and \(Z\) be topological spaces.
Then

\begin{description}
\item[Constant function] The constant function is continuous
\item[Inclusion] If \(A\) is a subspace of \(X\), the inclusion function from \(A\) to \(X\) to continuous
\item[Composition] The composition of two continuous functions is continuous
\item[Restriction of domain] If \(f: X \to Y\) is continuous and \(A\) is a subspace of \(X\),
    the restriction of \(f\) to \(A\), \(f|_A: A\to Y\) is also continuous
\item[Restriction or expansion of range] Suppose that \(f: X \to Y\) is continuous
    
    \begin{enumerate}[label=\alph*)]
    \item If \(Z \subseteq Y\) is a subspace such that \(f(X) \subseteq Z\), then \(f: X \to Z\)
        is continuous
    \item If \(Y \subseteq Z\) is a subspace, then \(f: X \to Z\)
        is continuous
    \end{enumerate}
\end{description}

These results are not too hard to prove

\begin{proposition}[{Local formulation of continuity}]
Let \(f: X \to Y\) and \(\{U_\alpha\}_{\alpha \in I} \subseteq \mathcal{T}_X\) such that
\(X = \bigcup_{\alpha \in I} U_\alpha\).
Then, \(f\) is continuous iff \(f|_{U_\alpha}: U_\alpha \to Y\) is continuous (using the subspace topology)
for each \(\alpha \in I\).

\begin{proof}[{}]
The forward direction is simple. Consider arbitrary \(\alpha \in I\)
and open \(V\subseteq Y\). Then, since \(f\) is continuous, \(f^{-1}(V)\)
is open in \(X\). Also, \(f|_{U_\alpha}^{-1}(V) = f^{-1}(V) \cap U_\alpha\)
is open in subspace \(U_\alpha\).
Therefore, we have the desired result.

Conversely, suppose that each \(f|_{U_\alpha}\) is continuous and consider arbitrary open \(V \subseteq Y\).
Then, each \(f|_{U_\alpha}^{-1}(V)\) is open and hence \(f|_{U_\alpha}^{-1}(V) = U_\alpha \cap W_\alpha\)
by definition of the subspace topology. Therefore,

\begin{equation*}
f^{-1}(V) = \bigcup_{\alpha_I} f|_{U_\alpha}^{-1}(V)
= \bigcup_{\alpha \in I} (U_\alpha \cap W_\alpha)
\end{equation*}

is open as it is the union of open sets. Therefore \(f\) is continuous.
\end{proof}
\end{proposition}

\begin{lemma}[{Pasting lemma (unions of closed domains)}]
Let \(X = A \cup B\) where \(A\) and \(B\) are closed in \(X\).
Let \(f: A \to Y\) and \(B \to Y\) be continuous and \(f(x) = g(x)\) for
each \(x \in A \cap B\).
Then the function defined by

\begin{equation*}
h(x) = \begin{cases}
f(x) \quad\text{if } x \in A\\
g(x) \quad\text{if } x \in B\\
\end{cases}
\end{equation*}

is continuous.

\begin{admonition-note}[{}]
\(h\) is well defined since \(f(x) = g(x)\) on the intersection of the two domains.
\end{admonition-note}

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof is from Dr Tweedle's notes.
\end{admonition-note}

Consider arbitrary closed \(F \subseteq Y\). Then,

\begin{equation*}
h^{-1}(F)
= (h^{-1}(F) \cap A) \cup (h^{-1}(F) \cap B)
= f^{-1}(F) \cup g^{-1}(F)
\end{equation*}

is closed since \(f\) and \(g\) are continuous. Therefore \(h\)
is also continuous
\end{proof}
\end{lemma}

\begin{proposition}[{Product topologies}]
Let \(f: A \to X\) and \(g: A \to Y\) and \(h: A \to X \times Y\) be
defined by \(h(x) = (f(x), g(x))\).
Then, \(h\) is continuous iff \(f\) and \(g\) are continuous.

\begin{proof}[{}]
Suppose that \(h\) is continuous and consider arbitrary open \(U \subseteq X\)
and \(V \subseteq Y\).
Then,

\begin{equation*}
f^{-1}(U) = \{a \ | \ f(a) \in U\} = \{a \ | \ f(a) \in U \text{ and } g(a) \in Y\} = h^{-1}(U \times Y)
\end{equation*}

Therefore \(f^{-1}(U)\) is open and \(f\) is continuous. By symmetry, \(g\) is also continuous.

Conversely, suppose that \(f\) and \(g\) are both continuous.
Recall that a function is continuous iff the pre-image of basis elements is open.
So consider arbitrary open \(U \times V\). Then,

\begin{equation*}
h^{-1}(U \times V) = \{a \ | \ f(a) \in U \text{ and } g(a) \in V\} = f^{-1}(U) \cap g^{-1}(V)
\end{equation*}

Therefore, \(h\) must be continuous as desired.
\end{proof}
\end{proposition}

This proposition may be used to show that polynomials are continuous.
To do this, we need to show that

\begin{itemize}
\item constant functions are continuous
\item The identity function is continuous
\item the operation \( + : X\times X \to X\) is continuous.
    Then for any two continuous functions \((f,g)\) is continuous and hence \((+)\circ (f,g)\) is also continuous.
\item the operation \(\times: X\times X \to X\) is continuous.
    Then for any two continuous functions \((f,g)\) is continuous and hence \((\times)\circ (f,g)\) is also continuous.
\end{itemize}

\begin{proposition}[{Continuation from dense subset}]
Let \(f: X\to Y\) be continuous and \(Y\) be Hausdorff.
Then, \(f\) is defined by its values on any dense subset of \(X\).

That is, if \(f, f': X\to Y\) are continuous and agree on a dense subset \(A \subseteq X\),
\(f=f'\).

\begin{proof}[{}]
Let \(f, g: X\to Y\) continuous and agree on a dense subset \(A\subseteq X\).
Suppose there exists a \(x \in X - A\) such that \(f(x) \neq g(x)\).
Then, since \(Y\) is Hausdorff, there exists disjoint \(U, V \subseteq Y\)
such that \(f(x) \subseteq U\) and \(g(x) \subseteq V\).

Consider \(W=f^{-1}(U)\cap g^{-1}(V)\) which must be an open neighbourhood of \(x\).
Since \(x \in X =\closure A - A = \limit A\), there exists
\(y\in W\cap A\) which differs from \(x\).
This implies that \(f(y)\in U\) and \(g(y) \in V\).
Also since \(y\in A\), \(f(y)=g(y)\) and hence \(f(y)=g(y) \in U\cap V\).
This contradicts the choice of disjoint \(U\) and \(V\) and hence out assumption
of the existence of \(x\) was incorrect.
\end{proof}
\end{proposition}
\end{document}
