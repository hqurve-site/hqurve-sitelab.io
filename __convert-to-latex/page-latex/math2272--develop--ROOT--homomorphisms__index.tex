\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Homomorphisms and Isomorphisms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \((G, \cdot)\) and \((H, \star)\) be two groups
and \(f: G\to H\). Then \(f\) is called a \emph{group
homomorphism} if

\begin{equation*}
f(a\cdot b) = f(a) \star f(b) \ \forall a, b \in G
\end{equation*}

That is, \(f\) ''preserves the structure of \(G\).''

\section{Nice properties}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---nice-properties}

Let \((G, \cdot)\) and \((H, \star)\) be groups with
\(f\) being an homomorphism from \(G\) to
\(H\) then

\begin{enumerate}[label=\arabic*)]
\item \(f(e_G) = e_H\)
\item \(f(a^{-1}) = f(a)^{-1}\ \forall a \in G\)
\end{enumerate}

and hence the image of \(f\) is a subgroup of \(H\).

\begin{example}[{Proof}]
Firstly, notice that

\begin{equation*}
f(e_G) \star f(e_G) = f(e_G \cdot e_G) = f(e_G) = f(e_G) \star e_H
\end{equation*}

and the result follows by the cancellation property.

Next, let \(a \in G\) and notice that

\begin{equation*}
f(a) \star f(a^{-1}) = f(a \cdot a^{-1}) = f(e_G) = e_H = f(a) \star f(a)^{-1}
\end{equation*}

and again the result follows by the cancellation property.

 ◻
\end{example}

\section{Order}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---order}

Let \((G, \cdot)\) and \((H, \star)\) be groups with
\(f\colon G \to H\) being an homomorphism and
\(a \in G\). Then, if the order of \(a\) is finite,
then the order of \(f(a)\) divides the order of
\(a\).

\begin{example}[{Proof}]
Let \(a \in G\) have finite order \(n\). Then

\begin{equation*}
f(a)^n = f(a^n) = f(e_G) = e_H
\end{equation*}

therefore, the order of \(f(a) \leq n\) and is also finite.
Next, let the order of \(f(a)\) be \(m\) and by the
usual division algorithm procedure, \(n = qm\) and we are
done.

 ◻
\end{example}

\section{Isomorphisms}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---isomorphisms}

Let \((G, \cdot)\) and \((H, \star)\) be groups with
\(f\colon G \to H\) being an homomorphism. Then, if
\(f\) is bijective, we call \(f\) an \emph{isomorphism}.
Furthermore, if there exists an isomorphism between the two groups, we
call them isomorphic and write

\begin{equation*}
(G, \cdot) \cong (H, \star)
\end{equation*}

Note that \(\cong\) is an equivalence relation (the necessary
properties will be proven later). Additionally, if
\((G, \cdot) = (H, \star)\) we call \(f\) an
\emph{automorphism}.

\subsection{Identity as an automorphism}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---identity-as-an-automorphism}

Let \((G, \cdot)\) be a group. Then \((G, \cdot)\)
is isomorphic with itself since the identity is an automorphism. This is
easily seen since

\begin{equation*}
\forall a, b \in G: id(a) \cdot id(b) = a \cdot b = id(a\cdot b)
\end{equation*}

\subsection{Inverse as isomorphism}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---inverse-as-isomorphism}

Let \((G, \cdot)\) and \((H, \star)\) be groups with
\(f\colon:G \to H\) being an isomorphism. Then, we claim that
\(f^{-1}\) is also an isomorphism and hence

\begin{equation*}
(G, \cdot) \cong (H, \star) \implies (H, \star) \cong (G, \cdot)
\end{equation*}

\begin{example}[{Proof}]
Firstly, since \(f\) is bijective, then \(f^{-1}\)
exists and is also bijective. Then, consider \(x, y \in H\).
Then we get

\begin{equation*}
f^{-1}(x \star y)
        = f^{-1}(f(f^{-1}(x)) \star f(f^{-1}(y)))
        = f^{-1}(f(f^{-1}(x) \cdot f^{-1}(y)))
        = f^{-1}(x) \cdot f^{-1}(y)
\end{equation*}

and we are done.

 ◻
\end{example}

\subsection{Composition of isomorphisms}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---composition-of-isomorphisms}

Let \((F, \oplus)\), \((G, \cdot)\) and
\((H, \star)\) be groups with \(f\colon F \to G\)
and \(g\colon G\to H\) both being isomorphisms. Then,
\(f\circ g\) is also an isomorphism.

\begin{example}[{Proof}]
Firstly, since \(f\) and \(g\) are both bijections,
\(f\circ g\) is also bijective. Now, consider
\(a, b \in F\), then

\begin{equation*}
fg(a\oplus b) = f(g(a) \cdot g(b)) = fg(a) \star fg(b)
\end{equation*}

and we are done.

 ◻
\end{example}

\subsection{Preservation of order}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---preservation-of-order}

Let \((G, \cdot)\) and \((H, \star)\) be groups and
\(f\colon G \to H\) be an isomorphism. Then if the order of
\(a \in G\) is finite, it is equal to the order of
\(f(a)\). Otherwise, if it is infinite, both have infinite
order.

\begin{example}[{Proof}]
First, consider when the order of \(a\) is finite. Then, the
order of \(f(a)\) is also finite and divides the order of
\(a\). Next, since \(f^{-1}\) is also an
isomorphism, and \(f(a)\) has finite order, the order of
\(f^{-1}f(a) = a\) divides the order of \(f(a)\).
Therefore, the two orders are equal.

On the other hand, suppose that \(a\) had infinite order, then
\(f(a)\) must also have infinite order otherwise we would get
a contradiction since \(f^{-1}\) is an isomorphism.

 ◻
\end{example}

\subsection{Cylic isomorphic groups}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---cylic-isomorphic-groups}

Let \((G, \cdot)\) be a cyclic group with generator
\(g \in G\). Then, \((G, \cdot)\) is isomorphic to
\(C_n\) where \(|G| = n\) is finite, otherwise,
\((G, \cdot)\) is isomorphic to \((\mathbb{Z}, +)\).

\begin{example}[{Proof}]
Firstly, suppose that \(|G| = n\) is finite, then, let
\(C_n = gp(c)\). We define the function
\(f\colon G \to C_n\) as

\begin{equation*}
f(g^k) = c^k
\end{equation*}

Then, note that each element in \(G\) and \(C_n\)
can be can be written as a unique power of the generator when restricted
to \(\{0\ldots, (n-1)\}\). So, \(f\) is also
bijective. Also \(f\) is a homomorphism since

\begin{equation*}
f(g^j g^k) = f(g^{j+k}) = c^{j+k} = c^j c^k = f(g^j)f(c^k)
\end{equation*}

and hence \((G, \cdot) \cong (C_n, \cdot)\)

Now, suppose that \(|G|\) is infinite. Then we define the
function \(f\colon G \to \mathbb{Z}\) as

\begin{equation*}
f(g^k) = k
\end{equation*}

Since each element in \(g\) can be written as a unique power
of \(g\) (otherwise infiniteness would be invalidated) and
hence \(f\) is injective. Also, clearly, \(f\) is
surjective. Finally, note that \(f\) is a homomorphism since

\begin{equation*}
f(g^j g^k) = f(g^{j+k}) = j+k = f(g^j)f(c^k)
\end{equation*}

and hence \((G, \cdot) \cong (\mathbb{Z}, +)\).

 ◻
\end{example}

\section{Kernel}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---kernel}

Let \((G, \cdot)\) and \((H, \star)\) be two groups
with \(f\colon G \to H\) being a homomorphism. Then, the
\emph{kernel} of \(f\) is defined as

\begin{equation*}
\ker f = \{g \in G: f(g) = e_H\} = f^{-1}\{e_H\}
\end{equation*}

then

\begin{enumerate}[label=\arabic*)]
\item \(\ker f\) is a normal subgroup of \(G\).
\item \(f\) is injective iff \(\ker f = \{e_G\}\)
\end{enumerate}

\begin{example}[{Proof}]
Firstly, note that \(\ker f \neq \varnothing\) since
\(f(e_G) = e_H\) and hence \(e_G \in \ker f\). Also,
consider arbitrary \(g \in G\) and \(h \in H\), then

\begin{equation*}
f(g^{-1}hg)
        = f(g)^{-1}f(h) f(g)
        = f(g)^{-1}e_H f(g)
        = f(g)^{-1} f(g)
        = e_H
\end{equation*}

and hence \(g^{-1} h g \in H\). Therefore, by the normal
subgroup criterion, the result follows.

The forward direction is immediate since \(f(e_G) = e_H\).
Now, consider the converse and suppose \(f(g_1) = f(g_2)\)
where \(g_1, g_2 \in G\). Then

\begin{equation*}
f(g_1g_2^{-1}) = f(g_1)f(g_2)^{-1} = f(g_1)f(g_1)^{-1} = e_H
\end{equation*}

therefore \(g_1g_2^{-1} = e_H = g_2g_2^{-1}\) and the result
follows by cancellation property.

 ◻
\end{example}

\section{Direct products}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---direct-products}

Let \((G, \cdot)\) and \((H, \star)\) be groups,
then \((G\times H, \circ)\) is also a group where
\(\circ\) is defined by

\begin{equation*}
\forall (g_1, h_1), (g_2, h_2) \in G\times H:
    (g_1, h_1)\circ (g_2, h_2) = (g_1 \cdot g_2, h_1\star h_2)
\end{equation*}

\section{First Isomorphism Theorem}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---first-isomorphism-theorem}

Let \((G, \cdot)\) and \((H, \star)\) be two groups
with \(f\colon G \to H\) being a homomorphism. Then
\(G/\ker f\) is isomorphic to the image of \(G\) in
\(f\) with isomorphism
\(\Psi\colon G/\ker f \to f(G)\) defined by
\(\Psi(Kg) = f(g)\).

\begin{example}[{Proof}]
Let \(K = \ker f\). Then, we first need to show that \(\Psi\) is well defined.
Consider \(Kg_1 = Kg_2 \in G/K\) then \(g_1g_2^{-1} \in K\) and

\begin{equation*}
f(g_1)f(g_2)^{-1} = f(g_1g_2^{-1}) = e_H
\end{equation*}

Then, by the uniqueness of inverse \(\Psi(Kg_1) = f(g_1) = f(g_2) = \Psi(Kg_2)\) and
\(\Psi\) is well defined.

Next, \(\Psi\) is a homomorphism since

\begin{equation*}
\Psi((Kg_1)(Kg_2)) = \Psi(Kg_1g_2) = f(g_1g_2) = f(g_1)f(g_2) = \Psi(Kg_1)\Psi(Kg_2)
\end{equation*}

Next, \(\Psi\) is injective since

\begin{equation*}
\begin{aligned}
\Psi(Kg_1) = \Psi(Kg_2)
&\implies f(g_1) = f(g_2)
\\&\implies f(g_1g_2^{-1}) = f(g_1)f(g_2)^{-1} = e_H
\\&\implies g_1g_2^{-1} \in K
\\&\implies Kg_1 = Kg_2
\end{aligned}
\end{equation*}

Finally, \(\Psi\) is surjective since

\begin{equation*}
\forall f(g) \in f(G): \exists Kg \in G/K: \Psi(Kg) = f(g)
\end{equation*}

Therefore, \(\Psi\) is an isomorphism from \(G/K\) to \(f(G)\).
\end{example}

\subsection{Nice result}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---nice-result}

Let \(G_1\) and \(G_2\) be groups with subgroups
\(K_1 \Delta G_1\) and \(K_2 \Delta G_2\). Then,

\begin{equation*}
(K_1 \times K_2) \Delta (G_1 \times G_2)
    \quad\text{and}\quad
    (G_1 \times G_2)/(K_1 \times K_2) \cong (G_1 / K_1) \times (G_2 / K_2)
\end{equation*}

Note that this follows from the first isomorphism theorem since
\(f:(G_1 \times G_2) \to  (G_1 / K_1) \times (G_2 / K_2)\) is
an onto homomorphism with kernel \(K_1 \times K_2\) defined by

\begin{equation*}
f(g_1, g_2) \to (g_1K, g_2K)
\end{equation*}

\subsection{Second Isomorphism Theorem}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---second-isomorphism-theorem}

Let \(G\) be a group with subgroup \(G\) and normal
subgroup \(N\). THen \(HN\) (set of products) is a
subgroup of \(G\), \(H \cap N\) is a normal subgroup
of \(H\) and

\begin{equation*}
H / H \cap N \cong HN / N
\end{equation*}

\subsection{Thid Isomorphism Theorem}
\label{develop--math2272:ROOT:page--homomorphisms/index.adoc---thid-isomorphism-theorem}

Let \(G\) be a group with normal subgroups \(M\) and
\(N\) where \(N \Delta M\). Then,
\(\phi: G/N \to G/M\) where \(\phi(Ng) = Mg\) is a
well defined homomorphism and

\begin{equation*}
(G/N) / (M/N) \cong G /M
\end{equation*}
\end{document}
