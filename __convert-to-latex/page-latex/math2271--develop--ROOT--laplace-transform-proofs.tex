\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Laplace Transformation Proofs}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

{} \def\paren#1{\left({#1}\right)}
% Title omitted
\section{Reciprocal Square Root}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---reciprocal-square-root}

Let \(f(t) = \frac{1}{\sqrt{t}}\), then
\(\mathcal{L}[f] = \frac{\sqrt{\pi}}{\sqrt{s}}\) for all \(s > 0\).

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
  \mathcal{L}[f]
  &= \int_0^\infty \frac{e^{-st}}{\sqrt{t}} \ dt \quad\text{let } st = x^2 \implies s\ dt = 2x \ dx
\\&= \int_0^\infty \frac{e^{-x}}{\sqrt{\frac{x^2}{s}}} \frac{2x}{s}\ dx
\\&= \frac{2}{\sqrt{s}}\int_0^\infty e^{-x^2}\ dx
\\&= \frac{2}{\sqrt{s}} \frac{\sqrt{\pi}}{2} \quad\text{by gaussian integral}
\\&= \frac{\sqrt{\pi}}{\sqrt{s}}
\end{aligned}
\end{equation*}
\end{example}

\section{Product of power and exponential}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---product-of-power-and-exponential}

Let \(f(t) = t^r e^{at}\) then,

\begin{equation*}
\mathcal{L}[f] = \frac{\Gamma(r+1)}{(s-a)^{r+1}}
\end{equation*}

for \(s > a\).

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
    \mathcal{L}[f]
    &= \int_0^\infty e^{-st}t^r e^{at} \ dt
  \\&= \int_0^\infty e^{(a-s)t}t^r\ dt \quad\text{let } x = (s-a)t > 0
  \\&= \int_0^\infty e^{-x}\left(\frac{x}{s-a}\right)^r \frac{1}{s-a}\ dx
  \\&= \frac{1}{(s-a)^{r+1}}\int_0^\infty e^{-x}x^r\ dx
  \\&= \frac{\Gamma(r+1)}{(s-a)^{r+1}}
\end{aligned}
\end{equation*}
\end{example}

\section{Complex exponential}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---complex-exponential}

Let \(f(t) = e^{wt}\) where \(w \in \mathbb{C}\), then

\begin{equation*}
\mathcal{L}[f] = \frac{1}{s-w}
\end{equation*}

where \(s > \Re(w)\).

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
    \mathcal{L}[f]
    &= \int_{0}^{\infty} e^{-st}e^{wt} \ dt
  \\&= \int_{0}^{\infty} e^{(w-s)t} \ dt
  \\&= \frac{1}{w-s} \left. e^{(w-s)t} \right|_0^\infty
  \\&= \frac{-1}{w-s} + \lim_{t \to \infty} e^{(w-s)t}
  \\&= \frac{-1}{w-s}
\end{aligned}
\end{equation*}

Note that the last limit converges iff

\begin{equation*}
|e^{w-s}| < 1
\iff \Re(w-s) < 0
\iff s > \Re(w)
\end{equation*}
\end{example}

\section{Sine and cosine functions}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---sine-and-cosine-functions}

Let \(f(t) = \sin(kt)\) and \(g(t) = \cos(kt)\) where \(k \in \mathbb{R}\), then

\begin{equation*}
\mathcal{L}[f] = \frac{k}{s^2 + k^2}
\quad\text{and}\quad
\mathcal{L}[g] = \frac{s}{s^2 + k^2}
\end{equation*}

where \(s > 0\).

\begin{admonition-remark}[{}]
This was done as a pair as their results depend on each other.
\end{admonition-remark}

\begin{example}[{Proof}]
Firstly,

\begin{equation*}
\begin{aligned}
    \mathcal{L}[f]
    &= \int_0^\infty e^{-st}\sin(kt) \ dt
  \\&= \int_0^\infty \sin(kt) \ d\paren{\frac{e^{-st}}{-s}}
  \\&= \left. \sin(kt) \paren{\frac{e^{-st}}{-s}} \right|_0^\infty + \frac{1}{s}\int_0^\infty e^{-st} \ d(\sin(kt))
  \\&= 0 - 0 + \frac{k}{s}\int_0^\infty e^{-st} \cos(kt) \ dt
  \\&= \frac{k}{s}\mathcal{L}[g]
\end{aligned}
\end{equation*}

Note the above limit converges because \(|e^{-st}| < 1 \iff -s < 0 \iff s > 0\).
Next,

\begin{equation*}
\begin{aligned}
    \mathcal{L}[f]
    &= \int_0^\infty e^{-st}\sin(kt) \ dt
  \\&= \int_0^\infty e^{-st}\ d \paren{\frac{\cos(kt)}{-k}}
  \\&= \left. e^{-st} \paren{\frac{\cos(kt)}{-k}} \right|_0^\infty + \frac{1}{k}\int_0^\infty \cos(kt) \ d(e^{-st})
  \\&= 0 - \frac{1}{-k} - \frac{s}{k}\int_0^\infty e^{-st} \cos(kt) \ dt
  \\&= \frac{1}{k} - \frac{s}{k}\mathcal{L}[g]
  \\&= \frac{1}{k} - \frac{s^2}{k^2}\mathcal{L}[f]
\end{aligned}
\end{equation*}

Note the above limit converges because \(|e^{-st}| < 1 \iff -s < 0 \iff s > 0\).
Therefore,

\begin{equation*}
\mathcal{L}[f]\paren{1 + \frac{s^2}{k^2}}  = \frac{1}{k}
\implies\mathcal{L}[f]
= \frac{1}{k\paren{1 + \frac{s^2}{k^2}}}
= \frac{k}{s^2 + k^2}
\end{equation*}

and

\begin{equation*}
    \mathcal{L}[g]
    = \frac{s}{k} \paren{\frac{k}{s^2 + k^2}}
    = \frac{s}{s^2 + k^2}
\end{equation*}
\end{example}

\section{Hyperbolic sine and cosine}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---hyperbolic-sine-and-cosine}

Let \(f(x) = \sinh(kt)\) and \(g(t) = \cosh(kt)\) where \(k \in \mathbb{R}\)
then,

\begin{equation*}
\mathcal{L}[f] = \frac{k}{s^2 - k^2}
\quad\text{and}\quad
\mathcal{L}[g] = \frac{s}{s^2 - k^2}
\end{equation*}

where \(s > |k|\).

\begin{example}[{Proof}]
By, definition \(\sinh(kt) = \frac{e^{kt} - e^{-kt}}{2}\) and \(\cosh(kt) = \frac{e^{kt} + e^{-kt}}{2}\).
Then,

\begin{equation*}
\mathcal{L}[f]
= \frac{1}{2}\paren{\mathcal{L}[e^{kt}] - \mathcal{L}[e^{-kt}] }
= \frac{1}{2}\paren{\frac{1}{s-k} - \frac{1}{s+k}}
= \frac{k}{s^2-k^2}
\end{equation*}

Now, notice that if \(s > -|k|\), it must also be greater than \(|k|\) since,
if one of the Laplacians converge, the other must as well. On the other hand,
what if \(s < -|k|\); we cannot be sure that it doesn't converge since
we are dealing with the difference of two positive values.
We need to use the definition to prove that this laplacian
would not converge

\begin{equation*}
\begin{aligned}
    \mathcal{L}[f]
    &= \frac12 \int_0^{\infty} e^{-st} (e^{kt} - e^{-kt}) \ dt
  \\&= \frac12 \int_0^\infty e^{(k-s)t} - e^{-(k+s)t} \ dt
  \\&= \frac12 \left[\frac{e^{(k-s)t}}{k-s} + \frac{e^{-(k+s)t}}{k+s}\right]_0^\infty
  \\&= \frac12 \left[\frac{(k+s)e^{(k-s)t} + (k-s)e^{-(k+s)t}}{k^2-s^2}\right]_0^\infty
  \\&= \frac12 \left[e^{-st} \frac{k(e^{kt} + e^{-kt}) + s(e^{kt} - e^{-kt})}{k^2-s^2}\right]_0^\infty
\end{aligned}
\end{equation*}

Now, if

\begin{description}
\item[Case 1] \(k > 0\), then \(e^{-kt} \to 0\) and
    
    \begin{equation*}
        \mathcal{L}[f]
        = \frac12 \left[e^{-st} \frac{(k + s)e^{kt}}{k^2-s^2}\right]_0^\infty
        = \frac12 \left[\frac{e^{(k-s)t}}{k-s}\right]_0^\infty
    \end{equation*}
    
    and since \(s < -|k| = -k < 0\), \(k-s > 0\) and the above integral diverges.
\item[Case 2] \(k < 0\), then \(e^{kt} \to 0\) and
\end{description}

\begin{equation*}
    \mathcal{L}[f]
    = \frac12 \left[ e^{-st}\frac{(k-s)e^{-kt}}{k^2-s^2} \right]_0^\infty
    = \frac12 \left[ \frac{e^{-(k+s)t}}{k+s} \right]_0^\infty
\end{equation*}

and since \(s < -|k| = k < 0\), \(-(k+s) > 0\) and hence the above integral diverges.

Therefore, the \(\mathcal{L}[f]\) is only valid for \(s > |k|\).

Next, for \(\mathcal{L}[g]\), note that

\begin{equation*}
    \mathcal{L}[f]
    = \frac{1}{2}\paren{\mathcal{L}[e^{kt}] + \mathcal{L}[e^{-kt}] }
    = \frac{1}{2}\paren{\frac{1}{s-k} + \frac{1}{s+k}}
    = \frac{s}{s^2-k^2}
\end{equation*}

then, since we are dealing with the sum of two positive terms, the laplacian of \(g\) converges iff
\(s > k\) and \(s > -k\). That is, \(\mathcal{L}[g]\) is only valid for \(s > |k|\).
\end{example}

\section{\(s\)-shift}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---latex-backslashs-latex-backslash-shift}

Let \(y:[0, \infty) \to \mathbb{R}\) where \(\mathcal{L}[y] = Y(s)\)
is valid for \(s \in I\), then

\begin{equation*}
    \mathcal{L}[e^{at}y(t)] = Y(s-a)
\end{equation*}

which is valid for \(s-a \in I\).

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
    \mathcal{L}[e^{at}y(t)]
    &= \int_0^{\infty} e^{-st} e^{at}y(t) \ dt
  \\& = \int_0^{\infty} e^{-(s-a)t} y(t) \ dt
  \\&= Y(s-a)
\end{aligned}
\end{equation*}

which is valid for \(s-a \in I\).
\end{example}

\section{\(t\)-shift}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---latex-backslasht-latex-backslash-shift}

Let \(y:[0, \infty) \to \mathbb{R}\) where \(\mathcal{L}[y] = Y(s)\)
is valid for \(s \in I\), then

\begin{equation*}
    \mathcal{L}[H(t-t_0)y(t-t_0)]
    = e^{-st_{0}}Y(s)
    \quad\text{where } t_0 \geq 0
\end{equation*}

which is valid for \(s \in I\).

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
    \mathcal{L}[H(t-t_0)y(t-t_0)]
    &= \int_0^{\infty} e^{-st}H(t-t_0)y(t-t_0) \ dt
  \\&= \int_0^{t_0} e^{-st}H(t-t_0)y(t-t_0) \ dt + \int_{t_0}^{\infty} e^{-st}H(t-t_0)y(t-t_0) \ dt
  \\&= \int_0^{t_0} 0 \ dt + \int_{t_0}^{\infty} e^{-st}H(t-t_0)y(t-t_0) \ dt
  \\&= \int_{t_0}^{\infty} e^{-st}H(t-t_0)y(t-t_0) \ dt
  \\&= \int_{0}^{\infty} e^{-s(t+t_0)}H(t)y(t) \ dt
  \\&= e^{-st_0}\int_{0}^{\infty} e^{-st}H(t)y(t) \ dt
  \\&= e^{-st_0} Y(s)
\end{aligned}
\end{equation*}

which has the same domain of validity.
\end{example}

\section{Dirac delta function}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---dirac-delta-function}

Let \(y:[0, \infty) \to \mathbb{R}\) and \(t_0 \geq 0\), then

\begin{equation*}
    \mathcal{L}[\delta(t-t_0)y(t)] = e^{-st_0}y(t_0)
\end{equation*}

\begin{example}[{Proof}]
This follows from definition

\begin{equation*}
    \mathcal{L}[\delta(t-t_0)y(t)] = \int_0^\infty e^{-st}\delta(t-t_0)y(t) \ dt =  e^{-st_0}y(t_0)
\end{equation*}
\end{example}

\section{Derivaties}
\label{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---derivaties}

Let \(y:[0, \infty) \to \mathbb{R}\) where \(\mathcal{L}[y] = Y(s)\)
is valid on \(I\). Then

\begin{equation*}
    \mathcal{L}[y'] = sY(s) - y(0)
    \quad\text{and in general}\quad
    L[y^{(n)}] = s^n Y(s) - \sum_{m=0}^{n-1} s^{(n-1)- m} y^{(m)}(0)
\end{equation*}

which are valid on \(I\).
\end{document}
