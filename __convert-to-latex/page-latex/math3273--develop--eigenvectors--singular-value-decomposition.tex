\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Singular Value decomposition}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
\section{Positive definite and positive semi-definite matrices}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---positive-definite-and-positive-semi-definite-matrices}

Let \(A \in \mathbb{C}^{n\times n}\) be hermatian then all of its
eigenvalues are real [\myautoref[{see here}]{develop--math3273:eigenvectors:page--index.adoc--normal-matrices}].
We then say that \(A\) is \emph{positive definite} iff one of the following equivalent
statements hold

\begin{itemize}
\item \(\forall \vec{x} \in \mathbb{C}^n \backslash \{\vec{0}\}: \vec{x}^*A\vec{x} > 0 \)
\item All the eigenvalues of \(A\) are positive
\end{itemize}

\begin{example}[{Proof of equivalence}]
Firstly, if all the eigenvalues of \(A\) are positive, \(A\) is similar to
a diagonal matrix with positive entries (eigenvalues). Hence

\begin{equation*}
\vec{x}^* A \vec{x} = \vec{x}^* U D U^* \vec{x} = \vec{y}^*D \vec{y} = \sum_{i=1}^n \lambda_i \overline{y_i}y_i > 0
\end{equation*}

where \(\vec{y} = U^*\vec{x}\) since all the \(\lambda_i > 0\).

Conversely, if

\begin{equation*}
\vec{x}^* A \vec{x} = \vec{x}^* U D U^* \vec{x} = \vec{y}^*D \vec{y} = \sum_{i=1}^n \lambda_i \overline{y_i}y_i > 0
\end{equation*}

We can limit \(\vec{x}\) such that only one of the \(y_i \neq 0\) and hence show one by one that each of the \(\lambda_i > 0\).
\end{example}

Furthermore, if the eigenvalues of \(A\) are non-negative or equality holds in the first
statement, we say that \(A\) is \emph{positive semi-definite}.

\begin{admonition-note}[{}]
Analogous definitions are made for real symmetric matrices.
\end{admonition-note}

\subsection{Square roots}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---square-roots}

Let \(A\) be positive definite. Then there exists \(B\) such that
\(B^2 = A\).

To see this let \(A = UDU^*\) and notice that since all of the eigenvalues of
\(A\) are positive, the entries in \(A\) are all positive and hence there exists
\(D^{1/2}\) where \((D^{1/2})^2 = D\). Finally we let \(B = UD^{1/2}U^*\) and notice

\begin{equation*}
B^2 = UD^{1/2}U^*UD^{1/2}U^* = UD^{1/2}D^{1/2}U^* = UDU^* = A
\end{equation*}

\section{Singular Values}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---singular-values}

Let \(A \in \mathbb{C}^{m\times n}\) and \(r = rank(A)\). Then \(AA^*\)
and \(A^*A\) are both positive semi-definite with the same non-zero eigenvalues

\begin{equation*}
\sigma_1^2, \sigma_2^2, \ldots \sigma_r^2
\end{equation*}

\begin{example}[{Proof}]
Firstly, we can clearly see that \(A^*A\) is positive definite since

\begin{equation*}
\forall \vec{x} \in \mathbb{C}^n:
\vec{x}^*A^*A\vec{x}
= (A\vec{x})^*(A\vec{x})
= \|A \vec{x}\|^2 \geq 0
\end{equation*}

Likewise for \(AA^*\). Therefore all of its eigenvalues are non-negative.

Now, consider arbitrary \(\lambda \in \mathbb{C} \backslash\{0\}\). We want to show that

\begin{equation*}
\dim(\ker(AA^* - \lambda))
=
\dim(\ker(A^*A - \lambda))
\end{equation*}

We would show the inequality from both sides.

\begin{example}[{Proof that \(\dim(\ker(A^*A - \lambda)) \leq \dim(\ker(AA^* - \lambda))\)}]
Firstly, if \(\ker(A^*A - \lambda) =\{\vec{0}\}\), the inequality holds trivially.
Else let \(\vec{v}_1, \ldots \vec{v}_k\) be a basis for \(\ker(A^*A - \lambda)\).
Then, \(A\vec{v}_1,A\vec{v}_2, \ldots A\vec{v}_k\) are linearly independent since

\begin{equation*}
\vec{0} = \sum_{i=1}^k c_iA\vec{v}_i
\implies \vec{0} = A^*\vec{0} = \sum_{i=1}^k c_iA^*A\vec{v}_i = \sum_{i=1}^k c_i\lambda\vec{v}_i
\implies c_i =0
\end{equation*}

since \(\lambda\neq 0\).
Also, notice that

\begin{equation*}
(AA^* - \lambda)(A\vec{v}_i)
= AA^*A\vec{v}_i - \lambda A\vec{v}_i
= A(A^*A -\lambda)\vec{v}_i = \vec{0}
\end{equation*}

Therefore, \(A\vec{v}_i \in \ker(AA^* - \lambda)\). Then since we have a linear independent
set of \(k\) vectors

\begin{equation*}
k = \dim(\ker(A^*A - \lambda)) \leq \dim(\ker(AA^* - \lambda))
\end{equation*}
\end{example}

We can then replace \(A\) with \(A^*\) and get the reverse inequality.

This result firstly implies that \(\lambda \neq 0\) is an eigenvector of \(AA^*\) iff
it is an eigenvector of \(A^*A\). Additionally, it shows that the algebraic multiplicity
must also be the same.

Now, we to show that there are precisely \(r\) non-zero eigenvalues (with multiplicity).
Firstly notice that \(\ker(A) = \ker(A^*A)\) otherwise there would exist \(\vec{x} \notin \ker(A)\)
such that \(A^*A\vec{x} = \vec{0}\) however, \(\vec{x}^*A^*A\vec{x} = \|A\vec{x}\|^2 > 0\).
Therefore,

\begin{equation*}
rank(A^*A) = rank(A) = rank(A^*) = rank(AA^*)
\end{equation*}

Then since \(rank(A) = r\) and \(A^*A\) is hermatian an hence has all of its eigenvectors, there are precisely
\(r\) non-zero eigenvalues. Likewise \(A^*A\) has precisely \(r\) non-zero eigenvalues.
\end{example}

We then let \(\sigma_1, \ldots \sigma_r\) be the positive square roots
of \(\sigma_1^2, \ldots \sigma_r^2\) and \(\sigma_{r+1} = \cdots =\sigma_q = 0\).
We call the set of \(\sigma_i\), the \emph{singular values} of \(A\). Furthermore, we order the signgular
values in non-increasing order. That is

\begin{equation*}
\sigma_1 \geq \sigma_2 \geq \cdots \geq \sigma_q
\end{equation*}

\section{Decomposition}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---decomposition}

Let \(A \in \mathbb{C}^{m\times n}\) and we define

\begin{equation*}
\sum = \begin{pmatrix}
\small \begin{matrix}
\sigma_1 & & & \\
& \sigma_2 & & \\
& & \ddots & \\
& & & \sigma_r \\
\end{matrix} & \Large 0 \\
\Large 0 & \Large 0
\end{pmatrix} \in \mathbb{R}^{m\times n}
\end{equation*}

where \(\sigma_i\) is the i'th non-zero singular value.
Then there exists unitary \(V \in \mathbb{C}^{m\times m}\) and unitary \(W \in \mathbb{C}^{n\times n}\) such
that

\begin{equation*}
A = V\Sigma W^*
\end{equation*}

\subsection{The situation}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---the-situation}

Firstly, we rearrange the equation as

\begin{equation*}
AW = V\Sigma
\end{equation*}

and focus on the \(i \leq r\) column. We get that
\(A\vec{w}_i = \sigma_i\vec{v}_i\)
And for \(i > r\), we get that
\(A\vec{w}_i = \vec{0} = 0\vec{v}_i\).

Therefore necessary and sufficient conditions for \(V\) and \(W\) are that

\begin{itemize}
\item Both \(V\) and \(W\) are unitary. Equivalently, the columns of \(V\)
    and \(W\) each form orthonormal basis.
\item For \(i \leq r\), \(\vec{v}_i = \frac{1}{\sigma_i} A\vec{w}_i\)
\item For \(i > r\), \(\vec{w}_i \in \ker(A)\).
\end{itemize}

\subsection{Compuation}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---compuation}

Now, firstly, from above \(A^*A\) has non-zero eigenvalues \(\{\sigma_i^2\}\).
Then since \(A^*A\) is hermitian, it is unitarily diagonalizable with

\begin{equation*}
A^*A = W \begin{pmatrix}
\small \begin{matrix}
\sigma_1^2 & & & \\
& \sigma_2^2 & & \\
& & \ddots & \\
& & & \sigma_r^2 \\
\end{matrix} & \Large 0 \\
\Large 0 & \Large 0
\end{pmatrix} W^*
\end{equation*}

Let \(i \leq r\). Then consider \(\vec{w}_i\). Then,

\begin{equation*}
A^*A\vec{w}_i = \sigma_i^2\vec{w}_i^2
\end{equation*}

Then firstly, for \(i \leq r\), \(A\vec{w}_i\) are independent since

\begin{equation*}
\sum_{i=1}^r c_iA\vec{w}_i = \vec{0} \implies
\vec{0} = A^*\vec{0} = \sum_{i=1}^r c_iA^*A\vec{w}_i = \sum_{i=1}^r c_i\sigma_i^2\vec{w}_i
\implies c_i = 0
\end{equation*}

since \(\sigma_i \neq 0\) and the \(\vec{w}_i\) are independent. We now define
\(\vec{v}_i = \frac{1}{\sigma_i}A\vec{w}_i\). Then from before,
the \(\vec{v}_i\) are independent and

\begin{equation*}
\vec{v}_i^*\vec{v}_j = \frac{1}{\sigma^2}\vec{w}_i^*A^*A\vec{w}_j = \vec{w}_i^*\vec{w}_j = \delta_{ij}
\end{equation*}

since the columns of \(W\) (which is unitary) form an orthonormal basis. Then the
\(\vec{v}_i\) are orthonormal and independent. Let \(V'\) be the matrix with
the columns of \(\vec{v}_i\) and we extend \(V'\) to \(V\) by finding an
orthonormal basis for \(\mathbb{C}^n\) containing the \(\vec{v}_i\)

Next, notice that for \(i > r\), \(A^*A\vec{w}_i= \vec{0}\). Then, each
of these \(\vec{w}_i \in \ker(A)\) since

\begin{equation*}
\|A\vec{w}_i\|^2 = \vec{w}_i^* A^*A\vec{w}_i = \vec{w}_i^*\vec{0} = 0
\end{equation*}

We have therefore found the \(W\) and \(V\) required.

\subsection{Remarks}
\label{develop--math3273:eigenvectors:page--singular-value-decomposition.adoc---remarks}

Overall, to find the singular value decomposition for \(A\) we conduct the following steps

\begin{itemize}
\item Find unitary \(W\) that diagonalizes \(A^*A\) which orders the eigenvalues in non-increasing
    order. To do this, we \emph{only} need to find the eigenvectors of \(A^*A\) and form
    orthonormal basis if necessary.
\item Let \(\vec{v}_i = \frac{1}{\sigma_i}A\vec{w}_i\) for \(i \leq r\).
\item Extend the \(\{\vec{v}_i\}\) to form an orthonormal basis for \(\mathbb{C}^n\)
\end{itemize}

Furthermore, nothing we have done here utilized features particular to the complex field.
That is, we can conduct singular value decomposition where \(V\) and \(W\)
are both orthogonal matrices
\end{document}
