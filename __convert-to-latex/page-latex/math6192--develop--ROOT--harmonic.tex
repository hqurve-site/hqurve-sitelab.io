\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Harmonic Oscillators}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Recall that the total energy in a 1-dimensional system is given by

\begin{equation*}
E = \frac{m}{2}\left(\frac{dx}{dt}\right)^2 + U(x)
\end{equation*}

where \(m\) is the reduced mass and \(U(x)\) is the potential energy at \(x\).
Suppose that \(U(x)\) has a (local) minimum at \(x=x_0\). So,
its Taylor series expansion is given by

\begin{equation*}
U(x) = U(x_0) + 0(x-x_0) + \frac{1}{2}U''(x_0) (x-x_0)^2 + \text{higher order terms}
\end{equation*}

since \(U'(x_0) = 0\).
By letting \(k=U''(x_0)\), we get an approximation for the energy in the system

\begin{equation*}
E \approx \frac{m}{2}\left(\frac{dx}{dt}\right)^2 + U(x_0) + \frac{k}{2}(x-x_0)^2
\end{equation*}

By letting \(q=x-x_0\) and differentiating with respect to \(t\), we get

\begin{equation*}
m\dot{q}\ddot{q} + kq\dot{q} = 0
\implies m\ddot{q} + kq = 0
\end{equation*}

Note that the case where \(\dot{q}=0\) is not interesting since \(x(t)\) is constant.
By letting \(\omega_0^2 = \frac{k}{m}\), we get a second order differential equation
with solution

\begin{equation*}
q(t) = a\cos(\omega_0t + \alpha)
\end{equation*}

where \(a\) and \(\alpha\) are constants to be determined.

\section{Driven system}
\label{develop--math6192:ROOT:page--harmonic.adoc---driven-system}

Now, suppose that we have a driving force \(F(t) = \frac{f}{m}\cos(\omega t)\) to
obtain the system

\begin{equation*}
\ddot{q} + \omega_0^2q = F(t) = \frac{f}{m}\cos(\omega t)
\end{equation*}

We can solve this to obtain that
a solution is given by

\begin{equation*}
q(t) = \begin{cases}
a\cos(\omega_0 t + \alpha) + \frac{f/m}{\omega_0^2-\omega^2}\cos(\omega t), &\quad\text{if } \omega\neq \omega_0
\\
a\cos(\omega_0 t + \alpha) + \frac{f}{2m\omega_0}t\sin(\omega t), &\quad\text{if } \omega= \omega_0
\end{cases}
\end{equation*}

We call the second case \emph{resonance}.
Note that if we have resonance, the amplitude
is unbounded and can cause damage of machinery.

Also, instead of splitting into cases, we let \(\omega = \omega_0 + \varepsilon\)
and consider the behaviour as \(\varepsilon \to 0\).
In order to do this, it is easier to consider the complex version of the differential
equation and obtain the following particular solution

\begin{equation*}
q_p(t) = \frac{f\exp(i\varepsilon t)}{-2m\omega_0\varepsilon}\exp(i\omega_0t)
\end{equation*}

Note that as \(\varepsilon \to 0\), we get the same particular solution. (Hint: use L'hopitals rule)

\begin{example}[{Frequency of two-atom compounds}]
Consider two forms of carbon monoxide \(C^{12}O^{16}\) and \(C^{14}O^{18}\)
where the superscript indicates the atomic mass of the carbon/oxygen atoms.
We wish to find the ratio of oscillation frequencies of the two molecules.

To do this, we first find governing equation for each of the two molecules
using the previously described method. The governing equation will be

\begin{equation*}
m\ddot{q} + kq = 0
\end{equation*}

Note

\begin{itemize}
\item for \(C^{12}O^{16}\), the reduced mass is \(m_1 = \frac{(12)(16)}{12+16}\).
    So, the frequency is given by \(\omega_1 = \sqrt{\frac{k}{m_1}}\)
\item for \(C^{14}O^{18}\), the reduced mass is \(m_2 = \frac{(14)(18)}{14+18}\).
    So, the frequency is given by \(\omega_2 = \sqrt{\frac{k}{m_2}}\)
\end{itemize}

So, the ratio of oscillation frequencies can be approximated to be

\begin{equation*}
\frac{\omega_1}{\omega_2} = \sqrt{\frac{k/m_1}{k/m_2}} = \sqrt{\frac{m_2}{m_1}}
\approx 1.071
\end{equation*}
\end{example}

\section{Friction}
\label{develop--math6192:ROOT:page--harmonic.adoc---friction}

\begin{admonition-important}[{}]
We are switching from \(q\) to \(x\) here.
\end{admonition-important}

We can model the force due to friction by \(-\alpha \dot{x}\) (where \(\alpha > 0\)).
So, upon applying this force, we obtain the system

\begin{equation*}
\ddot{x} + \frac{\alpha}{m}\dot{x} + \frac{k}{m}x = 0
\end{equation*}

and my letting \(2\gamma = \frac{\alpha}{m}\) and \(\omega_0^2 = \frac{k}{m}\),
we obtain characteristic roots

\begin{equation*}
\lambda_{1,2} = =\gamma \pm \sqrt{\gamma^2 - \omega_0^2}
\end{equation*}

If we let \(\omega = \sqrt{|\gamma^2 - \omega_0^2|}\),
there are three cases

\begin{description}
\item[Underdampened] 
    
    If \(\gamma^2 < \omega_0^2\), we have that
    \(x(t) = \exp(-\gamma t)\exp(i\omega t)\).
    Since \(\gamma > 0\), we see that we have oscillation with decreasing frequency.
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.4\linewidth]{images/develop-math6192/ROOT/harmonic-underdamped}
    \end{figure}
\item[Energy dissipated over time] 
    
    If \(\gamma^2 = \omega_0^2\), we have that
    \(x(t) = (A+Bt)\exp(-\gamma t)\).
    Since \(\gamma > 0\), we have an initial rise
    and then the position exponentially tends to \(0\).
    There is no oscillation
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.4\linewidth]{images/develop-math6192/ROOT/harmonic-dissipated}
    \end{figure}
\item[Overdamped] 
    
    If \(\gamma^2 > \omega_0^2\), note that both characteristic roots
    are negative.
    So, we have a graph similar to \(\exp(-\lambda t)\).
    There is no oscillation
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.4\linewidth]{images/develop-math6192/ROOT/harmonic-overdamped}
    \end{figure}
\end{description}

\section{Combined friction and external force}
\label{develop--math6192:ROOT:page--harmonic.adoc---combined-friction-and-external-force}

Consider a system with both friction and an external periodic force.
Then, the equation of the oscillator is

\begin{equation*}
\ddot{x} + 2\gamma\dot{x} + \omega_0^2 x = \frac{f}{m}\cos(\omega t)
\end{equation*}

Since the homogeneous version of the system is the same as with just frication,
we know that the complementary solution tends to zero.
So, we only care about the particular solution.
By using complex numbers we know that it has the form
\(A\exp(i\omega t)\) and by using undetermined coefficients, we get

\begin{equation*}
A = \frac{f/m}{\omega_0^2 -\omega^2 + 2\gamma\omega i}
\end{equation*}

So, the square of the real amplitude \(a\) is given by

\begin{equation*}
A\overbar{A} = \frac{f^2/m^2}{(\omega_0^2 - \omega^2)^2 + 4\gamma^2\omega^2}
\implies a = \frac{f/m}{\sqrt{(\omega_0^2 - \omega^2)^2 + 4\gamma^2\omega^2}}
\end{equation*}

If we consider \(a\) as a function of \(\omega\), we see that
it has a maximum of

\begin{equation*}
\frac{f}{m\sqrt{(2\gamma\omega_0)^2 -4\gamma^4}}\approx\frac{f}{2m\gamma\omega_0}
\end{equation*}

Note that the approximated form occurs when \(\omega = \omega_0\).
So, therefore, we have resonance when the frequency of the driving force is approximately equal
to the frequency of the system.

\begin{figure}[H]\centering
\includegraphics[width=0.4\linewidth]{images/develop-math6192/ROOT/harmonic-amplitude}
\caption{Graph of amplitude vs frequency of driving force}
\end{figure}

\section{Parametric Resonance}
\label{develop--math6192:ROOT:page--harmonic.adoc---parametric-resonance}

Consider the harmonic equation

\begin{equation*}
\ddot{x} + \omega_0^2 x = 0
\end{equation*}

where \(\omega_0 = \omega_0(t)\) is a function of time.
If \(\omega_0\) is periodic, the solutions are also periodic
and may have increasing amplitude. This phenomena
is called \emph{parametric resonance}.

Consider the case where \(\omega_0(t)^2 = \omega_0^2(1+\varepsilon \cos\omega t)\)
where \(\varepsilon\) is s small constant
so that the system is

\begin{equation*}
\ddot{x} + \omega_0^2 (1+\varepsilon \omega t) x = 0
\end{equation*}

Suppose we also have initial conditions \(x(0) = a\) and \(\dot{x}(0) = 0\).
So, by regular perturbation expansion (\href{https://en.wikipedia.org/wiki/Perturbation\_theory}{wiki article}),
we have that

\begin{equation*}
x(t) = x_0 + \varepsilon x_1 + \varepsilon^2 x_2 \cdots
\end{equation*}

where \(x_0, x_1, \ldots x_n\) are all functions of \(t\).
So, we have that

\begin{equation*}
\begin{aligned}
0
&=\sum_{i=0}^\infty \varepsilon^i\ddot{x}_i + \omega_0^2 (1+\varepsilon \cos \omega t) \sum_{i=0}^\infty \varepsilon^i x_i
\\&=\sum_{i=0}^\infty \varepsilon^i(\ddot{x}_i + \omega_0^2 x_i) + \sum_{i=0}^\infty \varepsilon^{i+1}\omega_0^2(\cos \omega t)x_i
\\&= \ddot{x}_i + \omega_0^2 x_i + \sum_{i=1}^\infty \varepsilon^i \left[\ddot{x}_i + \omega_0^2 x_i + \omega_0^2(\cos \omega t)x_{i-1}\right]
\end{aligned}
\end{equation*}

By comparing coefficients, we obtain a series of systems to solve.
The zero'th order problem is

\begin{equation*}
\ddot{x}_0 + \omega_0^2 x_0 = 0
\end{equation*}

which has solution \(a\cos(\omega_0 t)\) after applying the initial conditions.
Then, the order \(\varepsilon\) problem is

\begin{equation*}
\ddot{x}_1 + \omega_0^2 x_1
= -\omega_0^2 (\cos\omega t)(a\cos \omega_0 t)
= -\frac{\omega_0^2a}{2}\left[\cos(\omega + \omega_0)t + \cos (\omega-\omega_0) t \right]
\end{equation*}

So, we have resonance either if either of the frequencies of the non-homogeneous terms is \(\omega_0\).
The only interesting case is when \(\omega = 2\omega_0\).
\end{document}
