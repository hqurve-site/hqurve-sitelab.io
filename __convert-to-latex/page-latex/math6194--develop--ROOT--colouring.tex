\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Colouring}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Let \(G\) be a graph.

\begin{description}
\item[Independent set] An \textbf{independent set} of vertices in a graph is a set of vertices such that no two are adjacent.
    Note that an independent set need not be maximal (ie no more vertices can be added) and the empty set is an independent set.
\item[Bipartite] A \textbf{bipartite graph} is a graph whose vertex set can be partitioned into two disjoint independent sets.
    A necessary and sufficient condition for a graph to be bipartite is that is must not contain odd cycles.
    
    \begin{description}
    \item[Complete bipartitle] A \textbf{complete bipartite graph}, \(K_{m,n}\), is a graph whose vertex set can be partitioned into
        \(V_1\) and \(V_2\) where \(|V_1| = m\), \(|V_2| = n\) and \(xy\) is an edge iff \(x \in V_1\) and \(y \in V_2\).
    \end{description}
\item[Path] A (simple) \textbf{path} is a sequence of vertices where each successive pair is adjacent and there are no repeated vertices.
\item[Cut-vertex] A \textbf{cut-vertex} of a graph \(G\) is a graph whose removal increases the components of \(G\)
\item[Valency] The \textbf{valency} of a node is the number of nodes which it is adjacent to.
    We define \(\delta(G)\) and \(\Delta(G)\) to be the minimum and maximum valency respectively.
    
    \begin{description}
    \item[Regular] A graph is \textbf{regular} if \(\Delta(G) = \delta(G)\)
    \item[Circuit] A 2-regular graph is called a \textbf{circuit} graph. It consists of a single cycle
    \end{description}
\end{description}

Then, a \textbf{\(k\)-colouring} is a map \(c: V(G) \to S\) where \(S\) is a set of colours.
A colouring is called \textbf{proper} if no two adjacent vertices have the same colour.
Alternatively, we can define a proper colouring using a partition \(\{V_s: s\in S\}\) on \(V(G)\)
where \(V_s = \{v \in V(G): c(v) = s\}\) and no two elements of \(V_s\) are adjacent.
For this section, we will only consider proper colourings.

\begin{admonition-note}[{}]
For our purposes, we will only care about simple graphs (graphs without self-edges or multiple edges)
since graphs with self-edges have no colourings and multiple edges have no effect.
\end{admonition-note}

A graph is said to be \textbf{k-colourable} if and only if a k-colouring exists. The \textbf{chromatic number}, \(\bigchi(G)\),
is defined to be the smallest \(k\) such that \(G\) is \(k\)-colourable. In such a case,
we say that the graph is \textbf{k-chromatic}.

Note:

\begin{itemize}
\item if \(G\) has odd cycles, it is not bipartite and hence \(\bigchi(G) \geq 3\).
\item if \(G = K_n\), \(\bigchi(G) = n\).
\item if \(H \subseteq G\), \(\bigchi(H) \leq \bigchi(G)\).
\end{itemize}

\section{Applications}
\label{develop--math6194:ROOT:page--colouring.adoc---applications}

\subsection{Timetabling}
\label{develop--math6194:ROOT:page--colouring.adoc---timetabling}

When developing a timetable for a school/learning institution, one must ensure that at most one of a students courses are scheduled for a given time.
Therefore, nodes may represent courses and there is an edge between two nodes if there is a student/teacher who is registered for the two courses.
A valid colouring therefore is an allocation of each class to a given time slot.

\subsection{Planting}
\label{develop--math6194:ROOT:page--colouring.adoc---planting}

Nodes: plants

edge: if two plants are incompatible

colours: greenhouses/fields

\section{Greedy colouring heuristic}
\label{develop--math6194:ROOT:page--colouring.adoc---greedy-colouring-heuristic}

In general, developing a colouring which uses the minimal number of colours is difficult.
However, we can develop and algorithm which provides an upper bound for the chromatic number.
This algorithm is called greedy since it always makes the most optimal local decision without considering the problem globally.
Regardless, this algorithm is relatively quick and easy to implement.

Consider a graph \(G\) with some order on the vertices.
We initially have all vertices uncoloured and then colour the vertices in the specified order.
Let the colours be \(1,2,3,4,\ldots\).
At each stage, choose the minimal number which is not present in the coloured neighbours of the current vertex, and assign it this minimal number.
In the end, we develop a valid colouring since no two neighbours have the same assigned number.

Also, note that \(\bigchi(G) \leq \Delta(G) + 1\) since at each stage, we can consider up to \(\Delta(G)\) neighbours.
Either some or all of the numbers \(1,\ldots \Delta(G)\) are used in the neighbours.
In either case, the minimal assigned number is less than or equal to \(\Delta(G) + 1\).

However, there is a theorem called Brooks theorem (stated and proven below) which shows that in most cases,
\(\bigchi(G) \leq \Delta (G)\)

\subsection{Chartrand and Kronk}
\label{develop--math6194:ROOT:page--colouring.adoc---chartrand-and-kronk}

\begin{description}
\item[Hamiltonian path] A Hamiltonian path of a graph is a path which contains all the vertices of the graph.
\item[Depth first search tree] A \textbf{depth first search} (DFS) tree is a tree that can be formed using the following algorithm
    
    \begin{enumerate}[label=\arabic*)]
    \item Choose a start vertex and add it to the tree
    \item Look at the neighbours of the current vertex \(x\).
        
        \begin{itemize}
        \item If one of them is unvisited, choose one of the vertices \(y\) and add an edge \(xy\) and set the current vertex to \(y\).
        \item If all of them are visited (or there are no neighbours), go to the parent vertex of \(x\) (ie backtrack).
        \end{itemize}
    \item Repeat while there are unvisited vertices
    \end{enumerate}
    
    Note that different choices of the start vertex and different choices of \(y\) in step 2 may yield different depth first trees for the same graph.
    Therefore, the DFS tree is not unique.
\end{description}

\begin{lemma}[{}]
Let \(G\) be a graph in which evert depth first tree is a Hamiltonian path.
Then, \(G\) is either

\begin{itemize}
\item A circuit graph
\item A complete graph \(K_n\)
\item a regular bipartite graph \(K_{n,n}\)
\end{itemize}

\begin{admonition-todo}[{}]
proof
\end{admonition-todo}
\end{lemma}

\subsection{Brooks Theorem}
\label{develop--math6194:ROOT:page--colouring.adoc---brooks-theorem}

\begin{theorem}[{Brooks}]
If \(G\) is not an odd cycle or a complete graph,

\begin{equation*}
\bigchi(G) \leq \Delta (G)
\end{equation*}
\end{theorem}

This theorem is proven by taking cases

\begin{description}
\item[Case 1] Suppose the graph is not regular (\(\delta < \Delta\)). Consider a node which has valency \(\delta\)
    and a search tree rooted at this node.
    We colour the nodes according to reverse order of the tree (ie colour the children before the parents) using the greedy colouring heuristic.
    At each stage prior to the last, there is at least one uncoloured neighbour.
    So, there at are at most \(\Delta - 1\) adjacent colours and only the colours \(1,\ldots \Delta\) need to be used.
    At the last stage, we are at the root node and there are exactly \(\delta\) neighbours and hence at most \(\delta \leq \Delta - 1\) adjacent colours.
    So, the colour of the root node must also be one of \(1,\ldots \Delta\).
    Overall, we see that at most \(\Delta\) colours must be used.
\item[Case 2] Suppose the graph is regular and has a cut vertex.
    Let \(x\) be the cut vertex and \(G_1\) and \(G_2\) be the subgraphs such that
    \(G = G_1 \cup G_2\) and \(G_1 \cap G_2 = \{x\}\).
    Note that the valency of \(x\) in \(G_1\) must be strictly less than \(\Delta(G)\),
    otherwise \(x\) will not be connected in \(G_2\).
    So, \(G_1\) is not regular.
    Likewise, \(G_2\) is not regular.
    We can then apply the proof in case 1 to obtain that \(\bigchi(G_1) \leq \Delta\) and \(\bigchi(G_2) \leq \Delta\).
    By recolouring \(x\) if necessary, we can obtain a colouring for \(G\) by combining the colourings for \(G_1\) and \(G_2\).
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-math6194/ROOT/regular-cut}
    \caption{Regular graph with cut vertex}
    \end{figure}
\item[Case 3] Suppose the graph is regular and has no cut vertices and has a depth first search (DFS) tree which is not a path.
    Let \(T\) be the search tree and let \(x\) be a node which has at least two children in \(T\), \(y\) and \(z\).
    Note that such a node \(x\) exists since \(T\) is not a path.
    Since \(y\) and \(z\) are not cut vertices, their descendants must be connected to the rest of the graph.
    Also, Since \(T\) is a DFS tree, the \(y\), nor its descendants of can be connected to the \(z\) or its descendants.
    So, the descendants of \(y\) must be connected to \(x\) and/or its ancestors.
    Likewise for the descendants of \(z\). Therefore \(G' = G-\{y,z\}\) must be connected.
    Consider tree \(T'\) of \(G'\) which is rooted at \(x\).
    Colour \(y\) and \(z\) with the same colour, 1, and then colour the rest of the tree using the algorithm in case 1.
    At each stage except the last, we must use the colours \(1, \ldots \Delta(G)\) since at least one of the neighbours is uncoloured.
    When we reach the final node \(x\), there are exactly \(\Delta(G)\) neighbours, but two (\(x\) and \(y\)) are coloured the same.
    So, there are at most \(\Delta(G) - 1\) used colours and hence \(x\) must be one of \(1, \ldots \Delta(G)\).
    Overall, at most \(\Delta(G)\) colours must be used.
\item[Case 4] Suppose that \(G\) is regular, has no cut vertices and all DFS trees are paths.
    Then, from Chartrand and Kronk, the graph must either be a circuit or regular bipartite graph (by hypothesis of Brook's theorem, it cannot be a complete graph).
    Also, since there are no odd cycles, if the graph is a circuit, there must be an even number of nodes.
    In either case (circuit or regular bipartite), the graph is bipartite and \(\bigchi(G) = 2 \leq \Delta(G)\).
\end{description}
\end{document}
