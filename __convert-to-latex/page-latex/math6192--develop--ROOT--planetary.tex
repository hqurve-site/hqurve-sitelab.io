\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Planetary Motion - Derivation of Kepler's Laws}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\DeclareMathOperator{\erf}{erf}% error function
\def\tilde#1{\widetilde{#1}}
% \def\vec#1{\underline{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-remark}[{}]
Kepler's were stated by Kepler and shown to be consistent with Newton's laws
of motion by Newton. One nice thing about these laws is that it easily disproved
Copernicus' model (circular orbits) simply by using a theoretical formulation.
\end{admonition-remark}

In this section, we will derive Kepler's three laws of Planetary Motion.

\begin{enumerate}[label=\arabic*)]
\item The orbit of every planet is elliptical and the Sun is at one of the two foci of the ellipse
\item The line joining the planet and the Sun sweeps out equal areas during equal intervals of time.
\item \(a^3/T^2\) is constant for every planet where \(a\) is the semi-major axis of the orbit and \(T\)
    is the orbital period.
\end{enumerate}

\section{Assumptions}
\label{develop--math6192:ROOT:page--planetary.adoc---assumptions}

The assumptions used to prove Kepler's laws are.

\begin{itemize}
\item The Sun is the (only) physical cause of the acceleration of the planets.
\item The magnitude of the force on a planet is given by \(|\vec{F}| = \frac{GMm}{r^2}\)
    where
    
    \begin{itemize}
    \item \(\vec{r}\) is the vector from the Sun to the planet
    \item \(G\) is the \href{https://en.wikipedia.org/wiki/Gravitational\_constant}{gravitational constant}
    \item \(M\) is the mass of the Sun,
    \item \(m\) is the mass of the planet
    \end{itemize}
    
    It hence follows that \(\vec{F} = -\frac{GMm}{r^3}\vec{r}\)
\item Newton's second law of motion: \(\vec{F} = m\frac{d^2\vec{r}}{dt^2}\).
\end{itemize}

Note that the second and third assumptions are a result of Newton's
``Philosophiæ Naturalis Principia Mathematica''.

\begin{admonition-important}[{}]
Note that both the planet and the sun may be moving relative
to some other stationary object. But since \(\vec{r}\)
is the vector from the Sun to the planet, this motion is cancelled.
\end{admonition-important}

\section{Proving planar}
\label{develop--math6192:ROOT:page--planetary.adoc---proving-planar}

First, we will prove that the motion of the planet is planar.
But before that, note that we can obtain a formula for \(\frac{d^2\vec{r}}{dt^2}\)
using the force of gravity and Newton's second law.
That is,

\begin{equation*}
\frac{d^2\vec{r}}{dt^2} = -\frac{GM}{r^3}\vec{r} = -\frac{k}{r^3}\vec{r}
\end{equation*}

where \(k=GM\) is a constant.

Now, to prove that the motion is planar, it is sufficient to find a constant vector
which \(\vec{r}\) is always orthogonal to (and hence it lies in the orthogonal plane of the vector).
For this, we can use the cross-product to obtain the vector of interest.
\(\vec{r}\times\vec{r}\) is no good since it is always zero.
However, \(\vec{r}\times \vec{v}\) may be what we are after (where \(\vec{v}\) is the velocity; \(\frac{d\vec{r}}{dt}\)).

It will be sufficient to prove that the derivative of \(\vec{r}\times\vec{v}\) is zero.
Note that

\begin{equation*}
\begin{aligned}
\frac{d}{dt}(\vec{r}\times\vec{v})
&= \frac{d}{dt}\left(\vec{r}\times\frac{d\vec{r}}{dt}\right)
\\&= \frac{d\vec{r}}{dt}\times \frac{d\vec{r}}{dt} + \vec{r} \times \frac{d^2\vec{r}}{dt^2}
\\&= \vec{0} + \vec{r} \times \frac{d^2\vec{r}}{dt^2}
\\&= \vec{r} \times \left(-\frac{k}{r^3}\vec{r}\right)
\\&= \vec{0}
\end{aligned}
\end{equation*}

So, \(\vec{r}\times \vec{v}\) is constant.
This implies that \(\vec{r}\) and \(\vec{v}\) always lie in the plane orthogonal
to \((\vec{r}\times\vec{v})\) (which is some constant vector) and hence
we obtain that the planetary motion is planar.

\begin{admonition-important}[{}]
There is a degenerate case where \(\vec{r}\times \vec{v}=\vec{0}\).
However, in this case, it is clear that the motion of \(\vec{r}\) is planar
since it always lies in the same line (which is contained in at least one plane).
\end{admonition-important}

\section{Deriving laws 1 and 2}
\label{develop--math6192:ROOT:page--planetary.adoc---deriving-laws-1-and-2}

Since we have proven that the motion is planar, we can introduce some coordinates to simplify operations.
Let \(\vec{i}\) and \(\vec{j}\) be two orthogonal unit vectors in the plane which contains \(\vec{r}\).
Let \(\theta(t)\) be the angle between \(\vec{r}\) and \(\vec{i}\).
So, define

\begin{equation*}
\hat{e_r}(\theta) = \hat{i}\cos\theta + \hat{j}\sin\theta
,\quad\text{and}\quad
\hat{e_\theta}(\theta) = -\hat{i}\sin\theta + \hat{j}\cos\theta
\end{equation*}

So, \(\hat{e_r}\) is the vector in the direction of \(\vec{r}\) and
\(\hat{e_\theta}\) is orthogonal to \(\hat{e_r}\).
Using, these angle-dependent coordinates, we have that \(\vec{r} = r\hat{e_r}\).
Also, note that

\begin{equation*}
\frac{d}{d\theta}\hat{e_r} = \hat{e_\theta}
,\quad\text{and}\quad
\frac{d}{d\theta}\hat{e_\theta} = -\hat{e_r}
\end{equation*}

and can obtain that

\begin{equation*}
\begin{aligned}
\frac{d^2\vec{r}}{dt^2}
&= \frac{d}{dt}\left(\frac{d}{dt}(r\hat{e_r})\right)
\\&= \frac{d}{dt}\left(\frac{dr}{dt}\hat{e_r} + r \frac{d\hat{e_r}}{dt}\right)
\\&= \frac{d}{dt}\left(\frac{dr}{dt}\hat{e_r} + r \frac{d\hat{e_r}}{d\theta}\frac{d\theta}{dt}\right)
\quad\text{by chain rule}
\\&= \frac{d}{dt}\left(\frac{dr}{dt}\hat{e_r} + r \hat{e_\theta}\frac{d\theta}{dt}\right)
\\&= \left[\frac{d^2r}{dt^2}\hat{e_r} + \frac{dr}{dt}\frac{d\hat{e_r}}{dt}\right]
+ \left[
\frac{dr}{dt} \hat{e_\theta}\frac{d\theta}{dt}
+ r \frac{d\hat{e_\theta}}{dt}\frac{d\theta}{dt}
+ r \hat{e_\theta}\frac{d^2\theta}{dt^2}
\right]
\\&= \left[\frac{d^2r}{dt^2}\hat{e_r} + \frac{dr}{dt}\frac{d\hat{e_r}}{d\theta}\frac{d\theta}{dt}\right]
+ \left[
\frac{dr}{dt} \hat{e_\theta}\frac{d\theta}{dt}
+ r \frac{d\hat{e_\theta}}{d\theta}\frac{d\theta}{dt}\frac{d\theta}{dt}
+ r \hat{e_\theta}\frac{d^2\theta}{dt^2}
\right]
\\&= \left[\frac{d^2r}{dt^2}\hat{e_r} +\frac{dr}{dt}\hat{e_\theta}\frac{d\theta}{dt}\right]
+ \left[
\frac{dr}{dt} \hat{e_\theta}\frac{d\theta}{dt}
- r \hat{e_r} \left(\frac{d\theta}{dt}\right)^2
+ r \hat{e_\theta}\frac{d^2\theta}{dt^2}
\right]
\\&=
\left(\frac{d^2r}{dt^2} - r\left(\frac{d\theta}{dt}\right)^2\right)\hat{e_r}
+ \left(2\frac{dr}{dt}\frac{d\theta}{dt} + r \frac{d^2\theta}{dt^2}\right)\hat{e_\theta}
\end{aligned}
\end{equation*}

Therefore we have two expressions for \(\vec{d^2\vec{r}}{dt^2}\)

\begin{equation*}
\left(\frac{d^2r}{dt^2} - r\left(\frac{d\theta}{dt}\right)^2\right)\hat{e_r}
+ \left(2\frac{dr}{dt}\frac{d\theta}{dt} + r \frac{d^2\theta}{dt^2}\right)\hat{e_\theta}
= \frac{d^2\vec{r}}{dt^2}
= -\frac{k}{r^3}\vec{r} = -\frac{k}{r^2}\hat{e_r}
\end{equation*}

Since \(\hat{e_r}\) and \(\hat{e_\theta}\) form a basis, we must have that

\begin{equation*}
\frac{d^2r}{dt^2} - r\left(\frac{d\theta}{dt}\right)^2 = -\frac{k}{r^2}
,\quad\text{and}\quad
2\frac{dr}{dt}\frac{d\theta}{dt} + r \frac{d^2\theta}{dt^2} = 0
\end{equation*}

\subsection{Kepler's second law}
\label{develop--math6192:ROOT:page--planetary.adoc---keplers-second-law}

Let us focus on the second equation first.
By considering it a first order differential equation with dependent variable \(\frac{d\theta}{dt}\),
we get an integrating factor of \(r\) and hence

\begin{equation*}
\frac{d}{dt}\left(r^2\frac{d\theta}{dt}\right) = 0
\implies r^2 \frac{d\theta}{dt} =h
\end{equation*}

where \(h\) is some constant.
Note that this also implies Kepler's second law since the area swept out is directly
proportional to \(r^2\frac{d\theta}{dt}\).

\subsection{Kepler's first law}
\label{develop--math6192:ROOT:page--planetary.adoc---keplers-first-law}

To simplify the derivation of the first law, we will prove that \(\theta(t)\) is injective.
There are two cases for \(h\)

\begin{itemize}
\item If \(h=0\), we have that \(\frac{d\theta}{dt} = 0\) (we may assume that \(r\neq 0\)
    since magnitude of \(\vec{F}\) will be undefined).
    This implies that \(\theta\) is constant and there is no orbit.
    We ignore this case
\item If \(h\neq 0\), \(\frac{d\theta}{dt}\) has some fixed sign
    and hence \(\theta\) is injective.
\end{itemize}

Since \(\theta(t)\) is injective, it has an inverse and we can write \(r(\theta)\).
This will simplify calculations.

\begin{admonition-important}[{}]
This does not imply that \(r\) is periodic in \(\theta\).
\end{admonition-important}

Now, let us look at the first equation

\begin{equation*}
\frac{d^2r}{dt^2} - r\left(\frac{d\theta}{dt}\right)^2 = -\frac{k}{r^2}
\end{equation*}

We will use the fact that \(r(\theta)\) to simplify the equation as follows

\begin{equation*}
\begin{aligned}
&\frac{d^2r}{dt^2} - r\left(\frac{d\theta}{dt}\right)^2 = -\frac{k}{r^2}
\\&\implies
\frac{d}{d\theta}\left(\frac{dr}{d\theta}\frac{d\theta}{dt}\right)\frac{d\theta}{dt} - r\left(\frac{d\theta}{dt}\right)^2 = -\frac{k}{r^2}
\\&\implies
\frac{d}{d\theta}\left(\frac{dr}{d\theta}\frac{h}{r^2}\right)\frac{h}{r^2} - r\left(\frac{h}{r^2}\right)^2 = -\frac{k}{r^2}
\\&\implies
\frac{d}{d\theta}\left(-\frac{d}{d\theta}\frac{h}{r}\right)\frac{h}{r^2} - \frac{h^2}{r^3} = -\frac{k}{r^2}
\\&\implies
\frac{d^2}{d\theta^2}\left(\frac{1}{r}\right)\frac{-h^2}{r^2} - \frac{h^2}{r^3} = -\frac{k}{r^2}
\\&\implies
\frac{d^2}{d\theta^2}\left(\frac{1}{r}\right)+ \frac{1}{r} = \frac{k}{h^2}
\end{aligned}
\end{equation*}

This is a second order differential equation with constant coefficients with
dependent variable \(\frac{1}{r}\).
Therefore,

\begin{equation*}
\frac{1}{r} = A\cos\theta + B\sin\theta + \frac{k}{h^2} = C\cos(\theta-\alpha) + \frac{k}{h^2}
\end{equation*}

for some constants \(A,B,C,\alpha\)
and hence

\begin{equation*}
r
= \frac{1}{C\cos(\theta-\alpha) + \frac{k}{h^2}}
= \frac{\left(\frac{h}{k^2}\right)}{1 + \frac{C}{(k/h^2)}\cos(\theta-\alpha)}
= \frac{P}{1 + E\cos(\theta-\alpha)}
\end{equation*}

where \(P = \frac{h^2}{k}\) and \(E = \frac{C}{(k/h^2)}\).
From \url{https://en.wikipedia.org/wiki/Ellipse\#Polar\_form\_relative\_to\_focus},
we see that \(r\) is the distance between the focus of an ellipse and a point
at angle \(\theta\) which lies on the ellipse.
Therefore, the path of the planet is an ellipse with the Sun at one of the foci.

\section{Deriving third law}
\label{develop--math6192:ROOT:page--planetary.adoc---deriving-third-law}

The third law can be derived from the intermediary results of the first and second laws
and the properties of an ellipse.
The plan to prove that \(a^3/T^2\) is constant is by writing
\(T\) in terms of \(a\) only.

Computing the orbital period of the planet directly may prove difficult.
However, from the second law, we have that

\begin{equation*}
\frac{d}{dt}(\text{area swept}) = r^2\frac{d\theta}{dt} = h \implies A = \int_{0}^T h \ dt = hT
\implies T = \frac{A}{h}
\end{equation*}

where \(A\) is the area of the ellipse and \(T\) is the orbital period.
Since the area of an ellipse may also be written as \(A = \pi ab\) where
\(a\) and \(b\) are the semi-major and semi-minor axes (respectively).
So, we have that

\begin{equation*}
T = \frac{\pi ab}{h}
\end{equation*}

To obtain \(b\), we need a diagram

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6192/ROOT/ellipse}
\end{figure}

where \(F_2\) is the location of the Sun.
Recall the following property of an ellipse

\begin{custom-quotation}[{}][{}]
The sum \(|F_1P| + |F_2P|\) is constant for any choice of \(P\) on an ellipse.
\end{custom-quotation}

Therefore

\begin{equation*}
2x = |F_1P_1| + |F_2P_1| = |F_1P_2| + |F_2P_2| = (a+L) + (a-L) =2a
\end{equation*}

and hence \(a = x\) and \(b = \sqrt{a^2 - L^2}\).
Also, from the formula for \(r(\theta)\), we must have that

\begin{equation*}
a
= \frac{1}{2}\left(|F_2P_3| + |F_2P_2|\right)
= \frac{1}{2}\left(\max(r) + \min(r)\right)
= \frac{1}{2}\left(\frac{P}{1-E} + \frac{P}{1+E}\right)
= \frac{P}{1-E^2}
\end{equation*}

and

\begin{equation*}
L
= a-|F_2P_2|
= \frac{P}{1-E^2} - \frac{P}{1+E}
= \frac{P - P(1-E)}{1-E^2}
= \frac{PE}{1-E^2}
= Ea
\end{equation*}

So, we have that

\begin{equation*}
T
= \frac{\pi a b}{h}
= \frac{\pi a \sqrt{a^2-L^2}}{h}
= \frac{\pi a \sqrt{a^2-a^2E^2}}{h}
= \frac{\pi a^2 \sqrt{1-E^2}}{h}
= \frac{\pi a^2 \sqrt{\frac{P}{a}}}{h}
= \frac{\pi \sqrt{P}}{h} a^{3/2}
\end{equation*}

Finally,

\begin{equation*}
\frac{a^3}{T^2}
= \frac{h^2}{\pi^2 P}
= \frac{h^2}{\pi^2 \frac{h^2}{k}}
= \frac{k}{\pi^2}
= \frac{GM}{\pi^2}
\end{equation*}

Therefore, we have obtained the desired result.
\end{document}
