\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Confidence Intervals and Hypothesis tests summary}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% life is a lie
\def\bigchi{\chi}
\DeclareMathOperator{\SST}{SST}
\DeclareMathOperator{\SSE}{SSE}
\DeclareMathOperator{\SSTr}{SSTr}
\DeclareMathOperator{\SSB}{SSB}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\MSTr}{MSTr}
\DeclareMathOperator{\MSB}{MSB}

\def\bm#1{\mathbf #1}

% Title omitted
\section{Population Mean}
\label{develop--math2275:ROOT:page--confidence-intervals/summary.adoc---population-mean}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[l,h]|Q[c,m]|Q[c,m]|Q[l,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Name} & {Requirements} & {Distribution} & {Confidence Interval} & {Notes} \\
\hline[\thicktableline]
{\(Z\)} & {\begin{itemize}
\item \(\overbar{X} \sim N\left(\mu, \frac{\sigma^2}{n}\right)\)
\item The value of \(\sigma^2\) is known
\end{itemize}} & {\(\displaystyle Z = \frac{\overbar{X}-\mu}{\sigma/\sqrt{n}}\sim N(0,1)\)} & {\(\displaystyle \overbar{x} \pm z_{\alpha/2}\frac{\sigma}{n}\)} & {\begin{itemize}
\item \(P(Z > z_{\alpha/2}) = \frac{\alpha}{2}\)
\end{itemize}} \\
\hline
{\(T\)} & {\begin{itemize}
\item \(\overbar{X} \sim N\left(\mu, \frac{\sigma^2}{n}\right)\)
\item \(\frac{(n-1)S^2}{\sigma^2}\sim \bigchi^2_{n-1}\)
\item \(\overbar{X} \perp S^2\)
\end{itemize}} & {\(\displaystyle T = \frac{\overbar{X}-\mu}{S/\sqrt{n}}\sim T_{n-1}\)} & {\(\displaystyle \overbar{x} \pm t_{\alpha/2}\frac{s}{n}\)} & {\begin{itemize}
\item \(P(T > t_{\alpha/2}) = \frac{\alpha}{2}\)
\end{itemize}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\section{Difference between population means}
\label{develop--math2275:ROOT:page--confidence-intervals/summary.adoc---difference-between-population-means}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[l,m]|Q[c,m]|Q[c,m]|Q[l,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Name} & {Requirements} & {Distribution} & {Confidence Interval} & {Notes} \\
\hline[\thicktableline]
{Two-sample pooled \(Z\)} & {\begin{itemize}
\item \(X_1,\ldots X_n \sim N(\mu_x, \sigma_X^2)\)
\item \(Y_1,\ldots X_m \sim N(\mu_Y, \sigma_Y^2)\)
\item \(\forall i, j: X_i \perp X_j\)
\item The values of \(\sigma_X^2\) and \(\sigma_Y^2\) are known
\end{itemize}} & {\(\displaystyle Z = \frac{(\overbar{X} - \overbar{Y}) - (\mu_X - \mu_Y)}{\sqrt{\frac{\sigma_X^2}{n} + \frac{\sigma_Y^2}{m}}} \sim N(0, 1)\)} & {\(\displaystyle (\overbar{x}-\overbar{y}) \pm z_{\alpha/2}\sqrt{\frac{\sigma_X^2}{n} + \frac{\sigma_Y^2}{m}} \)} & {\begin{itemize}
\item \(P(Z > z_{\alpha/2}) = \tfrac{\alpha}{2}\)
\end{itemize}} \\
\hline
{Two-sample pooled \(T\)} & {\begin{itemize}
\item \(X_1,\ldots X_n \sim N(\mu_x, \sigma_X^2)\)
\item \(Y_1,\ldots X_m \sim N(\mu_Y, \sigma_Y^2)\)
\item \(\forall i, j: X_i \perp X_j\)
\item \(Var[X_i] = Var[Y_j] = \sigma^2\)
\end{itemize}} & {\(\displaystyle T = \frac{(\overbar{X} - \overbar{Y}) - (\mu_X - \mu_Y)}{S_p\sqrt{\frac{1}{n} + \frac{1}{m}}} \sim T_{m+n-2}\)} & {\(\displaystyle (\overbar{x} - \overbar{y}) \pm t_{\alpha/2} S_p\sqrt{\frac{1}{n} + \frac{1}{m}}\)} & {\begin{itemize}
\item \(\displaystyle P(T > t_{\alpha/2}) = \tfrac{\alpha}{2}\)
\item \(\displaystyle S_p = \sqrt{\frac{ (n-1)S_X^2 + (m-1)S_Y^2 }{ m+n-2 } }\)
\end{itemize}} \\
\hline
{Welch's \(T\)} & {\begin{itemize}
\item \(X_1,\ldots X_n \sim N(\mu_x, \sigma_X^2)\)
\item \(Y_1,\ldots X_m \sim N(\mu_Y, \sigma_Y^2)\)
\item \(\forall i, j: X_i \perp X_j\)
\end{itemize}} & {\(\displaystyle T = \frac{(\overbar{X} - \overbar{Y}) - (\mu_X - \mu_Y)}{\sqrt{\frac{S_X^2}{n} + \frac{S_Y^2}{m}}} \sim T_{r}\)} & {\(\displaystyle (\overbar{x} - \overbar{y}) \pm t_{\alpha/2}\sqrt{\frac{s_X^2}{n} + \frac{s_Y^2}{m}}\)} & {\begin{itemize}
\item \(P(T > t_{\alpha/2}) = \tfrac{\alpha}{2}\)
\item \(\displaystyle r = \frac{
        \left(\frac{s_X^2}{n} + \frac{s_Y^2}{m}\right)^2
    }{
        \frac{(s_X^2/n)^2}{n-1} + \frac{(s_Y^2/m)^2}{m-1}
    }\)
\item the integer (floor) of $r$ is used
\end{itemize}} \\
\hline
{Paired \(T\)} & {\begin{itemize}
\item \((X_1, Y_1),\ldots (X_n, Y_n)\) is a iid sample
\item \(D_i = X_i - Y_i \sim N(\mu_D, \sigma^2_D)\)
\end{itemize}} & {\(\displaystyle T = \frac{\overbar{D} - \mu_D}{\sqrt{S_D^2 / n}} \sim T_{n-1}\)} & {\(\displaystyle \overbar{d} \pm t_{\alpha/2}{\frac{s_D}{\sqrt{n}}}\)} & {\begin{itemize}
\item \(P(Z > z_{\alpha/2}) = \tfrac{\alpha}{2}\)
\end{itemize}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\section{Population Variance}
\label{develop--math2275:ROOT:page--confidence-intervals/summary.adoc---population-variance}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[l,m]|Q[c,m]|Q[c,m]|Q[l,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Name} & {Requirements} & {Distribution} & {Confidence Interval} & {Notes} \\
\hline[\thicktableline]
{single} & {\begin{itemize}
\item \(X_1, \ldots X_n \sim N(\mu, \sigma^2)\)
\end{itemize}} & {\(\displaystyle \bigchi^2 = \frac{(n-1)S^2}{\sigma^2} \sim \bigchi_{n-1}^2\)} & {\(\displaystyle \left(\frac{(n-1)S^2}{b}, \; \frac{(n-1)S^2}{a}\right)\)} & {\begin{itemize}
\item \(P(\bigchi^2 > a) = P(\bigchi^2 < b) = \tfrac{\alpha}{2} \)
\end{itemize}} \\
\hline
{ratio} & {\begin{itemize}
\item \(X_1, \ldots X_n \sim N(\mu_X, \sigma_X^2)\)
\item \(Y_1, \ldots Y_m \sim N(\mu_Y, \sigma_Y^2)\)
\end{itemize}} & {\(\displaystyle F = \frac{S_Y^2}{S_X^2}\frac{\sigma_X^2}{\sigma_Y^2} \sim F_{m-1, n-1}\)} & {\(\displaystyle \left(a \frac{S_X^2}{S_Y^2},\; b\frac{S_X^2}{S_Y^2}\right) \)} & {\begin{itemize}
\item \(P(F < a) = P(F > b) = \tfrac{\alpha}{2}\)
\end{itemize}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\section{Population Proportion}
\label{develop--math2275:ROOT:page--confidence-intervals/summary.adoc---population-proportion}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[l,m]|Q[c,m]|Q[c,m]|Q[l,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Name} & {Requirements} & {Distribution} & {Confidence Interval} & {Notes} \\
\hline[\thicktableline]
{single} & {\begin{itemize}
\item \(X_1, \ldots, X_n \sim Bernoulli(p)\)
\item \(n\) is 'large'
\end{itemize}} & {\(\displaystyle\hat{p} = \frac{\sum X_i}{n} \sim N\left(p, \frac{p(1-p)}{n}\right)\)\\\(\displaystyle Z = \frac{\hat{p} - p}{\sqrt{\frac{p(1-p)}{n}}} \sim N(0, 1)\)} & {\(\displaystyle \hat{p} \pm z_{\alpha/2}\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}\)} & {\begin{itemize}
\item \(P(Z > z_{\alpha/2}) = \tfrac{\alpha}{2}\)
\end{itemize}} \\
\hline
{display} & {\begin{itemize}
\item \(X_1, \ldots, X_n \sim Bernoulli(p_X)\)
\item \(Y_1, \ldots, Y_n \sim Bernoulli(p_Y)\)
\item \(X_i \perp X_j\)
\item \(n\) and \(m\) are 'large'
\end{itemize}} & {\(\displaystyle\hat{p}_X = \frac{\sum X_i}{n} \sim N\left(p_X, \frac{p_X(1-p_X)}{n}\right)\)\\\(\displaystyle \hat{p}_Y = \frac{\sum Y_i}{m} \sim N\left(p_Y, \frac{p_Y(1-p_Y)}{m}\right)\)\\\(\displaystyle Z = \frac{(\hat{p}_X - \hat{p}_Y) - (p_X - p_Y)}{\sqrt{\frac{p_X(1-p_X)}{n} + \frac{p_Y(1-p_Y)}{m}}} \sim N(0, 1) \)} & {\(\displaystyle (\hat{p}_X - \hat{p}_Y) \pm z_{\alpha/2}\sqrt{\frac{p_X(1-p_X)}{n} + \frac{p_Y(1-p_Y)}{m}}\)} & {\begin{itemize}
\item \(P(Z > z_{\alpha/2}) = \tfrac{\alpha}{2}\)
\item For confidence intervals, \(p_X = \hat{p}_X\) and \(p_Y = \hat{p}_Y\)
\end{itemize}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}
\end{document}
