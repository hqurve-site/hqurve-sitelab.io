\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Proving some nice results}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large\chi}}

% Title omitted
\section{Independence of sample mean and variance}
\label{develop--math3278:multivariate-normal:page--theorems.adoc---independence-of-sample-mean-and-variance}

Let \(X_1,\ldots X_n\) be iid \(N(\mu, \sigma^2)\). Then, we define

\begin{equation*}
\overbar{X} = \frac{1}{n} \sum_{i=1}^n X_i
\quad\text{and}\quad
S^2 = \frac{1}{n-1}\sum_{i=1}^n\left(X_i - \overbar{X}\right)^2
\end{equation*}

and \(\overbar{X} \perp S^2\)

\begin{example}[{Proof}]
Firstly, we define the following joint distribution of \(Y_1 \ldots Y_{n+1}\).
Where \(Y_i = X_i - \overbar{X}\) for \(i=1\ldots n\) and \(Y_{n+1} = \overbar{X}\).
Then, since each linear combination of the \(Y_i\) correspond to a linear combination of the \(X_i\),
each linear combination is normally distributed and hence the \(Y_i\) follow a multivariate normal.

Next, notice that

\begin{equation*}
\begin{aligned}
Cov(X_i - \overbar{X}, \overbar{X})
&= Cov(X_i, \overbar{X}) - Var(\overbar{X})
\\&= \frac{1}{n} Cov\left(X_i, \sum_{j=1}^n X_j\right) - \frac{\sigma^2}{n}
\\&= \frac{1}{n} Cov\left(X_i, X_i\right) - \frac{\sigma^2}{n}
\\&= \frac{1}{n} \sigma^2 - \frac{\sigma^2}{n}
\\&= 0
\end{aligned}
\end{equation*}

Since the \(X_i\) are independent. Therefore, Since the \(Y_i\) are multivariate normal,
\(Y_{n+1} \perp (Y_1,\ldots Y_n)\) and hence

\begin{equation*}
\overbar{X} \perp (X_1-\overbar{X}, \ldots X_n - \overbar{X})
\implies \overbar{X} \perp \sum_{i=1}^n (X_i - \overbar{X})^2
\implies \overbar{X} \perp S^2
\end{equation*}

\begin{admonition-caution}[{}]
It is important that \(Y_{n+1}\) is independent of \((Y_1,\ldots Y_n)\) and not just that \(Y_{n+1} \perp Y_i\)
for each \(i\leq n\). To see why consider the following
\href{https://stats.stackexchange.com/a/402654}{example} presented by
\href{https://stats.stackexchange.com/users/919/whuber}{whuber} on the statistics stack exchange. If we have a sample space

\begin{equation*}
\{(0,0,0), (1,1,0), (1,0,1), (0,1,1)\}
\end{equation*}

We get that \(X_1 \perp X_2\) and \(X_1 \perp X_3\).
However, clearly \(X_1 \not\perp (X_2,X_3)\) since
\(X_1 = 0 \implies X_2 = X_3\).
\end{admonition-caution}
\end{example}

\section{Distribution of sample variance}
\label{develop--math3278:multivariate-normal:page--theorems.adoc---distribution-of-sample-variance}

Let \(X_1,\ldots X_n\) be iid \(N(\mu, \sigma^2)\) and we define \(\overbar{X}\)
and \(S^2\) as \myautoref[{above}]{develop--math3278:multivariate-normal:page--theorems.adoc---independence-of-sample-mean-and-variance}. Then

\begin{equation*}
\frac{(n-1)S^2}{\sigma^2} \sim \bigchi^2_{(n-1)}
\end{equation*}

\begin{example}[{Proof}]
Firstly, we need to rewrite \(\frac{(n-1)S^2}{\sigma^2}\). Notice that

\begin{equation*}
\begin{aligned}
\sum_{i=1}^n (X_i - \overbar{X})^2
&= \sum_{i=1}^n (X_i - \mu + \mu - \overbar{X})^2
\\&= \sum_{i=1}^n \left[(X_i - \mu)^2 + 2(X_i - \mu)(\mu - \overbar{X}) + (\mu - \overbar{X})^2\right]
\\&= \sum_{i=1}^n (X_i - \mu)^2 + 2(\mu - \overbar{X})\sum_{i=1}^n(X_i - \mu) + n(\mu - \overbar{X})^2
\\&= \sum_{i=1}^n (X_i - \mu)^2 + 2(\mu - \overbar{X})(n\overbar{X} - n\mu) + n(\mu - \overbar{X})^2
\\&= \sum_{i=1}^n (X_i - \mu)^2 - 2n(\mu - \overbar{X})^2 + n(\mu - \overbar{X})^2
\\&= \sum_{i=1}^n (X_i - \mu)^2 - n(\mu - \overbar{X})^2
\end{aligned}
\end{equation*}

And hence

\begin{equation*}
\begin{aligned}
\frac{(n-1)S^2}{\sigma^2}
&= \frac{1}\sigma^2\sum_{i=1}^n (X_i - \overbar{X})^2
\\&= \frac{1}\sigma^2\sum_{i=1}^n (X_i - \mu)^2 - \frac{n}{\sigma^2}(\mu - \overbar{X})^2
\\&= \sum_{i=1}^n \left(\frac{X_i - \mu}{\sigma}\right)^2 - \left(\frac{\mu - \overbar{X}}{\sigma/\sqrt{n}}\right)^2
\end{aligned}
\end{equation*}

And by rearranging we get that

\begin{equation*}
\sum_{i=1}^n \left(\frac{X_i - \mu}{\sigma}\right)^2
=
\frac{(n-1)S^2}{\sigma^2}
+ \left(\frac{\mu - \overbar{X}}{\sigma/\sqrt{n}}\right)^2
\end{equation*}

and notice that the two terms on the right are independent since \(S^2 \perp \overbar{X}\). Therefore, if we know the
mgf of the right and leftmost terms, we can use the \myautoref[{result for the mgf of a convolution}]{develop--math3278:moment-generating-functions:page--index.adoc---convolution}
to determine the mgf of the middle term.

Firstly, \(\frac{X_i - \mu}{\sigma} \sim N(0,1)\) and hence \(\left(\frac{X_i - \mu}{\sigma}\right) \sim \bigchi^2_1\) since the
mgf of \(Z^2\) is given by

\begin{equation*}
\begin{aligned}
M_{Z^2}(t) = E[e^{tZ^2}]
&= \int_\mathbb{R} e^{tx^2}\frac{1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}} \ dx
\\&= \int_\mathbb{R} \frac{1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}(1-2t)} \ dx
\\&= \frac{1}{\sqrt{1-2t}}\int_\mathbb{R} \frac{1}{\sqrt{2\pi\left(\frac{1}{\sqrt{1-2t}}\right)^2}}e^{-\frac{x^2}{2\left(\frac{1}{\sqrt{1-2t}}\right)^2}} \ dx
\\&= \frac{1}{\sqrt{1-2t}}
\end{aligned}
\end{equation*}

which is the MGF of a \(\bigchi^2_1\) distribution [\myautoref[{see here}]{develop--math2274:ROOT:page--random/continuous/chi-squared.adoc---moment-generation-function}].
Therefore

\begin{equation*}
\sum_{i=1}^n \left(\frac{X_i - \mu}{\sigma}\right)^2] \sim\bigchi^2{n}
\end{equation*}

and has mgf \((1-2t)^{n/2}\).
Additionally, since \(\overbar{X} \sim N\left(\mu, \frac{\sigma}{\sqrt{n}}\right)\),

\begin{equation*}
\frac{\mu - \overbar{X}}{\sigma/\sqrt{n}} \sim N(0,1)
\end{equation*}

hence its square follows a \(\bigchi_1^2\) distribution and has mgf \((1-2t)^{1/2}\). Therefore, the mgf of \(\frac{(n-1)S^2}{\sigma^2}\)
is given by the ratio of the two, \((1-2t)^{(n-1)/2}\) and hence

\begin{equation*}
\frac{(n-1)S^2}{\sigma^2} \sim \bigchi_{n-1}^2
\end{equation*}
\end{example}
\end{document}
