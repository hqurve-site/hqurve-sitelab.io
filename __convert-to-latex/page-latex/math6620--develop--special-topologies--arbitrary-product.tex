\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Arbitrary products}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(\{X_\alpha\}_{\alpha \in I}\) be a collection of topological spaces.
Then, we define the \emph{product topology} on \(\prod X_\alpha\) by
the subbasis

\begin{equation*}
\mathcal{S} = \left\{\pi_{\alpha}^{-1}(U_\alpha) \ \middle| \ \alpha \in I \text{ and } U_\alpha \text{ is open in } X_\alpha\right\}
\end{equation*}

where \(\pi_\alpha(\vec{x}) = x_\alpha\).
We may equivalently define the product topology by the basis

\begin{equation*}
\mathcal{B} = \left\{\prod U_\alpha \ \middle| \
\begin{array}{l}
U_\alpha \text{ is open in } X_\alpha \text{ for each } \alpha\\
U_\alpha = X_\alpha \text{ for all but finitely many } \alpha
\end{array}
\right\}
\end{equation*}

We may also define the \emph{box topology} on \(\prod X_\alpha\) using the basis

\begin{equation*}
\mathcal{B} = \left\{\prod U_\alpha \ \middle| \
\begin{array}{l}
U_\alpha \text{ is open in } X_\alpha \text{ for each } \alpha\\
\end{array}
\right\}
\end{equation*}

Note that there is no restriction on the \(U_\alpha\) in the box topology. Therefore,
by comparing the bases, the box topology is finer than the product topology.
Furthermore, if infinitely many \(X_\alpha\) have a non-trivial open set,
the box topology is strictly finer than the product topology.

\begin{admonition-important}[{}]
The box and product topologies are the same if \(I\) is finite.
\end{admonition-important}

\begin{admonition-tip}[{}]
Like the pairwise product topology, we may use bases for the above definitions to yield the same topologies.
\end{admonition-tip}

So, the question remains; why designate the name ``product topology'' to the product topology defined in this section?
The box topology seems to be the more natural simply by looking at the basis.
However, there are few reasons why we prefer the product topology to the box topology

\begin{itemize}
\item The product topology is the coarsest topology such that \(\pi_\alpha\) is continuous.
\item In the product topology, the mapping \(A \to \prod X_\alpha\) is continuous iff all component functions are continuous.
    This is not true in the box topology.
\end{itemize}

\begin{admonition-important}[{}]
For the remainder of this section, \(\prod X_\alpha\) has the product topology unless stated otherwise.
\end{admonition-important}

\begin{proposition}[{Products and subspaces}]
Let \(\{X_\alpha\}\) be a collection of topologies and \(\{A_\alpha\}\) be a collection of subspace topologies on
the \(X_\alpha\). Then, the following topologies are the same

\begin{itemize}
\item The product topology of \(\prod A_\alpha\)
\item The subspace topology on \(\prod A_\alpha\) of the product topology of \(\prod X_\alpha\).
\end{itemize}

This claim is also true for the box topology and the proof is very similar.

\begin{proof}[{}]
To prove this claim, we compare their two bases.

First, consider basis element \(\prod U_\alpha'\) of the product topology on \(\prod A_\alpha\) where \(U_\alpha' = U_\alpha \cap A_\alpha\)
and \(U_\alpha' = A_\alpha\) for all but finitely many \(\alpha\).
Then

\begin{equation*}
\prod U_\alpha' = \prod (U_\alpha \cap A_\alpha)
\end{equation*}

Let \(V_\alpha = U_\alpha\) if \(U_\alpha' \neq A_\alpha\) and \(V_\alpha = X_\alpha\) if \(U\alpha' = A_\alpha\).
Then,

\begin{equation*}
\prod U_\alpha' = \prod (V_\alpha \cap A_\alpha) = \left(\prod V_\alpha\right) \cap \left(\prod A_\alpha\right)
\end{equation*}

Note that only finitely many of the \(V_\alpha\) are different from \(X_\alpha\) and hence \(\prod V_\alpha\)
is a basis element in \(\prod X_\alpha\).
Therefore, \(\prod U_\alpha'\) is contained in the subspace topology on \(\prod A_\alpha\)
and hence, the product topology on \(A_\alpha\) is coarser than the subspace topology on \(\prod A_\alpha\).

Conversely, consider a basis element \(\left(\prod V_\alpha\right) \cap \left(\prod A_\alpha\right)\) of the subspace topology on \(\prod A_\alpha\).
Then,

\begin{equation*}
\left(\prod V_\alpha\right) \cap \left(\prod A_\alpha\right) = \prod (V_\alpha \cap A_\alpha)
\end{equation*}

Since only finitely many of the \(V_\alpha\) are different from \(X_\alpha\), finitely many of the
\(V_\alpha \cap A_\alpha\) are different from \(A_\alpha\).
Therefore, \(\left(\prod V_\alpha\right) \cap \left(\prod A_\alpha\right)\) is contained in the product topology on \(\prod A_\alpha\)
and hence, the subspace topology on \(A_\alpha\) is coarser than the product topology on \(\prod A_\alpha\).

Therefore, the two topologies are the same.
\end{proof}
\end{proposition}

\begin{proposition}[{Continuous functions}]
Consider \(f: A \to \prod X_\alpha\) with \(f(a) = \left(f_\alpha(a)\right)_{\alpha \in I}\) for each \(a \in A\).
Then, \(f\) is continuous iff each of the \(f_\alpha\) is continuous.

\begin{admonition-caution}[{}]
This does not hold for the box topology.
\end{admonition-caution}

\begin{proof}[{}]
Suppose that \(f\) is continuous, then each of the \(\pi_\alpha\) are continuous and hence
\(f_\alpha = \pi_\alpha \circ f\) is also continuous.

Conversely, suppose that each of the \(f_\alpha\) are continuous and consider basis element
\(\prod U_\alpha\) of \(\prod X_\alpha\). Then,

\begin{equation*}
f^{-1}\left(\prod U_\alpha\right)
= \{a \in A \ | \  \forall \alpha \in I: f_\alpha(a) \in U_\alpha\}
= \prod f^{-1}_\alpha(U_\alpha)
\end{equation*}

Now, this is an arbitrary intersection which cannot be determined to be open in topologies.
However, notice that only finitely many of the \(f^{-1}_\alpha(U_\alpha)\) are possibly different from \(A\)
since \(U_\alpha  =X_\alpha\) for all but finitely many \(\alpha\).
Therefore, the above intersection only contains finitely many non-trivial sets and hence we can conclude that
\(f^{-1}\left(\prod U_\alpha\right)\) must be open.
Therefore \(f\) is continuous.
\end{proof}
\end{proposition}

\begin{proposition}[{Hausdorff}]
Let \(\{X_\alpha\}\) be a collection of Hausdorff spaces. Then, \(\prod X_\alpha\) is also Hausdorff

\begin{proof}[{}]
Consider arbitrary \(\vec{x}_\alpha, \vec{y}_\alpha \in \prod X_\alpha\) which are different.
That means that there exists \(\beta \in I\) such that \(x_\beta \neq y_\beta\).
Since \(X_\beta\) is Hausdorff, there exists open disjoint neighbourhoods \(U_\beta', V_\beta'\subseteq X_\beta \)
of \(x_\beta\) and \(y_\beta\).

We now define

\begin{equation*}
U_\alpha = \begin{cases}
U_\beta' \quad&\text{if } \alpha = \beta\\
X_\alpha \quad&\text{if } \alpha \neq \beta\\
\end{cases}
\quad\text{ and }
V_\alpha = \begin{cases}
V_\beta' \quad&\text{if } \alpha = \beta\\
X_\alpha \quad&\text{if } \alpha \neq \beta\\
\end{cases}
\end{equation*}

Then, \(\prod U_\alpha\) and \(\prod V_\alpha\)

\begin{itemize}
\item are open in \(\prod X_\alpha\)
\item are disjoint
\item contain \(\vec{x}\) and \(\vec{y}\) respectively
\end{itemize}

Therefore, we have shown that \(\prod X_\alpha\) is Hausdorff.
\end{proof}
\end{proposition}

\begin{proposition}[{Closure}]
Let \(\{X_\alpha\}\) be a collection of topological spaces and \(A_\alpha \subseteq X_\alpha\) for each \(\alpha \in I\).
Then, in \(\prod X_\alpha\)

\begin{equation*}
\closure\left(\prod A_\alpha\right) = \prod \closure(A_\alpha)
\end{equation*}

\begin{proof}[{}]
Consider arbitrary \(\vec{x} \in \closure\left(\prod A_\alpha\right)\).
We need to show that for each \(\beta \in I\), \(x_\beta \in \closure(A_\beta)\).

Consider arbitrary \(A_\beta \in I\) and open neighbourhood \(U_\beta\) of \(x_\beta\).
Define \(V_\alpha = U_\beta\) if \(\alpha = \beta\) and \(V_\alpha = A_\alpha\) otherwise.
Then, \(\vec{x} \in \prod V_\alpha\) and \(\prod V_\alpha \cap \prod A_\alpha \neq \varnothing\).
This implies that \(U_\beta \cap A_\beta = V_\beta \cap A_\beta \neq \varnothing\)
and hence \(\vec{x} \in \closure\left(\prod A_\alpha\right)\).
Therefore

\begin{equation*}
\closure\left(\prod A_\alpha\right) \subseteq \prod \closure(A_\alpha)
\end{equation*}

Conversely, suppose that \(\vec{x} \in \prod \closure(A_\alpha)\) and consider arbitrary
open \(\prod U_\alpha\).
Then, for each \(\alpha\), \(U_\alpha\) is a neighbourhood of \(x_\alpha\) and hence
\(U_\alpha \cap A_\alpha \neq \varnothing\). Therefore,
\(\prod U_\alpha \cap \prod A_\alpha \neq \varnothing\) and \(\vec{x} \in \closure\left(\prod A_\alpha\right)\).
Therefore, we have the desired result.
\end{proof}
\end{proposition}

\begin{lemma}[{Finite products are connected}]
Let \(X\) and \(Y\) be two topological spaces. Then,
\(X\times Y\) is connected iff \(X\) and \(Y\) are connected.

By induction, it follows that any finite product is connected
iff each of individual spaces are connected.

\begin{proof}[{}]
First, suppose that \(X\times Y\) is connected.
Then, \(X = \pi_X(X\times Y)\) and \(Y = \pi_Y(X\times Y)\) are connected
as they are images of continuous functions.

Conversely suppose that \(X\) and \(Y\) are connected.
Notice that for any fixed \((x_0, y_0) \in X\times Y\), the following
two subspaces of \(X\times Y\) are connected

\begin{equation*}
\{x_0\}\times Y \quad\text{and}\quad X \times \{y_0\}
\end{equation*}

since they are homomorphic to \(Y\) and \(X\) respectively.
Then \(\left(\{x_0\}\times Y\right) \cup \left(X \times \{y_0\}\right)\)
is also connected since they contain a common point \((x_0, y_0)\).

Finally, notice that

\begin{equation*}
X\times Y
= (\{x_0\} \times Y) \cup X\times Y
= \cup_{y_1 \in Y} \left(\{x_0\}\times Y\right) \cup \left(X \times \{y_1\}\right)
\end{equation*}

is connected since \(\{x_0\}\times Y\) is common to each set in the union.
Hence we have the desired result.
\end{proof}
\end{lemma}

\begin{proposition}[{Connected}]
Let \(\{X_\alpha\}\) be a collection of topological spaces.
Then, \(\prod X_\alpha\) is connected iff each of the \(X_\alpha\)
are connected.

\begin{admonition-caution}[{}]
This does not hold in the box topology.
For example, we may separate \(\mathbb{R}^\omega\) into the set of
bounded and unbounded sequences; both of which are open in the box topology.
\end{admonition-caution}

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof is from \url{https://planetmath.org/proofthatproductsofconnectedspacesareconnected}
by user \texttt{yark (2760)}
\href{https://web.archive.org/web/20231022192304/https://planetmath.org/proofthatproductsofconnectedspacesareconnected}{[wayback]}.
Although I typically do not like proofs by contradiction, the logic of this proof is much
simpler than the proof presented in class.
\end{admonition-note}

First suppose that \(\prod X_\alpha\) is connected. Then
\(X_\beta = \pi_\beta\left(\prod X_\alpha\right)\) is connected
for each \(\beta\).

Conversely, suppose that each of the \(X_\alpha\) are connected
and suppose that we have a separation \(U\), \(V\) of \(\prod X_\alpha\).
Since this is the product topology, \(U\) and \(V\) differ from the
\(X_\alpha\) are only finitely many indices.
Therefore there exists points \(\vec{u} \in U\)
and \(\vec{v} \in V\) at only finitely many indices.

We would show that there exists a \(\vec{u}' \in U\) which differs from \(\vec{v}\) at only one index.
Suppose that \(\vec{u}\) and \(\vec{v}\) differ at only \(n\) indices.
If \(n=1\), we are done. Otherwise, choose an index \(\beta\) at which the two
points differ and consider \(\vec{u}'\) which is the same as \(\vec{u}\) at all indices
except \(\beta\) at which \(\vec{u}'_\beta = \vec{v}_\beta\).
Since \(U\) and \(V\) form a separation, either \(\vec{u}'\) is in one of the two sets.
If \(\vec{u}' \in V\), then \(\vec{u}\) and \(\vec{u}'\) differ at exactly one index
and belong to \(U\) and \(V\) respectively.
However, if \(\vec{u}' \in U\), we repeat the above procedure with \(\vec{u}'\) and \(\vec{v}\)
which now only differ at \(n-1\) indices.

Now, suppose that \(\vec{u} \in U\) and \(\vec{v} \in V\) only differ at index \(\beta\).
Define the function \(f: X_\beta \to \prod X_\alpha\) such that \(f(x)_\beta = x\)
and \(f(x)_\alpha = \vec{u}_\alpha = \vec{v}_\alpha\) for \(\alpha \neq \beta\).
Notice that \(\pi_\beta|_{f(X_\beta)} \circ f\) is the identity (hence \(f^{-1} = \pi_\beta|_{f(X_\beta)}\))
and \(\pi_\beta|_{f(X_\beta)}\) is an open map since it maps the subbasis to open sets

\begin{equation*}
\forall \alpha: \forall \text{open } W \subseteq X_\alpha:
\pi_\beta|_{f(X_\beta)}(\pi_\alpha^{-1}(W) \cap f(X_\beta)) = \begin{cases}
X_\beta \quad&\text{if } \beta \neq \alpha\\
W \quad&\text{if } \beta =\alpha\\
\end{cases}
\end{equation*}

Therefore \(f = \pi_\beta|_{f(X_\beta)}^{-1}\) is continuous.
Now, since \(X_\beta\) is connected, \(f(X_\beta)\) is connected.
However, \(U\cap f(X_\beta)\) and \(V\cap f(X_\beta)\) form a separation on \(f(X_\beta)\).
This is a contradiction to the assumption that \(\prod X_\alpha\) is not connected.
\end{proof}
\end{proposition}

\begin{lemma}[{Tube lemma}]
Let \(X\) be a topological space and \(Y\) be compact.
Let \(x_0 \in X\) and \(N \subseteq X\times Y\) be open
and

\begin{equation*}
\{x_0\}\times Y = \{(x_0, y) \ | \ y \in Y\} \subseteq N
\end{equation*}

Then, there exists open \(W \subseteq X\) such that \(W\times Y \subseteq N\).

\begin{proof}[{}]
For each \(y \in Y\) determine open \(U_y\times V_y\) such that
\((x_0, y) \in U_y \times V_y \subseteq N\).
Then, the collection \(\{U_y \times V_y\}_{y\in Y}\) forms an open cover
of \(\{x_0\}\times Y\).
Since \(\{x_0\} \times Y\) is compact (by continuous map \(f(y) = (x_0, y)\)),
there exists a finite subcover

\begin{equation*}
\{(U_{y_i} \times V_{y_i})\}_{i=1}^n
\end{equation*}

Let \(W = \cap_{i=1}^n U_{y_i}\) and notice that it is open.
Then, \((x_0, y) \in W\times Y\).
Consider arbitrary \((x, y) \in W\times Y\),
then \((x_0, y) \in U_{y_i} \times V_{y_i}\) for some \(i\).
Notice that \(x \in W \subseteq U_{y_i}\)
and hence \((x, y) \in U_{y_i} \times V_{y_i} \subseteq N\)
for each \((x,y) \in W\times Y\).
Therefore we have the desired result.
\end{proof}
\end{lemma}

\begin{theorem}[{Finite products are compact}]
Let \(X\) and \(Y\) be compact.
Then \(X\times Y\) is also compact.

Note that this implies that finite products can are compact by induction.

\begin{proof}[{}]
Let \(\{U_\alpha\}\) be an open cover of \(X\times Y\).
The plan is to

\begin{enumerate}[label=\arabic*)]
\item Determine an open cover for each \(\{x\} \times Y\)
\item Determine a \(W_x\) such that \(W_x \times Y\) lies in the open cover for \(\{x\} \times Y\) and \(x \in W_x\)
\item Show that the \(W_x\) forms an open cover of \(X\)
    and find an open cover of \(X\times Y\) using the open covers of \(\{x\} \times Y\)
    for finitely many \(x\).
\end{enumerate}

First consider arbitrary \(x \in X\).
Then since \(\{x\} \times Y\) is homeomorphic to \(Y\), it is compact.
Since the \(U_\alpha\) forms a cover for \(\{x\}\times Y\),
there exists a finite subcover \(\mathcal{U}_x\).

Since \(\mathcal{U}_x\) forms an open cover of \(\{x\}\times Y\),
\(\{x\} \times Y \subseteq \bigcup_{U_\alpha \in \mathcal{U}_x} U_\alpha\).
Then, by the tube lemma, there exists an open \(W_x \subseteq X\) such that

\begin{equation*}
\{x\} \times Y \subseteq W_x \times Y \subseteq \bigcup_{U_\alpha \in \mathcal{U}_x} U_\alpha
\end{equation*}

Now, notice that the \(W_x\) form an open cover for \(X\) since each
\(x \in W_x\). Then, by the compactness of \(X\), there exists
\(x_1, \ldots x_n\) be such that \(X = \bigcup_{i=1}^n W_i\).
We claim that \(\mathcal{U} = \mathcal{U}_{x_1} \cup \cdots \mathcal{U}_{x_n}\)
forms an open cover of \(X\times Y\). Also, since each of the \(\mathcal{U}_{x_i}\)
are finite, \(\mathcal{U}\) is also finite.

Consider arbitrary \((x, y) \in X\times Y\). Then, \(x \in W_{x_i}\) for some
\(x_i\). Therefore \((x, y) \subseteq W_{x_i}\times Y\) and is hence in one
of the \(U_\alpha \in \mathcal{U}_{x_i} \subseteq \mathcal{U}\).
Therefore \(\mathcal{U}\) is a finite subcover as desired.
\end{proof}
\end{theorem}

\begin{theorem}[{Tychonoff's theorem}]
Let \(\{X_\alpha\}\) be a collection of compact topological spaces.
Then, \(X = \prod X_\alpha\) is also compact.

\begin{admonition-note}[{}]
Both Dr Tweedle and the book on Munkres states that this theorem is hard to prove.
So, for now, we do not prove it.
\end{admonition-note}
\end{theorem}

\section{Example with \(\mathbb{R}\)}
\label{develop--math6620:special-topologies:page--arbitrary-product.adoc--example-with-R}

Consider the set \(\mathbb{R}^\omega = \{x_i\ | \ x_i \in \mathbb{R}, \ i=0,1,2,\ldots\}\) which is the product
of \(\mathbb{R}\) with index set \(\mathbb{N}\). We wish to compare the product and box topologies
on this set.
Define

\begin{equation*}
% Not sure if it should be oo or infinity
\mathbb{R}^{oo} = \left\{\vec{x} \in \mathbb{R}^w \ \middle| \ \exists N \in \mathbb{B}: \forall i \geq N: x_i = 0 \right\}
\end{equation*}

This is a set in which all sequences eventually terminate (become zeros). Let us fine the closure of this
set in the product and box topologies on \(\mathbb{R}^\omega\).

\begin{itemize}
\item In the product topology, \(\closure(\mathbb{R}^{oo}) = \mathbb{R}^\omega\)
    
    \begin{proof}[{}]
    Consider arbitrary \(\vec{x} \in \mathbb{R}^{\omega}\) and each open set \(\prod U_i\) in \(\mathbb{R}^\omega\)
    such that \(x \in \prod U_i\). Consider the intersection
    
    \begin{equation*}
    \prod U_i \cap \mathbb{R}^{oo}
    =
    \left\{\vec{y} \in U \ \middle| \
    \begin{array}{l}\vec{y} \text{ eventually terminates}\\ \exists N \geq 0: \forall i \leq N: y_i \in U_i\end{array}\right\}
    \end{equation*}
    
    Therefore, the above set always contains \((x_0, \ldots x_n, 0,\ldots\) where \(n\) is the largest such that \(U_n \neq \mathbb{R}\)
    (or zero if all are \(\mathbb{R}\)).
    
    Therefore, have have that \(x \in \closure(\mathbb{R}^{oo})\) and we get the desired result.
    \end{proof}
\item In the box topology, \(\closure(\mathbb{R}^{oo}) = \mathbb{R}^{oo}\)
    
    \begin{proof}[{}]
    Consider arbitrary \(\vec{x} \in \mathbb{R}^{\omega}\) and each open set \(\prod U_i\) in \(\mathbb{R}^\omega\)
    such that \(x \in \prod U_i\). Consider the intersection
    
    \begin{equation*}
    \prod U_i \cap \mathbb{R}^{oo}
    =
    \left\{\vec{y} \in \mathbb{R}^\omega \ \middle| \ \vec{y} \text{ eventually terminates}\right\}
    \end{equation*}
    
    The above set contains is non-empty iff the \(U_i\) always contains zero after a finite number of points.
    However, if \(\vec{x} \notin \mathbb{R}^{oo}\) there is no such guarantee.
    Let us prove this.
    
    Let \(\vec{x} \notin \mathbb{R}^{oo}\) and define
    
    \begin{equation*}
    U_i = \begin{cases}
    (0, \infty) \quad&\text{if } x_i > 0\\
    (-\infty, 0) \quad&\text{if } x_i < 0\\
    (-1, 1) \quad&\text{if } x_i = 0\\
    \end{cases}
    \end{equation*}
    
    Since \(x_i\) does not eventually terminate, \(\vec{y} \in \prod U_i\) implies that
    \(\forall N \geq 0: \exists i \geq N: y_i \neq 0\). Therefore, \(\vec{x} \notin \closure(\mathbb{R}^{oo})\)
    and we get the desired result.
    \end{proof}
\end{itemize}
\end{document}
