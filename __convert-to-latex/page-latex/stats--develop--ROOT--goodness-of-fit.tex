\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Goodness of fit}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
\section{QQ-plot}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---qq-plot}

A QQ-plot plots the quartiles of the observed data vs the quartiles of the expected data.
Ideally, when plotted, the points should lie on the straight line \(y=x\).
But because of the random nature of the sample, some variation should be expected.

Note that this method only works for univariate data.
So, if the data is multivariate, it must be transformed to a univariate form.
For example, multivariate normal to statistical distance.

The following R code generates the qqplot

\begin{listing}[{}]
# The variable x contains the data
n = length(x)

# The quartile function is required
# ppoints generates the equispaced points between 0 and 1
qqplot(
    qnorm(ppoints(n), mu, sd),
    x
)
# plot the line y=x
abline(0,1)
\end{listing}

\section{Assessing univariate normality}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---assessing-univariate-normality}

\begin{admonition-tip}[{}]
Read \url{https://core.ac.uk/download/pdf/234680353.pdf}.
Oppong, Felix Boakye, and Senyo Yao Agbedra. "Assessing univariate and multivariate normality. a guide for non-statisticians." Mathematical theory and modeling 6, no. 2 (2016): 26-33.
\end{admonition-tip}

To assess univariate normality, we

\begin{description}
\item[Plot data] A histogram may be used. In the plot, identify
    
    \begin{itemize}
    \item Outliers
    \item Possible skewness issues
    \item Heavy tails
    \item Non-symmetric
    \end{itemize}
\item[Plot qqplot] 
\item[Formal test] A common test is the Shapiro-Wilk test
    which can be conducted in R using \texttt{shapiro.test}
\end{description}

\section{Assessing multivariate normality}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---assessing-multivariate-normality}

To assess multivariate normality of \(p\)-dimensional data, we

\begin{description}
\item[Plot data] Since we are dealing with multivariate data,
    it is hard to directly plot the data.
    Instead, we can look at the box plots of the marginals
    and the scatter plots between the pairs.
    
    The function \texttt{pairs} produces a matrix of scatter plots
    similar to figure 1.5 of Applied Multivariate Statistics (6th)
    but without the boxplots. Boxplots need to be plotted separately
    using \texttt{boxplot}.
    
    Pairwise scatter plots are useful for identifying possible clusters of data
    and assessing bivariate normality.
    Points should fit an ellipsoid.
\item[Plot qqplot] Under the assumption of normality,
    the Mahalanobis Distance \((\vec{x}-\vec{\mu})^T\mat{\Sigma}^{-1}(\vec{x}-\vec{\mu})\)
    is chi-squared with \(p\) degrees of freedom.
    So, we can plot as follows
    
    \begin{listing}[{}]
    # x contains a (n x p) matrix
    n <- nrow(x)
    p <- ncol(x)
    
    d <- mahalanobis(x, colMeans(x), cov(x))
    
    qqplot(
        qchisq(ppoints(n), df=p),
        d
    )
    abline(0,1)
    \end{listing}
\item[Formal test] There are several formal tests for multivariate normality
    
    \begin{description}
    \item[Correlation coefficient] The qqplot can be used to perform a test.
        We can consider the correlation coefficient between the observed
        and expected quartiles. Table 4.2 of Applied Multivariate Statistics (6th)
        contains the critical values for this correlation coefficient.
    \item[Royston] This test is an extension of the Shapiro-Wilk test.
        It is implemented in the \texttt{MVN} package.
    \item[Mardia] This test looks at the skewness and kurtosis.
        It is implemented in the \texttt{MVN} package.
    \item[Multivariate Shapiro-Wilk] It is implemented in the \texttt{mvnormtest} library via \texttt{mshapiro.test}
        Note that this takes the transpose of the data (ie rows are variables)
    \end{description}
\end{description}

\begin{admonition-tip}[{}]
Several tests are implemented
in the \texttt{MVN} package. See
\url{https://cran.r-project.org/web/packages/MVN/vignettes/MVN.html}
\end{admonition-tip}

\section{Assessing Model requirements}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---assessing-model-requirements}

Sometimes, we need to ensure that our data indeed matches
the expected behaviour of the theoretical model we are using.

\subsection{Using residuals}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---using-residuals}

Often after fitting our data to our model, we may look at the residuals of the data
to check how well the model works.

\begin{listing}[{}]
# result contains fitted values and residuals. (eg lm function produces this)
plot(result$fitted.values, result$residuals)
abline(h=0)
\end{listing}

\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/residuals-vs-fitted}
\end{figure}

\begin{admonition-note}[{}]
The following images were obtained from the math2275 lecture notes in Semester 2 of 2020-2021
and taught by Mr. Brendon Bhagwandeen.
\end{admonition-note}

\begin{description}
\item[Linearity] In the plot, the residuals are evenly distributed about the x-axis.
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/linear}
    \end{figure}
\item[Independence] In the plot, the residuals are evenly distributed about the x-axis.
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/independent}
    \end{figure}
\item[Homoscedasicity] In the plot, the spread of the residuals does not vary with the fitted value
    
    \begin{figure}[H]\centering
    \includegraphics[width=0.6\linewidth]{images/develop-stats/ROOT/linear-regression/constant-variance}
    \end{figure}
\end{description}

\subsection{Constant variance (homoscedasicity)}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---constant-variance-homoscedasicity}

To test the constant variance assumption, Bartlett's test may be used.
We can use the R function \texttt{bartlett.test}

\subsubsection{Box's M-test for equal covariance matrices}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---boxs-m-test-for-equal-covariance-matrices}

To test whether the covariance matrices of different populations are equal,
Box's M-test can be performed.
Let there be \(g\) groups, each having \(n_i\) samples.
Let \(n = \sum n_i\) and \(p\) be the number of variables.

Let

\begin{equation*}
\begin{aligned}
\mat{S}_{pooled} &= \frac{1}{n-g}\left[
    (n_1-1)\mat{S}_1
    + (n_2-1)\mat{S}_2
    +\cdots
    + (n_g-1)\mat{S}_g
\right]
\\
M &=
(n-g)\ln \left|\mat{S}_{pooled}\right|
- \sum_{i} (n_i-1)\ln \left|\mat{S}_i\right|
\\
u &= \left[
    \sum_{i} \frac{1}{n_i-1}
    - \frac{1}{n-g}
\right]
\left[
\frac{2p^2+3p-1}{6(p+1)(g-1)}
\right]
\end{aligned}
\end{equation*}

Then,

\begin{equation*}
C = (1-u)M \approx \chi^2_{\nu}
\end{equation*}

where

\begin{equation*}
\nu = \frac{1}{2} p (p+1)(g-1)
\end{equation*}

\begin{admonition-important}[{}]
This approximation only works
well if each sample size \(n_i > 20\)
 and \(p,g \leq 5\).
\end{admonition-important}

In \texttt{R}, the \texttt{boxM} method from the \texttt{heplots} package
can be used.

\subsubsection{Levene's test for equal variance}
\label{develop--stats:ROOT:page--goodness-of-fit.adoc---levenes-test-for-equal-variance}
\end{document}
