\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Examples}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
\def\normalsubgroup{\trianglelefteq}
\DeclareMathOperator{\ord}{ord}

% Title omitted
We would give several interesting examples of group actions.
Not all of them would be fully fleshed out nor would they be fully generalized
as most are obvious.

\section{Groups}
\label{develop--math3272:group-actions:page--examples.adoc---groups}

\myautoref[{}]{develop--math3272:group-actions:page--group-examples.adoc}.

\section{Symmetric group}
\label{develop--math3272:group-actions:page--examples.adoc---symmetric-group}

Let \(X\) be a set and \(S(X)\) be the symmetric group on
\(X\). Then \(S(X)\) acts on \(X\)

\subsection{Symmetric group on subsets}
\label{develop--math3272:group-actions:page--examples.adoc---symmetric-group-on-subsets}

Let \(Z = \{ Y \subseteq X : |Y| =k \leq |X| = n\}\), then \(G = S(X)\) acts on \(Z\)
by

\begin{equation*}
\sigma \cdot Y = \{\sigma(y): y\in Y\}
\end{equation*}

Then, firstly,

\begin{equation*}
[Y] = \{\sigma Y: \sigma \in S(X)\} = Z
\end{equation*}

since for each \(Y'\), we can construct \(\sigma\) which maps
each of values in \(Y\) to \(Y'\). Next, the stabilizer is

\begin{equation*}
G_Y = \{\sigma \in S(X): \sigma Y = Y\}
\end{equation*}

Then \(\sigma\) must permute the \(k\) values of \(Y\) to themselves.
By the orbit stabilizer theorem, \(|G_Y| = \frac{|S(X)|}{|[Y]|} = \frac{n!}{|Z|} = \frac{n!}{\binom{ n }{ k}}\).

\subsection{Cool result}
\label{develop--math3272:group-actions:page--examples.adoc---cool-result}

Consider we have \(\tau = (a_1\ b_1)(a_2\ b_2)\cdots (a_m\ b_m) \in S(X)\) (product of disjoint transpositions).
Then if \(\sigma \in C_{S(X)}(\tau)\), we see that \(\tau^{-1} = \tau\) and \(\sigma = \tau \sigma \tau^{-1}\).
If we consider \(a_i\), \(\sigma(a_i) = \tau \sigma(b_i)\). Therefore, \(\sigma(b_i)\)
must be one of the \(a_j\) or \(b_j\) otherwise we would get that \(\sigma(a_i) = \sigma(b_i)\).
Furthermore, notice that if \(\sigma(b_i) = a_j\), then \(\sigma(a_i) = b_j\).

Therefore if we let \(Y = \{a_1, b_1, \ldots a_m, b_m\}\), \(\sigma \in G_Y\).
If \(m= 1\) then we see that \(G_Y = C_{S(X)}(\tau)\). However, in
general, consider \(\mu \in S_m\), then

\begin{equation*}
\sigma(x) = \begin{cases}
a_{\mu(i)}\text{ or } b_{\mu(i)} \quad\text{if } x \in \{a_i, b_i\} \text{ for some } 1 \leq i \leq m\\
x \quad\text{otherwise}
\end{cases}
\end{equation*}

Then, we have \(2^mm!\) choices for choosing ways of permuting \(x \in Y\)
and \((n - 2m)!\) ways of permuting \(X \backslash Y\).
Therefore, we see that \(|C_{S(X)}(\tau)| = 2^m m!(n-2m)!\).

\subsection{Dihedral Group}
\label{develop--math3272:group-actions:page--examples.adoc---dihedral-group}

Consider the dihedral group \(D_n\), then it acts on the points of a regular \(n\)-gon
where

\begin{itemize}
\item \(a\) is rotation by \(\frac{2\pi}{ n }\)
\item \(b\) is reflection through the perpendicular bisector of points \(1\) and \(2\)
\end{itemize}

Note that when we
\myautoref[{initially defined}]{develop--math2272:ROOT:page--symmetries/index.adoc---dihedral-group}
\(D_n\), we utilized a similar group action.

\subsection{Symmetric group on multinomials}
\label{develop--math3272:group-actions:page--examples.adoc---symmetric-group-on-multinomials}

The group \(S_n\) acts on \(R[x_1, x_2, \ldots x_n]\) where

\begin{equation*}
\sigma \cdot f(x_1, \ldots x_n) = f(x_{\sigma(1)}, \ldots x_{\sigma(n)})
\end{equation*}

Proving that this operation is compatible is a bit tricker than we might initially think
simply because of how we defined our operation. Instead, lets define it as

\begin{equation*}
\sigma \cdot f(x_{i_1}, x_{i_2}, \ldots x_{i_n}) = f(x_{\sigma(i_1)}, x_{\sigma(i_2)}, \ldots x_{\sigma(i_n)})
\end{equation*}

Then, it becomes clear that we indeed have a group action.

Also, note that we see there is a isomorphism between \(\mathbb{R}^n\) and \(\mathbb{R}[x_1, \ldots x_n]\)
and hence there is the analogous isomorphism. Notice that

\begin{equation*}
\sigma \cdot (c_1, \ldots c_n)
\rightarrow \sigma \cdot (c_1 x_1 + c_2 x_2 + \cdots c_n x_n)
\rightarrow (c_1 x_{\sigma(1)} + c_2 x_{\sigma(2)} + \cdots c_n x_{\sigma(n)})
\rightarrow (c_{\sigma^{-1}(1)}, \ldots c_{\sigma^{-1}(n)})
\end{equation*}

Why wouldn't \(c_i\to c_{\sigma(i)}\) work? Consider

\begin{equation*}
\tau\sigma (c_1, \ldots c_n) = \tau (c_{\sigma(1)}, \ldots c_{\sigma(n)})
\end{equation*}

Then, the first entry is not \(c_{\tau\sigma(1)}\) but \(c_{\sigma(j)}\) where \(\tau(1) = j\).
That is, our composition is \(c_{\sigma\tau(1)}\) and hence we see why
we may rather take the inverse.

\section{Matricies}
\label{develop--math3272:group-actions:page--examples.adoc---matricies}

Let \(G = GL(n, \mathbb{F})\) then \(G\) acts on \(\mathbb{F}^n\).

\section{Mobius transforms}
\label{develop--math3272:group-actions:page--examples.adoc---mobius-transforms}

Let \(G\) be the group of
\myautoref[{mobius transforms}]{develop--math3275:conformal-mappings:page--mobius-transformation.adoc---formation-of-a-group}.
Then if we define

\begin{equation*}
\begin{aligned}
H &= \{f \in G: \det(f) > 0\} \leq G\\
X &= \{z \in \mathbb{C}: \Im(z) > 0\}
\end{aligned}
\end{equation*}

Then, \(H\) acts on \(X\).

\section{Rubix cubes}
\label{develop--math3272:group-actions:page--examples.adoc---rubix-cubes}

Let \(X\) be the set of ways to place stickers on a Rubix cube.
Then, if \(G\) is the group of operations generated by

\begin{itemize}
\item Turning any of the 6 outside layers
\item Reorienting the cube
\end{itemize}

Then \(G\) acts on \(X\). Furthermore, the orbit
of the Rubix cube in its ``solved'' state
is the set of solvable Rubix cubes.
\end{document}
