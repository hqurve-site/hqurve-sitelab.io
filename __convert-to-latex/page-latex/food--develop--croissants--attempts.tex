\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Attempts}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
This is moved to another page as it loads many large images.

\section{Attempt 1}
\label{develop--food:croissants:page--attempts.adoc---attempt-1}

This was perfectly done.

\section{Attempt 2}
\label{develop--food:croissants:page--attempts.adoc---attempt-2}

After attempt 1, I believed that I had allowed the dough to rise too much as the original
recipe stated to allow the dough to rise to twice its size before beginning to roll. Hence,
I promptly began working the dough as soon as it reached twice it size.
This caused the dough to rise during the rolling and folding process causing me to get only
get 2-3 folds before dough began ripping.

The next day was not kind either as the dough was again ripping and releasing too much margarine
onto the kitchen counter. This caused me to give up shaping half way through.

After baking, because the dough had not completely risen prior to the initial rolling,
there was a lot of trapped yeast byproducts in the croissants, hence giving it a yeasty flavour.

Recommendations for next attempt

\begin{itemize}
\item Allow the dough to completely rise
\item Split dough into more manageable amounts for easy folding and rolling.
\end{itemize}

\section{Attempt 3}
\label{develop--food:croissants:page--attempts.adoc---attempt-3}

I took the previous recommendations into consideration and overall, it was much better.
However, I still believe it can be improved

\begin{itemize}
\item Ensure that no yeast is lost. I believe that some may have been stuck on the spoon and pot
\item After baking the croissants are easy to come apart; in particular, the outer layers while cutting.
    I believe this may be caused by using too much flour right before shaping the croissant.
    As such I believe that if a sprinkle water is applied right before shaping, the dough would be able
    to join properly.
\item Upon inspection from my aunt who is a cooking teacher, she found that the croissants were `bready'
    (mainly towards the middle). I suspect this is may be since some yeast was lost. However, another possibility
    is that the center of the roll was too tight. Perhaps next time, space should be left in the center
    of the roll so that the entire croissant is able to grow while baking instead of just
    the outer layers.
\end{itemize}

\subsection{Images}
\label{develop--food:croissants:page--attempts.adoc---images}

\begin{table}[H]\centering
\caption{Batches}

\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Batch} & {Pre} & {Post} \\
\hline[\thicktableline]
{Batch 1} & {\begin{tblr}{colspec={Q[l,h] Q[l,h]}, measure=vbox}
{\begin{subfigure}{0.15\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/pre-cook-batch-1-0}
\end{subfigure}} & {\begin{subfigure}{0.15\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/pre-cook-batch-1-1}
\end{subfigure}} \\
\SetCell[r=1, c=2]{}{\begin{subfigure}{0.3\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/pre-cook-batch-1-2}
\end{subfigure}} &  \\
\end{tblr}} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/post-cook-batch-1}
\end{subfigure}} \\
\hline
{Batch 2} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/pre-cook-batch-2}
\end{subfigure}} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/post-cook-batch-2}
\end{subfigure}} \\
\hline
{Batch 3} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/pre-cook-batch-3}
\end{subfigure}} & {} \\
\hline
{Batch 4} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/pre-cook-batch-4}
\end{subfigure}} & {\begin{subfigure}{0.4\textwidth}\centering
\includegraphics[]{images/develop-food/croissants/post-cook-batch-4}
\end{subfigure}} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-food/croissants/final-0}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-food/croissants/final-1}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-food/croissants/final-2}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-food/croissants/final-3}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-food/croissants/final-4}
\end{figure}
\end{document}
