\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Random Variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
In order to easier deal with data from an experiment, the outcomes are
transformed based on various properties into either categories or
numerical values. Furthermore, these categories can be encoded as
numerical values which are usually a subset of the real numbers. This
transformation or mapping is referred to as \emph{random variable}; that is,
a random variable is a function which maps the outcomes of a statistical
experiment onto the set of real numbers (usually a subset).

\section{Side note}
\label{develop--math2274:ROOT:page--random/index.adoc---side-note}

Apparently a random variable really is a function which maps to any
measurable set, not necessarily the reals. However, for this course, it
will only be mapped to the reals.

\section{Types of random variables}
\label{develop--math2274:ROOT:page--random/index.adoc---types-of-random-variables}

The approach used to analyze random variables is typically based on the
range of the random variable. Typically, if the range is countable, ie
either finite or countably infinite, the random variable is referred to
as \emph{discrete}. Otherwise, if the range is uncountably infinite, it is
called \emph{continuous}. However, this classification is not strictly
correct since there are random variables which can be considered both
discrete and continuous. Nevertheless, both types will be discussed
simultaneously.

\section{Probability mass/density function}
\label{develop--math2274:ROOT:page--random/index.adoc---probability-massdensity-function}

Let \(X\) be a random variable.

\subsection{Discrete Random Variable}
\label{develop--math2274:ROOT:page--random/index.adoc---discrete-random-variable}

If \(X\) is discrete, it has a probability (mass) function,
\(f\), defined by

\begin{equation*}
f(x) = P(X = x)
\end{equation*}

and hence has the properties that

\begin{enumerate}[label=\arabic*)]
\item \(0 \leq f(x) \leq 1\) forall \(x\) in the range
    of \(X\).
\item \(\displaystyle\sum_x f(x) = 1\)
\end{enumerate}

\subsection{Continuous Random Variable}
\label{develop--math2274:ROOT:page--random/index.adoc---continuous-random-variable}

If \(X\) is continuous, it has a probability density function,
\(f\), such that for all \(a\) and \(b\)

\begin{equation*}
P(a < X \leq b) = \int_a^b f(t) dt
\end{equation*}

Notice that there is little concern whether or not either of the end
points are strict since the endpoints of an integral can be considered
as limits. Furthermore, \(f\) has the following properties

\begin{enumerate}[label=\arabic*)]
\item \(f(x) \geq 0 \; \forall x \in \mathbb{R}\)
\item \(\int_{-\infty}^{\infty} f(x) dx = 1\)
\end{enumerate}

Notice that unlike in the discrete case, the probability density
function is not a probability.

\section{Probability distribution function}
\label{develop--math2274:ROOT:page--random/index.adoc---probability-distribution-function}

Let \(X\) be a random variable, then the probability
distribution function (or cumulative distribution function),
\(F\), completely defines \(X\) where
\(F(x)\) is defined as

\begin{equation*}
F(x) = P(X \leq x)
\end{equation*}

\subsection{Properties}
\label{develop--math2274:ROOT:page--random/index.adoc---properties}

If \(F\) is a valid pdf, it has the following properties

\begin{enumerate}[label=\arabic*)]
\item \(0 \leq F(x) \leq 1 \quad \forall x\in \mathbb{R}\)
\item \(F(x)\) is monotonic increasing. That is
    
    \begin{equation*}
    \forall\, x, y\in \mathbb{R}: x < y \implies F(x) \leq F(y)
    \end{equation*}
\item \(\displaystyle\lim_{x\rightarrow -\infty} F(x) = 0\) and
    \(\displaystyle\lim_{x\rightarrow +\infty} F(x) = 1\)
\item \(F(x)\) is right continuous . That is
    
    \begin{equation*}
    \forall a\in \mathbb{R}: F(a) = \lim_{x\rightarrow a^+} F(x)
    \end{equation*}
\end{enumerate}

Notice that all these properties can be established using the axioms of
probability.

\subsection{Computation}
\label{develop--math2274:ROOT:page--random/index.adoc---computation}

The probability distribution function is computed slightly differently
for the discrete and continuous cases as follows

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,h] Q[c,h]}, measure=vbox}
{\(\displaystyle F(x) = \sum_{t \leq x} f(t) \)} & {\(\displaystyle F(x) = \int_{-\infty}^x f(t) dt \)} \\
{Discrete Random Variable} & {Continuous Random Variable} \\
\end{tblr}
\end{table}

\subsection{Note about bounds for continuous random variables}
\label{develop--math2274:ROOT:page--random/index.adoc---note-about-bounds-for-continuous-random-variables}

It is easily seen that for a continuous random variable,
\(P(X = x) = 0\) for all \(x \in \mathbb{R}\).
Therefore, the probabilities of open and closed intervals are the same.
That is

\begin{equation*}
P(a < X \leq b) = P(a \leq X < b) = P(a \leq X \leq b) = P(a < X < b)
\end{equation*}

\section{Expectation and Variance}
\label{develop--math2274:ROOT:page--random/index.adoc---expectation-and-variance}

Let \(X\) be a random variable then its \emph{expected value} or
\emph{mean} , \(E[X]\) or \(\mu_X\), is defined as

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,h] Q[c,h]}, measure=vbox}
{\(\displaystyle E[X] = \sum_{t} tf(t)\)} & {\(\displaystyle E[X] = \int_{-\infty}^\infty tf(t) dt\)} \\
{Discrete Random Variable} & {Continuous Random Variable} \\
\end{tblr}
\end{table}

The expected value of a random variable \(X\) can be thought
of as a 'typical value' of \(X\) and is a sort of weighted
average. Notice, that the expected value doesn’t always exist or is
finite.

\subsection{Equivalent formulation for random variables with integer values}
\label{develop--math2274:ROOT:page--random/index.adoc---equivalent-formulation-for-random-variables-with-integer-values}

If \(X\) is a discrete random variable taking integer values,
then its expectation can be written as

\begin{equation*}
\begin{aligned}
    E[X]
    &= \sum_{x=-\infty}^\infty xP (X = x)\\
    &= \sum_{x=1}^\infty xP (X = x) + \sum_{x=1}^{\infty} (-x) P (X = -x)\\
    &= \sum_{x=1}^\infty \sum_{k=1}^x P (X = x) - \sum_{x=1}^{\infty} \sum_{k=1}^{x} P (X = -x)\\
    &= \sum_{k=1}^\infty \sum_{x=k}^\infty P (X = x) - \sum_{k=1}^{\infty} \sum_{x=k}^{\infty} P (X = -x)\\
    &= \sum_{k=1}^\infty P(X \geq k) - \sum_{k=1}^{\infty} P(X \leq -k)\\
    &= \sum_{k=1}^\infty \left[ P(X \geq k) - P(X \leq -k) \right]\end{aligned}
\end{equation*}

\begin{admonition-note}[{}]
An analogous formulation holds for continuous random variables using
almost the same steps.
\end{admonition-note}

\subsection{Expectation of a function}
\label{develop--math2274:ROOT:page--random/index.adoc---expectation-of-a-function}

If \(g\) is a real-valued function, then the expectation of
\(g(X)\) is

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,h] Q[c,h]}, measure=vbox}
{\(\displaystyle E[g(X)] = \sum_{t} g(t)f(t)\)} & {\(\displaystyle E[g(X)] = \int_{-\infty}^\infty g(t)f(t) dt\)} \\
{Discrete Random Variable} & {Continuous Random Variable} \\
\end{tblr}
\end{table}

This is useful when the a transformation of the values of a random
variable is required. Note that expectation of a function can be thought
of function composition of \(g\) with \(X\) since
\(X\) is itself a function. Furthermore, \(g(X)\) is
indeed, in its own right, a random variable.

\begin{admonition-caution}[{}]
Apparently, this result is not merely a definition but is infact a
theorem called ''The Law of the Unconscious Statistician'' and arises
from the fact that \(g(X)\) is a random variable. The law is
not hard to prove but I will leave it out.
\end{admonition-caution}

\subsection{Linearity of the expectation}
\label{develop--math2274:ROOT:page--random/index.adoc---linearity-of-the-expectation}

The expectation is indeed a linear transformation from the function
space \(\{g:\mathbb{R} \rightarrow \mathbb{R}\}\) to the
reals, \(\mathbb{R}\). That is, the expectation satisfies the
following properties

\begin{enumerate}[label=\arabic*)]
\item \(E[g(X) + h(X)] = E[g(X)] + E[h(X)]\)
\item \(E[a\, g(X)] = aE[g(X)]\)
\end{enumerate}

These properties follow from the fact that summation/integral is also a
linear transformation. Additionally, since the total probability of the
sample space is \(1\) (which is a constant function), then

\begin{equation*}
E[1] =  1
\end{equation*}

Together these three properties help simplify the computation of
complicated expectations.

\subsection{Moments}
\label{develop--math2274:ROOT:page--random/index.adoc---moments}

The \(r\)’th moment of \(X\) is given by

\begin{equation*}
\mu_r = E[X^r]
\end{equation*}

this will be useful later.

\subsection{Fair games}
\label{develop--math2274:ROOT:page--random/index.adoc---fair-games}

A game, whose winnings follow the random variable \(X\), is
\emph{fair} if

\begin{equation*}
E[X] = c
\end{equation*}

where \(c\) is the cost to play a game.

\subsection{Variance}
\label{develop--math2274:ROOT:page--random/index.adoc---variance}

If \(X\) has a finite mean and second moment then the
\emph{variance} of \(X\) denoted \(Var[X]\) is given by

\begin{equation*}
\sigma^2 = Var[X] = E[(X - \mu)^2]
\end{equation*}

and can be simplified as follows

\begin{equation*}
\begin{aligned}
    Var[X]
    &= E[(X - \mu)^2]\\
    &= E[X^2 - 2\mu X + \mu^2]\\
    &= E[X^2] + E[-2\mu X] + E[\mu^2]\\
    &= E[X^2] -2\mu E[X] + \mu^2\\
    &= E[X^2] - E[X]^2\end{aligned}
\end{equation*}

Note that the latter formula is much simpler to use in practice.
However, the first supplies intuition as to what the variance means.

In fact, the variance is a measure of how ''spread-out'' the range of
\(X\) is, or how much \(X\) deviates from the mean.

\begin{admonition-note}[{}]
Unlike the expectation, variance is not a linear operator. Nevertheless,
it exhibits the following properties

\begin{enumerate}[label=\arabic*)]
\item \(Var[a] = E[(a - E[a])^2] = E[0] = 0\)
\item \(Var[X + a] = E[(X +a - E[X + a])^2] = E[(X - E[X])^2] = Var[X]\)
\item \(Var[aX] = E[(aX)^2] - E[aX]^2 = a^2E[X] - a^2E[X]^2 = a^2Var[X]\)
\end{enumerate}

Note that the previous properties were stated in terms of a basic random
variable, \(X\), with no function applied. But since
\(g(X)\) is also a random variable, as previously discussed,
the previous properties also apply to it.
\end{admonition-note}

\section{Percentiles}
\label{develop--math2274:ROOT:page--random/index.adoc---percentiles}

The \(p\)th percentile, \(\theta_p\), of
\(X\) is defined by

\begin{equation*}
F(\theta_p) = P(X \leq \theta_p) = \frac{p}{100}
\end{equation*}

where \(p \in [0, 100] \subset \mathbb{R}\). The percentile
can be thought of a sort of inverse for the distribution function
(although multiplying by \(100\)).

\subsection{Median}
\label{develop--math2274:ROOT:page--random/index.adoc---median}

The median of \(X\) is defined as the \(50\)th
percentile. Note, that for symmetric distributions (distributions
symmetric about their mean), the median is equal to the mean.

\section{Transformation of random variables}
\label{develop--math2274:ROOT:page--random/index.adoc---transformation-of-random-variables}

This is pretty basic and I don’t really want to go through it. Basically
it amounts to finding the range and pre-image (not necessarily inverse)
of the transformed variable.

\subsection{Continuous case}
\label{develop--math2274:ROOT:page--random/index.adoc---continuous-case}

If \(X\) and \(Y\) are continuous random variables
such that \(Y = g(X)\) where \(g\) is bijective and
differentiable (both almost everywhere), then the probability density
function of \(Y\) is

\begin{equation*}
f(g^{-1}(y)) |J_{g^{-1}}|
\end{equation*}

where \(f\) is the probability density function of
\(X\) and \(J_{g^{-1}}\) is the jacobian of
transformation \(g^{-1}\).

\subsubsection{Remark}
\label{develop--math2274:ROOT:page--random/index.adoc---remark}

I’m pretty sure this follows directly from the fundamental theorem of
calculus.
\end{document}
