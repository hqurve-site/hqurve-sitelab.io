\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Isolated and Limit Points}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(A \subseteq X\) then we define the following

\begin{description}
\item[Isolated Point] \(x \in A\) is an \emph{isolated point} if \(\exists r > 0\) such that
    \(B(x,r) \cap A = \{x\}\). This could be equivalently stated
    as \((B(x,r) \backslash \{x\}) \cap A = \varnothing\).
\item[Limit Point] \(x \in X\) is a \emph{limit point} if \(\forall r > 0\), \((B(x,r) \backslash \{x\}) \cap A \neq \varnothing\).
    The set of limit points is called the \emph{derived set} of \(A\) and is denoted \(A'\).
\end{description}

\begin{admonition-caution}[{}]
Unlike limit points, isolated points are limited to the set in question.
\end{admonition-caution}

Now, if we limit \(x\in A\), we see that the definition of a limit point is the negation of the definition of an isolated
point hence each point in \(A\) is either a limit or isolated point. In particular
the set of isolated points is \(A \backslash A'\).

\section{Limit points of the derived set}
\label{develop--math3277:topology:page--isolated-limit.adoc---limit-points-of-the-derived-set}

Let \(A\subseteq X\) the

\begin{equation*}
(A')' \subseteq A'
\end{equation*}

\begin{example}[{Proof}]
Consider \(x \in (A')'\) then

\begin{equation*}
\forall r > 0: (B(x,r)\backslash \{x\}) \cap A' \neq \varnothing
\end{equation*}

Consider some fixed \(r > 0\) and \(y \in (B(x,r)\backslash \{x\}) \cap A'\). Then \(y \in A'\)
and let \(s = \min\{d(x,y), r-d(x,y)\} > 0\). Then, \((B(y,s) \backslash \{y\}) \cap A \neq \varnothing\).
Now, notice that \(d(x,y) \geq \min\{d(x,y), r-d(x,y)\} = s\) and hence \(x \notin B(y,s)\). Also,
\(B(y,s) \subseteq B(x,r)\) since

\begin{equation*}
z \in B(y,s) \implies d(y,z) < s \leq r-d(x,y) \implies d(x,z) \leq d(x,y) + d(y,z) < r \implies z \in B(x,r)
\end{equation*}

Then

\begin{equation*}
\varnothing
\subset (B(y,s) \backslash \{y\}) \cap A
\subseteq B(y,s) \cap A = (B(y,s)
\backslash \{x\}) \cap A
\subseteq (B(x,r) \backslash \{x\}) \cap A
\end{equation*}

And we are done.

For a counter example as to why equality holds, consider \(A = \{\frac{1}{n}: n \in \mathbb{N}\}\)
\end{example}

\section{Nice properties}
\label{develop--math3277:topology:page--isolated-limit.adoc---nice-properties}

Let \(A, B \subseteq X\), then the following holds

\begin{enumerate}[label=\arabic*)]
\item \(A \subseteq B \implies A' \subseteq B'\)
\item \((A\cup B)' = A' \cup B'\)
\item \((A\cap B)' \subseteq A' \cap B'\)
\end{enumerate}

\begin{example}[{Proof}]
\begin{example}[{Proof of 1}]
This is a direct proof

\begin{equation*}
\begin{aligned}
x \in A'
&\implies \forall r > 0:
\varnothing \subset (B(x,r) \backslash \{x\}) \cap A \subseteq (B(x,r)\backslash\{x\})\cap B
\\&\implies x \in B'
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Proof of 2}]
\begin{equation*}
\begin{aligned}
x \in (A \cup B)'
&\iff \forall r> 0: (B(x,r)\backslash \{x\}) \cap (A \cup B) \neq \varnothing
\\&\iff \forall r> 0: [(B(x,r)\backslash \{x\}) \cap A] \cup [(B(x,r)\backslash \{x\}) \cap B] \neq \varnothing
\\&\iff \forall r> 0: (B(x,r)\backslash \{x\}) \cap A \neq \varnothing \text{ or } (B(x,r)\backslash \{x\}) \cap B \neq \varnothing
\\&\impliedby [\forall r> 0: (B(x,r)\backslash \{x\}) \cap A \neq \varnothing] \text{ or }
    [\forall r > 0: (B(x,r)\backslash \{x\}) \cap B] \neq \varnothing
\\&\iff x \in A' \text{ or } x \in B'
\\&\iff x\in A' \cup B'
\end{aligned}
\end{equation*}

For the forward direction of the forth implication, consider the contrapositive. That is
\(\exists r_1, r_2 > 0\) such that \((B(x,r_1) \backslash \{x\}) \cap A = \varnothing\)
and \((B(x,r_2) \backslash \{x\}) \cap B = \varnothing\). Then let \(r = \min\{r_1,r_2\}\) then
since balls of smaller radius are nested, \((B(x,r) \backslash \{x\})\cap A = \varnothing\)
and \((B(x,r)\backslash \{x\}) \cap B = \varnothing\). Hence, \(\exists r > 0\) such that

\begin{equation*}
(B(x,r)\backslash \{x\}) \cap A = \varnothing \text{ and }(B(x,r)\backslash \{x\}) \cap B = \varnothing
\end{equation*}
\end{example}

\begin{example}[{Proof of 3}]
\begin{equation*}
\begin{aligned}
x \in (A\cap B)'
&\iff \forall r > 0: (B(x,r)\backslash \{x\}) \cap (A\cap B) \neq \varnothing
\\&\iff \forall r > 0: ((B(x,r)\backslash \{x\}) \cap A) \cap ((B(x,r)\backslash \{x\}) \cap B) \neq \varnothing
\\&\implies \forall r > 0: (B(x,r)\backslash \{x\}) \cap A \neq \varnothing \text{ and } (B(x,r)\backslash \{x\}) \cap B \neq \varnothing
\\&\iff [\forall r > 0: (B(x,r)\backslash \{x\}) \cap A \neq \varnothing] \text{ and }
        [\forall r > 0: (B(x,r)\backslash \{x\}) \cap B \neq \varnothing]
\\&\iff x \in A' \text{ and } x \in B'
\\&\iff x \in A'\cap B'
\end{aligned}
\end{equation*}

The reverse direction of fails since two sets being non-empty, does not imply that their intersection is non empty.
For an example, consider \(A = (0,1), B = (1,2) \subseteq \mathbb{R}\) with the usual metric. Then
\((A\cap B)' = \varnothing\) however \(A' \cap B' = \{1\}\).
\end{example}
\end{example}
\end{document}
