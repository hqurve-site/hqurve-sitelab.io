\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Gamma Distribution}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A continuous random variable \(X\) has a gamma distribution
with parameters \(\alpha > 0\) and \(\beta > 0\) ,
written as

\begin{equation*}
X \sim Gamma(\alpha, \beta)
\end{equation*}

if its probability density function is given by

\begin{equation*}
f(x) = \begin{cases}
        \frac{1}{\beta^\alpha \Gamma(\alpha)}x^{\alpha - 1}e^{-\frac{x}{\beta}} \quad&\text{if } x\geq 0\\
        0                                                                          \quad&\text{otherwise}
    \end{cases}
\end{equation*}

where \(\Gamma: \mathbb{R}\rightarrow\mathbb{R}\) is the gamma
function. Furthermore, \(\alpha\) is sometimes referred to as
the \emph{shape parameter} while \(\beta\) is referred to as the
\emph{scale parameter}, for obvious reasons

\section{Moment}
\label{develop--math2274:ROOT:page--random/continuous/gamma.adoc---moment}

\begin{equation*}
\begin{aligned}
    E[X^r]
    &= \int_{-\infty}^{\infty} t^rf(t) dt\\
    &= \int_{0}^{\infty} \frac{t^r}{\beta^\alpha \Gamma(\alpha)}t^{\alpha - 1}e^{-\frac{t}{\beta}} dt\\
    &= \frac{1}{\Gamma(\alpha)}\int_{0}^{\infty} \frac{t^{\alpha + r- 1}}{\beta^\alpha}e^{-\frac{t}{\beta}} dt\\
    &= \frac{\beta^{r}}{\Gamma(\alpha)}\int_{0}^{\infty} \frac{t^{\alpha + r- 1}}{\beta^{\alpha + r -1}}e^{-\frac{t}{\beta}} \frac{1}{\beta}dt\\
    &= \frac{\beta^{r}}{\Gamma(\alpha)}\int_{0}^{\infty} s^{\alpha + r- 1}e^{-s}ds\\
    &= \frac{\beta^{r}}{\Gamma(\alpha)}\Gamma(\alpha + r)\\
    &= \frac{\Gamma(\alpha + r)}{\Gamma(\alpha)}\beta^{r}\end{aligned}
\end{equation*}

\section{Expectation}
\label{develop--math2274:ROOT:page--random/continuous/gamma.adoc---expectation}

\begin{equation*}
E[X] = \frac{\Gamma(\alpha + 1)}{\Gamma(\alpha)}\beta = \alpha\beta
\end{equation*}

\section{Variance}
\label{develop--math2274:ROOT:page--random/continuous/gamma.adoc---variance}

\begin{equation*}
Var[X]
    = E[X^2] - E[X]^2
    = \frac{\Gamma(\alpha + 2)}{\Gamma(\alpha)}\beta^2 - \alpha^2\beta^2
    = \left[ (\alpha+1)\alpha - \alpha^2 \right] \beta^2
    = \alpha\beta^2
\end{equation*}

\section{Moment Generating Function}
\label{develop--math2274:ROOT:page--random/continuous/gamma.adoc---moment-generating-function}



\begin{equation*}
\begin{aligned}
    M_X(t)
    &= E[e^{Xt}]\\
    &= \frac{1}{\beta^\alpha \Gamma(\alpha)} \int_0^\infty e^{xt} x^{\alpha - 1}e^{-\frac{x}{\beta}} dx\\
    &= \frac{1}{\beta^\alpha \Gamma(\alpha)} \int_0^\infty x^{\alpha - 1}e^{-x(1/\beta - t)} dx
            \quad \text{let } s = x\left( \frac{1}{\beta} - t \right)\\
    &= \frac{1}{\beta^\alpha \Gamma(\alpha)}
            \int_0^\infty \left( \frac{s}{1/\beta - t} \right)^{\alpha - 1}e^{-s} \frac{1}{1/\beta - t} ds\\
    &= \frac{1}{\beta^\alpha \Gamma(\alpha)} \frac{1}{(1/\beta - t)^\alpha}
            \int_0^\infty s^{\alpha - 1}e^{-s} ds\\
    &= \frac{1}{\beta^\alpha \Gamma(\alpha)} \frac{1}{(1/\beta - t)^\alpha} \Gamma(\alpha)\\
    &= \frac{1}{\beta^\alpha (1/\beta - t)^\alpha}\\
    &= \frac{1}{ (1 - t\beta)^\alpha}\end{aligned}
\end{equation*}

Note that \(s\) must be greater than 0 for the above integral
to converge. Therefore, the above holds iff
\(\frac{1}{\beta} - t > 0 \Longleftrightarrow t\beta < 1\)
\end{document}
