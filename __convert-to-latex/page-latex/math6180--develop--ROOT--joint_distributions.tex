\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Joint Random Variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-note}[{}]
In this section we mostly speak of a two joint random variables.
However, the definitions/statements can be extended to more than
two joint random variables.
See \myautoref[{}]{develop--math3278:distribution-theory:page--several-random-variables.adoc}
\end{admonition-note}

Let \((X,Y)\) be a joint random variable with pdf \(f(x,y)\).
Then the \emph{marginal distributions} are defined as

\begin{equation*}
f_X(x) = \int_{\mathbb{R}} f(x,y) \ dy
\quad\text{and}\quad
f_Y(y) = \int_{\mathbb{R}} f(x,y) \ dx
\end{equation*}

and the conditional density functions are given as

\begin{equation*}
f_{X|Y=y}(x) = \frac{f(x,y)}{f_Y(y)}
\quad\text{and}\quad
f_{Y|X=x}(y) = \frac{f(x,y)}{f_X(x)}
\end{equation*}

Note that

\begin{itemize}
\item \(f_X(x)\) and \(f_{X|Y=y}\) are valid pdfs for \(X\)
\item \(f_Y(y)\) and \(f_{Y|X=x}\) are valid pdfs for \(Y\)
\end{itemize}

The conditional expectation/variance are just defined
using the pdfs of the conditional distributions.
There is no need to speak of the marginal expectation/variance;
but the result is the same whether you use the joint pdf or marginal distributions.

\section{Laws}
\label{develop--math6180:ROOT:page--joint-distributions.adoc---laws}

Let \((X,Y)\) be random variables with joint pdf \(f(x,y)\).

\begin{theorem}[{Law of iterated expectation/total expectation}]
\begin{equation*}
E[X] = E_Y[E_X[X|Y]]
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
E_Y[E[X|Y]]
&= \int_{\mathbb{R}} E[X|Y=y] f_Y(y) \ dy
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x f_{X|Y=y}(x) \ dx f_Y(y) \ dy
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x \frac{f(x,y)}{f_Y(y)} \ dx f_Y(y) \ dy
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x \frac{f(x,y)}{f_Y(y)} f_Y(y) \ dx \ dy
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x f(x,y)\ dx \ dy
\\&= E[X]
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{theorem}[{Law of iterated variance/total variance}]
\begin{equation*}
Var[X] = E_Y[Var_X[X|Y]] + Var_Y[E_X[X|Y]]
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
This result is not as immediate to remember.
So, this proof goes through the trouble of finding it.
\end{admonition-note}

We will start by examining \(E_Y[Var_X[X|Y]]\) as we may expect
this to just be \(Var[X]\). (It isn't).

\begin{equation*}
\begin{aligned}
E_Y[Var_X[X|Y]] = E_Y\left[ E_X[X^2|Y] - \left(E_X[X|Y]\right)^2\right]
\end{aligned}
\end{equation*}

We actually know that \(E_Y[ E_X[X^2|Y]] = E[X^2]\) by the law
of iterated expectation.
So

\begin{equation*}
\begin{aligned}
E_Y[Var_X[X|Y]]
&= E_Y\left[ E_X[X^2|Y] - \left(E_X[X|Y]\right)^2\right]
\\&= E[X^2] - E_Y\left[ \left(E_X[X|Y]\right)^2\right]
\\&= E[X^2] - (E[X])^2 + (E[X])^2 - E_Y\left[ \left(E_X[X|Y]\right)^2\right]
\\&= Var[X] + (E_Y[E_X[X|Y]])^2 - E_Y\left[ \left(E_X[X|Y]\right)^2\right]
\\&= Var[X] - \left(E_Y\left[ \left(E_X[X|Y]\right)^2\right] - (E_Y[E_X[X|Y]])^2\right)
\\&= Var[X] - Var_Y[E_X[X|Y]]
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{theorem}[{Law of iterated conditional expectation}]
\begin{equation*}
E[X|Y=y] = E_{Z|Y=y}[E_X[X|Y=y, Z=z]]
\end{equation*}

\begin{proof}[{}]
\begin{equation*}
\begin{aligned}
E_{Z|Y=y}[E_X[X|Y=y, Z=z]]
&= \int_{\mathbb{R}} E[X|Y=y, Z=z] f_{Z|Y=y}(z) \ dz
\\&= \int_{\mathbb{R}} \left(\int_{\mathbb{R}} x f_{X|Y=y,Z=z}(x) \ dx\right) \left(\frac{f_{Y,Z}(y,z)}{f_{Y}(y)}\right) \ dz
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x f_{X|Y=y,Z=z}(x) \frac{f_{Y,Z}(y,z)}{f_{Y}(y)} \ dx \ dz
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x \frac{f(x,y,z)}{f_{Y}(y)} \ dx \ dz
\\&= \int_{\mathbb{R}} \int_{\mathbb{R}} x f_{X,Z|Y=y}(x,y,z)\ dx \ dz
\\&= E_{X}[X|Y=y]
\end{aligned}
\end{equation*}
\end{proof}
\end{theorem}

\begin{admonition-important}[{}]
The following ``law'' obtained by replacing \(X\) with \(X|Y=y\)
in the law of total expectation
is incorrect.

\begin{equation*}
E[X|Y=y] = E_Z[E_X[X|Y=y,Z]]
\end{equation*}

Consider the counterexample given by
antkam on
\url{https://math.stackexchange.com/questions/3437231/law-of-total-expectation-with-conditional-expectations}

We choose a random number \(N\) from \(1,2,3\) uniformly.

\begin{itemize}
\item Let \(X\) be \(1\) if \(N\in \{1\}\) otherwise \(0\)
\item Let \(Y\) be \(1\) if \(N\in\{1,2\}\) otherwise \(0\)
\item Let \(Z\) be \(1\) if \(N\in \{2,3\}\) otherwise \(0\)
\end{itemize}

We wish to find \(E[X|Y=1]\).
This value is clearly \(\frac{1}{2}\).
However, from the incorrect ``law''

\begin{equation*}
\begin{aligned}
E[X|Y=1]
&= E_X[X|Y=1,Z=0]P(Z=0) + E_X[X|Y=1, Z=1]P(Z=1)
\\&= (1)\frac{1}{3} + (0)\frac{2}{3}
\\&= \frac13
\end{aligned}
\end{equation*}
\end{admonition-important}

\subsection{Examples}
\label{develop--math6180:ROOT:page--joint-distributions.adoc---examples}

\begin{example}[{Random Sums}]
Suppose that \(X_1, \ldots X_n, \ldots\) is a sequence of iid random variables
with \(E[X_i]=\mu_X\) and \(Var[X_i]=\sigma^2_X\).
Also let \(N\) be a random variable which is independent of the \(X_i\)
and \(E[N]=\mu_N\) and \(Var[N]=\sigma^2_N\).
Consider the random sum \(S = \sum_{i=1}^N X_i\).
We will find \(E[S]\) and \(Var[S]\) using the laws of total expectation and variance.

First, by the law of total probability,

\begin{equation*}
E[S]
= E_N[E_{\vec{X}}[S|N]]
\end{equation*}

Also, since \(N\) is independent of the \(X_i\),

\begin{equation*}
E[S|N = n]
= E\left[\sum_{i=1}^N X_i | N=n\right]
= E\left[\sum_{i=1}^n X_i\right]
= \sum_{i=1}^n E\left[ X_i\right]
= \sum_{i=1}^n \mu_X
= n\mu_X
\end{equation*}

So, we obtain that

\begin{equation*}
E[S] = E_N[N\mu_X] = \mu_X\mu_N
\end{equation*}

Next, by the law of total expectation,

\begin{equation*}
Var[S] =
E_N[Var_{\vec{X}}[S|N]]
+ Var_N[E_{\vec{X}}[S|N]]
\end{equation*}

By using a similar technique as before, we can obtain
that \(Var_{\vec{X}}[S|N] = N\sigma^2_X\) since the \(X_i\)
are also independent of each other.
Therefore,

\begin{equation*}
Var[S] =
E_N[N\sigma^2_X] + Var_N[N\mu_X]
=\mu_N\sigma^2_X + \mu_X^2\sigma^2_N
\end{equation*}
\end{example}

\begin{example}[{Party hats}]
At a party, there are \(n\) men and they throw their hats into the center of the room.
The hats are mixed up and each man randomly selects a hat.
Let \(X_i\) be \(1\) if the i'th person picks his hat and zero otherwise.
Let \(X = \sum_{i=1}^n X_i\). We want to find \(E[X]\) and \(Var[X]\).

Note that each \(X_i \sim Bernoulli\left(\frac{1}{n}\right)\) but they are not independent.
So,

\begin{equation*}
E[X_i] = \frac{1}{n},\quad\text{and}\quad Var[X_i] = \frac{n-1}{n^2}
\end{equation*}

Therefore

\begin{equation*}
E[X] = E\left[\sum_{i=1}^n X_i\right] = \sum_{i=1}^n E[X_i] = 1
\end{equation*}

To find the variance, we use the following relation (which is yielded from the bilinearity of the covariance)

\begin{equation*}
Var[X] = \sum_{i=1}^n Var[X_i] + 2\sum_{i < j} Cov[X_i, X_j]
\end{equation*}

So, we only need to find the pairwise covariance. Note that for \(i\neq j\)

\begin{equation*}
\begin{aligned}
E[X_iX_j]
&= 0P(X_iX_j=0) + 1P(X_iX_j=1)
\\&= P(X_i=1,\ X_j=1)
\\&= P(X_i=1|X_j=1)P(X_j=1)
\\&= \frac{1}{n-1}\frac{1}{n}
\end{aligned}
\end{equation*}

So, we have that \(Cov[X_iX_j] = \frac{1}{(n-1)n} - \frac{1}{n^2}\) and hence

\begin{equation*}
\begin{aligned}
Var[X]
&= n\left(\frac{n-1}{n^2}\right) + 2\binom{n}{2}\left(\frac{1}{(n-1)n} - \frac{1}{n^2}\right)
\\&= \frac{n-1}{n} + n(n-1)\left(\frac{1}{(n-1)n} - \frac{1}{n^2}\right)
\\&= 1
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Die and coins}]
We roll a fair die (1 to 6) and then flip \(n\) coins where \(n\) is the result of the die roll.
If a head appears among the \(n\) coins, we stop. Otherwise we repeat the experiment.
What is the expected number of coin flips.

Let \(X\) be the number of coins we flip.
Let \(N\) be the result of the first die roll and let \(Z\) be the number of heads which appear in the first experiment.

By the law of total expectation, by conditioning on the result of the first die roll

\begin{equation*}
E[X] = \sum_{k=1}^6 E[X|N=k]P(N=k) = \frac{1}{6}\sum_{k=1}^6 E[X|N=k]
\end{equation*}

We need to compute \(E[X|N=k]\).
Note that \(Z|N=k \sim Binom(k, 0.5)\). So, by the law of total conditional expectation
and conditioning on whether or not a head occurs,

\begin{equation*}
\begin{aligned}
E[X|N=k]
&= E[X|N=k, Z=0]P(Z=0|N=k) + E[X|N=k, Z \neq 0]P(Z\neq 0|N=k)
\\&= (k + E[X])\frac{1}{2^k} + k\left(1-\frac{1}{2^k}\right)
\\&= k+ E[X]\frac{1}{2^k}
\end{aligned}
\end{equation*}

After substituting into \(E[X]\) and rearranging, we obtain

\begin{equation*}
E[X] = \frac{448}{107} \approx 4.19
\end{equation*}
\end{example}

\subsection{Random sums}
\label{develop--math6180:ROOT:page--joint-distributions.adoc---random-sums}

\begin{theorem}[{Probability generating function of random sum}]
Let \(X_1, X_2, \ldots\) be a sequence of iid random variables with PGF \(G_X(s)\).
Let \(N\) be a random variable independent of the \(X\)'s with PGF \(G_N(s)\).
Define the random sum

\begin{equation*}
T = \sum_{i=1}^N X_i
\end{equation*}

Then, the PGF of \(T\) is

\begin{equation*}
G_T(s) = G_N(G_X(s))
\end{equation*}

\begin{proof}[{}]
By conditioning on \(N\), we get

\begin{equation*}
G_T(s)
= E[s^T]
= E\left[s^{\sum_{i=1}^N X_i}\right]
= E_N\left[E_X\left[\left.s^{\sum_{i=1}^N X_i}\ \right|\ N\right]\right]
\end{equation*}

When \(N=n \geq 0\), we have that

\begin{equation*}
\begin{aligned}
E_X\left[\left.s^{\sum_{i=1}^N X_i}\ \right|\ N=n\right]
&= E_X\left[s^{\sum_{i=1}^n X_i}\right]\quad\text{since the }X_i \text{ are independent of }N
\\&= E_X\left[\prod_{i=1}^n s^{X_i}\right]
\\&= \prod_{i=1}^n E_X\left[ s^{X_i}\right] \quad\text{since the }X_i's\text{ are independent}
\\&= \prod_{i=1}^n G_{X_i}(s)
\\&= \prod_{i=1}^n G_{X}(s)
\\&= G_{X}(s)^n
\end{aligned}
\end{equation*}

So, we have that \(E_N\left[E_X\left[\left.s^{\sum_{i=1}^N X_i}\ \right|\ N\right]\right]= G_X(s)^N\)

\begin{equation*}
G_T(s)
= E_N\left[E_X\left[\left.s^{\sum_{i=1}^N X_i}\ \right|\ N\right]\right]
= E_N\left[G_X(s)^N\right]
= G_N(G_X(s))
\end{equation*}

as desired
\end{proof}
\end{theorem}
\end{document}
