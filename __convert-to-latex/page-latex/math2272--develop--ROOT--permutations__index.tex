\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Permutation Groups}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
A \emph{permutation} \(\sigma\) is defined as a bijection on a
finite set. Since the set is finite there is an equivalence to
\(J_n\) and hence we can equivalently represent
\(\sigma \in S(J_n)\). Furthermore, we call
\(S_n = S(J_n)\) the \emph{symmetric group of degree}
\(n\). Then immediately using combinatorial arguments,
\(|S_n| = n!\). Additionally, a subgroup of a symmetric group
is called a \emph{permutation group}.

\section{Notation}
\label{develop--math2272:ROOT:page--permutations/index.adoc---notation}

Let \(\sigma \in S_n\), then we can write

\begin{equation*}
\sigma
    = \begin{pmatrix}
        1 & 2 & 3 & \cdots & n\\
        \sigma(1) & \sigma(2) & \sigma(3) & \cdots & \sigma(n)
    \end{pmatrix}
\end{equation*}

additionally if \(\sigma_1, \sigma_2 \in S_n\), we use the
following notation to represent functional composition (the group
operation)

\begin{equation*}
\sigma_1 \sigma_2 (x) = (\sigma_1 \circ \sigma_2) (x) = \sigma_1(\sigma_2(x))
\end{equation*}

\section{Cycles}
\label{develop--math2272:ROOT:page--permutations/index.adoc---cycles}

A permutation \(\sigma \in S_n\) is called an
\(r\)-cycle if
\(\exists a_1, a_2, \ldots ,a_r \in J_n\) where all
\(a_i\) are distinct such that

\begin{equation*}
\sigma(a_1) = a_2
    ,\quad
    \sigma(a_2) = a_3
    ,\quad \cdots \quad,
    \sigma(a_{r-1}) = a_{r}
    ,\quad
    \sigma(a_r) = a_1
\end{equation*}

and \(\sigma(x) = x\) if
\(x \notin \{a_1, a_2, \ldots, a_r\}\). Additionally, we can
write \(\sigma\) as

\begin{equation*}
\sigma = (a_1\; a_2\; a_3\; \cdots\; a_r)
\end{equation*}

For intuition, the following diagram is presented

Note that the representation of a cycle is not unique however by
convention, where possible, the first element is the smallest.

\subsection{Disjoint and simplification}
\label{develop--math2272:ROOT:page--permutations/index.adoc---disjoint-and-simplification}

Two cycles \(\sigma_1 = (a_1\; a_2\; \cdots\; a_m)\) and
\(\sigma_2 = (b_1\; b_2\; \cdots\; b_n)\) are disjoint if
\(a_i \neq b_j\) \(\forall\; i, j\). Furthermore, it
is easy to see that if \(\sigma_1\) are \(\sigma_2\)
are disjoint, then they commute; ie
\(\sigma_1\sigma_2 = \sigma_2\sigma_1\). Additionally, this
product cannot be simplified (merged into a single cycle) since each
\(a_i\) must map to some \(a_j\) and similarly for
the elements of \(\sigma_2\).

\subsection{Cycle decomposition}
\label{develop--math2272:ROOT:page--permutations/index.adoc---cycle-decomposition}

Each permutation \(\sigma \in S_n\) can be uniquely written as
a product of disjoint cycles (up to permutation since composition is
commutative).

\begin{example}[{Proof}]
Let \(\sigma \in S_n\). Then, we define

\begin{equation*}
X_1 = \{1, \sigma(1), \sigma^2(2), \sigma(3), \ldots\}
\end{equation*}

where \(\sigma^n = \sigma \cdot (\sigma^{n-1})\) (ie repeated
application). Then since \(J_n\) is finite, so is
\(X_1 \subseteq J_n\). Now, if we have have
\(X_1, \ldots X_k\) there are two cases

\begin{itemize}
\item If \(\bigcup_{i=1}^k X_i = J_n\) we continue. Note that each
    of the \(X_i\) are disjoint.
\item \(\exists x \in J_n: x \notin \bigcup_{i=1}^k X_i\) then we
    define \(X_{k+1} = \{x, \sigma(x), \sigma^2(x), \ldots\}\) and
    repeat this check.
\end{itemize}

This process must end since \(J_n\) is finite. Now, we now
define

\begin{equation*}
\sigma_i = \text{ cycle defined by } X_i
\end{equation*}

for example \(\sigma_1 = (1\;\; \sigma(1)\;\cdots)\). Note
that the choice of the first element does not matter in each cycle.
Then,

\begin{equation*}
\sigma = \sigma_1\sigma_2\cdots \sigma_k
\end{equation*}

For the uniqueness of the above product (up to permutation of the
disjoint cycles), consider \(x \in J_n\). Therefore, each of
the \(X_i\) are uniquely defined and hence so are each of the
\(\sigma_i\).

 ◻
\end{example}

\subsection{Order}
\label{develop--math2272:ROOT:page--permutations/index.adoc---order}

The order of an \(r\)-cycle is \(r\). Furthermore,
the order of a general permutation is the lowest common multiple of the
orders of its disjoint cycles.

\subsection{Cycles of equal length}
\label{develop--math2272:ROOT:page--permutations/index.adoc---cycles-of-equal-length}

Two cycles \(\tau, \mu \in S_n\) have the same length if and
only if \(\exists \sigma \in S_n\) such that
\(\mu = \sigma \tau \sigma^{-1}\).

\begin{example}[{Proof}]
This proof is taken from theorem 6.16 in Judson however, note that the
second part is worded slightly differently. Also, for brevity we define
\(a_{k+1} = a_1\) and \(b_{k+1}=b_1\).

Firstly, suppose that

\begin{equation*}
\tau = (a_1\; a_2 \; \cdots\; a_k)
        \quad\text{and}\quad
        \mu = (b_1\; b_2 \; \cdots\; b_k)
\end{equation*}

Then we define the permutation \(\sigma \in S_n\)

\begin{equation*}
\sigma(a_i) = b_i \forall i = 1,\ldots,k
        \quad\text{and }
        \sigma(x) =x \text{ otherwise}
\end{equation*}

Then note that when \(t = b_i\),

\begin{equation*}
\sigma\tau\sigma^{-1}(b_i)
        = \sigma\tau(a_i)
        = \sigma(a_{i+1})
        = b_{i+1}
        = \mu(b_i)
\end{equation*}

while if \(t \neq b_i\),
\(\sigma^{-1}(t) = t \neq a_i\) and hence

\begin{equation*}
\sigma\tau\sigma^{-1}(t)
        = \sigma\tau(t)
        = \sigma(t)
        = t
        = \mu(t)
\end{equation*}

Therefore, \(\mu = \sigma\tau\sigma^{-1}\).

On the other hand, suppose that
\(\tau = (a_1\; a_2 \; \cdots\; a_k)\) and
\(\mu = \sigma\tau\sigma^{-1}\). Then, consider
\(b \in J_n\),

\begin{itemize}
\item If \(\sigma^{-1}(b) \neq a_i\),
    \(\mu(b) = \sigma\tau\sigma^{1}(b) = \sigma\sigma^{-1}(b) = b\).
\item If \(\sigma^{-1}(b) = a_i\),
    \(\mu(\sigma(a_i)) =\mu(b) = \sigma\tau\sigma^{-1}(b) = \sigma\tau(a_i) = \sigma(a_{i+1})\).
\end{itemize}

Therefore, we get that

\begin{equation*}
\mu = \sigma\tau\sigma^{1} = \left(\sigma(a_1)\; \sigma(a_2)\;\cdots\; \sigma(a_k)\right)
\end{equation*}

and the result follows.

 ◻
\end{example}

\section{Transpositions}
\label{develop--math2272:ROOT:page--permutations/index.adoc---transpositions}

The simplest permutation is one which interchanges just two elements and
is hence a \(2\)-cycle. Such a permutation is called a
\emph{transposition}. Additionally, each cycle can be written as a product of
transpositions as follows

\begin{equation*}
(a_1\; a_2\; a_3 \cdots a_r) = (a_1\; a_r)(a_1\; a_{r-1})\cdots (a_1\; a_3)(a_1 \; a_2)
\end{equation*}

and as a result any permutation can be written as a product of
transpositions.

\subsection{Acknowledgement}
\label{develop--math2272:ROOT:page--permutations/index.adoc---acknowledgement}

This section was taken from ''Abstract Algebra Theory and Applications''
by Judson and Beezer. The following two proofs are taken almost verbatim
from the aforementioned book.

\subsection{Identity as a product of transpositions}
\label{develop--math2272:ROOT:page--permutations/index.adoc---identity-as-a-product-of-transpositions}

If \((1)\) the identity permutation is written as a product of
transpositions, then the product consists of an even number of
transpositions.

\begin{example}[{Proof}]
We will prove this using induction. We write \((1)\) as

\begin{equation*}
(1) = \tau_1\tau_2\cdots \tau_r
\end{equation*}

For the base case, notice that a single transposition cannot be the
identity, then we consider \(r=2\). In such a case we are
done.

Now suppose that \(r > 2\) and all other shorter
representations of the identity are products of an even number of
transpositions. Focus on a fixed element \(a \in J_n\) which
occurs in the product. Then, the product of the first two transpositions
\(\tau_{1}\tau_{2}\) can be one of the following cases

\begin{equation*}
\begin{aligned}
        (ab)(ab) &= (1)\\
        (ab)(ac) &= (bc)(ab)\\
        (ab)(cd) &= (cd)(ab)\\
        (cd)(ab) & \\
        (bc)(de) &
    \end{aligned}
\end{equation*}

Where \(a,b,c,d,e\) are all distinct. Then if the first case
occurs, we are done since all shorter representations consists of an
even number of transpositions. On the other hand, if it is any of the
other four, we replace the product \(\tau_1\tau_2\) with their
equivalent representation on the right hand side (where applicable).

Next, we focus on the new \(\tau_2\tau_3\) and repeat the same
process. This process would either end when we get the first case or we
are left with \(a\) only occurring in the last transposition.
However, the second case cannot occur since the value mapping onto
\(a\) would never get remapped and hence the overall product
would not be the identity. Therefore, by the inductive hypothesis, we
are done.

 ◻
\end{example}

\subsection{Parity of a permutation}
\label{develop--math2272:ROOT:page--permutations/index.adoc---parity-of-a-permutation}

Clearly, the product which a permutation can be written as is not unique
however, its parity (evenness or oddness) is. The \emph{parity} of a
permutation is the parity of the number of transpositions used in one of
its product representations. That is if \(\sigma  \in S_n\)
and

\begin{equation*}
\sigma = \sigma_1\sigma_2\cdots \sigma_m = \tau_1\tau_2\cdots \tau_n
\end{equation*}

\begin{example}[{Proof}]
Notice that the inverse of \(\sigma\) can be written as

\begin{equation*}
\sigma^{-1} = \sigma_m\sigma_{m-1}\cdots\sigma_1
\end{equation*}

then

\begin{equation*}
(1) = \sigma\sigma^{-1} = (\tau_1\tau_2\cdots \tau_n)(\sigma_m\sigma_{m-1}\cdots\sigma_1)
\end{equation*}

and the result follows since all representations of the identity
permutation consists of an even number of transpositions.

 ◻
\end{example}

\subsection{Alternating Group}
\label{develop--math2272:ROOT:page--permutations/index.adoc---alternating-group}

The \emph{Alternating group of degree \(n\)} denoted
\(A_n\) is the subset of even permutations in
\(S_n\). It is clear that \(A_n\) is in fact a
subgroup of \(S_n\) by the two step subgroup test since

\begin{itemize}
\item the composition of two even permutations is also even (count the sum
    of transpositions)
\item the inverse of an even permutation is even (the inverse is simply
    given by reversing the order of the product).
\end{itemize}

Furthermore \(|A_n| = \frac{|S_n|}{2} = \frac{n!}{2}\).

\subsubsection{Note:}
\label{develop--math2272:ROOT:page--permutations/index.adoc---note}

\begin{admonition-todo}[{}]
I do not know how to prove that the order \(A_n\) is
half that of \(S_n\).
\end{admonition-todo}
\end{document}
