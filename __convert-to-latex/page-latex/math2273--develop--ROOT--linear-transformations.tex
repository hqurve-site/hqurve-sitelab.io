\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Linear Transformations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Definition}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---definition}

Let \(V\) and \(W\) be vector spaces over a field
\(\mathbb{F}\). A function \(T: V \rightarrow W\) is
called called a \emph{linear transformation} if
\(\forall \alpha \in \mathbb{F}\) and
\(\underline{u}, \underline{v} \in V\)

\begin{equation*}
T(\alpha \underline{u}) = \alpha T(\underline{u}) \quad \text{and}\quad T(\underline{u} + \underline{v}) = T(\underline{u}) + T(\underline{v})
\end{equation*}

Furthermore, the set of all linear Transformations from \(V\)
to \(W\) is denoted by \(\mathcal{L}(V, W)\). If
\(V = W\), then the linear transformation \(T\) is
called a \emph{linear operator}, and the set of linear operators is denoted
\(\mathcal{L}(V)\).

\subsection{Note}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---note}

\begin{itemize}
\item The identity function \(\theta: V \rightarrow V\) defined by
    \(\theta(\underline{u}) = \underline{u}\)
    \(\forall \underline{u} \in V\) is a linear transformation
    (called the \emph{identity operator})
\item The zero function \(\theta: V \rightarrow W\) defined by
    \(\theta(\underline{u}) = \underline{0}_W\)
    \(\forall \underline{u} \in V\) is a linear transformation
    (called the \emph{zero transformation})
\item The following statement provides an alternative to definition.
    \(T \in \mathcal{L}(V, W)\) iff
    \(\forall\, \alpha, \beta \in \mathbb{F}\) and
    \(\underline{u}, \underline{v} \in V\):
    \(T(\alpha\underline{u} + \beta\underline{y}) = \alpha T(\underline{u}) + \beta T(\underline{v})\).
\item \(T(\underline{0}_V) = \underline{0}_W\). The proof of this
    is trivial. Since \(T(\underline{0}_V) \in W\) and
    \(T(\underline{0}_V) = T(\underline{0}_V + \underline{0}_V) = T(\underline{0}_V) + T(\underline{0}_V)\)
    it follows that \(T(\underline{0}_V) = \underline{0}_W\).
\item \(T \in \mathcal{L}(V, W)\) is completely defined by
    specifying \(T(\underline{b})\) for all
    \(\underline{b}\) in any basis of \(V\).
\end{itemize}

\section{Range and null space}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---range-and-null-space}

Let \(V\) and \(W\) be vector spaces over the field
\(\mathbb{F}\) and \(T \in \mathcal{L}(V, W)\). Then

\begin{itemize}
\item The set
    \(T(V) = Ran(T) = \{T(\underline{u}) \mid \underline{u} \in V\}\)
    is called the \emph{range} or \emph{range space} of \(T\).
\item The set
    \(\ker(T) = Null(T) = \{\underline{u} \in V \mid T(\underline{u}) = \underline{0}\}\)
    is called the \emph{kernel} or \emph{null space} of \(T\).
\end{itemize}

Furthermore

\begin{enumerate}[label=\roman*)]
\item \(T(V)\) is a subspace of \(W\).
\item \(\ker(T)\) is a subspace of \(V\).
\end{enumerate}

\begin{example}[{Proof}]
\begin{example}[{Proof of (i)}]
Notice that \(\underline{0} \in T(V)\), since
\(T(\underline{0}) = \underline{0}\), and hence
\(T(V) \neq \varnothing\).

Now consider \(\underline{x}, \underline{y} \in T(V)\) and
\(\alpha, \beta \in \mathbb{F}\). Then \(\exists\)
\(\underline{u}, \underline{v} \in V\) such that
\(\underline{x} = T(\underline{u})\) and
\(\underline{y} = T(\underline{v})\). Then

\begin{equation*}
\alpha \underline{x} + \beta\underline{y} = \alpha T(\underline{u}) + \beta T(\underline{y}) = T(\alpha \underline{u} + \beta\underline{v}) \in T(V)
\end{equation*}

Therefore, \(T(V)\) is a subspace of \(W\). ◻
\end{example}

\begin{example}[{Proof of (ii)}]
Notice that \(\underline{0} \in \ker(T)\) since
\(T(\underline{0}) = \underline{0}\) and hence
\(\ker(T) \neq varnothing\).

Now consider \(\underline{u}, \underline{v} \in \ker(T)\) and
\(\alpha, \beta \in \mathbb{F}\). Then

\begin{equation*}
T(\alpha \underline{u} + \beta\underline{v}) = \alpha T(\underline{u}) + \beta T(\underline{v}) = \underline{0}
\end{equation*}

hence
\(\alpha \underline{u} + \beta \underline{v} \in \ker(T)\).
Therefore, \(\ker(T)\) is a subspace of \(V\). ◻
\end{example}
\end{example}

\section{Rank-Nullity Theorem}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---rank-nullity-theorem}

Let \(V\) and \(W\) be vector spaces over the field
\(\mathbb{F}\) and \(T \in \mathcal{L}(V, W)\) such
that \(\dim(V)\) is finite. Then we define

\begin{itemize}
\item \(Rank(T) = \dim(Ran(T))\)
\item \(Nullity(T) = \dim(Null(T))\)
\end{itemize}

which must both be finite. Then the rank-nullity theorem asserts

\begin{equation*}
Rank(T) + Nullity(T) = \dim(V)
\end{equation*}

\begin{example}[{Proof}]
Let the \(\dim(V) = n\) and
\(Nullity(T) = m\). Since \(Null(T)\) is a subspace,
and hence a vector space, there exists vectors

\begin{equation*}
\underline{b}_1, \underline{b}_2 , \ldots, \underline{b}_m \in Null(T) \subseteq V
\end{equation*}

which form a basis of \(Null(T)\). Furthermore, a basis of
\(V\) can be constructed by extending the previously stated
basis by the \(n-m\) vectors

\begin{equation*}
\underline{b}_{m+1}, \underline{b}_{m+2} , \ldots, \underline{b}_n \in V
\end{equation*}

Now we claim that the independent vectors
\(\underline{b}_{m+1}, \ldots, \underline{b}_n\) are also
independent under transformation \(T\). That is the following
vectors are independent

\begin{equation*}
T(\underline{b}_{m+1}), T(\underline{b}_{m+2}) , \ldots, T(\underline{b}_n)
\end{equation*}

Suppose that these vectors weren’t independent. Then there exists a
linear combination of them (with not all coefficients begin zero) which
is equal to zero. Let
\(a_{m+1}, a_{m+2}, \ldots, a_n \in \mathbb{F}\) be the
coefficients of this linear combination. Then

\begin{equation*}
\underline{0}
        = a_{m+1}T(\underline{b}_{m+1})+ a_{m+2}T(\underline{b}_{m+2}) + \cdots + a_nT(\underline{b}_n)
        = T(a_{m+1}\underline{b}_{m+1} +a_{m+2}\underline{b}_{m+2} + \cdots + a_n\underline{b}_n)
\end{equation*}

which implies that a linear combination of
\(\underline{b}_{m+1}, \ldots, \underline{b}_n\) which is in
the null space of \(T\). This violates the independence of the
formed basis thereby causing a contradiction. Therefore, the vectors
\(T(\underline{b}_{m+1}) , \ldots, T(\underline{b}_n)\) are
linearly independent. Furthermore,

\begin{equation*}
T(V) = span(\{T(\underline{b}_{1}), \ldots, T(\underline{b}_{m+1}), \ldots, T(\underline{b}_n)\}) = span(\{T(\underline{b}_{m+1}), \ldots, T(\underline{b}_n)\})
\end{equation*}

since
\(\underline{b}_1, \ldots, \underline{b}_m \in Null(T)\).
Therefore
\(T(\underline{b}_{m+1}) , \ldots, T(\underline{b}_n)\) form a
basis of \(T(V)\) and the result follows. ◻
\end{example}

\section{Singular}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---singular}

Let \(V\) and \(W\) be vector spaces over the field
\(\mathbb{F}\) and \(T \in \mathcal{L}(V, W)\).
Then, \(T\) is said to be \emph{singular} if
\(\exists \underline{v} \in V\) such that
\(\underline{v} \neq \underline{0}\) but
\(T(\underline{v}) = \underline{0}\). Otherwise,
\(T\) is said to be \emph{non-singular}. Furthermore, the following
statements are equivalent

\begin{enumerate}[label=\roman*)]
\item \(T\) is injective
\item \(T\) is non-singular
\item \(|\ker(T)| = 1\)
\item If \(S \subseteq V\) is linearly independent, then
    \(T(S)\) is also linearly independent
\end{enumerate}

\begin{example}[{Proof}]
These equivalences will be proven in a cyclic manner.

=== (i) \(\Rightarrow\) (ii)

Given that \(T\) is injective, suppose
\(\exists vec{v} \in V\) such that
\(T(\underline{v}) = \underline{0}\). However,
\(T(\underline{0}) = \underline{0} = T(\underline{v})\) and by
the injectivity of \(T\),
\(\underline{v} = \underline{0}\). Therefore, \(T\)
is non-singular.

=== (ii) \(\Rightarrow\) (iii)

Given that \(T\) is non-singular, clearly
\(\ker(T) = \{\underline{0}\}\) and hence the result follows.

=== (iii) \(\Rightarrow\) (iv)

Given that \(|\ker(T)| = 1\) and \(S\) is linearly
independent, suppose that \(T(S)\) is linearly dependent. Then
there exists a linear combination of \(T(S)\) (with not all
coefficients being \(0\)) which is equal to
\(\underline{0}\). By using the linearity of \(T\),
this linear combination of \(T(S)\) being
\(\underline{0}\) corresponds to a linear combination of
\(S\) which cannot be equal to \(\underline{0}\)
since \(S\) is linearly independent. However, this contradicts
the fact that \(|\ker(T)| = 1\) since
\(T(\underline{0}) = \underline{0}\). Therefore,
\(T(S)\) must ber linearly independent.

=== (iv) \(\Rightarrow\) (i)

Given that \(S\) is linearly independent implies that
\(T(S)\) is also linearly independent, suppose that
\(T\) was singular. Then \(\exists v \in V\) such
that \(\underline{v} \neq 0\) and
\(T(\underline{v}) = \underline{0}\). However, the singleton
\(\{\underline{v}\}\) is linearly independent while
\(T(\{\underline{v}\}) = \{\underline{0}\}\) is linearly
dependent. This is a contradiction and hence \(T\) is
non-singular. ◻
\end{example}

\section{Isomorphisms}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---isomorphisms}

Let \(V\) and \(W\) be vector spaces over the field
\(\mathbb{F}\) and \(T \in \mathcal{L}(V, W)\).
Then, \(T\) is called an \emph{isomorphism} if \(T\) is
bijective. Furthermore, vectors spaces \(V\) and
\(W\) are referred to as \emph{isomorphic}, denoted by
\(V \cong W\), if there is an isomorphism from \(V\)
to \(W\).

\subsection{Pre-image of an isomorphism}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---pre-image-of-an-isomorphism}

\(T\) is an isomorphism if and only if

\begin{equation*}
\forall \underline{w} \in W: \left|T^{-1}(\{\underline{w}\})\right| = 1
\end{equation*}

\begin{example}[{Proof}]
Consider any \(\underline{w} \in W\). Then
\(\exists \underline{v} \in V\) such that
\(T(\underline{v}) = \underline{w}\) since \(T\) is
onto. This implies that
\(T^{-1}(\{\underline{w}\}) \neq \varnothing\). Furthermore,
consider
\(\underline{v}_1, \underline{v}_2 \in T^{-1}(\{\underline{w}\})\).
Then
\(T(\underline{v}_1) = \underline{w} = T(\underline{v}_2) \implies \underline{v}_1 = \underline{v}_2\).
Therefore, \(\left|T^{-1}(\{\underline{w}\})\right| = 1\).

Conversely, suppose that
\(\forall \underline{w} \in W: \left|T^{-1}(\{\underline{w}\})\right| = 1\),
then \(T\) is onto. Furthermore, \(T\) is injective
since the pre-image of all \(\underline{w}\) contains at most
one element. ◻
\end{example}

\subsection{Inverse of an isomorphism}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---inverse-of-an-isomorphism}

If \(T\) is an isomorphism, \(T^{-1}\) is also an
isomorphism.

\begin{example}[{Proof}]
Since \(T\) is an isomorphism and hence a bijection,
there exists \(T^{-1}\) which is also a bijection. Now all
that is needed to show is that
\(T^{-1} \in \mathcal{L}(W, V)\).

Consider \(\underline{w}_1, \underline{w}_2 \in W\). Then
\(\exists\) unique
\(\underline{v}_1, \underline{v}_2 \in V\) such that
\(T(\underline{v_1}) = \underline{w}_1\) and
\(T(\underline{v_2}) = \underline{w}_2\). Let
\(\alpha, \beta \in \mathbb{F}\). Then since \(T\)
is a linear transformation

\begin{equation*}
T(\alpha \underline{v}_1 + \beta \underline{v}_2)
        = \alpha T(\underline{v}_1) + \beta T(\underline{v}_2)
        = \alpha\underline{w}_1 + \alpha\underline{w}_2
\end{equation*}

and hence

\begin{equation*}
T^{-1}(\alpha\underline{w}_1 + \alpha\underline{w}_2)
        = \alpha \underline{v}_1 + \beta \underline{v}_2
        = \alpha T^{-1}(\underline{w}_1) + \beta T^{-1}(\underline{w}_2)
\end{equation*}

Therefore, \(T^{-1} \in \mathcal{L}(W, V)\) and
\(T^{-1}\) is also an isomorphism. ◻
\end{example}

\subsection{Isomorphism as an equivalence relation}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---isomorphism-as-an-equivalence-relation}

The relation \(\cong\) is in fact an equivalence relation.

\begin{example}[{Proof}]
Let \(U, V, W\) be vector spaces over the field
\(\mathbb{F}\).

==== Reflexive

\(U \cong U\) since the identity function \(I\) is
an isomorphism from \(U\) to \(U\).

==== Symmetric

Suppose \(U \cong V\), then
\(\exists T \in \mathcal{L}(U, V)\) such that \(T\)
is is an isomorphism. Therefore,
\(\exists T^{-1} \in \mathcal{L}(V, U)\) which is also an
isomorphism and hence \(V \cong U\).

==== Transitive

Suppose \(U \cong V\) and \(V \cong W\). Then
\(\exists\) isomorphisms \(T_1\) and
\(T_2\) such that \(T_1 \in \mathcal{L}(U, V)\) and
\(T_2 \in \mathcal{L}(V, W)\). Immediately
\(T_1 \circ T_2\) is bijective. Also, consider
\(\underline{u}_1, \underline{u}_2 \in U\) and
\(\alpha, \beta \in \mathbb{F}\). Then

\begin{equation*}
\begin{aligned}
                    (T_2 \circ T_1)(\alpha \underline{u}_1 + \beta \underline{u}_2)
                    &= T_2(T_1 (\alpha \underline{u}_1 + \beta \underline{u}_2))\\
                    &= T_2(\alpha T_1(\underline{u}_1) + \beta T_1(\underline{u}_2))\\
                    &= \alpha T_2(T_1(\underline{u}_1)) + \beta T_2(T_1(\underline{u}_2))\\
                    &= \alpha (T_2 \circ T_1)(\underline{u}_1) + \beta (T_2 \circ T_1)(\underline{u}_2)
                \end{aligned}
\end{equation*}

and hence \(T_2 \circ T_1 \in \mathcal{L}(U, W)\). Therefore,
\(T_2 \circ T_1\) is an isomorphism from \(U\) to
\(W\) and hence \(U \cong W\).

Since \(\cong\) is reflexive, symmetric and transitive,
\(\cong\) is an equivalence relation. ◻
\end{example}

\subsection{Isomorphism with \(\mathbb{F}^n\)}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---isomorphism-with-latex-backslash-latex-backslashmathbb-latex-openbracef-latex-closebrace-latex-caretn-latex-backslash}

If \(V\) is finite dimensional with \(\dim(V) = n\),
then \(V \cong \mathbb{F}^n\). Note that this isomorphism is
what allows for the coordinate vector representation of vectors given
any basis.

\begin{example}[{Proof}]
This proof will construct a bijective, linear transformation
from \(\mathbb{F}^n\) to \(V\).

Let
\(\underline{v}_1, \underline{v}_2, \ldots, \underline{v}_n\)
be form basis of \(V\) and
\(\underline{e}_1, \underline{e}_2, \ldots,
    \underline{e}_n\) be the standard basis of
\(\mathbb{F}^n\). Now, define
\(T:\mathbb{F}^n\rightarrow V\) as follows

\begin{equation*}
T(\underline{a})
        =
        T(a_1\underline{e}_1 + a_2\underline{e}_2 +\cdots a_n\underline{e}_n)
        =
        T \left(\begin{bmatrix}
            a_1\\ a_2\\ \vdots \\ a_n
        \end{bmatrix}\right)
        =
        a_1\underline{v}_1 + a_2\underline{v}_2 +\cdots a_n\underline{v}_n
\end{equation*}

Now clearly, \(T\) is surjective since all vectors in
\(V\) can be represented as a linear combination of the
aforementioned basis which. Furthermore \(T\) is injective
since

\begin{equation*}
\underline{a} \in \ker(T) \implies a_1 = a_2 = \cdots = a_n = 0 \implies \underline{a} = \underline{0}
\end{equation*}

and hence \(\ker(T) = \{\underline{0}\}\).

Now all that remains is to show that
\(T \in \mathcal{L}(\mathbb{F}^n, V)\). This fact is easy to
show.

\begin{equation*}
T(\alpha \underline{a})
        = T(\alpha a_1\underline{e}_1 + \alpha a_2\underline{e}_2  + \cdots + \alpha a_n\underline{e}_n )
        = \alpha a_1\underline{v}_1 + \alpha a_2\underline{v}_2  + \cdots + \alpha a_n\underline{v}_n
        = \alpha T(\underline{a})
\end{equation*}

\begin{equation*}
\begin{aligned}
        T(\underline{a} + \underline{b})
        &= T((a_1+b_1)\underline{e}_1 + (a_2 + b_2)\underline{e}_2 +\cdots (a_n+b_n)\underline{e}_n)\\
        &= (a_1+b_1)\underline{v}_1 + (a_2 + b_2)\underline{v}_2 +\cdots (a_n+b_n)\underline{v}_n\\
        &= T(\underline{a}) + T(\underline{b})
    \end{aligned}
\end{equation*}

Therefore \(\mathbb{F}^n \cong V\). ◻
\end{example}

\section{Algebra of Linear Transformations}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---algebra-of-linear-transformations}

Let \(V\) and \(W\) be vector spaces over the field
\(\mathbb{F}\) and \(S, T \in \mathcal{L}(V, W)\).
Then

\begin{itemize}
\item The \emph{sum} of \(S\) and \(T\), denoted
    \(S + T\), is defined by
    
    \begin{equation*}
    (S+T)(\underline{u}) = S(\underline{u}) + T(\underline{v}),\; \forall \underline{u} \in V
    \end{equation*}
\item \emph{Scalar multiplication}, denoted \(\alpha T\) for
    \(\alpha \in \mathbb{F}\), is defined by
    
    \begin{equation*}
    (\alpha T)(\underline{u}) = \alpha T(\underline{u}),\; \forall \underline{u} \in V
    \end{equation*}
\end{itemize}

Then, \(\mathcal{L}(V, W)\) is a vector space over
\(\mathbb{F}\) where the additive identity is the zero
transformation, \(\theta\) (proof is trivial). Moreover,

\begin{equation*}
\dim(\mathcal{L}(V, W)) = \dim(V)\dim(W)
\end{equation*}

This result directly follows by representing a linear transformation by
a matrix.

\section{Matrix of a Linear Transformation}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---matrix-of-a-linear-transformation}

Let \(V\) and \(W\) be vector spaces over the field
\(\mathbb{F}\) and let \(T\in \mathcal{L}(V, W)\).
Let
\(\mathcal{B}=\{\underline{v}_1, \underline{v}_2, \ldots, \underline{v}_n\}\)
and
\(\mathcal{C}=\{\underline{w}_1, \underline{w}_2, \ldots, \underline{w}_m\}\)
be bases of \(V\) and \(W\) respectively. Then the
\(m\times n\) matrix,
\(A=[T]_\mathcal{B}^\mathcal{C}\) whose entries,
\(a_{ij}\) are defined by

\begin{equation*}
T(\underline{v}_j) = a_{1j}\underline{w}_1 + a_{2j}\underline{w}_2 + \cdots + a_{mj}\underline{w}_m\quad \text{for }j=1,2,\ldots n
\end{equation*}

is called the \emph{matrix of linear transformation} \(T\) with
respect to bases \(\mathcal{B}\) and
\(\mathcal{C}\). Also, if \(V=W\) and
\(\mathcal{B} = \mathcal{C}\), we can also denote,
\(A = [T]_\mathcal{B}\). Note that
\((a_{1j}, a_{2j},\ldots,a_{mj})\) is just the coordinate
vector \([T(\underline{v}_j)]_\mathcal{C}\) and hence
\(A\) is well-defined.

\subsection{Linear Transformations as matrix multiplication}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---linear-transformations-as-matrix-multiplication}

The matrix of linear transformation can be used to compute linear
transformations of any vector. In fact

\begin{equation*}
[T(\underline{v})]_\mathcal{C} = [T]_\mathcal{B}^\mathcal{C} [\underline{v}]_\mathcal{B} \quad \forall \underline{v} \in V
\end{equation*}

\begin{example}[{Proof}]
Since \(\mathcal{B}\) is a basis of \(V\),
\(\exists b_1, b_2,\ldots b_n \in \mathbb{F}\) such that

\begin{equation*}
\underline{v} = b_1\underline{v}_1 + b_2\underline{v}_2 +\cdots +b_n\underline{v}_n
\end{equation*}

and hence

\begin{equation*}
\begin{aligned}
_\mathcal{C}
            &= [T(b_1\underline{v}_1 + b_2\underline{v}_2 +\cdots +b_n\underline{v}_n)]_\mathcal{C}\\
            &= [b_1T(\underline{v}_1) + b_2 T(\underline{v}_2) +\cdots +b_nT(\underline{v}_n)]_\mathcal{C}\\
            &= b_1[T(\underline{v}_1)]_\mathcal{C} + b_2 [T(\underline{v}_2)]_\mathcal{C} +\cdots +b_n[T(\underline{v}_n)]_\mathcal{C}\\
            &= \begin{bmatrix}
                [T(\underline{v}_1)]_\mathcal{C},
                &[T(\underline{v}_2)]_\mathcal{C},
                &\ldots,
                &[T(\underline{v}_n)]_\mathcal{C}
            \end{bmatrix}
            (b_1, b_2, \ldots, b_n)^T\\
            &= [T]_\mathcal{B}^\mathcal{C}[\underline{v}]_\mathcal{B}
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\subsubsection{Corollary:}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---corollary}

Composition of linear transformations is equivalent to matrix
multiplication. That is, given vector spaces \(U\),
\(V\) and \(W\) with bases \(A\),
\(B\) and \(C\) (respectively) and linear
transformations \(T \in \mathcal{L}(U, V)\) and
\(S \in \mathcal{L}(V, W)\).

\begin{equation*}
[S\circ T]_A^C = [S]_B^C \times [T]_A^B
\end{equation*}

\subsection{Change of basis}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---change-of-basis}

A linear transformation which is a change of basis is just the identity
function \(I\). Therefore, if \(A\) and
\(B\) are bases of a vector space \(V\), the matrix
representing the change of basis from \(A\) to \(B\)
is \([I]_A^B\).

\subsection{Similar Matrices}
\label{develop--math2273:ROOT:page--linear-transformations.adoc---similar-matrices}

Two square matrices, \(A\) and \(B\), are similar if
there exists an invertible matrix \(P\) such that
\(P^{-1}AP = B\). This definition is equivalent to saying that
\(A\) and \(B\) both represent the same linear map
but in two different bases where \(P\) is the change of basis
matrix. Furthermore, similarity forms an equivalence relation where
those similar to diagonal matrices are called \emph{diagonalizable}.
\end{document}
