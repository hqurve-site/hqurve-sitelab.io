\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Limits}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
For the sake of brevity we will only consider functions of two
variables. However, the following can easily be extended for any number
of variables.

\section{Epsilon delta definition}
\label{develop--math2270:ROOT:page--limits.adoc---epsilon-delta-definition}

Let \(f(x, y)\) be defined in a deleted
\(\delta\)-neighbourhood of \((x_0, y_0)\). We say
that \(L\) is the limit of \(f(x, y)\) as
\(x\) approaches \(x_0\) and \(y\)
approaches \(y_0\) and write

\begin{equation*}
\lim_{(x, y) \rightarrow (x_0, y_0)} f(x, y) = L
\end{equation*}

if \(\forall \varepsilon > 0\),
\(\exists \delta > 0\) such that

\begin{equation*}
0 < |(x, y) - (x_0, y_0)| < \delta \implies |f(x,y) - L | < \varepsilon
\end{equation*}

\subsection{Definition of the neighbourhood}
\label{develop--math2270:ROOT:page--limits.adoc---definition-of-the-neighbourhood}

Note that the neighbourhood used doesn’t matter. In fact both deleted
circular and rectangular neighbourhoods are valid. That is we can define
\(0 < |(x, y) - (x_0, y_0)| < \delta\) as either

\begin{itemize}
\item circular: \(0 < \sqrt{(x-x_0)^2 + (y-y_0)^2} <\delta\)
\item rectangular: \(0 < |x - x_0| < \delta\) and
    \(0 < |y-y_0| < \delta\)
\end{itemize}

Additionally, we can notice that the circular
\(\delta\)-neighbourhood is a subset of the rectangular
\(\delta\)-neighbourhood.

\subsubsection{Worked example:}
\label{develop--math2270:ROOT:page--limits.adoc---worked-example}

Prove that
\(\lim_{(x, y)\rightarrow (1, 1)} (x^2 + 2y^2) = 3\)

\begin{proof}[{}]
required to show that for \(\varepsilon>0\),
\(\exists \delta > 0\) such that

\begin{equation*}
0 < |x-1|< \delta \text{ and } 0 < |y-1| < \delta \implies |x^2 + 2y^2 - 3| < \varepsilon
\end{equation*}

Suppose we already have such \(\delta\)

\begin{equation*}
\begin{aligned}
    |x^2 + 2y^2 - 3|
    &= |x^2 -1 + 2y^2 - 2|\\
    &\leq |x^2 - 1| + 2|y^2-1| \quad= |x - 1||x+1| + 2|y-1||y+1|\\
    &< \delta|x + 1| + 2\delta|y+1| \quad= \delta|x-1 + 2|  + 2 \delta|y-1+2|\\
    &\leq\delta(|x-1| + 2) + 2\delta(|y-1| + 2) \quad= \delta (|x-1| + 2|y-1| +6)\\
    &< \delta (3\delta + 6)\end{aligned}
\end{equation*}

Now it would be nice if \(\delta\) was also at most
\(1\) (note another number could have been used).\newline
So lets assume \(\delta \leq 1\). This gives

\begin{equation*}
|x^2 + 2y^2 - 3| < \delta (3\delta + 6) \leq \delta (3 + 6) = 9\delta = \varepsilon
\end{equation*}

Therefore for any \(\varepsilon > 0\),
\(\exists \delta = \min(1, \frac{\varepsilon}{1}) > 0\) such
that for

\begin{equation*}
0 < |x-1| < \delta \text{ and } 0 < |y-1| < \delta \implies |x^2 + 2y^2 - 3| < \varepsilon
\end{equation*}
\end{proof}

\section{Path taken for the limit}
\label{develop--math2270:ROOT:page--limits.adoc---path-taken-for-the-limit}

Sometimes, before we can prove the value of a limit, we need to
determine whether the limit exists in addition to finding its value
(possible value). In order to do this, we examine the approach taken to
the limit.

Similar to the directed limits for single variable functions, the limit
of a multivariable function exists if the limit of all approaches exists
and are equal. However, there exists an infinite number of possible
approaches thereby making it tedious to prove the limit this way.
Despite this, considering multiple approaches is not useless as we can
use the information gained to disprove the existence of a limit or find
its possible value. In order to do we use the following theorem.

\begin{lemma}[{}]
If \(\lim_{t\rightarrow a} g(t) = x_0\) and
\(\lim_{t\rightarrow a} h(t) = y_0\) and
\(\lim_{(x, y) \rightarrow (x_0, y_0)} f(x, y) = L\) then

\begin{equation*}
\lim_{t\rightarrow a} f(g(t), h(t)) = L
\end{equation*}

\begin{proof}[{}]
Consider \(\varepsilon > 0\). Therefore,
\(\exists \delta_1 > 0\) such that

\begin{equation*}
0<| x - x_0|< \delta_1 \text{ and } 0 < |y-y_0| < \delta_1 \implies |f(x, y) - L| < \varepsilon
\end{equation*}

Additionally, since \(\delta_1 > 0\),
\(\exists \delta_2, \delta_3 > 0\) such that

\begin{equation*}
(0 < |t - a| < \delta_2 \implies |g(t) - x_0| < \delta_1)
        \text{ and }
        (0 < |t - a| < \delta_3 \implies |h(t) - y_0| < \delta_1)
\end{equation*}

Now, we can choose \(\delta = \min(\delta_2, \delta_3)\) such
that

\begin{equation*}
\begin{aligned}
        0 < |t- a| < \delta
        &\implies |g(t) - x_0| < \delta_1 \text{ and } |h(t) - y_0 | < \delta_1\\
        &\implies |f(g(t), h(t)) - L| < \varepsilon
    \end{aligned}
\end{equation*}

Therefore

\begin{equation*}
\lim_{t\rightarrow a} f(g(t), h(t)) = L
\end{equation*}
\end{proof}
\end{lemma}

\begin{admonition-note}[{}]
The above statement only holds if \((g(t),h(t))\) is not
equal to \((x_0, y_0)\) within a certain neighbourhood.
A better requirement is that \(f\) is continuous at \((x_0, y_0)\).
\end{admonition-note}

Using this information, we can easily compare two paths to a certain
limit and prove that it does not exist (via contradiction) or find a
possible value.

\subsection{Example 1}
\label{develop--math2270:ROOT:page--limits.adoc---example-1}

Given that \(f(x, y) = \frac{x^2y}{x^3+y^3}\), find
\(\lim_{(x, y)\rightarrow(0, 0)} f(x,y)\) if it exists.

\begin{proof}[{}]
Suppose the limit exists. Then via the above lemma,

\begin{equation*}
\lim_{(x, y)\rightarrow(0, 0)} f(x, y) = \lim_{t\rightarrow 0 } f(t, 0) = \lim_{t \rightarrow 0} \frac{0}{t^3} = 0
\end{equation*}

Also

\begin{equation*}
\lim_{(x, y)\rightarrow(0, 0)} f(x, y) = \lim_{t\rightarrow 0 } f(t,t) = \lim_{t \rightarrow 0} \frac{t^3}{2t^3} = \frac{1}{2}
\end{equation*}

This is a contradiction, hence the limit does not exist.
\end{proof}

\subsection{Example 2}
\label{develop--math2270:ROOT:page--limits.adoc---example-2}

Given that \(f(x, y) = \frac{3x^2y}{x^2+y^2}\), find
\(\lim_{(x, y)\rightarrow(0, 0)} f(x,y)\) if it exists.

Investigation\newline
Let \(a, b \in \mathbb{R}\). Suppose
\(\lim_{(x, y)\rightarrow(0, 0)} f(x,y)\) exists. Then

\begin{equation*}
\begin{aligned}
    \lim_{(x, y)\rightarrow(0, 0)} f(x,y)
    &= \lim_{t \rightarrow 0} f(at, bt)\\
    &= \lim_{t \rightarrow 0} \frac{3a^2b t^3}{(a^2 + b^2) t^2}\\
    &= \lim_{t \rightarrow 0} \frac{3a^2b}{a^2 + b^2}t\\
    &= 0\end{aligned}
\end{equation*}

This is convincing evidence that the limit is equal to \(0\).
But we need to formally prove it.

\begin{proof}[{}]
required to show that for \(\varepsilon>0\),
\(\exists \delta > 0\) such that

\begin{equation*}
0 < |x| < \delta \text{ and } 0 < |y| < \delta \implies |\frac{3x^2y}{x^2+y^2}| < \varepsilon
\end{equation*}

Suppose we already have such \(\delta\)

\begin{equation*}
\begin{aligned}
    |\frac{3x^2y}{x^2+y^2}|
    &= \frac{3|x|^2|y|}{|x|^2 + |y|^2}\\
    &\leq \frac{3|x|^2|y|}{2|x||y|} \text{by AM-GM}\\
    &= \frac{3}{2}|x|\\
    &< \frac{3}{2} \delta = \varepsilon\end{aligned}
\end{equation*}

Therefore for any \(\varepsilon > 0\),
\(\exists \delta = \frac{2}{3}\varepsilon > 0\) such that for

\begin{equation*}
0 < |x| < \delta \text{ and } 0 < |y| < \delta \implies |\frac{3x^2y}{x^2+y^2}| < \varepsilon
\end{equation*}
\end{proof}

\section{Using polar coordinates to determine a limit}
\label{develop--math2270:ROOT:page--limits.adoc---using-polar-coordinates-to-determine-a-limit}

Based on the nature of our function \(f\), it is sometimes
easier to use polar coordinates to evaluate or prove a limit. This is
because

\begin{equation*}
\lim_{(x, y)\rightarrow (x_0, y_0)} f(x, y) = L \iff \lim_{(r, \theta) \rightarrow (r_0, \theta_0)} f(r\cos\theta, r\sin\theta) = L
\end{equation*}

where \(x_0 = r\cos\theta_0\) and
\(y_0 = r\sin\theta_0\). Note for the case of
\(x_0 = y_0 = 0\), \(\theta_0\) can take any value,
an is usually left arbitrary.

\subsection{Proof of this property}
\label{develop--math2270:ROOT:page--limits.adoc---proof-of-this-property}

I am not going to prove this for sake of my current sanity. But I
believe that the general form of this is as follows: Given metric spaces
\(A\) and \(B\), the limit points
\(a_0\in A\) and \(y_0 \in B\) and the bijection
\(T: A\rightarrow B\) such that

\begin{itemize}
\item \(T(a_0) = b_0\)
\item \(T^{-1}(b_0) = a_0\)
\item \(T\) is continuous at \(a_0\)
\item \(T^{-1}\) is continuous at \(b_0\)
\end{itemize}

Then

\begin{equation*}
\lim_{a\rightarrow a_0} f(a) = L \iff \lim_{b\rightarrow b_0} f(T^{-1}(b)) = L
\end{equation*}

\begin{proof}[{Forward direction}]
Since the \(\lim_{a\rightarrow a_0} f(a) = L\),
consider \(\varepsilon > 0\), then
\(\exists \delta_1 > 0\) such that

\begin{equation*}
0 < |a - a_0| < \delta_1 \implies |f(a) - L| < \varepsilon
\end{equation*}

Also, since \(T^{-1}\) is continuous at \(b_0\),
\(\exists \delta_2 > 0\) such that

\begin{equation*}
0 < |b - b_0| < \delta_2 \implies |T^{-1}(b) - a_0| = |T^{-1}(b) - T^{-1}(b_0)| < \delta_1
\end{equation*}

Also, since \(T\) is a bijection:

\begin{equation*}
0 < |b-b_0| \implies 0 < |T^{-1}(b) - T^{-1}(b_0)|
\end{equation*}

Finally, let \(\delta = \delta_2 > 0\). Then

\begin{equation*}
\begin{aligned}
        0 < |b - b_0| < \delta
        &\implies 0 < |T^{-1}(b) - T^{-1}(b_0)| < \delta_1\\
        &\implies 0 < |T^{-1}(b) - a_0| < \delta_1\\
        &\implies |f(T^{-1}(b)) - L| < \varepsilon
    \end{aligned}
\end{equation*}
\end{proof}

\begin{admonition-note}[{}]
The reverse direction is the exact same steps since the
conditions are symmetric. In fact, since the conditions are symmetric, there is no need to prove
the reverse direction.
\end{admonition-note}

\begin{admonition-remark}[{}]
I ended up doing it :P.\newline
Also, I don’t know if the conditions I placed are either necessary or
sufficient, but the seem to get it done.
\end{admonition-remark}

\subsection{Sufficient \(T\) for this property}
\label{develop--math2270:ROOT:page--limits.adoc---sufficient-latex-backslasht-latex-backslash-for-this-property}

\begin{equation*}
T: \mathbb{R}^2 \rightarrow \mathbb{R}^+ \times [0, 2\pi) \cup \{(0, 0)\}
\end{equation*}

\begin{equation*}
\arctan: \mathbb{R} \rightarrow [0, 2\pi)
\end{equation*}

\begin{equation*}
\begin{aligned}
    sign(x) &= \begin{cases}
        0 \quad &\text{if}\, x = 0\\
        1 \quad &\text{if}\, x > 0\\
        -1 \quad &\text{if}\, x < 0
    \end{cases}\\
    T(x, y) &= \begin{cases}
        (0, 0) \quad &\text{if}\, x = y= 0\\
        (|y|, \frac{\pi}{2}\text{sign}(y)) \quad &\text{if}\, x = 0\\
        (\sqrt{x^2 + y^2}, \arctan\frac{y}{x}) \quad &\text{otherwise}
    \end{cases}\end{aligned}
\end{equation*}

\section{Iterated Limits}
\label{develop--math2270:ROOT:page--limits.adoc---iterated-limits}

Yet another way to disprove the existence of limits is through the use
of iterated limits. We will only consider the case of two variables, but
it is generalizable.

\begin{proposition}[{}]
If

\begin{equation*}
\lim_{(x, y)\rightarrow(x_0, y_0)} f(x, y) = L
\end{equation*}

then

\begin{equation*}
\lim_{x\rightarrow x_0}\left[\lim_{y\rightarrow y_0} f(x, y)\right]
    = L =
    \lim_{y\rightarrow y_0}\left[\lim_{x\rightarrow x_0} f(x, y)\right]
\end{equation*}
\end{proposition}

\begin{admonition-tip}[{}]
The contrapositive of this statement is what is really useful when
disproving the existence of limits.
\end{admonition-tip}

\subsection{Worked example}
\label{develop--math2270:ROOT:page--limits.adoc---worked-example-2}

Prove that \(\lim_{(x, y)\rightarrow (0, 0)} \frac{x-y}{x+y}\)
does not exist.

\begin{proof}[{}]
Note that

\begin{equation*}
\lim_{x\rightarrow 0}\left[\lim_{y\rightarrow 0} \frac{x-y}{x+y}\right]
        =
        \lim_{x\rightarrow 0} \frac{x}{x}
        = 1
\end{equation*}

however,

\begin{equation*}
\lim_{y\rightarrow 0}\left[\lim_{x\rightarrow 0} \frac{x-y}{x+y}\right]
        =
        \lim_{x\rightarrow 0} \frac{-y}{y}
        = -1
\end{equation*}

Therefore, since

\begin{equation*}
\lim_{x\rightarrow 0}\left[\lim_{y\rightarrow 0} \frac{x-y}{x+y}\right]
        \neq
        \lim_{y\rightarrow 0}\left[\lim_{x\rightarrow 0} \frac{x-y}{x+y}\right]
\end{equation*}

The \(\lim_{(x, y)\rightarrow(x_0, y_0)} \frac{x-y}{x+y}\)
does not exist.
\end{proof}
\end{document}
