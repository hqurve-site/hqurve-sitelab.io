\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Axiom of Specification}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
\begin{custom-quotation}[{}][{}]
To every set \(A\) and to every condition \(S(x)\)
there corresponds a set \(B\) whose elements are
exactly those elements \(x\) of \(A\) for which
\(S(x)\) holds.
\end{custom-quotation}

In an attempt to not get side tracked, as we previously did,
we need to separate the idea of definition and an axiom.
That is, although the above axiom of specification may seem like
a definition, we need to instead understand it as an axiom.

Firstly, lets attempt to formalize the idea of a collection
of sets. We need several items

\begin{description}
\item[Collection of sets] We need some collection \(\mathcal{C}\)
    which contains all of our 'sets'. The idea of this is itself
    a bit recursive (or rather circular) but (I believe) it is
    necessary to be able to separate the idea of an axiom from definition.
\item[Universal set] We require things which belong to our sets.
    In standard fashion, we would call this \(U\). Note
    that \(U\) need not be distinct from \(\mathcal{C}\).
\item[Belonging] This connects \(U\) and \(\mathcal{C}\).
    In particular, it is a predicate \(\in\) which takes two
    arguments, one from \(\mathcal{C}\) and the other from \(U\).
    By looking at it as a predicate, we might immediately write it
    as \(\in(A, x)\) where \(A\) is in \(\mathcal{C}\) (ie a set)
    and \(x\) is in \(U\) (ie an element). Of course, however,
    we would write it in the standard format \(x \in A\).
\item[Equality (of sets)] Perhaps this is not necessarily required
    but rather encoded in the idea of \(\mathcal{C}\) itself.
    However, we would put it here. We need to be able to determine
    if two sets are equal. Additionally, by formalizing this,
    we see a hole in the \myautoref[{previous example}]{develop--math3274:reading-notes:page--axiom-of-extension.adoc---adjusted-example}
    in that we were not talking about persons but
    rather equivalence classes of persons (by DNA). Regardless,
    it works.
\end{description}

Now, consider \(\mathcal{C}\) and \(U\) and ask ourselves,
``is \(\mathcal{C}\) necessarily the powerset of \(U\)''?
The answer to this is immediately no. On its own, the tuple
\((\mathcal{C}, U, \in, =)\) need not require that
\(\mathcal{C} = 2^U\) (or rather \(\mathcal{C} \cong 2^U\)).
But, does the axiom of specification require it?

In the case when \(U\) is finite (or rather countable) and
there exists a set in \(\mathcal{C}\) which contains all the elements
of \(U\) (note that this may be a different structure from \(U\)
itself), the answer is yes. We can construct a sentence (possibly
countably infinitely long) specifying whether or not each element in \(U\)
belongs to our desired set. The axiom of specification would in
turn require that there exists \(S\) of \(\mathcal{C}\)
which contains precisely these elements.

\begin{admonition-remark}[{}]
Again, a lot of this may seem a bit recursive. Aren't
\(\mathcal{C}\) and \(U\) sets themselves? If so, is there some
minimal example? An atom? Perhaps we should look to the physical
world for a hint. Once we thought that the atom was the smallest,
indivisible item of the universe, however today we know that
it goes deeper, there are subatomic `particles', quarks,
and who knows what else. Why search for the bottom as long as we
know the properties of what we know? The problem is that we do not know.
\end{admonition-remark}

\section{An example}
\label{develop--math3274:reading-notes:page--axiom-of-specification.adoc---an-example}

One thing which seems to satisfy our, so far, two axioms
is the idea of a sentence itself. Let

\begin{itemize}
\item \(U\) be a set of our choice
    (for example, \(\mathbb{N}\) or \(\mathbb{R}^2\) or \(\{a,b,c\}\))
\item \(\mathcal{C}\) be the collection of equivalence classes
    of predicates which act on elements
    of \(U\). Where two predicates are in the same equivalence class
    if they produce the same truthiness for each element in \(U\).
\item \(\in(A, x) = A(x)\). That is belonging is defined by whether
    a representative of our equivalence class evaluates true on an element.
    Note that as a result of how we defined our equivalence classes, \(\in\)
    is well defined.
\item \(=(A,B)\) is just whether two equivalence classes are the same.
\end{itemize}

\begin{admonition-note}[{}]
I believe that we could have gotten away by just letting
\(\mathcal{C}\) be the set of sentences (rather than equivalence classes)
and defined equality as belonging to the same equivalence class. However,
doing it like this makes things a bit clearer.
\end{admonition-note}

Now, lets try see if this system satisfies the axiom of extension.
Firstly, if \(A\) and \(B\) are equal, \(\in\) produces
the same results for all elements in \(U\) (as a result of it being
well defined). Conversely, if \(A\) and \(B\) evaluate \(\in\)
for each element in \(U\), then by definition of equality, we must
have that \(A=B\). (Not very exciting. We have to do a better example).

Now, for our example of specification. Let \(A\) be a set and \(P\)
be a sentence. Then we want to show that there is \(B\) of \(\mathcal{C}\)
whose
elements are precisely those in \(A\) for which \(P\) evaluates true.
Consider a representative of \(A\), \(a\) and we construct the sentence,
\(b\),
``\(a\) and \(P\)'' and consider the equivalence class of \(b\), \(B\).
Then firstly, \(B\) is in \(\mathcal{C}\). Next, for any element in \(B\),
it belongs to \(A\) based on how the `and' operator works. And finally, any
element in \(B\) must also satisfy \(P\). Therefore, the axiom of
specification holds.

\section{A better example}
\label{develop--math3274:reading-notes:page--axiom-of-specification.adoc---a-better-example}

Previously, we let \(\mathcal{C}\) be a set of equivalence classes of sentences.
However, this made things a bit complex as these things were, in their own right,
also sets. So instead, we let \(\mathcal{C}\) be the set of sentences themselves
and equality be defined such that \(A\) and \(B\) are equal iff they
act the same on all elements of \(U\)
\myautoref[{[Callback to the third definition of equal]}]{develop--math3274:reading-notes:page--axiom-of-extension.adoc---definition-of-equal}.

Now, if \(A=B\), then by definition they act the same on all elements of \(U\)
and hence \(\in(A, \_ )\) and \(\in(B, \_ )\) act the same for all elements of \(U\) and hence \(A\)
and \(B\) contain the same elements. Conversely, if \(A\) and \(B\)
contain the same elements, that means that \(\in\) acts the same on all elements
and hence \(A\) and \(B\) act the same on all elements. Therefore, \(A=B\).
Therefore, the axiom of extension is satisfied.

Next, consider a set \(A\) and sentence \(P\). Then we could define \(B = A \land P\)
(since \(A\) is itself a sentence). Now, clearly, any element for which \(\in(B, \_ )\)
is true, \(\in(A, \_ )\) must be true and \(P( \_ )\) must be true. On the other hand,
if \(\in(A, \_ )\) is true and \(P( \_ )\) is true, \(\in(B, \_ )\) must also be true.
Therefore, \(B\) is a set whose elements are precisely those of \(A\)
for which \(P\) is true. Therefore, the axiom of specification is satisfied.

\subsection{Applying Russell's Argument}
\label{develop--math3274:reading-notes:page--axiom-of-specification.adoc---applying-russells-argument}

Although we haven't yet shown that this system of sets is in fact valid, it is interesting to see what
Russell's argument yields.

To do this, we would focus on specific examples for \(U\). If \(U = \mathbb{R}\) or really
anything which is \(\in\) doesnt really apply, we get that the sentence (set) \(x \notin x\)
returns \(U\) itself. It does exist. Why? The answer is simple, \(U\)
never claimed to contain everything, as evident by the fact that its elements are not actually considered sets.

Now, what if \(U\) is the set of sentences? This is the same as \(\mathcal{C}\). What happens?
Let \(S(x) = x \notin x \equiv !x(x)\); thats a sentence, so it does `exist'. Lets apply
\(S\). We get \(S(S) = !S(S)\); immediate contradiction. What is the problem? I don't know,
however, it does show that our sentences example has some restriction on the set \(U\). Additionally,
does this mean that not all things are proper sentences?

Perhaps the issue lies not with \(U\) or \(\mathcal{C}\) but rather the nature of \(S\) itself.
We cannot simply exclude \(S\) and other like sentences, but perhaps we could make
a pretty valid restriction. Let \(\mathcal{C}\) be the set of non-self-referential sentences.
By doing this Russell's sentence is automatically discarded. (NOT ALL MAGIC WORDS ARE VALID!!!)

\begin{example}[{Russell}]
\begin{figure}[H]\centering
\includegraphics[draft,width=8cm,height=5cm]{placeholder.png}
\caption{ (omitted external image: https://i.kym-cdn.com/photos/images/newsfeed/002/311/327/a9d.jpg)}
\end{figure}

\begin{admonition-remark}[{}]
This joke would have been even more funny if I was one year younger and could have done this course last year.
\end{admonition-remark}
\end{example}

\begin{admonition-remark}[{}]
There seems to be a strange parallel between Russell's paradox and the machine made to prove that halting problem is not computable.
\end{admonition-remark}

\subsection{Comments}
\label{develop--math3274:reading-notes:page--axiom-of-specification.adoc---comments}

Notice that we can draw a parallel to the proof of \((\mathbb{F}, \mathbb{F})\) is a vector
space. Since \(\mathcal{C}\) already contains sentences, the proof of axioms seem to
be a natural consequence.

Additionally this example clearly shows two important points

\begin{itemize}
\item Sets need not be structures which actually contain elements. In this case,
    they are predicates which are just sentences. For example, say \(U = \mathbb{Z}\)
    and consider \(A(x) = x \text{ is even}\). Then \(A\) does not actually
    ``contain'' \(x\).
\item Equality does not necessarily mean that two things are the same. For example,
    consider the sentence \(B(x) = x\text{ is not odd}\), then \(A=B\). However,
    they are clearly not the same sentence but only in the context of \(\mathbb{Z}\)
    do they evaluate the same for each element.
\end{itemize}

\section{A constructed non-example}
\label{develop--math3274:reading-notes:page--axiom-of-specification.adoc---a-constructed-non-example}

Consider the tuple \((\mathcal{C}, U, \in, =)\) which satisfies
the axiom of specification. Then, consider
some set \(A\) and sentence \(P(x) = x \notin A\). Then by the axiom
of specification, there exists \(B = \{x\in A: P(x)\}\). We can now construct
the collection \(\mathcal{D}\) which contains everything in \(\mathcal{C}\)
unless it is equal to \(B\). Then the tuple \((\mathcal{D}, U, \in, =)\)
can does not satisfy the axiom of specification since \(B\) does not belong
to \(D\).

\begin{admonition-note}[{}]
Although this might seem a bit boring, it is important to realize that
\(\mathcal{C}\) is just a collection.
\end{admonition-note}

\section{Sets with duplicates}
\label{develop--math3274:reading-notes:page--axiom-of-specification.adoc---sets-with-duplicates}

Previously, we asked whether or not

\begin{equation*}
\{1,2,3\} = \{1,2,2,3\}
\end{equation*}
\end{document}
