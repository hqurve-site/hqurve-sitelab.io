\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Existence and Uniqueness}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
\section{Exercise 2.1}
\label{develop--math6120:exercises:page--chapter2.adoc---exercise-2-1}

\subsection{Question 1}
\label{develop--math6120:exercises:page--chapter2.adoc---question-1}

Prove that \(f(t,x) = \sqrt{x}\) on \(R = \{(t,x):\ |t| \leq 2, |x| \leq 2\}\)
does not admit a partial derivative with respect to \(x\) and \((0,0)\).

Note that \(f(t,x)\) is not defined for \(x < 0\), so we only need to consider \(x \geq 0\).
Then, by definition

\begin{equation*}
f_x(0,0)
= \lim_{x \to 0} \frac{f(0,x) - f(0,0)}{x - 0}
= \lim_{x \to 0}\frac{\sqrt{x}}{x}
= \lim_{x \to 0}\frac{1}{\sqrt{x}} = \infty
\end{equation*}

\subsection{Question 2}
\label{develop--math6120:exercises:page--chapter2.adoc---question-2}

Show that

\begin{equation*}
f(t,x) = \frac{e^{-x}}{1+t^2}
\end{equation*}

defined on \(0 < x < p\), \(0 < t < N\) (where \(N \in \mathbb{Z}^+\)) satisfies
the Lipschitz condition with Lipschitz constant \(K = 1\).

\begin{equation*}
\left|f(t,x_1) - f(t,x_2)\right|
= \frac{1}{1+t^2}\left|e^{-x_1} - e^{-x_2}\right|
< 1(1)
\end{equation*}

since \(e^{-x} < 1\) for \(0 < x < p\) and \(t^2 > 0\) for \(0 < t < N\).

\subsection{Question 3}
\label{develop--math6120:exercises:page--chapter2.adoc---question-3}

Show that the following satisfy the Lipschitz condition in the indicated rectangle
and find the corresponding Lipschitz constants

\subsubsection{Part i}
\label{develop--math6120:exercises:page--chapter2.adoc---part-i}

\(f(t, x) = e^t\sin x\) where \(|x| \leq 2\pi\) and \(|t| < 1\).

Since \(f_x\) is continuous on the closed rectangle, \(f\) must be Lipschitz.
Notice that

\begin{equation*}
|f_x| = |e^t||\cos x| \leq e = K
\end{equation*}

\subsubsection{Part ii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-ii}

\(f(t,x) = (x+x^2)\frac{\cos t}{t^2}\) where \(|x| \leq 1\) and \(|t-1| \leq \frac12\).

\begin{equation*}
\begin{aligned}
|f_x| = \left|\frac{\cos t}t^2 (2x+1)\right| \leq \frac{1}{\frac1/2^2}(3) = 12 = K
\end{aligned}
\end{equation*}

\subsubsection{Part iii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-iii}

\(f(t,x) = \sin(xt)\) where \(|x| \leq 1\) and \(|t| \leq 1\)

\begin{equation*}
|f_x| = |t\cos(xt)| \leq (1)(1) = K
\end{equation*}

\subsection{Question 4}
\label{develop--math6120:exercises:page--chapter2.adoc---question-4}

Show that the following functions do not satisfy the Lipschitz condition in the region indicated.

\subsubsection{Part i}
\label{develop--math6120:exercises:page--chapter2.adoc---part-i-2}

\(f(t,x) = \exp\left(\frac{1}{t^2}\right)x\), \(f(0,x) = 0\) where \(|x| \leq 1\) and \(|t| \leq 1\)

\begin{equation*}
|f(t,0) - f(t,1)| = \frac{1}{t^2}|0-1|
\end{equation*}

This tends to infinity as \(t \to 0\).

\subsubsection{Part ii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-ii-2}

\(f(t,x) = \frac{\sin x}{t}\), \(f(0,x) = 0\) where \(|x| < \infty\) and \(|t| \leq 1\)

\begin{equation*}
\frac{\left|f\left(t,\frac{\pi}{2}\right) - f\left(t,0\right)\right|}{\left|\frac{\pi}2 - 0\right|} = \frac{2}{\pi}\frac{1}{|t|}
\end{equation*}

This tends to infinity as \(t \to 0\).

\subsubsection{Part iii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-iii-2}

\(f(t,x) = \frac{e^t}{x^2}\), \(f(t,0) = 0\) where \(|x| \leq \frac12\) and \(|t| \leq 2\)

\begin{equation*}
|f(0, 0) - f(0,x)| = \left| 0 - \frac{1}{x^2}\right| = \frac{1}{x^3}|0-x|
\end{equation*}

This tends to infinity as \(x \to 0\).

\section{Exercise 2.2}
\label{develop--math6120:exercises:page--chapter2.adoc---exercise-2-2}

\subsection{Question 1}
\label{develop--math6120:exercises:page--chapter2.adoc---question-1-2}

Let \(g\) be a continuous real-valued non-negative function on \(t \in [t_0, b]\).
If for each \(k > 0\) and \(A \geq 0\),

\begin{equation*}
\forall t \in [a,b]: g(t) \leq A + k\int_{t_0}^t g(s) \ ds
\end{equation*}

Show that \(g(t) \leq Ae^{k(t-t_0)}\).

Solution:
By applying Grandwall's inequality, we obtain that

\begin{equation*}
g(t) \leq A \exp\left[\int_{t_0}^t k \ ds \right] = A e^{k(t-t_0)}
\end{equation*}

\subsection{Question 2}
\label{develop--math6120:exercises:page--chapter2.adoc---question-2-2}

\begin{admonition-warning}[{}]
The desired inequality is wrong, from Wikipedia, it is supposed to be

\begin{equation*}
f(t) \leq h(t) + \int_{t_0}^t g(s)h(s)\exp\left(\int_{s}^t g(u) \ du\right) \ ds
\end{equation*}

This inequality is not hard to prove given the hint.

See below (after tip) for counterexample.
\end{admonition-warning}

Let \(I = [a,b] \subseteq \mathbb{R}\)  and let \(f,g,h\) be continuous non-negative functions
on \(I\). Let \(t_0\)
Show that

\begin{equation*}
\forall t \geq t_0: f(t) \leq h(t) + \int_{t_0}^t g(s)f(s) \ ds
\end{equation*}

implies the inequality

\begin{equation*}
f(t) \leq h(t) + \int_{t_0}^t g(s)h(s)\exp\left(\int_{t_0}^s g(u) \ du\right) \ ds
\end{equation*}

\begin{admonition-tip}[{}]
The following hint was given. Let \(z(t) = \int_{t_0}^t g(s) f(s) \ ds\). Then

\begin{equation*}
z'(t) = g(t)f(t) \leq g(t)[h(t) + z(t)]
\end{equation*}

Hence

\begin{equation*}
z'(t) - z(t)g(t) \leq g(t)h(t)
\end{equation*}

Multiply by \(\exp\left(\int_{t_0}^t -g(s) \ ds\right)\) and integrate over \([t_0, t]\).

\begin{admonition-note}[{}]
There is a typo in the notes. It simply states that \(z'(t) \leq g(t)h(t)\).
Also, it stated to multiply be \(\exp\left(\int_{t_0}^t g(s) \ ds\right)\) instead of the above.
\end{admonition-note}
\end{admonition-tip}

\subsection{Counterexample for desired inequality}
\label{develop--math6120:exercises:page--chapter2.adoc---counterexample-for-desired-inequality}

Take \(f(t) = \frac{1}{100}t^2\), \(g(t)=1\), \(h(t)=e^{-t}\) and
\(t_0=0\).
Then,

\begin{equation*}
A(t) = h(t) + \int_0^t g(s)f(s) \ ds = e^{-t} + \int_0^t \frac{1}{100}s^2 \ ds
= e^{-t} + \frac{1}{300}t^3
\end{equation*}

and

\begin{equation*}
B(t)
= h(t) + \int_0^t g(s)h(s)\exp\left(\int_0^s g(u) \ du\right) \ ds
= e^{-t} + \int_0^t e^{-s}\exp(s) \ ds = e^{-t} + t
\end{equation*}

We show that \(f(t) \leq A(t)\) for all \(t \geq 0\).
The inequality trivially holds for \(t=0\).
Also, the inequality holds trivially if \(t\geq 3\).
We need only show that it holds for \(t\in (0,3)\).
We consider the series expansion of \((A(t)-f(t))e^t\)
and show it is positive for \(t \in (0,3)\).

\begin{equation*}
\begin{aligned}
(A(t) - f(t))e^t
&= 1 + \frac{1}{300}e^t(t^3-3t^2)
\\&= 1 + \frac{1}{300}(t^3-3t^2)\sum_{n=0}^\infty \frac{t^n}{n!}
\\&= 1 + \frac{1}{300}\left(\sum_{n=0}^\infty \frac{t^{n+3}}{n!} - 3\sum_{n=0}^\infty \frac{t^{n+2}}{n!}\right)
\\&= 1 + \frac{1}{300}\left(\sum_{n=3}^\infty \frac{t^{n}}{(n-3)!} - 3\sum_{n=2}^\infty \frac{t^{n}}{(n-2)!}\right)
\\&= 1 + \frac{1}{300}\left(-3t^2 + \sum_{n=3}^\infty \left(\frac{1}{(n-3)!} - \frac{3}{(n-2)!}\right)t^n\right)
\\&= 1 + \frac{1}{300}\left(-3t^2 + \sum_{n=3}^\infty \frac{(n-2) -3}{(n-2)!}t^n\right)
\\&= 1 + \frac{1}{300}\left(-3t^2 + \sum_{n=3}^\infty \frac{n-5}{(n-2)!}t^n\right)
\\&= 1 + \frac{1}{300}\left(-3t^2 - \frac{-2}{1!}t^3 + \frac{-1}{2!}t^4 + \sum_{n=5}^\infty \frac{n-5}{(n-2)!}t^n\right)
\\&= 1 + \frac{1}{300}\left(-3t^2 - 2t^3-\frac{1}{2}t^4 + \sum_{n=5}^\infty \frac{n-5}{(n-2)!}t^n\right)
\\&\geq 1 + \frac{1}{300}\left(-3t^2 - 2t^3-\frac{1}{2}t^4\right)
\quad\text{since the terms in the summation are all positive}
\\&\geq 1 + \frac{1}{300}\left(-3(9) - 2(27)-\frac{1}{2}(81)\right)
\\&\geq 1 - \frac{121.5}{300} > 0
\end{aligned}
\end{equation*}

Therefore \(A(t) - f(t) > 0\) and \(A(t) > f(t)\) for \(t\in (0,3)\).
Therefore we have shown that

\begin{equation*}
\forall t \geq 0: f(t) \leq A(t) = h(t) + \int_0^t g(s)f(s) \ ds
\end{equation*}

However, note that there must exist a \(t > 0\) such that the below inequality does not hold

\begin{equation*}
\frac{1}{100}t^2 = f(t) \leq h(t) + \int_0^t g(s)h(s)\exp\left(\int_0^s g(u) \ du\right) \ ds
= e^{-t}+t
\end{equation*}

since \(e^{-t} \to 0\) as \(t\to \infty\).
Therefore, we have found a counterexample.

\subsection{Question 3}
\label{develop--math6120:exercises:page--chapter2.adoc---question-3-2}

According to the Uniqueness theorem, determine whether the following IVPs have a unique solution.

\subsubsection{Part i}
\label{develop--math6120:exercises:page--chapter2.adoc---part-i-3}

\(x' = tx^{\frac12}\), \(x(0) = 0\)

Consider any rectangle containing interior point \((t,x) = (0,0)\).
Then, notice that \(tx^{\frac12}\) is not Lipschitz in this rectangle since

\begin{equation*}
|tx_1^{\frac12} - t(0)^{\frac12}| = |t|\sqrt{x_1} = \frac{|t|}{\sqrt{x}_1}(x_1 - 0)
\end{equation*}

and \(\frac{|t|}{\sqrt{x_1}}\) tends to infinity as \(x_1 \to \infty\).

\subsubsection{Part ii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-ii-3}

\(x' = tx^{\frac12}\), \(x(2) = 1\)

Consider the rectangle \(R = \{(t,x) \ | \ |t| \leq a, \ |x-1| \leq \frac12 \}\).
Then, \(tx^{\frac1{2}}\) is has continuous partial derivative (wrt \(x\))
on \(R\). Therefore this function is Lipschitz and the IVP has a unique solution.

\subsection{Question 4}
\label{develop--math6120:exercises:page--chapter2.adoc---question-4-2}

Determine a region of the \(tx\) plane for which the following
differential equation would have a unique solution whose
graph passes through at point \((t_0, x_0)\) in the region.
\(x' = \sqrt{tx}\).

If \(x_0 =0\), then the following are two solutions for the IVP

\begin{itemize}
\item \(x(t) = 0\)
\item \(x(t) = \exp\left(\frac{2}{3}t^{\frac{3}{2}} - \frac{2}{3}t_0^{\frac{3}{2}}\right)\)
\end{itemize}

Otherwise, suppose that \(x_0 > 0\).
Then, there exists a rectangle \(R = \left\{(t,x) \ \middle| \ |t-t_0| \leq a, \ |x-x_0| < \frac12 x_0\right\}\)
surrounding \((t_0, x_0)\). Then, notice that \(\sqrt{tx}\) is Lipschitz on \(R\)
since the partial derivative wrt \(x\) is continuous on \(R\). Therefore, the following IVP has a unique solution.

\subsection{Question 5}
\label{develop--math6120:exercises:page--chapter2.adoc---question-5}

Determine whether the Uniqueness Theorem guarantees that the differential equation
\(x' = \sqrt{x^2-9}\) possesses a unique solution through the given point \((2,-3)\).

Suppose BWOC that \(f(t,x) = \sqrt{x^2-9}\) is Lipschitz on rectangle \(R = \{(t,x) \ |t-2| \leq a,\ |x+3| \leq b\}\).
Then, for each \(x > -3\)

\begin{equation*}
|f(t,x) - f(t,-3)| = \sqrt{x^2-9} = \sqrt{(x+3)(x-3)} = \sqrt{\frac{x-3}{x+3}} |x+3|
\end{equation*}

However, notice that \(\sqrt{\frac{x-3}{x+3}}\) tends to infinity as \(x \to -3\).
Therefore \(f(t,x)\) is not Lipschitz on any rectangle centered at \((2,-3)\) and hence
the Uniqueness theorem cannot guarantee the uniqueness of a solution.

\section{Exercise 2.3}
\label{develop--math6120:exercises:page--chapter2.adoc---exercise-2-3}

\subsection{Question 1}
\label{develop--math6120:exercises:page--chapter2.adoc---question-1-3}

Calculate the successive approximations for the IVP

\begin{equation*}
x' = g(t), \ x(0) = 0
\end{equation*}

Solution

\begin{equation*}
\phi_0(t) = 0
\end{equation*}

\begin{equation*}
\phi_1(t) = \int_0^t g(s) \ ds
\end{equation*}

\begin{equation*}
\phi_{n+1}(t) = \int_0^t f(s, \phi_n(s)) \ ds = \int_0^t g(s) \ ds
\end{equation*}

Therefore the sequence of approximations converges.

\subsection{Question 2}
\label{develop--math6120:exercises:page--chapter2.adoc---question-2-3}

Solve the IVP

\begin{equation*}
x' =x, \  x(0) = 1
\end{equation*}

by successive approximations.

\begin{equation*}
\phi_0(t) = 1
\end{equation*}

Claim that

\begin{equation*}
\phi_n(t) = \sum_{k=0}^n \frac{t^k}{k!}
\end{equation*}

True for \(\phi_0\) and

\begin{equation*}
\phi_{n+1}(t)
= 1 + \int_{0}^t \phi_n(s) \ ds
= 1 + \int_{0}^t \sum_{k=0}^n \frac{s^k}{k!} \ ds
= 1 + \sum_{k=0}^n \frac{t^{k+1}}{(k+1)!}
= \sum_{k=0}^{n+1} \frac{t^{k}}{k!}
\end{equation*}

as desired

\subsection{Question 3}
\label{develop--math6120:exercises:page--chapter2.adoc---question-3-3}

Compute the first three successive approximations for the solutions of the following equations

\subsubsection{Part i}
\label{develop--math6120:exercises:page--chapter2.adoc---part-i-4}

\begin{equation*}
x' = x^2, \ x(0) = 1
\end{equation*}

Then, \(\phi_0(t) = 1\)

\begin{equation*}
\phi_1(t) = 1 + \int_0^t 1^2 \ ds = 1+t
\end{equation*}

\begin{equation*}
\phi_2(t) = 1 + \int_0^t (1+s)^2 \ ds = 1 + s+ s^2 + \frac{s^3}{3}
\end{equation*}

\subsubsection{Part ii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-ii-4}

\begin{equation*}
x' = e^x, \ x(0) = 0
\end{equation*}

Then, \(\phi_0(t) = 0\)

\begin{equation*}
\phi_1(t) = \int_0^t e^{0} \ ds = t
\end{equation*}

\begin{equation*}
\phi_2(t) = \int_0^t e^{s} \ ds = e^t - 1
\end{equation*}

\subsubsection{Part iii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-iii-3}

\begin{equation*}
x' = \frac{x}{1+x^2}, \ x(0) = 1
\end{equation*}

Then, \(\phi_0(t) = 1\)

\begin{equation*}
\phi_1(t) = 1 + \int_0^t \frac{1}{2} \ ds = 1 + \frac12 t
\end{equation*}

\begin{equation*}
\phi_2(t)
= 1+ \int_0^t \frac{1+\frac12 s}{1 + \left(1+\frac12 s\right)^2} \ ds
= 1+ \left.\ln\left(1+\left(1+\frac{s}{2}\right)^2\right)\right|_0^t
= 1+ \ln\left(1+\left(1+\frac{t}{2}\right)^2\right) - \ln(2)
\end{equation*}

\subsection{Question 4}
\label{develop--math6120:exercises:page--chapter2.adoc---question-4-3}

Show that the error due to the truncation at the \(n\)'th approximation tends to zero as \(n\to\infty\).

Recall that the error is given by

\begin{equation*}
\frac{M}{K}\frac{(K\alpha)^{n+1}}{(n+1)!} e^{K\alpha}
\end{equation*}

\begin{admonition-remark}[{}]
At the time of writing, I am not sure how best to do this.
\end{admonition-remark}

\subsection{Question 5}
\label{develop--math6120:exercises:page--chapter2.adoc---question-5-2}

Determine the constant \(M\), \(K\) and \(\alpha\) for the following IVPs.
Is Picard's theorem applicable to the above three problems?
If so, find the least \(n\) such that the error left over does not exceed \(2\),
\(1\) and \(0.5\) respectively.

\begin{admonition-caution}[{}]
I found the least \(n\) using the bounds assured. This is not necessarily
the least \(n\).
\end{admonition-caution}

Recall that the error for \(\phi_n\) is given by

\begin{equation*}
\frac{M}{K} \frac{(K\alpha)^{n+1}}{(n+1)!}e^{K\alpha}
\end{equation*}

\subsubsection{Part i}
\label{develop--math6120:exercises:page--chapter2.adoc---part-i-5}

\(x' = x^2\), \(x(0)= 1\), \(R = \{(t,x) \ | \ |t| \leq 2, \ |x-1|\leq 2\}\).
Let \(f(t,x) =x^2\). Then, on \(R\)

\begin{equation*}
|f(t,x)| = x^2 \leq 3^2 = 9 \implies M=9
\end{equation*}

and

\begin{equation*}
|f(t,x_1) - f(t,x_2)| = |x_1+x_2||x_1 - x_2| \leq 6|x_1 - x_2| \implies K=6
\end{equation*}

This implies that \(f\) is Lipschitz.
Finally, \(\alpha = \min\left(2, \frac{2}{9}\right) = \frac{2}{9}\).

Therefore Picard's theorem is applicable and there exists a unique solution to the IVP on \(R\).

The error is given by \(\frac{3}{2}\frac{\left(\frac{4}{3}\right)^{n+1}}{(n+1)!}e^{\frac{4}{3}}\).
Then,

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(n\)} & {\(\varepsilon\)} \\
\hline[\thicktableline]
{0} & {7.6} \\
\hline
{1} & {5.1} \\
\hline
{2} & {2.2} \\
\hline
{3} & {0.75} \\
\hline
{4} & {0.2} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\subsubsection{Part ii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-ii-5}

\(x' = \sin(x)\), \(x\left(\frac{\pi}{2}\right) = 1\),
\(R = \left\{(t,x) \ \middle| \ \left|t-\frac{\pi}{2}\right| \leq \frac{\pi}{2}, \ |x-1|\leq 1\right\}\).
Let \(f(t,x) = \sin(x)\). Then, on \(R\)

\begin{equation*}
|f(t,x)| \leq 1 \implies M= 1
\end{equation*}

and \(f_x = \cos(x)\) is bounded which implies that \(f\) is Lipschitz
with \(K = \sup_{|x-1| \leq 1} \cos(x) = 1\)
Finally, \(\alpha = \min\left(\frac{\pi}{2}, \frac{1}{1}\right) = 1\).

Therefore Picard's theorem is applicable and there exists a unique solution to the IVP on \(R\).

The error is given by \(\frac{1}{(n+1)!}e^{1}\).
Then,

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(n\)} & {\(\varepsilon\)} \\
\hline[\thicktableline]
{0} & {2.6} \\
\hline
{1} & {1.4} \\
\hline
{2} & {0.45} \\
\hline
{3} & {0.11} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\subsubsection{Part iii}
\label{develop--math6120:exercises:page--chapter2.adoc---part-iii-4}

\(x' = e^x\), \(x(0) = 0\),
\(R = \left\{(t,x) \ \middle| \ |t| \leq 3,\ |x| \leq 4\right\}\).
Let \(f(t,x) = e^x\). Then, on \(R\)

\begin{equation*}
|f(t,x)| \leq e^4 \implies M= e^4
\end{equation*}

and \(f_x = e^x\) is bounded on \(R\) which implies that \(f\) is Lipschitz
with \(K = \sup_{|x| \leq 4} e^x = e^4\).
Finally, \(\alpha = \min\left(3, \frac{4}{e^4}\right) = \frac{4}{e^4}\).

Therefore Picard's theorem is applicable and there exists a unique solution to the IVP on \(R\).

The error is given by \(\frac{4^{n+1}}{(n+1)!}e^{4}\).
Then,

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(n\)} & {\(\varepsilon\)} \\
\hline[\thicktableline]
{\(\vdots\)} & {\(\vdots\)} \\
\hline
{10} & {5.7} \\
\hline
{11} & {1.9} \\
\hline
{12} & {0.59} \\
\hline
{13} & {0.17} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\subsection{Question 6}
\label{develop--math6120:exercises:page--chapter2.adoc---question-6}

Let \(I = [a,b] \subset \mathbb{R}\) be an interval and \(g,h: I \to \mathbb{R}\)
be continuous functions. Show that the following IVP has a unique solution

\begin{equation*}
x' + g(t)x = h(t), \ x(t_0) = x_0, \quad t,t_0 \in I
\end{equation*}

Let \(f(t,x) = h(t) - g(t)x\) and

\begin{equation*}
R = \{(t,x) \ | \ |t-t_0| \leq \min(b-t_0, t_0-a),\ |x-x_0| \leq c\}
\end{equation*}

for some \(c > 0 \).
Then, if

\begin{itemize}
\item \(f\) is Lipschitz continuous on \(R\)
\item There exists \(M = \max_{(t,x) \in R} |f(t,x)|\).
\end{itemize}

Then the IVP has a unique solution on \(I = |t-t_0| \leq \alpha\) where

\begin{equation*}
\alpha = \min\left(\min(b-t_0, t_0-a), \frac{c}{M}\right)
\end{equation*}

First, notice that \(f\) is Lipschitz continuous on \(R\) since

\begin{equation*}
|f(t,x_1) - f(t,x_2)| = |g(t)||x_1 - x_2| \leq 2c|g(t)|
\end{equation*}

is bounded since \(g(t)\) is continuous on \(I\).
Also, \(f(t,x)\) must also be bounded since it is continuous on compact set \(R\).
Therefore, the IVP has a unique solution.

\section{Lecture 4 (Continuation and dependence on initial conditions)}
\label{develop--math6120:exercises:page--chapter2.adoc---lecture-4-continuation-and-dependence-on-initial-conditions}

\subsection{Question 1}
\label{develop--math6120:exercises:page--chapter2.adoc---question-1-4}

Consider a linear equation \(x' = a(t)x\) with initial condition \(x(t_0) = x_0\),
where \(a(t)\) is a continuous function on interval \(I\) containing \(t_0\).
Solve the IVP and show that the solution \(\phi(t;t_0,x_0)\) is a continuous
function of \((t_0, x_0)\) for each fixed \(t \in I\).

Since \(a(t)\) is continuous, there exists a maximum \(M> 0\) of \(a(t)\) on \(I\)
and this is a first-order linear IVP with a unique solution.
Also, let \(I \subseteq (a,b)\).
Therefore

\begin{equation*}
\phi(t; t_0, x_0) = x_0\exp\left[\int_{t_0}^t a(s) \ ds\right].
\end{equation*}

Now consider arbitrary \(\varepsilon > 0\) and alternative initial condition \((t_0^*, x_0^*)\)
such that \(|t_0 - t_0^*| < \delta\) and \(|x_0 - x_0^*| < \delta\).
Also, WLOG, suppose that \(t_0 > t_0^*\)
Then

\begin{equation*}
\begin{aligned}
\forall t \in I:
&|\phi(t; t_0, x_0) - \phi(t; t_0^*, x_0^*)|
\\&= \left|
x_0\exp\left[\int_{t_0}^t a(s) \ ds\right]
-x_0^*\exp\left[\int_{t_0^*}^t a(s) \ ds\right]
\right|
\\&= \left|
x_0
-x_0^*\exp\left[\int_{t_0}^t a(s) \ ds - \int_{t_0}^{t_0^*} a(s) \ ds\right]
\right|
\\&= \left|
x_0
-x_0^*\exp\left[\int_{t_0^*}^{t_0} a(s) \ ds\right]
\right|
\exp\left[\int_{t_0}^t a(s) \ ds\right]
\\&\leq \left(|x_0 - x_0^*| + |x_0^*|\left|1- \exp\left[\int_{t_0^*}^{t_0} a(s) \ ds\right]\right|\right)
\exp\left[\int_{t_0}^t a(s) \ ds\right]
\\&\leq \left(|x_0 - x_0^*| + b\max(1- e^{-M|t_0-t_0^*|}, e^{M|t_0-t_0^*|}-1)\right)M(b-a)
\\&\leq \left(\delta + b\max(1- e^{-M\delta}, 1-e^{M\delta})\right)M(b-a)
\end{aligned}
\end{equation*}

Since the limit of the above as \(\delta \to 0\) is zero, there exists a \(\delta > 0\) such that the above is less than
\(\varepsilon\).
Therefore, we have shown the desired result.

\subsection{Question 2}
\label{develop--math6120:exercises:page--chapter2.adoc--continuation-and-dependence-question2}

Consider the IVPs

\begin{enumerate}[label=\arabic*)]
\item \(x' = f(t,x)\), \(x(t_0) = x_0^*\)
\item \(y' = f(t,y)\), \(y(t_0) = y_0^*\)
\end{enumerate}

where \(f(t,x)\) and \(g(t,x)\) are continuous functions in \((t,x)\) defined
on the rectangle

\begin{equation*}
R = \{(t,x) \ | \ |t-t_0| \leq a,\ |x-x_0| \leq b\}
\end{equation*}

where \((t_0, x_0^*)\) and \((t_0, y_0^*)\) are in \(R\).
In addition, let \(f\) be Lipschitz in \(R\) with Lipschitz constant \(K\)
and

\begin{equation*}
\forall \varepsilon > 0: \forall (t,x) \in R: |f(t,x) - g(t,x)| \leq \varepsilon
\end{equation*}

Let \(x(t; t_0, x_0^*)\) and \(y(t; t_0, y_0^*)\) be two solutions of the respective
IVPs on \(I: |t-t_0| \leq a\). If \(|x_0^* - y_0^*| \leq \delta\),
then show that

\begin{equation*}
\forall t \in I: |x(t) - y(t)| \leq \delta e^{K|t-t_0|} + \frac{\varepsilon}{K}\left(e^{K|t-t_0|} - 1\right)
\end{equation*}

We show, this directly

\begin{equation*}
\begin{aligned}
\forall t \in I: &|x(t) - y(t)|
\\&= \left|x_0^*-y_0^* + \int_{t_0}^t f(s,x(s)) - g(s, y(s)) \ ds\right|
\quad\text{by using the integral forms of the IVP}
\\&\leq |x_0^* - y_0^*| +\left|\int_{t_0}^t |f(s,x(s)) - g(s, y(s))| \ ds\right|
\\&= |x_0^* - y_0^*| +\left|\int_{t_0}^t f(s,x(s)) - f(s, y(s)) + f(s,y(s)) - g(s,y(s)) \ ds\right|
\\&\leq |x_0^* - y_0^*|
+ \left|\int_{t_0}^t |f(s,x(s)) - f(s, y(s))| \ ds \right|
+ \left|\int_{t_0}^t |f(s,y(s)) - g(s, y(s))| \ ds \right|
\\&\leq \delta
+ \left|\int_{t_0}^t K|x(s) - y(s)| \ ds \right|
+ |t-t_0| \varepsilon
\end{aligned}
\end{equation*}

There are now two cases to consider

\begin{itemize}
\item If \(t \geq t_0\), then
    
    \begin{equation*}
    \forall t \geq t_0: |x(t) - y(t)|
    \leq \delta + \int_{t_0}^t K|x(s) - y(s)| \ ds + (t-t_0)\varepsilon
    \end{equation*}
    
    If we let the right hand side be \(g(t)\), we get that \(g(t_0) = \delta\) and hence
    
    \begin{equation*}
    \frac{1}{K}\left(\ln(\varepsilon + Kg(t))- \ln(\varepsilon +K\delta)\right)
    = \int_{t_0}^t \frac{g'(s)}{\varepsilon + Kg(s)} \ ds
    = \int_{t_0}^t \frac{\varepsilon + K|x(s) - y(s)|}{\varepsilon + Kg(s)} \ ds
    \leq t-t_0
    \end{equation*}
    
    by rearranging, we get that
    
    \begin{equation*}
    |x(s) - y(s)| \leq g(s) \leq \delta e^{K(t-t_0)} + \frac{\varepsilon}{K}\left(e^{K(t-t_0)}-1\right)
    \end{equation*}
\item If \(t \leq t_0\), then
    
    \begin{equation*}
    \forall t \leq t_0: |x(t) - y(t)|
    \leq \delta + \int_{t}^{t_0} K|x(s) - y(s)| \ ds + (t_0-t)\varepsilon
    \end{equation*}
    
    If we let the right hand side be \(g(t)\), we get that \(g(t_0) = \delta\) and hence
    
    \begin{equation*}
    \frac{1}{K}\left(\ln(\varepsilon + Kg(t))- \ln(\varepsilon +K\delta)\right)
    = -\int_{t}^{t_0} \frac{g'(s)}{\varepsilon + Kg(s)} \ ds
    = \int_t^{t_0} \frac{\varepsilon + K|x(s) - y(s)|}{\varepsilon + Kg(s)} \ ds
    \leq t_0-t
    \end{equation*}
    
    by rearranging, we get that
    
    \begin{equation*}
    |x(s) - y(s)| \leq g(s) \leq \delta e^{K(t_0-t)} + \frac{\varepsilon}{K}\left(e^{K(t_0-t)}-1\right)
    \end{equation*}
    
    In both cases, we get the desired result.
\end{itemize}

\begin{admonition-thought}[{}]
There is probably a more clever way to prove this.
\end{admonition-thought}

\subsection{Question 3}
\label{develop--math6120:exercises:page--chapter2.adoc---question-3-4}

This question simply asks for the proof of continuation to the left.
This was done in \myautoref[{}]{develop--math6120:existence-and-uniqueness:page--continuation-and-dependence.adoc---continuation}.

\section{Lecture 5 (Existence in the large)}
\label{develop--math6120:exercises:page--chapter2.adoc---lecture-5-existence-in-the-large}

\subsection{Question 1}
\label{develop--math6120:exercises:page--chapter2.adoc---question-1-5}

This question simply asks for the proof of the non-local existence theorem on the left of the initial condition
This was already done

\subsection{Question 2}
\label{develop--math6120:exercises:page--chapter2.adoc---question-2-4}

Let \(a(t)\) be a continuous function defined on \(I: |t-t_0| \leq \alpha\).
Prove uniform convergence of the series for \(x\) defined by

\begin{equation*}
x(t) = x_0(t) + \sum_{n=0}^\infty [x_{n+1}(t) -x_n(t)]
\end{equation*}

\begin{admonition-remark}[{}]
I have no idea what this question is asking.
\end{admonition-remark}

\subsection{Question 3}
\label{develop--math6120:exercises:page--chapter2.adoc---question-3-5}

Let \(I \subset \mathbb{R}\) be an interval. By solving the linear
equation

\begin{equation*}
x'(t) =a(t)x(t) \quad x(t_0) = x_0
\end{equation*}

show that it has a unique solution on the whole of \(I\).
Use the non-local existence theorem to arrive at the same conclusion.

This question is first asking us to find the solution using the method described in
\myautoref[{}]{develop--math6120:ROOT:page--simple-results.adoc}; then, use the non-local existence theorem to show that the solution is unique.

This IVP has a unqiue solution

\begin{equation*}
x(t) = x_0e^{\int_{t_0}^t a(s) \ ds}
\end{equation*}

Alternatively, since \(f(t,x) = a(t)x\) has a partial derivative

\begin{equation*}
\frac{\partial f}{\partial x} = a(t)
\end{equation*}

which is bounded on \(I\). Then, \(f\) is Lipschitz on \(I\) and by the non-local
existence theorem, there exists a unique solution to the IVP on \(I\).

\subsection{Question 3}
\label{develop--math6120:exercises:page--chapter2.adoc---question-3-6}

By solving the IVP

\begin{equation*}
x' = -x^2,\ x(0) = -1, \quad 0 \leq t \leq T
\end{equation*}

show that the solution does not exist for \(t \geq 1\).
Does this example contradict the non-local existence theorem when \(T\geq 1\)?

Let \(\phi(t)\) be a solution to the above IVP.
Since \(\phi(t)\) is continuous and \(\phi(0) = -1\),
there exists \(a \in (0, T]\) such that \(\phi(t) < 0\) on
\([0, a)\). We choose the supremum of all such \(a\). Such a supremum exists
since the set of valid \(a\) is bounded above by \(T\).
Then,

\begin{equation*}
\forall t \in [0,a): \frac{1}{\phi(t)} + 1
= \left.\frac{1}{\phi(s)}\right|_0^t
= \int_{0}^t \frac{-\phi'(s)}{\phi(s)^2} \ ds
= \int_{0}^t 1 \ ds
= t
\end{equation*}

Therefore,

\begin{equation*}
\forall t \in [0,a): \phi(t) = \frac{1}{t-1}
\end{equation*}

Also notice that at \(t=a\), \(\phi\) must also be continuous and hence
\(\phi(a) = \phi(a^-) = \frac{1}{a-1}\). So,

\begin{equation*}
\forall t \in [0,a]: \phi(t) = \frac{1}{t-1}
\end{equation*}

Note that \(a < 1\) otherwise we have a discontinuity at \(t=a\).

Also, BWOC suppose that \(T\geq 1\), then since \(\frac{1}{a-1} < 0\),
we may have chosen \(a' > a\) for which \(\phi(t) > 0\) on \([0, a')\).
This invalidates the maximality of \(a\) and is a contradiction. Therefore \(T < 1\).

Also, BWOC, if \(a < T\), then since \(\frac{1}{a-1} > 0\), we may have chosen \(a' > a\)
for which \(\phi(t) < 0\) on \([0, a')\).
This invalidates the maximality of \(a\) and is a contradiction. Therefore \(a=T\).

Therefore, there exists a unique solution to the IVP

\begin{equation*}
\phi(t) = \frac{1}{t-1}, \ \forall t \in [0,T]
\end{equation*}

Also note throughout this proof, we showed that it is impossible for a solution to exist
for \(t \geq 1\). This does not contradict the non-local existence theorem since \(f(t,x)=-x^2\)
is not Lipschitz on any strip.
\end{document}
