\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Hypothesis testing}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
A \textbf{hypothesis} is a statement about the population parameter \(\theta\) that is \textbf{emperically testable}.
Such a statement can either be true or false.
We test a pair of hypotheses

\begin{description}
\item[Null hypothesis (\(H_0\))] \(\theta \in \Omega_0\).
    This statement is taken to be true throughout the duration of the test
\item[Alternative hypothesis (\(H_1\))] \(\theta \in \Omega_0^c\)
    If the null hypothesis is false, this statement is taken to be true
\end{description}

A hypothesis test is

\begin{description}
\item[Simple] If \(\Omega_0\) and \(\Omega_0^c\) are singletons
\item[Composite] If either \(\Omega_0\) or \(\Omega_0^c\) are not singletons
\end{description}

A \textbf{hypothesis testing procedure} is a rule that specifies

\begin{description}
\item[Acceptance region] The set of sample points for which \(H_0\) is accepted.
    For example
    
    \begin{equation*}
    R_1 = \{\vec{x} \in \chi: W(\vec{x}) > c\}
    \end{equation*}
\item[Rejection region] The set of sample points for which \(H_0\) is rejected and \(H_1\) is accepted.
    For example
    
    \begin{equation*}
    R_2 = \{\vec{x} \in \chi: W(\vec{x}) \leq c\}
    \end{equation*}
    
    where \(\chi\) is the sample space and \(W(\vec{X})\) is some statistic.
\end{description}

\begin{admonition-note}[{}]
We typically only specify the rejection region \(R = R_2\).
\end{admonition-note}

\section{Power}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---power}

In a hypothesis test, we can have any of the following 4 outcomes

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{} & {Accept \(H_0\)} & {Reject \(H_0\)} \\
\hline
{\(H_0\) is true} & {Correct decision} & {Type I error} \\
\hline
{\(H_1\) is true} & {Type II error} & {Correct} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Ideally, we want to minimize the probability of both a type I and type II error.

We define the \textbf{power function} of the test, which is based on the parameter value \(\theta\)

\begin{equation*}
\beta(\theta) = P(\vec{X} \in R | \theta)
\end{equation*}

So if

\begin{description}
\item[\(H_0\) is true, \(\theta \in \Omega_0\)] \(P(type I error) = \beta(\theta)\)
\item[\(H_1\) is true, \(\theta \in \Omega_0^c\)] \(P(type II error) = 1-\beta(\theta)\)
\end{description}

\begin{admonition-caution}[{}]
Usually, we define probability of making a type I error as \(\alpha\) and probability
of making a type II error as \(\beta\)
\end{admonition-caution}

We further say that this test is a \textbf{size} \(\alpha\) test if

\begin{equation*}
\sup_{\theta\in \Omega_0} \beta(\theta) = \alpha
\end{equation*}

That is the maximum probability of a type I error is \(\alpha\).

We say that this is a \textbf{level} \(\alpha\) test if

\begin{equation*}
\sup_{\theta\in \Omega_0} \beta(\theta) \leq \alpha
\end{equation*}

That is the maximum probability of a type II error is at most \(\alpha\)

\begin{example}[{Example hypothesis test}]
Suppose we have \(X_1, \ldots X_5 \sim Ber(\theta)\) and hypotheses

\begin{equation*}
H_0: \theta \leq 0.5
\quad\text{vs}\quad
H_1: \theta > 0.5
\end{equation*}

with rejection region \(R = \left\{\vec{x}: \sum_{i=1}^5 x_i = 5\right\}\) (ie reject if all 1)

Then

\begin{equation*}
\beta(\theta) = P(X\in R|\theta) = P(X_1=X_2=\cdots =X_5=1 | \theta) = \theta^5
\end{equation*}

Note that this is a strictly increasing function.
So

The maximum probability of a type \(I\) error is

\begin{equation*}
\sup_{\theta \leq 0.5} = \sup_{\theta \leq 0.5}\theta^5 = 0.5^5 = 0.03125
\end{equation*}

So this is a size 0.03125 test. But it can also said to be a level 0.05 test or a level 0.1 test.
\end{example}

\section{Likelihood Ratio Test}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---likelihood-ratio-test}

Let \(L(\theta|\vec{x})\) be the likelihood function of \(\theta\).
Then the \textbf{likelihood ratio test statistic} for testing

\begin{equation*}
H_0: \theta \in \Omega_0
\quad\text{vs}\quad
H_1: \theta \in \Omega_0^c
\end{equation*}

is given by

\begin{equation*}
\lambda(\vec{x})
=
\frac
{\sup_{\theta \in \Omega_0}L(\theta|\vec{x})}
{\sup_{\theta \in \Omega}L(\theta|\vec{x})}
=
\frac{L(\hat{\theta_0}|\vec{x})}{L(\hat{\theta}|\vec{x})}
\end{equation*}

where

\begin{description}
\item[Restricted MLE] \(\hat{\theta_0}\) is the MLE of \(\theta\) over \(H_0\)
\item[Unrestricted MLE] \(\hat{\theta}\) is the MLE of \(\theta\) over \(\theta \in \Omega\)
\end{description}

The \textbf{likelihood ratio test} is a test that rejects \(H_0\)
iff \(\lambda(\vec{x})\leq c\) where \(0 \leq c \leq 1\).
The value of \(c\) is chosen based on the desired size/level of the test.
Note that likelihood ratio test statistic \(\lambda(\vec{x}) \leq 1\).

Sometimes, we may want to produce a test for a parameter
\(\theta^{(1)}\), but the distribution also depends
on an unknown parameter \(\theta^{(2)}\).
In such a case \(\theta^{(2)}\) is referred to as a
\textbf{nuisance parameter}.

\begin{example}[{LRT for normal with unknown variance}]
Suppose we have \(X_1, \ldots X_n \sim N(\theta, \sigma^2)\)
and both \(\theta\) and \(\sigma^2\) are unknown
and we want to test

\begin{equation*}
H_0: \theta \leq \theta_0
\quad\text{vs}\quad
H_0: \theta > \theta_0
\end{equation*}

Note that in this case, \(\sigma^2\) is a nuisance parameter.

The likelihood function is

\begin{equation*}
L(\theta, \sigma^2| \vec{x}) = (2\pi\sigma^2)^{-n/2}\exp\left\{-\frac{1}{2\sigma^2}\sum_{i=1}^n (x_i-\theta)^2\right\}
\end{equation*}

We obtain

\begin{itemize}
\item unrestricted MLEs of \(\hat{\theta} = \overline{x}\) and \(\hat{\sigma}^2 = \frac{1}{n}\sum_{i=1}^n (x_i-\overline{x})^2\).
    So
    
    \begin{equation*}
    L(\hat{\theta}, \hat{\sigma}^2|\vec{x})
    = (2\pi\hat{\sigma}^2)^{-n/2}\exp\left\{-\frac{1}{2\hat{\sigma}^2}(n\hat{\sigma}^2)\right\}
    = (2\pi\hat{\sigma}^2)^{-n/2}\exp\left\{-\frac{n}{2}\right\}
    \end{equation*}
\item restricted MLEs of
    
    \begin{equation*}
    (\hat{\theta}_0, \hat{\sigma}^2_0) = \begin{cases}
    \left(\overline{x}, \frac{1}{n}\sum_{i=1}^n (x_i-\overline{x})^2 \right)
    ,&\quad\text{if } \vec{x} \leq \theta_0
    \\
    \left(\theta_0, \frac{1}{n}\sum_{i=1}^n (x_i-\theta_0)^2 \right)
    ,&\quad\text{if } \vec{x} \leq \theta_0
    \end{cases}
    \end{equation*}
    
    So
    
    \begin{equation*}
    L(\hat{\theta}_0, \hat{\sigma}_0^2|\vec{x})=
    \begin{cases}
    (2\pi\hat{\sigma}^2)^{-n/2}\exp\left\{-\frac{n}{2}\right\}
    ,&\quad\text{if } \vec{x} \leq \theta_0
    \\
    (2\pi\hat{\sigma}_0^2)^{-n/2}\exp\left\{-\frac{n}{2}\right\}
    ,&\quad\text{if } \vec{x} \leq \theta_0
    \end{cases}
    \end{equation*}
\end{itemize}

Therefore, the LRT statistic is

\begin{equation*}
\lambda(\vec{x})
= \frac{
    L(\hat{\theta}_0, \hat{\sigma}_0^2|\vec{x})
}{
    L(\hat{\theta}, \hat{\sigma}^2|\vec{x})
}
= \begin{cases}
1,&\quad\text{if } \vec{x} \leq \theta_0
\\
\left(\frac{\hat{\sigma}^2}{\hat{\sigma}^2_0}\right)^{n/2}
,&\quad\text{if } \vec{x} \leq \theta_0
\end{cases}
\end{equation*}

We therefore reject \(H_0\) if \(\lambda(\vec{x}) \leq c\).
This can be rearranged to show that LRT rejects \(H_0\) if

\begin{equation*}
\frac{\overbar{x} - \theta_0}{s/\sqrt{n}} \geq c^*
\end{equation*}

for some \(c^*\).
\end{example}

\begin{proposition}[{LRT of sufficient statistics}]
Let \(X_1, \ldots X_n\) be a random sample with parameter \(\theta\)
and sufficient statistic \(T(\vec{X})\) is pdf \(g(t|\theta)\).
Then,

\begin{equation*}
\lambda(\vec{x}) = \frac{\sup_{\theta \in \Omega_0}g(T(\vec{x})|\theta)}{\sup_{\theta \in \Omega}g(T(\vec{x})|\theta)}
= \lambda^*(T(\vec{x}))
\end{equation*}

where \(\lambda(\vec{x})\) and \(\lambda^*(t)\) are
the LRT statistics for \(\vec{X}\) and \(T(\vec{X})\) respectively.
\end{proposition}

\section{Monotone Likelihood Ratio}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---monotone-likelihood-ratio}

A family of pdfs/pmfs \(\{g(t|\theta)\ :\ \theta \in \Omega\}\) for a univariate
random variable \(T\) and real valued parameter \(\theta\) has
a \textbf{monotone likelihood ratio} if

\begin{equation*}
\frac{g(t|\theta_2)}{g(t|\theta_1)}
\end{equation*}

is an \textbf{increasing} (or decreasing) function of \(t\) for all \(\theta_2 > \theta_1\)
on \(\{t: g(t|\theta_1)>0\text{ or }g(t|\theta_2)>0\}\).

\begin{admonition-note}[{}]
Division by zero is allowed and is defined as positive infinity.
This is sensible since we are only dealing with non-negative values.
\end{admonition-note}

Common families have a MLR including: Normal (with known variance), Poisson and Binomial.
Additionally, if \(T\) is from an exponential family with \(g(t|\theta)=h(t)c(\theta)e^{w(\theta)t}\)
then \(T\) has a MLR if \(w(\theta)\) is non-decreasing with \(\theta\).

\section{Union-intersection tests}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---union-intersection-tests}

Suppose we want to test

\begin{equation*}
H_0: \theta \in \bigcap_{\gamma \in \Gamma}\Theta_\gamma
\end{equation*}

where \(\Gamma\) is some index set.

Sometimes it may be easier to test each \(H_{0\gamma}: \theta \in \Omega_\gamma\) individually.
The intersection-test rejects \(H_0\) if any of the individual
tests reject \(H_{0\gamma}\). That is if the tests
have rejection regions \(R_\gamma\),
the rejection region for \(H_0\) is \(R=\bigcup_{\gamma \in \Gamma}R_\gamma\).
Such a test is called a \textbf{intersection} test.

We can likewise define a union test for

\begin{equation*}
H_0: \theta \in \bigcup_{\gamma \in \Gamma} \Theta_\gamma
\end{equation*}

which has rejection region \(R = \bigcap_{\gamma \in \Gamma}R_\gamma\).

Although it may seem inefficient, the union-intersection tests
may yield equivalent forms of tests as the likelihood ratio test.

\begin{example}[{Normal union-intersection test (Example 8.2.8 in Casella)}]
Let \(X_1, \ldots X_n \sim N(\mu, \sigma^2)\) where \(\sigma^2\)
is unknown and we want to test

\begin{equation*}
H_0: \mu = \mu_0
\end{equation*}

We can rewrite this hypothesis as

\begin{equation*}
H_0: \mu \in (\{\mu: \mu \leq \mu_0\} \cap \{\mu:\mu \geq \mu_0\})
\end{equation*}

We can then produce rejection regions \(R_1\) and \(R_2\)
by producing the LRTs for the individual parameter spaces.

It is interesting that the rejection region \(R_1 \cap R_2\)
is the same as is yielded for the LRT of the original hypothesis.
\end{example}

\section{Properties}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---properties}

\subsection{Unbiased}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---unbiased}

A test is called \textbf{unbiased} if it satisfies

\begin{equation*}
P(\text{reject }H_0\ |\ H_0\text{ is false})
\geq
P(\text{reject }H_0\ |\ H_0\text{ is true})
\end{equation*}

Or equivalently

\begin{equation*}
\forall \theta' \in \Omega_0^c:
\forall \theta \in \Omega_0:
\beta(\theta') \geq \beta(\theta)
\end{equation*}

\subsection{Uniformly most powerful}
\label{develop--stat6110:ROOT:page--hypothesis-tests.adoc---uniformly-most-powerful}

Let \(\mathcal{C}\) be a class of tests for hypotheses

\begin{equation*}
H_0: \theta\in \Omega_0
\quad\text{vs}\quad
H_1: \theta\in \Omega_0^c
\end{equation*}

A test in \(C\) with power function \(\beta(\theta)\)
is called a \textbf{uniformly most powerful} (UMP) test
in class \(C\) if

\begin{equation*}
\beta(\theta) \geq \beta^*(\theta)
\quad \forall \theta \in\Omega_0^c
\quad\text{ for all other tests in C with }\beta^*(\theta)
\end{equation*}

If \(C\) is the class of all level \(\alpha\) tests,
the UMP test is called a \textbf{UMP level \(\alpha\) test}.

\begin{theorem}[{Neyman-Pearson Lemma}]
Consider testing

\begin{equation*}
H_0: \theta =\theta_0
\quad\text{vs}\quad
H_1: \theta =\theta_1
\end{equation*}

using rejection region

\begin{equation*}
\begin{aligned}
\vec{x} \in R \quad &\text{if} f(\vec{x}|\theta_1) > kf(\vec{x}|\theta_0)
\\
\vec{x} \notin R \quad &\text{if} f(\vec{x}|\theta_1) < kf(\vec{x}|\theta_0)
\end{aligned}
\end{equation*}

for some \(k \geq 0\).
Also let \(\alpha = P(\vec{x} \in R|\theta_0)\).

Then

\begin{description}
\item[Sufficiency] Any test with the above rejection region
    and having size \(\alpha\) is a UMP level \(\alpha\) test.
\item[Necessity] If there is a test with the above rejection region
    and having size \(\alpha\), then
    
    \begin{itemize}
    \item every UMP level \(\alpha\)
        test is a size \(\alpha\) test
    \item every UMP level \(\alpha\)
        test has the above rejection region, except perhaps on a set \(A\)
        satisfying
        
        \begin{equation*}
        P(\vec{X} \in A|\theta_0) = P(\vec{X} \in A|\theta_1) = 0
        \end{equation*}
    \end{itemize}
\end{description}

\begin{admonition-tip}[{}]
The above simply states that the above test
is a UMP level \(\alpha\) test and
it is the unique UMP level \(\alpha\) test (except possibly on
a set with measure zero).
\end{admonition-tip}
\end{theorem}

\begin{theorem}[{Karlin-Rubin}]
Suppose \(T(\vec{X})\) be a sufficient statistic for \(\theta\)
with family of pdfs \(\{g(t|\theta)\ : \ \theta \in \Omega\}\)
which has a monotone likelihood ratio.
Then

\begin{itemize}
\item If we want to test \(H_0: \theta \leq \theta_0\)
    vs \(H_1: \theta > \theta_0\),
    the UMP level \(\alpha\) test rejects \(H_0\)
    iff \(T>t_0\) where \(\alpha = P(T>t_0|\theta_0)\).
\item If we want to test \(H_0: \theta \geq \theta_0\)
    vs \(H_1: \theta < \theta_0\),
    the UMP level \(\alpha\) test rejects \(H_0\)
    iff \(T<t_0\) where \(\alpha = P(T<t_0|\theta_0)\).
\end{itemize}
\end{theorem}
\end{document}
