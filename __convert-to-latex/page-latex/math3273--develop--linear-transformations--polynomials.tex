\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Polynomials}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Let \(\mathbb{K}\) be a field. Then, we define \(\mathbb{K}[X]\) be
the set of all polynomials with finitely many terms. That is

\begin{equation*}
\DeclareMathOperator{\deg}{deg}
\mathbb{K}[X] = \{a_0 + a_1x +\cdots a_mx_m \ | \ m \geq 0, a_i \in \mathbb{K}\}
\end{equation*}

Then, \(\mathbb{K}[X]\) forms a euclidean domain with euclidean function being
the degree of the polynomial.

\begin{description}
\item[Characteristic Polynomial] Let \(A\) be a square matrix. Then, the characteristic polynomial of \(A\) ,
    \(\rho_A\) is defined as
    
    \begin{equation*}
    \rho_A(x) = \det(xI -A)
    \end{equation*}
\end{description}

\section{All Ideals are principal}
\label{develop--math3273:linear-transformations:page--polynomials.adoc---all-ideals-are-principal}

Let \(I \subseteq \mathbb{K}[X]\) be an \myautoref[{ideal}]{develop--math2272:ROOT:page--rings/index.adoc---ideals}. That is, \(I\)
is a subring for \(\mathbb{K}[X]\) where

\begin{equation*}
I \mathbb{K}[X] = \{f(x)g(x) \ | \ f \in I, g \in \mathbb{K}[X]\} \subseteq I
\end{equation*}

Then, \(I\) is a principal ideal. That is, \(\exists f \in I\) such that \(f\)
is monic and

\begin{equation*}
(f) = \{f(x) g(x) \ | \ g \in \mathbb{K}[X]\} = I
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
This is the proof presented in class
\end{admonition-note}

Let \(f\) be the monic polynomial of smalles degree. Then, by the closure of \(I\), \((f) \subseteq I\).
Next, consider arbriary \(g \in I\), then,

\begin{equation*}
g(x) = q(x)f(x) + r(x)
\end{equation*}

for some \(q, r \in \mathbb{K}[X]\) such that \(r = 0\) or \(\deg(r) < \deg(f)\). If \(r \neq 0\),

\begin{equation*}
r(x) = g(x) - q(x)f(x) \in I
\end{equation*}

since \(qf \in I\) since \(I\) is an ideal.
Then we can construct a monic from \(r\) of the same degree that is in \(I\), which contradicts the minimality fo
\(f\). Hence, \(r = 0\) and we get that

\begin{equation*}
g(x) = q(x)f(x) \in (f)
\end{equation*}

and hence \(I \subseteq (f)\) and we are done.
\end{example}

\section{GCD}
\label{develop--math3273:linear-transformations:page--polynomials.adoc---gcd}

Let \(f, g \in \mathbb{K}[X]\), then,

\begin{equation*}
I = \{f(x)u(x) + g(x)v(x) \ | \ u,v \in \mathbb{K}[X]\} = (f) +(g)
\end{equation*}

is an ideal. Then, \(\exists\) monic \(h \in I\) which generates \(I\). Then this \(I\) is the gcd of \(f\) and \(g\).

\begin{admonition-note}[{}]
This is not a definiton but actually a theorem since \(h\) is a common divisor fo \(f\)  and \(g\), and any common
divisor of \(f\) and \(g\) must also divide \(h\) since \(h\) is a linear combination of \(f\) ang \(g\).
\end{admonition-note}

\section{Unique factorization domain}
\label{develop--math3273:linear-transformations:page--polynomials.adoc---unique-factorization-domain}

A polynomial \(p \in \mathbb{K}[X]\) is called \emph{irriducable} if

\begin{itemize}
\item \(p\) is non-constant
\item \(p(x) = g(x)h(x)\) implies that \(g\) or \(h\) is contant.
\end{itemize}

Then, for any \(f \in \mathbb{K}[X]\) there exists a unique set of monic irriducable polynomials \(p_1,\ldots p_n \in \mathbb{K}[X]\)
and constant \(c \in \mathbb{K}\) such that

\begin{equation*}
f(x) = c p_1(x)\cdots p_n(x)
\end{equation*}
\end{document}
