\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Linear Differential Equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
In general, a linear ODE can be written as

\begin{equation*}
\numberandlabel{lin:gen}
    a_n(x) y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = g(x)
\end{equation*}

where \(a_n \neq \theta\) (the zero function) and as a result,
the above general linear ODEs may also be written as

\begin{equation*}
y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = g(x)
\end{equation*}

Furthermore, a linear ODE is said to be homogenous if
\(g = \theta\) (the zero function), that is

\begin{equation*}
\numberandlabel{lin:gen_hom}
    a_n(x) y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = 0
\end{equation*}

\section{Existence and Uniqueness of solutions}
\label{develop--math2271:ROOT:page--linear-differential-equations.adoc---existence-and-uniqueness-of-solutions}

Consider \(\eqref{lin:gen}\) where \(a_0, \ldots, a_n\) as
well as \(g\) are continuous in the interval
\(I=\{a < x < b\}\). Then, there exists a unique function
\(y(x)\) satisfying \(\eqref{lin:gen}\) in \(I\) as
well as the following IVP

\begin{equation*}
y(x_0) = y_0, \; y'(x_0) = y_1, \;\cdots\; , y^{(n-1)}(x_0)=y_{n-1}
\end{equation*}

\section{General Approach}
\label{develop--math2271:ROOT:page--linear-differential-equations.adoc---general-approach}

In order to find the general solution for the ODE \(\eqref{lin:gen}\),
the following steps are taken

\begin{enumerate}[label=\arabic*)]
\item Find the \emph{complementary solution} \(y_c\) which is the
    general solution for \(\eqref{lin:gen_hom}\).
\item Find a \emph{particular solution} \(y_p\) which is one solution
    for \(\eqref{lin:gen}\).
\end{enumerate}

Then, the general solution for \(\eqref{lin:gen}\) would be given by

\begin{equation*}
y = y_h + y_p
\end{equation*}

In order to see why this method is used, consider two solutions
\(y_1\) and \(y_2\) of \(\eqref{lin:gen}\) then

\begin{equation*}
y_1^{(n)} + a_{n-1}(x) y_1^{(n-1)} + \cdots + a_1(x)y_1' + a_0(x)y_1 = g(x)
\end{equation*}

and

\begin{equation*}
y_2^{(n)} + a_{n-1}(x) y_2^{(n-1)} + \cdots + a_1(x)y_2' + a_0(x)y_2 = g(x)
\end{equation*}

Then, by taking the difference, we get that

\begin{equation*}
(y_2-y_1)^{(n)} + a_{n-1}(x) (y_2-y_1)^{(n-1)} + \cdots + a_1(x)(y_2-y_1)' + a_0(x)(y_2-y_1) = 0
\end{equation*}

since the differential is a linear operator. Therefore, the difference
between any 2 solutions of \(\eqref{lin:gen}\) is a solution to
\(\eqref{lin:gen_hom}\) and hence all solutions can be generated
using the set of solutions to \(\eqref{lin:gen_hom}\) and any one
solution of \(\eqref{lin:gen}\).

\subsection{Principle of Superposition}
\label{develop--math2271:ROOT:page--linear-differential-equations.adoc---principle-of-superposition}

Notice that since the differential is a linear operator, if
\(y_1\) and \(y_2\) are solutions to
\(\eqref{lin:gen_hom}\), then their linear combination is also a
solution. Therefore, in general if \(\eqref{lin:gen_hom}\) has
solutions \(y_1, \ldots y_k\)

\begin{equation*}
y = C_1 y_1 + \cdots C_k y_k
\end{equation*}

is also a solution.

\subsection{Finding the complementary solution}
\label{develop--math2271:ROOT:page--linear-differential-equations.adoc---finding-the-complementary-solution}

By using the Principle of Superposition, it is clear that finding the
complementary solution gets reduced to finding a set of linearly
independent solutions for \(\eqref{lin:gen_hom}\). Now, since the
number of arbitrary constants of and ODE is equal to its degree, the
dimension of set of solutions is \(n\). Therefore, there are
at most \(n\) linearly independent solutions for
\(\eqref{lin:gen_hom}\); let these solutions be
\(f_1, f_2, \ldots f_n\). Then, the complementary solution is
given by

\begin{equation*}
y_c
    = C_1 f_1 + C_2 f_2 + \cdots + C_n f_n
\end{equation*}

where \(C_1, \ldots C_n\) are arbitrary constants.

\subsection{Wronskian and linear independence}
\label{develop--math2271:ROOT:page--linear-differential-equations.adoc---wronskian-and-linear-independence}

The Wronskian of the functions \(f_1, \ldots f_n\) is defined
as

\begin{equation*}
W(f_1, \ldots f_n)
    = \begin{vmatrix}
        f_1 & f_2 &\cdots &f_n\\
        f_1' & f_2' &\cdots &f_n'\\
        \vdots & \vdots & \ddots & \vdots\\
        f_1^{(n-1)} & f_2^{(n-1)} &\cdots &f_n^{(n-1)}\\
    \end{vmatrix}
\end{equation*}

Then, if the functions are linearly dependent, so are the columns (since
the differential is linear) and hence the Wronskian vanishes. Therefore,
by taking the contrapositive

\begin{equation*}
W(f_1, \ldots f_n) \neq 0
    \implies
    \{f_1, \ldots f_n\} \text{ are linearly independent}
\end{equation*}

Notice though that this is not a two way implication as there exists
sets of functions which are linearly independent, yet the Wronskian is
equal to \(0\). A notable example is
\(\{x^2, x|x|\}\) which was pointed out by Peano. Therefore,
using the Wronskian to test linear independence may produce false
negatives (but never produces false positives).
\end{document}
