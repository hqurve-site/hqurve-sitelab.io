\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Projections}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
Let \(V\) be vector space. Then, a linear transformation \(P \in \mathcal{L}(V)\)
is called a \emph{projection} if it is
\href{https://en.wikipedia.org/wiki/Idempotence}{idempotent}. That is

\begin{equation*}
P^2 = P
\end{equation*}

Then, since \(P\) is linear, \(P(V)\) and \(\ker(V)\) are subspaces of \(V\). Let
\(P(V) = U\) and \(\ker(P) = W\) and we index \(P\) as \(P_{U,W}\) since we would show that
\(P\) is uniquely defined by its image and kernel. Indeed, we would show
that a projection is unquiely defined by a (ordered) direct sum of \(V\)

\section{Properties}
\label{develop--math3273:inner-product-space:page--projection.adoc---properties}

\subsection{Identity within image}
\label{develop--math3273:inner-product-space:page--projection.adoc---identity-within-image}

Firstly, note that if \(\vec{v} \in P(V)\), \(\exists \vec{v}' \in V\) such
that \(P(\vec{v}') = \vec{v}\) hence

\begin{equation*}
P(\vec{v}) = P(P(\vec{v}')) = P(\vec{v}') = \vec{v}
\end{equation*}

\subsection{Direct sum with kernel}
\label{develop--math3273:inner-product-space:page--projection.adoc---direct-sum-with-kernel}

Consider \(\ker(P)\). Firstly, note that

\begin{equation*}
\vec{v} \in \ker(P) \cap P(V)
\implies \vec{v} = P(\vec{v}) = \vec{0}
\end{equation*}

and hence \(\ker(P) \cap P(V) = \{0\}\).
Also for any \(\vec{v} \in V\),
\(\vec{v} = P(\vec{v}) + (\vec{v} - P(\vec{v}))\)
then since

\begin{equation*}
P(\vec{v} - P(\vec{v})) = P(\vec{v}) - P(\vec{v}) = \vec{0}
\end{equation*}

\(\vec{v} -P(\vec{v}) \in \ker(P)\) and \(\vec{v}\) can be
written as an element in \(\ker(P) + P(V)\).

\subsection{Projection onto kernel}
\label{develop--math3273:inner-product-space:page--projection.adoc---projection-onto-kernel}

Let \(Q = I - P\), then we claim that \(Q\) is a projection onto \(\ker(P)\). Firstly

\begin{equation*}
Q^2 = (I-P)(I-P) = I^2 -2P + P^2 = I -2P + P = I-P = Q
\end{equation*}

Also, for any \(\vec{v} \in V\), we can uniquely write it as \(\vec{v} = \vec{w} + \vec{u}\)
where \(\vec{w} \in \ker(P)\) and \(\vec{u} \in P(V)\). Then,

\begin{equation*}
Q(\vec{v}) = \vec{v} - P(\vec{v}) = \vec{w} + \vec{w} - \vec{u} = \vec{w}
\end{equation*}

and hence \(Q(V) = \ker(P)\). Furthermore,

\begin{equation*}
QP = (I-P)P = P - P^2 = \theta = P^2 - P = P(I-P) = PQ
\end{equation*}

\subsection{Defining projection from direct sum}
\label{develop--math3273:inner-product-space:page--projection.adoc---defining-projection-from-direct-sum}

Consider the direct sum \(U \oplus W = V\),
we want to show that there exists a unique projection \(P \in \mathcal{L}(V)\) such
\(P(V) = U\) and \(\ker(P) = W\)

\begin{description}
\item[Existance] Firstly, we would show that one exists.
    Then, for any \(\vec{v} \in V\), we can uniquely write \(\vec{v} = \vec{w} + \vec{u}\) such that \(\vec{w} \in W\) and \(\vec{u} \in U\)
    then let \(P \in \mathcal{L}(V)\) such that
    
    \begin{equation*}
    P(\vec{v}) = \vec{u}
    \end{equation*}
    
    Clearly, this is indeed a well defined linear operator. Next, notice that
    
    \begin{equation*}
    P(P(\vec{v})) = P(\vec{u}) = P(\vec{u} + \vec{0}) = \vec{u} = P(\vec{v})
    \end{equation*}
    
    Hence, \(P\) is a projection with the desired property.
\item[Uniqueness] Let \(P, P' \in \mathcal{L}(V)\) be projections with \(P(V) = P'(V) = U\).
    and \(\ker(P) = \ker(P') = W\).
    Then, consider arbirary \(\vec{v} \in V\). We can uniquely write it as \(\vec{v} = \vec{w} + \vec{u}\)
    and hence
    
    \begin{equation*}
    P(\vec{v}) = P(\vec{w} + \vec{u}) = \vec{u} = P'(\vec{w} + \vec{u}) = P'(\vec{v})
    \end{equation*}
\end{description}
\end{document}
