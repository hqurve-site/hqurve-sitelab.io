\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Second-Order Linear Differential Equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\(\def\bm#1{{\bf #1}}\)

In general a second-order linear ODE can be written as

\begin{equation*}
\numberandlabel{2ol:gen}
    \frac{d^2y}{dx^2} + p(x)\frac{dy}{dx} + q(x)y = g(x)
\end{equation*}

Also, recall that a second-order linear ODE is said to be homogenous if
\(g = \theta\) (the zero function), that is

\begin{equation*}
\numberandlabel{2ol:gen_hom}
    \frac{d^2y}{dx^2} + p(x)\frac{dy}{dx} + q(x)y = 0
\end{equation*}

\begin{admonition-note}[{}]
Most of the methods here work for higher order linear ODEs.
\end{admonition-note}

\section{Fundamental Solutions}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---fundamental-solutions}

Fundamental solutions utilize the principle of superposition in order to
generate solutions for a homogeneous ode. Then, since we are dealing
with a second-order ode, we only need two two linearly independent
solutions of \(\eqref{2ol:gen_hom}\). Hence, if

\begin{itemize}
\item \(y_1\) and \(y_2\) are solutions to \(\eqref{2ol:gen_hom}\).
\item \(W(y_1, y_2) \neq 0\)
\end{itemize}

then our general solution for the ODE is \(y = ay_1 + by_2\).
Also, note that for our region of validity we will exclude value of
\(x\) which cause \(W(y_1, y_2) = 0\).

\section{Reduction of order}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---reduction-of-order}

If we know a solution \(y_1\) to \(\eqref{2ol:gen_hom}\)
then we can generate another solution \(vy_1\) by solving for
the function \(v\) in \(\eqref{2ol:gen_hom}\). We will
analyze the substitution term by term and will utilize the Leibniz rule
for differentiation. Then

\begin{equation*}
\frac{d^n(vy_1)}{dx^n} = v \frac{d^ny_1}{dx^n} + \text{terms in }v', v'', \ldots v^{(n)}
\end{equation*}

Note that the above statement even holds when \(n=0\). Then,
since \(y_1\) is a solution of \(\eqref{2ol:gen_hom}\) and
by adding all the differential terms, we get an equation only including
terms in \(v', v'' \ldots v^{(n)}\). We can then use the
substitution \(w = v'\) and solve a differential equation of
lower order (first-order in this case).

\subsection{Read more}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---read-more}

\url{https://math.stackexchange.com/questions/235855/why-does-reduction-of-order-work-for-linear-odes}

\section{Constant Coefficients}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---constant-coefficients}

If our homogenous second-order linear ODE has has constant coefficients,
that is

\begin{equation*}
a\frac{d^2y}{dx^2} + b\frac{dy}{dx} + cy =0
\end{equation*}

then one of our solutions will be in the form \(y=e^{mx}\)
where \(m\) is a constant. By substituting, we get that

\begin{equation*}
am^2e^{mx} + bme^{mx} + e^{mx} = e^{mx}(am^2 + bm + c) = 0
\end{equation*}

Since \(e^mx \neq 0\), finding a solution to the ODE reduces
to solving a quadratic equation. Note though that we have three cases to
consider

\begin{itemize}
\item Two distinct real roots. In this case, our solutions are already
    independent, so \(y = Ae^{m_1x} + Be^{m_2x}\).
\item Two complex roots. Since \(a, b, c\in \mathbb{R}\), the
    roots are a conjugate pair. So let \(m = \lambda \pm \mu i\).
    Then, again the solutions are linearly independent but we have complex
    numbers (eww). So by applying Euler’s formula for
    \(e^{\theta i}\) and simplifying, we get that our solution is
    \(y=e^{\lambda x}(A\sin\mu x + B\cos \mu x)\).
\item One real root. In this case \(m = \frac{-b}{2a}\), we will
    have to use reduction of order. So by letting \(y = ve^{mx}\).
    and substituting, we get that
    
    \begin{equation*}
    av'' + 2amv' + bv' = av'' + (2am+b)v' = 0
    \end{equation*}
    
    Because of the value of \(m\), the coefficient of
    \(v'\) is \(0\) and we get that
    \(v = Ax + B\) (affine). Therefore, our general solution is
    \(y=e^{mx}(Ax + B)\).
\end{itemize}

\subsection{Method of undetermined coefficients}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---method-of-undetermined-coefficients}

If \(g\) can be represented as the sum and/or product of

\begin{itemize}
\item polynomials
\item exponential functions including sine and cosine by Euler’s formula
\end{itemize}

then we can construct a particular solution using the method of
undetermined coefficients. Our particular solution would have a similar
'form' to that of \(g\). The following table outlines some of
the forms

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(\bm{g(x)}\)} & {\(\bm{y_p(x)}\)} \\
\hline[\thicktableline]
{\(x^n\)} & {\(a_nx^n + a_{n-1}x^{n-1} + \cdots + a_1x + a_0\)} \\
\hline
{\(e^{\alpha x}\)} & {\(ke^{\alpha x}\)} \\
\hline
{\(\sin (\alpha x)\) or \(\cos (\alpha x)\)} & {\(a_1 \sin (\alpha x) + a_2 \cos (\alpha x)\)} \\
\hline
{\(x^n e^{\alpha x}\)} & {\((a_n x^n + \cdots + a_0)e^{\alpha x}\)} \\
\hline
{\(e^{\beta x}\sin (\alpha x)\) or
\(e^{\beta x}\cos (\alpha x)\)} & {\(a_1 e^{\beta x} \sin (\alpha x) + a_2 e^{\beta x} \cos (\alpha x)\)} \\
\hline
{\(\cdots\)} & {\(\cdots\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Additionally, if the designated \(y_p\) already occurs as part
of the homogeneous solution, then successively multiply
\(y_p\) by \(x\) until it no longer does.

\subsubsection{Note:}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---note}

I think this method arises due to the Laplace transform however at the
time of writing, I know very little about the aforementioned
transformation.

\section{Variation of Parameters}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---variation-of-parameters}

If \(y_1\) and \(y_2\) are solutions to
\(\eqref{2ol:gen_hom}\) then we can generate the general solution to
\(\eqref{2ol:gen}\) by letting

\begin{equation*}
y(x) = u_1(x) y_1(x) + u_2(x) y_2(x)
\end{equation*}

Then by imposing the restriction of \(u_1'y_1 + u_2'y_2 = 0\),
we get that

\begin{equation*}
\begin{aligned}
        u_1'y_1 + u_2'y_2 &= 0\\
        u_1'y_1' + u_2'y_2' &= g(x)
    \end{aligned}
    \iff
    \begin{bmatrix}
        y_1 & y_2\\
        y_1'& y_2'
    \end{bmatrix}
    \begin{bmatrix}
        u_1'\\
        u_2'
    \end{bmatrix}
    =
    \begin{bmatrix}
        0\\
        g(x)
    \end{bmatrix}
\end{equation*}

we can then apply Cramer’s rule and integrating to get that

\begin{equation*}
u_1(x) = \int \frac{\begin{vmatrix} 0 & y_2 \\ g & y_2' \end{vmatrix}}{W(y_1, y_2)} dx = \int \frac{-y_2(x)g(x)}{W(y_1, y_2)} dx
\end{equation*}

\begin{equation*}
u_2(x) = \int \frac{\begin{vmatrix} y_1 & 0 \\ y_1' & g \end{vmatrix}}{W(y_1, y_2)} dx = \int \frac{y_1(x)g(x)}{W(y_1, y_2)} dx
\end{equation*}

Note that this method can be extended to any \(n\)’th order
linear ODE by using the restrictions of
\(u_1^{(r)}y_1 + u_2^{(r)}y_2 = 0\) for
\(r \in {1, 2, \cdots (n-2)}\).

\subsection{Read more}
\label{develop--math2271:ROOT:page--secondorder-linear-differential-equations.adoc---read-more-2}

\url{https://en.wikipedia.org/wiki/Variation\_of\_parameters\#Description\_of\_method}.
\end{document}
