\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Welcome}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\emoji{👋}

\section{Timetable}
\label{develop--main:ROOT:page--index.adoc---timetable}

\url{https://hqurve-site.gitlab.io/timetable}

\section{Notes}
\label{develop--main:ROOT:page--index.adoc---notes}

\begin{admonition-warning}[{}]
These `notes' may not (do not) follow directly what is taught in class
but rather seek to learn more about the topic. As a result, it is not recommended to
use them as the sole source for preparing for exams but rather a place to understand
more about a topic. That being said, I use them as my source of information as I am
aware of any possible discrepancies between here and what is taught in class.
\end{admonition-warning}

\subsection{Year 1}
\label{develop--main:ROOT:page--index.adoc---year-1}

These were handwritten \emoji{😥}

\subsection{Year 2}
\label{develop--main:ROOT:page--index.adoc---year-2}

These were originally written in latex for pdf output however by using
\texttt{pandoc} (as well as some other minor processing), I have converted them to
asciidoc. Note that because of the difference in output,
the structure of these pages are significantly different from
pages made with website in mind. Also beware, there may still be many visual errors.

For future reference, the original
latex documents (as well as pdf outputs) are in the relevant source trees.

\begin{description}
\item[Semester 1] 
    
    \begin{itemize}
    \item \myautoref[{Math2270: Multivariable Calculus}]{develop--math2270:ROOT:page--index.adoc}
    \item \myautoref[{Math2273: Linear Algebra I}]{develop--math2273:ROOT:page--index.adoc}
    \item \myautoref[{Math2274: Probability I}]{develop--math2274:ROOT:page--index.adoc}
    \item \myautoref[{Math2276: Discrete Mathematics}]{develop--math2276:ROOT:page--index.adoc}
    \item Math2410: Combinatorics I (merged with discrete mathematics)
    \end{itemize}
\item[Semester 2] 
    
    \begin{itemize}
    \item Comp2603: Object Oriented Programming I
        (I never really took notes)
    \item \myautoref[{Math2271: Introduction to Ordinary Differential Equations}]{develop--math2271:ROOT:page--index.adoc}
    \item \myautoref[{Math2272: Abstract Algebra I}]{develop--math2272:ROOT:page--index.adoc}
    \item \myautoref[{Math2275: Statistics I}]{develop--math2275:ROOT:page--index.adoc}
    \item \myautoref[{Math2277: Introduction to Real Analysis I}]{develop--math2277:ROOT:page--index.adoc}
    \end{itemize}
\end{description}

\subsection{Year 3}
\label{develop--main:ROOT:page--index.adoc---year-3}

The aim was to be able to produce web output for my notes for easy sharing and viewing.
Initially I was going to use \texttt{pandoc} (or some other converter) to convert
my tex files to html. However after some testing, I was not pleased with the result
and eventually followed the recommendation of
\href{https://tex.stackexchange.com/a/196520}{Ciro Santilli}, I am now using asciidoc
(see \myautoref[{here}]{develop--main:ROOT:page--index.adoc---technology-behind-this-website} for complete explanation)
which I am very pleased with.

\begin{description}
\item[Semester 1] 
    
    \begin{itemize}
    \item \myautoref[{Math2400: Number Theory}]{develop--math2400:ROOT:page--index.adoc}
    \item \myautoref[{Math3273: Linear Algebra II}]{develop--math3273:ROOT:page--index.adoc}
    \item \myautoref[{Math3277: Introduction to Real Analysis II}]{develop--math3277:ROOT:page--index.adoc}
    \item \myautoref[{Math3278: Probability II}]{develop--math3278:ROOT:page--index.adoc}
    \item \myautoref[{Math3402: Introduction to Partial Differential Equations}]{develop--math3402:ROOT:page--index.adoc}
    \end{itemize}
\item[Semester 2] 
    
    \begin{itemize}
    \item \myautoref[{Math3272: Abstract Algebra II}]{develop--math3272:ROOT:page--index.adoc}
    \item \myautoref[{Math3274: Set Theory}]{develop--math3274:ROOT:page--index.adoc}
    \item \myautoref[{Math3275: Introduction to Complex Analysis}]{develop--math3275:ROOT:page--index.adoc}
    \item \myautoref[{Math3465: Statistical Inference}]{develop--math3465:ROOT:page--index.adoc}
    \item \myautoref[{Math3610: Combinatorics II}]{develop--math3610:ROOT:page--index.adoc}
    \end{itemize}
\end{description}

\section{Postgrad Notes}
\label{develop--main:ROOT:page--index.adoc---postgrad-notes}

Note that I was initially doing an MSc. Mathematics, but then switched to MSc. Statistics.

\begin{description}
\item[Maths] 
    
    \begin{itemize}
    \item \myautoref[{Math6120: Differential Equations}]{develop--math6120:ROOT:page--index.adoc}
    \item \myautoref[{Math6620: Topology}]{develop--math6620:ROOT:page--index.adoc}
    \item \myautoref[{Math6180: Applied Probability Theory}]{develop--math6180:ROOT:page--index.adoc}
    \item \myautoref[{Math6192: Advanced Mathematical Modelling}]{develop--math6192:ROOT:page--index.adoc}
    \item \myautoref[{Math6194: Discrete Mathematics}]{develop--math6194:ROOT:page--index.adoc}
    \end{itemize}
\item[Stats] 
    
    \begin{itemize}
    \item \myautoref[{Statistical tools}]{develop--stats:ROOT:page--index.adoc}
    \item \myautoref[{Stat6110: Applied Statistical Inference}]{develop--stat6110:ROOT:page--index.adoc}
    \item \myautoref[{Stat6170: Multivariate Analysis}]{develop--stat6170:ROOT:page--index.adoc}
    \item \myautoref[{Stat6180: Advanced Topics in Statistics}]{develop--stat6180:ROOT:page--index.adoc}
    \item \myautoref[{Stat6130: Sampling Theory and Techniques}]{develop--stat6130:ROOT:page--index.adoc}
    \item \myautoref[{Stat6140: Experimental Designs and Analysis}]{develop--stat6140:ROOT:page--index.adoc}
    \item \myautoref[{Stat6160: Data Analysis}]{develop--stat6160:ROOT:page--index.adoc}
    \end{itemize}
\end{description}

\section{\strikethrough{Opinions} Facts}
\label{develop--main:ROOT:page--index.adoc---latex-backslashstrikethrough-latex-openbraceopinions-latex-closebrace-facts}

These pages outline what is right. They are not satire \emoji{😤}
and must be taken very seriously.

\begin{itemize}
\item \myautoref[{Vacuous Proofs}]{develop--main:opinions:page--vacuous.adoc}
\end{itemize}

\section{Food}
\label{develop--main:ROOT:page--index.adoc---food}

Recipes and so on.

\myautoref[{Food!}]{develop--food:ROOT:page--index.adoc}

\section{Technology behind this website}
\label{develop--main:ROOT:page--index.adoc---technology-behind-this-website}

\begin{description}
\item[Antora] The backbone of this site rests on \href{https://antora.org}{antora} and its default UI (well actually a fork of it).
    It is responsible for
    
    \begin{itemize}
    \item Collecting all the pages and generating the website files
    \item Managing extensions
    \item Managing references between and within pages
    \end{itemize}
\item[Asciidoc(tor)] All source files are written in
    \href{https://asciidoc-py.github.io/index.html}{asciidoc} and are then processed by the
    \href{https://asciidoctor.org}{asciidoctor(js) processor}. Note that processing chain
    is directed by antora. Additionally, the following extensions are used
    
    \begin{itemize}
    \item A (very heavily) modified version of \href{https://github.com/Mogztter/asciidoctor-emoji}{Asciidoctor Emoji Extension}
        to generate emojis from descriptive text. For example \texttt{emoji:fire[]} results in \emoji{🔥} (If this
        does not work, the extension is not working correctly). We utilize data from the
        \href{https://cldr.unicode.org/}{Unicode CLDR Project} for the symbol lookup.
    \item Several other custom extensions (look in the playbook source tree for them).
    \end{itemize}
\item[Katex] Mathematical expressions are written in latex which are processed at build time using \href{https://katex.org/}{Katex}.
    Locally however, the \href{https://www.mathjax.org/}{MathJax} library is used as rendering can be done per page instead
    of having to process everything.
    
    This however could not have been achieved without inspiration from \href{https://gitlab.com/djencks/asciidoctor-mathjax.js}{David Jencks' extension}
    which utilizes \texttt{Mathjax}. However, I was not able to make it work.
    
    \begin{admonition-remark}[{}]
    Yay \emoji{🎉}, we finally moved away from mathjax on production.
    \end{admonition-remark}
\item[Conversion to pdf] To convert to pdf, we first convert to latex and then use a normal latex compiler to convert to pdf.
    To convert to latex, we use custom code implemented through an Antora extension.
    Please see \url{https://gitlab.com/hqurve-site/coordinator}.
\end{description}
\end{document}
