\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Isomorphism Theorems}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
\def\normalsubgroup{\trianglelefteq}
\DeclareMathOperator{\ord}{ord}

% Title omitted
We have several notable isomorphism theorems

\section{First Isomorphism Theorem}
\label{develop--math3272:ROOT:page--isomorphism-theorems.adoc---first-isomorphism-theorem}

Let \((G,\star)\) and \((H,\cdot)\) be groups with
homomorphism \(f: G\to H\). Then if \(N = \ker(f)\),

\begin{equation*}
G/N \cong f(G)
\end{equation*}

with isomorphism \(\tilde{f}: G/N\to f(G)\) such that

\begin{equation*}
f = \tilde{f} \circ \phi
\end{equation*}

where \(\phi: G\to G/N\) defined by \(\phi(a) = aN\).

\begin{example}[{Proof}]
Since, from the requirement of composition, we get that

\begin{equation*}
f(a) = \tilde{f}(\phi(a)) = \tilde{f}(aN)
\end{equation*}

which gives us a definition for \(\tilde{f}\) (it also implies uniqueness).
That is, we define \(\tilde{f}\) to be

\begin{equation*}
\tilde{f}(aN) = f(a)
\end{equation*}

Notice that \(\tilde{f}\) is well defined since if \(aN = bN\),
we get that \(b^{-1}a \in N\) and hence \(f(b^{-1}a) = e_H\).
Therefore,

\begin{equation*}
f(b)^{-1}f(a) = f(b^{-1}a) = e_H \implies \tilde{f}(aN) = f(a) = f(b) = \tilde{f}(bN)
\end{equation*}

and hence \(\tilde{f}\) is well defined.

Next, we need to show that \(\tilde{f}\) is bijective.

\begin{equation*}
f(a) = \tilde{f}(aN) = \tilde{f}(bN) = f(b) \implies f(ab^{-1}) = f(a)f(b)^{-1} = e_H \implies ab^{-1} \in N \implies aN = bN
\end{equation*}

Hence injective. Next, for any \(f(a) \in f(G)\), there exists \(aN\) where \(\tilde{f}(aN) = f(a)\); hence surjective.

Finally, \(\tilde{f}\) is a homomorphism since

\begin{equation*}
\tilde{f}((aN)(bN)) = \tilde{f}((ab)N) = f(ab) = f(a)f(b) = \tilde{f}(aN)\tilde{f}(bN)
\end{equation*}

and we are done.
\end{example}

\begin{figure}[H]\centering
\includegraphics[]{images/develop-math3272/ROOT/first-isomorphism-theorem}
\end{figure}

\section{Second Isomorphism Theorem}
\label{develop--math3272:ROOT:page--isomorphism-theorems.adoc---second-isomorphism-theorem}

Let \((G, \star)\) be a group with \(N \normalsubgroup G\) and \(H \leq G\), then

\begin{enumerate}[label=\arabic*)]
\item \((H \cap N) \normalsubgroup H\)
\item \(HN \leq G\)
\item \(HN / N \cong H/(H\cap N)\)
\end{enumerate}

\begin{example}[{Proofs}]
\begin{example}[{Proof of 1}]
All we need to show is that

\begin{equation*}
\forall h \in H: \forall a \in (H \cap N): h^{-1}ah \in (H \cap N)
\end{equation*}

So, consider arbitrary \(a \in (H\cap N)\) and \(h\in H\). Then immediately,
by closure of subgroups, \(h^{-1} a h \in H\). Additionally, since \(N\normalsubgroup G\)
and \(h \in G\), we get that \(h^{-1} a h \in N\) and we are done.
\end{example}

\begin{example}[{Proof of 1 Alternative}]
Firstly, we establish that for any \(A, B \subseteq G\) and \(g \in G\), we have
that

\begin{equation*}
g(A \cap B) = g A \cap g B \quad\text{and}\quad (A\cap B)g = Ag \cap Bg
\end{equation*}

To see this, for any \(gx \in g(A\cap B)\), \(x \in A\) and \(x \in B\) and hence \(gx \in gA \cap gB\).
Conversely, if \(gx \in gA\) and \(gx \in gB\), \(x \in A\) and \(x \in B\) and hence \(gx \in g(A\cap B)\).

Now, consider arbitrary \(h \in H\). Then

\begin{equation*}
h(H\cap N) = hH \cap hN = H \cap Nh = Hh \cap Nh = (H\cap N)h
\end{equation*}
\end{example}

\begin{example}[{Proof of 2}]
\begin{equation*}
HN = \bigcup \{hN: h\in H\} = \bigcup \{Nh : h \in H\} = NH
\end{equation*}
\end{example}

\begin{example}[{Proof of 2 Alternative}]
Consider \(hn \in HN\), then \(hnh^{-1} \in N\) and hence \(hn \in NH\).

Conversely, consider \(nh\in NH\), then \(h^{-1} n h \in N\) and hence \(nh \in HN\).
\end{example}

\begin{example}[{Proof of 3}]
We define \(f: HN /N \to H/(H\cap N)\) by

\begin{equation*}
f(hnN) = h(H\cap N)
\end{equation*}

Then, notice that

\begin{equation*}
\begin{aligned}
hnN = h'n'N
&\iff (h')^{-1}h \in N
\\&\iff (h')^{-1}h \in H \cap N
\\&\iff h(H\cap N) = h'(H\cap N)
\end{aligned}
\end{equation*}

hence \(f\) is well defined and also injective. Furthermore, notice that trivially \(f\)
is surjective since \(\forall h(H\cap N) \in H/(H\cap N)\), \(f(heN) = h(H\cap N)\). Therefore,
\(f\) is bijective.

Finally, consider \(an_1N, bn_2N \in HN/N\). Then

\begin{equation*}
\begin{aligned}
f((an_1N)(bn_2N))
&= f(abN)
\\&= ab(H\cap N)
\\&= (a(H\cap N)) (b(H\cap N))
\\&= f(an_1N)f(bn_2N)
\end{aligned}
\end{equation*}

hence \(f\) is an isomorphism.

\begin{admonition-remark}[{}]
What on earth is the purpose of \(N\) in \(HN\)? I believe it might just be a technicality
since clearly the cosets of \(N\) relative to \(HN\) are the same as those relative to \(H\). Perhaps its
just a technicality since we need that \(N \normalsubgroup HN\).
\end{admonition-remark}
\end{example}
\end{example}

This theorem allows us to take quotient groups for a subgroup just as if the normal group was a subset.
Notice that if \(N\subseteq H\), then all three points are trivially true.

Perhaps a more interesting case is when \(N \cap H = \{e\}\). In such a case, the first result
is trivially true and the second and third results state that \(H \cong HN/N\).
If we add the extra property that \(HN = G\), we achieve the
amazing result that \(H \cong G/N\). That is \(H\) is sort of a complement
component of \(N\) in \(G\) and hence adds extra validity of thinking
of \(G/N\) as residues. Furthermore, this adds strong parallels to
\myautoref[{}]{develop--math3273:ROOT:page--direct-sums.adoc} and complement spaces of linear algebra.

\section{Lattice Isomorphism Theorem}
\label{develop--math3272:ROOT:page--isomorphism-theorems.adoc---lattice-isomorphism-theorem}

Let \((G, \star)\) be a group with \(N \normalsubgroup G\), then we define

\begin{equation*}
\mathcal{G} = \{A \ | \ N \leq A \leq G\}
\quad\text{and}\quad
\mathcal{N} = \{S \ | \ S \leq G / N\}
\end{equation*}

Then, we define the function \(\phi: \mathcal{G} \to \mathcal{N}\) by

\begin{equation*}
\phi(A) = A/N
\end{equation*}

Then, this is a well defined function (it actually maps to subgroups of \(G/N\)) and it is
bijective.

\begin{example}[{Proof}]
\begin{admonition-remark}[{}]
The fact that we have to prove that \(A/N\) is a subgroup of \(G/N\) is a bit of technical detail
since strictly speaking the group operator on \(A/N\) is different from that on \(G/N\).
Hence we would have to ``prove'' that there is a subgroup isomorphic to it. I'm omitting this detail
as it is unnecessary.
\end{admonition-remark}

To see that \(\phi\) is injective, we would show that \(\cup A/N = A\). Clearly,
\(A \subseteq \cup A/N \) since for each \(a \in A\), \(a \in aN \in A/N\). Now,
consider \(aN \in A/N\), then since \(N \leq A\), \(aN \subseteq A\) and hence
\(\cup A/N \subseteq A\).
Therefore, if \(A/N = B/N\) it would imply that their respective unions, \(A\) and \(B\), are equal.

Next, consider \(S \leq G/N\) we have to find \(N \leq A \leq G\) such that
\(S = A/N\). Note that we can define \(A = \cup S\); if \(N \leq A \leq G\) we have
found out \(A\).

Firstly, since \(N\) is the identity in \(G/N\), \(N \in S\) and hence
\(N \subseteq A\). Additionally by closure of the group, \(S \subseteq G\).
Now, consider \(a,b \in A\). Then, \(a \in a'N\) and \(b \in b'N\) where \(a'N, b'N \in S\).
Therefore, \(aN = a'N, bN = b'N \in S\) and since \(S\) is a subgroup,
\((aN)(bN)^{-1} = ab^{-1}N \in S\). Then since \(e \in N\), \(ab^{-1}e \in ab^{-1}N \in S\)
and hence \(ab^{-1} \in A\). Therefore, \(A\) is our subgroup of interest.
\end{example}

Furthermore, if \(A, B \in \mathcal{G}\), \(\phi\) preserves the following

\begin{description}
\item[Subgroup] \(A \leq B \iff A /N \leq B / N\)
\item[Normal Subgroup] \(A\normalsubgroup B \iff A /N \normalsubgroup B/N\)
\item[Index] \([B: A] = [B/N : A/N ]\)
\end{description}

\begin{example}[{Proofs}]
\begin{example}[{Proof for subgroup}]
Clearly the forward direction is true since the subset inequality is preserved
and both are subgroups of \(G/N\). Next, suppose that \(A/N \leq B/N \leq G/N\)
then \(A / N \subseteq B/N\) and hence

\begin{equation*}
A = \cup A/N \subseteq \cup B/N = B
\end{equation*}

Then since we already know \(A\) and \(B\) are subgroups, we are done.
\end{example}

\begin{example}[{Proof for normal subgroup}]
Suppose that \(A \normalsubgroup B\) then

\begin{equation*}
\forall b \in B: \forall a \in A: b a b^{-1} \in A
\end{equation*}

Then,

\begin{equation*}
\forall bN \in B/N: \forall aN \in A/N: (bN)(aN)(bN)^{-1} = (bab^{-1})N \in A/N
\end{equation*}

Therefore, \(A/N \normalsubgroup B/N\).

Conversely, suppose that \(A/N\normalsubgroup B/N\). Then

\begin{equation*}
\forall bN \in B/N: \forall aN \in A/N: (bN)(aN)(bN)^{-1} = (bab^{-1})N \in A/N
\end{equation*}

hence we get the desired result.
\end{example}

\begin{example}[{Proof for index}]
\begin{admonition-remark}[{}]
This is just an extract of the proof given in the
\myautoref[{third isomorphism theorem}]{develop--math3272:ROOT:page--isomorphism-theorems.adoc---third-isomorphism-theorem}.
Perhaps there is the minor difference that \(A\) and \(B\)
need not be normal, but the proof is virtually identical.
\end{admonition-remark}

To show that both indexes are equal, we would prove that there is a bijection
between the left cosets of both.

Let \(f:\{bA: b\in B\} \to \{(bN) (A/N): bN \in B\}\) defined by

\begin{equation*}
f(bA) = (bN)(A/N)
\end{equation*}

Then, \(f\) is well defined and injective since

\begin{equation*}
b'A = bA
\iff b^{-1}b' \in A
\iff (bN)^{-1}(b'N) = (b^{-1}b')N \in A/N
\iff (b'N)(A/N) = (bN)(A/N)
\end{equation*}

Next, notice that \(f\) is clearly bijective since for each left coset of \(A/N\),
\((bN)(A/N)\), there exists left coset \(bA\) of \(A\) such that \(f(bA) = (bN)(A/N)\).
\end{example}
\end{example}

Furthermore, \(\phi\) distributes over

\begin{description}
\item[Intersection] \((A \cap B) /N =(A/N) \cap (B/N)\)
\end{description}

\begin{example}[{Proof}]
\begin{example}[{Proof for intersection}]
This is pretty trivial to see. If \(xN \in (A\cap B)/N\),
\(x \in A\cap B\) which implies that \(xN \in A/N\) and
\(xN \in B/N\) and we are done. The reverse direction
is identical.
\end{example}
\end{example}

\begin{admonition-remark}[{}]
Usually, this theorem is split into
several parts and the fact that \(\phi\) is well defined is stated as a separate part.
That is a bit too cumbersome.
\end{admonition-remark}

Now, this theorem establishes two very useful pieces of information

\begin{itemize}
\item Subgroups of \(G/ N\) are uniquely determined by a subgroup between \(N\) and \(G\)
    and hence could be written in the form \(A/N\).
\item Subgroups of \(G/N\) preserve properties from its original subgroup.
\end{itemize}

\section{Third Isomorphism Theorem}
\label{develop--math3272:ROOT:page--isomorphism-theorems.adoc---third-isomorphism-theorem}

Let \((G, \star)\)  be a group with \(N \normalsubgroup G\). Then if \(N \leq K \normalsubgroup G \)

\begin{equation*}
G / K \cong (G/N) / (K /N )
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-remark}[{}]
This proof is very methodical and does not really give insight into the structure
of \((G/N)/(K/N)\).
\end{admonition-remark}

Since \((G/N)/(K/N)\) consists of \((xN)(K/N)\) and \(G/K\) consists of
\(xK\), the obvious choice for our function \(f: G/K \to (G/N)/(K/N)\)
is

\begin{equation*}
f(gK) = (gN)(K/N)
\end{equation*}

Then, notice if \(aK = bK\)

\begin{equation*}
aK = bK
\iff ab^{-1} \in K
\iff (aN)(bN)^{-1} = ab^{-1}N \in K/N
\iff (aN)(K/N) = (bN)(K/N)
\end{equation*}

\begin{admonition-tip}[{}]
To understand the final jump, notice that \(K/N \normalsubgroup G/N\) (from the lattice isomorphism theorem)
and it follows from properties of cosets.
\end{admonition-tip}

Therefore, \(f\) is well defined and injective.
Additionally, \(f\) is clearly surjective and hence a bijection.

Next, consider \(aK, bK \in G/N\). Then

\begin{equation*}
\begin{aligned}
f((aK)(bK))
&= f(abK)
\\&= (abN)(K/N)
\\&= ((aN)(bN))(K/N)
\\&= [(aN)(K/N)][(bN)(K/N)]
\\&= f(aK)f(bK)
\end{aligned}
\end{equation*}

hence \(f\) is an isomorphism
\end{example}

\subsection{Understanding the structure of \((G/N) / (K/N)\)}
\label{develop--math3272:ROOT:page--isomorphism-theorems.adoc---understanding-the-structure-of-latex-backslashgn-kn-latex-backslash}

Firstly, \(K/N\) is the set of cosets of \(N\) in \(K\). That is

\begin{equation*}
K/N = \{kN: k \in K\}
\end{equation*}

Next, what is \((G/N) / (K/N)\)? Well just going off notation, we get

\begin{equation*}
\begin{aligned}
(G/N) / (K/N)
&= \{x (K/N): x \in G/N\}
\\&= \{ gN(K/N): g \in G \}
\\&= \{ \{(gN)(kN): k \in K \} : g \in G \}
\\&= \{ \{gkN: k \in K \} : g \in G \}
\end{aligned}
\end{equation*}

That is, it is the set of cosets of cosets (obviously). Well, lets look at its
identity. It would be \(N(K/N)\) since \(N\) is the identity in \((K/N)\).
Upon expanding

\begin{equation*}
N(K/N) = \{N(kN): k \in K\} = \{kN: k \in K\}
\end{equation*}

 ... which is the set of all cosets generated by \(K\).
Next, for any two elements \((aN)(K/N), (bN)(K/N) \in (G/N)/(K/N)\),
their product is

\begin{equation*}
\begin{aligned}
\{akN: k \in K\}\{bkN: k \in K\}
&= \{(aN)(kN): k \in K\}\{(bN)(kN): k \in K\}
\\&= [(aN)(K/N)][(bN)(K/N)] = ((aN)(bN))(K/N)
\\&= (abN)(K/N)
\\&= \{(abN)(kN): k \in K\}
\\&= \{abkN: k \in K\}
\end{aligned}
\end{equation*}

by definition of the quotient group.
This almost looks like the set of products ... which it more or less is [\myautoref[{see here}]{develop--math3272:ROOT:page--basics.adoc---quotient-groups}].

\subsection{Example}
\label{develop--math3272:ROOT:page--isomorphism-theorems.adoc---example}

Perhaps an example would do better. Let \(a, b \in \mathbb{Z}\)
and let \(N = ab\mathbb{Z}\) and \(K = a\mathbb{Z}\). Then,
\(N \leq K \leq \mathbb{Z}\) and they are normal subgroups since \((\mathbb{Z}, + )\)
is abelian. Then, the theorem states that

\begin{equation*}
\mathbb{Z}_{a}
= \mathbb{Z}/a\mathbb{Z}
= G/K
\cong (G/N)/(K/N)
= (\mathbb{Z} / ab\mathbb{Z})/(a\mathbb{Z}/ab\mathbb{Z})
= \mathbb{Z}_{ab}/(a\mathbb{Z}/ab\mathbb{Z})
\end{equation*}

To see better, let \(a= 3\) and \(b = 5\). Then,

\begin{equation*}
\mathbb{Z}_a = \{3\mathbb{Z},1 + 3\mathbb{Z},2 + 3\mathbb{Z}\}
\quad\text{and}\quad
\mathbb{Z}_{ab} = \{15\mathbb{Z},1 + 15\mathbb{Z}, 2 + 15\mathbb{Z} \ldots, 14 + 15\mathbb{Z}\}
\end{equation*}

and

\begin{equation*}
a\mathbb{Z}/ab\mathbb{Z} = \{15\mathbb{Z}, 3 + 15\mathbb{Z}, 6 + 15\mathbb{Z}, 9 + \mathbb{Z}, 12 + 15\mathbb{Z}\}
\end{equation*}

Then, the theorem states that \(\{0 + 3\mathbb{Z},1 + 3\mathbb{Z},2 + 3\mathbb{Z}\}\)
is isomorphic to

\begin{equation*}
\left\{
\begin{matrix}
\{0 + 15\mathbb{Z}, 3 + 15\mathbb{Z}, 6 + 15\mathbb{Z}, 9 + \mathbb{Z}, 12 + 15\mathbb{Z}\}, \\
\{1 + 15\mathbb{Z}, 4 + 15\mathbb{Z}, 7 + 15\mathbb{Z}, 10 + \mathbb{Z}, 13 + 15\mathbb{Z}\}, \\
\{2 + 15\mathbb{Z}, 5 + 15\mathbb{Z}, 8 + 15\mathbb{Z}, 11 + \mathbb{Z}, 14 + 15\mathbb{Z}\}
\end{matrix}
\right\}
\end{equation*}

which is definitely correct
\end{document}
