\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Hypothesis Testing}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
A \emph{statistical hypothesis} is simply a statement about a population
parameter. Then, there are two types of hypotheses

\begin{description}
\item[Simple] A hypothesis \emph{simple} when it asserts a particular value for the parameter.
\item[Composite] A hypothesis is \emph{composite} when it asserts a parameter belongs to a
    set (typically an interval).
\end{description}

\begin{admonition-note}[{}]
In this section we would only focus on distributions
indexed by parameter in question. For example, the population
mean of an exponential distribution.
\end{admonition-note}

\section{Our framework}
\label{develop--math3465:testing:page--index.adoc---our-framework}

The typical framework for hypotheses involves two hypotheses.

\begin{description}
\item[Null Hypothesis] \(H_0\) is the hypothesis we assume to be true whilst conducting the test
\item[Alternative Hypothesis] \(H_1\) is the hypothesis we take to be true if we are confident
    that the null hypothesis is false.
\end{description}

Then a test can be defined by the region of the sample space on which we reject \(H_0\).
That is the \emph{rejection (or critical) region}, \(R\), of a test is the set of observations which if observed,
we reject \(H_0\).

Then a test \(\delta\) specifies our critical region. For the proposes of this section,
we would let a test be defined by its critical region. That is the critical
region of \(\delta\) is \(R = \delta\).

\section{Simple Hypotheses}
\label{develop--math3465:testing:page--index.adoc---simple-hypotheses}

For simple hypotheses

\begin{equation*}
H_0 : \theta = \theta_0 \quad\text{and}\quad H_1: \theta = \theta_1
\end{equation*}

we immediately have two types of errors

\begin{description}
\item[Type I] This occurs when we reject \(H_0\) when it is true. The probability
    of this occurring is the \emph{significance level} (or \emph{size}) of the test and usually denoted
    
    \begin{equation*}
    \alpha = P(H_0 \text{ rejected} \ | \ H_0 \text{ is true})
    \end{equation*}
\item[Type II] This occurs when we fail to reject \(H_0\) when \(H_1\) is true.
    The probability of this not occurring is the \emph{power} of the test and is usually denoted
    
    \begin{equation*}
    1 - \beta = P(H_0 \text{ is not rejected} \ | \ H_1 \text{ is true}) = P(H_1 \text{ accepted} \ | \ H_1 \text{ is true})
    \end{equation*}
\end{description}

For any pair of hypothesis, there are many tests (precisely the power-set of the sample space).
We therefore require a way to judge them. What is usually done, is that the maximum probability
of committing a type I error is fixed and then we judge tests based on their power. That is,
we choose the test from the set

\begin{equation*}
\{\delta \subseteq S: \alpha(\delta) \leq \alpha_0 \}
\end{equation*}

such that we minimize \(\beta(\delta)\). We would now show that since both \(H_0\) and \(H_1\) are
simple, such a \emph{best} test occurs.

\subsection{Neyman-Pearson-Fisher Lemma}
\label{develop--math3465:testing:page--index.adoc---neyman-pearson-fisher-lemma}

Let \(H_0\) and \(H_1\) be simple tests

\begin{equation*}
H_0 : \theta = \theta_0 \quad\text{and}\quad H_1: \theta = \theta_1
\end{equation*}

Now, let \((X_1, \ldots X_n)\) be iid \(f(x; \theta)\) and let
\(f_n(\vec{x}; \theta)\) be their joint density function.
Then, if there exists a positive constant \(c\) and
test \(\delta\) with significance level \(\alpha_0\)
which rejects \(H_0\) if

\begin{equation*}
\frac{f_n(\vec{x}, \theta_1)}{f_n(\vec{x}; \theta_0)} > c
\end{equation*}

and accepts \(H_0\), \(\delta\) is the
best test (ie has greatest power).

\begin{example}[{Proof}]
Suppose we have a test \(\delta_0\) and constant \(c\) which satisfies the conditions.
Then, by rearranging the inequality

\begin{equation*}
\forall \vec{x} \in \delta_0: c f_n(\vec{x}; \theta_0) - f_n(\vec{x}; \theta_1) < 0
\end{equation*}

Therefore, \(\delta_0\) minimizes the linear combination

\begin{equation*}
\begin{aligned}
c \alpha(\delta) + \beta(\delta)
&= \int_{\delta} f_n(\vec{x} ; \theta_0) \ d\vec{x} + 1 \int_{\delta^c} f_n(\vec{x}; \theta_1)\ d\vec{x}
\\&= \int_{\delta} f_n(\vec{x} ; \theta_0) \ d\vec{x} + 1 - \int_{\delta} f_n(\vec{x}; \theta_1)\ d\vec{x}
\\&= 1 + \int_{\delta} [cf_n(\vec{x} ; \theta_0) - f_n(\vec{x}; \theta_1)]\ d\vec{x}
\end{aligned}
\end{equation*}

Since the integral is least when taken over non-positive values.
However, notice that the linear combination \(c\alpha(\delta) + \beta(\delta)\)
is only minimized by best tests since \(\alpha(\delta)\) is constant.
Therefore \(\delta_0\) is a best test.
\end{example}

\begin{admonition-note}[{}]
Actually, on the boundary it doesn't matter if the test
is rejected of accepted. Hence there may be multiple best tests
which are practically identical.
\end{admonition-note}

\begin{admonition-caution}[{}]
The way how this was worded in class was a bit different. It stated
that the best test is the one which satisfies those conditions. However, this
is not the case. It is only the best if such a test exists in the first place.
\end{admonition-caution}

\subsubsection{Intuition of the result}
\label{develop--math3465:testing:page--index.adoc---intuition-of-the-result}

Firstly, lets rearrange the formula

\begin{equation*}
f_n(\vec{x}, \theta_1) > cf_n(\vec{x}; \theta_0)
\end{equation*}

Then, this can be easily interpreted as we reject \(H_0\)
if the probability of obtaining the sample under \(H_1\)
is \(c\) times that of \(H_0\).

\section{Composite Tests}
\label{develop--math3465:testing:page--index.adoc---composite-tests}

Consider test \(\delta\) whose hypotheses are

\begin{equation*}
H_0 : \theta \in \Omega_0 \quad\text{and}\quad H_1: \theta \in \Omega_1
\end{equation*}

In such a case, probabilities of type I and II errors are not as easy to define since
the error may depend on the specific value of \(\theta\). Instead,
we define \emph{significance level} (or \emph{size}) to be the supremum of all significance levels. That is

\begin{equation*}
\alpha = \sup_{\theta \in \Omega_0} P(\vec{x} \in \delta; \theta)
\end{equation*}

Likewise, the concept of power is not readily apparent for composite hypotheses. So we
would define the \emph{power function} of a test to be

\begin{equation*}
\pi(\theta) = P(\vec{x} \in \delta; \theta) \quad\text{where } \theta \in \Omega_1
\end{equation*}

From this we see that two tests may not be readily comparable. However,
in the case where \(\delta_1, \delta_2\) are such that

\begin{equation*}
\pi_1(\theta) \geq \pi_2(\theta) \quad \forall \theta \in \Omega_1
\end{equation*}

we say that \(\delta_1\) is \emph{uniformly more powerful} than \(\delta_2\).

\subsection{Note on point tests}
\label{develop--math3465:testing:page--index.adoc---note-on-point-tests}

Sometimes we wish to test whether \(\theta\) is a specific value. That is

\begin{equation*}
H_0 : \theta = \theta_0 \quad\text{and}\quad H_1: \theta \neq \theta_0
\end{equation*}

Then, if the true value of \(\theta\) deviates from \(\theta_0\) even slightly,
our test would be rejected if the its power is great. However,
is that typically the power of a test would increase as the sample size increases and hence
given a large enough sample size, we are guaranteed to reject \(H_0\).

The problem arises from the fact that when we say \(\theta = \theta_0\), we typically mean
that \(|\theta - \theta_0| < \varepsilon\) where \(\varepsilon\) is our acceptable error
bound.

For example, suppose we wanted to test whether the average height of humans
is \(170cm\). Then, the true value is likely different, however, no measurement tool
would ever allow us to even obtain the true value. Regardless, using our
method of hypothesis tests, given a large enough sample size, we are almost
guaranteed to reject \(H_0\).

This leads to the discussion of statistical significance versus practical significance.
That is, our tests may yeild statistically significant results however, in practice,
the result obtained may be insignificant. For example, if we were designing clothing
to fit on all of our staff, the distinction between \(170cm\) and \(170.1cm\)
is not important.

\subsection{Uniformly most powerful}
\label{develop--math3465:testing:page--index.adoc---uniformly-most-powerful}

We say that test \(\delta\) with size \(\alpha_0\) is a
\emph{uniformly most powerful} (UMP) if \(\delta\)
is the most powerful test of size \(\alpha_0\) of

\begin{equation*}
H_0 : \theta = \theta_0 \quad\text{and}\quad H_1: \theta = \theta_1
\end{equation*}

for all \(\theta_0 \in \Omega_0\) and \(\theta_1 \in \Omega_1\).

\subsection{Monotone likelihood ratio property}
\label{develop--math3465:testing:page--index.adoc---monotone-likelihood-ratio-property}

Let \(f_n\) be the joint pdf of \(\vec{X} = (X_1, \ldots X_n)\)
and let \(T = r(\vec{X})\) be a statistic.
Then, \(f_n\) has a \emph{monotone likelihod ratio}
if for every \(\theta_1 < \theta_2\), the ratio

\begin{equation*}
\frac{f_n(\vec{x}; \theta_2)}{f_n(\vec{x}; \theta_1)}
\end{equation*}

depends on \(\vec{x}\) only through \(T\)
and is a monotonic function in \(T\).

\subsection{Theorem}
\label{develop--math3465:testing:page--index.adoc---theorem}

Let \(f_n\) have an increasing likelihood ratio
wrt \(T\).
Furthermore, let \(c\) and \(\alpha_0\) be constants
such that

\begin{equation*}
P(T \geq c \ | \ \theta = \theta_0) = \alpha_0
\end{equation*}

and hypotheses

\begin{equation*}
H_0: \theta \leq \theta_0 \quad\text{and}\quad H_1: \theta > \theta_0
\end{equation*}

Then the test \(\delta\) which rejects \(H_0\) when \(T \geq c\)
is a uniformly most powerful test of \(H_0\) vs \(H_1\).

\begin{admonition-note}[{}]
In the case where \(f_n\) has a decreasing
likelihood ratio, the inequalities are reversed.
\end{admonition-note}

\subsection{Likelyhood ratio tests}
\label{develop--math3465:testing:page--index.adoc---likelyhood-ratio-tests}

As we have seen, unlike the \href{https://www.youtube.com/watch?v=eVFd46qABi0}{simple} case
in which we can use the Neyman-Pearson Lemma, composite hypotheses need not
have a best test. Regardless, we can still get a good test constructed using
the likelihood ratio. That is, the test rejects \(H_0\) if

\begin{equation*}
\frac{\max_{\theta \in \Omega_1} f_n(x; \theta)}{\max_{\theta \in \Omega_0} f_n(x;\theta)} > c
\end{equation*}

where \(c\) is some constant depending on the power of the test. Also,
note that in general, the supremum would be used.

\begin{admonition-tip}[{}]
Don't be scared by finding the maximum, the methodology is similar to that used
in finding
\myautoref[{maximum likelihood estimators}]{develop--math3465:point-estimators:page--construction.adoc---maximum-likelihood-estimator}.
However, now, we are also interested in the value attained instead of the position at which it is attained.
\end{admonition-tip}

\begin{admonition-tip}[{}]
Typically, for normally distributed variables, the rejection criterion would simplify to
\(\frac{\hat{\sigma}_0^2}{\hat{\sigma}_1^2} > k\). Yes; that is the correct fraction
\end{admonition-tip}
\end{document}
