\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{ANOVA}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\mat#1{{\bf #1}}
\def\vec#1{{\bf #1}}

% Title omitted
We may have data from many different populations,
each of which gets different \textbf{treatments}.
We use ANOVA to determine whether these different treatments
cause any difference on the mean.
The ANOVA test simply states whether any of the treatments
have cause a difference, we need to perform \textbf{post-hoc analysis} to determine
which treatments cause significant differences.

\section{One-way ANOVA}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---one-way-anova}

The first, most simple ANOVA is where there is only
one factor being test.
Suppose that we have \(g\) treatment groups
each with \(n_i\) observations.
The observations must be of the form

\begin{equation*}
X_{ij} = \mu + \tau_i + \varepsilon_{ij}
\end{equation*}

where

\begin{itemize}
\item \(X_{ij}\) is the \(j\)'th observation from the \(i\)'th group
\item \(\mu\) is the overall mean
\item \(\tau_i\) is the effect of the \(i\)'th treatment.
    Where \(\sum_{i=1}^g n_i\tau_i = 0\)
\item \(\varepsilon_{ij} \sim N(0,\sigma^2)\) is the random
    error associated with \(X_{ij}\)
\end{itemize}

Note that this implicitly requires the following assumptions

\begin{itemize}
\item Normality
\item Equal variance (Homoscedasicity)
\item Independence
\end{itemize}

\begin{admonition-important}[{}]
If the assumptions are not met, the Kuskal-Wallis test can be performed instead.
\end{admonition-important}

\begin{admonition-tip}[{}]
The \(\tau_i\) can be obtained from \(\mu_i\)
by letting \(\mu\) be the weighted mean of \(\mu_i\) (by \(n_i)\)
and \(\tau_i = \mu_i - \mu\).
\end{admonition-tip}

The hypotheses are

\begin{itemize}
\item \(H_0\): \(\tau_i = 0\) for all \(i\)
\item \(H_1\): \(\tau_i \neq 0\) for at least one \(i\)
\end{itemize}

To assess these hypotheses, we first decompose each observation
as follows

\begin{equation*}
x_{ij} = \overbar{x} + (\overbar{x}_i - \overbar{x}) + (x_{ij} - \overbar{x}_i)
\end{equation*}

The factors are

\begin{itemize}
\item \(\overbar{x}\): the overall sample mean. This is an estimate of \(\mu\)
\item \(\overbar{x}_i - \overbar{x}\): the effect of treatment \(i\). Note
    that \(\overbar{x}_i\) is the mean of observations for treatment \(i\).
    Also, note that \(\overbar{x}_i - \overbar{x}\) is an estimate of \(\tau_i\)
\item \(x_{ij} - \overbar{x}_i\): the random variation in this observation.
    This is an estimate of \(\varepsilon_{ij}\).
\end{itemize}

\begin{admonition-tip}[{}]
If we have a table where each row is for the \((i,j)\) observation
with columns \(\overbar{x}\), \((\overbar{x}_i - \overbar{x})\)
and \((x_{ij} - \overbar{x})\), these columns are orthogonal (dot product is zero).
I am not sure how useful this is, but I put it here.
See also pg 299 of the Applied Multivariate Statistics (6th).
\end{admonition-tip}

Then, we use the following (ANOVA) table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Source of variation} & {Sum of Squares} & {degrees of freedom} & {Mean Squares} & {F-stat} \\
\hline[\thicktableline]
{Treatements (between)} & {\(SSTR = \sum_{i} n_i(\overbar{x}_i - \overbar{x})^2\)} & {\(g-1\)} & {\(MSTR = \frac{SSTR}{g-1}\)} & {\(F=\frac{MSTR}{MSE}\)} \\
\hline
{Residuals (within)} & {\(SSE = \sum_{i,j} (x_{ij} - \overbar{x}_i)^2\)} & {\(n-g\)} & {\(MSE = \frac{SSE}{n-g}\)} & {} \\
\hline
{Total} & {\(SST = SSTR + SSE = \sum_{i,j} (x_{ij} - \overbar{x})^2\)} & {\(n-1\)} & {} & {} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Note that the sum of squares values (divided by population variance) have chi-squared distribution with the respective degrees of freedom.
Hence,

\begin{equation*}
F = \frac{MSTR}{MSE} \sim F_{g-1, n-g}
\end{equation*}

and we reject H0 if \(F\) is large.

\begin{admonition-tip}[{}]
The \(MSE\) is a generalization of the pooled
means since

\begin{equation*}
MSE = \frac{1}{n-g}\sum_{i=1} (n_i-1)s_i^2
\end{equation*}
\end{admonition-tip}

\subsection{Implementing in R}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---implementing-in-r}

To use the ANOVA model in \(R\), first notice that it is simply a reframed
version of a linear problem since \(X_{ij} \sim N(\mu + \tau_i, \sigma^2)\).
So, we can use the following code

\begin{listing}[{}]
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the independent and dependent variables
# each row contains a pair (independent, dependent)

is.factor(data$independent) #ensure that independent data is a a factor
data$independent <- as.factor(data$independent) #convert if necessary

# Either
result <- lm(dependent ~ independent, data=data)
anova(result)
# OR
result <- aov(dependent ~ independent, data=data)
summary(result)
\end{listing}

We must also assess the assumptions of the model on our data

\begin{description}
\item[Normality] We can plot a qq-plot and use the Shapiro-Wilk test
\item[Independence] Can only be verified during collection
    or using ACF plots
\item[Constant variance] We can
    
    \begin{itemize}
    \item use the \texttt{bartlett.test} function
        
        \begin{listing}[{}]
        bartlett.test(dependent ~ independent, data=data)
        \end{listing}
    \item plot boxplots for each of the treatment groups
        
        \begin{listing}[{}]
        boxplot(dependent ~ independent, data=data)
        \end{listing}
    \item Plot a scatter plot of the residuals
        
        \begin{listing}[{}]
        plot(result$fit, result$res)
        \end{listing}
    \end{itemize}
\end{description}

\section{One-way MANOVA}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---one-way-manova}

The MANOVA model is simply the multivariate extension to the ANOVA model.
The key differences to the model are

\begin{itemize}
\item Each of the observations has \(p\) variables
\item The random errors are \(\vec{\varepsilon}_{ij} \sim N(\vec{0}, \mat{\Sigma})\)
\end{itemize}

There are many assumptions:

\begin{description}
\item[During collection] 
    
    \begin{itemize}
    \item two or more dependent variables (\(p \geq 2\))
    \item one independent categorical variable (for treatments)
    \item independent observations
    \item adequate sample size
    \end{itemize}
\item[After collection] 
    
    \begin{itemize}
    \item No outliers
    \item Multivariate normality
    \item linear relationship between variables
    \item constant covariance
    \item no multicollinearity.
        
        \begin{itemize}
        \item If the multicollinearity is low, we use separate ANOVA on each variable
        \item If the multicollinearity is high, use PCA
        \end{itemize}
    \end{itemize}
\end{description}

We use the following (MANOVA) table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Source of variation} & {Sum of Squares} & {degrees of freedom} \\
\hline[\thicktableline]
{Treatements (between)} & {\(\mat{B} = \sum_{i} n_i(\vec{\overbar{x}}_i - \vec{\overbar{x}})(\vec{\overbar{x}}_i - \vec{\overbar{x}})^T\)} & {\(g-1\)} \\
\hline
{Residuals (within)} & {\(\mat{W} = \sum_{i,j} (\vec{x}_{ij} - \vec{\overbar{x}}_i)(\vec{x}_{ij} - \vec{\overbar{x}}_i)^T\)} & {\(n-g\)} \\
\hline
{Total} & {\(\mat{B} + \mat{W} = \sum_{i,j} (\vec{x}_{ij} - \vec{\overbar{x}})(\vec{x}_{ij} - \vec{\overbar{x}})^T\)} & {\(n-1\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Note that the sum of squares values have Wishart distribution with the respective degrees of freedom
and covariance matrix \(\Sigma\).

We use Wilk's Lambda test statistic

\begin{equation*}
\Lambda^* = \frac{|\mat{W}|}{|\mat{B} + \mat{W}|}
= \prod_{i=1}^{\min(\rho, g-1)} \left(\frac{1}{1+\lambda_i}\right)
\end{equation*}

where the \(\lambda_i\) are the eigenvalues of \(\mat{W}^{-1}\mat{B}\).

We reject the null hypotheses if \(\Lambda^*\) is small.
The exact distribution of this statistic is complex
and there are a few special cases stated in
Table 6.3 of Applied Multivariate Statistics (6th).
But, if \(n\) is large, we have the following approximation

\begin{equation*}
-\left(n-1-\frac{p+g}{2}\right)\ln \Lambda^*
\approx \chi^2_{p(g-1)}
\end{equation*}

and we reject if the above transformation is large.

\begin{admonition-important}[{}]
This approximation is only valid if the \(n_i > 20\) and \(p,g \leq 5\).
\end{admonition-important}

\begin{admonition-note}[{}]
There are other tests such as

\begin{itemize}
\item Pillai-Bartlett's trace
\item Lawley-Hotelling trace
\item Roy's greatest root
\end{itemize}
\end{admonition-note}

\subsection{Implementing in R}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---implementing-in-r-2}

To use the MANOVA model in \(R\),
first notice that it is simply a reframed
version of a linear problem since \(X_{ij} \sim N(\mu + \tau_i, \sigma^2)\).
So, we can use the following code

\begin{listing}[{}]
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the independent and dependent variables
# each row contains a tuple (independent, dependent1, dependent2, ..dependentp)

is.factor(data$independent) #ensure that independent data is a a factor
data$independent <- as.factor(data$independent) #convert if necessary

# NOTE: the dots below must be manually filled with respective collumn names
result <- manova(cbind(dependent1, .. dependentp) ~ independent, data=data)
summary(result)
\end{listing}

We must also assess the assumptions of the model on our data

\begin{description}
\item[Normality] We can plot a qq-plot of statistical distances and use the Shapiro-Wilk test
\item[Independence] Can only be verified during collection
\item[Constant variance] We can use the \texttt{boxM} function
    
    \begin{listing}[{}]
    library(heplots)
    boxM(cbind(dependent1, .. dependentp) ~ independent, data=data)
    \end{listing}
\end{description}

\section{Two-way ANOVA (fixed effects with interaction)}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---two-way-anova-fixed-effects-with-interaction}

In the two-way ANOVA, there are two factors being tested.
Suppose that there are \(g\) levels of factor 1 and \(b\) levels
of factor 2. For each pair of factors, suppose that \(n\)
observations have been made (for a total of \(ngb\) observations).
The observations must be of the form

\begin{equation*}
X_{ijr} = \mu + \alpha_i + \beta_j + \gamma_{ij} + \varepsilon_{ijr}
\end{equation*}

where

\begin{itemize}
\item \(X_{ijr}\) is the \(r\)'th observation from the \(i\)'th level of the first
    factor and \(j\)'th level of the second factor
\item \(\mu\) is the overall mean
\item \(\alpha_i\) is the effect of the \(i\)'th level of the first factor.
    We require \(\sum_i \alpha_i = 0\)
\item \(\beta_j\) is the effect of the \(j\)'th level of the second factor
    We require \(\sum_j \beta_i = 0\)
\item \(\gamma_{ij}\) is the effect of the interaction between the \(i\)'th level
    of the first factor and the \(j\)'th level of the second factor.
        We require that the average effect per level is zero. That is
        \(\sum_i \gamma_{ik} = 0\) and \(\sum_{j} \gamma_{kj} = 0\)
        for all \(k\).
\item \(\varepsilon_{ijr} \sim N(0,\sigma^2)\) is the random error associated with \(X_{ijr}\)
\end{itemize}

Note that this implicitly requires the following assumptions

\begin{itemize}
\item Normality
\item Equal variance (Homoscedasicity)
\item Independence
\end{itemize}

There are 3 tests that can be performed

\begin{description}
\item[Effects of first factor] \(H_{01}\): \(\alpha_i=0\) for all \(i\)
\item[Effects of second factor] \(H_{02}\): \(\beta_j=0\) for all \(j\)
\item[Effects of interaction] \(H_{03}\): \(\gamma_{ij}=0\) for all \(i\) and \(j\)
\end{description}

\begin{admonition-important}[{}]
The effects of the interaction should be tested first since
a significant interaction can lead to distorted results.
\end{admonition-important}

To test these factors, we decompose as follows

\begin{equation*}
x_{ijr} =
\overbar{x}
+ (\overbar{x}_{i.} - \overbar{x})
+ (\overbar{x}_{.j} - \overbar{x})
+ (\overbar{x}_{ij} - \overbar{x}_{i.} - \overbar{x}_{.j} + \overbar{x})
+ (x_{ijr} - \overbar{x}_{ij})
\end{equation*}

The factors are

\begin{itemize}
\item \(\overbar{x}\): the overall sample mean.
    This is an estimate of \(\mu\)
\item \((\overbar{x}_{i.} - \overbar{x})\): the effect of the \(i\)'th level of the first factor.
    This is an estimate of \(\alpha_i\).
\item \((\overbar{x}_{.j} - \overbar{x})\): the effect of the \(j\)'th level of the second factor.
    This is an estimate of \(\beta_j\).
\item \((\overbar{x}_{ij} - \overbar{x}_{i.} - \overbar{x}_{.j} + \overbar{x})\):
    the effect of the interaction of the \(i\)'th level of the first factor
    and the \(j\)'th level of the second factor.
    This is an estimate of \(\gamma_{ij}\)
\item \((x_{ijr} - \overbar{x}_{ij})\): the random variation of this observation.
    This is an estimate of \(\epsilon_{ijr}\).
\end{itemize}

Then, we use the following (ANOVA) table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Source of variation} & {Sum of Squares} & {degrees of freedom} & {Mean Squares} & {F-stat} \\
\hline[\thicktableline]
{Factor 1} & {\(SSA = \sum_{i} bn(\overbar{x}_{i.} - \overbar{x})^2\)} & {\(g-1\)} & {\(MSA = \frac{SSA}{g-1}\)} & {\(FA = \frac{MSA}{MSE}\)} \\
\hline
{Factor 2} & {\(SSB = \sum_{j} gn(\overbar{x}_{.j} - \overbar{x})^2\)} & {\(b-1\)} & {\(MSB = \frac{SSB}{g-1}\)} & {\(FB = \frac{MSB}{MSE}\)} \\
\hline
{Interaction} & {\(SSAB = \sum_{ij} n(\overbar{x}_{ij} - \overbar{x}_{i.} -\overbar{x}_{.j} + \overbar{x})^2\)} & {\((g-1)(b-1)\)} & {\(MSAB = \frac{SSAB}{(g-1)(b-1)}\)} & {\(FAB = \frac{MSAB}{MSE}\)} \\
\hline
{Residuals} & {\(SSE = \sum_{i,j,r} (x_{ijr} - \overbar{x}_{ij})^2\)} & {\(gb(n-1)\)} & {\(MSE = \frac{SSE}{gb(n-1)}\)} & {} \\
\hline
{Total} & {\(SST = \sum_{i,j,r} (x_{ijr} - \overbar{x})^2\)} & {\(gbn-1\)} & {} & {} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Tests are performed on the respective F-statistics with the respective degrees of freedom.

\subsection{Implementing in R}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---implementing-in-r-3}

To use the ANOVA model in \(R\),
we use the following code

\begin{listing}[{}]
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the independent and dependent variables
# each row contains a pair (FA, FB, dependent)

is.factor(data$FA) #ensure that independent data is a factor
is.factor(data$FB) #ensure that independent data is a factor
data$FA <- as.factor(data$FA) #convert if necessary
data$FB <- as.factor(data$FB) #convert if necessary

# Note that the multiplication indicates that we want interactions
result <- aov(dependent ~ FA*FB, data=data)
summary(result)
\end{listing}

We must also assess the assumptions of the model on our data

\begin{description}
\item[Normality] We can plot a qq-plot and use the Shapiro-Wilk test
\item[Independence] Can only be verified during collection
    or using ACF plots
\item[Constant variance] We can
    
    \begin{itemize}
    \item use the \texttt{bartlett.test} function
        
        \begin{listing}[{}]
        bartlett.test(dependent ~ FA*FB, data=data)
        \end{listing}
    \item plot boxplots for each of the levels of both factors
        
        \begin{listing}[{}]
        boxplot(dependent ~ FA*FB, data=data)
        \end{listing}
    \item Plot a scatter plot of the residuals
        
        \begin{listing}[{}]
        plot(result$fit, result$res)
        \end{listing}
    \end{itemize}
\end{description}

\section{Two-way MANOVA (fixed effects with interaction)}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---two-way-manova-fixed-effects-with-interaction}

The MANOVA model is simply the multivariate extension to the ANOVA model.
The key differences to the model are

\begin{itemize}
\item Each of the observations has \(p\) variables
\item The random errors are \(\vec{\varepsilon}_{ij} \sim N(\vec{0}, \mat{\Sigma})\)
\end{itemize}

Then, we use the following (MANOVA) table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Source of variation} & {Sum of Squares} & {degrees of freedom} \\
\hline[\thicktableline]
{Factor 1} & {\(\mat{SSA} = \sum_{i} bn(\vec{\overbar{x}}_{i.} - \vec{\overbar{x}})(\vec{\overbar{x}}_{i.} - \vec{\overbar{x}})^T\)} & {\(g-1\)} \\
\hline
{Factor 2} & {\(\mat{SSB} = \sum_{j} gn(\vec{\overbar{x}}_{.j} - \vec{\overbar{x}})(\vec{\overbar{x}}_{.j} - \vec{\overbar{x}})^T\)} & {\(b-1\)} \\
\hline
{Interaction} & {\(\mat{SSAB} = \sum_{ij} n(\vec{\overbar{x}}_{ij} - \vec{\overbar{x}}_{i.} -\vec{\overbar{x}}_{.j} + \vec{\overbar{x}})(\vec{\overbar{x}}_{ij} - \vec{\overbar{x}}_{i.} -\vec{\overbar{x}}_{.j} + \vec{\overbar{x}})^T\)} & {\((g-1)(b-1)\)} \\
\hline
{Residuals} & {\(\mat{SSE} = \sum_{i,j,r} (\vec{x}_{ijr} - \vec{\overbar{x}}_{ij})(\vec{x}_{ijr} - \vec{\overbar{x}}_{ij})^T\)} & {\(gb(n-1)\)} \\
\hline
{Total} & {\(\mat{SST} = \sum_{i,j,r} (\vec{x}_{ijr} - \vec{\overbar{x}})(\vec{x}_{ijr} - \vec{\overbar{x}})^T\)} & {\(gbn-1\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Note that the sum of squares values have Wishart distribution with the respective degrees of freedom
and covariance matrix \(\Sigma\).

Each of the hypotheses have different test statistics

\begin{description}
\item[Interaction effects] 
    
    We test \(H_0: \vec{\gamma}_{ij} = 0\) for all \(i,j\).
    Consider the ratio
    
    \begin{equation*}
    \Lambda^* = \frac{|\mat{SSE}|}{|\mat{SSAB} + \mat{SSE}|}
    \end{equation*}
    
    Then
    
    \begin{equation*}
    -\left[gb(n-1) - \frac{p+1 - (g-1)(b-1)}{2}\right] \ln \Lambda^*
    \approx \chi^2_{(g-1)(b-1)p}
    \end{equation*}
    
    and we reject if the above is large.
\item[Factor 1 effects] 
    
    We test \(H_0: \vec{\alpha}_{i} = 0\) for all \(i\).
    Consider the ratio
    
    \begin{equation*}
    \Lambda^* = \frac{|\mat{SSE}|}{|\mat{SSA} + \mat{SSE}|}
    \end{equation*}
    
    Then
    
    \begin{equation*}
    -\left[gb(n-1) - \frac{p+1 - (g-1)}{2}\right] \ln \Lambda^*
    \approx \chi^2_{(g-1)p}
    \end{equation*}
    
    and we reject if the above is large.
\item[Factor 2 effects] 
    
    We test \(H_0: \vec{\beta}_{j} = 0\) for all \(j\).
    Consider the ratio
    
    \begin{equation*}
    \Lambda^* = \frac{|\mat{SSE}|}{|\mat{SSB} + \mat{SSE}|}
    \end{equation*}
    
    Then
    
    \begin{equation*}
    -\left[gb(n-1) - \frac{p+1 - (b-1)}{2}\right] \ln \Lambda^*
    \approx \chi^2_{(b-1)p}
    \end{equation*}
    
    and we reject if the above is large.
\end{description}

\begin{admonition-tip}[{}]
Note that if \(\mat{H} \sim W_h(\cdot, \mat{\Sigma})\)
and \(\mat{E}\sim W_e(\cdot, \mat{\Sigma})\) and

\begin{equation*}
\Lambda^* = \frac{|\mat{E}|}{|\mat{H}+\mat{E}|}
\end{equation*}

Then,

\begin{equation*}
-\left[e - \frac{p+1-h}{2}\right]\ln \Lambda^* \approx \chi^2_{ph}
\end{equation*}

See also \url{https://en.wikipedia.org/wiki/Wilks\%27s\_lambda\_distribution\#Approximations}
\end{admonition-tip}

\subsection{Implementing in R}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---implementing-in-r-4}

To use the MANOVA model in \(R\),
first notice that it is simply a reframed
version of a linear problem since \(X_{ij} \sim N(\mu + \tau_i, \sigma^2)\).
So, we can use the following code

\begin{listing}[{}]
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the independent and dependent variables
# each row contains a pair (FA, FB, dependent1, ..dependentp)

is.factor(data$FA) #ensure that independent data is a factor
is.factor(data$FB) #ensure that independent data is a factor
data$FA <- as.factor(data$FA) #convert if necessary
data$FB <- as.factor(data$FB) #convert if necessary

# NOTE: the dots below must be manually filled with respective collumn names
result <- manova(cbind(dependent1, .. dependentp) ~ FA*FB, data=data)
summary(result)
\end{listing}

We must also assess the assumptions of the model on our data

\begin{description}
\item[Normality] We can plot a qq-plot of statistical distances and use the Shapiro-Wilk test
\item[Independence] Can only be verified during collection
\item[Constant variance] We can use the \texttt{boxM} function
    
    \begin{listing}[{}]
    library(heplots)
    boxM(cbind(dependent1, .. dependentp) ~ FA*FB, data=data)
    \end{listing}
\end{description}

\section{Post-hoc analysis}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---post-hoc-analysis}

If the ANOVA rejects the null hypothesis, it is important to know why

\subsection{Tukey-Kramer}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---tukey-kramer}

\begin{admonition-todo}[{}]
See old nodes lecture 5
\end{admonition-todo}

\subsection{HSD}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---hsd}

\subsection{Simultaneous confidence intervals for one-way}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---simultaneous-confidence-intervals-for-one-way}

The simultaneous confidence intervals can be used
to determine which effects differ.
Note that in the multivariate case, we also care about variable cause the difference.

We want to compare \(\tau_{ki}\) and \(\tau_{li}\),
the means of the \(i\)'th component in the \(k\)'th and \(l\)'th groups.
The confidence interval for \(\tau_{ki} - \tau_{li}\) is

\begin{equation*}
(\overbar{x}_{ki} - \overbar{x}_{li})
\pm
t_{n-g}\left(\frac{\alpha}{pg(g-1)}\right)
\sqrt{\frac{w_{ii}}{n-g}\left(\frac{1}{n_k} + \frac{1}{n_l}\right)}
\end{equation*}

Since

\begin{itemize}
\item \(Var(\overbar{X}_{ki}- \overbar{X}_{li}) =\left(\frac{1}{n_k} + \frac{1}{n_l}\right)\sigma_{ii}\)
\item \(\frac{1}{n-g}\mat{W}\) is the pooled covariance matrix
\item The Bonferroni method is used to obtain the factor \(pg(g-1)\), which is the number of pairs
    which this test checks.
\end{itemize}

\subsection{Simultaneous confidence intervals for two-way}
\label{develop--stats:ROOT:page--hypothesis-tests/anova.adoc---simultaneous-confidence-intervals-for-two-way}

The simultaneous confidence intervals can be used
to determine which effects differ.
Note that in the multivariate case, we also care about variable cause the difference.

\begin{description}
\item[Factor 1] 
    
    We want to compare \(\alpha_{ki}\) and \(\alpha_{li}\),
    the means of the \(i\)'th component in the \(k\)'th and \(l\)'th levels
    of factor 1.
    The confidence interval for \(\alpha_{ki} - \alpha_{li}\) is
    
    \begin{equation*}
    (\overbar{x}_{k.i} - \overbar{x}_{l.i})
    \pm
    t_{\nu}\left(\frac{\alpha}{pg(g-1)}\right)
    \sqrt{\frac{e_{ii}}{\nu}\left(\frac{2}{bn}\right)}
    \end{equation*}
    
    where \(\nu = gb(n-1)\).
    The above is a confidence interval since
    
    \begin{itemize}
    \item \(Var(\overbar{X}_{k.i}- \overbar{X}_{l.i}) =\left(\frac{1}{bn} + \frac{1}{bn}\right)\sigma_{ii}\)
    \item \(\frac{1}{\nu}\mat{SSE}\) is the pooled covariance matrix
        which is Wishart with \(\nu\) degrees of freedom
    \item The Bonferroni method is used to obtain the factor \(pg(g-1)\), which is the number of pairs
        which this test checks.
    \end{itemize}
\item[Factor 2] 
    
    We want to compare \(\beta_{ki}\) and \(\beta_{li}\),
    the means of the \(i\)'th component in the \(k\)'th and \(l\)'th levels
    of factor 2.
    The confidence interval for \(\beta_{ki} - \beta_{li}\) is
    
    \begin{equation*}
    (\overbar{x}_{.ki} - \overbar{x}_{.li})
    \pm
    t_{\nu}\left(\frac{\alpha}{pb(b-1)}\right)
    \sqrt{\frac{e_{ii}}{\nu}\left(\frac{2}{gn}\right)}
    \end{equation*}
    
    where \(\nu = gb(n-1)\).
    The above is a confidence interval since
    
    \begin{itemize}
    \item \(Var(\overbar{X}_{.ki}- \overbar{X}_{.li}) =\left(\frac{1}{gn} + \frac{1}{gn}\right)\sigma_{ii}\)
    \item \(\frac{1}{\nu}\mat{SSE}\) is the pooled covariance matrix
        which is Wishart with \(\nu\) degrees of freedom
    \item The Bonferroni method is used to obtain the factor \(pb(b-1)\), which is the number of pairs
        which this test checks.
    \end{itemize}
\end{description}
\end{document}
