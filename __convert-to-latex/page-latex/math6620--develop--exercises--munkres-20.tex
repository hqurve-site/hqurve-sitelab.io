\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{The Metric Topology Exercises}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
These come from Section 20 of \href{http://www.alefenu.com/libri/topologymunkres.pdf}{Topology by James Munkres}.

\section{Question 4}
\label{develop--math6620:exercises:page--munkres-20.adoc---question-4}

Consider the product, uniform and box topologies on \(\mathbb{R}^\omega\).

\subsection{Part a}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-a}

In which topologies are the following functions from \(\mathbb{R}\) to
\(\mathbb{R}^\omega\) continuous?

\begin{enumerate}[label=\arabic*)]
\item \(f(t) = (t, 2t, 3t, \ldots)\)
\item \(g(t) = (t,t,t,\ldots)\)
\item \(h(t) = \left(t, \frac12 t, \frac13 t, \ldots\right)\)
\end{enumerate}

\subsubsection{Product topology}
\label{develop--math6620:exercises:page--munkres-20.adoc---product-topology}

Consider the arbitrary open basis element \(U = (a_1, b_1) \times (a_n, b_n) \times \mathbb{R}\times\cdots \).

Note that \(t \in f^{-1}(U)\) iff \(t \in \left(\frac{a_i}{i}, \frac{b_i}{i}\right)\)
for \(i=1\) to \(n\). This forms a finite intersection of open intervals.
Therefore \(f^{-1}(U)\) is an open interval in \(\mathbb{R}\)
and \(f\) is continuous.

For similar reasons, \(g\) and \(h\) are also continuous.

\subsubsection{Uniform topology}
\label{develop--math6620:exercises:page--munkres-20.adoc---uniform-topology}

Consider arbitrary open ball \(U = B_{\overbar{\rho}}(\vec{x}, \varepsilon)\)
in the uniform topology.

Note that \(t \in f^{-1}(U)\) iff \(t \in \left(\frac{x_i - \delta}{i}, \frac{x_i + \delta}{i}\right)\)
for each \(i\) and for some \(\delta < \varepsilon\). This is an infinite intersection and may not be open in \(\mathbb{R}\).
Consider the case where \(\vec{x} = \vec{0}\) and \(\varepsilon = \frac{1}{2}\).
Then, \(t \in \left(-\frac{\delta}{i}, \frac{\delta}{i}\right)\) for each \(i\)
for some \(\delta < \frac12\).
The only value which satisfies this is \(t=0\). Therefore,
\(f^{-1}\left(B\left(\vec{x}, \frac12\right)\right) = \{0\}\)  which is not open.
Therefore \(f\) is not continuous.

We aim to show that \(g^{-1}(U)\) is open.
Consider arbitrary \(t_0 \in g^{-1}(U)\).
Then, there exists a \(\delta < \varepsilon\) such that

\begin{equation*}
\overbar{\rho}(\vec{x}, g(t_0)) = \sup_{i} |x_i - t_0| \leq \delta < \varepsilon
\end{equation*}

Consider \(V = B\left(t_0, \frac{\varepsilon -\delta}{2}\right)\).
Then, for any \(t' \in V\),

\begin{equation*}
\overbar{\rho}(\vec{x}, g(t'))
= \sup_i |x_i - t'|
\leq \sup_i |x_i - t_0| + |t_0 -t'|
\leq \delta + \frac{\varepsilon - \delta}{2}
\implies t' \in g^{-1}(U)
\end{equation*}

Therefore, \(t_0  \in V \subseteq g^{-1}(U)\).
Since the choice of \(t_0\) and \(\vec{x}\) were arbitrary,
\(g\) is continuous.

Finally, we also aim to show that \(h^{-1}(U)\) is also open.
Consider arbitrary \(t_0 \in h^{-1}(U)\).
Then, there exists a \(\delta < \varepsilon\) such that

\begin{equation*}
\overbar{\rho}(\vec{x}, h(t_0)) = \sup_i \left|x_i - \frac{t_0}{i}\right| \leq \delta < \varepsilon
\end{equation*}

Consider \(V = B\left(t_0, \frac{\varepsilon - \delta}{2}\right)\).
Then, for any \(t' \in V\)

\begin{equation*}
\overbar{\rho}(\vec{x}, h(t'))
= \sup_i \left|x_i - \frac{t'}{i}\right|
\leq \sup_i \left|x_i - \frac{t_0}{i}\right| + \sup_i \left|\frac{t_0-t'}{i}\right|
\leq \delta + \frac{\varepsilon - \delta}{2}
< \varepsilon
\implies t' \in h^{-1}(U)
\end{equation*}

Therefore, \(t_0  \in V \subseteq h^{-1}(U)\).
Since the choice of \(t_0\) and \(\vec{x}\) were arbitrary,
\(h\) is continuous.

\subsubsection{Box topology}
\label{develop--math6620:exercises:page--munkres-20.adoc---box-topology}

Since the uniform topology is coarser than the box topology,
we only need to check whether \(g\) and \(h\) are continuous.

We would show that \(g\) is not continuous. Consider
the open set \(U = \prod_i\left(-\frac{1}{i}, \frac{1}{i}\right)\).
Then, \(g(t) \in U\) iff \(t = \{0\}\).
Since \(g^{-1}(U) = \{0\}\) is not open, \(g\) is not continuous.

We can similarly show that \(h\) is not continuous.
Consider the open set \(U = \prod_i\left(-\frac{1}{i^2}, \frac{1}{i^2}\right)\).
Then, \(h(t) \in U\) iff for each \(i\)

\begin{equation*}
\frac{t}{i} \in \left(-\frac{1}{i^2}, \frac{1}{i^2}\right)
\implies t \in \left(-\frac{1}{i}, \frac{1}{i}\right)
\end{equation*}

Therefore \(t = 0\).
Since \(h^{-1}(U) = \{0\}\) is not open, \(h\) is not continuous.

\subsection{Part b}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-b}

In which topologies do the following sequences converge?

Recall

\begin{itemize}
\item a sequence \(\{x_n\}\) converges iff for each open set \(U\) containing the limit \(x\),
    there is an \(N\) such that each \(n \geq N\), \(x_n \in U\).
\item since each of the topologies on \(\mathbb{R}^\omega\) are Hausdorff,
    there is at most one limit point.
\item the box topology is finer than the uniform topology
    which is finer than the product topology. Therefore the limit in the three topologies
    must be the same if they exist
\end{itemize}

\subsubsection{Part i}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-i}

\begin{equation*}
\begin{aligned}
\vec{w}_1 &= (1, 1, 1, 1, \ldots)\\
\vec{w}_2 &= (0, 2, 2, 2, \ldots)\\
\vec{w}_3 &= (0, 0, 3, 3, \ldots)\\
\end{aligned}
\end{equation*}

We claim that \(\vec{0}\) is the limit in the product topology.
This is true since for any open \(U\subseteq \mathbb{R}^\omega\) containing \(\vec{0}\),
there must be an \(N\) for which each \(n \geq N\) component
is \(\mathbb{R}\). Therefore each \(\vec{w}_n\) with \(n \geq N\)
must also be in \(U\).

Note that \(\vec{0}\) is not the limit in the uniform topology since
for an \(B(\vec{0}, \varepsilon)\), there exists a \(N > \varepsilon\)
such that each \(n\geq N\), \(\vec{w}_n \notin B(\vec{0}, \varepsilon)\) (since
\(\overbar{\rho}(\vec{w}_n, \vec{0}) = n \geq N > \varepsilon\).
Therefore the limit does not exist in the box and uniform topologies.

\subsubsection{Part ii}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-ii}

\begin{equation*}
\begin{aligned}
\vec{x}_1 &= (1, 1, 1, 1, \ldots)\\
\vec{x}_2 &= \left(0, \frac12, \frac12, \frac12, \ldots\right)\\
\vec{x}_3 &= \left(0, 0, \frac13, \frac13, \ldots\right)\\
\end{aligned}
\end{equation*}

We claim that \(\vec{0}\) is the limit in the uniform topology.
To see this consider arbitrary \(\varepsilon > 0\).
Then, there exists \(N \geq 1\) such that \(\frac{1}{N} < \varepsilon\).
Then, \(\rho(\vec{x}_n, \vec{0}) = \frac{1}{n} \leq \frac{1}{N} < \varepsilon\)
for each \(n \geq N\).
Therefore \(\vec{0}\) is the limit in the uniform and product topologies.

To show that \(\vec{0}\) is not the limit in the box topology consider
the product of \(U_i = \left(-\frac{1}{i}, \frac{1}{i}\right)\).
Then, for any \(n \geq 1\), \(\vec{x}_n \notin \prod U_i\) since there exists
an \(i \geq 1\) such that \(\frac{1}{i} < \frac{1}{n}\).
Therefore the limit does not exist in the box topology.

\subsubsection{Part iii}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-iii}

\begin{equation*}
\begin{aligned}
\vec{y}_1 &= (1, 0, 0, 0, \ldots)\\
\vec{y}_2 &= \left(\frac12, \frac12, 0, 0, \ldots\right)\\
\vec{y}_3 &= \left(\frac13, \frac13, \frac13, 0,\ldots\right)\\
\end{aligned}
\end{equation*}

The limit is \(\vec{0}\) in the product and uniform topologies.
The reasoning is the same as in part ii.

To show that \(\vec{0}\) is not the limit in the box topology,
consider the product of \(U_i = \left(-\frac{1}{i^2}, \frac{1}{i^2}\right)\).
Then, for any \(n \geq 2\), consider the \(n\)'th component of \(\vec{y}_n\).
This is \(\frac{1}{n}\) which is to be in \(U_n\).
However, \(\frac{1}{n} > \frac{1}{n^2}\). So \(\vec{y}_n \notin \prod U_i\).
Therefore the limit does not exist in the box topology.

\subsubsection{Part iv}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-iv}

\begin{equation*}
\begin{aligned}
\vec{y}_1 &= (1, 1, 0, 0, \ldots)\\
\vec{y}_2 &= \left(\frac12, \frac12, 0, 0, \ldots\right)\\
\vec{y}_3 &= \left(\frac13, \frac13, 0, 0,\ldots\right)\\
\end{aligned}
\end{equation*}

Consider arbitrary open \(U_1\times U_2, \times \cdots\) in the box topology containing \(\vec{0}\).
Then, \(U_1 = (a_1, b_1)\) and \(U_2 = (a_2, b_2)\).
There exists a \(N \geq 1\) such that \(\frac{1}{N} < \min(b_1, b_2)\).
Then, for any \(\vec{y}_n\) with \(n \geq N\), \(\vec{y}_n \in \prod U_i\).
Therefore the limit is \(\vec{0}\) in all three topologies.

\section{Question 6}
\label{develop--math6620:exercises:page--munkres-20.adoc---question-6}

Let \(\overbar{\rho}\) be the uniform metric on \(\mathbb{R}^\omega\).
Given \(\vec{x} \in \mathbb{R}^\omega\) and \(0 < \varepsilon < 1\).
Let

\begin{equation*}
U(\vec{x}, \varepsilon) = \prod_i (x_i - \varepsilon, x_i + \varepsilon)
\end{equation*}

\subsection{Part a}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-a-2}

Show that \(U(\vec{x}, \varepsilon)\) is not equal to \(B_{\overbar{\rho}}(\vec{x}, \varepsilon)\).

This simple. Consider \(\vec{y}\) defined by \(y_i = x_i + \varepsilon\left(1 - \vec{1}{i}\right)\).
Then, \(\vec{y} \in U(\vec{x}, \varepsilon)\); however
\(\overbar{\rho}(\vec{x}, \vec{y}) = \varepsilon \sup_i\left(1 - \vec{1}{i}\right) = \varepsilon\)
which implies that \(\vec{y} \notin B_{\overbar{\rho}}(\vec{x}, \varepsilon)\).

\subsection{Part b}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-b-2}

Show that \(U(\vec{x}, \varepsilon)\) is not open in the uniform topology.

Consider \(\vec{y} \in U(\vec{x}, \varepsilon)\) defined by \(y_i = x_i + \varepsilon\left(1 - \vec{1}{i}\right)\).
We want to show that for any \(\delta > 0\) the following is false
\(B_{\overbar{\rho}}(\vec{y}, \delta) \subseteq U(\vec{x}, \varepsilon)\).
Note that since \(\delta > 0\), there exists \(j \geq 0\) such that
\(\frac{\varepsilon}{j} < \delta\). Then,
\(\vec{z} = \vec{y} + \frac{\varepsilon}{j}\vec{1} \in B_{\overbar{\rho}}(\vec{y}, \delta)\)
since

\begin{equation*}
\overbar{\rho}(\vec{z}, \vec{y}) = \sup_i \frac{\varepsilon}{j}1 =\frac{\varepsilon}{j} < \delta
\end{equation*}

However,

\begin{equation*}
z_j = y_j + \frac{\varepsilon}{j} = x_j + \varepsilon \implies \vec{z} \notin U(\vec{x}, \varepsilon)
\end{equation*}

Therefore, there exists no \(\delta > 0\) such that \(B_{\overbar{\rho}}(\vec{y}, \delta) \subseteq U(\vec{x}, \varepsilon)\)
and \(\vec{y} \notin \interior U(\vec{x}, \varepsilon)\).
Therefore, this set is not open in the uniform topology.

\subsection{Part c}
\label{develop--math6620:exercises:page--munkres-20.adoc---part-c}

Show that

\begin{equation*}
B_{\overbar{\rho}}(\vec{x}, \varepsilon) = \bigcup_{\delta < \varepsilon} U(\vec{x}, \delta)
\end{equation*}

The above is clearly true since

\begin{equation*}
\begin{aligned}
\vec{y} \in B_{\overbar{\rho}}(\vec{x}, \varepsilon)
&\iff \overbar{\rho}(\vec{x}, \vec{y}) < \varepsilon
\\&\iff \exists \delta < \varepsilon: \overbar{\rho}(\vec{x}, \vec{y}) < \delta
\\&\iff \exists \delta < \varepsilon: \vec{y} \in B_{\overbar{\rho}}(\vec{x}, \delta)
\end{aligned}
\end{equation*}
\end{document}
