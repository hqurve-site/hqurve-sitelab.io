\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Stirling Numbers}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\{{#1}\right\}}
\DeclareMathOperator{\sign}{sign}

% Title omitted
Consider the falling factorial \((t)_n\) where \(t\) is indeterminate. Then,
we define the \emph{stirling number of the first kind}, \(s(n, k)\), to be

\begin{equation*}
(t)_n = \sum_{k=1}^n s(n, k)t^k
\end{equation*}

and we define the \emph{stirling number of the second kind}, \(S(n, k)\),
to be

\begin{equation*}
t^n = \sum_{k=1}^n S(n, k)(t)_k
\end{equation*}

Although it may not be immediately apparent, \(S(n, k)\) is well defined
and an integer
since \(t^1 = (t)_1\) and inductively we can represent \(t^n\)
as \((t)_n\) plus the sum of lower powers.

\section{Recurrences}
\label{develop--math3610:generating-functions:page--stirling.adoc---recurrences}

Since \((t)_{n+1} = (t-n)(t)_n\), we get that

\begin{itemize}
\item \(s(n+1, k) = s(n,k-1) - ns(n, k)\)
\item \(S(n+1,k) = S(n, k-1) + kS(n,k)\)
\end{itemize}

where we take \(S(n, k) = s(n, k) = 0\) if \(k \leq 0\) or \(k > n\).

\begin{example}[{Proof}]
\begin{example}[{Proof of 1}]
\begin{equation*}
\begin{aligned}
(t)_{n+1}
&= (t-n)(t)_n
\\&= (t-n)\sum_{k=1}^n s(n, k)t^k
\\&= \sum_{k=1}^n s(n, k)t^{k+1} - \sum_{k=1}^n ns(n, k)t^k
\\&= s(n, n)t^{n+1} - ns(n,1)t + \sum_{k=1}^{n-1} s(n, k)t^{k+1} - \sum_{k=2}^n ns(n, k)t^k
\\&= s(n, n)t^{n+1} - ns(n,1)t + \sum_{k=2}^{n} s(n, k-1)t^{k} - \sum_{k=2}^n ns(n, k)t^k
\\&= s(n, n)t^{n+1} - ns(n,1)t + \sum_{k=2}^{n} [s(n, k-1) - ns(n,k)]t^{k}
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
s(n+1,k)
= \begin{cases}
(-n)s(n,1) &\quad\text{ if } k=1\\
s(n, k-1) -ns(n,k) &\quad\text{ if } 2 \leq k \leq n\\
s(n,n) &\quad\text{ if } k=n+1\\
\end{cases}
\end{equation*}
\end{example}

\begin{example}[{Proof of 2}]
\begin{equation*}
\begin{aligned}
t^{n+1}
&= tt^n
\\&= t\sum_{k=1}^n S(n, k)(t)_k
\\&= \sum_{k=1}^n S(n, k)[t(t)_k]
\\&= \sum_{k=1}^n S(n, k)[(t-k)(t)_k + k (t)_k]
\\&= \sum_{k=1}^n S(n, k)[(t)_{k+1} + k (t)_k]
\\&= \sum_{k=1}^n S(n, k)(t)_{k+1} + \sum_{k=1}^n S(n, k)k (t)_k
\\&= S(n, n)(t)_{n+1} + 1S(n,1)(t)_1 + \sum_{k=1}^{n-1} S(n, k)(t)_{k+1} + \sum_{k=2}^n S(n, k)k (t)_k
\\&= S(n, n)(t)_{n+1} + S(n,1)(t)_1 + \sum_{k=2}^{n} S(n, k-1)(t)_{k} + \sum_{k=2}^n S(n, k)k (t)_k
\\&= S(n, n)(t)_{n+1} + S(n,1)(t)_1 + \sum_{k=2}^{n} [S(n, k-1) + kS(n, k)](t)_{k}
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
S(n+1,k)
= \begin{cases}
S(n,1) &\quad\text{ if } k=1\\
S(n, k-1) + kS(n,k) &\quad\text{ if } 2 \leq k \leq n\\
S(n,n) &\quad\text{ if } k=n+1\\
\end{cases}
\end{equation*}
\end{example}
\end{example}

\section{Signs}
\label{develop--math3610:generating-functions:page--stirling.adoc---signs}

The sign of stirling numbers of the first kind is alternating.
Notice

\begin{equation*}
(-1)^n\prod_{k=1}^n (t+k) = \prod_{k=1}^n (-t-k) = (-t)_n = \sum_{k=1}^n s(n, k)(-t)^k
\end{equation*}

Then, on the left hand side, the coefficient of \(t^k\) has sign \((-1)^n\)
while on the left, this sign is \(\sign(s(n, k))(-1)^k\). Therefore, we
get that \(\sign(s(n,k)) = (-1)^{n-k}\).

On the other hand, the sign of stirling numbers of the second kind is always positive.
This is easily seen from the recurrence relation along with the fact that \(S(1,1) = 1\).

\section{Cool}
\label{develop--math3610:generating-functions:page--stirling.adoc---cool}

By combining the first and second formulae, we get that

\begin{equation*}
t^n
= \sum_{k=1}^n S(n, k)(t)_k
= \sum_{k=1}^n S(n, k)\sum_{m=1}^k s(k,m) t^m
= \sum_{m=1}^n \sum_{k=m}^n S(n, k)s(k,m) t^m
\end{equation*}

and by comparing coefficients

\begin{equation*}
\sum_{k=m}^n S(n, k)s(k,m)
= \begin{cases}
1 \quad\text{if } m = n\\
0 \quad\text{otherwise}
\end{cases}
\end{equation*}

Therefore, if we have two sequences \(\{a_n\}\) and \(\{b_n\}\), we get that

\begin{equation*}
a_n = \sum_{k=1}^n s(n, k)b_k
\quad\iff\quad
b_n = \sum_{k=1}^n S(n, k)a_k
\end{equation*}

This is easily obtained by direct substitution.
\end{document}
