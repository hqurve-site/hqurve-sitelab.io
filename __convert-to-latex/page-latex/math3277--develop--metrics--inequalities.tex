\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Inequalities}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
This page contains several very important inequalities which are useful when
showing that a function is a valid metric.

\section{Cauchy-Schwarz Inequality}
\label{develop--math3277:metrics:page--inequalities.adoc---cauchy-schwarz-inequality}

If \(\vec{a} = (a_1,\ldots a_n), \vec{b}=(b_1,\ldots b_n) \in \mathbb{R}^n\), then

\begin{equation*}
\sum_{i=1}^n a_ib_i \leq \sqrt{\sum_{i=1}^n a_i^2} \sqrt{\sum_{i=1}^n b_i^2 }
\end{equation*}

where equality occurs iff \(\vec{a} = \lambda\vec{b}\) or \(\vec{b} = \vec{0}\).

This is a result from linear algebra,
\myautoref[{see here}]{develop--math2273:ROOT:page--inner-products.adoc---cauchy-schwarz-inequality} for explaination and
proof.

\subsection{Generalized Cauchy-Schwarz Inequality}
\label{develop--math3277:metrics:page--inequalities.adoc---generalized-cauchy-schwarz-inequality}

\begin{admonition-remark}[{}]
I'm almost certain that if I look up this inequality, it would be named after someone.
However, where is the fun in that?. This inequality is sufficient for the triangle
inequality in \((\mathbb{R}^n, d_k)\), and is a natural extension the
Cauchy-Schwarz.
\end{admonition-remark}

Firstly, we denote \(\mathbb{R}^* = \mathbb{R}^+ \cup \{0\}\). Now,
let
\(\vec{a}_j = (a_{1j}, a_{2j},\ldots a_{nj}) \in (\mathbb{R}^*)^n\)
for \(j=1,2,\ldots k\), then

\begin{equation*}
\left(\sum_{i=1}^n \prod_{j=1}^k a_{ij}\right)^k \leq \prod_{j=1}^k \sum_{i=1}^n a_{ij}^k
\end{equation*}

Where equality holds iff one of the following

\begin{itemize}
\item \(k = 1\)
\item one of the \(\vec{a}_j = \vec{0}\)
\item \(\exists \vec{b} \in (\mathbb{R}^*)\) and \(c_1, \ldots c_k \in \mathbb{R}^+\) such that \(\vec{a}_j = c_j \vec{b}\)
\end{itemize}

\begin{admonition-note}[{}]
When \(k=1\), the last statement is trivially true.
\end{admonition-note}

\begin{admonition-note}[{}]
If \(k\) is even, this result can be extended to all real vectors using \(x \leq |x|\), however, the condition for equality is a little.... complex,
but still obvious.
\end{admonition-note}

\begin{example}[{Proof}]
\begin{admonition-thought}[{}]
The approach of this proof follows that of the generalized mean inequality on wikipedia (if I remember correctly).
That is, we would prove for an arbitrary power of \(2\) and show that if it holds for a value of \(k\), it holds
for \(k-1\).
I would not be surprised if there is a more organic way of proving this result, however, I am unable to synthesize such a proof.
\end{admonition-thought}

\begin{admonition-remark}[{}]
Proof for sufficiency for equality would not be given since its easily observed.
\end{admonition-remark}

\begin{example}[{Proof for powers of \(2\)}]
Firstly, we would show that the result holds for any power of \(2\).
We would do this inductively.

The base case
is true as it is just the usual Cauchy-Schwarz inequality where equality
holds iff \(\exists \vec{b} \in \mathbb{R}^n\) and \(c_1, c_2 \in \mathbb{R}\)
such that \(\vec{a}_1 = c_1\vec{b}, \vec{a}_2 = c_2\vec{b}\). Now,
if one of \(c_1, c_2 =0\) or \(\vec{b} = 0\), it means that one of the \(\vec{a}_j = \vec{0}\).
Instead suppose that neither of \(c_1, c_2 =0\) and \(\vec{b} \neq \vec{0}\).
If all of the entries in \(\vec{b}\) are non-positive, we are done. Otherwise,
if any of the entries in \(\vec{b}\) are negative, \(c_1, c_2\) must be negative
and hence all other entries in \(\vec{b}\) must be non-positive and instead
we take \(-c_1, -c_2\) and \(-\vec{b}\).

Next, suppose that it holds for some \(2^k\), then

\begin{equation*}
\begin{aligned}
\left(\sum_{i=1}^n \prod_{j=1}^{2^{k+1}} a_{ij}\right)^{2^{k+1}}
&= \left(\sum_{i=1}^n \prod_{j=1}^{2^{k}} a_{i(2j-1)}a_{i(2j)}\right)^{2^{k+1}}
\\&\leq \left(\prod_{j=1}^{2^{k}} \sum_{j=1}^n a_{i(2j-1)}^{2^k}a_{i(2j)}^{2^k}\right)^{2}
    \quad\text{by generalized Cauchy-Schwarz with }2^k
\\&= \prod_{j=1}^{2^{k}} \left(\sum_{j=1}^n a_{i(2j-1)}^{2^k}a_{i(2j)}^{2^k}\right)^{2}
\\&\leq \prod_{j=1}^{2^{k}} \left(\sum_{j=1}^n (a_{i(2j-1)}^{2^k})^2\sum_{j=1}^n (a_{i(2j)}^{2^k})^2\right)
    \quad\text{by Cauchy-Schwarz}
\\&= \prod_{j=1}^{2^{k}} \sum_{j=1}^n a_{i(2j-1)}^{2^{k+1}}\sum_{j=1}^n a_{i(2j)}^{2^{k+1}}
\\&= \prod_{j=1}^{2^{k+1}} \sum_{j=1}^n a_{ij}^{2^{k+1}}
\end{aligned}
\end{equation*}

Now, for the first inequality, equality holds iff
\(\exists \vec{v} = (v_1, \ldots v_n) \in (\mathbb{R}^*)^n\) and \(d_1,\ldots d_{2^k} \in \mathbb{R}^+\)
such that

\begin{equation*}
(a_{1(2j-1)}a_{1(2j)}, \ldots a_{n(2j-1)}a_{n(2j)}) = d_j \vec{v} = (d_jv_1, \ldots d_jv_n)
\end{equation*}

That is, \(a_{i(2j-1)}a_{i(2j)} = d_j v_i\) for all \(i=1\ldots n\) and \(j = 1\dots 2^k\).

And the second equality holds iff \(\exists \vec{u}_j = (u_{1j}, \ldots u_{nj}) \in \mathbb{R}^n\) and \(e_j, f_j \)
for \(j = 1\ldots 2^k\) such that

\begin{equation*}
(a_{1(2j-1)}^{2^k}, \ldots a_{n(2j-1)}^{2^k}) = e_j \vec{u}_{j} = (e_ju_{1j}, \ldots e_ju_{nj})
\quad\text{and}\quad
(a_{1(2j)}^{2^k}, \ldots a_{n(2j)}^{2^k}) = f_j \vec{u}_{j} = (f_ju_{1j}, \ldots f_ju_{nj})
\end{equation*}

That is, \(a_{i(2j-1)}^{2^k} = e_j u_{ij}\) and \(a_{i(2j)}^{2^k} = f_ju_{ij}\) for all \(i=1\ldots n\) and \(j = 1\dots 2^k\).
Then if any of the \(e_j,f_j = 0\), we get that \(\vec{a}_j = \vec{0}\).
Otherwise, note that for each \(j\), \(e_j\) and \(u_{ij}\) must either both be non-negative or non-positive;
likewise for \(f_j\) and \(u_{ij}\). Hence, WLOG all are non-negative (multiplying by \(-1\) if necessary).
Then,

\begin{equation*}
a_{i(2j-1)}^{2^k}a_{i(2j)}^{2^k} = e_jf_ju_{ij}^2 = (d_jv_i)^{2^k} \implies u_{ij} = \sqrt{\frac{(d_jv_i)^{2^k}}{e_jf_j}}
\end{equation*}

Hence

\begin{equation*}
\begin{aligned}
&a_{i(2j-1)}^{2^k}
= e_j \sqrt{\frac{(d_jv_i)^{2^k}}{e_jf_j}}
= \left(e_j \sqrt{\frac{(d_j)^{2^k}}{e_jf_j}}\right)v_i^{2^{k-1}}
\quad\text{and}\quad
a_{i(2j)}^{2^k}
= f_j \sqrt{\frac{(d_jv_i)^{2^k}}{e_jf_j}}
= \left(f_j \sqrt{\frac{(d_j)^{2^k}}{e_jf_j}}\right)v_i^{2^{k-1}}
\\
&\implies
a_{i(2j-1)}
= \left[\left(e_j \sqrt{\frac{(d_j)^{2^k}}{e_jf_j}}\right)v_i^{2^{k-1}}\right]^{1/2^k}
= \sqrt{v_i}\left[e_j \sqrt{\frac{(d_j)^{2^k}}{e_jf_j}}\right]^{1/2^k}
\\&\hspace{4em}\text{and}\
a_{i(2j)}
= \left[\left(f_j \sqrt{\frac{(d_j)^{2^k}}{e_jf_j}}\right)v_i^{2^{k-1}}\right]^{1/2^k}
= \sqrt{v_i}\left[f_j \sqrt{\frac{(d_j)^{2^k}}{e_jf_j}}\right]^{1/2^k}
\end{aligned}
\end{equation*}

Therefore, each of the \(\vec{a}_j = c_j\vec{b}\) where

\begin{equation*}
\vec{b} = (\sqrt{v_1}, \ldots \sqrt{v_n})
\end{equation*}
\end{example}

\begin{example}[{Proof for smaller values}]
Next, suppose that the generalized Cauchy-Schwarz holds for some \(k\), then we need to show that it holds for \(m < k\).

\begin{equation*}
\begin{aligned}
\left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}\right)^k
&= \left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}^{m/k} a_{ij}^{1-m/k}\right)^k
\\&= \left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}^{m/k} a_{ij}^{(k-m)/k}\right)^k
\\&= \left(\sum_{i=1}^n \left(\prod_{j=1}^m a_{ij}^{m/k})\right) \prod_{j=1}^m a_{ij}^{(k-m)/k}\right)^k
\\&= \left(\sum_{i=1}^n \left(\prod_{j=1}^m a_{ij}^{m/k})\right) \left(\prod_{j=1}^m a_{ij}^{1/k}\right)^{k-m}\right)^k
\\&\leq \left(\prod_{j=1}^m \sum_{i=1}^n (a_{ij}^{m/k})^k\right)\left(\sum_{i=1}^n \left(\prod_{j=1}^m (a_{ij}^{1/k}\right)^k\right)^{k-m}
\\&= \left(\prod_{j=1}^m \sum_{i=1}^n (a_{ij}^{m/k})^k\right)\left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}\right)^{k-m}
\end{aligned}
\end{equation*}

Now, let

\begin{equation*}
\beta_{ij} = \begin{cases}
a_{ij}^{m/k} \quad&\text{if } j \leq m\\
\prod_{j'=1}^m a_{ij'}^{1/k} \quad&\text{if } m < j < k
\end{cases}
\end{equation*}

Then,

\begin{equation*}
\begin{aligned}
\left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}\right)^k
&= \left(\sum_{i=1}^n \left(\prod_{j=1}^m a_{ij}^{m/k})\right) \left(\prod_{j=1}^m a_{ij}^{1/k}\right)^{k-m}\right)^k
\\&= \left(\sum_{i=1}^n \prod_{j=1}^k \beta_{ij}\right)^k
\\&\leq \prod_{j=1}^k \sum_{i=1}^n \beta_{ij}^k
\\&= \left(\prod_{j=1}^k \sum_{i=1}^n \beta_{ij}^k\right)\left(\prod_{j=m+1}^k \sum_{i=1}^n \beta_{ij}^k\right)
\\&= \left(\prod_{j=1}^k \sum_{i=1}^n \beta_{ij}^k\right)\left(\sum_{i=1}^n \beta_{ij}^k\right)^{k-m}
\\&= \left(\prod_{j=1}^k \sum_{i=1}^n (a_{ij}^{m/k})^k\right)\left(\sum_{i=1}^n \left(\prod_{j=1}^m a_{ij}^{1/k}\right)^k\right)^{k-m}
\\&= \left(\prod_{j=1}^k \sum_{i=1}^n a_{ij}^{m}\right)\left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}\right)^{k-m}
\end{aligned}
\end{equation*}

and by dividing both sides by \(\left(\sum_{i=1}^n \prod_{j=1}^m a_{ij}\right)^{k-m}\), we get the desired inequality.
Note that if this product was \(0\), the desired inequality trivially holds.

Now, if equality holds, then since \(k > m \geq 0\) one
of the following occurs

\begin{itemize}
\item \((\beta_{1j}, \ldots \beta_{nj}) = \vec{0}\). If \(j > m\), then both sides of the desired
    inequality is \(0\) and hence one of the summations \(\sum_{i=1}^n a_{ij'}^m = 0\)
    and hence one of the \(\vec{a}_{j'} = \vec{0}\). If \(1 \leq j \leq m\), we get the same
    result.
\item \(\exists \vec{v} \in (\mathbb{R}^*)^n\) and \(c_1\ldots c_k \in \mathbb{R}^+\) such that
    
    \begin{equation*}
    (\beta_{1j}, \ldots \beta_{nj}) = c_j \vec{v}
    \end{equation*}
    
    for \(j=1\ldots k\). Then clearly this results for \(1\ldots m\) and hence \(\vec{a}_j = (c_j)^{k/m}\vec{v}'\) where
    \(v_i' = v_i^{k/m}\).
\end{itemize}
\end{example}
\end{example}

\subsection{Integral form}
\label{develop--math3277:metrics:page--inequalities.adoc---integral-form}

Let \(f, g \in \mathcal{C}[a,b]\) then.

\begin{equation*}
\int_a^b f(x)g(x) \ dx \leq \sqrt{\int_a^b f(x)^2 \ dx}\sqrt{\int_a^b g(x) \ dx}
\end{equation*}

where equality holds iff \(f = \lambda g\) or \(g = \theta\)
Again this is a result from the fact that the above operator is a inner product
(which we would prove below)

\begin{example}[{Proof}]
Firstly, the set of continuous functions is a vectorspace. Next,
clearly the above operator satisfies non-negativity, symmetry and linearity
as the integral is linear and preserves inequalities.

For coincidence,
suppose that \(f \neq \theta\). Then, \(f^2 \neq \theta\)
and \(f^2 \in \mathcal{C}[a,b]\). Hence, \(\exists c \in [a,b]\)
such that \(f(c)^2 \neq 0\) and since \(f^2\) is continuous,
\(\exists \delta > 0\) such that

\begin{equation*}
\forall x \in [a,b] \cap N(c, \delta):
f(x) \in N\left(f(c)^2 , \frac{f(c)^2}{4}\right) = \left(\frac{f(c)^2}{2}, \frac{3f(c)^2}{2}\right)
\end{equation*}

Next, we let

\begin{equation*}
[d,e] = \overline{[a,b] \cap N(c,\delta)} \subseteq [a,b]
\end{equation*}

since a limit point of \([d,e]\) is also a limit point of \([a,b]\).
Then

\begin{equation*}
\int_a^b f(x)^2 \ dx
= \int_a^d f(x)^2 \ dx + \int_d^e f(x)^2 \ dx + \int_e^b f(x)^2 \ dx
\geq \int_d^e f(x)^2 \ dx
\geq \int_d^e \frac{f(c)^2}{2} \ dx
= \frac{f(c)^2}{2}(e-d) > 0
\end{equation*}

That is, \(f \neq \theta \implies \) the norm is positive.
\end{example}

\subsection{Generalized integral form}
\label{develop--math3277:metrics:page--inequalities.adoc---generalized-integral-form}

Let \(f_1,\ldots f_k \in \mathcal{C}[a,b]\) such that \(f_j([a,b]) \subseteq [0\infty)\).
Then,

\begin{equation*}
\left(\int_a^b \prod_{j=1}^k f_j(x) \ dx \right)^k
\leq \prod_{j=1}^k \int_a^b f_j(x)^k \ dx
\end{equation*}

where equality holds if one of the following

\begin{itemize}
\item \(k = 1\)
\item \(f_j = \theta\)
\item \(\exists g \in \mathcal{C}[a,b]\) and \(c_1, \ldots c_k \in \mathbb{R}^+\)
    such that \(g([a,b]) \subseteq [0,\infty)\) and
    \(f_j = c_j g\).
\end{itemize}

\begin{admonition-important}[{}]
Upon thinking that the summation formula is a special case of this, I realize
that we don't really need the functions to be continuous but rather just integrable.
However, in such a case, I believe that we shouldnt deal with functions
but rather equivalence classes of functions where two functions are equivalent
iff they are different at finitely many points (ie almost equal). Using this we could then
replace our integral of a function with an integral of a representative.
\end{admonition-important}

I am not going to prove this result as it is just a step by step replication
of the discrete case, albeit with some minor differences when showing the case
for equality.

\section{My nice inequality (name wip)}
\label{develop--math3277:metrics:page--inequalities.adoc---my-nice-inequality}

Let \(t \in \mathbb{R}^*\) and \(m,n \in \mathbb{Z}^+\) such that \(m < n\). Then,

\begin{equation*}
\sqrt[m]{1+t^m} \geq \sqrt[n]{1+t^n}
\end{equation*}

where equality holds iff \(t = 0\).

\begin{example}[{Proof}]
\begin{admonition-remark}[{}]
Yet again, a very very ugly proof. I didn't want to use calculus and this is the result of my choice.

\begin{example}[{}]
\begin{figure}[H]\centering
\includegraphics[draft,width=8cm,height=5cm]{placeholder.png}
\caption{ (omitted external image: \url{https://i.kym-cdn.com/photos/images/newsfeed/002/163/934/18f.gif})}
\end{figure}
\end{example}
\end{admonition-remark}

We would show the result for \(n=m+1\) and hence it follows for all \(m < n\). Then
notice that that the inequality is equivalent to showing that

\begin{equation*}
(1+t^m)^{m+1} - (1+t^{m+1})^m
= \sum_{i=1}^m \binom{m+1}{i} t^{mi} -  \sum_{i=1}^{m-1} \binom{m}{i} t^{(m+1)i}
\geq 0
\end{equation*}

We would split this into two cases.

\begin{example}[{Case 1: \(m\) is odd}]
Then, \(m=2k+1\) and

\begin{equation*}
\begin{aligned}
&\sum_{i=1}^m \binom{m+1}{i} t^{mi} -  \sum_{i=1}^{m-1} \binom{m}{i} t^{(m+1)i}
\\&=
\left[\sum_{i=1}^k \binom{m+1}{i} t^{mi} + \binom{m+1}{k+1}t^{m(k+1)} + \sum_{i=k+2}^{m} \binom{m+1}{i} t^{mi}\right]
\\&\hspace{3em}
-
\left[\sum_{i=1}^{k} \binom{m}{i} t^{(m+1)i} + \sum_{i=k+1}^{m-1} \binom{m}{i} t^{(m+1)i}\right]
\\&=
\left[\sum_{i=1}^k \binom{m+1}{i} t^{mi} + \binom{m+1}{k+1}t^{m(k+1)} + \sum_{i=1}^{k} \binom{m+1}{m+1-i} t^{m(m+1-i)}\right]
\\&\hspace{3em}
-
\left[\sum_{i=1}^{k} \binom{m}{i} t^{(m+1)i} + \sum_{i=1}^{k} \binom{m}{m-i} t^{(m+1)(m-i)}\right]
\\&=
\sum_{i=1}^k \left[\binom{m+1}{i} t^{mi} + \binom{m+1}{m+1-i} t^{m(m+1-i)} - \binom{m}{i} t^{(m+1)i}- \binom{m}{m-i} t^{(m+1)(m-i)}\right]
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\binom{m+1}{i} t^{mi} + \binom{m+1}{i} t^{m(m+1-i)} - \binom{m}{i} t^{(m+1)i}- \binom{m}{i} t^{(m+1)(m-i)}\right]
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\left[\binom{m+1}{i} - \binom{m}{i}\right] \left(t^{mi} + t^{m(m+1-i)}\right) + \binom{m}{i}\left(t^{mi} + t^{m(m+1-i)} - t^{(m+1)i}- t^{(m+1)(m-i)}\right)\right]
\\&\hspace{3em}
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\binom{m}{i-1} \left(t^{mi} + t^{m(m+1-i)}\right) + \binom{m}{i}\left(t^{mi} + t^{m^2 +m-mi} - t^{mi +i}- t^{m^2+m-mi-i}\right)\right]
\\&\hspace{3em}
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\binom{m}{i-1} \left(t^{mi} + t^{m(m+1-i)}\right) + \binom{m}{i}t^{mi}\left(1 + t^{m^2 +m-2mi} - t^{i}- t^{m^2+m-2mi-i}\right)\right]
\\&\hspace{3em}
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\binom{m}{i-1} \left(t^{mi} + t^{m(m+1-i)}\right) + \binom{m}{i}t^{mi}\left((1-t^i) + t^{m^2 +m-2mi-i}(t^i-1)\right)\right]
\\&\hspace{3em}
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\binom{m}{i-1} \left(t^{mi} + t^{m(m+1-i)}\right) + \binom{m}{i}t^{mi}\left(1-t^i\right)\left(1- t^{m^2 +m-2mi-i}\right)\right]
\\&\hspace{2em}
+ \binom{m+1}{k+1}t^{m(k+1)}
\\&=
\sum_{i=1}^k \left[\binom{m}{i-1} \left(t^{mi} + t^{m(m+1-i)}\right) + \binom{m}{i}t^{mi}\left(1-t^i\right)\left(1- t^{(m-2i)(m+1) +i}\right)\right]
\\&\hspace{2em}
+ \binom{m+1}{k+1}t^{m(k+1)}
\end{aligned}
\end{equation*}

Then since \((m-2i)(m+1) +i > 0\),

\begin{equation*}
1-t^i > 0 \iff t < 1 \iff 1-t^{(m-2i)(m+1) + i} > 0
\end{equation*}

and hence all the terms in the above summation are non negative and
\((1+t^m)^{m+1} - (1+t^{m+1})^m \geq 0\).
Further, if it is equal to zero, the extra term must be zero and hence \(t=0\).
\end{example}

\begin{example}[{Case 2: \(m\) is odd}]
Then, \(m=2k\)

\begin{equation*}
\begin{aligned}
&\sum_{i=1}^m \binom{m+1}{i} t^{mi} -  \sum_{i=1}^{m-1} \binom{m}{i} t^{(m+1)i}
\\&=
\left[ \sum_{i=1}^{k-1} \binom{m+1}{i} t^{mi} + \binom{m+1}{k}t^{mk} + \binom{m+1}{k+1}t^{m(k+1)} + \sum_{i=k+2}^m \binom{m+1}{i} t^{mi}\right]
\\&\hspace{2em}
-
\left[ \sum_{i=1}^{k-1} \binom{m}{i} t^{(m+1)i} +\binom{m}{k}t^{(m+1)k} + \sum_{i=k+1}^{m-1} \binom{m}{i} t^{(m+1)i}\right]
\\&=
\left[ \sum_{i=1}^{k-1} \binom{m+1}{i} t^{mi} + \binom{m+1}{k}t^{mk} + \binom{m+1}{k+1}t^{m(k+1)} + \sum_{i=1}^{k-1} \binom{m+1}{m+1-i} t^{m(m+1-i)}\right]
\\&\hspace{2em}
-
\left[ \sum_{i=1}^{k-1} \binom{m}{i} t^{(m+1)i} +\binom{m}{k}t^{(m+1)k} + \sum_{i=1}^{k-1} \binom{m}{m-i} t^{(m+1)(m-i)}\right]
\\&=
\left[ \sum_{i=1}^{k-1} \binom{m+1}{i} t^{mi} + \binom{m+1}{k}t^{mk} + \binom{m+1}{k}t^{m(k+1)} + \sum_{i=1}^{k-1} \binom{m+1}{i} t^{m(m+1-i)}\right]
\\&\hspace{2em}
-
\left[ \sum_{i=1}^{k-1} \binom{m}{i} t^{(m+1)i} +\binom{m}{k}t^{(m+1)k} + \sum_{i=1}^{k-1} \binom{m}{i} t^{(m+1)(m-i)}\right]
\\&=
\sum_{i=1}^{k-1} \left[\binom{m+1}{i} (t^{mi} +t^{m(m+1-i)} ) -  \binom{m}{i} (t^{(m+1)i} + t^{(m+1)(m-i)})\right]
\\&\hspace{2em}
+ \binom{m+1}{k}t^{mk} + \binom{m+1}{k}t^{m(k+1)} - \binom{m}{k}t^{(m+1)k}
\\&=
\sum_{i=1}^{k-1} \left[\binom{m}{i-1} (t^{mi} +t^{m(m+1-i)} )  + \binom{m}{i} (t^{mi} +t^{m(m+1-i)} - t^{(m+1)i} - t^{(m+1)(m-i)})\right]
\\&\hspace{2em}
+ \left(\binom{m}{k-1} + \frac12 \binom{m}{k}\right)(t^{mk} + t^{m(k+1)})
+ \frac{1}{2}\binom{m}{k}(t^{mk} + t^{m(k+1)}- 2t^{(m+1)k})
\\&=
\sum_{i=1}^{k-1} \left[\binom{m}{i-1} (t^{mi} +t^{m(m+1-i)} )  + \binom{m}{i} t^{mi}\left(1-t^i\right)\left(1- t^{(m-2i)(m+1) +i}\right)\right]
\\&\hspace{2em}
+ \left(\binom{m}{k-1} + \frac12 \binom{m}{k}\right)(t^{mk} + t^{m(k+1)})
+ \frac{1}{2}\binom{m}{k}t^{mk}(1 + t^{2k}- 2t^{k})
\\&=
\sum_{i=1}^{k-1} \left[\binom{m}{i-1} (t^{mi} +t^{m(m+1-i)} )  + \binom{m}{i} t^{mi}\left(1-t^i\right)\left(1- t^{(m-2i)(m+1) +i}\right)\right]
\\&\hspace{2em}
+ \left(\binom{m}{k-1} + \frac12 \binom{m}{k}\right)(t^{mk} + t^{m(k+1)})
+ \frac{1}{2}\binom{m}{k}t^{mk}(1 - t^{k})^2
\end{aligned}
\end{equation*}

Again, this is the sum of non-negative terms and the extra term is zero iff \(t = 0\).
\end{example}

By getting lost in summations, the above proof loses the essence which
it is based upon. I would leave the above proof however, the below proof is
much cleaner (albeit still quite ugly).

Like above, we firstly raise to the power of \(mn\) in order to get
rid of radicals. Then, all we want to prove is that'

\begin{equation*}
\left(1+t^m\right)^n - \left(1+t^n\right)^m
= \sum_{i=1}^{n-1} \binom{n}{i}t^{mi} - \sum_{i=1}^{m-1} \binom{m}{i}t^{ni}
\geq 0
\end{equation*}

Now, we want to consider the \(i'th\) first and last terms from each summation.
That is

\begin{equation*}
\begin{aligned}
&
\binom{n}{i}t^{mi} + \binom{n}{n-i}t^{m(n-i)}
- \binom{m}{i}t^{ni} - \binom{m}{m-i}t^{n(m-i)}
\\&=
\binom{n}{i}t^{mi} + \binom{n}{i}t^{m(n-i)}
- \binom{m}{i}t^{ni} - \binom{m}{i}t^{n(m-i)}
\\&=
\left[\binom{n}{i} - \binom{m}{i}\right](t^{mi} + t^{m(n-i)})
+ \binom{m}{i} (t^{mi} + t^{m(n-i)} - t^{ni} - t^{n(m-i)})
\\&=
\left[\binom{n}{i} - \binom{m}{i}\right](t^{mi} + t^{m(n-i)})
+ \binom{m}{i} (t^{mi} + t^{mn-mi} - t^{ni} - t^{nm-ni})
\\&=
\left[\sum_{j=m}^{n-1} \binom{j-1}{i-1}\right](t^{mi} + t^{m(n-i)})
+ \binom{m}{i} \left((t^{mi} - t^{ni}) + t^{mn-mi-ni}(t^{ni} - t^{mi})\right)
\\&=
\left[\sum_{j=m}^{n-1} \binom{j-1}{i-1}\right](t^{mi} + t^{m(n-i)})
+ \binom{m}{i} (t^{mi} - t^{ni})(1 - t^{mn-mi-ni})
\\&=
\left[\sum_{j=m}^{n-1} \binom{j-1}{i-1}\right](t^{mi} + t^{m(n-i)})
+ \binom{m}{i}t^{mi} (1 - t^{ni-mi})(1 - t^{mn-mi-ni})
\\&=
\left[\sum_{j=m}^{n-1} \binom{j-1}{i-1}\right](t^{mi} + t^{m(n-i)})
+ \binom{m}{i}t^{mi} (1 - t^{(n-m)i})\left(1 - t^{\frac{m(n-2i)}{2} + \frac{n(m-2i)}2}\right)
\end{aligned}
\end{equation*}

which is non-negative when \(i < \frac{m}{2} < \frac{n}{2}\).
Then we group the four terms together for each \(i < \frac{m}{2}\)
Now, if \(\sum_{i=1}^{m-1}\binom{m}{i}t^{ni}\) has an
even number of terms, we are done. Otherwise suppose there is
a middle term. This occurs if \(m\) is even and the middle
term if when \(i = \frac{m}{2}\).

Let \(m  =2k\)
and notice that the summation \(\sum_{i=1}^{n-1}\binom{n}{i}t^{mi}\)
has at least one more left over term than the second series.
Namely, when \(i=\frac{m}{2}=k\), the terms \(\binom{n}{k}t^{mk}\)
and \(\binom{n}{n-k}t^{m(n-k)}\) are unused by the above grouping
process. Then, consider the sum of these two terms with the middle
term \(\binom{m}{i}t^{nk}\)

\begin{equation*}
\begin{aligned}
&
\binom{n}{k}t^{mk} + \binom{n}{n-k} t^{m(n-k)} - \binom{m}{k}t^{nk}
\\&=
\binom{n}{k}t^{mk} + \binom{n}{k} t^{m(n-k)} - \binom{m}{k}t^{nk}
\\&=
\left[\binom{n}{k}- \frac{1}{2}\binom{m}{k}\right]\left(t^{mk} + t^{m(n-k)}\right)
+ \frac12 \binom{m}{k}\left( t^{mk} + t^{m(n-k)}- 2t^{nk}\right)
\\&=
\left[\binom{n}{k}- \frac{1}{2}\binom{m}{k}\right]\left(t^{mk} + t^{m(n-k)}\right)
+ \frac12 \binom{m}{k}t^{mk}\left( 1 + t^{m(n-2k)}- 2t^{nk-mk}\right)
\\&=
\left[\binom{n}{k}- \frac{1}{2}\binom{m}{k}\right]\left(t^{mk} + t^{m(n-k)}\right)
+ \frac12 \binom{m}{k}t^{mk}\left( 1 - t^{\frac{m}{2}(n-2k)}\right)^2
\end{aligned}
\end{equation*}

Notice that

\begin{equation*}
\binom{n}{k} - \frac12 \binom{m}{k} \geq \binom{n}{k} - \binom{m}{k}
=\sum_{j=m}^{n-1} \binom{j-1}{k-1}
\end{equation*}

Therefore, we get a non-negative result in either case and we are done.
\end{example}

\subsection{Lemma 1}
\label{develop--math3277:metrics:page--inequalities.adoc---lemma-1}

Let \(x, y \in \mathbb{R}^*\), and \(m, n \in \mathbb{Z}^+\) such that
\(m \leq n\). Then

\begin{equation*}
\sqrt[m]{x^m + y^m} \geq \sqrt[n]{x^n + y^n}
\end{equation*}

where equality holds iff at least one of \(x\) or \(y\) is zero.

\begin{example}[{Proof}]
Firstly, note that if \(x = 0\), equality holds. Then, consider when \(x \neq 0\). Then

\begin{equation*}
\sqrt[m]{x^m + y^m} \geq \sqrt[n]{x^n + y^n}
\iff
\sqrt[m]{1 + \left(\frac{y}{x}\right)^m}
= \frac{1}{x}\sqrt[m]{x^m + y^m}
\geq \frac{1}{x}\sqrt[n]{x^n + y^n}
= \sqrt[n]{1 + \left(\frac{y}{x}\right)^n}
\end{equation*}

where equality holds iff \(\frac{y}{x} = 0\) and we are done.
\end{example}

\subsection{Lemma 2 (extension of lemma 1)}
\label{develop--math3277:metrics:page--inequalities.adoc---lemma-2-extension-of-lemma-1}

Let \(\vec{a} = (a_1,\ldots a_k) \in (\mathbb{R}^*)^k\) and \(m, n \in \mathbb{Z}^+\)
such that
\(m \leq n\). Then

\begin{equation*}
\sqrt[m]{\sum_{i=1}^k a_i^m} \geq \sqrt[n]{\sum_{i=1}^k a_i^n}
\end{equation*}

where equality holds iff at most one of the \(a_i \neq 0\) or \(k = 1\).

\begin{example}[{Proof}]
The result is trivially true when \(k=1\). Now, suppose that its
true for some arbitrary value of \(k \in \mathbb{Z}^+\). Then,

\begin{equation*}
\begin{aligned}
\sqrt[m]{\sum_{i=1}^{k+1} a_i^m}
&= \sqrt[m]{\sum_{i=1}^{k} a_i^m + a_{k+1}^m}
\\&= \sqrt[m]{\left(\sqrt[m]{\sum_{i=1}^{k} a_i^m}\right)^m + a_{k+1}^m}
\\&\geq \sqrt[m]{\left(\sqrt[n]{\sum_{i=1}^{k} a_i^n}\right)^m + a_{k+1}^m}
\quad\quad\text{by inductive step}
\\&\geq \sqrt[n]{\left(\sqrt[n]{\sum_{i=1}^{k} a_i^n}\right)^n + a_{k+1}^n}
\quad\quad\text{by lemma 1}
\\&= \sqrt[n]{\sum_{i=1}^{k} a_i^n + a_{k+1}^n}
\\&= \sqrt[n]{\sum_{i=1}^{k+1} a_i^n}
\end{aligned}
\end{equation*}

Now suppose equality holds. Then,
for the first inequality at most one of the \(a_1,\ldots a_k \neq 0\).
And for the second inequality \(\sqrt[n]{\sum_{i=1}^{k} a_i^n} = 0\)
or \(a_{k+1} = 0\). We take two cases

\begin{itemize}
\item If \(a_{k+1} = 0\). Then since at most one of \(a_1, \ldots a_k \neq 0\),
    at most one of \(a_1, \ldots a_{k+1} \neq 0\)
\item If \(a_{k+1} \neq 0\). Then \(\sqrt[n]{\sum_{i=1}^{k} a_i^n} = 0\) and hence
    each of the \(a_1,\ldots a_k = 0\). Then at most one of the \(a_1, \ldots a_{k+1} \neq 0\).
\end{itemize}

In both cases, we get the same result and we are done.
\end{example}
\end{document}
