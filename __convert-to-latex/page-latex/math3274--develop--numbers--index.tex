\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Natural Numbers}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
Now, recall the \myautoref[{axiom of infinity}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-infinity}; previously, the specific choice
of our infinite set may have seemed a bit useless. However,
this choice allows us to nicely define the natural numbers.

\begin{custom-quotation}[{}][{}]
Let \(N\) be a set and \(s\) be an operator
of \(n \in N\).
Then, we say that \(N\) is a \emph{successor set}
if

\begin{itemize}
\item \(0 \in N\)
\item \(\forall n \in N: s(n) \in N\)
\item If \(S \subseteq N\) and \(0 \in S\) and \(\forall n \in S: s(n) \in S\),
    then \(S = N\)
\item \(\forall n \in N : s(n) \neq 0 \)
\item \(\forall n,m \in N: s(n) = s(m) \implies n = m\)
\end{itemize}

where \(0\) is called the \emph{zero element}.
\end{custom-quotation}

These conditions could be simplified to

\begin{itemize}
\item \(0 \in N\)
\item \(s : N \to N\)
\item \(0 \notin \ran s\)
\item \(s\) is injective
\item \(N\) is a minimal. That is, there is strict subset of
    \(N\) which forms a successor set.
\end{itemize}

Additionally, sometimes, rather than writing \(s(n)\)
we write \(n^+ \).

\begin{admonition-remark}[{}]
The minimality of \(N\) is what allows us
to prove things easily using ``induction''.
\end{admonition-remark}

\section{Transitive Sets}
\label{develop--math3274:numbers:page--index.adoc---transitive-sets}

\begin{admonition-remark}[{}]
This section was sparked by
\url{https://people.hamilton.edu/scockbur/philosophy-seminar/set-theory-texbook/8-transfinite-numbers-in-zf}
\end{admonition-remark}

Before we get to proving that \(\mathbb{N}\) defined in \myautoref[{axiom of infinity}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-infinity} is
a successor set, we need to first explore the idea of transitive sets.

A set \(X\) is \emph{transitive} if all of its elements are subsets of
\(X\). Then this is equivalent to

\begin{equation*}
\forall T \in X: t \in T \implies t \in X
\end{equation*}

\begin{example}[{Proof}]
Firstly, suppose that \(X\) is transitive. Then if \(T \in X\)
and \(t \in T\), then since \(T \subseteq X\),
we get that \(t \in X\).

Conversely consider \(T \in X\), then since all of the elements of
\(T\) are also in \(X\), \(T \subseteq X\).
\end{example}

That is, \(\in\) could be thought of as a transitive relation.
Note however, that it is not a relation since there is no set of sets.

\subsection{Transformations}
\label{develop--math3274:numbers:page--index.adoc---transformations}

Let \(X\) be a transitive set and \(\mathcal{C}\) be a collection
of transitive sets. Then

\begin{itemize}
\item \(\mathcal{P}(X)\) is transitive
\item \(\bigcup \mathcal{C}\) is transitive
\item \(\bigcap \mathcal{C}\) is transitive
\end{itemize}

\begin{example}[{Proofs}]
\begin{example}[{Proof of power set}]
Consider \(T \in \mathcal{P}(X)\), then, by definition \(T \subseteq X\).
Now, consider \(t \in T\). Then \(t \in X\) which
implies that \( t \subseteq X\) which implies that
\(t \in P(X)\). Therefore since the choice of \(t \in T\)
was arbitrary, \(T \subseteq \mathcal{P}(X)\)
\end{example}

\begin{example}[{Proof of union}]
Consider \(T \in \bigcup \mathcal{C}\). Then,
\(\exists X \in \mathcal{C}\) such that \(T \in X\).
However, by the transitivity of \(X\),
\(T \subseteq X \subseteq \bigcup{C}\)
and we are done.
\end{example}

\begin{example}[{Proof of intersection}]
Consider \(T \in \bigcap \mathcal{C}\). Then \(\forall X \in \mathcal{C}\),
\(T \in X\). Then, by the transitivity of \(X\),
we get that \(T \subseteq X \) for all \(X \in \mathcal{C}\).
It therefore follows that \(T \subseteq \bigcap\mathcal{C}\).
\end{example}
\end{example}

\section{Axiom of Infinity}
\label{develop--math3274:numbers:page--index.adoc---axiom-of-infinity}

Then, we claim that \(\mathbb{N}\) defined by the \myautoref[{axiom of infinity}]{develop--math3274:set-theory:page--axioms.adoc---axiom-of-infinity}
together with the function \(x\mapsto x^+ \)
defined below, is a successor set.

\begin{equation*}
x^+ = x \cup \{x\}
\end{equation*}

\begin{example}[{Proof (To fix)}]
By the definition of AOI, \(\varnothing \in \mathbb{N}\) similarly,

\begin{equation*}
s = \{(x,y) \in \mathbb{N}\times\mathbb{N}: y = x \cup \{x\} \}
\end{equation*}

Hence, \(s : \mathbb{N} \to \mathbb{N}\) since the left is always
in \(\mathbb{N}\) by definition of AOI. Also,
it is clear that \(\varnothing \notin \ran s\) since

\begin{equation*}
y \in \ran s \implies [\exists x \in \mathbb{N}: y = x \cup \{x\}]
\implies [\exists x \in \mathbb{N}:  x\in y]
\implies y \neq \varnothing
\end{equation*}

To see injectivity, we first need to show that if \(t \in x \in \mathbb{N}\), then
\(t \subseteq x\).
To see that it is injective,
suppose that

\begin{equation*}
x \cup \{x\} = y \cup \{y\}
\end{equation*}

Now, notice that \(x \in y \cup \{y\}\) and hence \(x \in y\) or \(x = y\)
and likewise \(y \in x\) or \(x = y\).
BWOC, suppose that \(x \neq y\) then \(x \in y\) and \(y \in x\)
\end{example}

\section{Recursion Theorem}
\label{develop--math3274:numbers:page--index.adoc---recursion-theorem}

Let \(X\) be a set, \(a \in X\) and \(f : X \to X\).
Then there exists a function \(u: \mathbb{N} \to X\)
such that

\begin{equation*}
u(0) = a \quad\text{and}\quad u(n^+) = f(u(n))
\end{equation*}

\begin{example}[{Proof}]
Let

\begin{equation*}
u = \{(n, b) \in \mathbb{N} \times X | ( n =0\text{ and } b = a) \text{ or } (\exists m \in \mathbb{N}: \exists c \in X: n = m^+ \text{ and } (m, c) \in u \text{ and } b = f(c) \}
\end{equation*}

Then, we claim that \(u: \mathbb{N} \to X\). Firstly, notice

\begin{itemize}
\item \(0 \in \dom u\) since \((0, a) \in u\)
\item If \(m \in \dom u\), \(\exists c \in X: (m, c) \in u\) and hence \((m^, f(c)) {\textbackslash}in u]. Therefore, stem:[m^ \in \dom u\)
\end{itemize}

Therefore, \(\dom u \subseteq \mathbb{N}\) is a successor set and by axiom 3, \(\dom u = \mathbb{N}\).

Now, let

\begin{equation*}
S = \{n \in \mathbb{N}: \forall b,c \in X: [(n, b) \in u\text{ and } (n, c) \in u] \implies b =c \}
\end{equation*}

Then notice that

\begin{itemize}
\item \(0 \in S\) since \(\not\exists m \in S\) such that \(0 = m^+ \) and hence \(b = c =a\).
\item If \(m \in S\), then let \(n = m^+ \). Suppose that \((n, b) \in u\) and \((n, c) \in u\).
    Then, there exists \(b', c' \in X\) such that \(b = f(b')\) and \(c = f(c')\) and \((m, b') \in u\)
    and \((m, c') \in u\). Therefore, \(c' = b'\) and hence \(b = f(b') = f(c') = c\).
\end{itemize}

Therefore \(S\) is a successor set and \(u: \mathbb{N} \to X\)
\end{example}

\section{Arithmetic}
\label{develop--math3274:numbers:page--index.adoc---arithmetic}

Note that all of these functions exist by the recursion theorem.

\subsection{Addition}
\label{develop--math3274:numbers:page--index.adoc---addition}

Let \(m \in \mathbb{N}\), then we define \(s_m: \mathbb{N} \to \mathbb{N}\) such that

\begin{itemize}
\item \(s_m(0) = m\)
\item \(s_m(n^+ ) = (s_m(n))^+ \) for each natural number \(n\)
\end{itemize}

Then, we denote \(s_m(n) \defeq m + n\). Then \( + \)
is associative and commutative. Furthermore,

\begin{itemize}
\item \( + \) exhibits a cancellation property. That is \(m + a = n + a \iff m = n\)
\item \(\exists a \in \mathbb{N}: (m + a = n) \lor (n + a = m)\) where \(a\) is unique.
\end{itemize}

\begin{example}[{Proof}]
\begin{example}[{Proof of associativity}]
For associativity, choose arbitrary \(x,y \in \mathbb{N}\) and let

\begin{equation*}
S = \{n \in \mathbb{N}: (x + y) + n = x + (y + n)\}
\end{equation*}

Notice that

\begin{equation*}
(x + y) + 0 = s_{x + y}(0) = x + y = x + y = x + s_y(0) = x + (y + 0)
\end{equation*}

Also, consider if \(m \in S\), then

\begin{equation*}
\begin{aligned}
(x + y ) + m^+
&= s_{x+y}(m^+)
\\&= (s_{x + y}(m))^+
\\&= ((x + y) + m)^+
\\&= (x + (y + m))^+
\\&= (s_x(y + m))^+
\\&= s_x((y + m)^+)
\\&= s_x((s_y(m))^+)
\\&= s_x(s_y(m^+))
\\&= x + ( y + m^+)
\end{aligned}
\end{equation*}

hence \(m^+ \in S\) and we get that \(S  = \mathbb{N}\).
\end{example}

\begin{example}[{Proof of commutativity}]
We define

\begin{equation*}
C_n = \{ k \in \mathbb{N}: n + k = k +n\}
\end{equation*}

for each \(n \in \mathbb{N}\) and

\begin{equation*}
S = \{n \in \mathbb{N}: C_n = \mathbb{N}\}
\end{equation*}

Then, firstly, we want to show that \(0 \in S\).
Notice that \(0 \in C_0\) since \(0 + 0 = 0 + 0\).
Next, notice that if \(k \in C_0\),

\begin{equation*}
k^+ + 0 = s_{k^+}(0) = k^+ = (s_k(0))^+ = (s_0(k))^+ = s_0(k^+) = 0 + k^+
\end{equation*}

Therefore, \(k^+ \in C_0\) and \(C_0 = \mathbb{N}\).

Next, suppose that \(n \in S\). Then,

\begin{equation*}
n^+ + 0 = s_{n^+}(0) = n^+ = (s_n(0))^+ = (n+ 0)^+ = (0 + n)^+  = (s_0(n))^+ = s_0(n^+) = 0 + n^+
\end{equation*}

Therefore, \(0 \in C_{n^+ }\). Next, suppose that \(k \in C_{n^+ }\),

\begin{equation*}
\begin{aligned}
n^+ + k^+
&= s_{n^+}(k^+)
\\&= (s_{n^+}(k))^+
\\&= (n^+ + k)^+
\\&= (k + n^+)^+ \quad\text{since } k \in C_{n^+}
\\&= (s_k(n^+))^+
\\&= ((s_k(n))^+)^+
\\&= ((k + n)^+)^+
\\&= ((n + k)^+)^+ \quad\text{since } C_n = \mathbb{N}
\\&= ((s_n(k))^+)^+
\\&= (s_n(k^+))^+
\\&= (n + k^+)^+
\\&= (k^+ + n)^+ \quad\text{since } C_n = \mathbb{N}
\\&= (s_{k^+}(n))^+
\\&= s_{k^+}(n^+)
\\&= k^+ + n^+
\end{aligned}
\end{equation*}

Therefore, \(k^+ \in C_{n^+ }\) and we get that \(C_{n^+ } = \mathbb{N}\)
and hence \(n^+ \in S\). Therefore, \(S = \mathbb{N}\)
and we are done.

\begin{admonition-remark}[{}]
This proof has redundancy. For example, we prove that \(a  + b = b + a\)
twice if \(a\) or \(b\) is non zero. However, it still works.
\end{admonition-remark}
\end{example}

\begin{example}[{Proof of Cancellation}]
Let \(m, n \in \mathbb{N}\) and

\begin{equation*}
S = \{k \in \mathbb{N}: m + k = n + k \implies m = n\}
\end{equation*}

Then, if \(m + 0 = n + 0\), we get

\begin{equation*}
m = s_m(0) = s_n(0) = n
\end{equation*}

Therefore, \(0 \in S\). Next, suppose that
\(k \in \mathbb{N}\) and \(m + k^+ = n + k^+ \),
then

\begin{equation*}
(s_m(k))^+ = s_m(k^+) = s_n(k^+) = (s_n(k))^+ \implies s_m(k) = s_n(k) \implies m = n
\end{equation*}

by the 5th peano axiom. Therefore \(k^+ \in S\) and \(S = \mathbb{N}\).
Therefore, we are done.
\end{example}

\begin{example}[{Proof of existence of residue}]
Let \(m \in \mathbb{N}\) and

\begin{equation*}
S = \{n \in \mathbb{N}: \exists a \in \mathbb{N}: (m + a =n) \lor (n + a = m)\}
\end{equation*}

Then, \(0 \in S\) since \(0 + m = m\). Next, suppose that \(n \in S\). Then, we have
two cases

\begin{itemize}
\item \(n + a = m\). If \(a = 0\), \(m + 0^+ = n + 0^+ = n^+ \). If \(a \neq 0\),
    let \(a = b^+ \) and hence \(n^+ + b = n + b^+ = m\).
\item \(m + a = n\). Then, \(m + a^+ = (m + a)^+ = n^+ \)
\end{itemize}

In both cases, \(n^+ \in S\). Therefore \(S = \mathbb{N}\). Next, suppose
there exists \(a,b \in \mathbb{N}\) such that

\begin{equation*}
[(m + a = n) \lor (n + a = m)]\land [(m + b = n) \lor (n + b = m)]
\end{equation*}

Then, we have four cases

\begin{itemize}
\item \((m + a = n)\land(m + b= n)\), then by the cancellation property, \(a = b\).
\item \((m + a = n)\land(n + b= m)\), then \(n + b + a = n\) which implies that \(b + a =0\)
    which implies that \(b = a= 0\).
\item \((n + a = m)\land(m + b= n)\), then \(n + a + b = n\) which implies that \(b + a =0\)
    which implies that \(b = a= 0\).
\item \((n + a = m)\land(n + b= m)\), then by the cancellation property, \(a = b\).
\end{itemize}

In all cases, \(a = b\) and hence the existence is unique.
\end{example}
\end{example}

\begin{admonition-remark}[{}]
Using that second property, we can define a total ordering
on \(\mathbb{N}\) which is independent of the construction of
\(\mathbb{N}\) as it relies only on the peano axioms.
Furthermore, the constructed ordering is identical
to that using subsets.
\end{admonition-remark}

\subsection{Multiplication}
\label{develop--math3274:numbers:page--index.adoc---multiplication}

Let \(m \in \mathbb{N}\), then we define \(p_m: \mathbb{N} \to \mathbb{N}\)
such that

\begin{itemize}
\item \(p_m(0) = 0\)
\item \(p_m(n^+) = p_m(n) + m\)
\end{itemize}

Then, we denote \(p_m(n) \defeq m \times n\). Then \(\times\) is
associative, commutative and distributes over \(+ \).

Furthermore,

\begin{itemize}
\item \(\times\) has no zero divisors. That is \(m \times n = 0 \iff (m = 0 \ \lor \ n = 0)\)
\item If \(a \neq 0\), \(a \times m = a \times n \iff m = n\)
\end{itemize}

\begin{example}[{Proof}]
\begin{example}[{Proof of associativity}]
\begin{admonition-note}[{}]
This proof requires that \(\times\) distributes over \(+ \)
as well as commutativity of \(\times\).
\end{admonition-note}

Consider arbitrary \(x, y \in \mathbb{N}\) and let

\begin{equation*}
S = \{ n \in \mathbb{N} : (x \times y) \times n = x \times (y \times n)\}
\end{equation*}

Then \(0 \in S\) since

\begin{equation*}
(x \times y) \times 0 = p_{x \times y}(0) = 0 = p_x(0) = p_x(p_y(0)) = x \times (y \times 0)
\end{equation*}

Now, suppose that \(n \in S\). Then

\begin{equation*}
\begin{aligned}
(x \times y) \times n^+
&= p_{x \times y}(n^+)
\\&= p_{x \times y}(n) + (x \times y)
\\&= ((x\times y) \times n) + (x \times y)
\\&= (x\times (y \times n)) + (x \times y)
\\&= ((y \times n) \times x) + (y \times x)
\\&= ((y \times n) + y) \times x
\\&= x \times ((y \times n) + y)
\\&= x \times (y \times n^+)
\end{aligned}
\end{equation*}

Therefore, \(n^+ \in S\) and \(S = \mathbb{N}\).
\end{example}

\begin{example}[{Proof of commutativitiy}]
Let

\begin{equation*}
T_n = \{k \in \mathbb{N}: n \times k = k \times n\}
\end{equation*}

where \(n\in \mathbb{N}\) and

\begin{equation*}
S = \{n \in \mathbb{N}: T_n = \mathbb{N}\}
\end{equation*}

Firstly, notice that trivially \(0 \in T_0\). Next,
suppose that \(k \in T_0\), then

\begin{equation*}
k^+ \times 0 = p_{k^+}(0) = 0 = p_k(0) = p_0(k) = s_{p_0(k)}(0) = p_0(k) + 0 = p_0(k^+) = 0 \times k^+
\end{equation*}

Therefore \(k^+ \in T_0\) and hence \(T_0 = \mathbb{N}\) and \(0 \in S\).

Next, suppose that \(n \in S\). Then

\begin{equation*}
0 \times n^+ = p_0(n^+) = p_0(n) + 0 = p_0(n) = p_n(0) = 0 = p_{n^+}(0) = n^+ \times 0
\end{equation*}

Therefore, \(0 \in T_{n^+ }\). Next, suppose that \(k \in T_{n^+ }\).
Then

\begin{equation*}
\begin{aligned}
k^+ \times n^+
&= p_{k^+}(n^+)
\\&= p_{k^+}(n) + k^+
\\&= p_n(k^+) + k^+ \quad\text{since } T_n = \mathbb{N}
\\&= (p_n(k) + n) + k^+
\\&= p_n(k) + (n + k^+)
\\&= p_n(k) + s_n(k^+)
\\&= p_n(k) + (s_n(k))^+
\\&= p_n(k) + (s_k(n))^+
\\&= p_n(k) + (s_k(n^+))
\\&= p_n(k) + (k + n^+)
\\&= p_k(n) + (k + n^+) \quad\text{since } T_n = \mathbb{N}
\\&= (p_k(n) + k) + n^+
\\&= p_k(n^+) + n^+
\\&= p_{n^+}(k) + n^+ \quad\text{since } k \in T_{n^+}
\\&= p_{n^+}(k^+)
\\&= n^+ \times k^+
\end{aligned}
\end{equation*}

Therefore \(k^+ \in T_{n^+ }\) and hence \(T_{n^+ } = \mathbb{N}\).
Therefore, \(n^+ \in S\) and \(S = \mathbb{N}\).
\end{example}

\begin{example}[{Proof of \(\times\) distributes over \(+ \)}]
Let

\begin{equation*}
S = \{n \in \mathbb{N}: \forall x, y\in \mathbb{N}: (x + y) \times n = (x \times n) + (y \times n)\}
\end{equation*}

Then, notice that for arbitrary \(x, y\in \mathbb{N}\)

\begin{equation*}
(x + y) \times 0 = 0 = 0 + 0 = (x \times 0) + (y \times 0)
\end{equation*}

Therefore \(0 \in S\). Now, suppose that \(n \in S\).
Then

\begin{equation*}
\begin{aligned}
(x + y) \times n^+
&= ((x + y) \times n) + (x + y)
\\&= ((x \times n) + (y \times n)) + (x + y) \quad\text{since } n \in S
\\&= (x \times n) + ((y \times n) + (x + y))
\\&= (x \times n) + ((x + y) + (y \times n))
\\&= (x \times n) + (x + (y + (y \times n)))
\\&= ((x \times n) + x) + (y + (y \times n))
\\&= ((x \times n) + x) + ((y \times n) + y)
\\&= (x \times n^+) + (y \times n^+)
\end{aligned}
\end{equation*}

Hence \(n^+ \in S\) and \(S = \mathbb{N}\).
\end{example}

\begin{example}[{Proof of lack of zero divisors}]
Clearly, if \(m = 0\) or \(n=0\) we have that \(m \times n = 0\)
by commutativity and definition. Now, suppose that \(m \times n = 0\),
that is

\begin{equation*}
0 = m \times n = p_m(n)
\end{equation*}

Now, if \(n=0\), we are done. On the other hand if
\(n \neq 0\), we have \(n = k^+ \) for some \(k \in \mathbb{N}\)
and hence

\begin{equation*}
p_m(n) = p_m(k^+) = p_m(k) + m = s_{p_m(k)}(m)
\end{equation*}

Now, notice that \(m=0\) otherwise \(p_m(n) = t^+ = 0\) for some \(t \in \mathbb{N}\)
which is in contradiction to the 4th peano axiom.
\end{example}

\begin{example}[{Proof of cancellation property}]
Let \(m, a \in \mathbb{N}\) and let

\begin{equation*}
S = \{n \in \mathbb{N}: a^+ \times m = a^+ \times n \implies m = n\}
\end{equation*}

Then, clearly \(0 \in S\) since

\begin{equation*}
0 = a^+ \times 0 = a^+ \times n \implies n = 0
\end{equation*}

because of a lack of zero divisors.
Next, suppose that \(n \in S\) and

\begin{equation*}
a^+ \times m = a^+ \times n^+ = a^+ + (a^+ \times n)
\end{equation*}

Then \(m \neq 0\) because of a lack of zero divisors.
Then let \(m = k^+ \) and hence

\begin{equation*}
a^+ + (a^+ \times k) = a^+ \times m = a^+ + (a^+ \times n)
\implies a^+ \times k = a^+ \times n
\implies k = n \implies n^+ = k^+ = m
\end{equation*}

since \(n \in S\). Then \(n^+ \in S\) and \(S = \mathbb{N}\).
\end{example}
\end{example}

\subsection{Exponentiation}
\label{develop--math3274:numbers:page--index.adoc---exponentiation}

Let \(m \in \mathbb{N}\), then we define \(e_m: \mathbb{N} \to \mathbb{N}\)
where

\begin{itemize}
\item \(e_m(0) = 0^+ \)
\item \(e_m(n^+) = e_m(n) * m\)
\end{itemize}

Then, we denote \(e_m(n) \defeq m^n\). Then

\begin{itemize}
\item \(m^a \times n^a = (m \times n)^a\). That is exponentiation distributes over multiplication
\item \(m^a \times m^b = m^{a + b}\)
\end{itemize}

\begin{example}[{Proof}]
\begin{example}[{Proof of distribution over multiplication}]
Let \(m, n \in \mathbb{N}\) be arbitrary and define

\begin{equation*}
S = \{a \in \mathbb{N} : m^a \times n^a = (m \times n)^a \}
\end{equation*}

Firstly note that \(0 \in S\) since

\begin{equation*}
\begin{aligned}
m^0 \times n^0
&= 0^+ \times 0^+
\\&= p_{0^+}(0^+)
\\&= p_{0^+}(0) + 0^+
\\&= 0 + 0^+ = s_0(0^+)
\\&= (s_0(0))^+
\\&= 0^+
\\&= e_{m \times n}(0) = (m \times n)^0
\end{aligned}
\end{equation*}

Next, suppose that \(k \in S\). Then

\begin{equation*}
\begin{aligned}
m^{k^+} \times n^{k^+}
= (m^k \times m)\times (n^k \times n)
= (m^k \times n^k)\times (m \times n)
= (m \times n)^k \times (m \times n)
= (m \times n)^{k^+}
\end{aligned}
\end{equation*}

Therefore \(k^+ \in S\) and \(S = \mathbb{N}\).
\end{example}

\begin{example}[{Proof of property 2}]
Let \(m, a \in \mathbb{N}\) and

\begin{equation*}
S = \{b \in \mathbb{N}: m^a \times m^b = m^{a + b}\}
\end{equation*}

Then, notice that \(0 \in S\) since

\begin{equation*}
m^a \times m^0 = m^a \times 0^+ = p_{m^a}(0^+) = p_{m^a}(0) + m^a = 0 + m^a = s_{m^a}(0) = m^a = m^{s_{a}(0)} = m^{a + 0}
\end{equation*}

Next, suppose that \(b \in S\). Then,

\begin{equation*}
\begin{aligned}
m^a \times m^{b^+}
= m^a \times e_{m}(b^+)
= m^a \times (e_{m}(b) \times m)
= (m^a \times m^b) \times m
= m^{a + b} \times m
= m^{(a + b)^+}
= m^{a + b^+}
\end{aligned}
\end{equation*}

Therefore \(b^+ \in S\) and \(S = \mathbb{N}\).
\end{example}
\end{example}

\section{Camparisons}
\label{develop--math3274:numbers:page--index.adoc---camparisons}

We say that two natural numbers \(m\) and \(n\) are \emph{comparable} iff
\(m \in n\) or \(m = n\) or \(n \in m\). Then, we have
the two results

\begin{itemize}
\item \(n \notin n\)
\item Two natural numbers are always comparable
\end{itemize}

We refer to the second property as the \emph{law of trichotomy}.
We then define

\begin{equation*}
n \leq m \iff [n = m \text{ or } n \in m]
\end{equation*}

Then, \((\mathbb{N}, \leq)\) forms a \myautoref[{well ordering}]{develop--math3274:relations:page--order.adoc---well-ordering}.

Additionally, we have the following results

\begin{itemize}
\item \(n \leq m \iff n^+ \leq m^+ \)
\item \(n \leq m \iff \exists k \in \mathbb{N}: n + k = m\)
\item \(n \leq m \iff n + p \leq m + p\)
\item \(n \leq m \implies n\times p \leq m \times p\)
\item If \(p \neq 0\) then \(n \times p \leq m \times p \implies n \leq m\)
\end{itemize}

\begin{example}[{Proof}]
\begin{example}[{Proof of 1}]
Let \(m \in \mathbb{N}\) and

\begin{equation*}
S = \{n \in \mathbb{N} : m \leq n \implies m^+ \leq n^+\}
\end{equation*}

Then, notice that \(0 \in S\) since

\begin{equation*}
m \leq 0 \implies m = 0 \implies m^+ = 0^+ \implies m^+ \leq 0^+
\end{equation*}

Next, if \(n \in S\) then,

\begin{equation*}
m \leq n^+ \implies [m \in n^+\ \lor\ m = n^+ ]
\end{equation*}

In the second case, the result holds immediately. However, if \(m \in n^+ = n \cup \{n\}\),
either

\begin{itemize}
\item \(m \in n\) which implies \(m^+ \in n^+ \) since \(n \in S\) and hence
    \(m^+ \in n^+ \subseteq (n^+ )^+ \)
\item \(m = n\) which implies \(m^+ = n^+ \in (n^+ )^+ \)
\end{itemize}

In both cases we get the desired result and hence \(n^+ \in S\).
Then \(S\) is a successor set and

\begin{equation*}
\forall m, n \in \mathbb{N}: m \leq n \implies m^+ \leq n^+
\end{equation*}

Now, for the converse direction, suppose that \(m^+ \leq n^+ \).
Then, we have two cases

\begin{itemize}
\item If \(m^+ = n^+ \) by peano axiom \(3\) we get \(m = n\) and hence \(m \leq n\)
\item If \(m^+ \in n^+ = n \cup \{n\}\) Then, one of the following occur
    
    \begin{itemize}
    \item \(m^+ \in n\) which implies \(m \in m^+ \subseteq n\)
    \item \(m^+ = n\) which implies \(m \in m^+ = n\)
    \end{itemize}
\end{itemize}

In all cases we get that \(m \leq n\) and hence we are done.
\end{example}

\begin{example}[{Proof of 2}]
Let \(b \in \mathbb{N}\) and

\begin{equation*}
S = \{a \in \mathbb{N}: a \leq b \iff \exists k \in \mathbb{N}: a + k = b\}
\end{equation*}

Then \(0 \in S\) since \(0 \leq b\) and \(\exists k = b\) such that \(0 + b = b\).

Next, suppose that \(a \in S\), then if \(a^+ \leq b\),
\(a \leq b\) and hence \(\exists k\) such that \(a + k = b\).
Then note that \(k \neq 0\) otherwise \(a = b\) and hence \(b \leq a^+ \leq b\)
which would imply that \(a^+ = b = a\). Therefore \(\exists v \in \mathbb{N}\)
such that \(v^+ = k\). Then,

\begin{equation*}
a^+ + v = a + v^+ = b
\end{equation*}

Conversely, suppose that \(\exists k \in \mathbb{N}\) such that \(a^+ + k = b\)
then \(a + k^+ = b\) and hence \(a \leq b\).
This implies that \(a^+ \leq b\) since \(a \neq b\).

Therefore \(a^+ \in S\) and by peano axiom \(3\), \(S = \mathbb{N}\)
and we are done.
\end{example}

\begin{example}[{Proof of 3}]
Let \(m, n \in \mathbb{N}\) and

\begin{equation*}
S = \{p \in \mathbb{N}: m \leq n \iff m + p \leq n + p\}
\end{equation*}

Then clearly \(0 \in S\) since \(m + 0 = m \leq n = n + 0\).
Next, suppose that \(p \in S\) then from property 1,

\begin{equation*}
m \leq n \iff m + p \leq n + p \iff m+p^+ = (m + p)^+ \leq (n + p)^+ = n + p^+
\end{equation*}

Therefore \(p^+ \in S\) and hence \(S = \mathbb{N}\) and we are done.
\end{example}

\begin{example}[{Proof of 4}]
Let \(m, n \in \mathbb{N}\) such that \(n \leq m\) and

\begin{equation*}
S = \{p \in \mathbb{N}: n \times p \leq m \times p\}
\end{equation*}

Then, \(0 \in S\) since \(n\times 0 = 0 = m \times 0\).
Next, suppose that \(p \in \mathbb{N}\), then

\begin{equation*}
n \times p \leq m \times p \implies n \times p^+ = (n \times p) + p \leq (m \times p) + p = m\times p^+
\end{equation*}

from property 2. Therefore \(p^+ \in S\) and \(S = \mathbb{N}\)
\end{example}

\begin{example}[{Proof of 5}]
Let \(m, n \in \mathbb{N}\) and

\begin{equation*}
S = \{p \in \mathbb{N}: n\times p^+ \leq m \times p^+ \implies n \leq m\}
\end{equation*}

Then, clearly \(0 \in S\) since \(n\times 0^+ = n\).
Next, suppose that \(p \in S\), then
by property 2,

\begin{equation*}
\begin{aligned}
&(n\times p^+) + p = n \times (p^+)^+ \leq m \times (p^+)^+ = (m\times p^+) + p
\\&\implies n\times p^+ \leq m\times p^+
\\&\implies n\leq m
\end{aligned}
\end{equation*}

hence \(p^+ \in S\) and \(S = \mathbb{N}\)
\end{example}
\end{example}
\end{document}
