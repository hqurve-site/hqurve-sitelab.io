\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Croissants}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\begin{figure}[H]\centering
\includegraphics[]{images/develop-food/croissants/final-4}
\caption{I really love this image}
\end{figure}

\begin{admonition-important}[{}]
This recipe is based on (ingredients and general method)
the recipe for `Parisian Croissants' from the cookbook
`On Baking: A textbook of baking \& Pastry Fundamentals' (3rd Edition) by Sarah Labensky,
Priscilla Martel and Eddy van Damme.
\end{admonition-important}

\section{Ingredients}
\label{develop--food:croissants:page--index.adoc---ingredients}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{Flour} & {2 lb 4 oz + excess for easy manipulation} \\
\hline
{Salt} & {2-3 tsp} \\
\hline
{Granulated sugar} & {5 oz} \\
\hline
{Milk} & {21 fl oz} \\
\hline
{Active dry yeast} & {1 oz} \\
\hline
{Unsalted butter (softened)} & {1 lb 8 oz} \\
\hline
{Egg wash} & {} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\subsection{Notes}
\label{develop--food:croissants:page--index.adoc---notes}

\begin{itemize}
\item For milk, I utilize 330ml of Nestle carnation evaporated milk (someone is going
    to be upset) mixed with water.
    
    \begin{example}[{Images}]
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/milk-0}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/milk-1}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/milk-2}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/milk-3}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/milk-4}
    \end{figure}
    \end{example}
\item I use margarine instead of butter. Specifically Blue Band Creamy. (someone is going to
    be happy; does Nestle even make margarine?)
    
    \begin{example}[{Images}]
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/margarine-0}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/margarine-1}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/margarine-2}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/margarine-3}
    \end{figure}
    \end{example}
\item Eggwash is just the white of the egg with a little water. To obtain, crack a hole in the egg shell, such that the yolk
    cannot escape, and drain.
\item I believe that, like other recipes, milk can be used instead of eggwash. Its purpose is simply
    to give the golden finish.
\item Although I have never attempted it, the recipe does not depend on the amount
    made and as such can be scaled as necessary. Additionally, at the scale
    described here, mixing the dough may put strain on your mixer.
\item The recipe is very basic and not very sensitive to the exact amount of each ingredient used.
\end{itemize}

\section{Instructions}
\label{develop--food:croissants:page--index.adoc---instructions}

\begin{enumerate}[label=\arabic*)]
\item Mix the flour, salt and sugar
    
    \begin{admonition-tip}[{}]
    Mixing is done with a dough hook
    \end{admonition-tip}
\item Warm milk to 32°C and stir in the yeast.
    
    \begin{admonition-tip}[{}]
    If you, like I, do not own a thermometer, test the temperature with your finger. An
    exact measurement is not necessary. Your body temperature is
    about 37°C while room temperature is about 25°C.
    \end{admonition-tip}
\item Add the milk and yeast into dry ingredients and mix for 10 minutes
\item Place the dough into floured bowl to rise.
    
    \begin{admonition-tip}[{}]
    Upon mixing, the dough would likely be in one ball and would by quite
    sticky. Instead of flouring the bowl, add flour around the ball so that it is easy to handle
    \end{admonition-tip}
\item Allow the dough to rise for about one hour.
    
    \begin{admonition-important}[{}]
    It is very important that the dough rises as much as possible as to not affect the
    folding process. As such you can periodically check the dough to see when the
    rising process becomes very slow.
    \end{admonition-important}
    
    \begin{example}[{Images}]
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/pre-rise}
    \caption{Pre rise}
    \end{figure}
    
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/post-rise}
    \caption{Post rise}
    \end{figure}
    \end{example}
\item While allowing the dough to rise, shape the butter into a flat rectangle (approximately
    20cm x 27.5cm) between two sheets of plastic wrap and chill.
    
    \begin{example}[{Images}]
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/margarine-rolled}
    \end{figure}
    \end{example}
\item After the dough has risen, divide it into 5-6 balls and divide the butter into
    the same number of smaller rectangles each with about a 2:3 ratio
    
    \begin{admonition-note}[{}]
    Although the base recipe does not state to divide the dough, it becomes much
    more manageable if it is done.
    \end{admonition-note}
    
    \begin{admonition-note}[{}]
    The divisions of butter need not be single pieces. That is, you can (and likely
    would need to) cut and combine.
    \end{admonition-note}
    
    \begin{enumerate}[label=\alph*)]
    \item Flatten each of the balls of dough into large rectangles where one side
        length is slightly larger than that of the butter while the other is three times
        the length of the butter.
    \item Place the butter in the middle of the dough and wrap it.
        
        \begin{example}[{Images}]
        \begin{figure}[H]\centering
        \includegraphics[]{images/develop-food/croissants/folding}
        \end{figure}
        \end{example}
    \item Wrap in plastic wrap and chill for about 30 minutes
    \item Roll the dough into a long rectangle and fold into a thirds
        
        \begin{example}[{Images}]
        \begin{figure}[H]\centering
        \includegraphics[]{images/develop-food/croissants/folding-1}
        \end{figure}
        \end{example}
    \item Repeat 2-3 times for a total of about 4 folds,
        
        \begin{example}[{Images}]
        \begin{figure}[H]\centering
        \includegraphics[]{images/develop-food/croissants/post-fold}
        \end{figure}
        \end{example}
    \end{enumerate}
    
    \begin{admonition-tip}[{}]
    If the dough begins bursting and butter becomes messy, stop. If you proceed
    you would loose a lot of butter.
    \end{admonition-tip}
    
    \begin{admonition-tip}[{}]
    In reality, breaks need not be taken between folds, as long as but butter remains
    firm.
    \end{admonition-tip}
\item Chill overnight
\item Roll the dough into a large as possible rectangle with a thickness of
    0.5mm and cut into triangles.
    
    \begin{example}[{Images}]
    \begin{figure}[H]\centering
    \includegraphics[]{images/develop-food/croissants/cuts}
    \end{figure}
    \end{example}
\item For each triangle, flatten into larger triangle and then roll into the shape of
    a croissant.
\item Place onto a large baking sheet, apply eggwash and bake at 375°F until the desired colour is reached
    (15-20 minutes)
\end{enumerate}
\end{document}
