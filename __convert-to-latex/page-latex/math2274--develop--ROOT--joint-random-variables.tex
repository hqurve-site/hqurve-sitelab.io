\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Joint Random Variables}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Joint probability mass/density function}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---joint-probability-massdensity-function}

Let \(X\) and \(Y\) be two random variables.

\subsection{Discrete Random Variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---discrete-random-variables}

If \(X\) and \(Y\) are discrete, their joint
probability (mass) function, \(f_{XY}\), is defined by

\begin{equation*}
f_{XY}(x, y) = P(X = x \text{ and } Y =y)
\end{equation*}

This joint probability function shares the same properties of a single
random variables. That is

\begin{enumerate}[label=\arabic*)]
\item \(0 \leq f_{XY}(x, y) \leq 1\) \(\forall (x, y)\).
\item \(\displaystyle\sum_x\sum_y f_{XY}(x,y) = 1\)
\end{enumerate}

\subsection{Continuous Random Variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---continuous-random-variables}

If \(X\) and \(Y\) are continuous, their joint
probability density function is the function, \(f_XY\), such
that for all regions \(E \subseteq \mathbb{R}^2\)

\begin{equation*}
P[(X, Y) \in E] = \iint_E f_{XY}(x,y) dA
\end{equation*}

Similarly, this joint probability density function shares the same
properties as the a single random variable. That is

\begin{enumerate}[label=\arabic*)]
\item \(f_{XY}(x,y) \geq 0 \; \forall (x, y) \in \mathbb{R}^2\)
\item \(\iint_{\mathbb{R}^2} f_{XY}(x, y)\, dA = 1\)
\end{enumerate}

\section{Probability Distribution Function}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---probability-distribution-function}

Let \(X\) and \(Y\) be random variables. Then their
joint probability distribution function, \(F_{XY}\), is
defined by

\begin{equation*}
F_{XY}(x, y) = P(X \leq x \text{ and } Y \leq y)
\end{equation*}

and completely defined the joint distribution of \(X\) and
\(Y\).

\begin{admonition-note}[{}]
This function follows similar properties to those identified in
the single variable case.
\end{admonition-note}

\section{Marginal Distribution}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---marginal-distribution}

The marginal distribution of \(X\) is strictly the
distribution of \(X\) without taking into account any of the
other variables. The marginal distribution is defined as follows

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,h] Q[c,h]}, measure=vbox}
{\(\displaystyle f_X(x) = \sum_y f_{XY}(x,y)\)} & {\(\displaystyle f_X(x) = \int_{\mathbb{R}} f_{XY}(x, y)\, dy\)} \\
{Discrete Random Variables} & {Continuous Random Variables} \\
\end{tblr}
\end{table}

Note that the marginal distribution of \(Y\) is defined
analogously.

\section{Conditional Distribution}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---conditional-distribution}

The conditional distribution \(X\) given \(Y=y\) is
the distribution of \(X\) given that \(Y\) is
already known. It is defined by

\begin{equation*}
f_{X|Y=y}(x) = \frac{f_{XY}(x,y)}{f_Y(y)}
\end{equation*}

The formulation is the same in both discrete and continuous cases
however note that they resolve to probability mass and density functions
respectively.

\section{Independence}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---independence}

Two random variables \(X\) and \(Y\) are independent
if and only if

\begin{equation*}
f_{XY} = f_X(x) f_Y(y) \forall(x,y)
\end{equation*}

Note that pairwise independent does not imply mutual independence.

\section{Expectation}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---expectation}

The expectation of a function \(g(X, Y)\) is defined as

\begin{table}[H]\centering
\begin{tblr}{colspec={Q[c,h] Q[c,h]}, measure=vbox}
{\( \displaystyle E[g(X, Y)] = \sum_x\sum_y g(x, y) f_{XY}(x, y) \)} & {\( \displaystyle E[g(X, Y)] = \iint_{\mathbb{R}^2} g(x, y) f_{XY}(x,y) dA\)} \\
{Discrete Random Variables} & {Continuous Random Variables} \\
\end{tblr}
\end{table}

Similarly to the single variable case, the expectation is a linear
transformation where \(E[1] = 1\).

\subsection{Products of independent variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---products-of-independent-variables}

If \(X \perp Y\) then

\begin{equation*}
E[XY] = E[X]E[Y]
\end{equation*}

In fact this holds for any functions of the random variables. That is

\begin{equation*}
X\perp Y \implies E[g(X)h(Y)] = E[g(X)]E[h(Y)]
\end{equation*}

for any two functions \(g\) and \(h\).

\section{Extension to more than 2 random variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---extension-to-more-than-2-random-variables}

The above formulations and definitions can be extended to more than
\(2\) random variables. The language used describing them were
done in such a way to make this easily apparent.

\section{Covariance}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---covariance}

The covariance of two random variables \(X\) and
\(Y\) is a measure of the linear relationship between
\(X\) and \(Y\). It is defined by

\begin{equation*}
Cov[X, Y] = \sigma_{XY} = E[(X- E[X])(Y-E[Y])]
\end{equation*}

Futhermore, it can be simplified as follows

\begin{equation*}
\begin{aligned}
    Cov[X, Y]
    &= E[(X- E[X])(Y-E[Y])]\\
    &= E[XY - E[X]Y - E[Y]X + E[X]E[Y])]\\
    &= E[XY] - E[X]E[Y] - E[Y]E[X] + E[X]E[Y]\\
    &= E[XY] - E[X]E[Y]\end{aligned}
\end{equation*}

The latter formula is much simpler to used in practice when compared to
the definition.

\subsection{Note}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---note}

The covariance (as hinted by its name) is a sort of extension to the
concept of variance and has the following two properties

\begin{enumerate}[label=\arabic*)]
\item \(Cov[X, a] = E[(X - E[X])(a - E[a])] = 0\)
\item \(\begin{aligned}
        Cov[X + a, Y + b]
        &= E[(X + a - E[X + a])(Y + b - E[X + b])]\\
        &= E[(X- E[X])(Y-E[Y])]\\
        &= Cov[X, Y]
    \end{aligned}\)
\end{enumerate}

\subsection{Covariance as an inner product}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---covariance-as-an-inner-product}

The covariance can be viewed as an inner product over the vector space
of random variables. That is, it exhibits the following properties

\begin{enumerate}[label=\arabic*)]
\item \(Cov[X, X] = Var[X] \geq 0\)
\item \(Cov[X, X] = 0 \implies\) \(X\) is almost always
    constant. ie \(P(X = E[X]) =1\)
\item \(Cov[X, Y] = E[(X - E[X])(Y - E[Y])] = Cov[Y, X]\)
\item \(\begin{aligned}
        Cov[aX, Y]
        &= E[(aX - E[aX])(Y-E[Y])] \\
        &= a E[(X- E[X])(Y - E[Y])]\\
        &= aCov[X, Y]
    \end{aligned}\)
\item \(\begin{aligned}
        Cov[X + Y, Z]
        &= E[(X + Y - E[X + Y])(Z - E[Z])]\\
        &= E[(X- E[X])(Z - E[Z]) + (Y- E[Y])(Z - E[Z])]\\
        &= Cov[X, Z] + Cov[Y, Z]
    \end{aligned}\)
\end{enumerate}

Therefore, the covariance exhibits the Cauchy-Schwartz inequality below

\begin{equation*}
\left| Cov[X, Y] \right| \leq \sqrt{Var[X]}\sqrt{Var[Y]} = \sigma_X \sigma_Y
\end{equation*}

and is additionally bilinear

\subsection{Covariance of independent variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---covariance-of-independent-variables}

If \(X \perp Y\) then

\begin{equation*}
Cov[X, Y] = E[XY] - E[X]E[Y] = 0
\end{equation*}

\section{Linear combinations of Random Variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---linear-combinations-of-random-variables}

If \(\{X_i\}\) are a set of jointly distributed random
variables, a linear combination of them is also a random variable. That
is,

\begin{equation*}
X = a_1 X_1 + a_2 X_2 + \cdots + a_n X_n
\end{equation*}

is a random variable. Then, its expectation and variance are easily
obtainable and have values of

\begin{equation*}
E[X] = a_1 E[X_1] + a_2 E[X_2] + \cdots a_n E[X_n]
\end{equation*}

\begin{equation*}
Var[X] = \sum_{i=1}^n a_i^2 Var[X_i] + 2\sum_{i < j} a_ia_j Cov[X_i, X_j]
\end{equation*}

which follow immediately from the linearity and bilinearity of the
expectation and covariance.

\subsection{Normally distributed random variables}
\label{develop--math2274:ROOT:page--joint-random-variables.adoc---normally-distributed-random-variables}

If each of the \(X_i\) are normally distributed (not
necessarily independent), then \(X\) is also normally
distributed as

\begin{equation*}
X \sim N(\mu, \sigma^2)
\end{equation*}

where \(\mu = E[X]\) and \(\sigma^2 = Var[X]\).
\end{document}
