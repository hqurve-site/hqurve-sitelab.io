\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Differentiation}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Definition}
\label{develop--math3277:differentiation:page--index.adoc---definition}

Let \(A\subseteq\mathbb{R}\), \(f: A \to \mathbb{R}\) and \(x_0\) is an
interior point of \(A\). \(f\) is \emph{differentiable} at \(x_0\) if

\begin{equation*}
f'(x_0) = \lim_{h\to 0} \frac{f(x_0 + h) - f(x_0)}{h}
\end{equation*}

exists. In this case, \(f'(x_0)\) is called the \emph{derivative} of \(f\) at \(x_0\).

\begin{admonition-note}[{}]
We also write \(f'(x)\) as \(\frac{df(x)}{dx}\) or \(Df(x)\).
\end{admonition-note}

\subsection{Handed Derivatives}
\label{develop--math3277:differentiation:page--index.adoc---handed-derivatives}

Let \(f:A \to \mathbb{R}\) where \(x_0\) is an interior point of \(A \cap [x_0, \infty)\). Then
the \emph{right hand derivative} is defined as

\begin{equation*}
f'_+(x_0) = \lim_{x\to x_0^+} \frac{f(x) - f(x_0)}{x - x_0}
\end{equation*}

if the limit exists. Similarly, if \(x_0\) is an interior point of \(A \cap (-\infty, x_0]\), then
the \emph{left hand derivative} is defined as

\begin{equation*}
f'_-(x_0) = \lim_{x\to x_0^-} \frac{f(x) - f(x_0)}{x - x_0}
\end{equation*}

if the limit exists. Then, \(f\) is differentiable at \(x_0\) iff

\begin{equation*}
f'_+(x_0) = f'_-(x_0)
\end{equation*}

in which case \(f'(x_0) = f'_+(x_0) = f'_-(x_0)\)

\subsection{Differentiability on sets}
\label{develop--math3277:differentiation:page--index.adoc---differentiability-on-sets}

Let \(f: A\to \mathbb{R}\).

If \(A\) is an open set, we say that \(f\) is differentiable on \(A\) if \(f\)
is differentiable at every point in \(A\). Furthermore, if \(f'\) is continuous,
we say that \(f\) is \emph{continueously differentiable} on \(A\).

\begin{admonition-warning}[{}]
To fix. what happens if \(A\) is closed?
\end{admonition-warning}

However, if \(A\) is closed, we say that \(f\) is differentiable on \(A\) if \(f\)
is differentiable on

\subsection{Higher Order derivatives}
\label{develop--math3277:differentiation:page--index.adoc---higher-order-derivatives}

We say that, \(f\) is \emph{n times differentable} (where \(n\in \mathbb{N}\) on open set \(A\) if
\(f^{(k)}\) is differentable for all \(0 \leq k \leq n\). Furthermore if \(f\) has derviatives
of all orders \(n \in \mathbb{N}\), we say that \(f\) is \emph{infinitely differentiable}.

We can classify these functions as follows

\begin{enumerate}[label=\arabic*)]
\item The set of all continuous functions is denoted by \(C(I) = C^0(I)\).
\item The set of all functions which are n-times differentiable with continuous \(n\)\textsuperscript{th} derivative is
    denoted by \(C^n(I)\).
\item The set of all functions which are infinitely differentiable is denoted by \(C^\infty (I)\). Furthermore,
    these functions are called \emph{smooth}.
\end{enumerate}

\subsection{Caratheodory's Definition of the Derivative}
\label{develop--math3277:differentiation:page--index.adoc---caratheodorys-definition-of-the-derivative}

The function \(f\) defined on open set \(A\) is differentable
at \(a \in A\) iff there exists a function \(\phi\) that
is continuous at \(x=a\) and

\begin{equation*}
\forall x \in A: f(x) - f(a) = \phi(x) (x-a)
\end{equation*}

This was also referred to as "Proposition C" in class/assignments.

Reference: \url{https://doi.org/10.2307/44153724}

\begin{admonition-important}[{}]
Big thanks to the person who shared this with me (even though I don't know your name).
\end{admonition-important}

\section{Geometric Interpretation}
\label{develop--math3277:differentiation:page--index.adoc---geometric-interpretation}

\begin{admonition-warning}[{}]
TODO
\end{admonition-warning}

\section{Sums, products and ratios}
\label{develop--math3277:differentiation:page--index.adoc---sums-products-and-ratios}

Let \(A \subseteq \mathbb{R}\) and \(f, g: A \to \mathbb{R}\). Then if \(f\) and \(g\)
are differentiable at interior point \(x_0\) in \(A\),

\begin{enumerate}[label=\arabic*)]
\item Hello
\end{enumerate}

\section{Chain Rule}
\label{develop--math3277:differentiation:page--index.adoc---chain-rule}

Let \(f: A \to \mathbb{R}\), \(g: B \to \mathbb{R}\), where \(A, B \subseteq R\),
\(f(A) \subseteq B\), \(x_0 \in int(A)\) and \(f(x_0) \in int(B)\). Then,
if \(f\) is differentiable at \(x_0\) and \(g\) is differentiable at \(f(x_0)\),
\(g \circ f: A \to \mathbb{R}\) is differentiable at \(x_0\) and

\begin{equation*}
(g\circ f)'(x_0) = g'(f(x_0))f'(x_0)
\end{equation*}

\section{Differentiability of inverses}
\label{develop--math3277:differentiation:page--index.adoc---differentiability-of-inverses}

Let \(f: A \to B\) be bijective with inverse \(f^{-1}: B \to A\)
where \(A, B \subseteq \mathbb{R}\). Then, if \(f\) is differentiable at \(x_0\)
and \(f^{-1}\) is differentiable at \(f(x_0)\), \(f'(x_0) \neq 0\) and

\begin{equation*}
(f^{-1})'(f(x_0)) = \frac1{f'(x_0)}
\end{equation*}

\section{Theorem: Differentiability implies continuity}
\label{develop--math3277:differentiation:page--index.adoc---theorem-differentiability-implies-continuity}

Let \(A \subseteq \mathbb{R}\) and \(x_0\) be an interior point of \(A\). If \(f: A\to \mathbb{R}\)
is differentiable at \(x_0\), then \(f\), is continuous at \(x_0\)

\begin{example}[{Proof}]
All we need to show is that \(f(x_0) = \lim_{x \to x_0} f(x)\)

\begin{equation*}
\begin{aligned}
f(x_0)
&= \lim_{x\to x_0} f(x_0)
\\&= \lim_{x\to x_0} \left[(x_0-x)\frac{f(x) - f(x_0)}{x-x_0} + f(x) \right]
\\&= \left(\lim_{x\to x_0} (x_0-x)\right)
        \left(\lim_{x\to x_0}\frac{f(x) - f(x_0)}{x-x_0}\right)
        + \lim_{x\to x_0}f(x)
\\&= 0 f'(x_0) + \lim_{x\to x_0} f(x)
\\&= \lim_{x\to x_0} f(x)
\end{aligned}
\end{equation*}

Notice that \(\lim_{x \to x_0} f(x)\) exists since all other acommanying limits exist
\end{example}

\section{Some examples}
\label{develop--math3277:differentiation:page--index.adoc---some-examples}

\begin{enumerate}[label=\arabic*)]
\item The set of polynomials \(\mathbb{R}[X]\) are differentiable on \(\mathbb{R}\)
\item Circular functions \(\sin\) and \(\cos\) are both differentiable on \(\mathbb{R}\)
\item The exponential function \(e^x\) is differentiable on \(\mathbb{R}\)
\end{enumerate}
\end{document}
