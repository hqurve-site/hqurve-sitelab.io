\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Product Topology}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-note}[{}]
This section contains finite products. For infinite products please see \myautoref[{}]{develop--math6620:special-topologies:page--arbitrary-product.adoc}.
\end{admonition-note}

Let \((X, \mathcal{T}_X)\) and \((Y, \mathcal{T}_Y)\) be topological spaces.
Then, the \emph{product topology} is the topology on \(X\times Y\) generated by the following basis

\begin{equation*}
\mathcal{B} = \left\{ U\times V \ \middle| \ U \in \mathcal{T}_X \text{ and } V \in \mathcal{T}_Y\right\}
\end{equation*}

Note that the topology generated contains elements which are not in \(\mathcal{B}\).
For example, the following image illustrates three elements in \(\mathcal{B}\)
whose union is not in \(\mathcal{B}\).

\begin{figure}[H]\centering
\includegraphics[width=0.3\linewidth]{images/develop-math6620/special-topologies/product}
\end{figure}

\begin{proof}[{Basis}]
Consider arbitrary \(\bigcup \mathcal{B} = X\times Y\) since \((X\times Y) \in \mathcal{B}\) (since both are opens sets).

Next, consider \(U_1\times V_1, U_2\times V_2 \in \mathcal{B}\). Then

\begin{equation*}
(U_1\times V_1) \cap (U_2\times V_2) = (U_1\cap U_2) \times (V_1\cap V_2) \in \mathcal{B}
\end{equation*}

Therefore, \(\mathcal{B}\) is a basis for \(X\times Y\).
\end{proof}

\begin{proposition}[{Alternative basis}]
Let \((X, \mathcal{T}_X)\) and \((Y, \mathcal{T}_Y)\) be topological spaces
with bases \(\mathcal{B}_X\) and \(\mathcal{B}_Y\) respectively.
Then, the following also forms a basis for the product topology

\begin{equation*}
\left\{ B_X\times B_Y \ \middle| \ B_X \in \mathcal{B}_X \text{ and } B_Y \in \mathcal{B}_Y\right\}
\end{equation*}

Let \(\mathcal{T}\) be the product topology.
Let \(\mathcal{B}'\) be the set stated above and \(\mathcal{T}'\) be the generated topology.
We need to prove that \(\mathcal{B}'\) is a basis for \(X\times Y\) and \(\mathcal{T}' = \mathcal{T}\).

\begin{proof}[{Basis}]
First, notice that

\begin{equation*}
\bigcup \mathcal{B}'
= \bigcup_{B_X\in \mathcal{B}_X, B_Y\in \mathcal{B}_Y} B_X\times B_Y
= \left(\bigcup_{B_X \in \mathcal{B}_X} B_X\right) \times \left(\bigcup_{B_Y \in \mathcal{B}_Y} B_Y\right)
= X \times Y
\end{equation*}

Next, consider \(B_X^{(1)} \times B_Y^{(1)}, B_X^{(2)} \times B_Y^{(2)} \in \mathcal{B}'\).
Then,

\begin{equation*}
\left(B_X^{(1)} \times B_Y^{(1)}\right) \cap \left(B_X^{(2)} \times B_Y^{(2)}\right)
= \left(B_X^{(1)}\cap B_X^{(2)}\right) \cap \left(B_Y^{(1)}\cap B_Y^{(2)}\right)
\end{equation*}

and consider arbitrary \((x,y)\) in the above set. Since \(\mathcal{B}_X\) and \(\mathcal{B}_Y\)
are bases for \(X\) and \(Y\), there exists \(B_X^{(3)} \in \mathcal{B}_X\)
and \(B_Y^{(3)} \in \mathcal{B}_Y\) such that
\(x \in B_X^{(3)} \subseteq B_X^{(1)}\cap B_X^{(2)}\)
and
\(y \in B_Y^{(3)} \subseteq B_Y^{(1)}\cap B_Y^{(2)}\).
Then, we have found an element in \(\mathcal{B}'\) such that
\((x,y) \in B_X^{(3)} \times B_Y^{(3)} \subseteq \left(B_X^{(1)} \times B_Y^{(1)}\right) \cap \left(B_X^{(2)} \times B_Y^{(2)}\right)\).
Therefore, \(\mathcal{B}'\) is a basis for \(X\times Y\).
\end{proof}

\begin{proof}[{Generated topologies are the same}]
To prove that the generated topologies are the same, we compare the bases
using \myautoref[{the previously discussed theorem}]{develop--math6620:ROOT:page--basis.adoc--comparing-topologies}.
We need only prove that elements in each basis are open in the other's generated topology.

First consider \(U\times V \in \mathcal{B}\). We need to prove that \(U\times V \in \mathcal{T}'\).
Consider arbitrary \((x,y) \in U\times V\). Then, since \(\mathcal{B}_X\) and
\(\mathcal{B}_Y\) are bases, there exists \(B_X \in \mathcal{B}_X\)
and \(B_Y \in \mathcal{B}_Y\) such that

\begin{equation*}
x \in B_X \subseteq U
\quad\text{and}\quad
y \in B_Y \subseteq V
\end{equation*}

Therefore, we have found \(B_X \times B_Y \in \mathcal{B}'\) such that
\((x,y) \in B_X\times B_Y \subseteq U\times V\),
and we have that \(\mathcal{T} \subseteq \mathcal{T}'\).

Next, consider \(B_X\times B_Y \in \mathcal{B}'\).
Since \(B_X \in \mathcal{T}_X\) and \(B_Y \in \mathcal{T}_Y\), we have that

\begin{equation*}
\forall (x,y) \in B_X\times B_Y: \exists (B_X \times B_Y) \in \mathcal{T}: (x,y) \in (B_X \times B_Y) \subseteq (B_X \times B_Y)
\end{equation*}

Therefore, \(\mathcal{T}' \subseteq \mathcal{T}\) and the two generated topologies are equal.
\end{proof}
\end{proposition}

\section{Subbasis}
\label{develop--math6620:special-topologies:page--product.adoc---subbasis}

Let \(X\) and \(Y\) be topological spaces. Then, we define the \emph{projection maps}

\begin{equation*}
\begin{aligned}
&p_X: X\times Y \to X \quad\text{st } p_X(x,y) = x
\\
&p_Y: X\times Y \to Y \quad\text{st } p_X(x,y) = y
\end{aligned}
\end{equation*}

Then the following is a subbasis for the product topology on \(X \times Y\)

\begin{equation*}
\begin{aligned}
&
\left\{p_X^{-1}(U) \ \middle|\ U \text{is open in }X\right\}
\cup\left\{p_Y^{-1}(V) \ \middle|\ V \text{is open in }Y\right\}
\\&=
\left\{U\times Y \ \middle|\ U \text{is open in }X\right\}
\cup\left\{X\times V \ \middle|\ V \text{is open in }Y\right\}
\end{aligned}
\end{equation*}

Note that the usual basis elements for the product topology may be generated by simply taking the intersections of the left and right inverses,
and the above set is subset of the usual product topology. Therefore, it generates the same product topology.

\begin{admonition-tip}[{}]
Another subbasis may be generated using basis elements for \(X\) and \(Y\) instead.
\end{admonition-tip}

\begin{admonition-tip}[{}]
The product topology is the coarsest topology such that \(p_X\) and \(p_Y\) are continuous.

\begin{proof}[{}]
Let \(T \subseteq \mathcal{P}(X \times Y)\) be a topology such that \(p_X\) and \(p_Y\) are continuous.
To prove that the product topology is coarser than \(T\) we only need to show that
the basis elements of the product topology are in \(T\).

Consider basis element \(U \times V\) of the product topology. Then, since \(p_X\)
is continuous, \(p_X^{-1}(U) = U\times Y\) is open in \(\mathcal{T}\).
Likewise \(p_Y^{-1}(V) = X \times V\) is open in \(\mathcal{T}\).
Then, their intersection \(U \times V = (U\times Y) \cap (X\times V) \in \mathcal{T}\).

Since \(\mathcal{T}\) contains all the basis elements of the product topology,
we conclude that the product topology is coarser than \(\mathcal{T}\).
\end{proof}
\end{admonition-tip}

\section{Generalization to infinite Cartesian products}
\label{develop--math6620:special-topologies:page--product.adoc---generalization-to-infinite-cartesian-products}

\begin{admonition-note}[{}]
This is discussed in \myautoref[{}]{develop--math6620:special-topologies:page--arbitrary-product.adoc}.
\end{admonition-note}

In a discussion after class, it was realized that we must be careful when defining the product topology
in infinite Cartesian products.
Let \(\{X_n\}\) be a sequence of topological spaces and \(X\) be the Cartesian product.
Then, the following generates different topologies on \(X\)

\begin{description}
\item[Using usual definition] The basis for this has the form
\end{description}

\begin{equation*}
\left\{
U_1\times U_2\cdots
\ | \ U_n \subseteq X_n \text{ is open}
\right\}
\end{equation*}

\begin{description}
\item[Using subbasis] The basis has the form
    
    \begin{equation*}
    U_1 \times U_2 \times \cdots U_N \times X_{N+1} \times X_{N+2} \cdots
    \ | \ N \geq 0 \text{ and } U_n \subseteq X_n \text{ is open}
    \end{equation*}
    
    Due to the finite intersections when forming the basis using the subbasis, there are only finitely many components
    of the basis elements which are strict subsets of the \(X_n\).
    For this reason, any open set in the generated topology must have a similar trailing set of \(X_n\).
\end{description}

\section{Product topology and metric topology in \(\mathbb{R}^n\)}
\label{develop--math6620:special-topologies:page--product.adoc---product-topology-and-metric-topology-in-latex-backslash-latex-backslashmathbb-latex-openbracer-latex-closebrace-latex-caretn-latex-backslash}

The product topology and metric topology in \(\mathbb{R}^n\) are the same in \(\mathbb{R}^n\).
In this proof we consider the euclidean norm; however, all norms are equivalent in \(\mathbb{R}^n\)
(see \href{https://en.wikipedia.org/wiki/Norm\_(mathematics)\#Equivalent\_norms}{Wikipedia article for equivalent norms}).
Also, the euclidean norm is a bit more difficult since the region is not the direct product of spaces.

\begin{admonition-note}[{}]
Using the basis definition of product topologies, it is clear that we can consider finite products without worrying
about the order of composition.
\end{admonition-note}

For the basis of the product topology, we use the open intervals. For the basis of the metric topology, we use
symmetric open balls.

\begin{itemize}
\item Consider \(Y=(a_1, b_1)\times\cdots \times (a_n, b_n)\) in the product topology and arbitrary \(\vec{x} \in Y\)
    Define \(d = \min_i \min\{x_i - a_i, b_i - x_i\}\).
    Then, \(d> 0\) and \(B(\vec{x}, d)\) contains \(\vec{x}\) and is a subset of \(Y\).
\item Consider arbitrary \(B(\vec{a}, d)\) and arbitrary \(\vec{x} \in B(\vec{a}, d)\).
    Let \(\varepsilon = \frac{d - \|\vec{x} - \vec{a}\|}{\sqrt{n}}\).
    We claim that the set \(Y = (x_1-\varepsilon, x_1+\varepsilon)\times\cdot\times(x_n-\varepsilon, x_n+\varepsilon)\)
    is in \(B(\vec{a}, d)\).
    Notice
    
    \begin{equation*}
    \forall \vec{y} \in Y:
    \|\vec{y} - \vec{a}\|
    \leq \|\vec{a}-\vec{x}\| + \|\vec{x} - \vec{y}\|
    < \|\vec{a} - \vec{x}\| + \varepsilon\sqrt{n}
    = d
    \end{equation*}
    
    Therefore, we have that \(\vec{x} \in Y\subseteq B(\vec{a}, d)\).
\end{itemize}

Therefore, the two topologies are equal.

\begin{admonition-note}[{}]
We could easily adjust this proof for norms of the form \(\sqrt[k]{ \sum |x_i|^k}\)
\end{admonition-note}
\end{document}
