\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Non-parametric statistics}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% life is a lie
\def\bigchi{\chi}
\DeclareMathOperator{\SST}{SST}
\DeclareMathOperator{\SSE}{SSE}
\DeclareMathOperator{\SSTr}{SSTr}
\DeclareMathOperator{\SSB}{SSB}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\MSTr}{MSTr}
\DeclareMathOperator{\MSB}{MSB}

\def\bm#1{\mathbf #1}

% Title omitted
Non-parametric tests have few (if any) restrictions on the population
which the sample data comes from and as such, they are sometimes called
\emph{distribution-free tests}. This is unlike previous methods which
required that the sample come from a population with a specific
distribution for example a normal distribution. As a result,
non-parametric methods have the following advantages

\begin{itemize}
\item They can be applied to many different data as they do not require that
    the population have any specific distribution or that it be known at
    all.
\item They can be applied to qualitative (categorical) data unlike
    parametric tests which use quantitative data.
\item Typically, the computations are much simpler and easier to understand
    and apply.
\item More resilient to outliers.
\end{itemize}

However, there are also several disadvantages:

\begin{itemize}
\item Non-parametric tests waste information about the sample data.
    Intuitively, this is caused by placing the data into categories hence
    loosing the exact values in the process.
\item Non-parametric tests are less efficient and require more evidence.
    That is, they require larger sample sizes and greater differences to
    produce a significant findings.
\end{itemize}

\section{Sign Test}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---sign-test}

The \emph{sign test} is a test about the median of a population. It has no
requirements beside that the sample data must be randomly selected.
Additionally, this is the non-parametric equivalent one-sample t-test.
We use the following command to conduct the test from the 'BSDA' package

\begin{listing}[{}]
SIGN.test(data, md=m, alternative="less", conf.level=0.95)
\end{listing}

where 'm' is the median in the null hypothesis. In this case, our
hypotheses were

\begin{itemize}
\item H\textsubscript{0}: median \(=m\)
\item H\textsubscript{1}: median \(<m\)
\end{itemize}

Additionally, the sign test can be used to test matched pairs of data by
taking the differences. This is the non-parametric equivalent of the
paired t-test.

Finally, the sign-test can be used for nominal (categorical) data by
coding each of the states by a numerical value. This is sometimes called
the \emph{xsort} method.

\section{Wilcoxon Signed Rank test}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---wilcoxon-signed-rank-test}

Like the sign-test, the Wilcoxon signed rank test is a test about the
median of a population and is another equivalent of the one-sample
t-test. However, there is the additional requirement that the
distribution of the population is symmetric. It is performed with the
following command

\begin{listing}[{}]
wilcox.test(data, mu=m, alternative="less", conf.level=0.95)
\end{listing}

which has identical hypotheses as the sign-test. Additionally, the above
command can be used with paired data as follows

\begin{listing}[{}]
wilcox.test(data1, data2, mu=0, alternative="less", conf.level=0.95, paired=TRUE)
\end{listing}

Note that you can still just supply the difference of the two vectors,
as was done in the sign-test above.

\subsection{Wilcoxon Rank-Sum test for two independent samples}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---wilcoxon-rank-sum-test-for-two-independent-samples}

The wilcoxon test can also be used to test the medians of two
independent samples however, it has the additional requirement that each
sample contains at least \(10\) observations. This test is
sometimes also called the \emph{Mann-Whitney Test} and is performed as
follows

\begin{listing}[{}]
wilcox.test(data1, data2, mu=0, alternative="less", conf.level=0.95, paired=FALSE, correct=FALSE)
\end{listing}

Note that the 'correct' flag is used to signify whether continuity
correction is to be used. The hypotheses are

\begin{itemize}
\item H\textsubscript{0}: median of data1 - median of data2 \(= 0\)
\item H\textsubscript{1}: median of data1 - median of data2 \(< 0\)
\end{itemize}

\section{Kruskal-Wallis Test}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---kruskal-wallis-test}

The \emph{Kruskal-Wallis Test} (also called the H-test) is the non-parametric
equivalent of the completely randomized design (one-way ANOVA). This
test does not require normal distributions however, it requires that we
have at least three independent random samples, each with at least
\(5\) observations. Our hypotheses are

\begin{itemize}
\item H\textsubscript{0}: all populations have the same median.
\item H\textsubscript{1}: at least one population has a different median.
\end{itemize}

We use the following variables for the test

\begin{itemize}
\item \(N\) - total number of observatiosn in all samples combined
\item \(k\) - number of samples
\item \(n_i\) - number of observations in sample \(i\).
\item \(R_i\) - sum of ranks in sample \(i\)
\end{itemize}

In order to produce the test statistic, we do the following

\begin{enumerate}[label=\arabic*)]
\item Combine all of the samples into one list (remembering the sample from
    which each observation came from)
\item Sort the list and assign each observation its ''rank'' (ie position).
    Note that if two observations have a tie, there are different approaches
    to assigning the rank. For example if sample \(1\) and
    \(2\) both contain a obervation of value \(5\) to
    take ranks \(10\) and \(11\) respectively, we can
    
    \begin{itemize}
    \item assign both rank \(10\)
    \item assign both rank average \(10.5\)
    \item assign them \(10\) and \(11\) randomly
        
        The method which is used should be consistent. Note according to the
        wikipedia article, the average of the ranks should be used.
    \end{itemize}
\item calculate the sum of the ranks \(R_i\) for each of the
    samples.
\item calculate the test statistic \(H\) as follows
    
    \begin{equation*}
    H = \frac{12}{N(N+1)}\left(\frac{R_1^2}{n_1} + \frac{R_2^2}{n_2} + \cdots + \frac{R_k^2}{n_k}\right)
                - 3(N+1)
    \end{equation*}
\end{enumerate}

The test statistic \(H\) follows an approximate chi-squared
distribution with \(k-1\) degrees of freedom and a right
tailed test is performed. All the above calculations can be done in R
with the following command

\begin{listing}[{}]
kruskal.test(dependent ~ independent, data=data)
\end{listing}

Also, like the ANOVA, ensure that the independent variables are factors
by using the 'as.factor' command.

\section{Chi-squared goodness of fit}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---chi-squared-goodness-of-fit}

The chi-squared goodness of fit test is used to determine how well a
sample matches a given distribution. Firstly, the sample space is
partitioned into \(k\) categories each with \(p_i\)
probability of occurring. Also, let \(n\) be the sample size
and \(Y_i\) be the number of observations for the
\(i\)’th category. Then, our test statistic is given by

\begin{equation*}
\bigchi^2_{obs} = \sum_{i} \frac{(Y_i - np_i)^2}{np_i}
\end{equation*}

which follows a \(\bigchi^2_{k-1-r}\) degrees
of freedom where \(r\) is the number of estimated parameters.
Additionally, if any of the \(np_i\) are less than
\(5\), we merge the category with another one.

We then perform a left-tailed test with the following hypotheses

\begin{itemize}
\item H\textsubscript{0}: \(p_1 = ..,\ p_2 = ..,\  \cdots \ p_k = ..\) that is
    each of the events have a specified probability of occurring.
\item H\textsubscript{1}: at
    least one of the \(p_i\) are not as specified.
\end{itemize}

\section{Chi-squared test for homogeneity}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---chi-squared-test-for-homogeneity}

This test is used to determine whether the distributions of multiple
populations are equal. That is, given that there are \(h\)
populations and \(k\) categories, we are testing whether the
probability of each of the populations occurring in each of the
categories are the same. As can be seen already, in this test, the size
of the \(h\) populations are fixed before collecting data
while the amount that falls into each category is measured. We then use
the following symbols

\begin{itemize}
\item \(N\) is the total number of observations.
\item \(n_i\) is the amount of observations in the sample of the
    \(i\)’th population
\item \(y_{ij}\) is the number of observations in the
    \(i\)’th sample that fall into the \(j\)’th
    category. Note that \(\sum_{j=1}^k y_{ij} = n_i\).
\item \(\hat{p}_{ij} = \frac{y_{ij}}{n_i}\) is the estimated
    proportion of the \(i\)’th sample that falls in the
    \(j\)’th category.
\item \(y_{j} = \sum{i=1}^h y_{ij}\) is the total number of
    observations in the \(j\)’th category.
\item \(\hat{p}_j = \frac{y_{j}}{N}\) is the estimated proportion
    of the entire population that falls in the \(j\)’th category.
\end{itemize}

For visualization see the table below.

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetCell[r=2, c=2]{}{} &  & \SetCell[r=1, c=4]{}{Category, \(j\)} &  &  &  & {} \\
\hline
 &  & {\(1\)} & {\(2\)} & {\(\cdots\)} & {\(k\)} & {Total} \\
\hline
\SetCell[r=4, c=1]{}{Sample, \(i\)} & {\(1\)} & {\(y_{11}\)} & {\(y_{12}\)} & {\(\cdots\)} & {\(y_{1k}\)} & {\(n_1\)} \\
\hline
 & {\(2\)} & {\(y_{21}\)} & {\(y_{22}\)} & {\(\cdots\)} & {\(y_{2k}\)} & {\(n_2\)} \\
\hline
 & {\(\vdots\)} & {\(\vdots\)} & {\(\vdots\)} & {\(\ddots\)} & {\(\vdots\)} & {\(\vdots\)} \\
\hline
 & {\(h\)} & {\(y_{h1}\)} & {\(y_{h2}\)} & {\(\cdots\)} & {\(y_{hk}\)} & {\(n_h\)} \\
\hline
{} & {Total} & {\(y_1\)} & {\(y_2\)} & {\(\cdots\)} & {\(y_k\)} & {\(N\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Then, our hypotheses are

\begin{itemize}
\item H\textsubscript{0}: \(p_{1j} = p_{2j} = \cdots p_{hj} = p_{j}\) for all
    \(j=1\ldots k\). That is, the probability of occurring in
    the \(j\)’th category is not dependent on the population which
    the observation came from.
\item H\textsubscript{1}: at least one of the
    \(p_{ij}\) are not as specified.
\end{itemize}

We then perform a right-tailed test on the test statistic

\begin{equation*}
\bigchi^2_{obs} = \sum_{i=1}^h \sum_{j=1}^k \frac{(y_{ij} - n_i\hat{p}_j)^2}{n_i \hat{p}_j}
\end{equation*}

which has a distribution of
\(\bigchi^2_{(h-1)(k-1)}\).

\section{Chi squared test for independence}
\label{develop--math2275:ROOT:page--nonparametric-statistics.adoc---chi-squared-test-for-independence}

Although the test for independence looks similar to the test for
homogeneity, there is one key difference, there is only one population.
For the test of independence, we first take a fixed sample of size
\(n\) and place observations into categories of two variables
\(A\) and \(B\). We then use the following notation

\begin{itemize}
\item \(a\) is the number of categories in variable
    \(A\).
\item \(b\) is the number of categories in variable
    \(B\).
\item \(y_{ij}\) is the number of observations in the
    \(i\)’th category of variable \(A\) and the
    \(j\)’th category of variable \(B\).
\item \(\hat{p}_{ij} = \frac{y_{ij}}{n}\) is the estimated
    probability of falling into the \(i\)’th category of variable
    \(A\) and the \(j\)’th category of variable
    \(B\).
\item \(\hat{p}_{i.} = \sum_{j=1}^b \hat{p}_{ij}\) is the
    estimated probability of falling into the \(i\)’th category of
    variable \(A\).
\item \(\hat{p}_{.j} = \sum_{i=1}^a \hat{p}_{ij}\) is the
    estimated probability of falling into the \(j\)’th category of
    variable \(B\).
\end{itemize}

For visualization see the table below

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetCell[r=2, c=2]{}{} &  & \SetCell[r=1, c=4]{}{Variable \(B\), \(j\)} &  &  &  & {} \\
\hline
 &  & {\(1\)} & {\(2\)} & {\(\cdots\)} & {\(b\)} & {Total} \\
\hline
\SetCell[r=4, c=1]{}{Variable \(A\), \(i\)} & {\(1\)} & {\(y_{11}\)} & {\(y_{12}\)} & {\(\cdots\)} & {\(y_{1b}\)} & {\(y_{1.}\)} \\
\hline
 & {\(2\)} & {\(y_{21}\)} & {\(y_{22}\)} & {\(\cdots\)} & {\(y_{2b}\)} & {\(y_{2.}\)} \\
\hline
 & {\(\vdots\)} & {\(\vdots\)} & {\(\vdots\)} & {\(\ddots\)} & {\(\vdots\)} & {\(\vdots\)} \\
\hline
 & {\(h\)} & {\(y_{a1}\)} & {\(y_{a2}\)} & {\(\cdots\)} & {\(y_{ab}\)} & {\(y_{a.}\)} \\
\hline
{} & {Total} & {\(y_{.1}\)} & {\(y_{.2}\)} & {\(\cdots\)} & {\(y_{.b}\)} & {\(n\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Then, our hypotheses are

\begin{itemize}
\item H\textsubscript{0}: Variables \(A\) and \(B\) are independent.
    That is \(P(A_i \cap B_j) = P(A_i)\times P(B_j)\) for all
    \(i=1\ldots a\) and \(j=1\ldots b\).
\item H\textsubscript{1}: Variables \(A\) and \(B\) are not independent.
\end{itemize}

We then perform a right-tailed test with the test statistic

\begin{equation*}
\bigchi^2_{obs} = \sum_{i=1}^a \sum_{j=1}^b \frac{\left(y_{ij} - \frac{y_{i.}y_{.j}}{n}\right)^2 }{\frac{y_{i.}y_{.j}}{n}}
\end{equation*}

which has a distribution of
\(\bigchi^2_{(a-1)(b-1}\). In order to
understand the test statistic notice that under the null hypothesis,

\begin{equation*}
np_{ij} = nP(A_i \cap B_j) = nP(A_i)P(B_j)
    \approx n \hat{p}_{i.}\hat{p}_{j.}
    = n\left(\frac{y_{i.}}{n}\right)\left(\frac{y_{.j}}{n}\right)
    = \frac{y_{i.}y_{.j}}{n}
\end{equation*}
\end{document}
