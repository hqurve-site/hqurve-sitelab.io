\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Continued Fractions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

{} \def\floor#1{\left\lfloor{ #1 }\right\rfloor}
% Title omitted
Let \(a_0 \in \mathbb{Z}\) and \(a_1, \ldots a_n \in \mathbb{Z}^+\). Then
the following is a \emph{simple continued fraction}

\begin{equation*}
[a_0; a_1, \ldots a_n] = a_0 + \cfrac{1}{a_1 + \cfrac{1}{a_2 + \frac{1}{\ddots}}}
\end{equation*}

Then notice that for each rational number \(r\), the continued fraction is almost uniquely determined
since we could define the sequence \(r_0, \ldots r_n\) as follows.

\begin{enumerate}[label=\arabic*)]
\item Let \(i=0\) and \(r_0 =r\)
\item Then \(0 \leq r_i - \floor{r_i} < 1\). If it zero we stop
\item Then let \(r_{i+1} = \frac{1}{r_i - \floor{r_i}} > 1\)
\item Increment \(i\) and go to step 2
\end{enumerate}

Then we let \(a_i = \floor{r_i}\). Then, this process generates a simple continued fraction
representation for \(r\). Next notice that a continued fraction is almost unique
since the denominators of all but the last fraction must be strictly greater than one since it
is the sum of a positive integer and positive number. However, for the last
we have two options, we can either write \(a_n\) or \((a_n-1) + \frac11\).
Then we get the following equality

\begin{equation*}
[a_0; a_1, a_2,\ldots a_n] = [a_0; a_1, a_2, \ldots (a_n -1), 1]
\end{equation*}

To solve this we could specify that we require the continued fraction to be the smaller of the two
by saying that \(a_n \neq 1\) unless \(r=1\).

\section{Nearly simple continued fractions}
\label{develop--math2400:continued-fractions:page--index.adoc---nearly-simple-continued-fractions}

Let \(\alpha > 1\) be a real number and \(a_0 \in \mathbb{Z}\) and \(a_1, \ldots a_n \in \mathbb{Z}^+\).
A \emph{nearly simple continued fraction} is

\begin{equation*}
[a_0; a_1, a_2, \ldots a_n, \alpha]
\end{equation*}

\section{Continued fractions for irrational numbers}
\label{develop--math2400:continued-fractions:page--index.adoc---continued-fractions-for-irrational-numbers}

By conducting the process outlined above, we can generate the continued fraction representation for any
irrational number. This representation is infinite and know that it is unique (since \(r_i \notin \mathbb{Z}\))
however, we need to know whether it makes sense to say that this representation '`equals'' the irrational
number and in what sense.

Let \(c\) be our irrational number then, we can find real number \(\alpha_n > 1\) (by
conducting the process) such that

\begin{equation*}
c = [a_0; a_1, \ldots a_n, \alpha_n]
\end{equation*}

Then, we define

\begin{equation*}
c_n = [a_0; a_1, \ldots a_n] = \frac{p_n}{q_n}
\end{equation*}

to be the \emph{n\textsuperscript{th} convergent} of \(c\) where \(gcd(p_n, q_n) =1\).
We would show that the sequence \(\{c_n\}\) converges to \(c\).

\subsection{Recurrence relation for \(\frac{p_n}{q_n}\)}
\label{develop--math2400:continued-fractions:page--index.adoc--recurrence-of-convergents}

Let \(\beta \in \mathbb{R}\) then

\begin{equation*}
[a_0; a_1, a_2, \ldots a_n, \beta] = \frac{\beta p_n + p_{n-1}}{\beta q_n + q_{n-1}}
\end{equation*}

In particular,

\begin{equation*}
p_{n+1} = a_{n+1} p_n + p_{n-1}
\quad\text{and}\quad
q_{n+1} = a_{n+1} q_n + q_{n-1}
\end{equation*}

and

\begin{equation*}
p_{n+1}q_n - p_nq_{n+1} = (-1)^n
\end{equation*}

which would additionally imply that \(gcd(p_n, q_n) =1\)
and we have indeed found \(p_n\) and \(q_n\).

\begin{example}[{Proof}]
We would do this by induction. Then we say that \(P(n)\) denotes that the following statements are true

\begin{equation*}
[a_0; a_1, a_2, \ldots a_n, \beta] = \frac{\beta p_n + p_{n-1}}{\beta q_n + q_{n-1}}
\end{equation*}

and

\begin{equation*}
p_{n}q_{n-1} - p_{n-1}q_{n} = (-1)^{n-1}
\end{equation*}

where \(n \geq 1\)

\discretefloatingtitle{Base case \(n=1\)}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(n\)} & {} & {\(p_n\)} & {\(q_n\)} \\
\hline[\thicktableline]
{\(0\)} & {\([a_0] = a_0 = \frac{a_0}{1}\)} & {\(a_0\)} & {\(1\)} \\
\hline
{\(1\)} & {\([a_0; a_1] = a_0 + \frac{1}{a_1} = \frac{a_0a_1 + 1}{a_1}\)} & {\(a_0 a_1 + 1\)} & {\(a_1\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

also

\begin{equation*}
p_1q_0 - p_0q_1 = (a_0a_1 + 1)(1) - a_0(a_1) = 1 = (-1)^0
\end{equation*}

Then,

\begin{equation*}
[a_0; a_1, \beta]
= a_0 + \frac{1}{a_1 + \frac{1}{\beta}}
= a_0 + \frac{\beta}{a_1\beta + 1}
= \frac{a_0a_1\beta + a_0 + \beta}{a_1\beta + 1}
= \frac{(a_0a_1 + 1)\beta + a_0}{a_1\beta + 1}
= \frac{p_1\beta + p_0}{q_1\beta + q_0}
\end{equation*}

Hence \(P(1)\) is true.

\discretefloatingtitle{Inductive Step}

Suppose \(P(n)\) holds for some \(n \geq 1\).
Then, firstly by setting \(\gamma = a_{n+1}\) we get that

\begin{equation*}
[a_0; a_1, a_2, \ldots a_n, a_{n+1}]
= \frac{a_{n+1} p_n + p_{n-1}}{a_{n+1} q_n + q_{n-1}}
\end{equation*}

Then \(p_{n+1} = a_{n+1}p_n + p_{n-1}\) and \(q_{n+1} = a_{n+1}q_n + q_{n-1}\) and

\begin{equation*}
\begin{aligned}
p_{n+1}q_{n} - p_{n}q_{n+1}
&= (a_{n+1}p_n + p_{n-1})q_n - p_n(a_{n+1}q_n + q_{n-1})
\\&= p_{n-1}q_n - p_nq_{n-1}
\\&= -(p_nq_{n-1} - p_{n-1}q_n)
\\&= -(-1)^{n-1}
\\&= (-1)^n
\end{aligned}
\end{equation*}

Next, by setting \(\gamma = a_{n+1} + \frac{1}{\beta}\), we get

\begin{equation*}
\begin{aligned}
&[a_0; a_1, a_2, \ldots a_n, a_{n+1}, \beta]
\\&= \left[a_0; a_1, a_2, \ldots a_n, a_{n+1} + \frac{1}{\beta}\right]
\\&= \frac{\left(a_{n+1} + \frac{1}{\beta}\right) p_n + p_{n-1}}{\left(a_{n+1} + \frac{1}{\beta}\right) q_n + q_{n-1}}
\\&= \frac{\beta a_{n+1}p_n + p_n + \beta p_{n-1}}{ \beta a_{n+1}q_n + q_n + \beta q_{n-1}}
\\&= \frac{\beta (a_{n+1}p_n + p_{n-1}) + p_n}{ \beta(a_{n+1}q_n + q_{n-1}) + q_n}
\\&= \frac{\beta p_{n+1} + p_n}{\beta q_{n+1} + q_n}
\end{aligned}
\end{equation*}

Hence, \(P(n+1)\) is true.
\end{example}

\subsection{Convergence of the convergents}
\label{develop--math2400:continued-fractions:page--index.adoc---convergence-of-the-convergents}

\(\{c_n\}\) converges to \(c\), in particular

\begin{equation*}
c_{2n} < c_{2n+2} < c < c_{2n+3} < c_{2n+1}
\end{equation*}

and

\begin{equation*}
\left|c_{n} - c_{n-1}\right|
= \left|\frac{p_n}{q_n} - \frac{p_{n-1}}{q_{n-1}}\right|
= \left|\frac{p_nq_{n-1} - q_np_{n-1}}{q_n q_{n-1}}\right|
= \left|\frac{(-1)^{n-1}}{q_n q_{n-1}}\right|
= \frac{1}{q_n q_{n-1}}
\end{equation*}

Then since \(q_n \to \infty\) by using our
\myautoref[{recurrence relation}]{develop--math2400:continued-fractions:page--index.adoc--recurrence-of-convergents}, we see that
\(\{c_n\} \to c\) as \(n\to \infty\).

Furthermore,
notice that this proves that each infinite continued fraction
has a real limit since \(\mathbb{R}\) is complete.

\begin{example}[{Proof}]
To show that the inequality holds, we would show that \(c_{2n} < c < c_{2n+1}\)
and then show that \(|c_{n} - c|\) is strictly decreasing.

\discretefloatingtitle{Inequality}

Recall that

\begin{equation*}
c_{2n} = \frac{p_{2n}}{q_{2n}}
,\quad
c_{2n+1} = \frac{p_{2n+1}}{q_{2n+1}} = \frac{a_{2n+1} p_{2n} + p_{2n-1}}{a_{2n+1}q_{2n} + q_{2n-1}}
,\quad
c = \frac{\alpha_{2n}p_{2n} + p_{2n-1}}{\alpha_{2n}q_{2n} + q_{2n-1}}
\end{equation*}

where \(\alpha_{2n} = a_{2n+1} + \frac{1}{\alpha_{2n+1}} > a_{2n+1}\). Then, we define

\begin{equation*}
\begin{aligned}
f(t)
&= \frac{t p_{2n} + p_{2n-1}}{t q_{2n} + q_{2n-1}}
\\&= \frac{t p_{2n} + \frac{p_{2n}q_{2n-1}}{q_{2n}} - \frac{p_{2n}q_{2n-1}}{q_{2n}}+ p_{2n-1}}{t q_{2n} + q_{2n-1}}
\\&= \frac{\frac{p_{2n}}{q_{2n}} (t q_{2n} + q_{2n-1}) - \frac{p_{2n}q_{2n-1}}{q_{2n}}+ p_{2n-1}}{t q_{2n} + q_{2n-1}}
\\&= \frac{p_{2n}}{q_{2n}} + \frac{- \frac{p_{2n}q_{2n-1}}{q_{2n}}+ p_{2n-1}}{t q_{2n} + q_{2n-1}}
\\&= \frac{p_{2n}}{q_{2n}} + \frac{- p_{2n}q_{2n-1}+ p_{2n-1}q_{2n}}{(t q_{2n} + q_{2n-1})q_{2n}}
\\&= \frac{p_{2n}}{q_{2n}} + \frac{- (p_{2n}q_{2n-1} -p_{2n-1}q_{2n})}{(t q_{2n} + q_{2n-1})q_{2n}}
\\&= \frac{p_{2n}}{q_{2n}} + \frac{- (-1)^{2n-1}}{(t q_{2n} + q_{2n-1})q_{2n}}
\\&= \frac{p_{2n}}{q_{2n}} + \frac{1}{(t q_{2n} + q_{2n-1})q_{2n}}
\end{aligned}
\end{equation*}

which is clearly decreasing.
Then, since
\(c_{2n+1} = f(a_{2n+1})\),
\(c = f(\alpha_{2n})\)
and \(c_{2n} = \lim_{t\to\infty} f(t)\)
we get that

\begin{equation*}
a_{2n+1} < \alpha_{2n} < \infty \implies c_{2n+1} > c > c_{2n}
\end{equation*}

\discretefloatingtitle{Distance between convergent and actual value}

\begin{equation*}
\begin{aligned}
\left|c - c_n\right|
&= \left|\frac{\alpha_n p_n + p_{n-1}}{\alpha_n q_n + q_{n-1}} - \frac{p_n}{q_n}\right|
\\&= \left|\frac{(\alpha_n p_n + p_{n-1})q_n - p_n(\alpha_n q_n + q_{n-1})}{(\alpha_n q_n + q_{n-1})q_n}\right|
\\&= \left|\frac{p_{n-1}q_n - p_n q_{n-1}}{(\alpha_n q_n + q_{n-1})q_n}\right|
\\&= \left|\frac{- (-1)^{n-1}}{(\alpha_n q_n + q_{n-1})q_n}\right|
\\&= \frac{1}{(\alpha_n q_n + q_{n-1})q_n}
\end{aligned}
\end{equation*}

Then since \(q_n\) is strictly increasing, we see that \(|c-c_n|\) is strictly decreasing.
\end{example}

\section{Repeating continued fractions}
\label{develop--math2400:continued-fractions:page--index.adoc---repeating-continued-fractions}

Let \(\alpha \in \mathbb{R}\) and \(\alpha > 1\).
Then if \(\alpha = [a_0; a_1 \ldots a_n, \alpha]\)
we use the following notation

\begin{equation*}
\alpha = [a_0; a_1 \ldots a_n, \alpha] = \overline{[a_0; a_1, \ldots a_n]}
\end{equation*}

Then, \(\alpha\) is a root of a quadratic polynomial since

\begin{equation*}
\alpha = \frac{\alpha p_n + p_{n-1}}{\alpha q_n + q_{n-1}}
\implies \alpha^2 q_n + \alpha (q_{n-1} - p_n) - p_{n-1} = 0
\end{equation*}

Then if we let \(\beta = \overline{[a_n; a_{n-1},\ldots a_0]}\),
we see that
\(\frac{-1}{\beta}\) is the other root of the above polynomial \(q_n x^2 + (q_{n-1} - p_n)x - p_n{n-1}\).

\begin{example}[{Proof}]
Firstly, we write \(\alpha\) in form of the lemma

\begin{equation*}
\alpha = \frac{p_{n-1} + p_n \alpha }{q_{n-1} + q_n\alpha }
\end{equation*}

Also, \(\beta = [a_n; a_{n-1},\ldots a_0, \beta]\)

\begin{equation*}
\begin{aligned}
&\frac{1}{\beta}
= \frac{1}{[a_n; a_{n-1},\ldots a_0, \beta]}
= \frac{1}{\frac{p_{n-1} + q_{n-1}\frac{1}{\beta}}{p_n + q_n \frac{1}{\beta}}}
\\&\implies
q_n \left(\frac{1}{\beta}\right)^2 + (p_n - q_{n-1})\left(\frac{1}{\beta}\right) - p_{n-1} =0
\\&\implies
q_n \left(-\frac{1}{\beta}\right)^2 + (q_{n-1} - p_n)\left(-\frac{1}{\beta}\right) - p_{n-1} =0
\end{aligned}
\end{equation*}
\end{example}

Conversely if \(a x^2 + bx +c =0\) has two irrational roots such that
one is in \((1,\infty)\) and the other in \((-1,0)\), the positive
root is a purely periodic continued fraction.

\begin{example}[{Proof}]
\begin{admonition-important}[{}]
We assume that \(a > 0\). If it isnt, we simply multiply the equation by \(-1\)
\end{admonition-important}

Firstly, let \(D = b^2 - 4ac\) and

\begin{equation*}
A = \left\{
(P, Q) \in \mathbb{Z} \times \mathbb{Z}^+
: \frac{P + \sqrt{D}}{Q} > 1, \ -1 < \frac{P - \sqrt{D}}{Q} < 0, \ \frac{P^2 - D}{Q} \in \mathbb{Z}
\right\}
\end{equation*}

Then we claim that \(A\) is finite. Notice that

\begin{equation*}
\begin{aligned}
(P,Q) \in A
&\implies [ P + \sqrt{D} > Q \text{ and } -Q < P - \sqrt{D} < 0 ]
\\&\implies \sqrt{D} - P < Q < \sqrt{D} + P < 2\sqrt{D}
\\&\implies [0 < P < \sqrt{D} \text{ and } Q < 2\sqrt{D}]
\end{aligned}
\end{equation*}

Therefore \(A\) is bounded and hence finite. Next,
let \(B = \{\frac{P + \sqrt{D}}{Q}: (P,Q) \in A\}\)
and consider arbirary \(\alpha = \frac{P + \sqrt{D}}{Q} \in B\).
Then, \(\frac{1}{\alpha - \floor{\alpha}} \in B\).

\begin{example}[{Proof}]
Firstly, let \(k = \floor{\alpha}\) and

\begin{equation*}
\frac{1}{\alpha - k}
= \frac{Q}{(P - kQ) + \sqrt{D}}
= \frac{(P-kQ)Q - Q\sqrt{D}}{(P-kQ)^2 - D}
= \frac{(kQ - P) + \sqrt{D}}{\frac{D - (kQ-P)^2}{Q}}
\end{equation*}

Firstly, notice that \(D - (kQ - P)^2 \equiv D - P^2 \equiv 0 \mod Q\)
and hence the denominator is an integer and \(\frac{1}{\alpha - k}\) is of the correct form.

We now have four points to prove

\begin{description}
\item[Positive denominator] Since \(k = \floor{\alpha}\) and \(\alpha \notin \mathbb{Z}\), \(0 < k < \frac{P+ \sqrt{D}}{Q}\)
    which implies that \(0 < kQ - P <  \sqrt{D}\). Therefore \((kQ-P)^2 < D\)
    and we are done.
\item[Greater than \(1\)] Since \(k = \floor{\alpha}\), \(0 < \alpha - k < 1\) and by taking the reciprocal,
    we get the desired result.
\item[Conjugate lies in \((-1, 0)\)] Firstly, the conjugate is
    
    \begin{equation*}
    \frac{(kQ - P) - \sqrt{D}}{\frac{D - (kQ-P)^2}{Q}}
    = Q \frac{(kQ - P) - \sqrt{D}}{(\sqrt{D} - (kQ-P))(\sqrt{D} + (kQ-P))}
    = \frac{-Q}{\sqrt{D} + (kQ-P)}
    = \frac{Q}{P-kQ - \sqrt{D}}
    \end{equation*}
    
    Then, since \(-1 < \frac{P - \sqrt{D}}{Q} < 0\), \(-k-1 < \frac{P-kQ - \sqrt{D}}{Q} < -k \leq -1\)
    and hence the reciprocal lies in \((-1,0)\)
\item[Divisibility property] Directly,
    
    \begin{equation*}
    \frac{(kQ -P)^2 - D}{\frac{D - (kQ-P)^2}{Q}} = -Q \in \mathbb{Z}
    \end{equation*}
\end{description}

Therefore, \(\frac{1}{\alpha - k} = \frac{1}{\alpha - \floor{\alpha}} \in B\).
\end{example}

Now, consider the positive root of the equation

\begin{equation*}
\frac{-b + \sqrt{D}}{2a}
\end{equation*}

Then, it lies in \(B\) since \(\frac{(-b)^2 - (b-4ac)}{2a} = 2c \in \mathbb{Z}\). Let \(\alpha_0\), be this root
and we define the sequence

\begin{equation*}
\alpha_{n+1} = \frac{1}{\alpha_n - \floor{\alpha_{n}}}
\end{equation*}

Then since \(B\) is finite, this sequence must be recurrent.
Furthermore, at there exists \(\alpha_k = \alpha_0\)
where \(k > 0\).

\begin{example}[{Proof}]
We would show that each for each \(\alpha_n \in B\),
there exists at most one \(\alpha \in B\) which yields it.

Let \(\alpha = \frac{P + \sqrt{D}}{Q}, \beta = \frac{R + \sqrt{D}}{S}\in B\) such that

\begin{equation*}
\frac{1}{\alpha - \floor{\alpha}} =
\frac{1}{\beta - \floor{\beta}}
\end{equation*}

Then

\begin{equation*}
\frac{P + \sqrt{D}}{Q} = \alpha =
\beta -\floor{\beta} + \floor{\alpha}
= \frac{R + \sqrt{D}}{S} -\floor{\beta} + \floor{\alpha}
\end{equation*}

Then by multiplying through by \(QS\), we see that \(Q=S\).
Let \(\floor{\alpha} - \floor{\beta} = k\). Then

\begin{equation*}
\alpha = \frac{P + \sqrt{D}}{Q} = \beta + k
\end{equation*}

and

\begin{equation*}
\frac{P - \sqrt{D}}{Q} = \frac{R - \sqrt{D}}{S} + k > -1 + k
\end{equation*}

Now, since \(k \in \mathbb{Z}\), \(k = 0\) and we are done.
\end{example}

Furthermore, we define \(a_n = \floor{\alpha_n}\)
and rewrite the recurrence as \(\alpha_n = a_0 + \frac{1}{\alpha_{n+1}}\). Then

\begin{equation*}
\alpha_0 = a_0 + \cfrac{1}{a_1 + \cfrac{1}{\ddots \frac{}{a_{k-1} + \frac{1}{\alpha_k}}}}
= [\overline{a_0; a_1, \ldots a_{k-1}}]
\end{equation*}

and we are done
\end{example}

\subsection{Lemma: Reversing a continued fraction}
\label{develop--math2400:continued-fractions:page--index.adoc---lemma-reversing-a-continued-fraction}

Let \(a_0, \ldots a_n \in \mathbb{Z}^+\) then, if

\begin{equation*}
[a_0; a_1,\ldots a_n, z] = \frac{A + Bz}{C + Dz}
\end{equation*}

then

\begin{equation*}
\frac{1}{[a_n;, a_{n-1}, \ldots a_0; \frac{1}{z}]} = \frac{A + Cz}{B + Dz}
\end{equation*}

\begin{admonition-note}[{}]
\(z\) is just a symbol. All that is really important in this lemma is the form.
\end{admonition-note}

\begin{admonition-note}[{}]
\(A,B,C,D\) are used to simply represent the same number. We could have
alternatively written the second fraction as \(\frac{E + Fz}{I + Jz}\) and equated
coefficients.
\end{admonition-note}

\begin{example}[{Proof}]
As usual, we would prove this by induction.

When \(n=0\), we get that

\begin{equation*}
[a_0; z] = a_0 + \frac{1}{z} = \frac{1 + a_0z}{0 + 1z}
\end{equation*}

While

\begin{equation*}
\frac{1}{[a_0; \frac{1}{z}]} = \frac{1}{a_0 + z} = \frac{1 + 0z}{z_0 + 1z}
\end{equation*}

Therefore, it holds.

Now, suppose that it holds for \(n\). Let

\begin{equation*}
[a_0; a_1,\ldots a_n, z] = \frac{A + Bz}{C + Dz}
\end{equation*}

\begin{equation*}
\frac{1}{[a_n;, a_{n-1}, \ldots a_0; \frac{1}{z}]} = \frac{A + Cz}{B + Dz}
\end{equation*}

Then we get that

\begin{equation*}
[a_0; a_1, \ldots a_n, a_{n+1}, z]
= \left[a_0; a_1, \ldots a_n, a_{n+1} + \frac{1}{z}\right]
= \frac{A + B(a_{n+1} + \frac{1}{z})}{C + D(a_{n+1} + \frac{1}{z})}
= \frac{Az + Ba_{n+1}z + B}{Cz + Da_{n+1}z + D}
= \frac{B + (A + Ba_{n+1})z}{D + (C + Da_{n+1})z}
\end{equation*}

while

\begin{equation*}
\frac{1}{[a_{n+1}; a_n, \ldots a_0; \frac{1}{z}]}
= \frac{1}{a_{n+1} + \frac{1}{[a_n;, a_{n-1}, \ldots a_0; \frac{1}{z}]}}
= \frac{1}{a_{n+1} + \frac{A + Cz}{B + Dz}}
= \frac{B + Dz}{a_{n+1}(B+Dz) + A + Cz}
= \frac{B + Dz}{(A + Ba_{n+1}) + (C + Da_{n+1})z}
\end{equation*}

which is the expected result.
\end{example}

\subsection{Square roots}
\label{develop--math2400:continued-fractions:page--index.adoc---square-roots}

Let \(N \in \mathbb{N}\)  such that \(\sqrt{N} \notin \mathbb{N}\).
Then,

\begin{equation*}
\sqrt{N} = [a; \overline{a_1, a_2, \ldots a_k, a_k, \ldots a_1, 2a}]
\end{equation*}

or

\begin{equation*}
\sqrt{N} = [a; \overline{a_1, a_2, \ldots a_k, \ldots a_1, 2a}]
\end{equation*}

where \(a = \floor{\sqrt{N}}\).

\begin{example}[{Proof}]
Firstly, notice that the equation

\begin{equation*}
x^2 -2a + (a^2 - N) =0
\end{equation*}

has two roots, \(a + \sqrt{N} \in (1,\infty)\) and \(a - \sqrt{N} \in (-1, 0)\).
Hence,

\begin{equation*}
a + \sqrt{N} = [\overline{a_0, \ldots a_n}]
\end{equation*}

and if we let

\begin{equation*}
\beta = [\overline{a_n, \ldots a_0}]
\end{equation*}

\(\frac{-1}{\beta}\) is the other root of the equation, namely \(a - \sqrt{N}\).

\begin{example}[{}]
Clearly the other root must be \(m + n\sqrt{N}\) such that

\begin{equation*}
a + \sqrt{N} + m + n\sqrt{N} \in \mathbb{Z} \implies n+1 =0\implies n = -1
\end{equation*}

Also,

\begin{equation*}
(a+\sqrt{N})(m-\sqrt{N}) = am + (m-a)\sqrt{N} - N \implies m-a = 0\implies m=a
\end{equation*}
\end{example}

That is,

\begin{equation*}
\sqrt{N} - a = \frac{1}{\beta} \implies \sqrt{N} = [a;\overline{a_n, \ldots a_0}] = [a; \overline{a_n, \ldots a_1, a_0}]
\end{equation*}

However, from before

\begin{equation*}
\sqrt{N} = -a + [\overline{a_0, \ldots a_n}] = [-a + a_0; \overline{a_1, \ldots a_n, a_0}]
\end{equation*}

Therefore, \(2a = a_0\) and \(a_i = a_{n-i+1}\) for \(1 \leq i \leq n\) (ie same forward and backwards).
\end{example}
\end{document}
