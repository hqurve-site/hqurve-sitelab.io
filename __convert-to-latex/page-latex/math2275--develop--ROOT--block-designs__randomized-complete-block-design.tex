\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Randomized Complete Block Design}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% life is a lie
\def\bigchi{\chi}
\DeclareMathOperator{\SST}{SST}
\DeclareMathOperator{\SSE}{SSE}
\DeclareMathOperator{\SSTr}{SSTr}
\DeclareMathOperator{\SSB}{SSB}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\MSTr}{MSTr}
\DeclareMathOperator{\MSB}{MSB}

\def\bm#1{\mathbf #1}

% Title omitted
A randomized complete block design consists of \(a\)
treatments which are each randomly allocated to once to \(b\)
blocks. That is, each block contains each of the \(a\)
treatments, each occurring once, in a random ''order.'' Furthermore,
there are (possible) differences between pairs of different blocks,
however, there is to be no difference between different positions in a
particular block.

In a similar manner to the completely random design, this design seeks
to test whether the \(a\) treatment means are the same.

\section{Note}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---note}

I believe that these tests can be done with blocking in more than one
way.

\section{Approach and neccessity of two-way ANOVA}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---approach-and-neccessity-of-two-way-anova}

As with everything else, there are sources of error for everything we
do. We refer to these sources of error as \emph{nuisance factors} and if it
is

\begin{itemize}
\item unknown and uncontrolled: randomization is the technique of choice in
    order to combat it.
\item known but uncontrollable: we observe the behaviour of that particular
    nuisance factor throughout the experiment. In this case analysis of
    covariance is used.
\item known and controllable: we design the experiment around fixed
    instances of that particular factor. We use \emph{blocking} to segment the
    tests which can then be examined while taking into account the value of
    the nuisance factor.
\end{itemize}

In our particular situation, we know that each of the blocks may yield
different results and hence instead of testing all of the treatments on
just one block, we test all of the treatments on all of the blocks. Note
that randomization of treatment order within each of the blocks is
necessary to mitigate against any unknown nuisance factors within each
of the blocks.

After all of the tests are conducted, the information of each of the
treatments within each of the blocks are used to generate information
for each of the treatments independent of the blocks (assuming that the
blocks used are representative of the population of blocks).

\section{The models}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---the-models}

We will denote the observation for the \(i\)’th treatment
within the \(j\)’th block as \(y_{ij}\). Then, as
before, we can write each of the \(y_{ij}\) as

\begin{equation*}
y_{ij} = \mu_{ij} + \varepsilon_{ij}
\end{equation*}

where \(\mu_{ij}\) is the mean of the \(i\)’th
treatment within the \(j\)’th block and
\(\varepsilon_{ij}\) is due to random error. This is called
the \emph{means model} and we assume that each of the
\(\varepsilon_{ij}\) are iid with
\(\varepsilon_{ij} \sim N(0, \sigma^2)\).

We can simplify this in a manner similar to that used in the completely
random design (however, this time requiring an assumption). By assuming
the effect of the treatment and block on the mean are additive, we can
write \(\mu_{ij} = \mu + \alpha_i + \beta_j\). Where
\(\alpha_i\) is the effect of the \(i\)’th treatment
and \(\beta_j\) is the effect of the \(j\)’th block.
Then we get that \(\sum \alpha_i = \sum \beta_j = 0\) and that

\begin{equation*}
y_{ij} = \mu + \alpha_i + \beta_j + \varepsilon_{ij}
\end{equation*}

This model is called the \emph{means model} and would be the basis of our
investigation, furthermore, we obtain that each of the
\(y_{ij}\) are independent and

\begin{equation*}
y_{ij} \sim N(\mu + \alpha_i + \beta_j, \sigma^2)
\end{equation*}

At this point, we can again simplify our hypotheses as

H\textsubscript{0}: \(\alpha_i = 0\ \forall i = 1\ldots k\).
H\textsubscript{1}: at least one \(\alpha_i \neq 0\)

\subsection{Some common notations}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---some-common-notations}

Due to the large use of indices, the following summation conventions are
used

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
{\(\displaystyle y_{i.} = \sum_{j=1}^b y_{ij}\).} & {\(\displaystyle \overbar{y}_{i.} = \frac{1}{b}\sum_{j=1}^b y_{ij}\).} \\
\hline
{\(\displaystyle y_{.j} = \sum_{i=1}^a y_{ij}\).} & {\(\displaystyle \overbar{y}_{.j} = \frac{1}{a}\sum_{i=1}^a y_{ij}\).} \\
\hline
{\(\displaystyle y_{..} = \sum_{i=1}^a\sum_{j=1}^b y_{ij}\).} & {\(\displaystyle \overbar{y}_{..} = \frac{1}{ab}\sum_{i=1}^a\sum_{j=1}^b y_{ij}\).} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

\section{Variations}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---variations}

We define the following

\begin{itemize}
\item \(\displaystyle \text{Total variation} = \SST= \sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})^2\)
\item \(\displaystyle \text{Random variation} = \SSE= \sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{i.} - \overbar{y}_{.j} + \overbar{y}_{..})^2\)
\item \(\displaystyle \text{Variation due to treaments} = \SSTr= b\sum_{i=1}^a (\overbar{y}_{i.} - \overbar{y}_{..})^2\)
\item \(\displaystyle \text{Variation due to blocks} = \SSB= a\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})^2\)
\end{itemize}

To see the rational behind \(\SSE\), notice
that

\begin{equation*}
y_{ij} - \overbar{y}_{i.} - \overbar{y}_{.j} + \overbar{y}_{..}
    = (y_{ij} - \overbar{y}_{..}) - (\overbar{y}_{i.} - \overbar{y}_{..}) - (\overbar{y}_{.j}- \overbar{y}_{..})
\end{equation*}

\subsection{Important result}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---important-result}

We have the following result

\begin{equation*}
\SST= \SSTr+ \SSB+ \SSE
\end{equation*}

\begin{example}[{Proof}]
We will prove that
\(\SSE= \SST- \SSTr- \SSB\).
Now, from above

\begin{equation*}
\begin{aligned}
        &\SSE= \sum_{i=1}^a \sum_{j=1}^b ((y_{ij} - \overbar{y}_{..}) - (\overbar{y}_{i.} - \overbar{y}_{..}) - (\overbar{y}_{.j}- \overbar{y}_{..}))^2\\
        &=\sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})^2 + \sum_{i=1}^a\sum_{j=1}^b (\overbar{y}_{i.} - \overbar{y}_{..})^2 + \sum_{i=1}^a\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})^2\\
        &\quad\quad - 2\sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})(\overbar{y}_{i.} - \overbar{y}_{..}) - 2\sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})(\overbar{y}_{.j} - \overbar{y}_{..})\\
        &\quad\quad + 2\sum_{i=1}^a\sum_{j=1}^b (\overbar{y}_{i.} - \overbar{y}_{..})(\overbar{y}_{.j} - \overbar{y}_{..})\\
        &=\SST+ b\sum_{i=1}^a (\overbar{y}_{i.} - \overbar{y}_{..})^2 + a\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})^2\\
        &\quad\quad - 2\sum_{i=1}^a(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^b (y_{ij} - \overbar{y}_{..}) - 2\sum_{j=1}^b(\overbar{y}_{.j} - \overbar{y}_{..})\sum_{i=1}^a (y_{ij} - \overbar{y}_{..})\\
        &\quad\quad + 2\sum_{i=1}^a(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})\\
        &=\SST+ \SSTr+ \SSB\\
        &\quad\quad - 2\sum_{i=1}^a(\overbar{y}_{i.} - \overbar{y}_{..})b(\overbar{y}_{i.} - \overbar{y}_{..}) - 2\sum_{j=1}^b(\overbar{y}_{.j} - \overbar{y}_{..})a(\overbar{y}_{.j} - \overbar{y}_{..}) + 0\\
        &=\SST+ \SSTr+ \SSB- 2\SSTr-2\SSB\\
        &=\SST- \SSTr- \SSB
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\subsection{Distributions}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---distributions}

Given our effects model and our null hypothesis, we have the following
results

\begin{equation*}
\frac{\SSTr}{\sigma^2} \sim \bigchi_{(a-1)}^2
    \quad\text{and}\quad
    \frac{\SSE}{\sigma^2} \sim \bigchi_{(a-1)(b-1)}^2
\end{equation*}

\begin{example}[{Proof}]
These proofs are almost identical to those done for the CRD.

 ◻
\end{example}

\section{The test}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---the-test}

In order to determine whether each of the \(\alpha_i\) are
zero, we consider the distribution of

\begin{equation*}
F
    = \frac{\left.\frac{\SSTr}{\sigma^2}\right/(a-1)}{\left.\frac{\SSE}{\sigma^2}\right/((a-1)(b-1))}
    \sim F_{(a-1), (a-1)(b-1)}
\end{equation*}

and by letting
\(\tfrac{\SSTr}{a-1} = \MSTr\)
and
\(\tfrac{\SSE}{(a-1)(b-1)} = \MSE\),
we get that

\begin{equation*}
F = \frac{\MSTr}{\MSE} \sim F_{(a-1), (a-1)(b-1)}
\end{equation*}

We then produce the following table

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|Q[c,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Source of variation} & {Sum of Squares (SS)} & {Degrees of freedom (df)} & {Mean square (MS)} & {F-statistic} \\
\hline[\thicktableline]
{Treatments} & {\(\SSTr\)} & {\(a-1\)} & {\(\MSTr= \frac{\SSTr}{a-1}\)} & {\(F = \frac{\MSTr}{\MSE}\)} \\
\hline
{Blocks} & {\(\SSB\)} & {\(b-1\)} & {\(\MSB= \frac{\SSB}{b-1}\)} & {\({\frac{\MSB}{\MSE}}^*\)} \\
\hline
{Random variation} & {\(\SSE\)} & {\((a-1)(b-1)\)} & {\(\MSE= \frac{\SSE}{(a-1)(b-1)}\)} & {} \\
\hline
{Total} & {\(\SST\)} & {\(ab-1\)} & {} & {} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

At which point, we perform a right tailed test on the
\(F-statistic\). We use the following R-code to produce the
test.

\begin{listing}[{}]
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the treatment, blocks and dependent variables
# each row contains a pair (treatment, block, dependent)

is.factor(data$treatment) #ensure that treatment data is a a factor
data$treatment <- as.factor(data$treatment) #convert if necessary

is.factor(data$block) #ensure that block data is a a factor
data$block <- as.factor(data$block) #convert if necessary

result <- lm(dependent ~ treatment + block, data=data)
anova(result)
\end{listing}

Note that since blocking puts a restriction an randomization, the ratio
\(\frac{\MSB}{\MSE}\) does
not have an exact \(F\) distribution, however its magnitude
can be used to determine whether or not blocking was effective.

\section{Model Adequacy checking}
\label{develop--math2275:ROOT:page--block-designs/randomized-complete-block-design.adoc---model-adequacy-checking}

As with the one-way ANOVA test, there were several assumptions made. The
methods used to validate these assumptions are the same as what was done
with the completely random block design.
\end{document}
