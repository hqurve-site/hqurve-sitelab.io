\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Bayesian Inference}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
In Bayesian statistics, we try to incorporate \textbf{prior} knowledge
about the parameters. This information may come from
\textbf{expert witness} who may know some information about the model.

A simple Bayesian model may have samples
\(Y_1,\ldots Y_n\) and parameter \(\theta\).
We define

\begin{description}
\item[Prior distribution] \(\theta \sim f(\theta)\)
\item[Data distribution] \(Y_i | \theta \sim f(y|\theta)\)
\item[Posterior distribution] 
    
    \begin{equation*}
    f(\theta | -) = \frac{f(y_1,\ldots y_n|\theta)f(\theta)}{f(y_1,\ldots y_n)}
    \end{equation*}
    
    \begin{admonition-tip}[{}]
    When computing the posterior, we can instead only look at the joint of
    \(Y_1,\ldots Y_n, \theta\) and drop any factors excluding \(\theta\).
    This yields a function which is proportional to the posterior.
    This works since the marginal is just a constant (since the data is known)
    \end{admonition-tip}
\end{description}

\section{Loss functions}
\label{develop--stat6180:ROOT:page--bayesian.adoc---loss-functions}

After collecting the data, we obtain the posterior distribution
of \(\theta\).
However, we may often want to report a single value for \(\theta\).
To choose the best one, we need to consider which loss function we are using.

The \textbf{loss function} is simply a measure for how \textbf{bad} an estimate \(\hat{\theta}\) for \(\theta\)
is. It is denoted by \(L(\theta, \hat{\theta})\) and is situation specific.
The \textbf{Bayes estimator} is therefore the estimator which minimizes
the loss function. That is

\begin{equation*}
\hat{\theta}_{Bayes}(\vec{y}) = argmin_{\hat{\theta}} E[L(\theta, \hat{\theta})|\vec{y}]
\end{equation*}

Below are several common Bayes estimators

\begin{description}
\item[Mean] \(\hat{\theta}_{Bayes} = E[\theta|\vec{y}]\)
    minimizes \(L(\theta, \hat{\theta}) = (\theta - \hat{\theta})^2\)
\item[Median] \(\int_{\hat{\theta}_{Bayes}}^\infty = \frac12\)
    minimizes \(L(\theta, \hat{\theta}) = |\theta - \hat{\theta}|\)
\item[Mode] \(\hat{\theta}_{Bayes} = argmax f(\theta|\vec{y})\)
    minimizes \(L(\theta, \hat{\theta}) = -I(|\theta-\hat{\theta}| < \varepsilon)\)
    as \(\varepsilon \to 0\).
    This is also called the \textbf{maximum a posterior} (MAP) estimator.
\end{description}

\section{Credible Intervals}
\label{develop--stat6180:ROOT:page--bayesian.adoc---credible-intervals}

If we want a range of values for which we think \(\theta\)
may lie, we need to use \textbf{credible intervals}.
To construct these intervals, we look
at the posterior distribution and choose some \(100(1-\alpha)\%\)
of it.

Let the \(100(1-\alpha)\%\) credible interval be \((L, U)\).
That is

\begin{equation*}
1-\alpha = \int_L^U f(\theta|\vec{y}) \ d\theta
\end{equation*}

There are several popular choices of the credible interval.

\begin{description}
\item[Equal-tailed] where
    
    \begin{equation*}
    \frac{\alpha}{2}
    = \int_{-\infty}^L f(\theta|\vec{y})\ d\theta
    = \int_U^{\infty} f(\theta|\vec{y})\ d\theta
    \end{equation*}
\item[One-sided] Either \(L=-\infty\) or \(U=\infty\)
\item[Highest posterior density (HPD)] \(f(L|\vec{y}) = f(U|\vec{y})\)
    for a uni-modal posterior which also has the shortest interval.
\end{description}

\begin{admonition-note}[{}]
The below image is obtained from STAT6180 lecture notes
in Semester 1 of 2024-2025 and taught by Dr. Isaac Dialsingh.
\end{admonition-note}

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-stat6180/ROOT/bayesian-credible-intervals}
\end{figure}

\section{Choosing priors}
\label{develop--stat6180:ROOT:page--bayesian.adoc---choosing-priors}

The choice of prior drives Bayesian analysis.

\begin{description}
\item[Conjugage] A prior is \textbf{conjugate} if the prior and the posterior
    come from the same family of distributions.
    For example, if \(Y|\theta \sim Bin(n,\theta)\)
    and \(\theta \sim Beta(\alpha, \beta)\),
    \(\theta | Y \sim Beta(\alpha+y, \beta + n-y)\).
\item[Natural conjugate] A conjugate prior is \textbf{natural conjugate} if it
    has the same functional form as the likelihood (of the data).
    For example, \(Y|\theta \sim Bin(n,\theta)\)
    and \(\theta \sim Beta(\alpha, \beta)\)
    
    \begin{equation*}
    f(\theta) \propto \theta^{\alpha-1}(1-\theta)^{\beta-1}
    ,\quad\text{and}\quad
    L(\theta|\vec{y}) \propto \theta^{y}(1-\theta)^{n-y}
    \end{equation*}
    
    \begin{admonition-remark}[{}]
    In a sense, natural conjugates arise from simple mathematical calculations
    instead of long-winded coincidences/simplifications.
    \end{admonition-remark}
\item[Default] A \textbf{default prior} is used when the data analyst is unable or unwilling
    to specify information about the parameter.
    
    Note that it is not always suitable to choose \(f(\theta)\propto 1\).
    For example, if we use change of variable \(\theta = \log\left(\frac{\phi}{1-\phi}\right)\),
    the prior with respect to \(\phi\) is
    
    \begin{equation*}
    f(\phi) \propto f(\theta) J(\phi \to \theta)
    = 1\begin{vmatrix}
    \frac{d}{d\theta} \log \left(\frac{\phi}{1-\phi}\right)
    \end{vmatrix}
    = \theta^{-1}(1-\theta)^{-1}
    \end{equation*}
    
    This is a \(Beta(0,0)\) distribution (which is not a proper distribution).
    Also, note that this indicates that setting \(f(\theta) \propto 1\)
    does not indicate that the prior is constant with respect to
    other parameterizations.
\item[Jeffrey's prior] Jeffrey's prior is one that is invariant with respect to parameterization.
    Is is defined as \(f(\theta)\propto \sqrt{\det I(\theta)}\)
    where \(I(\theta)\) is the fisher information matrix with
    \((i,j)\) entry equal to
    
    \begin{equation*}
    I(\theta)_{ij} = E\left[
        \left(\frac{\partial}{\partial \theta_i}\log f(Y|\theta)\right)
        \left(\frac{\partial}{\partial \theta_j}\log f(Y|\theta)\right)
    \right]
    \end{equation*}
\end{description}

\subsection{Improper priors}
\label{develop--stat6180:ROOT:page--bayesian.adoc---improper-priors}

A prior distribution is \textbf{proper} if \(\int f(\theta) \ d\theta < \infty\),
and otherwise it is \textbf{improper}.

\section{Gibbs Sampler}
\label{develop--stat6180:ROOT:page--bayesian.adoc---gibbs-sampler}

Suppose we have a sample \(Y_1, \ldots Y_n\) which depends
on parameters \(\theta_1, \ldots \theta_d\).
We wish to estimate the distributions of each of the \(\theta_i\).
To do this, we

\begin{enumerate}[label=\arabic*)]
\item Generate an initial guesses for each of the parameters,
    \(\theta^{(t)}_1, \ldots \theta^{(t)}_p\).
\item Now, repeat the process \(N\) times.
    At step \(t\),
    Estimate each of the parameters in turn using the most recent values
    of each of the other parameters.
    That is
    
    \begin{equation*}
    \begin{aligned}
    (\theta_1^{(t+1)}|-)&
        \sim f(\theta_1 | \theta_2^{(t)}, \theta_3^{(t)},\ldots \theta_p^{(t)})\\
    (\theta_2^{(t+1)}|-)&
        \sim f(\theta_2 | \theta_1^{(t+1)}, \theta_3^{(t)},\ldots \theta_p^{(t)})\\
    (\theta_3^{(t+1)}|-)&
        \sim f(\theta_3 | \theta_1^{(t+1)}, \theta_2^{(t+1)}, \theta_4^{(t)},\ldots \theta_p^{(t)})\\
        &\vdots\\
    (\theta_{p-1}^{(t+1)}|-)&
        \sim f(\theta_{p-1} | \theta_1^{(t+1)}, \theta_2^{(t+1)},\ldots \theta_{p-2}^{(t+1)}, \theta_p^{(t)})\\
    (\theta_{p}^{(t+1)}|-)&
        \sim f(\theta_{p} | \theta_1^{(t+1)}, \theta_2^{(t+1)},\ldots \theta_{p-2}^{(t+1)}, \theta_{p-1}^{(t+1)})
    \end{aligned}
    \end{equation*}
\end{enumerate}

Under mild conditions (see 7.2.2 of Computational Statistics)
the limiting distribution of \(\theta_i^{(t)}\) is the marginal
distribution of \(\theta_i\).

An update step is called

\begin{description}
\item[Gibbs update] If we can readily sample from the conditional \(\theta_i | -\).
\item[Metropolis-Hastings update] If we need to use the Metropolis-Hasting algorithm
    to sample \(\theta_i | -\)
\end{description}

\begin{admonition-note}[{}]
The order of the parameters matter.
In general, it should not matter, but we often try to have parameters
before hyperparameters.
This is to ensure that the information gets properly back-propagated
from the data to parameters to hyperparameters.
\end{admonition-note}

\section{Empirical Bayes}
\label{develop--stat6180:ROOT:page--bayesian.adoc---empirical-bayes}

Consider the hierarchical problem where we have \(Y\) depending on \(\lambda\)
where \(\lambda\) has a distribution with hyperparameter \(\theta\).
If we do not know the value of \(\theta\)
nor have an assigned distribution of \(\theta\), we cannot perform
Bayesian analysis.
However, we can instead use a frequentist approach to first
estimate \(\theta\) and then conduct Bayesian analysis using \(\theta = \hat{\theta}\).
This approach is called \textbf{Emperical Bayes}.

Typically, the process of estimating the hyperparameter \(\theta\)
consists of first determining the marginal distribution of the data
and then finding the MLE of the hyperparameter.
If the parameters are used for one observation at a time (as in the following example),
the marginal of the data can be found by finding the product of the marginal of the observations.

\begin{example}[{Poisson with gamma prior and unknown scale parameter}]
Suppose we have

\begin{equation*}
Y_i | \lambda_i \sim Poisson(\lambda_i)
,\quad\text{and}\quad
\lambda_i \sim Gamma(\alpha, \beta)
\end{equation*}

where \(\alpha\) is known but \(\beta\) is unknown.

First, we find the marginal distribution of the \(Y_i\) given \(\beta\),
that is

\begin{equation*}
\begin{aligned}
f(Y_i | \beta)
&= \int_{0}^\infty f(Y_i, \lambda_i|\beta) \ d\lambda_i
\\&= \int_{0}^\infty f(Y_i|\lambda_i)f(\lambda_i | \beta) \ d\lambda_i
\\&= \int_{0}^\infty \frac{e^{-\lambda_i}\lambda_i^{y_i}}{y_i!} \frac{\beta^\alpha}{\Gamma(\alpha)}\lambda_i^{\alpha-1}e^{-\beta\lambda_i} \ d\lambda_i
\\&= \frac{\beta^\alpha}{\Gamma(\alpha)y_i!}\frac{\Gamma(y_i+\alpha)}{(1+\beta)^{y_i+\alpha}}
    \int_{0}^\infty \frac{(1+\beta)^{y_i+\alpha}}{\Gamma(\alpha+y_i)}\lambda^{y_i+\alpha-1}e^{-(1+\beta)\lambda_i} \ d\lambda_i
\\&= \frac{\beta^\alpha}{\Gamma(\alpha)y_i!}\frac{\Gamma(y_i+\alpha)}{(1+\beta)^{y_i+\alpha}}
\\&= \binom{\alpha + Y_i-1}{Y_i} \left(\frac{\beta}{1+\beta}\right)^{\alpha}  \left(1-\frac{\beta}{1+\beta}\right)^{Y_i}
\end{aligned}
\end{equation*}

This is the probability of getting \(\alpha\) successes and \(Y_i\) failures where the last
trial is a success and the probability of success is \(\frac{\beta}{1+\beta}\).
Therefore \(Y_i \sim NegBinom\left(\alpha, \frac{\beta}{1+\beta}\right)\).
We can now find the MLE by constructing the log-likelihood as

\begin{equation*}
\begin{aligned}
l(\beta|\vec{y})
&= \sum_{i=1}^n \ln\binom{\alpha+y_i-1}{y_i}
    + n\alpha\ln\left(\frac{\beta}{1+\beta}\right) + \ln\left(\frac{1}{1+\beta}\right)\sum_{i=1}^n y_i
\\&= \sum_{i=1}^n \ln\binom{\alpha+y_i-1}{y_i}
    + n\alpha\ln \beta - n\alpha\ln(1+\beta)
    - \ln(1+\beta)\sum_{i=1}^n y_i
\end{aligned}
\end{equation*}

We differentiate wrt \(\beta\) and set equal to zero to get

\begin{equation*}
\begin{aligned}
\frac{dl}{d\beta} =
\frac{n\alpha}{\beta} - \frac{n\alpha}{1+\beta} - \frac{1}{n+\beta}\sum_{i=1}^n y_i
&=0
\\
\frac{n\alpha(1+\beta-\beta) -\beta\sum_{i=1}^n y_i}{\beta(1+\beta)}
&=0
\\
n\alpha - \beta \sum_{i=1}^n y_i
&=0
\\
\hat{\beta}_{MLE} &= \frac{n\alpha}{\sum_{i=1}^n y_i} = \frac{\alpha}{\overbar{y}}
\end{aligned}
\end{equation*}

and we can find the mle of \(\beta\) to be \(\frac{\alpha}{\overbar{Y}}\)
\end{example}
\end{document}
