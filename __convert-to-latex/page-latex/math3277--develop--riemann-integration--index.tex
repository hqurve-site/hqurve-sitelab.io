\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Riemann Integration}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
\section{Partitions}
\label{develop--math3277:riemann-integration:page--index.adoc---partitions}

We define a \emph{partition} of the interval
\( [a,b]\) as a finite set \(P = \{x_0\ldots x_n\} \subseteq [a,b]\)
where

\begin{equation*}
a=x_0 < x_1 <\cdots < x_n =b
\end{equation*}

We also define the following

\begin{itemize}
\item \(n\) is the \emph{order} of \(P\)
\item \(x_0\ldots x_n\) are the \emph{nodes} (or \emph{partition points}) of \(P\)
\item \( [x_0,x_1],\ [x_0,x_1]\ldots[x_{n-1},x_n]\) are the \emph{segments} of \(P\).
\item \(\Delta_i\) or \(\Delta x_i\) is the length of the segment \( [x_{i-1},x_i]\)
    and notice that their sum is simply \(b-a\).
\item the \emph{mesh size} or \emph{norm} of \(P\) is defined as \(||P|| = \max\{\Delta_1\ldots \Delta_n\}\)
\item \(\mathcal{P}[a,b]\) as the set of all paritions of interval \( [a,b]\)
\end{itemize}

If all the segments of \(P\) have the same length, \(P\) is called \emph{equidistant} or \emph{regular}.

\begin{admonition-note}[{}]
Some other authors define partions differently.
\end{admonition-note}

\subsection{Refinements}
\label{develop--math3277:riemann-integration:page--index.adoc---refinements}

Let \(P, P^* \in \mathcal{P}[a,b]\). Then \(P^*\) is a \emph{refinement} of \(P\)
if \(P \subseteq P^*\). In such a case notice that

\begin{equation*}
||P^*|| \leq ||P||
\end{equation*}

\subsection{Opertations on partitions}
\label{develop--math3277:riemann-integration:page--index.adoc---opertations-on-partitions}

Since a partition is simply a set, we can perform the usual operations of intersection
and union. In fact, \(\mathcal{P}[a,b]\) is closed under these two operations. Also, note that

\begin{itemize}
\item \(P_1 \cup P_2\) is a refinement of both \(P_1\) and \(P_2\)
\item \(P_1\) and \(P_2\) are refiments of \(P_1 \cap P_2\)
\end{itemize}

\section{Darboux Sums}
\label{develop--math3277:riemann-integration:page--index.adoc---darboux-sums}

Let \(f: [a,b]\to \mathbb{R}\) be bounded and \(P \in \mathcal{P}[a,b]\).
Then, we define

\begin{equation*}
M_i(P, f) = \sup\{f(x): x\in [x_{i-1}, x_i]\} = \sup_{x \in [x_{i-1},x_i]} f(x)
\end{equation*}

\begin{equation*}
m_i(P, f) = \inf\{f(x): x\in [x_{i-1}, x_i]\} = \inf_{x \in [x_{i-1},x_i]} f(x)
\end{equation*}

Then the \emph{upper} and \emph{lower} \emph{Darboux sums} of \(f\) on \([a,b]\) are defined as

\begin{equation*}
U(P, f) =\sum_{i=1}^n M_i(P,f)\Delta x_i
\quad\text{and}\quad
L(P, f) =\sum_{i=1}^n m_i(P,f)\Delta x_i
\end{equation*}

\subsection{Sums and multiples of Darboux Sums}
\label{develop--math3277:riemann-integration:page--index.adoc---sums-and-multiples-of-darboux-sums}

Consider functions, \(f,g: [ a,b] \to \mathbb{R}\)
and \(P \in \mathcal{P}[a,b]\). Then, we have the following

\begin{itemize}
\item If \(k \geq 0\)
    
    \begin{equation*}
    U(P, kf) = kU(P,f)
    \quad\text{and}\quad
    L(P, kf) = kL(P,f)
    \end{equation*}
\item If \(k < 0\)
    
    \begin{equation*}
    U(P, kf) = kL(P,f)
    \quad\text{and}\quad
    L(P, kf) = kU(P,f)
    \end{equation*}
\end{itemize}

\begin{admonition-note}[{}]
these two identities follow directly from analogous identities including
supremum and infimum
\end{admonition-note}

\subsection{Darboux sums of refinements}
\label{develop--math3277:riemann-integration:page--index.adoc---darboux-sums-of-refinements}

Let \(P_1, P_2 \in \mathcal{P}[a,b]\) where \(P_1 \subseteq P_2\),
then

\begin{equation*}
L(P_1, f) \leq L(P_2,f) \leq U(P_2, f) \leq U(P_1,f)
\end{equation*}

This result follows from the fact that if \(A \subseteq B\),
 \(\sup A \leq \sup B\).

\subsection{Bounds for Darboux sums}
\label{develop--math3277:riemann-integration:page--index.adoc---bounds-for-darboux-sums}

Consider bounded function \(f :[a,b]\to\mathbb{R}\). Then, \(\exists M \in \mathbb{R}\)
such that \(f[a,b] \subseteq [-M, M]\) and hence

\begin{equation*}
\forall P \in \mathcal{P}[a,b]: -M(b-a) \leq L(P,f) \leq U(P, f) \leq M(b-a)
\end{equation*}

\section{Reimann integrals}
\label{develop--math3277:riemann-integration:page--index.adoc---reimann-integrals}

Let \(f: [a,b]\to \mathbb{R}\) be a bounded function, then we define

\begin{equation*}
\int_{a}^{\overline{b}} f(x)\ dx = \inf\{U(P,f): P \in \mathcal{P}[a,b]\}
\end{equation*}

\begin{equation*}
\int_{\underline{a}}^{b} f(x)\ dx = \sup\{U(P,f): P \in \mathcal{P}[a,b]\}
\end{equation*}

which are called the \emph{upper} and \emph{lower reimann integrals} respectively.
Notice that both of these values exist since \(f\) is bounded and hence the
\myautoref[{darboux sums are bounded}]{develop--math3277:riemann-integration:page--index.adoc---bounds-for-darboux-sums}.

Now, if the upper and lower reimann integrals are equal, we define the \emph{reimann
integral} of \(f\) as their common value. That is

\begin{equation*}
\int_{a}^{b} f(x)\ dx
=
\int_{a}^{\overline{b}} f(x)\ dx
=
\int_{\underline{a}}^{b} f(x)\ dx
\end{equation*}

Also, we let the set of reimman integrable functions on \([ a,b]\) be
denoted by \(\mathcal{R}[a,b]\).

\begin{admonition-important}[{}]
Upon looking at this definition, an immediate question is

\begin{custom-quotation}[{}][{}]
Can the reimann integral only be definite?
\end{custom-quotation}

and indeed this same question was asked during lecture, to which
the response was

\begin{custom-quotation}[{}][{}]
Let's leave that alone for now and come back to that
\end{custom-quotation}

\begin{example}[{My thoughts}]
At the time of writing, I do not know for certain what the answer to this question is. However, I
would give by past and current thoughts of the relationship of definite and indefinite integrals.
The relationship is simply that a indefinite integral is simply the set (family)
of all definite integrals with fixed upper bound and varying lower bound. More concretely,
let \(F(x)\) be the indefinite integral of \(f\), then

\begin{equation*}
F(x) = \left\{\left.\int_{k}^x f(x) \ dx \ \right| \ k \in \mathbb{R}\right\}
\in \mathbb{R}^\mathbb{R} / \{x \mapsto c\}
\end{equation*}

where \(\{x \mapsto c\}\) is the set of all constant functions. This definition including
quotient groups is such a nice, elegant way of describing integrals, which I first saw on an internet
comment thread.
\end{example}
\end{admonition-important}

\subsection{Common convention/definition}
\label{develop--math3277:riemann-integration:page--index.adoc---common-conventiondefinition}

We define the following

\begin{equation*}
\int_{a}^b f(x) \ dx = - \int_b^a f(x) \ dx
\end{equation*}

\subsection{Darboux's Criterion for Riemann Integratibility}
\label{develop--math3277:riemann-integration:page--index.adoc--darbouxs-criterion}

Let \(f: [a,b]\to \mathbb{R}\) then \(f \in \mathcal{R}[a,b]\)
iff

\begin{equation*}
\forall \varepsilon> 0: \exists P \in \mathcal{P}[a,b]:
U(P, f) - L(P, f) < \varepsilon
\end{equation*}

This result is easily seen by using the fact that \(L(P, f) \leq U(P,f)\)
and the criterion for supremum/infinum.

\subsection{Inequalities and reimann integrals}
\label{develop--math3277:riemann-integration:page--index.adoc---inequalities-and-reimann-integrals}

Let \(f,g \in \mathcal{R}[a,b]\) and \(f \leq g\), then the reimann
integral preserves this inequality. That is,

\begin{equation*}
\int_a^b f(x) \ dx \leq \int_a^b g(x) \ dx
\end{equation*}

To see this, take the difference and see that the integral is non-negative.

\subsection{Integral Mean Value theorem}
\label{develop--math3277:riemann-integration:page--index.adoc---integral-mean-value-theorem}

Let \(f \in C[a,b]\) and \(g \in \mathcal{R}[a,b]\) such that \(g(x) \geq 0\),
then \(\exists \xi \in [a,b]\) suhc that

\begin{equation*}
\int_a^b f(x)g(x) \ dx = f(\xi) \int_a^b g(x) \ dx
\end{equation*}

\begin{example}[{Proof}]
Let \(M, N\) be the supremum and infimum bounds of \(f\) (respectively). Then

\begin{equation*}
N \int_a^b g(x) \ dx \leq \int_a^b f(x) g(x) \ dx \leq M \int_a^b g(x) \ dx
\end{equation*}

and hence

\begin{equation*}
N \leq \frac{\int_a^b f(x) g(x) \ dx}{\int_a^b g(x) \ dx}\leq M
\end{equation*}

Note that if \(\int_a^b g(x) =0\), \(g(x) =0\) and the result is trivial.

Then since \(f\) is continuous, \(\exists \xi \in [a,b]\) at which

\begin{equation*}
f(\xi) = \frac{\int_a^b f(x) g(x) \ dx}{\int_a^b g(x) \ dx}
\end{equation*}
\end{example}

\subsubsection{Corollery of integral mean value theorem}
\label{develop--math3277:riemann-integration:page--index.adoc---corollery-of-integral-mean-value-theorem}

By setting \(g(x) = 1\), we get that \(\exists \xi \in [a,b]\) such that

\begin{equation*}
f(\xi) = \frac{1}{b-a} \int_a^b f(x) \ dx
\end{equation*}

\subsection{Reimann Integral over joint intervals}
\label{develop--math3277:riemann-integration:page--index.adoc---reimann-integral-over-joint-intervals}

Let \(f: [a,c] \to \mathbb{R} \) and \(a < b < c\), then

\begin{equation*}
f \in \mathcal{R}[a,c] \iff f\in \mathcal{R}[a,b] \ \text{and}\ f\in \mathcal{R}[b,c]
\end{equation*}

and

\begin{equation*}
\int_a^c f(x) \ dx = \int_a^b f(x) \ dx + \int_b^c f(x) \ dx
\end{equation*}

\begin{example}[{Proof}]
\begin{example}[{Splitting}]
First suppose that \(f \in \mathcal{R}[a,c]\), then \(f \in \mathcal{R}[a,b]\)
and \(f\in \mathcal{R}[b,c]\) since
\myautoref[{\(f\) is riemann integrable on subintervals}]{develop--math3277:riemann-integration:page--algebra.adoc---subintervals}.
\end{example}

\begin{example}[{Joining}]
On the other hand, suppose that \(f \in \mathcal{R}[a,b]\) and \(f \in \mathcal{R}[b,c]\). Then,
consider arbitrary \(\varepsilon > 0\), then \(\exists P_1 \in \mathcal{P}[a,b]\)
and \(P_2 \in \mathcal{P}[b,c]\) such that

\begin{equation*}
U(P_1, f) - L(P_1, f) < \frac{\varepsilon}{2}
\quad\text{and}\quad
U(P_2, f) - L(P_2, f) < \frac{\varepsilon}{2}
\end{equation*}

Then since \(P = P_1 \cup P_2 \in \mathcal{P}[a,b]\), \(U(P, f) = U(P_1, f) + U(P_2, f)\)
and \(L(P, f) = L(P_1, f) + L(P_2, f)\) and hence

\begin{equation*}
U(P, f) - L(P, f) < \varepsilon
\end{equation*}
\end{example}

\begin{example}[{Equality.}]
Now, \(f\) is riemann integrable on all three intervals.
Consider arbitrary \(\varepsilon > 0\), then \(\exists P = P_1 \cup P_2 \in \mathcal{P}[a,c]\)
such that \(P_1 \in \mathcal{P}[a,b]\), \(P_2 \in \mathcal{P}[b,c]\),

\begin{equation*}
0 < U(P, f) - \int_a^c f(x) \ dx < \frac{\varepsilon}{2}
\end{equation*}

and

\begin{equation*}
0 < U(P_1,f) - \int_a^b f(x)\ dx < \frac{\varepsilon}{4}
\quad\text{and}\quad
0 < U(P_2,f) - \int_b^c f(x)\ dx < \frac{\varepsilon}{4}
\end{equation*}

Then, \(U(P, f) = U(P_1, f) + U(P_2, f)\), hence

\begin{equation*}
0 < U(P, f) -\int_a^b f(x)\ dx - \int_b^c f(x)\ dx
= U(P_1,f) + U(P_2, f) - \int_a^b f(x)\ dx - \int_b^c f(x)\ dx < \frac{\varepsilon}{2}
\end{equation*}

Furthermore,

\begin{equation*}
-\frac{\varepsilon}{2} < \int_a^c f(x) \ dx - U(P,f) < 0
\end{equation*}

Hence by adding both sets of inequalities

\begin{equation*}
-\frac{\varepsilon}{2} < \int_a^c f(x) \ dx - \int_a^b f(x)\ dx - \int_b^c f(x)\ dx < \frac{\varepsilon}{2}
\end{equation*}

Therefore, since the choice of \(\varepsilon\) was arbirary, we get the desired result.
\end{example}
\end{example}

\section{Fundamental Theorems of Calculus}
\label{develop--math3277:riemann-integration:page--index.adoc---fundamental-theorems-of-calculus}

Let \(F \in C[a,b]\) be differentable on \((a,b)\) and let \(f\in \mathcal{R}[a,b]\).
Then \(F\) is called the \emph{antiderivative} or \emph{primative} of \(f\) if \(F'(x) = f(x)\ \forall x \in (a,b)\).

\begin{admonition-note}[{}]
You likely already know this, but the antiderivative is not unique.
\end{admonition-note}

\subsection{First Fundamental Theorem of Calculus}
\label{develop--math3277:riemann-integration:page--index.adoc---first-fundamental-theorem-of-calculus}

Let \(f \in C[a,b]\), then we define \(F:[a,b]\to \mathbb{R}\) by

\begin{equation*}
F(x) = \int_a^x f(t)\ dt
\end{equation*}

Then, \(F\) is uniformly continuous on \( [a,b]\) and differentable on \((a,b)\)
and \(F'(x) = f(x)\ \forall x\in (a,b)\).

\begin{example}[{Proof}]
Consider the difference

\begin{equation*}
\begin{aligned}
F(y) - F(x) = \int_{x}^y f(t) \ dt
\end{aligned}
\end{equation*}

and by the integral mean value theorem, \(\exists \xi \in [x,y] \subseteq [a,b]\) such that

\begin{equation*}
f(\xi)(y-x) = \int_x^y f(t) \ dt = F(y) - F(x)
\end{equation*}

Now, since \(f\) is reimann integrable, its bounded by some \(-M, M\). Now, consider arbirary
\(\varepsilon > 0\), we get that if \(|y-x| < \frac{\varepsilon}{M}\)

\begin{equation*}
|F(y) - F(x)| \leq M|y-x| < \varepsilon
\end{equation*}

and hence \(F\) is uniformly continuous. Next, since \(f\) is continuous,
\(\exists \delta > 0\) such that \(|f(x) - f(y)| < \varepsilon\) for all \(y \in N(x, \delta)\).
Then, consider arbirary \(y\in N(x,\delta)\)

\begin{equation*}
\left|f(x) - \frac{F(y) - F(x)}{y-x}\right|
= |f(x) - f(\xi)| < \varepsilon
\end{equation*}

since \(\xi \in N(x,\delta)\). Hence,

\begin{equation*}
\lim_{\delta \to 0} \frac{F(x+\delta) - F(x)}{\delta} = f(x)
\end{equation*}

and we done.
\end{example}

\subsection{Second Fundamental Theorem of Calculus}
\label{develop--math3277:riemann-integration:page--index.adoc---second-fundamental-theorem-of-calculus}

Suppose that \(f \in \mathcal{R}[a,b]\) with antiderivative \(F\). Then,

\begin{equation*}
\int_a^b f(x)\ dx = F(b) - F(a)
\end{equation*}

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
I took this proof from Dr Daaga's notes. I was unable to come up with it as well.
\end{admonition-note}

We do this proof using the definition of the reimann integral.

Consider arbirary partition \(P \in \mathcal{P}[a,b]\) and focus on the
segment \( [x_{i-1}, x_i]\). Then, since \(F\) is differentiable and continuous,
\(\exists c_i \in [x_{i-1}, x_i]\) such that \(f(c_i)(x_i-x_{i-1}) = F(x_{i}) - F(x_{i-1})\)
and hence

\begin{equation*}
m_i(P, f)\Delta x_i \leq f(c_i)(x_i-x_{i-1}) = F(x_i) - F(x_{i-1}) \leq M_i(P,f)
\end{equation*}

and by summing (notice the teliscoping nature)

\begin{equation*}
L(P,f) \leq F(b)- F(a) \leq U(P,f)
\end{equation*}

Then since \(f\) is reimann integrable and \(F(b) - F(a)\) is an upper bound for all \(L(P,f)\) and
a lower bound for all \(U(P,f)\), we get the desired result.
\end{example}
\end{document}
