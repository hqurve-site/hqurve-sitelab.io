\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Algebra of Riemann Integrals}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Throughout this section we would be looking at the properties
of \(\mathcal{R}[a,b]\) sometimes in relation to the value of the reimann integral.

\section{Linearity of the integral}
\label{develop--math3277:riemann-integration:page--algebra.adoc---linearity-of-the-integral}

Simply put, \(\mathcal{R}[a,b]\) is a vector space with field \(\mathbb{R}\)
and the reimann integral is a linear transformation from \(\mathcal{R}[a,b]\)
to \(\mathbb{R}\).

\begin{example}[{Proof}]
Notice that \(\mathcal{R}[a,b]\) is already a subset of the vector field of real functions on \( [ a,b]\),
then we only need to do a subspace test.

Consider arbirary \(f,g \in \mathcal{R}[a,b]\) and \(k \in \mathbb{R}\). Then, we
want to show that \(kf + g \in \mathcal{R}[a,b]\) and

\begin{equation*}
\int_a^b (kf+g)(x) \ dx = k\int_a^b f(x) \ dx + \int_a^b g(x) \ dx
\end{equation*}

To simplify the work, let \(k < 0\). If \(k=0\) the result is trivial and if \(k>0\), we can
apply this proof twice, once with \((-k)f\) and the other with \((-a)(-k)f + g\). Now, we proceed.

Consier arbirary \(P \in \mathcal{P}[a,b]\). Then

\begin{equation*}
\begin{aligned}
M_i(P, kf+g)
&= \sup_{x \in [x_{i-1}, x_i]}\{(kf+g)(x)\}
\\&= \sup_{x \in [x_{i-1}, x_i]}\{kf(x) + g(x)\}
\\&= \sup_{x \in [x_{i-1}, x_i]}\{kf(x)\} + \sup_{x \in [x_{i-1}, x_i]}\{g(x)\}
\\&= k\inf_{x \in [x_{i-1}, x_i]}\{f(x)\} + \sup_{x \in [x_{i-1}, x_i]}\{g(x)\}
\\&= km_i(P, f) + M_i(P, g)
\end{aligned}
\end{equation*}

and similarly

\begin{equation*}
m_i(P, kf+g) = kM_i(P, f) + m_i(P, g)
\end{equation*}

Then

\begin{equation*}
\begin{aligned}
\int_{a}^{\underline{b}} (kf+g)(x) \ dx
&= \inf\{M_i(P, kf+g)\ | \ P \in \mathcal{P}[a,b]\}
\\&= \inf\{km_i(P, f) + M_i(g)\ | \ P \in \mathcal{P}[a,b]\}
\\&= k\sup\{m_i(P, f)\ | \ P \in \mathcal{P}[a,b]\} + \inf\{M_i(g)\ | \ P \in \mathcal{P}[a,b]\}
\\&= k \int_{\underline{a}}^b f(x) \ dx + \int_{a}^{\underline{b}} g(x) \ dx
\\&= k \int_{a}^b f(x) \ dx + \int_{a}^{b} g(x) \ dx
\end{aligned}
\end{equation*}

and similarly

\begin{equation*}
\int_{\underline{a}}^b (kf+g)(x) \ dx
= k \int_{a}^b f(x) \ dx + \int_{a}^{b} g(x) \ dx
\end{equation*}

and hence the result follows.
\end{example}

\section{Ring of \(\mathcal{R}[a,b]\)}
\label{develop--math3277:riemann-integration:page--algebra.adoc--ring-of-r-a-b}

\(\mathcal{R}[a,b]\) forms a ring with unity where addition and multiplication
of functions are defined as

\begin{equation*}
\begin{aligned}
&(f + g)(x) = f(x) + g(x)\\
&(f\cdot g)(x) = f(x)g(x)
\end{aligned}
\end{equation*}

\begin{example}[{Proof}]
We already showed that the integral is linear, so all that remains
is to show that the product of two reimann integrable functions is
also reiman integrable

Firstly note that since \(f,g \in \mathcal{R}[a,b]\) then they are both
bounded by some \(M, N\) such that \(|f(x)| \leq M\) and \(|g(x)| \leq N\).
Now, consider arbirary \(\varepsilon > 0\), then \(\exists P \in \mathcal{P}[a,b]\)

\begin{equation*}
U(P,f) - L(P, f) < \frac{\varepsilon}{2M}
\quad\text{and}\quad
U(P,g) - L(P, g) < \frac{\varepsilon}{2N}
\end{equation*}

then, consider an arbirary interval \( [x_{i-1}, x_{i}] \), we get that

\begin{equation*}
\begin{aligned}
M_i(P, f\cdot g) - m_i(P, f\cdot g)
&= \sup\{|f(x)g(x) - f(y)g(y)| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&= \sup\{|f(x)g(x) -f(x)g(y) + f(x)g(y)- f(y)g(y)| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&= \sup\{|f(x)(g(x) -g(y)) + g(y)(f(x)- f(y))| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&\leq \sup\{|f(x)(g(x) -g(y))| + |g(y)(f(x)- f(y))| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&\leq
    \sup\{|f(x)(g(x) -g(y))| \ |\ x, y\in [x_{i-1}, x_i]\}
    \\&\hspace{4em}+\sup\{|g(x)(f(x) -f(y))| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&\leq
    \sup\{M|g(x) -g(y)| \ |\ x, y\in [x_{i-1}, x_i]\}
    \\&\hspace{4em}+ \sup\{N|f(x) -f(y)| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&=
    M\sup\{|g(x) -g(y)| \ |\ x, y\in [x_{i-1}, x_i]\}
    \\&\hspace{4em}+ N\sup\{|f(x) -f(y)| \ |\ x, y\in [x_{i-1}, x_i]\}
\\&= M(M_i(P, f) - m_i(P,f)) + N(M_i(P,g), m_i(P, g))
\end{aligned}
\end{equation*}

then

\begin{equation*}
\begin{aligned}
U(P,f\cdot g) - L(P,f\cdot g)
&= \sum_{i=1}^n (M_i(P,f\cdot g) - m_i(P,f\cdot g)) \Delta x_i
\\&\leq \sum_{i=1}^n \left[M(M_i(P, f) - m_i(P,f)) + N(M_i(P,g), m_i(P, g))\right]\Delta x_i
\\&= M\sum_{i=1}^n (M_i(P, f) - m_i(P,f))\Delta x_i
    + N\sum_{i=1}^n (M_i(P, g) - m_i(P,g))\Delta x_i
\\&= M(U(P, f) - L(P,f))
    + N(U(P,g) - L(P,g))
\\&< \frac{\varepsilon}{2} + \frac{\varepsilon}{2}
\\&= \varepsilon
\end{aligned}
\end{equation*}

and hence \(f\cdot g \in \mathcal{R}[a,b]\)
\end{example}

\section{Closure under more operations}
\label{develop--math3277:riemann-integration:page--algebra.adoc---closure-under-more-operations}

\subsection{Absolute value}
\label{develop--math3277:riemann-integration:page--algebra.adoc---absolute-value}

If \(f \in \mathcal{R}[a,b]\), then \(|f| \in \mathcal{R}[a,b]\) and

\begin{equation*}
\left|\int_a^b f(x) \ dx\right| \leq \int_a^b |f(x)| \ dx
\end{equation*}

this result is called the triangle inequality (I think).

\begin{example}[{Proof}]
Consider arbirary \(\varepsilon > 0\), then since \(f \in \mathcal{R}[a,b] \)
\(\exists P \in \mathcal{P}[a,b] \) such that

\begin{equation*}
U(P,f) - L(P,f) < \varepsilon
\end{equation*}

Now, focus on an arbirary segment \( [ x_{i-1}, x_i]\), and we get

\begin{equation*}
\begin{aligned}
M_i(P,|f|) - m_i(P,|f|)
&= \sup\{ ||f(x)| - |f(y)|| \ | \ x,y\in [x_{i-1},x_i] \}
\\&\leq \sup\{ |f(x) - f(y)| \ | \ x,y\in [x_{i-1},x_i] \}
\\&= M_i(P, f) - m_i(P,f)
\end{aligned}
\end{equation*}

Hence,

\begin{equation*}
\begin{aligned}
U(P, |f|) - L(P, |f|)
&= \sum_{i=1}^n [M_i(P,|f|) - m_i(P,|f|)]\Delta x_i
\\&= \sum_{i=1}^n [M_i(P,f) - m_i(P,f)]\Delta x_i
\\&= U(P,f) - L(P,f)
\\&< \varepsilon
\end{aligned}
\end{equation*}
\end{example}

\begin{example}[{Proof of inequality}]
Consider arbirary \(P \in \mathcal{P}[a,b]\), then

\begin{equation*}
\begin{aligned}
|M_i(P,f)|
&= |\sup \{f(x) \ | \ x \in [x_{i-1}, x_i]\}|
\\&\leq |\sup \{|f(x)| \ | \ x \in [x_{i-1}, x_i]\}|
\\&= \sup \{|f(x)| \ | \ x \in [x_{i-1}, x_i]\}
\\&= M_i(P,|f|)
\end{aligned}
\end{equation*}

and

\begin{equation*}
\begin{aligned}
|U(P,f)|
&= \left|\sum_{i=1}^n M_i(P,f) \Delta x_i\right|
\\&\leq \sum_{i=1}^n |M_i(P,f)| \Delta x_i
\\&\leq \sum_{i=1}^n M_i(P,|f|) \Delta x_i
\\&= U(P,|f|)
\end{aligned}
\end{equation*}

and hence the

\begin{equation*}
\begin{aligned}
\left|\int_a^b f(x) \ dx\right|
&= \left|\int_a^{\overline{b}} f(x) \ dx\right|
\\&= \left|\{U(P,f) \ | \ P \in \mathcal{P}[a,b]\}\right|
\\&\leq \inf\left|\{|U(P,f)| \ | \ P \in \mathcal{P}[a,b]\}\right|
\\&= \inf\{|U(P,f)| \ | \ P \in \mathcal{P}[a,b]\}
\\&\leq \inf\{U(P,|f|) \ | \ P \in \mathcal{P}[a,b]\}
\\&= \int_a^{\overline{b}} |f(x)| \ dx
\\&= \int_a^b |f(x)| \ dx
\end{aligned}
\end{equation*}
\end{example}

\subsection{Reciprical}
\label{develop--math3277:riemann-integration:page--algebra.adoc---reciprical}

If \(f \in \mathcal{R}[a.b]\) and \(|f| \geq M\) for some \(M > 0\),
then \(1/f \in \mathcal{R}[a,b]\).

\begin{admonition-note}[{}]
This condition is equivalent to saying that \(1/f\) is bounded.
\end{admonition-note}

\begin{example}[{Proof}]
Consider arbirary \(\varepsilon > 0\) then by Darboux's Criterion since \(\varepsilon\rho^2 > 0\)
there exists \(P \in \mathcal{P}[a,b]\)
such that

\begin{equation*}
    U(P, f) - L(P,f) < \varepsilon\rho^2
\end{equation*}

Let \(n\) be the order of \(P\).
Now, consider arbirary
segment \([x_{i-1}, x_i]\) of \(P\) and arbirary \(x,y \in [x_{i-1},x_i]\) then

\begin{equation*}
\begin{aligned}
    \left|\frac{1}{f(x)} - \frac{1}{f(y)}\right|
    &= \left|\frac{f(y)-f(x)}{f(x)f(y)}\right|
    \\&= |f(y)-f(x)|\frac{1}{|f(x)f(y)|}
    \\&\leq |f(y)-f(x)|\frac{1}{\rho^2}
\end{aligned}
\end{equation*}

Then,

\begin{equation*}
\begin{aligned}
    M_i(P, 1/f) - m_i(P,1/f)
    &= \sup\left\{\left|\frac{1}{f(x)} - \frac{1}{f(y)}\right|\ :\ x,y\in [x_{i-1},x_i]\right\}
    \\&\leq \sup\left\{\frac{1}{\rho^2}|f(x)-f(y)|\ :\ x,y\in [x_{i-1},x_i]\right\}
    \\&= \frac{1}{\rho^2}\sup\left\{|f(x)-f(y)|\ :\ x,y\in [x_{i-1},x_i]\right\}
    \\&= \frac{1}{\rho^2}\left[M_i(P, f) - m_i(P,f)\right]
\end{aligned}
\end{equation*}

Then since the choice of segment was arbirary, we get

\begin{equation*}
\begin{aligned}
    U(P, 1/f) - L(P, 1/f)
    &= \sum_{i=1}^n [M_i(P, 1/f) - m_i(P, 1/f)] \Delta x_i
    \\&\leq \sum_{i=1}^n \frac{1}{\rho^2}\left[M_i(P, f) - m_i(P,f)\right] \Delta x_i
    \\&= \frac{1}{\rho^2}\sum_{i=1}^n \left[M_i(P, f) - m_i(P,f)\right] \Delta x_i
    \\&= \frac{1}{\rho^2} [U(P, f) - L(P, f)]
    \\&= \frac{1}{\rho^2} [U(P, f) - L(P, f)]
    \\&< \varepsilon
\end{aligned}
\end{equation*}

And by Darboux Criterion for integrability, \(1/f \in \mathcal{R}[a,b]\).
\end{example}

\section{All continuous functions are reimann integrable}
\label{develop--math3277:riemann-integration:page--algebra.adoc---all-continuous-functions-are-reimann-integrable}

\(C^0 [a,b] \subset \mathcal{R}[a,b]\)

\subsection{Corrollery: All functions with a finite number of discontinuties are reimann integrable}
\label{develop--math3277:riemann-integration:page--algebra.adoc---corrollery-all-functions-with-a-finite-number-of-discontinuties-are-reimann-integrable}

\section{Monotonic functions are reimmann integrable}
\label{develop--math3277:riemann-integration:page--algebra.adoc---monotonic-functions-are-reimmann-integrable}

Let \(f:[a,b]\to \mathbb{R}\) be a monotonic function, then \(f\) is reimann integrable.

\begin{example}[{Proof}]
WLOG, let \(f\) be monotonic increasing. And consider arbirary \(\varepsilon > 0\).

Now, consider arbitrary regular \(P \in \mathcal{P}[a,b]\) where
\(P = \{x_0\ldots x_n\}\) and \(\Delta x_i = \Delta\). Then

\begin{equation*}
\begin{aligned}
U(P, f)
&= \sum_{i=1}^n M_i(P,f)\Delta
\\&= \Delta\sum_{i=1}^n \sup\{f(x): x \in [x_{i-1}, x_i]\}
\\&= \Delta\sum_{i=1}^n f(x_i)
\end{aligned}
\end{equation*}

Similarly,

\begin{equation*}
\begin{aligned}
L(P, f)
&= \sum_{i=1}^n m_i(P,f)\Delta
\\&= \Delta\sum_{i=1}^n \inf\{f(x): x \in [x_{i-1}, x_i]\}
\\&= \Delta\sum_{i=1}^n f(x_{i-1})
\\&= \Delta\sum_{i=0}^{n-1} f(x_{i})
\end{aligned}
\end{equation*}

Then

\begin{equation*}
\begin{aligned}
U(P, f) - L(P,f)
= \Delta\sum_{i=1}^n f(x_i)
    - \Delta\sum_{i=0}^{n-1} f(x_i)
= \Delta(f(x_n) - f(x_0))
\end{aligned}
\end{equation*}

Lets restrict \(\Delta < \frac{\varepsilon}{f(x_n) - f(x_0)}\) and we get the desired result using
\myautoref[{Darboux's Criterion}]{develop--math3277:riemann-integration:page--index.adoc--darbouxs-criterion}.

\begin{admonition-note}[{}]
If \(f(x_n) = f(x_0)\), \(f\) would be constant and the result is trivial.
\end{admonition-note}
\end{example}

\section{Subintervals}
\label{develop--math3277:riemann-integration:page--algebra.adoc---subintervals}

Consider \(a < a' < b' < b\), then \(\mathcal{R}[a,b] \subseteq \mathcal{R}[a',b']\).
That is if \(f\) is reimann integrable on \( [a,b] \) it is also reimann integrable
on \( [a', b'] \)

\begin{example}[{Proof}]
This is a relatively trivial proof. Consider arbirary \(\varepsilon > 0\), then
\(\exists P \in \mathcal{P}[a,b]\) such that \(U(P,f) - L(P,f) < \varepsilon\).
Then, we add \(a'\) and \( b'\) to \(P\) to get \(P'\) where

\begin{equation*}
U(P', f) - L(P',f) \leq U(P,f) - L(P,f) < \varepsilon
\end{equation*}

Finally let \(P''\) be the partition in \(\mathcal{P}[a',b'] \) which contains the
same points as \(P'\). That is \(P'' = P' \cap [a',b']\). Then we get that

\begin{equation*}
\begin{aligned}
U(P'', f) - L(P'', f) \leq U(P',f) - L(P', f) < \varepsilon
\end{aligned}
\end{equation*}

and we are done
\end{example}
\end{document}
