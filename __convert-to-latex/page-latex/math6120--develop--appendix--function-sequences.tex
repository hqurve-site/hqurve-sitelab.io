\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Sequences and Series of functions}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}

% Title omitted
This contains a bit more information than
\myautoref[{Math3277 notes}]{develop--math3277:function-sequences:page--index.adoc}.

\section{Bounded}
\label{develop--math6120:appendix:page--function-sequences.adoc---bounded}

A sequence of functions \(f_n: D \subseteq \mathbb{R} \to \mathbb{R}^n\)
is \emph{pointwise bounded} if there exists \(\phi(D) \to \mathbb{R}\)
such that

\begin{equation*}
\forall t \in D: \forall n \in \mathbb{N}: |f_n(t)| \leq \phi(t)
\end{equation*}

If there exists a constant function \(\phi(t) = M\),
then we instead say that \(f_n\) is \emph{uniformly bounded}.

\section{Equicontinuous}
\label{develop--math6120:appendix:page--function-sequences.adoc---equicontinuous}

Let \(\mathcal{F}\) be a family of real or complex valued functions
defined on \(D\).
Then, we say that \(\mathcal{F}\) is equicontinuous iff

\begin{equation*}
\forall \varepsilon > 0: \exists \delta >0: \forall x,y \in D:
\forall f \in \mathcal{F}: |x-y| < \delta \implies |f(x) - f(y)| < \varepsilon
\end{equation*}

\begin{admonition-remark}[{}]
Note the similarity with the definition of uniformly continuous.
The only difference is that the value of \(\delta(\varepsilon)\) works
with a collection of functions instead of just one.
Therefore \(\mathcal{F}\) is not only a collection of uniformly continuous functions,
but we also have a statement about ``how much all of them vary''.
\end{admonition-remark}

\subsection{Arzela-Ascoli Theorem}
\label{develop--math6120:appendix:page--function-sequences.adoc---arzela-ascoli-theorem}

\begin{theorem}[{Arzela-Ascoli Theorem}]
Let \(f_n\) be a sequence of continuous functions in \(\mathcal{C}[a,b]\).
Then, if \(\{f_n\}\) is uniformly bounded and equicontinuous,
then, there exists a subsequence of \(\{f_n\}\) which converges uniformly
on \([a,b]\).
\end{theorem}

\begin{admonition-remark}[{}]
This theorem is very similar to the Bolzano-Weierstrass theorem
which asserts that all bounded sequences have a convergent subsequence.
Since the domain of the functions is compact, their image is also compact
and hence for each \(x \in [a,b]\), there is a subsequence of
\(\{f_n(x)\}\) which converges.
However, this observation does not assert that each of the subsequences for
each value of \(x\) consist of the same functions.
\end{admonition-remark}

\begin{admonition-tip}[{}]
This theorem can be generalized for multivariate vector functions on closed and bounded
regions. The following proof still works; just, care needs to be taken
when extending from a finite set of points to the entire domain (you have to layout
the points in a grid).
\end{admonition-tip}

Our aim is as follows.

\begin{enumerate}[label=\arabic*)]
\item Take a finite set of points \(A \subseteq [a,b]\)
    and find the set of functions whose function values at these points differ by at most some fixed about.
\item If this is possible in general, we can take a sequence of \(n\) equally spaced points and find
    a set of functions which differ by at most some distance.
\item Using the fact that the sequence of functions is equicontinuous, we would be able to
    determine that functions which differ by some fixed about on all of \([ a,b]\).
    This proves that there is a subsequence of functions which are pointwise convergent
\item Prove that the convergence is actually uniform.
\end{enumerate}

\begin{lemma}[{Lemma 1: Arbitrarily close at a point}]
Let \(\mathcal{F} \subseteq \{f_n\}\) be infinite and let \(x_0 \in [a,b]\).
Then, we claim that for all \(\varepsilon > 0\),
there exists an infinite subset \(\mathcal{F}_0\subseteq \mathcal{F}\) such that

\begin{equation*}
\forall f,g \in \mathcal{F}_0:
|f(x_0) - g(x_0)| < \varepsilon
\end{equation*}

We prove this as follows. Consider arbitrary \(\varepsilon > 0\) and the set \(\{f(x_0) \ | \ f \in \mathcal{F}\}\).
If this set is finite then there must be infinitely many functions mapping to the same value at \(x_0\) and we are done.
Otherwise suppose that this set is finite.
By the Bolzano-Weierstrass theorem, this set has a limit point \(y_0 \in \mathbb{R}\)
Then, the following mapping produces non empty sets for each \(\varepsilon' > 0\)

\begin{equation*}
\mathcal{F}_0(\varepsilon') = \left\{f \in \mathcal{F} \ \middle| \ 0 < |f(x_0) - y_0| < \frac{\varepsilon'}{2}\right\}
\end{equation*}

Since the above mapping is non-empty for each \(\varepsilon' > 0\), is must also be infinite.
Thus we have found an infinite set such that
\(\forall f,g \in \mathcal{F}_0: |f(x_0) - g(x_0)| < \varepsilon\).
\end{lemma}

\begin{lemma}[{Lemma 2: Arbitrarily close at a finite set of points}]
Let \(\mathcal{F} \subseteq \{f_n\}\) be infinite and
Consider a finite set \(A \subseteq [a,b]\) and \(\varepsilon > 0\).
Then, by using the above result, we may inductively (on \(|A|\)) prove that there are an infinite set of functions
\(\mathcal{F}_0\subseteq \mathcal{F}\) such that

\begin{equation*}
\forall x \in A: \forall f, g \in \mathcal{F}_0: |f(x) - g(x)| < \varepsilon
\end{equation*}
\end{lemma}

\begin{lemma}[{Lemma 3: Arbitrarily close on the entire domain}]
Let \(\mathcal{F} \subseteq \{f_n\}\) be infinite and \(\varepsilon > 0\).
Then, there exists \(\mathcal{F}_0 \subseteq \mathcal{F}\) such that

\begin{equation*}
\forall x \in [a,b]: \forall f, g \in \mathcal{F}_0: |f(x) - g(x)| < \varepsilon
\end{equation*}

To prove this first let \(\delta > 0\) be such that

\begin{equation*}
\forall x,y \in [a,b]: \forall f \in \{f_n\}: |x-y| < \delta \implies |f(x) - f(y)| < \frac{\varepsilon}{3}
\end{equation*}

This is a result of \(\{f_n\}\) being equicontinuous.
Notice that there is a finite set \(A\subseteq [a,b]\) such that

\begin{equation*}
\forall x \in [a,b]: \exists x' \in A: |x-x'| < \delta
\end{equation*}

We may construct such a \(A\) be equally spacing sufficiently many points.
Also, from lemma 2, there exists \(\mathcal{F}_0 \subseteq \mathcal{F}\) such that

\begin{equation*}
\forall x \in A: \forall f, g \in \mathcal{F}_0: |f(x) - g(x)| < \frac{\varepsilon}{3}
\end{equation*}

This set satisfies the desired condition since

\begin{equation*}
\forall x \in [a,b]: \forall f,g \in \mathcal{F}_0:
|f(x) - g(x)| < |f(x) - f(x')| + |f(x') - g(x')| + |g(x') - g(x)|
< 3 \cdot \frac{\varepsilon}{3} = \varepsilon
\end{equation*}

where \(x' \in A\)  such that \(|x'-x| < \delta\).
Therefore, we have proven the desired result.
\end{lemma}

\begin{lemma}[{Convergent subsequence}]
We would prove that there is a subsequence of \(\{f_n\}\) which is uniformly convergent; hence proving the Arzela-Ascoli theorem.

To prove this, we consider a sequence of infinite subsets of \(\{f_n\}\) such that

\begin{equation*}
\mathcal{F}_0 = \{f_n\}
\quad\text{and}\quad
\mathcal{F}_{k+1} \subseteq \mathcal{F}_k
\ \text{such that}\
\forall f,g \in \mathcal{F}_{k+1}: \sup_{x \in [a,b]} |f(x) - g(x)| < \frac{1}{k+1}
\end{equation*}

By lemma 3, such a sequence exists since there always exists \(\mathcal{F}_{k+1}\) which is infinite.

Now, given such a sequence of subsets, we define \(n_k = \min\{n \in \mathbb{N}: f_n \in \mathcal{F}_k\}\).
We claim that the sequence \(\{f_{n_k}\}\) is uniformly convergent.
We do this in two stages, first prove that it is pointwise convergent, then prove uniform convergence.

For pointwise convergence, consider arbitrary \(x \in [a,b]\) and arbitrary \(\varepsilon > 0\).
Let \(k \geq 1\) be such that \(\frac{1}{k} < \varepsilon\). Then, we have that

\begin{equation*}
\forall i > j > k: |f_{n_i}(x) - f_{n_j}(x)| < \frac{1}{j} < \frac{1}{k} < \varepsilon
\end{equation*}

since \(f_{n_i}, f_{n_j} \in \mathcal{F}_{j} \subseteq \mathcal{F}_k\).
Therefore the sequence \(\{f_{n_k}(x)\}\) is Cauchy for each \(x \in [a,b]\) and hence
the subsequence \(\{f_{n_k}\}\) is pointwise convergent.

For uniform convergence,
let \(f^*\) be the function which \(\{f_{n_k}\}\) converges to
and consider arbitrary \(\varepsilon > 0\).
Let \(k \geq 1\) be such that \(\frac{1}{k} < \varepsilon\).
Then, for each \(i, j > k\) and \(x \in [a,b]\), \(|f_{n_i}(x) - f_{n_j}(x)| < \frac{1}{k}\) (as shown above).
Therefore, we have that

\begin{equation*}
\begin{aligned}
&\forall i, j > k: \forall x \in [a,b]: |f_{n_i}(x) - f_{n_j}(x)| < \frac{1}{k}
\\&\implies \forall i > k: \forall x \in [a,b]: \forall j > k: f_{n_i}(x) - \frac{1}{k} < f_{n_j}(x) < f_{n_i}(x) + \frac{1}{k}
\\&\implies \forall i > k: \forall x \in [a,b]: f_{n_i}(x) - \frac{1}{k} \leq \lim_{j\to\infty}f_{n_j}(x) = f^*(x) \leq f_{n_i}(x) + \frac{1}{k}
\\&\implies \forall i > k: \forall x \in [a,b]: |f_{n_i}(x) - f^*(x)| \leq  \frac{1}{k} < \varepsilon
\end{aligned}
\end{equation*}

Therefore the sequence is also uniformly convergent

\begin{admonition-important}[{}]
The proof of a cauchy (under sup norm) sequence of functions converging was adapted from \url{https://math.stackexchange.com/questions/827786/proving-that-a-uniformly-cauchy-sequence-of-functions-f-n-mathbbr-to-mathbb}.
\end{admonition-important}
\end{lemma}
\end{document}
