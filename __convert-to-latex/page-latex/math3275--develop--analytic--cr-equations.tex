\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cauchy-Riemann Equations}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Ln}{Ln}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\Res}{Res}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
Let \(f(z)= u(x,y)+iv(x,y)\) be analytic on a region \(R\), then
it \(f\) satisfies the following pair of equations

\begin{equation*}
u_x = v_y
\quad\text{and}\quad
u_y = -v_x
\end{equation*}

hold at each point in \(R\).
These two equations are called the \emph{Cauchy-Riemann equations}.

\begin{example}[{Proof}]
By considering the path \(\mathbb{R} \subseteq \mathbb{C}\),
we get that

\begin{equation*}
f'(z_0)
= \lim_{x\to 0}\frac{u(x_0 + x, y_0) + iv(x_0 + x, y_0) - u(x_0, y_0) - iv(x_0, y_0)}{x}
= \lim_{x\to 0}\left[\frac{u(x_0 + x, y_0) - u(x_0, y_0)}{x}  + i\frac{v(x_0 + x, y_0)- iv(x_0, y_0)}{x}\right]
= u_x(x_0, y_0) + iv_x(x_0, y_0)
\end{equation*}

while by considering the path \(i\mathbb{R} \subseteq \mathbb{C}\),
we get

\begin{equation*}
f'(z_0)
= \lim_{y\to 0}\frac{u(x_0, y_0 + y) + iv(x_0 , y_0 + y) - u(x_0, y_0) - iv(x_0, y_0)}{iy}
= \lim_{y\to 0}\left[\frac{u(x_0, y_0 + y) - u(x_0, y_0)}{iy}  + i\frac{v(x_0, y_0 + y)- iv(x_0, y_0)}{iy}\right]
= \frac{1}{i}u_y(x_0, y_0) + v_y(x_0, y_0)
\end{equation*}

and we get the desired result by equating the real and imaginary components.
\end{example}

These equations arrive by limiting the derivative
along the real and imaginary axis, then comparing the real and imaginary
components. That is, the Cauchy-Riemann equations are a
result of the following equality

\begin{equation*}
f'(z_0)
= \lim_{z\to 0}\frac{f(z_0 + z) - f(z_0)}{z}
= \lim_{x\to 0}\frac{f(z_0 + x) - f(z_0)}{x}
= \lim_{y\to 0}\frac{f(z_0 + iy) - f(z_0)}{iy}
\end{equation*}

Hence the Cauchy-Riemann equations also provide a simpler
way to compute the derivative, given that it is already analytic.

In fact, we can generalize this to any path passing through the origin.
That is if \(\mu(t)\) is a continuous function
with \(\mu(0) = 0\), then we get that

\begin{equation*}
f'(z_0) = \lim_{t\to 0} \frac{f(z_0 + \mu(t)) - f(z_0)}{\mu(t)}
\end{equation*}

\begin{example}[{Proof}]
Let \(\varepsilon > 0\), then since \(f'(z_0)\) exists
\(\delta > 0\) such that

\begin{equation*}
\forall |z| < \delta: \left|\frac{f(z_0 + z) - f(z_0)}{z} - f'(z_0)\right| < \varepsilon
\end{equation*}

Then since \(\mu\) is continuous, there exists \(\gamma > 0\)
such that \(|t| < \gamma\) implies that \(|\mu(t)| < \delta\).
Therefore,

\begin{equation*}
\forall |t| < \gamma : \left|\frac{f(z_0 + \mu(t)) - f(z_0)}{\mu(t)} - f'(z_0)\right| < \varepsilon
\end{equation*}

and since we found \(\gamma > 0\), we are done.
\end{example}

This general version implies the Cauchy-Riemman equations with
\(\mu_1(t) = t\) and \(\mu_2(t) = it\).

\section{Polar form}
\label{develop--math3275:analytic:page--cr-equations.adoc---polar-form}

We can rewrite the Cauchy-Riemann equations in ``polar form''
with use of the equations

\begin{equation*}
x=r\cos\theta \quad\text{and}\quad y=r\sin\theta
\end{equation*}

and the chain rule to obtain the following pair of equations

\begin{equation*}
\frac{\partial u}{\partial r} = \frac{1}{r}\frac{\partial v}{\partial \theta}
\quad\text{and}\quad
\frac{\partial u}{\partial \theta} = - r \frac{\partial v}{\partial r}
\end{equation*}

\begin{admonition-remark}[{}]
The reason why ``polar form'' is in quotation is that it
still relies on the cartesian form of the function itself and only
takes the polar form of the inputs. However, it should be noted
that full polar form would be equivalent to the derivative since
it would only yield one equation which necessarily would not depend on \(\theta\).
\end{admonition-remark}

\section{Simplified expression for derivative}
\label{develop--math3275:analytic:page--cr-equations.adoc---simplified-expression-for-derivative}

Given that \(w = f(z) = u + iv\) is analytic, we can write its derivative in several ways.

\begin{description}
\item[Cartesian Form] 
    
    \begin{itemize}
    \item \(u_x + iv_x\)
    \item \(\frac{1}{i}(u_y + iv_y)\)
    \item \(u_x - iu_y\)
    \item \(v_y + iv_x\)
    \end{itemize}
\item[Polar Form] 
    
    \begin{itemize}
    \item \(-\frac{i}{r}(\cos \theta -i\sin \theta) w_\theta\)
    \item \((\cos \theta -i\sin \theta) w_r\)
    \end{itemize}
\end{description}

The Cartesian forms are a result of the CR equations and the nature of the derivative.

\begin{example}[{Proof of polar forms}]
Firstly recall the equations we use to relate the cartesian and polar coordinates are

\begin{equation*}
x = r\cos\theta, \quad y= r\sin\theta
,\quad
r = \sqrt{x^2 + y^2},\quad \tan\theta = \frac{y}{x}
\end{equation*}

We would only be using the first two. Then, we get the following set of equations

\begin{equation*}
\begin{aligned}
&
\begin{bmatrix}
1 & 0\\
0 & 1
\end{bmatrix}
=
\begin{bmatrix}
x_x & x_y\\
y_x & y_y
\end{bmatrix}
=
\begin{bmatrix}
r_x\cos\theta - r\theta_x\sin\theta & r_y\cos\theta - r\theta_y\sin\theta \\
r_x\sin\theta + r\theta_x\cos\theta & r_y\sin\theta + r\theta_y\cos\theta
\end{bmatrix}
=
\begin{bmatrix}
\cos\theta & -r\sin\theta\\
\sin\theta & r\cos\theta
\end{bmatrix}
\begin{bmatrix}
r_x & r_y\\
\theta_x & \theta_y
\end{bmatrix}
\\&\implies
\begin{bmatrix}
r_x & r_y\\
\theta_x & \theta_y
\end{bmatrix}
=
\begin{bmatrix}
\cos\theta & -r\sin\theta\\
\sin\theta & r\cos\theta
\end{bmatrix}^{-1}
=
\frac{1}{r(\cos^2\theta +\sin^2\theta)}
\begin{bmatrix}
r\cos\theta & r\sin\theta\\
-\sin\theta & \cos\theta
\end{bmatrix}
=
\begin{bmatrix}
\cos\theta & \sin\theta\\
-\frac{1}{r}\sin\theta & \frac{1}{r}\cos\theta
\end{bmatrix}
\end{aligned}
\end{equation*}

Then, by using the chain rule

\begin{equation*}
\begin{aligned}
w_x
&= w_r r_x + w_\theta \theta_x
\\&= w_r \cos\theta - w_\theta \frac{1}{r}\sin\theta
\\&= w_r \cos\theta - (u_\theta + iv_\theta) \frac{1}{r}\sin\theta
\\&= w_r \cos\theta - (u_\theta + iv_\theta) \frac{1}{r}\sin\theta
\\&= w_r \cos\theta - (-rv_r + iru_r) \frac{1}{r}\sin\theta \quad\text{by polar CR equations}
\\&= w_r \cos\theta -i (u_r + iv_r)\sin\theta
\\&= w_r \cos\theta -i w_r\sin\theta
\\&= (\cos\theta -i \sin\theta)w_r
\end{aligned}
\end{equation*}

Additionally

\begin{equation*}
\begin{aligned}
w_x
&= (\cos\theta -i \sin\theta)w_r
\\&= (\cos\theta -i \sin\theta)(u_r + i v_r)
\\&= (\cos\theta -i \sin\theta)\frac{1}{r}(v_\theta - i u_\theta)
\\&= (\cos\theta -i \sin\theta)\frac{-i}{r}(u_\theta + i v_\theta)
\\&= -\frac{i}{r}(\cos\theta -i \sin\theta)w_\theta
\end{aligned}
\end{equation*}
\end{example}

\section{Added conditions for sufficiency}
\label{develop--math3275:analytic:page--cr-equations.adoc---added-conditions-for-sufficiency}

The Cauchy-Riemann equations on their own are not sufficient
for a function to be analytic. However, if we add that

\begin{equation*}
u_x, u_y, v_x, v_y
\end{equation*}

are all continuous at some point, then the function is differentiable at that point.

Therefore, if the Cauchy-Riemann equations hold and the partial first derivatives
are continuous some domain, then the function is analytic in that domain (by
definition of analytic).

\begin{example}[{Proof}]
\begin{admonition-note}[{}]
This general idea of this proof (mainly the use of the mean value theorem)
was taken from Dr Dyer's Lecture notes.
\end{admonition-note}

Lets firstly go over what we have.

\begin{equation*}
g(x, y) = \lim_{t\to 0} \frac{f(x + t, y) - f(x, y)}{t}
\quad\text{and}\quad
h(x, y) = \lim_{t\to 0} \frac{f(x, y + t) - f(x, y)}{it}
\quad t \in \mathbb{R}
\end{equation*}

both exist in some neighbourhood of \((x_0, y_0) \in R\) and are continuous
and both equal to the same value at \((x_0, y_0) \in R\)
Let the value they attain at that
point be \(L \in \mathbb{C}\).

Additionally, notice that we can say that \(k(t)\) is differentiable at \(t_0\) iff

\begin{equation*}
\forall \varepsilon > 0: \exists \delta > 0: |t| < \delta \implies |k(t_0 + t) - k(t_0) - tk'(t_0)| \leq |t|\varepsilon
\end{equation*}

This is equivalent to the standard form of the derivative since \(k\) must be continuous at \(t_0\)
and better since it allows for \(t =0\).

Now, consider arbitrary \(\varepsilon > 0\) and let \(\varepsilon_1, \varepsilon_2 > 0\)
be yet to be specified. Then, since \(g\) is continuous and equal to \(L\) at \((x_0, y_0)\), there
exists \(\delta_1 > 0\) such that

\begin{equation*}
|x+ iy| < \delta_1 \implies |g(x_0 + x, y_0+y) - L| < \varepsilon_1
\end{equation*}

Next, since \(h\) exists at \((x_0 , y_0)\) there exists \(\delta_2 > 0\)
such that

\begin{equation*}
|y| < \delta_2 \implies |f(x_0, y_0 + y) - f(x_0, y_0) - yih(x_0, y_0)| < |iy|\varepsilon_2
\end{equation*}

And recall that \(h(x_0, y_0) = L\).

Now, let \(\delta = \min(\delta_1, \delta_2)\) and \(|x + i y| < \delta\). Then
since \(g\) exists in the neighbourhood, by the mean value theorem, there exists
\(0 \leq x' \leq x\) such that

\begin{equation*}
f(x_0 + x, y_0 + y) - f(x_0, y_0 + y) = xg(x_0 + x', y_0 + y)
\end{equation*}

also notice that \(|x' + iy| < |x + iy| < \delta\).
Now, onto our central attraction.

\begin{equation*}
\begin{aligned}
&\left|\frac{f(x_0 + x, y_0 + y) - f(x_0, y_0) - (x+iy)L}{x+iy}\right|
\\&= \left|\frac{xg(x_0 + x', y_0 + y) + f(x_0, y_0 + y) - f(x_0, y_0) - (x+iy)L}{x+ iy}\right|
\\&= \left|\frac{xg(x_0 + x', y_0 + y) - xL}{x + iy}\right| + \left|\frac{f(x_0, y_0 + y) - f(x_0, y_0) - iyL}{x+ iy}\right|
\\&= \left|\frac{x}{x+iy}\right||g(x_0 + x', y_0 + y) - L| + \frac{|f(x_0, y_0 + y) - f(x_0, y_0) - iyL|}{|x+ iy|}
\\&< \left|\frac{x}{x+iy}\right|\varepsilon_1 + \frac{|iy|\varepsilon_2}{|x+ iy|}
\\&\leq \varepsilon_1 + \varepsilon_2
\\&= \varepsilon
\end{aligned}
\end{equation*}

where we finally assign \(\varepsilon_1 = \varepsilon_2 = \frac{\varepsilon}{2}\).

\begin{admonition-remark}[{}]
Now, notice that the real star of this proof is the mean value theorem. Without it, our bound
on \(|f(x_0 + x, y_0 + y) - f(x_0, y_0 + y) - xg(x_0, y_0 + y)|\) would depend on \(y\).
\end{admonition-remark}
\end{example}

\begin{admonition-remark}[{}]
I am yet to prove this, but I believe that we might be able to generalize
this to any two curves which span the region of the function.
\end{admonition-remark}
\end{document}
