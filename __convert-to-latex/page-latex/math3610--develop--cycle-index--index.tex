\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Cycle Index}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\{{#1}\right\}}
\DeclareMathOperator{\sign}{sign}

 \def\phi{\varphi}
% Title omitted
Let \(\sigma\) be a permutation on \(n\) letters. Then, we define
\(c_k(\sigma)\) to be the number of cycles of of length \(k\)
when \(\sigma\) is written as a product of disjoint cycles.
Then, we define the \emph{cycle structure} of \(\sigma\) as

\begin{equation*}
t_1^{c_1(\sigma)}t_2^{c_2(\sigma)} \cdots t_n^{c_n(\sigma)}
\end{equation*}

where \(t_1,\ldots t_n\) are just \(n\) symbols. Also,
typically we let \(t_i\) be the symbol \(i\).
Then, when interpreted as a numerical product,
we obtain the number permutations with the
specified cycle structure.

Then if \(G \leq S_n\) is a permutation group, we define the cycle index of \(G\)
as

\begin{equation*}
H = \frac{1}{|G|} \sum_{\sigma \in G} t_1^{c_1(\sigma)} t_2^{c_2(\sigma)} \cdots t_n^{c_n(\sigma)} = \frac{1}{|G|} \sum h_{i_1, i_2, \ldots i_n} t_1^{i_1}t_2^{i_2}\cdots t_n^{i_n}
\end{equation*}

where \(h_{i_1,\ldots i_n}\) is the number of permutations with structure \(t_1^{i_1}\dots t_n^{i_n}\).

\section{Stores}
\label{develop--math3610:cycle-index:page--index.adoc---stores}

Let \(X\) be a collection on objects and suppose there exists
a function \(\eta\) which maps \(x \in X\) to a (possibly infinite) vector with natural elements.
We call \(\eta(x)\) the \emph{vector structure} of \(x\).

Then, we define \(\mu\) by

\begin{equation*}
\mu(x) = t_1^{\eta(x)_1}t_2^{\eta(x)_2}\cdots
\end{equation*}

where \(\mu(x)\) is the \emph{structure of x}.
We then define the \emph{store} of \(X\) to be

\begin{equation*}
S(t_1, t_2, \ldots) = \sum_{x \in X} \mu(x) = \sum_{i_1, i_2, \ldots \in \mathbb{N}} w_{i_1, i_2, \ldots} t_1^{i_1} t_2^{i_2} \cdots
\end{equation*}

where \(w_{i_1, i_2, \ldots}\) is the number of elements in \(X\) have structure \(t_1^{i_1}t_2^{i_2}\cdots\).

Therefore \(S\) is a generating function for the elements in \(X\). Also, note that
we did not have to define \(S\) in this manner, but could have instead defined
it purely as a generating function.

\begin{admonition-warning}[{}]
Some places call the structure the weight ... which is utterly ridiculous.
\end{admonition-warning}

\section{Configurations}
\label{develop--math3610:cycle-index:page--index.adoc---configurations}

Let \(X\) be a set of \emph{slots} and \(Y\) be a set of objects which can be placed in
those slots. Then the set \(Y^X\) is precisely the set of ways to fill
\(X\) with objects in \(Y\). We call each \(\phi \in Y^X\)
a \emph{configuration} and \(\phi(x)\) a \emph{component}.

\begin{admonition-tip}[{}]
Think of a \(X\) as a machine whose components can be customized
where each component can come from set \(Y\). Then a configuration
is simply the way how we configure the machine.
\end{admonition-tip}

\subsection{Product rule}
\label{develop--math3610:cycle-index:page--index.adoc---product-rule}

Let \(S\) a store for \(Y\). Then, a store \(T\) for \(Y^X\)
satisfies the \emph{product rule} if the structure of \(\phi \in Y^X\)
is determined by the product of structures of its components.
That is

\begin{equation*}
\mu(\phi) = \prod_{x \in X} \mu(\phi(x))
\end{equation*}

Furthermore, if \(n = |X|\) is finite, we see that

\begin{equation*}
T = S^n
\end{equation*}

since the structure of each configuration on \(Y^X\) is uniquely determined
by a term in \(S^n\) (before simplification).

\begin{admonition-note}[{}]
Sometimes that last relation is what is meant by the product rule.
\end{admonition-note}

\subsection{Actions on configurations}
\label{develop--math3610:cycle-index:page--index.adoc---actions-on-configurations}

Suppose \(G\) is a group action on \(X\). Then, \(\sigma \in G\)
can be thought of as a permutation on \(X\)
and hence \(G\) induces an action on \(Y^X\).

Let \(\phi \in Y^X\) be a configuration,
then we define \(\sigma\phi\) as

\begin{equation*}
(\sigma\phi)(x) = \phi(\sigma(x))
\end{equation*}

Furthermore, notice that if store \(T\) satisfies the product rule,
then the structure of \(\sigma\phi\) is the same as the structure
of \(\phi\). And hence we may define the structure of the orbit
of \(\phi\) to be just the structure of \(\phi\).

\begin{example}[{Proof}]
\begin{equation*}
\mu(\sigma\phi) = \prod_{x \in X} \mu(\sigma\phi(x)) = \prod_{x \in X} \mu(\phi(\sigma(x))) = \prod_{x \in \sigma X} \mu(\phi(x)) = \prod_{x \in X} \mu(\phi(x)) = \mu(\phi)
\end{equation*}

since \(\sigma\) is a permutation on \(X\).
\end{example}

\subsection{Polya's Theorem}
\label{develop--math3610:cycle-index:page--index.adoc---polyas-theorem}

Let

\begin{itemize}
\item \(|X| = n\) (finite)
\item \(G\) be a group action on \(X\)
\item \(S\) be a store for \(Y\)
\item \(T\) be a store on \(Y^X\) which satisfies the product rule
\item \(H\) be the cycle index of \(G\)
\item \(F\) be the store of the orbits of \(Y^X\)
\end{itemize}

Then Polya's theorem asserts that

\begin{equation*}
F(t_1, t_2, \ldots) = H\left(S(t_1, t_2, \ldots), S(t_1^2, t_2^2, \ldots), \ldots S(t_1^n, t_2^n, \ldots)\right)
\end{equation*}

\begin{example}[{Proof}]
Let \(X^Y/G\) denote the set of orbits. Then,

\begin{equation*}
\begin{aligned}
F(t_1, t_2, \ldots)
&= \sum_{[\phi] \in X^Y/G} \mu(\phi) \quad\text{by definition}
\\&= \sum_{\phi \in X^Y} \frac{1}{|[\phi]|} \mu(\phi)
\\&= \sum_{\phi \in X^Y} \frac{|G_\phi|}{|G|} \mu(\phi) \quad\text{by the orbit stabalizer theorem}
\\&= \frac{1}{|G|}\sum_{\phi \in X^Y} \sum_{\sigma \in G_\phi} \mu(\phi)
\\&= \frac{1}{|G|}\sum_{\sigma \in G} \sum_{\phi \in G^\sigma} \mu(\phi)
\end{aligned}
\end{equation*}

where \(G^\sigma\) is the set of configurations fixed by \(\sigma\). Now, lets compare this to what we want to get.

\begin{equation*}
H\left(S(t_1, t_2, \ldots), S(t_1^2, t_2^2, \ldots), \ldots S(t_1^n, t_2^n, \ldots)\right)
=\frac{1}{|G|}\sum_{\sigma \in G} \prod_{k=1}^n S(t_1^k, t_2^k, \ldots)^{c_k(\sigma)}
\end{equation*}

Then, if we show that for each \(\sigma \in G\),

\begin{equation*}
\sum_{\phi \in G^\sigma} \mu(\phi) = \prod_{k=1}^n S(t_1^k, t_2^k, \ldots)^{c_k(\sigma)}
\end{equation*}

On the left we have the store for \(G^\sigma\).
Then, we would partition each function \(\phi \in G^\sigma\) according to the
cycle decomposition of \(\sigma\).

Then, notice if we have cycle \((a_1, \ldots a_k)\) in the cycle decomposition on \(\sigma\),
\(\phi\) must have the same value for each of these \(a_i\). Therefore, the
partial store for \(G^\sigma\) on these \(a_i\) is given by
\(S(t_1^k, t_2^k, \ldots)\) since the store follows the product rule and we just need
\(k\) times the components.

Hence the store for \(G^\phi\) is precisely the right hand side of the equation since
it accounts for all possible configurations which \(\sigma\) fixes. Therefore we are done.
\end{example}
\end{document}
