\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Simple Linear Regression}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% life is a lie
\def\bigchi{\chi}
\DeclareMathOperator{\SST}{SST}
\DeclareMathOperator{\SSE}{SSE}
\DeclareMathOperator{\SSTr}{SSTr}
\DeclareMathOperator{\SSB}{SSB}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\MSTr}{MSTr}
\DeclareMathOperator{\MSB}{MSB}

\def\bm#1{\mathbf #1}

% Title omitted
Regression analysis is used to predict the value of a dependent variable
based on the values of one or more independent variables. Additionally,
it is used to explain the how changes in the independent variable(s)
affect the value of the dependent variable.

In this section, we will only focus on the case when there is only one
independent variable and where the relationship can be described using a
linear function. We call this approach \emph{simple linear regression}.

\section{The model}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---the-model}

For simple linear regression, we have the following requirements

\begin{itemize}
\item Linearity: The relationship between the independent and dependent
    variable is linear.
\item Independence: The random errors are independent
\item Normality of error: The random error is normally distributed with mean
    \(0\).
\item Equal variance: The probability distribution of the random error has
    constant variance. This is also called homoscedasticity.
\end{itemize}

We will denote the independent variables \(X_1, \ldots X_n\)
and dependent variables \(Y_1, \ldots Y_n\), then we write
each of the \(Y_i\) as follows

\begin{equation*}
Y_i = \alpha + \beta X_i + \varepsilon_i
\end{equation*}

where

\begin{itemize}
\item \(\varepsilon_i\) is the random error associated with
    \(Y_i\).
\item \(\alpha\) is the \emph{population \(Y\) intercept}.
\item \(\beta\) is the \emph{population slope coefficient}.
\end{itemize}

\subsection{Least squares model}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---least-squares-model}

In order to estimate the values of \(\alpha\) and
\(\beta\), we use the least squares model in which we seek to
minimize the sum of squares difference between the observed and
predicted values of the dependent variable. That is, we seek to minimize

\begin{equation*}
\sum_{i=1}^n (Y_i - \hat{Y}_i)^2 = \sum_{n=1}^n (Y_i - (\hat\alpha + \hat\beta X_i))^2
\end{equation*}

where \(\hat{Y}_i = \hat\alpha + \hat\beta X_i\) is the
estimated value for \(Y_i\). Then, we obtain that

\begin{equation*}
\hat\alpha = \overbar{y} - \hat\beta\overbar{x} \sim N\left( \alpha, \sigma^2\left[ \frac{1}{n} + \frac{\overbar{x}^2}{S_{XX}} \right] \right)
\quad\text{and}\quad
\hat\beta = \frac{S_{XY}}{S_{XX}} \sim N\left(\beta, \frac{\sigma^2}{S_{XX}}\right)
\end{equation*}

where

\begin{itemize}
\item \(S_{XX} = \sum_{i=1}^n (x_i - \overbar{x})^2 = \sum_{i=1}^n x_i^2 - n \overbar{x}^2\).
\item \(S_{YY} = \sum_{i=1}^n (y_i - \overbar{y})^2 = \sum_{i=1}^n y_i^2 - n \overbar{y}^2\).
\item \(S_{XY} = \sum_{i=1}^n (x_i - \overbar{x})(y_i - \overbar{y}) = \sum_{i=1}^n x_iy_i - n\overbar{x}\overbar{y}\)
\end{itemize}

\begin{example}[{Proof of values of parameters}]
Let

\begin{equation*}
\begin{aligned}
S
&= \sum_{i=1}^n (y_i - \hat{y}_i)^2
\\&= \sum_{n=1}^n (y_i - \alpha - \beta x_i)^2
\\&= \sum_{n=1}^n \left[y_i^2 + \alpha^2 + \beta^2 x_i^2 - 2\alpha y_i -2\beta x_iy_i + 2\alpha\beta x_i\right]
\\&= \sum_{n=1}^n y_i^2 + n\alpha^2 + \beta^2 \sum_{i=1}^n x_i^2 - 2\alpha \sum_{i=1}^n y_i -2\beta \sum_{i=1}^n x_iy_i + 2\alpha\beta \sum_{i=1}^n x_i
\end{aligned}
\end{equation*}

and to minimize this, we find the partial differentials and set them equal to zero

\begin{equation*}
\frac{\partial S}{\partial \alpha} = 2n\alpha - 2\sum_{i=1}^n y_i + 2\beta \sum_{i=1}^n x_i =0
    \iff \sum_{i=1}^n y_i = n\alpha + \left(\sum_{i=1}^n x_i\right)\beta
\end{equation*}

\begin{equation*}
\frac{\partial S}{\partial \beta} = 2\beta \sum_{i=1}^n x_i^2 - 2\sum_{i=1}^n x_i y_i + 2\alpha\sum_{i=1}^n x_i = 0
    \iff \sum_{i=1}^n x_i y_i = \left(\sum_{i=1}^n x_i\right)\alpha + \left(\sum_{i=1}^n x_i^2\right)\beta
\end{equation*}

More compactly, in matrix form,

\begin{equation*}
\begin{bmatrix}
    \sum y_i \\
    \sum x_i y_i
\end{bmatrix}
=
\begin{bmatrix}
    n & \sum x_i\\
    \sum x_i & \sum x_i^2
\end{bmatrix}
\begin{bmatrix}
    \alpha \\
    \beta
\end{bmatrix}
\iff
\begin{bmatrix}
    \overbar{y} \\
    \frac{1}{n}\sum x_i y_i
\end{bmatrix}
=
\begin{bmatrix}
    1 & \overbar{x}\\
    \overbar{x} & \frac{1}{n}\sum x_i^2
\end{bmatrix}
\begin{bmatrix}
    \alpha \\
    \beta
\end{bmatrix}
\end{equation*}

where summations run from \(i=1\ldots n\). Therefore, we get that

\begin{equation*}
\begin{aligned}
\begin{bmatrix}
    \alpha \\
    \beta
\end{bmatrix}
&=
\frac{1}{\frac{1}{n} \sum x_i^2 - \overbar{x}^2}
\begin{bmatrix}
    \frac{1}{n} \sum x_i^2 & -\overbar{x}\\
    - \overbar{x} & 1
\end{bmatrix}
\begin{bmatrix}
    \overbar{y} \\
    \frac{1}{n}\sum x_i y_i
\end{bmatrix}
\\&=
\frac{1}{nS_{XX}}
\begin{bmatrix}
    \overbar{y}\frac{1}{n} \sum x_i^2 -\overbar{x}\frac{1}{n}\sum x_iy_i\\
    - \overbar{x}\overbar{y} + \frac{1}{n}\sum x_i y_i
\end{bmatrix}
\\&=
\frac{n}{S_{XX}}
\begin{bmatrix}
    \overbar{y}\frac{1}{n} \sum x_i^2 -\overbar{x}\frac{1}{n}\sum x_iy_i\\
    \frac{1}{n}S_{XY}
\end{bmatrix}
\\&=
\frac{1}{S_{XX}}
\begin{bmatrix}
    \overbar{y} \sum x_i^2 -\overbar{x}\sum x_iy_i\\
    S_{XY}
\end{bmatrix}
\end{aligned}
\end{equation*}

Therefore, we get that \(\beta = \frac{S_{XY}}{S_{XX}}\) and instead of using this complicated
formula for \(\alpha\), we use one from the previous matrix equation

\begin{equation*}
\overbar{y} = \alpha + \beta \overbar{x} \iff \alpha = \overbar{y} - \beta\overbar{x}
\end{equation*}
\end{example}

\begin{example}[{Proof of distribution of parameters}]
Firstly, consider

\begin{equation*}
\hat\beta = \frac{S_{XY}}{S_{XX}} = \frac{\sum_{i=1}^n X_i Y_i - n\overbar{X}\overbar{Y}}{\sum_{i=1}^n X_i^2 - n \overbar{X}^2}
\end{equation*}

Then, notice that the \(X_i\) are constant and only the \(Y_i\) vary. Therefore, since \(\hat\beta\) is a linear combination
of the \(Y_i\), each of which is normally distributed, \(\hat\beta\) is also normally distributed. Therefore,
we need only find the expectation and variance of \(\hat\beta\) to determine its distribution.

Then, since \(E[Y_i] = E[\alpha + \beta X_i + \varepsilon] = \alpha + \beta X_i\), we get

\begin{equation*}
\begin{aligned}
S_{XX}E[\hat\beta]
&= E\left[\sum_{i=1}^n X_i Y_i - n\overbar{X}\overbar{Y}\right]
\\&= \sum_{i=1}^n X_i E[Y_i] - n\overbar{X}E[\overbar{Y}]
\\&= \sum_{i=1}^n X_i E[Y_i] - \overbar{X}\sum_{i=1}^nE[Y_i]
\\&= \sum_{i=1}^n X_i (\alpha + \beta X_i) - \overbar{X}\sum_{i=1}^n(\alpha + \beta X_i)
\\&= \alpha \sum_{i=1}^n X_i + \beta \sum_{i=1}^n X_i^2 - n\alpha\overbar{X} + \beta\overbar{X}\sum_{i=1}^n X_i
\\&= n\alpha \overbar{X} + \beta \sum_{i=1}^n X_i^2 - n\alpha\overbar{X} + \beta n\overbar{X}^2
\\&= \beta \left[\sum_{i=1}^n X_i^2 + n\overbar{X}^2\right]
\\&= \beta S_{XX}
\end{aligned}
\end{equation*}

Therefore, \(E[\hat\beta] = \beta\).
And since \(Var[Y_i] = Var[\alpha + \beta X_i + \varepsilon] = \sigma^2\), we get

\begin{equation*}
\begin{aligned}
Var[\hat\beta]
&= \frac{1}{S_{XX}^2} Var\left[\sum_{i=1}^n X_i Y_i - n\overbar{X}\overbar{Y}\right]
\\&= \frac{1}{S_{XX}^2} Var\left[\sum_{i=1}^n X_i Y_i - \overbar{X}\sum_{i=1}^n Y_i\right]
\\&= \frac{1}{S_{XX}^2} Var\left[\sum_{i=1}^n (X_i - \overbar{X}) Y_i\right]
\\&= \frac{1}{S_{XX}^2} \sum_{i=1}^n (X_i - \overbar{X})^2 Var[ Y_i]
\\&= \frac{1}{S_{XX}^2} \sum_{i=1}^n (X_i -n^2\overbar{X})^2 \sigma^2
\\&= \frac{1}{S_{XX}^2} S_{XX} \sigma^2
\\&= \frac{\sigma^2}{S_{XX}}
\end{aligned}
\end{equation*}

Therefore, \(\hat\beta \sim N\left(\beta, \frac{\sigma^2}{S_{XX}}\right)\). Next, since \(\hat\alpha\)
is just a linear transformation on \(\hat\beta\) and \(Y_i\), it also has normal distribution where

\begin{equation*}
\begin{aligned}
E[\hat\alpha]
&= E\left[\overbar{Y} - \hat\beta \overbar{X}\right]
\\&= E\left[\frac{1}{n}\sum_{i=1}^n Y_i - \hat\beta \overbar{X}\right]
\\&= \frac{1}{n}\sum_{i=1}^n E[Y_i] - E[\hat\beta] \overbar{X}
\\&= \frac{1}{n}\sum_{i=1}^n (\alpha + \beta X_i) - \beta \overbar{X}
\\&= \alpha + \beta \overbar{X} - \beta \overbar{X}
\\&= \alpha
\end{aligned}
\end{equation*}

Next, for the variance, we use the form produced in the previous proof

\begin{equation*}
\begin{aligned}
Var[\hat\alpha]
&= \frac{1}{S_{XX}^2}Var\left[\overbar{Y}\sum_{i=1}^n X_i^2 - \overbar{X}\sum_{i=1}^n X_iY_i\right]
\\&= \frac{1}{S_{XX}^2}Var\left[\frac{1}{n}\sum_{i=1}^n X_i^2\sum_{i=1}^n Y_i - \overbar{X}\sum_{i=1}^n X_iY_i\right]
\\&= \frac{1}{S_{XX}^2}Var\left[\sum_{i=1}^n \left(\frac{1}{n}\sum_{j=1}^n X_j^2 - \overbar{X}X_i\right)Y_i\right]
\\&= \frac{1}{S_{XX}^2}\sum_{i=1}^n \left(\frac{1}{n}(S_{XX} + n\overbar{X}^2) - \overbar{X}X_i\right)^2\sigma^2
\\&= \frac{\sigma^2}{S_{XX}^2}\sum_{i=1}^n\left[\frac{1}{n^2}(S_{XX} + n\overbar{X}^2)^2 - 2\overbar{X}X_i\frac{1}{n}(S_{XX} + n\overbar{X}^2) + \overbar{X}^2X_i^2\right]
\\&= \frac{\sigma^2}{S_{XX}^2}\left[\frac{1}{n}(S_{XX} + n\overbar{X}^2)^2 - 2\overbar{X}\overbar{X}(S_{XX} + n\overbar{X}^2) + \overbar{X}^2\sum_{i=1}^n X_i^2\right]
\\&= \frac{\sigma^2}{S_{XX}^2}\left[\frac{1}{n}(S_{XX} + n\overbar{X}^2)^2 - 2\overbar{X}^2(S_{XX} + n\overbar{X}^2) + \overbar{X}^2(S_{XX} + n\overbar{X}^2)\right]
\\&= \frac{\sigma^2}{S_{XX}^2}\left[\frac{S_{XX}^2}{n} + 2S_{XX}\overbar{X}^2 + n\overbar{X}^4 - 2\overbar{X}^2S_{XX} - 2n\overbar{X}^4 + \overbar{X}^2S_{XX} + n\overbar{X}^4\right]
\\&= \frac{\sigma^2}{S_{XX}^2}\left[\frac{S_{XX}^2}{n} + \overbar{X}^2S_{XX}\right]
\\&= \sigma^2\left[\frac{1}{n} + \frac{\overbar{X}^2}{S_{XX}}\right]
\end{aligned}
\end{equation*}

Therefore, we get the desired result.
\end{example}

\begin{example}[{Proof of equivalent formulas for sum of squares}]
We would only prove the first formula and two previous would follow

\begin{equation*}
\begin{aligned}
S_{XY}
&= \sum_{i=1}^n (x_i - \overbar{x})(y_i - \overbar{y})
\\&= \sum_{i=1}^n (x_iy_i - \overbar{x}y_i - x_i\overbar{y} + \overbar{x}\overbar{y})
\\&= \sum_{i=1}^n x_iy_i - \overbar{x}\sum_{i=1}^n y_i - \overbar{y}\sum_{i=1}^nx_i + n\overbar{x}\overbar{y}
\\&= \sum_{i=1}^n x_iy_i - n\overbar{x}\overbar{y} - n\overbar{x}\overbar{y} + n\overbar{x}\overbar{y}
\\&= \sum_{i=1}^n x_iy_i - n\overbar{x}\overbar{y}
\end{aligned}
\end{equation*}
\end{example}

Then, we see that \(\hat\alpha\) and
\(\hat\beta\) are unbiased estimators and we can interpret
their values as follows

\begin{itemize}
\item \(\hat{\alpha}\) is the estimated average value for
    \(Y\) when \(X\) is zero
\item \(\hat{\beta}\) is the estimated change in the average value
    of \(Y\) as a result of a one unit change in \(X\)
\end{itemize}

\section{ANOVA Table for regression}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---anova-table-for-regression}

We can split the total variation from the regression line as follows

\begin{equation*}
\mathrm{SST} = \mathrm{SSR} + \mathrm{SSE}
\end{equation*}

where

\begin{itemize}
\item \(\mathrm{SST} = \sum_{i=1}^n (Y_i - \overbar{Y})^2\)
    is the total variation
\item \(\mathrm{SSR} = \sum_{i=1}^n (\hat{Y}_i - \overbar{Y})^2\)
    is the variation due to the regression (explained variation)
\item \(\mathrm{SSE} = \sum_{i=1}^n (Y_i- \hat{Y}_i)^2\) is the
    unexplained variation
\end{itemize}

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
\mathrm{SST}
&= \sum_{i=1}^n (Y_i - \overbar{Y})^2
\\&= \sum_{i=1}^n (Y_i - \hat{Y}_i + \hat{Y}_i - \overbar{Y})^2
\\&= \sum_{i=1}^n \left[(Y_i - \hat{Y}_i)^2 + (\hat{Y}_i - \overbar{Y})^2 + 2(Y_i - \hat{Y}_i)(\hat{Y}_i - \overbar{Y})\right]
\\&= \sum_{i=1}^n (Y_i - \hat{Y}_i)^2 + \sum_{i=1}^n (\hat{Y}_i - \overbar{Y})^2 + 2\sum_{i=1}^n(Y_i - \hat{Y}_i)(\hat{Y}_i - \overbar{Y})
\\&= \mathrm{SSE} + \mathrm{SSR} + 2\sum_{i=1}^n(Y_i - \hat{Y}_i)(\hat{Y}_i - \overbar{Y})
\end{aligned}
\end{equation*}

Now, focus on the last term

\begin{equation*}
\begin{aligned}
\sum_{i=1}^n(Y_i - \hat{Y}_i)(\hat{Y}_i - \overbar{Y})
&= \sum_{i=1}^n(Y_i - \hat{Y}_i)(\hat{Y}_i - \overbar{Y})
\\&= \sum_{i=1}^n(Y_i - \hat\alpha - \hat\beta X_i)(\hat\alpha + \hat\beta X_i - \overbar{Y})
\\&= \sum_{i=1}^n(Y_i - \overbar{Y} + \hat\beta\overbar{X} - \hat\beta X_i)(\overbar{Y} - \hat\beta\overbar{X} + \hat\beta X_i - \overbar{Y})
\\&= \sum_{i=1}^n(Y_i - \overbar{Y} + \hat\beta(\overbar{X} - X_i))\hat\beta(X_i - \overbar{X})
\\&= \hat\beta\sum_{i=1}^n(Y_i - \overbar{Y})(X_i - \overbar{X}) + \hat\beta^2 \sum_{i=1}^n(\overbar{X} - X_i)(X_i - \overbar{X})
\\&= \hat\beta S_{XY} - \hat\beta^2 S_{XX}
\\&= \hat\beta \left[ S_{XY} - \hat\beta S_{XX} \right]
\\&= \hat\beta \left[ S_{XY} - \frac{S_{XY}}{S_{XX}} S_{XX} \right]
\\&= 0
\end{aligned}
\end{equation*}
\end{example}

We then define \emph{coefficient of determination}, \(r^2\) as

\begin{equation*}
r^2 = \frac{\mathrm{SSR}}{\mathrm{SST}}
\end{equation*}

which can be interpreted as the proportion of variation which is
explained by the model. Below are some interpretations of theoretical
values

\begin{itemize}
\item \(r^2 = 0\), there is no linear relationship between
    \(X\) and \(Y\).
\item \(0 < r^2 < 1\), there is some linear relationship between
    \(X\) and \(Y\).
\item \(r^2 = 1\), there is a perfect linear relationship between
    \(X\) and \(Y\).
\end{itemize}

\subsection{Estimation}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---estimation}

\subsection{Standard Error of estimate}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---standard-error-of-estimate}

We define the \emph{standard error of estimate} as

\begin{equation*}
S = \sqrt{\frac{\mathrm{SSE}}{n-1}}
\end{equation*}

which is the standard deviation of the variation of the observations
around the regression line.

\section{Residual Analysis}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---residual-analysis}

Firstly, we use the following function to construct the linear model for
the data and store it in the 'result' variable,

\begin{listing}[{}]
result <- lm(formula = dependent ~ independent, data=data)
\end{listing}

we then plot a scatter plot of the residuals using

\begin{listing}[{}]
plot(data$independent, result$residuals)
# or
plot(result$fitted, result$residuals)
\end{listing}

Then

\begin{itemize}
\item The data is linear and independent if the points are evenly
    distributed around the x-axis.
\item The data has constant variance if the points are evenly distributed
    around the x-axis (or abound the regression line)
\end{itemize}

For normality, we use the qq plot as follows

\begin{listing}[{}]
qqnorm(result$residuals)
qqline(result$residuals)
\end{listing}

If the data is normal, the points follow the 45 degree line.

\section{Inference from Slope}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---inference-from-slope}

We can perform a T-test on the slope coefficient \(\beta\) to
determine whether or not there is a linear relationship between the
independent and dependent variables. We test the two hypotheses,

\begin{itemize}
\item \(H_0: \beta = 0\) (no linear relationship between the
    variables)
\item \(H_1: \beta \neq 0\) (there is a linear relationship between
    the variables)
\end{itemize}

We use the test statistic

\begin{equation*}
t_{stat} = \frac{\hat{\beta} - \beta}{S_\beta} \sim T_{n-2}
\end{equation*}

where

\begin{itemize}
\item \(\hat\beta\) is the estimated regression slope coefficient
\item \(\beta\) the hypothesized slope under \(H_0\)
\item \(S_\beta = \frac{S}{\sqrt{S_{XX}}}\) is the standard error
    of the slope
\item \(S = \sqrt{\frac{\mathrm{SSE}}{n-2}}\) is the standard
    error of the estimate (see above)
\end{itemize}

We can use the output of the 'result' variable produced in residual
analysis to obtain values for this test

\begin{listing}[{}]
result <- lm(formula = dependent ~ independent, data=data)
result
# look in the coefficients table in the row corresponding to the independent variable
\end{listing}

\subsection{F-test of significance: OMNIBUS F-test}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---f-test-of-significance-omnibus-f-test}

Likewise to previous ANOVA, we have a \(F\) statistic which we
can use to perform an overall test on the linear relationship. This
test, unlike the previous \(T\) test, can be used with more
than one independent variable. We use the following test-statistic

\begin{equation*}
F_{stat} = \frac{\mathrm{MSR}}{\mathrm{MSE}} \sim F_{k, n-k-1}
\end{equation*}

where

\begin{itemize}
\item \(\mathrm{MSR} = \frac{\mathrm{SSR}}{k}\)
\item \(\mathrm{MSE} = \frac{\mathrm{SSE}}{n - k -1}\)
\item \(k\) is the number of independent variables in the
    regression model.
\end{itemize}

We can again use the output of the 'result' to obtain values for this
test

\begin{listing}[{}]
result <- lm(formula = dependent ~ independent, data=data)
result
# look for the F-statistic on the last line
\end{listing}

Additionally, we can use the 'anova' function to perform the
calculations

\begin{listing}[{}]
anova(result)
\end{listing}

\subsubsection{Note}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---note}

when \(k=1\) (there is only one independent variable), this
test is equivalent to the t-test. That is, the result always coincides
with the result of the t-test.

\subsection{Confidence interval}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---confidence-interval}

Finally, we can construct a confidence interval to test the hypotheses.
We can use the output of the 'confint' command as follows

\begin{listing}[{}]
confint(result, level=0.95)
\end{listing}

and if \(0\) lies in the interval, we do not reject
\(H_0\).

\section{Correlation}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---correlation}

Suppose instead, that we are only interested in the presence of a linear
relationship between the two variables \(X\) and
\(Y\) (note there is neither any independent nor dependent
variable). Then, we want to measure the correlation between the two
variables.

We use the \emph{correlation coefficient}, \(\rho\), defined as
follows,

\begin{equation*}
\rho = \frac{Cov(X, Y)}{\sigma_X \sigma_Y}
\end{equation*}

For samples, we use Person’s Correlation Coefficient \(r\)

\begin{equation*}
r = \frac{S_{XY}}{\sqrt{S_{XX}S_{YY}}}
\end{equation*}

as a estimate for \(\rho\). We have the following
interpretations for (theoretical) values

\begin{itemize}
\item \(r=1\), then \(X\) and \(Y\) have a
    perfect positive linear correlation.
\item \(r=-1\), then \(X\) and \(Y\) have a
    perfect negative linear correlation.
\item \(r = 0\), then \(X\) and \(Y\) are not
    linearly correlated.
\item \(r > 0\), \(X\) and \(Y\) have a
    positive linear relationship.
\item \(r < 0\), \(X\) and \(Y\) have a
    negative linear relationship.
\end{itemize}

While, we use the magnitude of \(r\) to determine the strength
of the relationship

\begin{itemize}
\item \(0 \leq |r| \leq 0.5\) then we say that \(X\) and
    \(Y\) have a weak linear relationship
\item \(0.5 \leq |r| \leq 0.8\) then we say that \(X\)
    and \(Y\) have a moderate linear relationship
\item \(0.8 \leq |r|\) then we say that \(X\) and
    \(Y\) have a strong linear relationship
\end{itemize}

Note, that if there are any unaccounted outliers in the data set, we
perform the test both with and without the point and determine whether
it causes a significant effect.

\subsection{Properties of coefficient}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---properties-of-coefficient}

Notice firstly that

\begin{equation*}
-1 \leq r \leq 1
\end{equation*}

since \(S_{XY}\) is an inner product. Next, notice that
\(r\) is

\begin{itemize}
\item symmetric in \(X\) and \(Y\)
\item scale independent
\item location independent
\end{itemize}

Finally note that as hinted by notation \(r^2\) is the same
coefficient of determination which was previously discussed.
That is,

\begin{equation*}
\frac{\mathrm{SSR}}{\mathrm{SST}} = \left(\frac{ S_{XY} }{\sqrt{ S_{XX}S_{YY}} } \right)^2
\end{equation*}

\begin{example}[{Proof}]
\begin{equation*}
\begin{aligned}
    \frac{\mathrm{SSR}}{\mathrm{SST}}
  &=\frac{ \sum_{i=1}^n (\hat{Y}_i - \overbar{Y})^2 }{ \sum_{i=1}^n (Y_i - \overbar{Y})^2 }
  \\&=\frac{ \sum_{i=1}^n (\hat{\alpha} + \hat\beta X_i - \overbar{Y})^2 }{ S_{YY} }
  \\&=\frac{ \sum_{i=1}^n ((\overbar{Y} - \hat\beta\overbar{X}) + \hat\beta X_i - \overbar{Y})^2 }{ S_{YY} }
  \\&=\frac{ \beta^2\sum_{i=1}^n ( X_i - \hat\beta\overbar{X})^2 }{ S_{YY} }
  \\&=\frac{ \beta^2 S_{XX} }{ S_{YY} }
  \\&=\frac{ \left(\frac{S_{XY}}{S_{XX}}\right)^2 S_{XX} }{ S_{YY} }
  \\&=\frac{ S_{XY}^2 }{ S_{XX}S_{YY} }
\end{aligned}
\end{equation*}
\end{example}

\subsection{Hypothesis test}
\label{develop--math2275:ROOT:page--simple-linear-regression.adoc---hypothesis-test}

We can perform a \(T\)-test on the correlation coefficient, to
determine whether the two variables are linearly correlated. We test the
hypotheses,

\begin{itemize}
\item \(H_0: \rho = 0\) (no linear relationship between the
    variables)
\item \(H_1: \rho \neq 0\) (there is a linear relationship between
    the variables)
\end{itemize}

and use the test statistic

\begin{equation*}
t_{stat} = \frac{r}{\sqrt{\frac{1-r^2}{n-2}}} \sim T_{n-2}
\end{equation*}

Note that we must first inspect the data to determine if there is any
indication of a linear relationship. We use the following command

\begin{listing}[{}]
plot(X, Y)
\end{listing}

and compute the correlation coefficient using 'cor(X, Y)'. Next, we
perform the test as follows

\begin{listing}[{}]
cor.test(X, Y, alternative="two-sided", method="pearson", conf.level=0.95)
\end{listing}
\end{document}
