\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Derivatives of a composition}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\{{#1}\right\}}
\DeclareMathOperator{\sign}{sign}

% Title omitted
Let \(f\) and \(g\) be differentiable functions and consider \(f\circ g\).
Then the derivatives of \(f \circ g\) are

\begin{equation*}
\begin{aligned}
D[f \circ g (x)] &= \frac{d f}{du} \frac{du} {dx}
\\
D^2[f \circ g (x)] &= \frac{d f}{du} \frac{d^2 u}{dx^2} + \frac{d^2 f}{du^2}\left(\frac{du}{dx}\right)^2
\\
D^3[f \circ g (x)] &=
\frac{d f}{d u} \frac{d^3 u}{dx^3} + \frac{d^2f}{du^2}\frac{du}{dx} \frac{d^2u}{du^2}
+ \frac{d^3 f}{du^3}\left(\frac{du}{dx}\right)^2 + 2\frac{d^2 f}{du^2}\frac{du}{dx}\frac{d^2u}{du^2}
\\&\quad=
\frac{d f}{d u} \frac{d^3 u}{dx^3} + 3\frac{d^2f}{du^2}\frac{du}{dx} \frac{d^2u}{du^2}
+ \frac{d^3 f}{du^3}\left(\frac{du}{dx}\right)^2
\\
\end{aligned}
\end{equation*}

where \(u = g(x)\). Then, we see that this gets messy very quickly. For the sake of simplicity, we would let
\(f(u) = e^{au}\) hence \(f^{(n)}(u)\) is given by a unique power of \(a\). And, we use the notation
\(g_n(x) = g^{(n)}(x)\).

Now, let

\begin{equation*}
A_n(a, x)
= \frac{1}{e^{g(x)}} D^n[f \circ g(x)]
= \frac{1}{e^{g(x)}} D^n[e^{ag(x)}]
\end{equation*}

Then, \(A_0(a,x) = 1\) and

\begin{equation*}
\begin{aligned}
A_{n+1}(a, x)
&= \frac{1}{e^{g(x)}} D^{n+1}[e^{ag(x)}]
\\&= \frac{1}{e^{g(x)}} D^n[D(e^{ag(x)})]
\\&= \frac{1}{e^{g(x)}} D^n[ae^{ag(x)}g_1(x)]
\\&= \frac{a}{e^{g(x)}} \sum_{k=0}^n \binom{n}{k} D^{n-k}(e^{ag(x)}) D^k(g_1(x))
\\&= a\sum_{k=0}^n \binom{n}{k} \frac{1}{e^{g(x)}} D^{n-k}(e^{ag(x)}) g_{k+1}(x)
\\&= a\sum_{k=0}^n \binom{n}{k} A_{n-k}(a,x) g_{k+1}(x)
\end{aligned}
\end{equation*}

\begin{admonition-remark}[{}]
While proving this, Dr De'matas used notational convenience of \(g_n = g^n\) and \(A_{n} = A^{n}\) in order to rewrite
\(A_{n+1}\) as \(ag(A + g)^n\). Although this notation is no issue as long as you are careful to not
conflate it with exponentiation; however, that is a massive burden of carefulness. As a result, we
would not use this convenience notation.
\end{admonition-remark}

Now, consider

\begin{equation*}
B(a,x,t) = \sum_{n=0}^\infty A_n(a,x) \frac{t^n}{n!}
\end{equation*}

Now, by differentiating wrt \(t\) and reindexing, we get

\begin{equation*}
\begin{aligned}
&B_t(a,x,t)
\\&= \sum_{n=0}^\infty A_{n+1}(a,x)\frac{t^{n}}{n!}
\\&= a \sum_{n=0}^\infty \sum_{k=0}^n \binom{n}{k} A_{n-k}(a,x) g_{k+1}(x) \frac{t^{n}}{n!}
\\&= a \sum_{0 \leq k \leq n} \frac{1}{k!(n-k)!} A_{n-k}(a,x) g_{k+1}(x) t^{n}
\\&= a \sum_{k=0}^\infty \sum_{n=k}^\infty \frac{1}{k!(n-k)!} A_{n-k}(a,x) g_{k+1}(x) t^{k + n-k}
\\&= a \sum_{k=0}^\infty \sum_{m=0}^\infty \frac{1}{k!m!} A_{m}(a,x) g_{k+1}(x) t^{k + m}
\\&= a \sum_{k=0}^\infty g_{k+1}(x)\frac{t^k}{k!} \sum_{m=0}^\infty A_{m}(a,x) \frac{t^{m}}{m!}
\\&= a \sum_{k=0}^\infty g_{k+1}(x)\frac{t^k}{k!} B(a,x,t)
\end{aligned}
\end{equation*}

Therefore

\begin{equation*}
\ln(B(a,x,t))
= \int \frac{B_t(a,x,t)}{B(a,x,t)} \ dt
= \int a \sum_{k=0}^\infty g_{k+1}(x)\frac{t^k}{k!} \ dt
= a \sum_{k=1}^\infty g_{k}(x)\frac{t^k}{k!} + C(a,x)
\end{equation*}

However, notice when \(t=0\), we get \(B(a,x,t) = A_0(a,x) = 0\) so \(C(a,x) = 0\).
Therefore,

\begin{equation*}
B(a,x,t)
= \exp\left(a \sum_{k=1}^\infty g_k(x)\frac{t^k}{k!} \right)
= \prod_{k=1}^\infty \exp\left(a g_k(x)\frac{t^k}{k!} \right)
= \prod_{i=1}^\infty \exp\left(a g_i(x)\frac{t^i}{i!} \right)
= \prod_{i=1}^\infty \sum_{k_i=1}^\infty (at^i)^{k_i} \left(\frac{g_i(x)}{i!} \right)^{k_i}
\end{equation*}

Then, by looking at the coefficient of \(t^n\), we get

\begin{equation*}
A_n(a,x)
= \sum \frac{a^{\sum_{i=1}^n k_i} n!}{\prod_{i=1}^n k_i!} \prod_{i=1}^n \left(\frac{g_i(x)}{i!} \right)^{k_i}
= \sum \frac{a^k n!}{k_1!\ k_2! \ \cdots k_n!}
   \left(\frac{g_1(x)}{1!} \right)^{k_1}
   \left(\frac{g_2(x)}{2!} \right)^{k_2}
   \cdots
   \left(\frac{g_n(x)}{n!} \right)^{k_n}
\end{equation*}

where the summation goes over \(k_1 + 2k_2 + \cdots nk_n = n\) and \(k = k_1 + k_2 + \cdots k_n\). We then
find

\begin{equation*}
D^n[e^{ag(x)}] = e^{a(g(x)}A_n(a,x)
= \sum \frac{a^ke^{ag(x)} n!}{k_1!\ k_2! \ \cdots k_n!}
   \left(\frac{g_1(x)}{1!} \right)^{k_1}
   \left(\frac{g_2(x)}{2!} \right)^{k_2}
   \cdots
   \left(\frac{g_n(x)}{n!} \right)^{k_n}
\end{equation*}

Therefore, since we know that the \(a^k\) comes from \(f^{(k)}(u)\), we can rewrite it to get that

\begin{equation*}
D^n[f\circ g(x)]
= \sum_{k_1 + 2k_2 + \cdots nk_n = n} \frac{f_k(u) n!}{k_1!\ k_2! \ \cdots k_n!}
   \left(\frac{g_1(x)}{1!} \right)^{k_1}
   \left(\frac{g_2(x)}{2!} \right)^{k_2}
   \cdots
   \left(\frac{g_n(x)}{n!} \right)^{k_n}
\end{equation*}

This is known as
\href{https://en.wikipedia.org/wiki/Fa\%C3\%A0\_di\_Bruno's\_formula}{Faà di Bruno's formula}
\end{document}
