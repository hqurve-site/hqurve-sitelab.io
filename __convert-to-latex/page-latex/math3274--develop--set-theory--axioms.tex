\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Zermelo–Fraenkel Axioms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\ran}{ran}
\DeclareMathOperator{\Card}{Card}
\def\defeq{=_{\mathrm{def}}}

% Title omitted
\begin{admonition-important}[{}]
I asked Daaga if the use of the word ``axiom'' was being used here was the same as in
group theory or vector spaces and further more, and if it was, would we be exploring
set-like objects. He said no, ``axioms'' here are being used as truths and that he
the use of the word ``properties'' in group theory.

I strongly dislike this sentiment and much prefer the use of axioms as properties.
This way, we are not asserting anything but only exploring what would happen if something
like this did exist. Furthermore, it gives more merit to if we want the axiom of
choice to be always true or always false as these would be two different systems rather
than ammendments to our ``current system''.

Even further, it allows us to see what other set-like
objects exist (eg predicates or lists).
Although it is a bit circular, it is good for investigation purposes.

As a result, this section would be presented using axioms as properties rather than
absolute truths. (Hopefully there are no bad side-effects; there shouldn't be since
its almost the same thing. Plus Moschovakis does something similar... right?). Beware! \emoji{👻}
\end{admonition-important}

Let \(\mathcal{W}\) be a container with the following operations
Equality:: if \(a = b\), we say that \(a\) \emph{is equal to} \(b\)
Set:: A predicate which states whether an object in \(\mathcal{W}\) is a \emph{set}.
Belonging:: If \(A\) is a set, then we say that an object \(x\) \emph{belongs} to \(A\) iff \(x \in A\).

Additionally, we define the following terminology

\begin{description}
\item[Subset] For any two sets, \(A\) and \(B\), we say that \(B\) is a \emph{subset} of \(A\) iff \(\forall x: x \in B \implies x \in A\)
    and we denote \(B \subseteq A\).
\item[Collection] This is a synonym of ``set'' which we use if we do not want
    to repeat the word ``set'' too much (Daaga 2022).
\end{description}

With these three operations, we now have the following axioms

\section{Axiom of Extension}
\label{develop--math3274:set-theory:page--axioms.adoc---axiom-of-extension}

For any two sets \(A\) and \(B\),

\begin{equation*}
A = B \iff \forall x: [ x \in A \iff x \in B]
\end{equation*}

That is a set is determined by its members.

\begin{admonition-remark}[{}]
Notice the use of the word ``determined'' and not ``defined''
\end{admonition-remark}

\section{Axiom of Specification}
\label{develop--math3274:set-theory:page--axioms.adoc---axiom-of-specification}

For each set \(A\) and definite unary condition \(P\), there exits a set \(B\)
which contains exactly the members of \(A\) which satisfy \(P\). That is,

\begin{equation*}
\forall x: x \in B \iff [x \in A\text{ and } P(x)]
\end{equation*}

Additionally, by the axiom of specification, such a \(B\) is unique and we write

\begin{equation*}
B = \{x \in A: P(x)\}
\end{equation*}

\subsection{Russell's operation}
\label{develop--math3274:set-theory:page--axioms.adoc---russells-operation}

Let \(A\) be a set. Then we define \(r(A)\) to be the set guaranteed by applying
the axiom of specification to the predicate \(P(x) \iff x \notin x\). That is,

\begin{equation*}
r(A) = \{x \in A: x \notin x\}
\end{equation*}

Then notice that \(r(A) \notin A\) otherwise it would lead to a contradiction. We have therefore
found that for each set \(A\), there exists a set (in particular, a subset) which does not belong to it.

\section{Axiom of pairs and the existence of a set}
\label{develop--math3274:set-theory:page--axioms.adoc---axiom-of-pairs-and-the-existence-of-a-set}

Firstly, there exists an object which is a set. Furthermore, for any two sets \(A\) and \(B\),
there exists a set \(C\) which contains both. We commonly denote

\begin{equation*}
C = \{A, B\}
\end{equation*}

Additionally, \(C\) is uniquely determined by the axiom of extension.

In the case where \(A = B\), we call \(C\) a \emph{singleton} and write \(C = \{A\}\).

\begin{admonition-important}[{}]
Note that \(C\) is an \emph{unordered pair}.
\end{admonition-important}

\subsection{The empty set}
\label{develop--math3274:set-theory:page--axioms.adoc---the-empty-set}

Let \(A\) be a set (its existence is granted by the previous axiom). Then, we apply the axiom
of specification to a statement which is false (or at least false for all elements in \(A\)).
For instance, let \(P(x) \iff x \neq x\). Then, by the axiom of specification, there exists
a set which containing all the elements of \(A\) which satisfy \(P\). Then, this set can
contain no elements and hence we call it the \emph{empty set} and denote

\begin{equation*}
\varnothing = \{x \in A: x \neq x\}
\end{equation*}

\begin{admonition-caution}[{}]
Instead of stating that a set exists, some authors prefer to state that the empty set,
in particular exists. Both statements are equivalent.
\end{admonition-caution}

\section{Axiom of unions}
\label{develop--math3274:set-theory:page--axioms.adoc---axiom-of-unions}

Let \(\mathcal{C}\) be a collection. Then there exists a set \(B\) which
contains exactly the members of the members of \(\mathcal{C}\). That is,

\begin{equation*}
x \in B \iff [\exists X \in \mathcal{C}: x \in X]
\end{equation*}

Additionally, we sometimes use the notation

\begin{equation*}
B = \bigcup_{X \in \mathcal{C}} X = \bigcup \mathcal{C}
\end{equation*}

and by the axiom of extension, this set is uniquely determined.

\begin{admonition-note}[{}]
Although it isn't explicitly stated here \(\mathcal{C}\) can also contain elements
which are not sets. However, since they have not elements, their presence is irrelevant.
\end{admonition-note}

\section{Axiom of the powersets}
\label{develop--math3274:set-theory:page--axioms.adoc---axiom-of-the-powersets}

Let \(A\) be a set. Then, there exists a set \(\mathcal{P}(A)\) whose members are
the subsets of \(A\). That is,

\begin{equation*}
X \in \mathcal{P}(A) \iff X \subseteq A
\end{equation*}

\section{Axiom of Infinity}
\label{develop--math3274:set-theory:page--axioms.adoc---axiom-of-infinity}

There exists a set \(I\) which contains the empty set and the singleton of each of its members.
That is

\begin{equation*}
x \in I \iff [x = \varnothing \text{ or } \exists y \in I: x = y \cup \{y\}]
\end{equation*}

In a less formal manner

\begin{equation*}
I = \{\varnothing, \{\varnothing\}, \{\varnothing, \{\varnothing\}\},\ldots \}
\end{equation*}

\begin{admonition-note}[{}]
The previous axioms do not make the axiom of infinity redundant as, on their own,
they can only guarantee finite sets.
\end{admonition-note}
\end{document}
