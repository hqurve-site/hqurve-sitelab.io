\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Graph path algorithms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\underline{#1}}
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
In this section, we are primarily interested in finding the minimum
path length between two nodes in a graph with weighted edges.

\section{Directed Acyclic Graphs (DAG)}
\label{develop--math6194:ROOT:page--graph-paths.adoc---directed-acyclic-graphs-dag}

If your graph is directed and acyclic, we can do a lot.
Most algorithms are performed in an order such that a node
is only visited/finalized if all of its processors have been visited/finalized.
Such an ordering is called a \textbf{topological ordering} and can be applied to many other contexts.

\subsection{Layered Approach}
\label{develop--math6194:ROOT:page--graph-paths.adoc---layered-approach}

\begin{admonition-caution}[{}]
This approach only works from some DAGs
\end{admonition-caution}

\begin{admonition-tip}[{}]
Dr. De'Matas refers to this as the dynamic programming approach.
\end{admonition-tip}

Suppose our DAG is such that we can form cluster nodes into layers, where the nodes
of one layer are only directly reachable from nodes of the previous layer.
This is possible iff the number of edges from the start node to another node is well defined (the same irrespective of the chosen path).
Then, we can determine the minimum/maximum path length to each node, by processing only layer at a time.

\begin{example}[{Finding the minimum distance}]
\begin{figure}[H]\centering
\includegraphics[width=0.9\linewidth]{images/develop-math6194/ROOT/layered-dag}
\end{figure}

In the below table, we store the possible minimum distances to each node
as well as the corresponding immediate predecessor (for backtracking).
We see that the shortest path length from s to t is 21 with path
\(s \to a \to d \to h \to k \to t\).

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Stage} & {Node} & {Possible distances} & {Minimum distance} \\
\hline[\thicktableline]
{Stage 0} & {s} & {-} & {0} \\
\hline
\SetCell[r=3, c=1]{}{Stage 1} & {a} & {2s} & {2s} \\
\hline
 & {b} & {4s} & {4s} \\
\hline
 & {c} & {3c} & {3c} \\
\hline
\SetCell[r=3, c=1]{}{Stage 2} & {d} & {6a, 9b} & {6a} \\
\hline
 & {e} & {5b} & {5b} \\
\hline
 & {f} & {10b, 8c} & {8c} \\
\hline
\SetCell[r=3, c=1]{}{Stage 3} & {g} & {13d} & {13d} \\
\hline
 & {h} & {11d, 12e} & {11d} \\
\hline
 & {i} & {13e, 14f} & {13e} \\
\hline
\SetCell[r=3, c=1]{}{Stage 4} & {j} & {19g} & {19g} \\
\hline
 & {k} & {15g, 12h, 18i} & {12h} \\
\hline
 & {l} & {17i} & {17i} \\
\hline
{Stage 5} & {t} & {23j, 21k, 22l} & {21k} \\
\hline[\thicktableline]
\end{tblr}
\end{table}
\end{example}

\subsubsection{Breath first search (BFS)}
\label{develop--math6194:ROOT:page--graph-paths.adoc---breath-first-search-bfs}

The layered approach can equivalently be implemented using a BFS.
A BFS is typically implemented using a queue (first-in, first-out)
with the following algorithm

\begin{enumerate}[label=\arabic*)]
\item Add the initial node to the queue
\item If the queue is empty, stop.
\item Otherwise, get the next node from the queue.
\item Finalize the minimum distance to this node using the temporary distances.
    This is zero if it is the initial node.
\item Update the temporary distance to each of its neighbours
\item Add each of the neighbours to the queue (if they are not already present)
\item Go to step 2
\end{enumerate}

\begin{example}[{Finding the minimum distance}]
\begin{figure}[H]\centering
\includegraphics[width=0.9\linewidth]{images/develop-math6194/ROOT/dag}
\end{figure}

In the below table, we store the temporary minimum distances to each node
as well as the corresponding immediate predecessor (for backtracking).
We see that the shortest path length from s to t is 21 with path
\(s \to a \to d \to h \to k \to t\).

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Node} & {Temp distances and previous nodes} & {Final distance} \\
\hline[\thicktableline]
{s} & {-} & {0} \\
\hline
{a} & {2s} & {2s} \\
\hline
{b} & {4s} & {4s} \\
\hline
{c} & {3c} & {3c} \\
\hline
{d} & {\strikethrough{6a} 9b} & {6a} \\
\hline
{e} & {5b} & {5b} \\
\hline
{f} & {\strikethrough{10b}, 8c} & {8c} \\
\hline
{g} & {13d} & {13d} \\
\hline
{h} & {11d \strikethrough{12e}} & {11d} \\
\hline
{i} & {13e \strikethrough{14f}} & {13e} \\
\hline
{j} & {19g} & {19g} \\
\hline
{k} & {\strikethrough{15g} 12h \strikethrough{18i}} & {12h} \\
\hline
{l} & {17i} & {17i} \\
\hline
{t} & {\strikethrough{23j} 21k \strikethrough{22l}} & {21k} \\
\hline[\thicktableline]
\end{tblr}
\end{table}
\end{example}

\subsection{Topological ordering}
\label{develop--math6194:ROOT:page--graph-paths.adoc---topological-ordering}

Unlike the layered and BFS approaches, this algorithm works for all DAGs.
We finalize nodes iff all of its processors have been finalized.

\begin{example}[{}]
\begin{figure}[H]\centering
\includegraphics[width=0.7\linewidth]{images/develop-math6194/ROOT/unlayered-dag}
\end{figure}

In the below table, we compute the minimum distance only after all preceding vertices have been visited.
We see that there are two shortest paths from s to t, both with minimum distance 13

\begin{equation*}
\begin{array}{c}
s \to a \to d\to t\\
s \to a\to c \to t
\end{array}
\end{equation*}

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Node} & {Preceeding nodes} & {Min distance} \\
\hline[\thicktableline]
{s} & {-} & {0} \\
\hline
{a} & {s} & {4s} \\
\hline
{b} & {s} & {3s} \\
\hline
{c} & {a,b} & {min(8a,9b) = 8a} \\
\hline
{d} & {a,c} & {min(11a,13c) = 11a} \\
\hline
{e} & {b,c} & {min(11c,11b) = 11b,c} \\
\hline
{t} & {c,d,e} & {min(13d, 13c,14e) = 13c,d} \\
\hline[\thicktableline]
\end{tblr}
\end{table}
\end{example}

\section{Dijkstra's Algorithm}
\label{develop--math6194:ROOT:page--graph-paths.adoc---dijkstras-algorithm}

Dijkstra's algorithm is a popular path finding algorithm which works with both directed
and undirected graphs.
However, it has two restrictions

\begin{itemize}
\item It can only be used to find the minimum distance
\item The path lengths must be non-negative
\end{itemize}

The algorithm is conducted as followed

\begin{enumerate}[label=\arabic*)]
\item Set the temporary distance to the start node as 0
\item Select the node with the smallest temporary distance
\item Finalize the chosen node
\item Update the temporary distances of (unfinalized) neighbours of the chosen node
\item If the end node was finalized, stop; otherwise go to step 2
\end{enumerate}

To see why this algorithm works,
we can restate the algorithm such that in each iteration, we finalize all nodes with temporary path length \(L\).
We only increase \(L\) once there are no nodes with temporary path length \(L\) remaining.
Then, the following invariants hold at the beginning of each iteration

\begin{itemize}
\item The distances of finalized nodes are minimal
\item The temporary distances of unfinalized nodes are greater than or equal to \(L\)
\end{itemize}

Note that these invariants are preserved since any newly finalized node could
not have been reached quicker.
Otherwise, its predecessor would have been finalized in an earlier iteration (since edges weights are non-negative)
and its temporary path length would be smaller.

\begin{example}[{Minimum distance for undirected graph}]
\begin{figure}[H]\centering
\includegraphics[width=0.6\linewidth]{images/develop-math6194/ROOT/undirected-graph}
\end{figure}

We see that the shortest path is \(s\to a \to d \to t\) which
has path length 10.

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[l,h]|Q[l,h]|Q[l,h]|Q[l,h]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{Index of finalization} & {Node} & {Temp distance} & {Final distance} \\
\hline[\thicktableline]
{1} & {s} & {0} & {0} \\
\hline
{2} & {a} & {3s} & {3s} \\
\hline
{4} & {b} & {9s, 8a, 9c} & {8a} \\
\hline
{3} & {c} & {7s} & {7s} \\
\hline
{5} & {d} & {9a, 12b} & {9a} \\
\hline
{6} & {t} & {11a, 12c, 10d} & {10d} \\
\hline[\thicktableline]
\end{tblr}
\end{table}
\end{example}
\end{document}
