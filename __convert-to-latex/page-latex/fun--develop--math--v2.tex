\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{The distance between two lines}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\abrack#1{\left\langle{#1}\right\rangle}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
% complex conjugate
\def\conj#1{\overbar{#1}}

% Title omitted
What if we wanted to determine the distance between two lines?
That is, the shortest distance between two lines.
One popular way (in three dimensions) is to use geometrical arguments to say that the shortest segment
connecting two points would be perpendicular to both lines and then use the three dimensional cross product to compute
this distance. This is nice, however, we can generalize this question to an arbitrary hermatian inner product
space.

Consider two lines \(\vec{a}_1 + t_1\vec{b}_1\) and \(\vec{a}_2 + t_2\vec{b}_2\) in vector space
\((\mathbb{C}, V)\). Then, what is the distance between these two lines? That is,
we seek to minimize

\begin{equation*}
\abrack{f(t_1, t_2), f(t_1, t_2)}
\quad\text{where}\quad
f(t_1, t_2) = (\vec{a}_1 + t_1\vec{b}_1) - (\vec{a}_2 + t_2\vec{b}_2)
\end{equation*}

\begin{admonition-important}[{}]
We are using the definition of hermatian inner product as defined in \myautoref[{Math3273 Inner product space}]{develop--math3273:inner-product-space:page--index.adoc}.
That is, the product is linear in the second argument.
\end{admonition-important}

\begin{admonition-important}[{}]
Throughout this page, I would call the \(\abrack{\vec{x} - \vec{y}, \vec{x} - \vec{y}}\), the
distance between \(\vec{x}\) and \(\vec{y}\) although it is really the square of the distance.
I don't feel like constantly having to place square roots or clarify. For the main part,
we would never discuss the actual distance in the page since it can be obtained by
using the square root.
\end{admonition-important}

\begin{example}[{Tedious expansion}]
Firstly, lets expand this expression.

\begin{equation*}
\begin{aligned}[t]
&\abrack{f(t_1, t_2), f(t_1, t_2)}
\\&= \abrack{
    (\vec{a}_1 + t_1\vec{b}_1) - (\vec{a}_2 + t_2\vec{b}_2),
    (\vec{a}_1 + t_1\vec{b}_1) - (\vec{a}_2 + t_2\vec{b}_2)
}
\\&= \abrack{
    t_1\vec{b}_1 - t_2\vec{b}_2 + (\vec{a}_1 - \vec{a}_2),
    t_1\vec{b}_1 - t_2\vec{b}_2 + (\vec{a}_1 - \vec{a}_2),
}
\\&=
    \abrack{t_1\vec{b}_1, t_1\vec{b}_1}
    + \abrack{t_1\vec{b}_1, -t_2\vec{b}_2}
    + \abrack{t_1\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}
\\&\quad
    + \abrack{-t_2\vec{b}_2, t_1\vec{b}_1}
    + \abrack{-t_2\vec{b}_2, -t_2\vec{b}_2}
    + \abrack{-t_2\vec{b}_2, (\vec{a}_1 - \vec{a}_2)}
\\&\quad
    + \abrack{(\vec{a}_1 - \vec{a}_2), t_1\vec{b}_1}
    + \abrack{(\vec{a}_1 - \vec{a}_2), -t_2\vec{b}_2}
    + \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
\\&=
    \conj{t_1}t_1\abrack{\vec{b}_1, \vec{b}_1}
    - \conj{t_1}t_2\abrack{\vec{b}_1, \vec{b}_2}
    + \conj{t_1}\abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}
\\&\quad
    - \conj{t_2}t_1\abrack{\vec{b}_2, \vec{b}_1}
    + \conj{t_2}t_2\abrack{\vec{b}_2, \vec{b}_2}
    - \conj{t_2}\abrack{\vec{b}_2, (\vec{a}_1 - \vec{a}_2)}
\\&\quad
    + t_1\abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_1}
    - t_2 \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_2}
    + \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
\\&= \begin{bmatrix}
        \conj{t_1} & -\conj{t_2} & 1
    \end{bmatrix}
    \begin{bmatrix}
        t_1\abrack{\vec{b_1}, \vec{b_1}} - t_2\abrack{\vec{b}_1, \vec{b}_2} + \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)} \\
        t_1\abrack{\vec{b}_2, \vec{b}_1} - t_2\abrack{\vec{b}_2, \vec{b}_2} + \abrack{\vec{b}_2, (\vec{a}_1 - \vec{a}_2)} \\
        t_1\abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_1} - t_2 \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_2} + \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
    \end{bmatrix}
\\&= \begin{bmatrix}
        \conj{t_1} & -\conj{t_2} & 1
    \end{bmatrix}
    \begin{bmatrix}
        \abrack{\vec{b}_1, \vec{b}_1} & \abrack{\vec{b}_1, \vec{b}_2} & \abrack{\vec{b}_1, (\vec{a}_1 -\vec{a}_2)} \\
        \abrack{\vec{b}_2, \vec{b}_1} & \abrack{\vec{b}_2, \vec{b}_2} & \abrack{\vec{b}_2, (\vec{a}_1 -\vec{a}_2)} \\
        \abrack{(\vec{a}_1 -\vec{a}_2), \vec{b}_1} & \abrack{(\vec{a}_1 -\vec{a}_2), \vec{b}_2} & \abrack{(\vec{a}_1 -\vec{a}_2), (\vec{a}_1 -\vec{a}_2)} \\
    \end{bmatrix}
    \begin{bmatrix}
        t_1 \\ -t_2 \\ 1
    \end{bmatrix}
\end{aligned}
\end{equation*}

This is a \href{https://en.wikipedia.org/wiki/Gram\_matrix}{Gram matrix} which is clearly Hermatian.
\end{example}

If we expand this, we get

\begin{equation*}
\abrack{f(t_1, t_2), f(t_1, t_2)}
= \vec{t}^* M \vec{t}
\end{equation*}

where

\begin{equation*}
    \vec{t} = \begin{bmatrix}
        t_1 \\ -t_2 \\ 1
    \end{bmatrix}
    \quad\text{and}\quad
    M = \begin{bmatrix}
        \abrack{\vec{b}_1, \vec{b}_1} & \abrack{\vec{b}_1, \vec{b}_2} & \abrack{\vec{b}_1, (\vec{a}_1 -\vec{a}_2)} \\
        \abrack{\vec{b}_2, \vec{b}_1} & \abrack{\vec{b}_2, \vec{b}_2} & \abrack{\vec{b}_2, (\vec{a}_1 -\vec{a}_2)} \\
        \abrack{(\vec{a}_1 -\vec{a}_2), \vec{b}_1} & \abrack{(\vec{a}_1 -\vec{a}_2), \vec{b}_2} & \abrack{(\vec{a}_1 -\vec{a}_2), (\vec{a}_1 -\vec{a}_2)} \\
    \end{bmatrix}
\end{equation*}

\section{Location of minimum}
\label{develop--fun:math:page--v2.adoc---location-of-minimum}

Now, suppose we already have a value \(\vec{t}_m\), such that \(\vec{t}^*M\vec{t}\) is minimized.
Then, for all \(\vec{x} = (x_0, x_1, 0)^T\) and \(\lambda \in \mathbb{C}\),

\begin{equation*}
\vec{t}^*M\vec{t}_m \leq (\vec{t}_m + \lambda\vec{x})^*M(\vec{t}_m + \lambda\vec{x})
\end{equation*}

\begin{admonition-note}[{}]
We choose the final component of \(\vec{x}\) to be zero since \(\vec{t}_m + \lambda\vec{x}\) represents
another possible value of \(\vec{t}\). Also, I first saw this trick used by user
"Did" on a math stack exchange post discussing the
\href{https://math.stackexchange.com/questions/280178/approximate-a-function-over-the-interval-0-1-by-a-polynomial-of-degree-n}{best polynomial polynomial approximation in accordance to the \(\mathcal{L}^2\) norm}
\end{admonition-note}

Which when rearranged yields

\begin{equation*}
0 \leq \vec{x}^*M\vec{x}\left|\lambda + \frac{\vec{x}^*M\vec{t}_m}{\vec{x}^*M\vec{x}}\right|^2
    - \frac{\left|\vec{x}^*M\vec{t}_m\right|^2}{\vec{x}^*M\vec{x}}
\end{equation*}

\begin{example}[{Rearrangement steps}]
\begin{equation*}
\begin{aligned}[t]
    & \vec{t}^*M\vec{t}_m \leq (\vec{t}_m + \lambda\vec{x})^*M(\vec{t}_m + \lambda\vec{x})
        = \vec{t}_m^*M\vec{t}_m + \lambda\vec{t}_m^*M\vec{x} + \conj{\lambda}\vec{x}^*M\vec{t}_m + \conj{\lambda}\lambda\vec{x}^*M\vec{x}
    \\&\iff 0 \leq \lambda\vec{t}_m^*M\vec{x} + \conj{\lambda}\vec{x}^*M\vec{t}_m + \conj{\lambda}\lambda\vec{x}^*M\vec{x}
\end{aligned}
\end{equation*}

Notice that since the terms in the above expression are complex numbers (1x1 matrices),

\begin{equation*}
\vec{t}_m^*M\vec{x} = (\vec{t}_m^*M\vec{x})^T = \left(\left(\vec{x}^*M\vec{t}_m\right)^*\right)^T = \conj{\vec{x}^*M\vec{t}_m}
\end{equation*}

Therefore, we obtain that

\begin{equation*}
\begin{aligned}[t]
0 &\leq \lambda\conj{\vec{x}^*M\vec{t}_m} + \conj{\lambda}\vec{x}^*M\vec{t}_m + \conj{\lambda}\lambda\vec{x}^*M\vec{x}
\\&= \vec{x}^*M\vec{x}\left(\lambda + \frac{\vec{x}^*M\vec{t}_m}{\vec{x}^*M\vec{x}}\right)\left(\conj{\lambda} + \frac{\conj{\vec{x}^*M\vec{t}_m}}{\vec{x}^*M\vec{x}}\right)
    - \frac{\vec{x}^*M\vec{t}_m \ \conj{\vec{x}^*M\vec{t}_m} }{\vec{x}^*M\vec{x}}
\end{aligned}
\end{equation*}
\end{example}

There is no need to consider the case when \(\vec{x} = 0\) since the inequality trivially holds. However, is
\(\vec{x}^*M\vec{x} = 0\) otherwise? Since the final component of \(x\) is zero, we only
need to consider the leading 2x2 block of \(M\), whose determinant is

\begin{equation*}
\begin{aligned}[t]
|M_{2\times 2}|
&= \abrack{\vec{b}_1, \vec{b}_1}\abrack{\vec{b}_2, \vec{b}_2} - \abrack{\vec{b}_1, \vec{b}_2}\abrack{\vec{b}_2, \vec{b}_1}
\\&= \abrack{\vec{b}_1, \vec{b}_1}\abrack{\vec{b}_2, \vec{b}_2} - \abrack{\vec{b}_1, \vec{b}_2}\conj{\abrack{\vec{b}_1, \vec{b}_2}}
\\&= \abrack{\vec{b}_1, \vec{b}_1}\abrack{\vec{b}_2, \vec{b}_2} - \left|\abrack{\vec{b}_1, \vec{b}_2}\right|^2
\end{aligned}
\end{equation*}

Now, by applying the Cauchy-Schwartz inequality, we get that \(|M_{2\times 2}|\) is greater than or equal
to zero with equality holding when \(\vec{b}_1\) and \(\vec{b}_2\) are linearly dependent.
We would ignore this case for now. Therefore, \(M_{2\times 2}\) is invertible and hence
\(\vec{x}^*M\vec{x} \neq 0\). Therefore, we obtain that

\begin{equation*}
0 \leq \left|\lambda + \frac{\vec{x}^*M\vec{t}_m}{\vec{x}^*M\vec{x}}\right|^2
    - \left|\frac{\vec{x}^*M\vec{t}_m}{\vec{x}^*M\vec{x}}\right|^2
\end{equation*}

Now, we can freely manipulate \(\lambda\) such that the first term becomes zero. Then, for the inequality to
hold we want that \(\vec{x}^*M\vec{t}_m = 0\). Furthermore, this must be true for all \(\vec{x}\)
otherwise, \(\vec{t}_m\) would not be at a minimum. Since the final coefficient of \(\vec{x}\)
is zero, we therefore only require that \(M_{2\times 3}\vec{t}_m = \vec{0}_2\). As we already established
\(M_{2\times 2}\) is invertible and hence there always exist a unique \(\vec{t}_m\).

\section{Computing the distance}
\label{develop--fun:math:page--v2.adoc---computing-the-distance}

Finally, let us compute the distance. Let

\begin{equation*}
a = \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
\quad\text{and}\quad
\vec{p} = \begin{bmatrix}
\abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}\\
\abrack{\vec{b}_2, (\vec{a}_1 - \vec{a}_2)}\\
\end{bmatrix}
\end{equation*}

Then,

\begin{equation*}
M = \begin{bmatrix}
M_{2\times 2} & \vec{p}\\
\vec{p}^* & a
\end{bmatrix}
\quad\text{and }\quad
\vec{t}_m = \begin{bmatrix}
- M_{2\times 2}^{-1}\vec{p} \\ 1
\end{bmatrix}
\end{equation*}

Therefore

\begin{equation*}
\begin{aligned}[t]
\vec{t}_m^*M\vec{t}_m
&= \vec{t}_m^* \begin{bmatrix}
    M_{2\times 3} \vec{t}_m \\
    \begin{bmatrix}\vec{p}^* & a\end{bmatrix} \vec{t}_m \\
\end{bmatrix}
= \vec{t}_m^* \begin{bmatrix}
    \vec{0}_2 \\
    \begin{bmatrix}\vec{p}^* & a\end{bmatrix} \vec{t}_m \\
\end{bmatrix}
= \begin{bmatrix}\vec{p}^* & a\end{bmatrix} \vec{t}_m
\\&= \begin{bmatrix}\vec{p}^* & a\end{bmatrix} \begin{bmatrix}-M_{2\times 2}^{-1}\vec{p} \\ 1\end{bmatrix}
\\&= -\vec{p}^*M_{2\times 2}^{-1}\vec{p} + a
\end{aligned}
\end{equation*}

At this point, we can already guess that this value is the ratio of two determinants, which from its beauty alone
implies that our calculations are correct. Still, we must calculate the expansion.

\begin{example}[{Expansion}]
To avoid the clutter, we would multiply be \(|M_{2\times 2}|\) for now

\begin{equation*}
\begin{aligned}[t]
&|M_{2\times 2}|(\vec{p}^*M_{2\times 2}^{-1}\vec{p} + a)
\\&=  \begin{bmatrix}
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_1} &
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_2}
    \end{bmatrix}
    \begin{bmatrix}
        \abrack{\vec{b}_1, \vec{b}_1} & -\abrack{\vec{b}_1, \vec{b}_2} \\
        -\abrack{\vec{b}_2, \vec{b}_1} & \abrack{\vec{b}_2, \vec{b}_2} \\
    \end{bmatrix}
    \begin{bmatrix}
        \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)} \\
        \abrack{\vec{b}_2, (\vec{a}_1 - \vec{a}_2)}
    \end{bmatrix}
    + a |M_{2\times 2}|
\\&=  \begin{bmatrix}
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_1} &
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_2}
    \end{bmatrix}
    \begin{bmatrix}
        \abrack{\vec{b}_1, \vec{b}_1}\abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)} - \abrack{\vec{b}_1, \vec{b}_2} \abrack{\vec{b}_2, (\vec{a}_1 - \vec{a}_2)}\\
        -\abrack{\vec{b}_2, \vec{b}_1}\abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)} + \abrack{\vec{b}_2, \vec{b}_2}\abrack{\vec{b}_2, (\vec{a}_1 - \vec{a}_2)} \\
    \end{bmatrix}
    + a |M_{2\times 2}|
\\&=  \begin{bmatrix}
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_1} &
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_2}
    \end{bmatrix}
    \begin{bmatrix}
        \begin{vmatrix}
            \abrack{\vec{b}_1, \vec{b}_1} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}\\
            \abrack{\vec{b}_1, \vec{b}_2} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}
        \end{vmatrix}
        \\
        -\begin{vmatrix}
            \abrack{\vec{b}_2, \vec{b}_1} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}\\
            \abrack{\vec{b}_2, \vec{b}_2} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}
        \end{vmatrix}
    \end{bmatrix}
    + \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)} |M_{2\times 2}|
\\&=  \begin{bmatrix}
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_1} &
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}_2} &
        \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
    \end{bmatrix}
    \begin{bmatrix}
        \begin{vmatrix}
            \abrack{\vec{b}_1, \vec{b}_1} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}\\
            \abrack{\vec{b}_1, \vec{b}_2} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}
        \end{vmatrix}
        \\
        -\begin{vmatrix}
            \abrack{\vec{b}_2, \vec{b}_1} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}\\
            \abrack{\vec{b}_2, \vec{b}_2} & \abrack{\vec{b}_1, (\vec{a}_1 - \vec{a}_2)}
        \end{vmatrix}
        \\
        |M_{2\times 2}|
    \end{bmatrix}
\\&= |M|
\end{aligned}
\end{equation*}
\end{example}

Yes, the guess was correct, we have that

\begin{equation*}
\vec{t}_m^*M\vec{t}_m = \frac{|M|}{|M_{2\times 2}|}
\end{equation*}

There is no need to double check as this result is simply too beautiful.
Additionally, based off the determinants of \(|M|\) and \(|M_{2\times 2}|\)
we can hypothesize the following

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetCell[r=2, c=2]{}{} &  & \SetCell[r=1, c=2]{}{\(M\) is invertible?} &  \\
\hline
 &  & {\emoji{👍}} & {\emoji{👎}} \\
\hline
\SetCell[r=2, c=1]{}{\(M_{2\times 2}\) is invertible?} & {\emoji{👍}} & {skew} & {intersect at a single point} \\
\hline
 & {\emoji{👎}} & {parallel but distinct} & {identical} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

Note that we only proved the first row. However, it would be a pretty nice result.

\section{Parallel lines}
\label{develop--fun:math:page--v2.adoc---parallel-lines}

What is the distance of parallel lines? Let \(\vec{b}_1 = \beta_1\vec{b}\)
and \(\vec{b}_2 = \beta_2\vec{b}\). Then, \(M\) reduces to

\begin{equation*}
\begin{bmatrix}
\conj{\beta_1}\beta_1\abrack{\vec{b}, \vec{b}} & \conj{\beta_1}\beta_2\abrack{\vec{b}, \vec{b}} & \conj{\beta_1}\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\\
\conj{\beta_2}\beta_1\abrack{\vec{b}, \vec{b}} & \conj{\beta_2}\beta_2\abrack{\vec{b}, \vec{b}} & \conj{\beta_2}\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\\
\beta_1\abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}} & \beta_2\abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}} & \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}\\
\end{bmatrix}
\end{equation*}

\begin{admonition-remark}[{}]
From this matrix, we see that the final row of our column was incorrect as \(M_{2\times 2}\) is non invertible
implies that \(M\) is non-invertible. We could have actually seen this directly since its not hard
to show that for any invertible matrix, any square (possibly sparse) block within is also invertible.
For example, take a vector with zeros in the columns of non-interest. If the product of the matrix
and vector is zero, it must imply that the entire vector is zero, in particular the entries in the columns
of interest are zero.
\end{admonition-remark}

Furthermore, we can simplify \(\vec{t}^*M\vec{t}\) as follows

\begin{equation*}
\begin{aligned}[t]
\vec{t}^*M\vec{t} =
\begin{bmatrix}
\beta_1t_1 - \beta_2t_2 \\ 1
\end{bmatrix}^*
\begin{bmatrix}
\abrack{\vec{b}, \vec{b}} & \abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\\
\abrack{(\vec{a}_1- \vec{a}_2), \vec{b}} & \abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}\\
\end{bmatrix}
\begin{bmatrix}
\beta_1t_1 -\beta_2t_2 \\ 1
\end{bmatrix}
\end{aligned}
\end{equation*}

Now, we can actually minimize this directly by factorizing

\begin{example}[{Steps}]
\begin{equation*}
\begin{aligned}[t]
\vec{t}^*M\vec{t}
&= \conj{(\beta_1 t_1 - \beta_2 t_2)}(\beta_1 t_1 - \beta_2 t_2)\abrack{\vec{b}, \vec{b}}
    + \conj{(\beta_1 t_1 - \beta_2 t_2)}\abrack{\vec{b} - (\vec{a}_1 - \vec{a}_2)}
\\&\quad + (\beta_1 t_1 - \beta_2 t_2)\abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}}
    + \abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
\\&= \conj{(\beta_1 t_1 - \beta_2 t_2)}(\beta_1 t_1 - \beta_2 t_2)\abrack{\vec{b}, \vec{b}}
    + \conj{(\beta_1 t_1 - \beta_2 t_2)}\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}
\\&\quad + (\beta_1 t_1 - \beta_2 t_2)\conj{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}
    + \abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
\\&= \abrack{\vec{b}, \vec{b}}
    \conj{\left((\beta_1 t_1 - \beta_2 t_2) + \frac{\abrack{\vec{b} - (\vec{a}_1, \vec{a}_2)}}{\abrack{\vec{b}, \vec{b}}}\right)}
    \left((\beta_1 t_1 + \beta_2 t_2) - \frac{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}{\abrack{\vec{b}, \vec{b}}}\right)
\\&\quad+ \abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
    - \abrack{\vec{b}, \vec{b}}\left(\frac{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}{\abrack{\vec{b}, \vec{b}}}\right)
    \conj{\left(\frac{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}{\abrack{\vec{b}, \vec{b}}}\right)}
\\&= \abrack{\vec{b}, \vec{b}}
    \left|(\beta_1 t_1 - \beta_2 t_2) + \frac{\abrack{\vec{b} - (\vec{a}_1, \vec{a}_2)}}{\abrack{\vec{b}, \vec{b}}}\right|^2
    + \abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
    - \frac{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\conj{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}}{\abrack{\vec{b}, \vec{b}}}
\end{aligned}
\end{equation*}
\end{example}

\begin{equation*}
\vec{t}^*M\vec{t} = \abrack{\vec{b}, \vec{b}}
    \left|(\beta_1 t_1 - \beta_2 t_2) + \frac{\abrack{\vec{b} - (\vec{a}_1, \vec{a}_2)}}{\abrack{\vec{b}, \vec{b}}}\right|^2
    + \abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}
    - \frac{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\conj{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}}{\abrack{\vec{b}, \vec{b}}}
\end{equation*}

Therefore, the minimum is

\begin{equation*}
\frac{\abrack{(\vec{a}_1- \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}\abrack{\vec{b}, \vec{b}}
    - \abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\conj{\abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}}
}{\abrack{\vec{b}, \vec{b}}}
= \frac{
    \begin{vmatrix}
        \abrack{\vec{b}, \vec{b}} & \abrack{\vec{b}, (\vec{a}_1 - \vec{a}_2)}\\
        \abrack{(\vec{a}_1 - \vec{a}_2), \vec{b}} & \abrack{(\vec{a}_1 - \vec{a}_2), (\vec{a}_1 - \vec{a}_2)}\\
    \end{vmatrix}
}{\abrack{\vec{b}, \vec{b}}}
\end{equation*}

Notice that this result is very similar to the previous result.

\section{Why is this result so beautiful?}
\label{develop--fun:math:page--v2.adoc---why-is-this-result-so-beautiful}

The beauty of this result implies that there is a much simpler way of determining it.
In the case of \(\mathbb{R}^n\), there is a simple derivation.
Indeed, let us focus on the case where the two lines are parallel.
Then consider the following diagram

\begin{figure}[H]\centering
\includegraphics[width=0.8\linewidth]{images/develop-fun/math/v2-parallel}
\end{figure}

Then, the distance between the two lines is equal to the parallelogram's area divided by the length
of \(\vec{b}\). However, for simplicity, we would instead compute the square of the
area and square of the length of \(\vec{b}\). The question remains how
to compute the parallelogram's area.

Recall that the determinant of a matrix formed by \(n\) \(\mathbb{R}^n\) vectors
is the signed n-dimensional volume of the parallelepiped with edges formed by these vectors.
However, we only have two vectors and therefore, we cannot immediately obtain the area
of their parallelogram.

To compensate for this, we can construct unit vectors
\(\vec{e}_3, \cdots \vec{e}_n\) which are orthogonal to \(\vec{a}\)
and \(\vec{b}\). Then, if \(N = \begin{bmatrix}\vec{a} & \vec{b} & \vec{e}_3 &\cdots& \vec{e}_n \end{bmatrix}\),
we see that magnitude of \(|N|\) would be equal to the area of the parallelogram
formed by \(\vec{a}\) and \(\vec{b}\) since the orthogonal volume is equal to \(1\).

Now, notice the following

\begin{equation*}
\begin{aligned}[t]
N^TN &=
    \begin{bmatrix}\vec{a} & \vec{b} & \vec{e}_3 &\cdots& \vec{e}_n\end{bmatrix}^T
    \begin{bmatrix}\vec{a} & \vec{b} & \vec{e}_3 &\cdots& \vec{e}_n\end{bmatrix}
\\&= \begin{bmatrix}
    \abrack{\vec{a}, \vec{a}} & \abrack{\vec{a}, \vec{b}} & \abrack{\vec{a}, \vec{e}_3} & \cdots & \abrack{\vec{a}, \vec{e}_n} \\
    \abrack{\vec{b}, \vec{a}} & \abrack{\vec{b}, \vec{b}} & \abrack{\vec{b}, \vec{e}_3} & \cdots & \abrack{\vec{b}, \vec{e}_n} \\
    \abrack{\vec{e}_3, \vec{a}} & \abrack{\vec{e}_3, \vec{b}} & \abrack{\vec{e}_3, \vec{e}_3} & \cdots & \abrack{\vec{e}_3, \vec{e}_n} \\
    \vdots                     &                            &                             & \ddots & \\
    \abrack{\vec{e}_n, \vec{a}} & \abrack{\vec{e}_n, \vec{b}} & \abrack{\vec{e}_n, \vec{e}_3} & \cdots & \abrack{\vec{e}_n, \vec{e}_n} \\
\end{bmatrix}
\\&= \begin{bmatrix}
    \abrack{\vec{a}, \vec{a}} & \abrack{\vec{a}, \vec{b}} & 0 & \cdots & 0 \\
    \abrack{\vec{b}, \vec{a}} & \abrack{\vec{b}, \vec{b}} & 0 & \cdots & 0 \\
    0 & 0 & 1 & \cdots & 0 \\
    \vdots &   &   & \ddots & \\
    0 & 0 & 0 & \cdots & 1 \\
\end{bmatrix}
\\&= \begin{bmatrix}
    \abrack{\vec{a}, \vec{a}} & \abrack{\vec{a}, \vec{b}} &  \\
    \abrack{\vec{b}, \vec{a}} & \abrack{\vec{b}, \vec{b}} &  \\
     &  & I_{n-2} \\
\end{bmatrix}
\end{aligned}
\end{equation*}

Therefore,
\(|N|^2 = \begin{vmatrix}
\abrack{\vec{a}, \vec{a}} & \abrack{\vec{a}, \vec{b}} \\
\abrack{\vec{b}, \vec{a}} & \abrack{\vec{b}, \vec{b}} \\
\end{vmatrix}
\) and hence the square height is as previously discussed.

Additionally notice that this same process can be replicated for the distance between two
lines and in fact with as many vectors as desired. However, we need to be more careful
with our vector spaces in general since they may not be real nor finite dimensional.

\section{General method}
\label{develop--fun:math:page--v2.adoc---general-method}

Hindsight is 2020. Now that we know what to expect, we can prove it in a relatively simple manner.
Firstly, let \(M\) be an positive definite matrix and \(\vec{b}\) be a vector compatible with height equal to the side-length of
\(M\). Then, we wish to minimize the following

\begin{equation*}
f(\vec{t}) =
\begin{bmatrix}\vec{t}^* & 1\end{bmatrix}
\begin{bmatrix}
    M & \vec{b} \\
    \vec{b} & 1
\end{bmatrix}
\begin{bmatrix}\vec{t} \\ 1\end{bmatrix}
\end{equation*}

By expanding and factorizing, we obtain the following

\begin{equation*}
f(\vec{t}) =
\left(\vec{t} + M^{-1}\vec{b}\right)^* M \left(\vec{t} + M^{-1}\vec{b}\right)
+ a - \vec{b}^* M^{-1} \vec{b}
\end{equation*}

Since \(\vec{t}\) is free, we see that the minimum value is simply \(a - \vec{b}^* M^{-1} \vec{b}\)
and it is attained when \(\vec{t} = -M^{-1}\vec{b}\).
However, there is a much simpler version. We want to write the minimum of \(f\) as a determinant.
We believe that it may be \(\frac{1}{|M|}\begin{vmatrix}M & \vec{b} \\ \vec{b}^* & a\end{vmatrix}\).
This might be a bit hard to prove without assistance.

Should we call this cheating? I am not too sure. But, I recall a nice paper by John R. Silvester entitled
\href{https://hal.archives-ouvertes.fr/hal-01509379/document}{Determinants of Block Matrices}.
It is not directly applicable; however, one trick is. Notice the following

\begin{equation*}
\begin{bmatrix}
M & \vec{b} \\
\vec{b}^* & a
\end{bmatrix}
\begin{bmatrix}
I & -M^{-1}\vec{b} \\
\vec{0}^* & 1
\end{bmatrix}
=
\begin{bmatrix}
M & \vec{0}^* \\
\vec{b}^* & a - \vec{b}^*M^{-1}\vec{b}
\end{bmatrix}
\end{equation*}

Therefore, the we obtain that

\begin{equation*}
a - \vec{b}^*M^{-1}\vec{b} = \frac{1}{|M|}\begin{vmatrix}M & \vec{b} \\ \vec{b}^* & a\end{vmatrix}
\end{equation*}

is the minimum value of \(f\) and is obtained when \(\vec{t} = -M^{-1}\vec{b}\).

\subsection{Application to the distance between affine regions}
\label{develop--fun:math:page--v2.adoc---application-to-the-distance-between-affine-regions}

Suppose we have two regions, \(\sum_{i=1}^{n_1} r_i \vec{v}_i + \vec{a}_1\) and \(\sum_{i=1}^{n_2} s_i \vec{u}_i + \vec{a}_2\),
where \(n_1\) and \(n_2\) are finite.
Then the square distance between the two is given by

\begin{equation*}
\abrack{
    \sum_{i=1}^{n_1}r_i\vec{v}_i - \sum_{i=1}^{n_2} s_i\vec{u}_i + (\vec{a}_1 - \vec{a}_2)
,
    \sum_{i=1}^{n_1}r_i\vec{v}_i - \sum_{i=1}^{n_2} s_i\vec{u}_i + (\vec{a}_1 - \vec{a}_2)
}
\end{equation*}

However, to find the minimum distance, we can rewrite this in terms of a independent vectors \(\vec{w}_1, \ldots \vec{w}_n\)
and \(\vec{a}\) for simplicity. Then the minimum distance is when the following is minimized

\begin{equation*}
\begin{aligned}[t]
&\abrack{\sum_{i=1}^n t_i\vec{w}_i + \vec{a}, \sum_{i=1}^n t_i\vec{w}_i + \vec{a}}
\\&= \abrack{\sum_{i=1}^n t_i\vec{w}_i, \sum_{i=1}^n t_i\vec{w}_i}
    + \abrack{\sum_{i=1}^n t_i \vec{w}_i, \vec{a}}
    + \abrack{\vec{a}, \sum_{i=1}^n t_i \vec{w}_i}
    + \abrack{\vec{a}, \vec{a}}
\\&= \sum_{i=1, j=1}^{n,n}\abrack{t_i\vec{w}_i, t_j\vec{w}_j}
    + \sum_{i=1}^n\abrack{t_i \vec{w}_i, \vec{a}}
    + \sum_{i=1}^n\abrack{\vec{a}, t_i \vec{w}_i}
    + \abrack{\vec{a}, \vec{a}}
\\&= \sum_{i=1, j=1}^{n,n}\conj{t_i}\abrack{\vec{w}_i, \vec{w}_j}t_j
    + \sum_{i=1}^n \conj{t}_i\abrack{\vec{w}_i, \vec{a}}
    + \sum_{i=1}^n\abrack{\vec{a}, \vec{w}_i}t_i
    + \abrack{\vec{a}, \vec{a}}
\\&= \vec{t}^*(\abrack{\vec{w}_i, \vec{w}_j})\vec{t}
    + \conj{t}^*(\abrack{\vec{w}_i, \vec{a}})
    + (\abrack{\vec{a}, \vec{w}_i})\vec{t}
    + \abrack{\vec{a}, \vec{a}}
\\&=
    \begin{bmatrix}\vec{t} \\ 1\end{bmatrix}^*
    \begin{bmatrix}
        (\abrack{\vec{w}_i, \vec{w}_j}) & (\abrack{\vec{w}_i, \vec{a}}) \\
        (\abrack{\vec{a}, \vec{w}_i}) & \abrack{\vec{a}, \vec{a}}
    \end{bmatrix}
    \begin{bmatrix}\vec{t} \\ 1\end{bmatrix}
\end{aligned}
\end{equation*}

We almost have a form to use the previous result. We only need to prove that \((\abrack{\vec{w}_i, \vec{w}_j})\) is positive-definite.
This is not hard to see since we can set \(\vec{a} = \vec{0}\) to obtain that the inner product above reduces to \(\vec{t}^* (\abrack{\vec{w}_i, \vec{w}_j}) \vec{t}\)
which is non-negative and only zero when \(\vec{t} = \vec{0}\).

\begin{admonition-note}[{}]
The bracket notation \((a_{ij})\) indicates the matrix as \(a_{ij}\) in the \(i\)'th row and \(j\)'th column.
\end{admonition-note}

\subsection{Rewriting using Gram Determiants}
\label{develop--fun:math:page--v2.adoc---rewriting-using-gram-determiants}

The \href{https://en.wikipedia.org/wiki/Gram\_matrix}{Gram matrix} of a set of vectors \(\vec{w}_1, \ldots \vec{w}_n\)
is the matrix whose \(ij\) entry contains \(\abrack{\vec{w}_i, \vec{w}_j}\); we
often denote it \(G(\vec{w}_1, \ldots \vec{w}_j)\). Then, by using this notation, we
see that the distance between the regions can be rewritten as follows

\begin{equation*}
\frac{|G(\vec{w}_1, \ldots \vec{w}_n, \vec{a})|}{|G(\vec{w}_1, \ldots \vec{w}_n)|}
\end{equation*}

Although the location of the minimum can be written in terms of Gram matrices,
it becomes a bit cumbersome. Instead, let us define a function \(H\)
which acts on vectors \(\vec{v}_1, \ldots \vec{v}_n\) and \(\vec{u}_1, \ldots \vec{u}_m\).
Then,

\begin{equation*}
H(\vec{v}_1, \ldots \vec{v}_n ; \vec{u}_1, \ldots \vec{u}_m)
= \begin{bmatrix}
\abrack{\vec{v}_1, \vec{u}_1} & \abrack{\vec{v}_1, \vec{u}_2} & \cdots & \abrack{\vec{v}_1, \vec{u}_m} \\
\abrack{\vec{v}_2, \vec{u}_1} & \abrack{\vec{v}_2, \vec{u}_2} &        & \abrack{\vec{v}_2, \vec{u}_m} \\
\vdots & & \ddots & \\
\abrack{\vec{v}_n, \vec{u}_1} & \abrack{\vec{v}_n, \vec{u}_2} &        & \abrack{\vec{v}_n, \vec{u}_m} \\
\end{bmatrix}
\end{equation*}

Note that this function is usually written as
\(
    \begin{bmatrix}\vec{v}_1, \ldots \vec{v}_n\end{bmatrix}^*
    \begin{bmatrix}\vec{u}_1, \ldots \vec{u}_m\end{bmatrix}
\)
where multiplication \(\vec{v}_i^*\vec{u}_j\) is interpreted
as the inner product \(\abrack{\vec{v}_i, \vec{u}_j}\).
However, I am not comfortable with this form as it is not associative.
Regardless, we see that \(G\) is \(H\) applied
with the same first and second sets of arguments.
Therefore, the location of the minimum is

\begin{equation*}
\vec{t} = - G(\vec{w}_1, \ldots \vec{w}_n)^{-1}H(\vec{w}_1, \ldots \vec{w}_n; \vec{a})
\end{equation*}

\subsection{Yet another interpretation}
\label{develop--fun:math:page--v2.adoc---yet-another-interpretation}

The above form for \(\vec{t}\) is very similar to projections. That is,
if we have a space spanned by \(\vec{w}_1, \ldots \vec{w}_n\),
the nearest point to \(-\vec{a}\) is the projection of \(-\vec{a}\)
into the subspace. We still need to prove this.

Let \(P \in \mathcal{L}(V, W)\) and \(T \in \mathcal{L}(V, \mathbb{C}^n)\) be defined as follows

\begin{equation*}
T(\vec{x}) = G(\vec{w}_1, \ldots \vec{w}_n)^{-1} H(\vec{w}_1, \ldots \vec{w}_n; \vec{x})
\quad \text{and}\quad
P(\vec{x}) = \sum_{i=1}^n T_i(\vec{x})\vec{w}_i
\end{equation*}

Then, we see that \(P\) is a projection into \(W\) since it is clearly
linear and

\begin{itemize}
\item \(T(\vec{w}_i) = \vec{e}_i\) since \(H(\vec{w}_1, \ldots \vec{w}_n; \vec{w}_i)\)
    is the \(i\)'th column of \(G(\vec{w}_1, \ldots \vec{w}_n)\)
\item \(P(\vec{w}_i) = \vec{w}_i\) for all \(i\)
\end{itemize}

Under this interpretation, we first make \(\vec{a}\) the new origin,
to make the region a subspace, then obtain the distance of the old
origin \(-\vec{a}\) to the subspace.

\section{Summary}
\label{develop--fun:math:page--v2.adoc---summary}

Given any pair of affine regions, \(A\) and \(B\), in a Hermatian product space,
we firstly compute a new region \(C = A - B\) consisting of differences
of vectors. Since \(A\) and \(B\)
are affine, \(C\) is also affine with the form

\begin{equation*}
C = \left\{\left.\vec{a} + \sum_{i=1}^n t_i\vec{w}_i \ \right|\ \vec{t} \in \mathbb{C}^n\right\}
\end{equation*}

where \(\vec{w}_1, \ldots \vec{w}_n\) are independent.
Notice that the distance from \(A\) to \(B\) is equal to the distance from
\(C\) to the origin. This distance is given by

\begin{equation*}
\frac{|G(\vec{w}_1, \ldots \vec{w}_n, \vec{a})|}{|G(\vec{w}_1, \ldots \vec{w}_n)|}
\end{equation*}

and is attained when \(\vec{t} = -G(\vec{w}_1, \ldots \vec{w}_n)^{-1}H(\vec{w}_1, \ldots \vec{w}_n; \vec{a})\)
\end{document}
