\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Laplace Transform}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(f:[a, b] \to \mathbb{R}\) be a function, then a
\emph{integral transform} is of \(f\) is defined by

\begin{equation*}
F(s) = \int_a^b k(s, t) f(t) \ dt
\end{equation*}

where \(k\) is the \emph{kernel} of the ransform. In this section,
we focus on the \emph{Laplace transform} with
\(k(s, t) = e^{-st}\), \(a = 0\) and
\(b = \infty\) and we denote it by

\begin{equation*}
F(s) = \mathcal{L}[f(t)] = \int_a^\infty e^{-st}f(t) \ dt
\end{equation*}

which exists only if the integral converges to a finite value.

\begin{admonition-note}[{}]
The laplace transform is an improper integral and should really be
written as

\begin{equation*}
\mathcal{L}[f(t)] = \lim_{R \to \infty} \int_0^R e^{-st} f(t) \ dt
\end{equation*}

however, for brevity, we often write it as an improper integral.
\end{admonition-note}

\section{Linearity}
\label{develop--math2271:ROOT:page--laplace-transform.adoc---linearity}

The laplace transform is a linear operator. That is, if
\(f, g: [0, \infty) \to \mathbb{R}\) have finite laplace
transforms at \(s\) and \(\alpha\) and
\(\beta\) are constants,

\begin{equation*}
\mathcal{L}[\alpha f + \beta g] = \alpha \mathcal{L}[f] + \beta \mathcal{L}[g]
\end{equation*}

this follows immediately from the linearity of limits and integrals.

\section{Exponential order, piecewise continuity and convergence}
\label{develop--math2271:ROOT:page--laplace-transform.adoc---exponential-order-piecewise-continuity-and-convergence}

A function \(f\) is said to be of \emph{exponential order}
\(a\) if \(\exists M, t_0 \in \mathbb{R}\) such that

\begin{equation*}
\forall t \in \mathbb{R}: t > t_0 \implies |f(t)| < Me^{at}
\end{equation*}

Note, that the value of \(a\) is not unique. Additionally, we
simply say that \(f\) has \emph{exponential order} if
\(\exists\) such a value \(a\).

Next, we say that \(f\) is \emph{piecewise continuous} if
\(f\) is continuous on its domain, possibly excluding a finite
number of points at which \(f\) is finite in value.
Equivalently, we can say that \(f\) is piecewise continuous on
its domain, if its domain can be divided into finitely many subintervals
on which it is continuous with finite left and right limits.

\subsection{Sufficient definition for existance of laplace transforms}
\label{develop--math2271:ROOT:page--laplace-transform.adoc---sufficient-definition-for-existance-of-laplace-transforms}

Let \(f\) be a piecewise continuous function on on
\([0, \infty)\) and with exponential order \(a\).
Then, the laplace transform of \(f\),
\(\mathcal{L}[f]\), exists \(\forall s > a\).

\begin{example}[{Proof}]
\begin{admonition-important}[{}]
For the concept of absolutely integrable, consult the
\href{https://en.wikipedia.org/wiki/Absolute\_convergence\#Absolute\_convergence\_of\_integrals}{wiki page}.
I am unsure how to properly do it, however, one concept would be to
integrate through the intervals \([n,n+1]\) and form a series which converges
absolutely.
\end{admonition-important}

Notice that since \(f\) is piecewise continuous, it is integrable and hence,
its product with \(e^-{st}\) is also integrable. Next, notice that

\begin{equation*}
\int_0^\infty |f(t)e^{-st}| \ dt
\leq \int_0^\infty Me^{at}e^{-st} \ dt
\leq M \int_0^\infty e^{(a-s)t} \ dt
\end{equation*}

which converges if \(a-s < 0 \iff s > a\). Therefore, \(f(t)e^{-st}\)
is absolutely integrable on \([0, \infty)\) and hence \(\mathcal{L}[f]\)
exists.
\end{example}

\begin{admonition-note}[{}]
This is condition is only sufficient and not necessary. For example, the
function \(t \mapsto \frac{1}{\sqrt{t}}\) is neither piecewise
continuous, not has exponential order but
\(\mathcal{L}[\tfrac{1}{\sqrt{t}}] = \frac{\sqrt{\pi}}{\sqrt{s}}\).
 [\myautoref[{see here}]{develop--math2271:ROOT:page--laplace-transform-proofs.adoc---reciprocal-square-root}]
\end{admonition-note}

\section{Popular functions}
\label{develop--math2271:ROOT:page--laplace-transform.adoc---popular-functions}

Below is a table containing popular laplace transforms.
See \myautoref[{here}]{develop--math2271:ROOT:page--laplace-transform-proofs.adoc} for proofs.

\begin{table}[H]\centering
\begin{tblr}{colspec={|[\thicktableline]Q[c,m]|Q[c,m]|Q[c,m]|[\thicktableline]}, measure=vbox}
\hline[\thicktableline]
\SetRow{font=\bfseries}{\(f(t)\)} & {\(\mathcal{L}[f(t)]\)} & {Validity} \\
\hline[\thicktableline]
{\(1\)} & {\(\frac{1}{s}\)} & {\(s > 0\)} \\
\hline
{\(t^n\)} & {\(\frac{n!}{s^{n+1}} = \frac{\Gamma(n+1)}{s^{n+1}}\)} & {\(s > 0, n > 0\)} \\
\hline
{\(e^{at}\)} & {\(\frac{1}{s-a}\)} & {\(s > \mathrm{Re}(a)\)} \\
\hline
{\(\sin(kt)\)} & {\(\frac{k}{s^2 + k^2}\)} & {\(s > 0\)} \\
\hline
{\(\cos(kt)\)} & {\(\frac{s}{s^2 + k^2}\)} & {\(s > 0 \)} \\
\hline
{\(\sinh(kt)\)} & {\(\frac{k}{s^2 - k^2}\)} & {\(s > |k|\)} \\
\hline
{\(\cosh(kt)\)} & {\(\frac{s}{s^2 - k^2}\)} & {\(s > |k|\)} \\
\hline
{\(y(t)\)} & {\(\mathcal{L}[y(t)] = Y(t)\)} & {\(s \in I\)} \\
\hline
{\(e^{at}y(t)\)} & {\(Y(s-a)\)} & {\(s -a \in I \iff s \in I + a\)} \\
\hline
{\(H(t - t_0)y(t-t_0)\)} & {\(e^{-st_0}Y(s)\)} & {\(t_0 \geq 0, s \in I\)} \\
\hline
{\(\delta(t-t_0)\)} & {\(e^{-st_{0}}\)} & {} \\
\hline
{\(y'(t)\)} & {\(sY(s) - y(0)\)} & {\(s \in I\)} \\
\hline
{\(y''(t)\)} & {\(s^2Y(s) - sy(0) - y'(0)\)} & {\(s \in I\)} \\
\hline[\thicktableline]
\end{tblr}
\end{table}

where

\begin{itemize}
\item \(H(t - t_0)\) is the \emph{Heaviside step function} defined by
    
    \begin{equation*}
    H(t-t_0) = \begin{cases}
                    0, \quad & t < t_0\\
                    1, \quad & t \geq t_0
                \end{cases}
    \end{equation*}
\item \(\delta(t - t_0)\) is the \emph{Dirac delta function} which is
    defined by
    
    \begin{equation*}
    \int_{-\infty}^\infty f(t)\delta(t - t_0) \ dt = f(t_0)
    \end{equation*}
    
    or in a less formal way as
    
    \begin{equation*}
    \delta(t-t_0) = \begin{cases}
                    \infty, \quad & t = t_0\\
                    0, \quad & \text{otherwise}
                \end{cases}
    \end{equation*}
\end{itemize}
\end{document}
