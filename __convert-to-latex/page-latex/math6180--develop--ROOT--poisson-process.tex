\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Poisson Processes}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\tr}{tr}% trace
\DeclareMathOperator{\trace}{tr}% trace
\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
\begin{admonition-tip}[{}]
Poisson processes are examples of \myautoref[{}]{develop--math6180:ROOT:page--ctmc.adoc}.
In particular, it is a pure birth process.
\end{admonition-tip}

Consider a collection of random variables \(\{X(t): t\geq 0\}\), which we will refer to as a \textbf{process}.
Typically we may think of \(t\) as time and \(X(t)\) as the evolution of some real world
process; however, this need not be necessary.
We define an \textbf{increment} of the process is the difference in the process at two times
\(s\) and \(t\) (\(s < t\)). That is \(X(t) - X(s)\).

\begin{description}
\item[Stationary increments] A process is said to have \textbf{stationary} increments
    if the distribution of \(X(t) - X(s)\) s a function of \(t-s\) only.
    So \(X(t_1) - X(s_1)\) and \(X(t_2) - X(s_2)\) have the same distribution
    if \(t_1 - s_1 = t_2 - s_2\).
\item[Independent increments] A process is said to have \textbf{independent} increments
    if the increments for two disjoint intervals are independent.
    That is for \(s_1 < t_1 < s_2 < t_2\), \(X(t_1) - X(s_1) \perp X(t_2) - X(s_2)\).
\end{description}

A \textbf{Poisson Process} with rate \(\lambda\) is a collection of discrete random variables (with continuous time)
\(\{N(t): t \geq 0\}\)
which satisfy the several properties

\begin{enumerate}[label=\arabic*)]
\item \(N(0) =0\)
\item The increments of the process are \textbf{stationary} and \textbf{independent}
\item For \(t > 0\), \(N(t) \sim Poisson(\lambda t)\)
\end{enumerate}

From this definition, we can get a few properties

\begin{itemize}
\item The Poisson process is non-decreasing (with probability 1).
    To see this, note that
    
    \begin{equation*}
    N(t)-N(s) \sim N(t-s)-N(0) = N(t-s) \sim Poisson(\lambda(t-s))
    \end{equation*}
    
    Note that we have to say with probability \(1\) since it is possible that our sample space
    allows for negative increments; but they occur with probability zero.
\item \(N(t+s) - N(t) \sim Poisson(\lambda s)\). For this reason, the Poisson process
    is a \textbf{counting process}.
\end{itemize}

Also, note that \(\{N(t) : t\geq 0\}\) can be viewed as a continuous time markov
chain with states \(\{0,1,2,3,\ldots\}\), constant rate \(\nu_n = \lambda\)
and transition probabilities of \(P_{i,i+1} = 1\).
Note that the constant rate \(\nu_n = \lambda\) is a result of the interarrival
times being iid \(Exp(\lambda)\) random variables (see below).

\section{Properties}
\label{develop--math6180:ROOT:page--poisson-process.adoc---properties}

Consider the Poisson process \(\{N(t): t \geq 0\}\) with rate \(\lambda\).
Since \(N(t)\) is a monotonic step function in \(t\) (with probability 1),
we can define \(S_1, S_2, \ldots\) to be the times at which steps
occur. That is for \(t < S_n\), \(P(N(t) < n) = 1\) and
for \(t > S_n\), \(P(N(t) \geq n) = 1\).
These are call the \textbf{event times} of the Poisson process.
We can also define \textbf{interarrival times} to be the times between events.
That is

\begin{itemize}
\item \(T_1 = S_1\)
\item \(T_n = S_n - S_{n-1}\) for \(n \geq 2\)
\end{itemize}

\begin{proposition}[{Distribution of increments}]
The interarrival times \(T_1, T_2 \ldots \) of a Poisson
process \(\{N(t): t\geq 0\}\) with rate \(\lambda\)
are iid \(Exponential(\lambda)\)
random variables.

\begin{admonition-tip}[{}]
Recall that \(\{X_i\}_{i\in I}\) is a collection
of linearly independent random variables iff
each finite subcollection is linearly independent
\end{admonition-tip}

\begin{proof}[{}]
To prove that the \(T_i\) are independent, we prove that each collection
of \(\{T_i\}_{i=1}^n\) is independent for each \(n\geq 1\).
To do this, we can simply find the joint pdf.

Note that

\begin{equation*}
\begin{aligned}
P\left(T_{n+1} > t_{n+1}\ |\ T_1=t_1,\ \ldots T_n=t_n\right)
&= P(N(t_{n+1} + t_n + \cdots + t_1) - N(t_n + \cdots + t_1) = 0)
\\&= P(N(t_{n+1}) = 0)
\quad\text{by stationary increments}
\\&= e^{-\lambda t_{n+1}}
\end{aligned}
\end{equation*}

So, the conditional pdf of \(T_{n+1}\ |\ T_1=t_1,\ \ldots T_n=t_n\)
is

\begin{equation*}
f_{T_{n+1}\ |\ T_1=t_1,\ \ldots T_n=t_n}(t_{n+1}) = \lambda e^{-\lambda t_{n+1}}
\end{equation*}

and hence the joint pdf is given by

\begin{equation*}
f_{T_1, \ldots T_n}(t_1, \ldots t_n)
= f_{T_1}(t_1) \prod_{i=1}^{n-1} f_{T_{i+1}\ |\ T_1=t_1,\ \ldots T_i=t_i}(t_{i+1})
= \prod_{i=1}^n \lambda e^{-\lambda t_{i}}
\end{equation*}

This is the joint pdf of \(n\) iid \(Exponential(\lambda)\) random variables as desired.
\end{proof}
\end{proposition}

\begin{corollary}[{Distribution of \(n\)'th event}]
Let \(\{N(t) : t \geq 0\}\) be a Poisson process with rate \(\lambda\).
Then, the time of the \(n\)'th event, \(S_n\), is a \(Gamma(n,\lambda)\) random variable.

\begin{admonition-caution}[{}]
This is the marginal distribution.
\end{admonition-caution}
\end{corollary}

\subsection{Conditional distribution}
\label{develop--math6180:ROOT:page--poisson-process.adoc---conditional-distribution}

If we know that \(n\) events occur in \([0, t]\) (ie \(N(t) = n\)),
we may also want to know in what the distribution of the events were.
It happens that the events occur uniformly in the \([0,t]\) interval.
I find this to be quite surprising

\begin{theorem}[{Conditional distribution of events}]
Let \(\{N(t) : t \geq 0\}\) be a Poisson process with rate \(\lambda\).
Then, the joint conditional density of \(S_1, \ldots S_n | N(t)=n\),

\begin{equation*}
f_{S_1, S_2, \ldots S_n\ |\ N(t) = n}(s_1, \ldots s_n) = \begin{cases}
\frac{n!}{t^n} , \quad&\text{if } 0 < t_1 < \cdots < t_n < t\\
0 , \quad&\text{otherwise}
\end{cases}
\end{equation*}

\begin{admonition-todo}[{}]
Proof
\end{admonition-todo}
\end{theorem}

\section{Alternative definition of Poisson process}
\label{develop--math6180:ROOT:page--poisson-process.adoc---alternative-definition-of-poisson-process}

Let \(\{N(t): t\geq 0\}\) be a continuous time stochastic process.
Then \(N\) is a Poisson process with rate \(\lambda > 0\) iff

\begin{enumerate}[label=\arabic*)]
\item \(N(0) = 0\)
\item \(N\) has stationary and independent increments
\item The following three hold (as \(h \to 0\))
    
    \begin{enumerate}[label=\alph*)]
    \item \(P(N(h) = 0) = 1-\lambda h + o(h)\)
    \item \(P(N(h) = 1) = \lambda h + o(h)\)
    \item \(P(N(h) \geq 2) = o(h)\)
    \end{enumerate}
\end{enumerate}

\begin{admonition-tip}[{}]
The symbol \(o(h)\) is an instance of
\href{https://en.wikipedia.org/wiki/Big\_O\_notation\#Little-o\_notation}{Landau's little-o notation}.
Note \(f(h) = o(g(h))\) (as \(h\to a\)) iff
\(\lim_{h \to a} \frac{f(h)}{g(h)} = 0\) where \(a\) is a limit point. That is \(a \in \mathbb{R} \cup \{\pm \infty\}\).
\end{admonition-tip}

Note that the only difference between this definition and the original definition is the third property.
So, we need to show that both third properties are equivalent (under assumption of the first two properties).

\subsection{Proof of forward direction}
\label{develop--math6180:ROOT:page--poisson-process.adoc---proof-of-forward-direction}

First assume that \(N(t) \sim Poisson(\lambda t)\).
Then,

\begin{enumerate}[label=\arabic*)]
\item \(P(N(h) = 0) = 1-\lambda h + o(h)\) since
    
    \begin{equation*}
    \lim_{h \to 0} \frac{P(N(h) = 0) - (1-\lambda h)}{h}
    = \lim_{h\to 0} \frac{e^{-\lambda h} + \lambda h}{h}
    = \lim_{h\to 0} \frac{-\lambda e^{-\lambda h} + \lambda}{1}
    = 0
    \end{equation*}
\item \(P(N(h) =1) = \lambda h + o(h)\) since
    
    \begin{equation*}
    \lim_{h \to 0} \frac{P(N(h) = 1) - \lambda h}{h}
    = \lim_{h\to 0} \frac{e^{-\lambda h} (\lambda h) - \lambda h}{h}
    = \lambda \lim_{h\to 0} (e^{-\lambda h} - 1)
    = 0
    \end{equation*}
\item \(P(N(h) \geq 2) = o(h)\)
    
    \begin{equation*}
    \begin{aligned}
    \lim_{h \to 0} \frac{P(N(h) \geq 2)}{h}
    &= \lim_{h \to 0} \frac{1-P(N(h) =0)-P(N(h) =1)}{h}
    \\&= \lim_{h \to 0} \frac{1-e^{-\lambda h} - e^{-\lambda h}(\lambda h)}{h}
    \\&= \lim_{h \to 0} \frac{e^{-\lambda h}(\lambda^2 h)}{1}
    \\&= 0
    \end{aligned}
    \end{equation*}
\end{enumerate}

Therefore the first definition of a Poisson process implies the second.

\subsection{Proof of reverse direction}
\label{develop--math6180:ROOT:page--poisson-process.adoc---proof-of-reverse-direction}

Next, assume that the second definition holds true.
Note that the proof of the reverse direction was not given in class.
But just prior, we did a proof of a Poisson approximation to the Binomial
by finding of \(P(X_n = k)\) as \(n\to \infty\), where \(X \sim Binomial\left(n, \frac{t\lambda}{n}\right)\).
So, it may make sense to prove that \(N(t) \sim Poisson(\lambda t)\) be a similar manner.
But instead, we can use the moment generating function approach.
We want to find the moment generating function of \(N(t)\) for some fixed \(t\).
We expect it to be \(e^{\lambda t (e^{s}-1)}\).

From the stationary and independent increments, we have that
for \(n\in \mathbb{Z}^+\) and \(s \in \mathbb{R}\)

\begin{equation*}
\begin{aligned}
E\left[s e^{N(t)}\right]
= E\left[e^{s\sum_{k=1}^n N\left(\frac{k}{n}t\right) - N\left(\frac{k-1}{n}\right)}\right]
= \prod_{k=1}^n E\left[e^{sN\left(\frac{k}{n}t\right) - sN\left(\frac{k-1}{n}\right)}\right]
= E\left[e^{sN\left(\frac{t}{n}\right)}\right]^n
\end{aligned}
\end{equation*}

Next, if \(h = \frac{t}{n}\) and restrict that \(s \leq 0\) (to ensure convergence), we have that

\begin{equation*}
\begin{aligned}
&E\left[e^{s N(h)}\right]
= \sum_{k=0}^\infty e^{sk} P(N(h) = k)
= P(N(h) = 0) + e^{s}P(N(h) = 1) + \sum_{k=2}^\infty e^{sk} P(N(h) = k)
\\&\implies
\begin{aligned}[t]
&P(N(h) = 0) + e^{s}P(N(h) = 1) + \sum_{k=2}^\infty (0) P(N(h) = k)
\\&\quad\leq E\left[e^{s N(h)}\right]
\\&\quad \leq
P(N(h) = 0) + e^{s}P(N(h) = 1) + \sum_{k=2}^\infty e^{2s} P(N(h) = k)
\end{aligned}
\\&\implies
\begin{aligned}[t]
&P(N(h) = 0) + e^{s}P(N(h) = 1)
\\&\quad\leq E\left[e^{s N(h)}\right]
\\&\quad \leq
P(N(h) = 0) + e^{s}P(N(h) = 1) + e^{2s}P(N(h) \geq 2)
\end{aligned}
\\&\implies
\begin{aligned}[t]
&P(N(t/n) = 0) + e^{s}P(N(t/n) = 1)
\\&\quad\leq E\left[e^{s N(t/n)}\right]
\\&\quad \leq
P(N(t/n) = 0) + e^{s}P(N(t/n) = 1) + e^{2s}P(N(t/n) \geq 2)
\end{aligned}
\end{aligned}
\end{equation*}

Also,
as \(n \to \infty\)

\begin{itemize}
\item \(P(N(t/n) = 0) + e^{s}P(N(t/n) = 1) = 1 + \frac{\lambda t (e^s - 1)}{n} + o\left(\frac{1}{n}\right)\)
\item \(P(N(t/n) = 0) + e^{s}P(N(t/n) = 1) + e^{2s}P(N(t/n) \geq 2) = 1 + \frac{\lambda t (e^s - 1)}{n} + o\left(\frac{1}{n}\right)\)
\end{itemize}

So,

\begin{equation*}
E\left[e^{sN(t)}\right] = E\left[e^{sN\left(\frac{t}{n}\right)}\right]^n
= \left[1 + \frac{\lambda t (e^s - 1)}{n} + o\left(\frac{1}{n}\right)\right]^n
\end{equation*}

Then, for each \(\varepsilon > 0\), we can find
sufficiently large \(n\) such that that the term inside the brackets differs from \(1 + \frac{\lambda t (e^s - 1)}{n}\)
by at most \(\frac{\varepsilon}{n}\).
Therefore, for each epsilon \(\varepsilon\), we can find sufficiently large \(n\) such that

\begin{equation*}
\left[1 + \frac{\lambda t (e^s - 1) - \varepsilon}{n}\right]^n
\leq E\left[e^{sN(t)}\right] \leq
\left[1 + \frac{\lambda t (e^s - 1) + \varepsilon}{n}\right]^n
\end{equation*}

and as \(n\to \infty\), we have that

\begin{equation*}
e^{\lambda t(e^s-1) - \varepsilon}
\leq E\left[e^{sN(t)}\right] \leq
e^{\lambda t(e^s-1) + \varepsilon}
\end{equation*}

Therefore,

\begin{equation*}
 E\left[e^{sN(t)}\right] =
e^{\lambda t(e^s-1)}
\end{equation*}

This is precisely the moment generating function of a \(Poisson(\lambda t)\) random variable and hence we have proven the desired result.

\section{Cool properties}
\label{develop--math6180:ROOT:page--poisson-process.adoc---cool-properties}

\begin{proposition}[{}]
Let \(\{N(t) : t \geq 0\}\) be a Poisson process with rate \(\lambda\).
Then,

\begin{equation*}
N(t) \xrightarrow[]{a.s.} \infty
\end{equation*}

That is

\begin{equation*}
P\left(\lim_{t \to \infty} N(t) = \infty\right) = 1
\end{equation*}

\begin{proof}[{}]
Consider arbitrary \(n\in \mathbb{Z}^+\) and \(t\geq 0\).
Then,

\begin{equation*}
\begin{aligned}
P(N(t) \geq n)
&\geq P\left(N\left(\frac{k}{n}t\right) - N\left(\frac{k-1}{n}t\right) \geq 1 \text{ for } k = 1, \ldots n\right)
\\&= \prod_{k=1}^n P\left(N\left(\frac{k}{n}t\right) - N\left(\frac{k-1}{n}t\right) \geq 1\right) \quad\text{by independence of increments}
\\&= P\left(N\left(\frac{t}{n}\right) \geq 1\right)^n
\\&= (1-e^{-\lambda t})^n
\end{aligned}
\end{equation*}

So,

\begin{equation*}
1 \leq P(N(t) \geq n \quad\text{for some } t \geq 0) \geq \lim_{t \to \infty} P(N(t) \geq n) \geq \lim_{t\to \infty} (1-e^{-\lambda t})^n
= 1
\end{equation*}

since for \(t_1 \leq t_2\), \(N(t_1) \geq n \implies N(t_2) \geq n\).
Therefore, we have that

\begin{equation*}
P\left(\lim_{t \to \infty} N(t) = \infty\right)
= P\left(\forall n \geq 1: \exists t \geq 0: N(t) \geq n\right)
= 1
\end{equation*}

since for each \(n \geq 1\), \(P\left(\lim_{t\to\infty} N(t) \geq n\right) = 1\)
\end{proof}
\end{proposition}

\begin{proposition}[{}]
Let \(\{N_1(t): t\geq 0\}\) and \(\{N_2(t): t\geq 0\}\) be independent Poisson processes with
rates \(\lambda_1\) and \(\lambda_2\) respectively.
Then, their superposition

\begin{equation*}
\{N_1(t) + N_2(t): t\geq 0\}
\end{equation*}

is also a Poisson process with rate \(\lambda_1 + \lambda_2\).

\begin{admonition-note}[{}]
The proof of this follows from the fact that the sum of two independent poisson random variables is also poisson,
but with the combined mean.
\end{admonition-note}
\end{proposition}

\section{Generalizations}
\label{develop--math6180:ROOT:page--poisson-process.adoc---generalizations}

\subsection{Multiple event types}
\label{develop--math6180:ROOT:page--poisson-process.adoc---multiple-event-types}

\begin{admonition-tip}[{}]
\strikethrough{This is a generalization of a theorem by Ross (1996) which was only two types.}
After reading the Ross text, this is the exact same theorem. The notes, that I was using, only focused on the special case where there are two
types.
\end{admonition-tip}

\begin{admonition-tip}[{}]
This can also be viewed as a \textbf{thinning} of the original process since each \(N_i\) counts only a subset of the events.
\end{admonition-tip}

Consider a poisson process \(\{N(t): t\geq 0\}\)
with rate \(\lambda\).
Now, suppose that event may be classified as one of \(i \in I\)
which occur with probabilities \(p_i(t)\) respectively
where at each \(t \geq 0\), \(\sum_{i\in I}p_i(t) = 1\).
So, we may be interested in the separate processes
\(\{N_i(t): t \geq 0\}\) which count the number of occurrences of event type \(i \in I\).

\begin{theorem}[{}]
For some fixed \(t \geq 0\), the variables \(N_i(t)\) are independent
poisson random variables with respective means \(\lambda P_i t\)
where

\begin{equation*}
P_i = \frac{1}{t} \int_0^t p_i(s) \ ds
\end{equation*}

\begin{proof}[{}]
We will find

\begin{equation*}
P(N_i(t) = n_i: i \in I)
\end{equation*}

for some fixed \(\{n_i\}_{i\in I} \in \mathbb{N}^I\).
Let \(n = \sum_{i\in I} n_i\).
Let \(0< T_1 < \cdots < T_n < t\) be the times of the events.
Then,

\begin{equation*}
\begin{aligned}
&P(N_i(t) = n_i: i \in I)
\\&= P(N(t) = n) P(N_i(t) = n_i: i \in I \ | \ N(t) = n)
\\&= P(N(t) = n) \int_{0 < s_1 < \cdots s_n < t} P\left(\begin{aligned}&N_i(t) = n_i: i \in I \\&| \ N(t) = n, \ T_1 = s_1, \ldots T_n=s_n\end{aligned}\right)
    f_{T_1,\ldots T_n| N(t) = n}(s_1, \ldots s_n) \ dt
\\&\hspace{4em}\text{by conditioning on } T_1, \ldots T_n
\\&= \frac{e^{-\lambda t} (\lambda t)^n}{n!} \int_{0 < s_1 < \cdots s_n < t} P\left(\begin{aligned}&N_i(t) = n_i: i \in I \\&| \ N(t) = n, \ T_1 = s_1, \ldots T_n=s_n\end{aligned}\right)
    \frac{n!}{t^n} \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} P\left(\begin{aligned}&N_i(t) = n_i: i \in I \\&| \ N(t) = n, \ T_1 = s_1, \ldots T_n=s_n\end{aligned}\right) \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}P\left(
    \text{events of class } i \text{ occur at } J_i \right) \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}P\left(
    \text{events of class } i \text{ occur at } J_i \right) \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}
    \prod_{i \in I} \prod_{j \in J_i} p_i(s_j) \ dt
\\&= e^{-\lambda t} \lambda^n \frac{1}{n!}\int_{s_1,\ldots s_n \in [0,t]} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}
    \prod_{i \in I} \prod_{j \in J_i} p_i(s_j) \ dt
    \quad\text{by symmetry of the }s_j
\\&= e^{-\lambda t} \lambda^n \frac{1}{n!}\int_{\begin{array}{c}s_{ij} \in [0,t]\\i \in I, \ j \in 1,\ldots n_i\end{array}} \binom{n}{n_i: i \in I} \prod_{i \in I}\prod_{j=1}^{n_i} p_{i}(s_{ij}) \ dt
    \quad\text{by assigning the }s_j
\\&= e^{-\lambda t} \lambda^n \frac{1}{n!} \binom{n}{n_i: i \in I} \prod_{i \in I} \left(\int_0^t  p_{i}(s) \ ds\right)^{n_i}
\\&= e^{-\lambda t} \lambda^n \frac{1}{\prod_{i \in I} n_i!} \prod_{i \in I} t^{n_i}\left(\frac{1}{t}\int_0^t  p_{i}(s) \ ds\right)^{n_i}
\\&= e^{-\lambda t} \lambda^n \prod_{i \in I} \frac{(\lambda t)^{n_i}}{n_i!} P_i^{n_i}
\\&= e^{-\lambda \sum_{i \in I} P_i t} \prod_{i \in I} \frac{(\lambda t)^{n_i}}{n_i!} P_i^{n_i}\quad\text{since } \sum_{i\in I}P_i = 1
\\&=  \prod_{i \in I} \frac{e^{-\lambda P_i t }(\lambda P_i t)^{n_i}}{n_i!}
\end{aligned}
\end{equation*}

Therefore, we have the desired result
\end{proof}
\end{theorem}

\subsection{Non-homogeneous Poisson process}
\label{develop--math6180:ROOT:page--poisson-process.adoc---non-homogeneous-poisson-process}

A Markov process \(\{N(t) : t \geq 0\}\) is a \textbf{non-homogeneous} Poisson
process with intensity \(\lambda(t)\) (where \(\lambda: \mathbb{R}^+ \to \mathbb{R}^+\)) if

\begin{enumerate}[label=\arabic*)]
\item \(N(0) = 0\)
\item \(\{N(t): t\geq 0\}\) has independent increments
\item For \(0 \leq s < t\), the random variable \(N(t) - N(s)\)
    is Poisson with mean \(\int_s^t \lambda(u) \ du\).
    That is,
    
    \begin{equation*}
    P(N(t) - N(s) = k) = \frac{e^{-\int_s^t \lambda(u) \ du} \left(\int_s^t \lambda(u) \ du\right)^k}{k!}
    ,\quad k = 0,1,2,\ldots
    \end{equation*}
\end{enumerate}

\subsection{Compound Poisson Process}
\label{develop--math6180:ROOT:page--poisson-process.adoc---compound-poisson-process}

Let \(\{N(t): t\geq 0\}\) be a poisson process and \(\{Y_i: i \in \mathbb{Z}^+\}\)
be a sequence of iid random variables.
Then, we can define a \textbf{compound poisson process} \(\{X(t): t\geq 0\}\)
as

\begin{equation*}
X(t) = \sum_{i=1}^{N(t)} Y_i
\end{equation*}
\end{document}
