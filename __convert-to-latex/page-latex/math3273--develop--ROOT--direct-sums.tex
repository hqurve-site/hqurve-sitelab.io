\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Direct Sums, Basis and Dimension}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
% Title omitted
\section{Defintions}
\label{develop--math3273:ROOT:page--direct-sums.adoc---defintions}

\subsection{Direct Sum}
\label{develop--math3273:ROOT:page--direct-sums.adoc---direct-sum}

Let \(U\) and \(W\) be subspaces of the vector space \(V\),
Then, if

\begin{itemize}
\item \(U+W = V\)
\item \(U \cap W = \{\vec{0}\}\)
\end{itemize}

we say that \(V\) is the direct sum of \(U\) and \(W\)
and write \(U \oplus W = V\).

\subsection{Linear Independence}
\label{develop--math3273:ROOT:page--direct-sums.adoc---linear-independence}

Let \(V\) be a vector space with field \(\mathbb{F}\).
Then a finite subset of \(V\), \(\{\vec{v}_1\ldots \vec{v}_n\}\)
is \emph{linear independent} iff

\begin{equation*}
\sum_{i=1}^n c_i \vec{v_i} = 0 \implies c_i = 0_{\mathbb{F}} \ \forall i =1\ldots n
\end{equation*}

where each of the \(c_i \in \mathbb{R}\). Additionally,
an infinite subset \(\mathcal{V} \subseteq V\) is linearly independent,
iff all of its finite subsets are linearly independent.

\subsection{Basis}
\label{develop--math3273:ROOT:page--direct-sums.adoc---basis}

Let \(V\) be a vector space. Then a subset \(\mathcal{B} \subseteq V\)
is a basis of \(V\) iff \(\mathcal{B}\) is linearly independent and
a spanning set of \(V\)

\section{Uniquenss of representation with respect to basis}
\label{develop--math3273:ROOT:page--direct-sums.adoc---uniquenss-of-representation-with-respect-to-basis}

Let \(V\) be a vector space with field \(\mathbb{F}\) and basis \(\mathcal{B}\).
Then,

\begin{enumerate}[label=\arabic*)]
\item \(\sum_{i} c_i\vec{b}_i = \sum_{i} d_i\vec{b}_i \implies c_i = d_i \)
\item \(\forall \vec{v} \in V: \exists \{c_i\}\subset \mathbb{F}: \vec{v} = \sum_{i} c_i\vec{b}_i\)
\end{enumerate}

\begin{admonition-note}[{}]
These two facts follow directly from the fact that \(\mathcal{B}\)
is linearly independent and a spanning set of \(V\).
\end{admonition-note}

\subsection{Coordinate vector}
\label{develop--math3273:ROOT:page--direct-sums.adoc---coordinate-vector}

If the \(|B|=n\) is finite, we write the \(c_i\) as a column vector and denote it as follows

\begin{equation*}
[\vec{v}]_\mathcal{B} = \begin{bmatrix}c_1\\\vdots\\ c_n\end{bmatrix}
\end{equation*}

and is called the \emph{coordinate vector} of \(\vec{v}\) relative to \(\mathcal{B}\).

\begin{admonition-note}[{}]
\([\vec{v}]_\mathcal{B} \in \mathbb{F}^n\) and by the uniqueness and existance of the coordinate vector, there is an isomorphism
between \(V\) and \(\mathbb{F}^n\). Therefore, no vector space with finite basis can be too "weird".

Furthermore, the set of coordinate vectors wrt \(\mathcal{B}\) form a vector space with the
same basis \(\mathbb{F}\) with the following properties

\begin{enumerate}[label=\arabic*)]
\item \([\vec{u} + \vec{v}]_\mathcal{B} = [\vec{u}]_\mathcal{B} + [\vec{v}]_\mathcal{B}\)
\item \([\alpha\vec{u}]_\mathcal{B} = \alpha[\vec{u}]_\mathcal{B} \)
\item \([\vec{u}]_\mathcal{B} = [\vec{v}]_\mathcal{B} \iff \vec{u} = \vec{v}\)
\end{enumerate}

Or simply, let \(\Phi: V \to \mathbb{F}^n\) defined by \(\Phi(\vec{v}) = [\vec{v}]_\mathcal{B}\). Then
\(\Phi\) is a linear isomorphism.
\end{admonition-note}

\section{(Property name yet to determined)}
\label{develop--math3273:ROOT:page--direct-sums.adoc---property-name-yet-to-determined}

Let \(V\) be a vector space, \(\vec{v}_1\ldots \vec{v}_n \in V\) be linearly independent
and \(\vec{v} = \sum_{i} c_i \vec{v}_i\) where \(\exists c_j \neq 0\). Then,

\begin{enumerate}[label=\arabic*)]
\item \(span(\vec{v}_1, \ldots,\vec{v}_n) = span(\vec{v}_1\ldots\xcancel{\vec{v}_{j}}\ldots\vec{v}_n, \vec{v})\)
\item If \(\vec{v}_1\ldots \vec{v}_n\) is linearly independent, so is \(\vec{v}_1\ldots\xcancel{\vec{v}_j}\ldots\vec{v}_n, \vec{v}\)
\item If \(\vec{v} \notin span(\vec{v}_1\ldots\vec{v}_n)\) then, \(\exists k > a\) such that \(c_k \neq 0\)
\item If \(\vec{v} \notin span(\vec{v}_1\ldots\vec{v}_n)\) then, \(\vec{v}_1\ldots\vec{v}_n,\vec{v}\) is linearly independent.
\end{enumerate}

\section{Extending a linearly independent set to be a basis}
\label{develop--math3273:ROOT:page--direct-sums.adoc---extending-a-linearly-independent-set-to-be-a-basis}

Let \(W\) be a subspace of \(V\) where \(dim(V) = n\). Then, if
\(\vec{w}_1\ldots \vec{w}_k \in W\) are linearly independent, \(\exists \vec{w}_{k+1}\ldots \vec{w}_u \in W\)
such that \(\vec{w}_1\ldots \vec{w}_u\) is a basis for \(W\) and \(u \leq n\)

\section{Existance of "complement" orthoganal direct sum}
\label{develop--math3273:ROOT:page--direct-sums.adoc---existance-of-complement-orthoganal-direct-sum}

Let \(W\) be a subspace of \(V\). Then, \(\exists\) subspace \(U\) of \(V\)
such that \(V = W \oplus U\).

\href{https://en.wikipedia.org/wiki/Complemented\_subspace}{Wikipedia article}

\section{Direct Sums as a generalization of a basis}
\label{develop--math3273:ROOT:page--direct-sums.adoc---direct-sums-as-a-generalization-of-a-basis}

Let \(V\) be a vector space. Then, if \(V_1\ldots V_n\) are subspaces such that

\begin{equation*}
V = V_1\oplus \cdots \oplus V_n
\end{equation*}

then \(\forall \vec{v} \in V\): \(\vec{v}\) can be uniquely represented by

\begin{equation*}
\vec{v} = \vec{v}_1 + \cdots + \vec{v}_n \quad\text{where } \vec{v}_i \in V_i
\end{equation*}

\begin{admonition-note}[{}]
This can be thought as a generalization of a basis since a basis can be transformed into
a collection of subspaces \(V_1\ldots V_n\) where \(V_i = \{k\vec{v}_i\ |\ k \in \mathbb{F}\}\).
\end{admonition-note}

\subsection{Validating a direct sum}
\label{develop--math3273:ROOT:page--direct-sums.adoc---validating-a-direct-sum}

In an almost anlogous way, the collection of subspaces \(V_1\ldots V_n\) is a direct sum of
vector space \(V\) if the following two hold

\begin{itemize}
\item each \(v\) can be written as the sum of \(\vec{v}_i \in V_i\)
\item \(\sum \vec{v}_i = 0\) implies that \(\vec{v}_i = 0\)
\end{itemize}

\subsection{Constructing a direct sum from a basis}
\label{develop--math3273:ROOT:page--direct-sums.adoc---constructing-a-direct-sum-from-a-basis}

Let \(\mathcal{B} = \{\vec{v}_1\ldots \vec{v}_n\}\) be a basis for vectorspace \(V\).
Then, we can partition \(\mathcal{B}\) into sub collections \(\mathcal{B}_i\). Then
the collection \(V_i = span(\mathcal{B}_i)\) is a direct sum of \(V\).

An interesting special case is when each of \(\mathcal{B}_i\) contains only one element.
In this case, each \(V_i = span(\vec{v}_i)\).

\subsection{Constructing basis from basis of direct sum subspaces}
\label{develop--math3273:ROOT:page--direct-sums.adoc---constructing-basis-from-basis-of-direct-sum-subspaces}

Let \(V\) be a vector space such that \(\oplus V_i\) where \(\mathcal{B}_i\)
is a basis for \(V_i\). Then \(\mathcal{B}^* = \cup \mathcal{B}_i\) is a basis for \(V\)
and in particular,

\begin{equation*}
[\vec{v}]_{\mathcal{B}^*} =
\begin{pmatrix}
\vec{x}_1\\\hline
\vdots \\\hline
\vec{x}_n
\end{pmatrix}
\end{equation*}

where \(\vec{v} = \sum \vec{v_i}\) and
\(\vec{x}_i = [\vec{v}_i]_{\mathcal{B}_i}\)
\end{document}
