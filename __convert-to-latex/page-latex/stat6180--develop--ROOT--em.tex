\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{The EM Algorithm}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\interior}{Int}
\DeclareMathOperator{\closure}{Cl}
\DeclareMathOperator{\limit}{Lim}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}

% Title omitted
The EM algorithm is used when a sample contains missing data.
Typically, this missing data may be in the form of

\begin{description}
\item[Unrecorded variables] For example, a discriminator to indicate which
    part of the population the sample came from
\item[Incomplete recordings] For example, a survey respondent may leave an entry blank
\end{description}

In both cases, proper inference cannot be done without the missing data.
Additionally, the data which we do have should not be discarded since they
may contain valuable insights.

The EM Algorithm solves the problem of missing data by using a two stage process:

\begin{description}
\item[Expectation] estimate the missing data using the expectation
\item[Maximization] estimate the parameters using the most likely estimates
\end{description}

Suppose we have a sample of \(n_1\) observed variables \(\vec{X} = (X_1,\ldots X_{n_1})\)
and \(n_2\) unobserved variables \(\vec{Z} = (Z_1,\ldots Z_{n_2})\).
Let \(\theta \in \Omega\) be the parameter of interest and

\begin{itemize}
\item \(g(\vec{x}\ |\ \theta)\) be the joint pdf of the known variables
\item \(h(\vec{x}, \vec{z}\ | \ \theta)\) be the joint pdf of the observed and unobserved items.
\item \(k(\vec{z}\ |\ \theta, \vec{x})\) be the conditional pdf of the unobserved data given the observed data \(\vec{z}\).
    Note that
    
    \begin{equation*}
    \numberandlabel{em:conditional}
    k(\vec{z}\ | \ \theta, \vec{x}) = \frac{h(\vec{x},\vec{z} \ | \ \theta)}{g(\vec{x} \ | \ \theta)}
    \end{equation*}
\end{itemize}

We also define

\begin{description}
\item[Observed likelihood function] \(L(\theta \ | \ \vec{x}) = g(\vec{x} \ | \ \theta)\)
\item[Complete likelihood function] \(L^c(\theta \ | \ \vec{x},\vec{z}) = h(\vec{x},\vec{z}\ | \ \theta)\)
\end{description}

The goal is to maximize \(L(\theta \ | \ \vec{x})\) by using \(L^c(\theta \ | \ \vec{x},\vec{z})\).
Note that we may be able to maximize \(L\) directly, but that will resort to using conditionals
and may get very difficult.

\section{The algorithm}
\label{develop--stat6180:ROOT:page--em.adoc---the-algorithm}

The EM algorithm produces a sequence of estimates
\(\hat{\theta}^{(0)}, \hat{\theta}^{(1)}, \hat{\theta}^{(2)}, \ldots\).
In each iteration, we produce \(\hat{\theta}^{(m+1)}\) using \(\hat{\theta}^{(m)}\).

At the algorithm is based on the below lemma.

\begin{lemma}[{}]
Let \(\theta_0 \in \Omega\). Then

\begin{equation*}
\begin{aligned}
\log L(\theta\ | \  \vec{x})
&= E_{\theta_0}\left[\log L(\theta\ \middle| \  \vec{x})\ | \ \theta_0,\vec{x}\right]
    \quad\text{this is the expectation with respect to random variable } \vec{Z}|\theta_0,\vec{x}
\\&= E_{\theta_0}\left[\log g(\vec{x}\ | \  \theta)\ \middle| \ \theta_0,\vec{x}\right]
\\&= E_{\theta_0}\left[\log \frac{h(\vec{x},\vec{Z} \ | \ \theta)}{k(\vec{Z} \ | \ \theta,\vec{x})}\ \middle| \ \theta_0,\vec{x}\right]
    \quad\text{by rearranging Equation } \eqref{em:conditional}
\\&= E_{\theta_0}\left[\log h(\vec{x},\vec{Z} \ | \ \theta) - \log k(\vec{Z} \ | \ \theta,\vec{x})\ \middle| \ \theta_0,\vec{x}\right]
\\&= E_{\theta_0}\left[\log h(\vec{x},\vec{Z} \ | \ \theta)\ \middle| \ \theta_0,\vec{x}\right]
        - E_{\theta_0}\left[\log k(\vec{Z} \ | \ \theta,\vec{x})\ \middle| \ \theta_0,\vec{x}\right]
\\&= E_{\theta_0}\left[\log L^c(\theta \ | \ \vec{x},\vec{Z} )\ \middle| \ \theta_0,\vec{x}\right]
        - E_{\theta_0}\left[\log k(\vec{Z} \ | \ \theta,\vec{x})\ \middle| \ \theta_0,\vec{x}\right]
\\&= Q(\theta \ | \ \theta_0,\vec{x})
        - E_{\theta_0}\left[\log k(\vec{Z} \ | \ \theta,\vec{x})\ \middle| \ \theta_0,\vec{x}\right]
\end{aligned}
\end{equation*}

where

\begin{equation*}
Q(\theta \ | \ \theta_0,\vec{x})
= E_{\theta_0}\left[\log L^c(\theta \ | \ \vec{x},\vec{Z} )\ \middle| \ \theta_0,\vec{x}\right]
\end{equation*}
\end{lemma}

Therefore, we can attempt to maximize \(L(\theta\ | \ \vec{x})\) by attempting
to maximize \(Q(\theta \ | \ \hat{\theta}^{(m)},\vec{x})\).
We state the formal algorithm below

Let \(\hat{\theta}^{(0)}\) be the initial estimate of \(\theta\).
Let \(\hat{\theta}^{(m)}\) denote the estimate on the \(m\)'th step.
To compute the estimate on the \((m+1)\)'st step,

\begin{enumerate}[label=\arabic*)]
\item Expectation Step: Compute
    
    \begin{equation*}
    Q(\theta \ | \ \hat{\theta}^{(m)},\vec{x})
    = E_{\hat{\theta}^{(m)}}\left[\log L^c(\theta \ | \ \vec{x},\vec{Z} )\ \middle| \ \hat{\theta}^{(m)},\vec{x}\right]
    \end{equation*}
    
    where the expectation is taken under the conditional pdf
    \(k(\vec{z} \ | \ \hat{\theta}^{(m)},\vec{x})\)
\item Maximization Step: Let
    
    \begin{equation*}
    \hat{\theta}^{(m+1)}
    = \operatorname{Argmax} Q(\theta \ | \ \hat{\theta}^{(m)},\vec{x})
    \end{equation*}
\end{enumerate}

\begin{theorem}[{Increasing likelihood}]
Consider the sequence of estimates \(\hat{\theta}^{(m)}\)
defined above.
Then,

\begin{equation*}
L\left(\hat{\theta}^{(m+1)} \ \middle|\ \vec{x}\right)
\geq L\left(\hat{\theta}^{(m)} \ \middle|\ \vec{x}\right)
\end{equation*}

\begin{proof}[{}]
\begin{admonition-note}[{}]
This proof is taken directly from the notes
\end{admonition-note}

Since \(\hat{\theta}^{(m+1)}\) maximizes
\(Q(\theta \ | \ \hat{\theta}^{(m)},\vec{x})\),
we have that

\begin{equation*}
\numberandlabel{em:increasing-q}
Q(\hat{\theta}^{(m+1)} \ | \ \hat{\theta}^{(m)},\vec{x})
\geq
Q(\hat{\theta}^{(m)} \ | \ \hat{\theta}^{(m)},\vec{x})
\end{equation*}

Next, note that

\begin{equation*}
\begin{aligned}
E_{\hat{\theta}^{(m)}}
\left\{\log \left[
    \frac{k(\vec{Z} \ | \ \hat{\theta}^{(m+1)},\vec{x})}
    {k(\vec{Z} \ | \ \hat{\theta}^{(m)},\vec{x})}
\right]\right\}
&\leq
\log E_{\hat{\theta}^{(m)}}
\left[
    \frac{k(\vec{Z} \ | \ \hat{\theta}^{(m+1)},\vec{x})}
    {k(\vec{Z} \ | \ \hat{\theta}^{(m)},\vec{x})}
\right]
\quad\text{by Jensen's Inequality}
\\&=
\log \int
    \frac{k(\vec{Z} \ | \ \hat{\theta}^{(m+1)},\vec{x})}
    {k(\vec{Z} \ | \ \hat{\theta}^{(m)},\vec{x})}
k(\vec{Z} \ | \ \hat{\theta}^{(m)},\vec{x})
\ d\vec{z}
\\&=
\log \int
    k(\vec{Z} \ | \ \hat{\theta}^{(m+1)},\vec{x})
\ d\vec{z}
\\&=\log 1 = 0
\end{aligned}
\end{equation*}

So, by rearranging, we have that

\begin{equation*}
E_{\hat{\theta}^{(m)}}\left[
    k(\vec{Z} \ | \ \hat{\theta}^{(m+1)},\vec{x})
\right]
\leq
E_{\hat{\theta}^{(m)}}\left[
    k(\vec{Z} \ | \ \hat{\theta}^{(m)},\vec{x})
\right]
\end{equation*}

and hence by combining the above inequality with Equation \(\eqref{em:increasing-q}\) we get the desired
inequality.
\end{proof}
\end{theorem}

\section{Example: Mixture model}
\label{develop--stat6180:ROOT:page--em.adoc---example-mixture-model}

\begin{admonition-tip}[{}]
For univariate and multivariate normal distributions, the \texttt{mclust} library can be used
for the mixture model.
\end{admonition-tip}

Consider a mixture model where we have \(p\) different distributions.
In this model, samples are pairs \((X, W)\) where \(W\) can take any
value from \(1,\ldots p\) with probabilities \(\alpha_j\)
and \(X\) follows the \(W\)'th distribution.
Then, the pdf of \(X|W\) is given by

\begin{equation*}
f(x \ | \ w,\theta) = \begin{cases}
f_1(x\ | \ \theta) &\quad\text{if } w=1\\
\vdots&\\
f_p(x\ | \ \theta) &\quad\text{if } w=p\\
\end{cases}
\end{equation*}

Note that \(f_j\) may only really require some of the parameters in \(\theta\).
Also, note that there are two sets of parameters to be estimated

\begin{itemize}
\item The distribution parameter \(\theta\)
\item The weights \(\alpha_1, \ldots \alpha_n\)
\end{itemize}

Now, suppose we have sample \((X_1, W_1), \ldots (X_n, W_n)\)
where the \(X_i\) are observed but the \(W_i\) are not.
So, the complete likelihood function is given
by

\begin{equation*}
L^c(\theta \ | \ \vec{x}, \vec{w})
=
\prod_{W_i = 1}f_1(x_i)
\prod_{W_i = 2}f_2(x_i)
\cdots
\prod_{W_i = p}f_p(x_i)
\end{equation*}

and hence the complete log likelihood function is

\begin{equation*}
\begin{aligned}
l^c(\theta \ | \ \vec{x},\vec{w})
&=
\sum_{W_i = 1} \log f_1(x_i)
+
\sum_{W_i = 2} \log f_2(x_i)
+
\cdots
+
\sum_{W_i = p} \log f_p(x_i)
\\&=
\sum_{i = 1}^n\left[
    I_1(w_i)\log f_1(x_i)
    + I_2(w_i)\log f_2(x_i)
    +\cdots
    + I_p(w_i)\log f_p(x_i)
\right]
\end{aligned}
\end{equation*}

where \(I_j(t) = 1\) if \(j=t\) and \(0\) otherwise.

\begin{admonition-warning}[{}]
The above is slightly wrong, but we can use it in this course.
The complete likelihood function is actually

\begin{equation*}
L^c(\theta \ | \ \vec{x}, \vec{w})
=
\prod_{W_i = 1}\alpha_1f_1(x_i)
\prod_{W_i = 2}\alpha_2f_2(x_i)
\cdots
\prod_{W_i = p}\alpha_nf_p(x_i)
\end{equation*}

So, the log likelihood function becomes

\begin{equation*}
l^c(\theta \ | \ \vec{x},\vec{w})
=
\sum_{i = 1}^n \sum_{j=1}^p
    I_j(w_i)\log f_j(x_i)
+
\sum_{i = 1}^n \sum_{j=1}^p
    I_j(w_i)\log \alpha_j
\end{equation*}

Note that this does not affect the maximization step,
except it explains the formulae we get for the \(\alpha_i\).
\end{admonition-warning}

\subsection{Performing expecation step}
\label{develop--stat6180:ROOT:page--em.adoc---performing-expecation-step}

\begin{equation*}
\begin{aligned}
Q(\theta \ | \ \theta_0, \vec{x})
&= E\left[
\sum_{i = 1}^n
    I_1(W_i)\log f_1(x_i)
    + I_2(W_i)\log f_2(x_i)
    +\cdots
    + I_p(W_i)\log f_p(x_i)
\right]
\\&\hspace{3em}
\text{since the expectation is with respect to }W|X
\\&=
\sum_{i = 1}^n
    E[I_1(W_i)]\log f_1(x_i)
    + E[I_2(W_i)]\log f_2(x_i)
    +\cdots
    + E[I_p(W_i)]\log f_p(x_i)
\end{aligned}
\end{equation*}

Note that in the above, the expectations are conditional
on \(\vec{x}\).
So,

\begin{equation*}
\begin{aligned}
\gamma_{ij} = E[I_j(W_i)]
&= P(W_i=j)
\\&= \frac{P(W_i=j,\ X_i=x_i)}{P(X_i=x_i)}
\\&= \frac{P(X_i=x_i \ | \ W_i=j)P(W_i=j)}{\sum_{k=1}^p P(X_i=x_i \ | \ W_i=k)P(W_i=k)}
\\&= \frac{\hat\alpha_j f_j(x_i)}{\sum_{k=1}^p \hat\alpha_k f_k(x_i)}
\\&= \frac{\hat\alpha_j f_j(x_i)}{\hat\alpha_1 f_1(x_i) + \cdots + \hat\alpha_p f_p(x_i)}
\end{aligned}
\end{equation*}

where \(\hat\alpha_j\) is the estimate for \(\alpha_j\)
from the previous stage
and \(f_k(x_i) = f_k(x_i \ | \ \hat{\theta})\).
Therefore at the end of our E step, we have that

\begin{equation*}
Q(\theta \ | \ \theta_0, \vec{x})
= \sum_{i = 1}^n
    \gamma_{i1}\log f_1(x_i)
    + \gamma_{i2}\log f_2(x_i)
    +\cdots
    + \gamma_{ip}\log f_p(x_i)
\end{equation*}

\subsection{Performing maximization step}
\label{develop--stat6180:ROOT:page--em.adoc---performing-maximization-step}

Once we have determined \(Q(\theta \ | \ \theta_0, \vec{x})\),
the next step is to estimate the parameters.

For the weight parameters \(\alpha_j\), the maximum likelihood estimates are

\begin{equation*}
\numberandlabel{em:mixture-mle-weights}
\hat\alpha_j = \frac1n \sum_{i=1}^n \gamma_{ij}
\end{equation*}

and its not hard to see that the \(\sum \hat\alpha_j =1\).

For the distribution parameter \(\theta\), we need to consider
the distributions of \(f_1,\ldots f_p\).
For the special case where \(f_j\) is the pdf of a \(N(\mu_j,\sigma_j^2)\)
distribution, we would have that

\begin{equation*}
\numberandlabel{em:mixture-mle-params}
\hat\mu_j = \frac{\sum_{i=1}^n \gamma_{ij} x_i}{\sum_{i=1}^n \gamma_{ij}}
\quad\text{and}\quad
\hat\sigma_j^2 = \frac{\sum_{i=1}^n \gamma_{ij} (x_i-\hat\mu_j)^2}{\sum_{i=1}^n \gamma_{ij}}
\end{equation*}

which are just the weighted versions of the usual MLEs.

Overall, the process consists of

\begin{enumerate}[label=\arabic*)]
\item Initial: Determine an initial guess for the \(\alpha_j\) and \(\mu_j, \sigma_j^2\)
\item Estimation: estimate the \(\gamma_{ij}\)
\item Maximization: update the guesses of \(\alpha_j\) and \(\mu_j, \sigma_j^2\)
    using Equations \(\eqref{em:mixture-mle-weights}\)
    and \(\eqref{em:mixture-mle-params}\).
\item Repeat steps 2 and 3 until desired convergence.
\end{enumerate}
\end{document}
