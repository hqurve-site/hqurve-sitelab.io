\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Empirical Distribution function}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\vec#1{\boldsymbol{#1}}
% overbar command from https://tex.stackexchange.com/a/22134 by user Michael Ummels
\def\overbar#1{\mkern 2mu \overline{\mkern-2mu {#1} \mkern-2mu}\mkern 2mu}
\def\bigchi{{\Large \chi}}

% Title omitted
Our first step is to collect data, \(X_1, \ldots X_n \sim F\) where \(F\) is unknown. Then, we can
estimate (guess) \(F\) simply by assuming
that \(X_1, \ldots X_n\) consists all the possible values of \(F\) and in the correct proportions.
Then, our estimated distribution function \(\widehat{F}\) is given by

\begin{equation*}
\widehat{F(x)} = \widehat{P(X \leq x)} = \frac{1}{n}\sum_{i=1}^n I_{[x_i, \infty)}(x) \quad\text{where}\quad
I_{E}(x) = \begin{cases}1 \quad&\text{if }x\in E \\ 0\quad&\text{if } x \notin E\end{cases}
\end{equation*}

We call this our \emph{emperical distribution function}. Note that this
function is essentially a step function with steps of size \(1/n\) at each \(x_i\)
(if non-repeating; otherwise we have to include multiplicity).

Also, for each fixed \(x\), we can consider \(n\widehat{F(x)}\)
as a random variable. Then since \(I_{[x_i, \infty)}(x) = I_{(\infty, x]}(x_i)\)
is a Bernoulli RV with probability \(F(x)\), we get that \(b\widehat{F(x)}\)
is a binomial (sum of bernoulli) and hence,

\begin{equation*}
E[n\widehat{F(x)}] = nF(x)
\quad\text{and}\quad
Var[n\widehat{F(x)}] = nF(x)[1-F(x)]
\end{equation*}

This immediately implies that, pointwise, \(\widehat{F(x)}\)
is an unbiased estimator for \(F(x)\). Also, by using
\myautoref[{chebyshev's inequality}]{develop--math3278:convergence:page--index.adoc---chebyshevs-inequality},
we get that

\begin{equation*}
\lim_{n\to \infty} P(|\widehat{F(x)} - F(x)| > \varepsilon)
\leq
\lim_{n\to \infty} \frac{F(x)(1-F(x)}{\varepsilon^2 n} = 0
\end{equation*}

Therefore, we get that, pointwise, \(\widehat{F(x)}\) converges
in probability to \(F(x)\).

\begin{admonition-note}[{}]
This section is based off of the
\href{https://en.wikipedia.org/wiki/Empirical\_distribution\_function}{wikipedia article}
as well as the notes. We can also show convergence almost surely, but I'm
not too sure of how to do so.
\end{admonition-note}

However, the question remains as to whether to \(\widehat{F}\) converges to \(F\)
globally. The
\href{https://en.wikipedia.org/wiki/Glivenko\%E2\%80\%93Cantelli\_theorem}{Glivenko–Cantelli\_theorem}
asserts that we indeed have that \(\widehat{F}\) converges almost surely to \(F\) with
the \(l_\infty\) norm

\begin{equation*}
\|\widehat{F} - F\| = \sup_{x \in \mathbb{R}}|\widehat{F}(x) - F(x)|
\end{equation*}

The value of this norm is called the
\href{https://en.wikipedia.org/wiki/Kolmogorov\%E2\%80\%93Smirnov\_test\#Kolmogorov\%E2\%80\%93Smirnov\_statistic}{Kolmogorov–Smirnov\_statistic}.

\section{Kolmogorov–Smirnov test}
\label{develop--math3465:non-parametric:page--empirical.adoc---kolmogorovsmirnov-test}

\subsection{One Sample}
\label{develop--math3465:non-parametric:page--empirical.adoc---one-sample}

Suppose we have a sample from population \(F\) and we want
to test if \(F = F_0\). That is

\begin{equation*}
H_0: F = F_0
\quad\text{vs}\quad
H_1: F \neq F_0
\end{equation*}

After collecting the data, we estimate \(F\) by \(\widehat{F}\)
and compute the Kolmogorov–Smirnov statistic. Our eventual
aim is to reject \(H_0\) if our statistic is too large. However
the question remains how large? Obviously, this answer
depends on \(F\).

In \texttt{R}, we can do the following

\begin{listing}[{}]
# help(ks.test)
# x    - a vector of data values
# dist - either a string of the distribution or the distribution itself
# ...  - extra parameters are for the distribution specified
ks.test(x, dist, ..., alternative="two-sided")
\end{listing}

\subsection{Two Sample}
\label{develop--math3465:non-parametric:page--empirical.adoc---two-sample}

Suppose, instead, we have two samples from distributions \(F\) and \(G\)
(not necessarily of the same size). Then we may want to test

\begin{equation*}
H_0: F = G
\quad\text{vs}\quad
H_1: F \neq G
\end{equation*}

We again conduct the same procedure and compute

\begin{equation*}
D_{nm} = \|\widehat{F} - \widehat{G}\| = \sup_{x \in \mathbb{R}}|\widehat{F}(x) - \widehat{G}(x)|
\end{equation*}

However this time, we do not have a good way to determine how large our statistic should be.
.... regardless, we have this nice function from \texttt{R}

\begin{listing}[{}]
# help(ks.test)
# x - a vector of data values
# y - a vector of data values
ks.test(x, y, alternative="two.sided")
\end{listing}

\section{The Bootstrap}
\label{develop--math3465:non-parametric:page--empirical.adoc---the-bootstrap}

Suppose we now want to know about the distribution of a sample statistic.
For example, we might want to know the distribution of sample variance for
some fixed sample size, \(m\).
Well, since we do not know the distribution of \(X\), we can instead
utilize the empirical distribution as an estimator. Suppose that our initial
sample is of size \(n\). Then, a naive way of computing the distribution
of the population variance is by finding each of the \(n^m\) samples,
computing the sample variance then finally summarizing. This is
extremely computationally expensive unless we have some method
to simplify the computations.

Instead, we would take a large number of samples from our empirical distribution
and use these samples to estimate the distribution of the statistic of interest.
This method is called bootstrapping and we are resampling from our original sample.
Additionally, in a similar way to how our empirical distribution is a good estimate
of the population distribution, the bootstrapped empirical distribution
is a good estimate of the distribution of the statistic of interest.

\subsection{Implimentation in \texttt{R}}
\label{develop--math3465:non-parametric:page--empirical.adoc---implimentation-in-latex-backslashtexttt-latex-openbracer-latex-closebrace}

In \texttt{R}, we have the method \texttt{boot::boot}

\begin{listing}[{}]
library(boot)
# data      - data (I dont think the format matter too much)
# statistic - a function which expects data and a list of indicies
#              eg: statistic = function(data, i) {
#                    return mean(data[i])/var(data[i])
#                  }
# R         - number of samples
boot(data, statistic, R=<number>)
\end{listing}

Then the result contains the generated statistics in the \(t\) entry.

Alternative, we can utilize the \texttt{sample} method in a loop to generate
samples and these statistics. For example,

\begin{listing}[{}]
R = 10000

bootstrap = rep(0, R)

for (i in 1:R) {
  # Note that in this case `statistic` would take the sample directly
  bootstrap[i] = statistic(sample(data, length(data), replace=TRUE))
}
\end{listing}

\begin{admonition-tip}[{}]
It might be useful to initialize the random seed to a known
value using \texttt{set.seed} to ensure reproducibility.
\end{admonition-tip}
\end{document}
