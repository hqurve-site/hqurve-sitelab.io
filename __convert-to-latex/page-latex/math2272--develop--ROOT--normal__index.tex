\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Normal Subgroups}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

% Title omitted
Let \(G\) be a group. Then a subgroup \(H\) of
\(G\) is \emph{normal} (or \emph{invariant}) if

\begin{equation*}
\forall g \in G: gH = Hg
\end{equation*}

and we write \(H \Delta G\). That is, all the right cosets of
\(H\) are equal to the left cosets of \(H\).

\section{Normal subgroup criterion}
\label{develop--math2272:ROOT:page--normal/index.adoc---normal-subgroup-criterion}

Let \(G\) be a group and \(H \subseteq G\), then the
following statements are equivalent.

\begin{enumerate}[label=\arabic*)]
\item \(H\) is a normal subgroup of \(G\)
\item \(\forall g \in G: \forall h \in H: g^{-1} h g \in H\)
\end{enumerate}

\begin{example}[{Proof}]
This will be proven directly

\begin{equation*}
\begin{aligned}
        &H \Delta G\\
        &\iff \forall g \in G: gH = Hg\\
        &\iff \forall g \in G: gH \subseteq Hg \ \land\ Hg \subseteq gH\\
        &\iff \forall g \in G: \left[\forall h \in H: gh \in Hg\right] \ \land\ \left[\forall h \in H: hg \in gH\right]\\
        &\iff \forall g \in G: \left[\forall h \in H: ghg^{-1} \in H\right] \ \land\ \left[\forall h \in H: g^{-1}hg \in H\right]\quad\text{as proven in Cosets}\\
        &\iff \left[\forall g \in G: \forall h \in H: ghg^{-1} \in H\right] \ \land\ \left[\forall g \in G: \forall h \in H: g^{-1}hg \in H\right]\\
        &\iff \left[\forall g \in G: \forall h \in H: g^{-1}hg \in H\right] \ \land\ \left[\forall g \in G: \forall h \in H: g^{-1}hg \in H\right]\\
        &\iff \forall g \in G: \forall h \in H: g^{-1}hg \in H
    \end{aligned}
\end{equation*}

 ◻
\end{example}

\section{Abelian subgroups}
\label{develop--math2272:ROOT:page--normal/index.adoc---abelian-subgroups}

Let \(G\) be a group and \(H\) be an abelian
subgroup, then clearly, \(H\) is also normal.

\section{Quotient Groups}
\label{develop--math2272:ROOT:page--normal/index.adoc---quotient-groups}

Let \(G\) be a group and \(N \Delta G\). Then,
consider the following set

\begin{equation*}
G/N = \{gN: g \in G\}
\end{equation*}

as well as the binary operation \(\cdot\) on \(G/N\)
defined by

\begin{equation*}
Na \cdot Nb = N(ab) \quad\text{where } a, b\in G
\end{equation*}

Then, \(\cdot\) is well defined and \((G/N, \cdot)\)
forms a group. We call this group the \emph{quotient group} (or \emph{factor
group}) of \(G\) by \(N\).

\begin{example}[{Proof}]
Firstly, we will prove that \(\cdot\) is well defined. Let,
\(a_1N = a_2N \in G/N\) and \(b_1N = b_2N \in G/N\).
Note that \(a_1N = a_2N \implies a_1^{-1}a_2\in N\) and since
\(N \Delta G\),

\begin{equation*}
b_2^{-1}(a_1^{-1}a_2)b_2 \in N
\end{equation*}

Additionally, \(b_1N = b_2N \implies b_1^{-1}b_2 \in N\) and
by the closure of \(N\),

\begin{equation*}
(b_1^{-1}b_2)b_2^{-1}(a_1^{-1}a_2)b_2 = b_1^{-1}a_1^{-1} a_2b_2 = (a_1b_1)^{-1}a_2b_2 \in N
\end{equation*}

and hence \((a_1b_1)N = (a_2b_2)N\) and the operation is well
defined.

Now, onto group operations

\begin{itemize}
\item Consider \(Na, Nb \in G/N\) then \(ab \in G\) and
    \(Na\cdot Nb = N(ab) \in G/N\).
\item Consider \(Na, Nb, Nc \in  G/N\) then, by the associativity
    in \(G\) \(a(bc) = (ab)c\) and hence
    
    \begin{equation*}
    Na \cdot (Nb \cdot Nc) =  N(a(bc)) = N((ab)c) = (Na \cdot Nb) \cdot Nc
    \end{equation*}
\item Consider \(Na \in G/N\) and let \(e\) be the
    identity in \(G\), then
    
    \begin{equation*}
    Na \cdot Ne = N(ae) = Na = N(ea) = Ne \cdot Na
    \end{equation*}
    
    Quick observation: note that \(Ne = N\).
\item Consider \(Na \in G/n\) and notice that
    \(Na^{-1} \in G/n\) and
    
    \begin{equation*}
    Na \cdot Na^{-1} = N(aa^{-1}) = Ne = N(a^{-1}a) = Na^{-1} \cdot Na
    \end{equation*}
\end{itemize}

Therefore \(G/N\) is a group. Additionally, notice that all of
its properties were inherited from \(G\).

 ◻
\end{example}

\subsection{Nice theorem}
\label{develop--math2272:ROOT:page--normal/index.adoc---nice-theorem}

Let \(G\) ba a group with subgroup \(H\). Then if
\(|G:H| = 2\), \(H\Delta G\) and \(G/H\)
is a cyclic group with order \(2\).

\begin{example}[{Proof}]
Let the two cosets be \(H\) and \(Hg\) where
\(g \in G - H\). Then, consider an arbitrary element
\(a \in G\), we have two cases

\begin{itemize}
\item \(a \in H\), then
    \(a^{-1}h a \in H\ \forall h \in H\) by the closure of
    subgroups.
\item \(a \in Hg\), then \(Hg = Ha\). Then suppose BWOC,
    \(\exists h \in H: a^{-1}ha \in Ha\), the
    \(\exists h_1 \in H: a^{-1}ha = h_1a \implies a^{-1}=h_1h^{-1} \implies a = hh_1^{-1} \in H\).
    This is a contradiction and hence,
    
    \begin{equation*}
    \forall h \in H: a^{-1}ha \in H
    \end{equation*}
\end{itemize}

Then, \(H \Delta G\). Also, note that \((gH)^2 = H\)
otherwise, \(gH = H\) which is a contradiction, therefore
since \(|G/H| = 2\) and the order of \(gH\) is
\(2\), \(G/H\) is a cyclic group of order
\(2\).

 ◻
\end{example}
\end{document}
