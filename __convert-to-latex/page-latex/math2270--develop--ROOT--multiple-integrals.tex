\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Multiple Integrals}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

\def\bm#1{{\bf #1}}

% Title omitted
\begin{admonition-note}[{}]
In this section most basic concepts will be omitted unless they are
accompanied by a pretty proof.
\end{admonition-note}

\section{Construction}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---construction}

I don’t feel like going through this. The version which I would have put
here is the Riemann double sum. If we went through a different type of
integral (like lebesgue or so?) most definitely this place would have
been filled.

\begin{theorem}[{Union of regions}]
If \(f\) is integrable over two closed disjoint (except
perhaps at their boundaries ) regions \(R_1\) and
\(R_2\), then \(f\) is integrable over
\(R_1 \cup R_2\) and

\begin{equation*}
\iint_{R_1 \cup R_2} f(x,y) dA = \iint_{R_1} f(x,y) dA + \iint_{R_2} f(x,y ) dA
\end{equation*}
\end{theorem}

\begin{theorem}[{Fubini’s theorem}]
If \(f\) is continuous on the rectangular region
\(R = [a,b] \times [c,d]\), then

\begin{equation*}
\iint_R f(x, y) = \int_a^b \int_c^d f(x, y) dy dx = \int_c^d \int_a^b f(x,y) dx dy
\end{equation*}

This result also holds if \(f\) is discontinuous only on a
finite number of smooth (??) curves and both of the iterated integrals
exists.
\end{theorem}

\section{Type I and type II}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---type-i-and-type-ii}

There is really nothing of significance here since they are just two
views of the same thing, iterated integrals. Type I refers to when
region \(R\) is defined in a way such that the
'\(x\)' (or first) variable is between two constant bounds
while Type II refers to when the '\(y\)' (or second) variable
is between two constant bounds.

\section{Jacobian matrix}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---jacobian-matrix}

Given a transformation function
\(\bm{f}=(f_1, f_2, \ldots f_m): \mathbb{R}^n \rightarrow\mathbb{R}^m\),
the \emph{Jacobian matrix} is defined as

\begin{equation*}
\bm{J_f(x)} = \begin{bmatrix}
        \frac{\partial \bm{f}}{\partial x_1}
        &
        \ldots
        &
        \frac{\partial \bm{f}}{\partial x_n}
    \end{bmatrix}
    =
    \begin{bmatrix}
        \frac{\partial f_1}{\partial x_1} & \cdots & \frac{\partial f_1}{\partial x_m}\\
        \vdots & \ddots & \vdots \\
        \frac{\partial f_n}{\partial x_1} & \cdots & \frac{\partial f_n}{\partial x_m}
    \end{bmatrix}
\end{equation*}

and is sometimes subscripted as \(\bm{J}_{\bm{f}}\) Note that
this is closely related (basically identical) to the total differential.
However, the key difference is that a function does not need to be
differentiable for its Jacobian matrix to exist.

\subsection{Chain Rule}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---chain-rule}

If \(\bm{y} \in\mathbb{R}^m\) is a function of
\(\bm{u}\in\mathbb{R}^n\) which is in itself a function of
\(\bm{x}\in\mathbb{R}^k\) then

\begin{equation*}
\bm{J_{y\circ u}(x)} = \bm{J_y(u)}\bm{J_u(x)}
\end{equation*}

This is easy to verify by consider the \(ij\)th entry of
\(\bm{J_{y\cdot u}(x)}\)

\begin{equation*}
\frac{\partial y_i}{\partial x_j}\bm{J_{y\circ u}(x)}_{ij} = \sum_{a=1}^n \bm{J_y(u)}_{ia}\bm{J_u(x)}_{aj} = \sum_{a=1}^n \frac{\partial y_i}{\partial u_a}\frac{\partial u_a}{\partial x_j}
\end{equation*}

This result follows from the chain rule. Therefore, the chain rule for
the Jacobian reduces to matrix multiplication.

\subsection{Determinant}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---determinant}

If \(m=n\), the determinant of the jacobian is well defined
and is denoted by

\begin{equation*}
\det(J)
    = \frac{\partial \bm{f}}{\partial \bm{x}}
    =  \frac{\partial(f_1, f_2, \ldots, f_n)}{\partial(x_1, x_2, \ldots x_n)}
    =\begin{vmatrix}
        \frac{\partial f_1}{\partial x_1} & \cdots & \frac{\partial f_1}{\partial x_m}\\
        \vdots & \ddots & \vdots \\
        \frac{\partial f_n}{\partial x_1} & \cdots & \frac{\partial f_n}{\partial x_m}
    \end{vmatrix}
\end{equation*}

\subsection{Application to integration}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---application-to-integration}

In the case when \(m=n\) and \(f\) is invertible,
the transformation function \(f\) can be thought as a
transformation between two coordinate spaces. This concept is especially
useful for multiple integrals where the coordinate space used can
greatly affect the ease of doing integration. If the vector
\(\bm{x} = (x_1, \ldots, x_n)\) is a function of
\(\bm{u} = (u_1, \ldots, u_n)\) (exactly
\(f(\bm{u}) = \bm{x}\)) and we are integrating a function
\(F: \mathbb{R}^n \rightarrow \mathbb{R}\) over a region
\(R\) using the coordinate space associated
\(\bm{x}\). Then

\begin{equation*}
\int_R F(\bm{x}) d\bm{x} = \int_{R'} F(f(\bm{u})) |\det(J)| d\bm{u}
\end{equation*}

\section{Different coordinate systems}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---different-coordinate-systems}

\subsection{Cylindrical coordinates}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---cylindrical-coordinates}

Cylindrical coordinates \((r, \theta, z)\) are the projection
of polar coordinates along the z-axis. That is \(r\) and
\(\theta\) determine the \(x\) and \(y\)
coordinates using the polar system while \(z\) simply
determines the height of the plane. Then

\begin{equation*}
x = r\cos\theta
    ,\quad
    y = r\sin\theta
    ,\quad
    z=z
\end{equation*}

and hence the jacobian of transformation to cylindrical system is

\begin{equation*}
\begin{vmatrix}
        \cos\theta  & -r\sin\theta  & 0\\
        \sin\theta  & r\cos\theta   & 0\\
        0           & 0             & 1
    \end{vmatrix}
    =
    \begin{vmatrix}
        \cos\theta  & -r\sin\theta\\
        \sin\theta  & r\cos\theta
    \end{vmatrix}
    = r
\end{equation*}

\subsection{Spherical coordinates}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---spherical-coordinates}

Spherical coordinates \((\rho, \theta,  \phi)\) are defined as
follows.

\begin{itemize}
\item \(\rho\) determines the radius of the sphere which is being
    projected onto (\(\rho \geq 0\)).
\item \(\phi\) determines the angle from the positive
    \(z\)-axis. (\(0 \leq \phi < \pi\))
\item \(\theta\) determines the angle of rotation about the
    \(z\)-axis rotating from the positive \(x\)-axis
    toward the positive \(y\)-axis.
    (\(0 \leq \theta < 2\pi\)).
\end{itemize}

Then consider the circle determined by \(\rho\) and
\(\phi\). It has radius \(\rho\sin\phi\) and lies in
the plane \(z=\rho\cos\phi\). Therefore,

\begin{equation*}
x = \rho\sin\phi \cos\theta
    ,\quad
    y = \rho\sin\phi \sin\theta
    ,\quad
    z = \rho\cos\phi
\end{equation*}

and hence the jacobian of transformation is

\begin{equation*}
\begin{aligned}
    &\begin{vmatrix}
        \sin\phi\cos\theta & -\rho\sin\phi \sin\theta & \rho \cos\phi \cos\theta\\
        \sin\phi\sin\theta &  \rho\sin\phi \cos\theta & \rho \cos\phi \sin\theta\\
        \cos\phi           &  0                       & -\rho \sin \phi
    \end{vmatrix}\\
    &=
    \rho^2 \left[
        \cos\phi \begin{vmatrix}
            -\sin\phi \sin\theta & \cos\phi \cos\theta\\
             \sin\phi \cos\theta & \cos\phi \sin\theta
        \end{vmatrix}
        -
        \sin\phi \begin{vmatrix}
            \sin\phi \cos\theta & -\sin\phi \sin\theta\\
            \sin\phi \sin\theta &  \sin\phi \cos\theta
        \end{vmatrix}
     \right]\\
    &=
    \rho^2 \left[
        \sin\phi\cos^2\phi \begin{vmatrix}
            -\sin\theta & \cos\theta\\
             \cos\theta & \sin\theta
        \end{vmatrix}
        -
        \sin^3\phi \begin{vmatrix}
            \cos\theta & -\sin\theta\\
            \sin\theta &  \cos\theta
        \end{vmatrix}
     \right]\\
    &=\rho^2 \left[
        -\sin\phi\cos^2\phi
        -\sin^3\phi
     \right]\\
    &=-\rho^2\sin\phi\end{aligned}
\end{equation*}

\section{Applications}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---applications}

\subsection{Center of mass of a lamina with variable density}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---center-of-mass-of-a-lamina-with-variable-density}

The center of mass of a lamina occupying the region \(R\) and
having density function \(\rho(x,y)\) is
\((\bar x, \bar y)\) such that

\begin{equation*}
\bar{x} = \frac{1}{m}\iint_R x\rho(x,y)dA
    \quad
    \bar{y} = \frac{1}{m}\iint_R y\rho(x,y)dA
\end{equation*}

where the mass \(m\) is given by

\begin{equation*}
m = \iint_R \rho(x,y)dA
\end{equation*}

This can be easily proven by considering, \(\bar{x}\), the
point at which \(\iint_R (x-\bar{x})\rho(x,y)dA = 0\) and
similarly for \(\bar{y}\).

Note that this concept can be extended to more than two variables (ie
center of mass of 3D objects etc).

\subsection{Surface area}
\label{develop--math2270:ROOT:page--multiple-integrals.adoc---surface-area}

The area of the surface governed by \(z = f(x, y)\) such that
\((x, y) \in R\) and \(f_x\) and \(f_y\)
are both continuous is

\begin{equation*}
A = \iint_R \sqrt{[f_x(x,y)]^2 + [f_y(x,y)]^2 + 1}\; dA
    = \iint_R\sqrt{1
        + \left(\frac{\partial z}{\partial x}\right)^2
        + \left(\frac{\partial z}{\partial y}\right)^2
    }\;dA
\end{equation*}
\end{document}
