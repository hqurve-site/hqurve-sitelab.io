\documentclass{article}

% imports
\usepackage[margin=2cm]{geometry}
\usepackage{changepage}

\usepackage{embedall} % embed source files in output pdf

\usepackage{fontspec} % used for emoji command
\usepackage{pifont} % used to get dings (eg cross)

\usepackage{titlesec}

\usepackage{hyperref}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{cancel}%https://jansoehlke.com/2010/06/strikethrough-in-latex/
\usepackage{bm} % boldface math
\usepackage{xurl}
\usepackage{enumitem}
\usepackage{graphicx, float} % float package for H (here!)
\usepackage{subcaption}
\usepackage{adjustbox} % to autoscale tables
\usepackage{listings}
\usepackage{multicol}

\usepackage{quoting}
\usepackage{xparse} % used to have multiple optional arguments

\usepackage{parskip} % disable indent
% \setlength{\parindent}{0pt}

\usepackage{soul}

\usepackage{tabularray} % making tables simple
\newcommand\thicktableline{2pt}
\UseTblrLibrary{counter} %https://github.com/lvjr/tabularray/issues/231 for subfigure numbers to work
\UseTblrLibrary{varwidth} % to have stuff like itemize inside table

\usepackage{xcolor}
\usepackage{etoolbox}
\usepackage[most]{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{calc}

\setcounter{secnumdepth}{3} % chapter(0), section(1), subsection(2), subsubsection(3)

% custom commands
\newcommand\myautoref[2][]{#1 \ref{#2}}
\newcommand\strikethrough[1]{\texorpdfstring{\st{#1}}{#1}}
\newcommand\emoji[1]{% from Matthias Braun https://tex.stackexchange.com/questions/497403/how-to-use-noto-color-emoji-with-lualatex
  {\setmainfont{Noto Color Emoji}[Renderer=OpenType]{#1}}%
}
\newcommand\xmark{\ding{55}} % https://tex.stackexchange.com/questions/42619/xmark-that-complements-the-ams-checkmark/42620#42620
\newcommand\numberandlabel[1]{\stepcounter{equation}\tag{\theequation}\label{#1}}
\newcommand\discretefloatingtitle[1]{\paragraph*{#1}}

% custom environments
\newcounter{Example}[\ifdef{\thechapter}{chapter}{section}]
\NewDocumentEnvironment{example}{o}{%
  \stepcounter{Example}%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Example~\theExample}{Example~\theExample: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}
\NewDocumentEnvironment{sidebar}{o}{%
  \begin{tcolorbox}[%
    breakable, parbox=false,%
    fonttitle=\bfseries,%
    title=\ifstrempty{#1}{Sidebar}{Sidebar: #1},%
  ]\relax\ignorespaces%
}{%
  \end{tcolorbox}%
}

% Create environments for theorems
% They all share the same counter
\newcounter{theoremenv}[\ifdef{\thechapter}{chapter}{section}]
    \renewcommand\thetheoremenv{\ifdef{\thechapter}{\thechapter}{\thesection}.\arabic{theoremenv}}
% name, label
\newcommand\createtheoremenvironment[2]{%
  % Based on https://tex.stackexchange.com/questions/469771/how-do-i-get-a-definition-and-theorem-environment-like-these
  \NewDocumentEnvironment{#1}{o}{%
    \stepcounter{theoremenv}
    \begin{tcolorbox}[%
      empty, breakable, parbox=false, enhanced,
      sharp corners, boxrule=-1pt,
      left=0ex, right=0ex, top=0ex, bottom=0ex, boxsep=2ex,
      left skip = 0.2ex,
      before skip=2.5ex, after skip=2ex,
      colback=white,colframe=white, coltitle=black,colbacktitle=white,
      borderline west = {0.15em}{0pt}{black!40!white},
      overlay unbroken and last = {%
        \draw[color=black!40!white, line width=0.15em]($(frame.south west)$) -- ++(2em,0);
      },
      fonttitle=\bfseries,%
      title=\ifstrempty{##1}{#2~\thetheoremenv}{#2~\thetheoremenv: ##1},%
      attach boxed title to top left={xshift=-0.5em},
      boxed title style={left=0ex, bottom=0ex, borderline south={0.15em}{0pt}{black!40!white}},
    ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}
\createtheoremenvironment{theorem}{Theorem}
\createtheoremenvironment{lemma}{Lemma}
\createtheoremenvironment{corollary}{Corollary}
\createtheoremenvironment{proposition}{Proposition}
% \newenvironment{example}{Example:\begin{admonition-thought}}{\end{admonition-thought}}

% proof environment based off of
% https://tex.stackexchange.com/questions/346429/modified-proof-environment
\RenewDocumentEnvironment{proof}{o}{%
  \begin{tcolorbox}[breakable, blanker, frame hidden, interior hidden,%
      top=3pt,bottom=4pt,left=6pt, boxsep=0pt,%
      borderline west={0.1em}{0pt}{black!30!white},%
      parbox=false,% allow parskip to work
      ]\relax%
  \ifstrempty{#1}%
    {\textbf{Proof.}}%
    {\textbf{Proof.} (#1)}% if not empty
}{%
  \end{tcolorbox}%
}
\AtEndEnvironment{proof}{\null\hfill$\square$}% from https://tex.stackexchange.com/questions/576345/tcolorbox-create-a-proof-environnement-with-a-qed-symbol-at-the-very-end

\lstnewenvironment{literal}{}{} %TODO
\lstnewenvironment{listing}{}{} %TODO

%% quotations
% based off of https://tex.stackexchange.com/questions/391726/the-quotation-environment
% attribution, citetitle
\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\normalfont(#1)}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}
\NewDocumentEnvironment{custom-quotation}{o o}
{\begin{quoting}[indentfirst=true, leftmargin=1em,rightmargin=1em]\itshape%
}
{%
  \ifstrempty{#1}{}{%
    \bywhom{#1}%
  }%
  \ifstrempty{#2}{}{%
    \vspace{-0.5em}%
    \hfill\mbox{\normalfont{\color{darkgray}{\textit{\small{#2}}}}}%
  }%
  \end{quoting}
}


%% create admonitions
% name, label, background color
\newcommand\createnewadmonition[3]{
  \definecolor{admonition-#1-color}{rgb}{#3} % define color
  % from example 6 of https://ftp.rrze.uni-erlangen.de/ctan/macros/latex/contrib/mdframed/mdframed.pdf
  % (version 2013/07/01)
  \newenvironment{admonition-#1}[1][]{% one argument which is optional
    \begin{tcolorbox}[enhanced,%
      fonttitle=\bfseries,%
      attach boxed title to top left={yshift=-3pt,yshifttext=-1pt, xshift=-3pt},%
      boxed title style={colback=admonition-#1-color, boxrule=0pt},%
      title=\ifstrempty{##1}{#2}{#2: ##1},%
      boxrule=0.7pt,%
      colframe=black!30!white,colback=black!0.5!white,%
      parbox=false,% allow parskip to work
      ]\relax\ignorespaces%
  }{%
    \end{tcolorbox}%
  }
}% end def createnewadmonition

% for the colours, I used the colours in the ui then used https://observablehq.com/@prayerslayer/hex-to-latex-color-converter
\createnewadmonition{caution}{CAUTION}{0.62745,0.26275,0.61176}
\createnewadmonition{important}{IMPORTANT}{0.82745,0.18431,0.18431}
\createnewadmonition{note}{NOTE}{0.12941,0.49412,0.90588}
\createnewadmonition{tip}{TIP}{0.2549,0.68627,0.27451}
\createnewadmonition{warning}{WARNING}{0.88235,0.50588,0.07843}
% custom admonitions
\createnewadmonition{idea}{IDEA}{1,0.79216,0.00392}
\createnewadmonition{remark}{REMARK}{0.27843,0.65098,0.8}
\createnewadmonition{thought}{THOUGHT}{0.58431,0.45882,0.81961}
\createnewadmonition{todo}{TODO}{0.84706,0.05882,0.47451}


\title{Norms}
\def\asciidocsinglepage{}
\ifdef{\asciidocsinglepage}{}{
\newcommand{\sectionbreak}{\clearpage} % https://tex.stackexchange.com/questions/9497/start-new-page-with-each-section
}

\begin{document}
% declaremathoperator is only available in prelude ... which is no longer a necessary restriction (https://tex.stackexchange.com/questions/175783/why-must-declaremathoperator-be-in-the-preamble)
% redefining in the prelude does not appear to work
\def\DeclareMathOperator#1#2{\def#1{\operatorname{#2}}}

\maketitle

\tableofcontents

 \def\vec#1{\underline{#1}} \def\abrack#1{\left\langle{#1}\right\rangle} 
{} \renewcommand\Re{\mathrm{ Re}}
% Title omitted
Let \(V\) be an inner product space over field
\(\mathbb{K}\). Then, we define the \emph{norm}
to be a function \(\|\cdot \|: V\to \mathbb{R}^+\)
defined by

\begin{equation*}
\|\vec{v}\| = \sqrt{\abrack{\vec{v}, \vec{v}}}
\end{equation*}

Then, we have the following properties

\begin{itemize}
\item \(\|\vec{v}\| \geq 0\) where equality holds iff \(\vec{v} = \vec{0}\)
\item \(\|c\vec{v}\| = |c|\, \|\vec{v}\|\)
\item \(\|\vec{v} + \vec{w}\| \leq \|\vec{v}\| + \|\vec{w}\|\)
\item If \(\vec{v} \perp \vec{w}\) we get \(\|\vec{v} + \vec{w}\|^2 = \|\vec{v}\|^2 + \|\vec{w}\|^2\)
\end{itemize}

\begin{example}[{Proof}]
The first and second properties follow directly from the definition of the norm
and inner product. While for the third inequality

\begin{equation*}
\begin{aligned}
\abrack{\vec{v} + \vec{w}, \vec{v} + \vec{w}}
&= \abrack{\vec{v},\vec{v}} + \abrack{\vec{w},\vec{w}} + \abrack{\vec{v},\vec{w}} + \abrack{\vec{w}, \vec{v}}
\\&= \|\vec{v}\|^2 + \|\vec{w}\|^2 + 2\Re\abrack{\vec{v},\vec{w}}
\\&\leq \|\vec{v}\|^2 + \|\vec{w}\|^2 + 2|\abrack{\vec{v},\vec{w}}|
\\&\leq \|\vec{v}\|^2 + \|\vec{w}\|^2 + 2\|\vec{v}\|\,\|\vec{w}|
\\&= (\|\vec{v}\| + \|\vec{w}\|)^2
\end{aligned}
\end{equation*}

where equality holds if \(\vec{v}\) and \(\vec{w}\) are linearly dependent and
\(\Re\abrack{\vec{v},\vec{w}} = |\abrack{\vec{v},\vec{w}}|\). Now, if either \(\vec{v} = \vec{0}\)
or
\(\vec{w} = \vec{0}\), both conditions are satisfied. Otherwise, \(\vec{w} = k \vec{v}\) and hence

\begin{equation*}
k\|\vec{v}\| = \Re\abrack{\vec{v},\vec{w}} = |\abrack{\vec{v},\vec{w}}| = |k|\,\|\vec{v}\|
\end{equation*}

and hence \(k \in \mathbb{R}^*\).

Finally, for the third inequality, we see that if we substitute \(\vec{v} \perp \vec{w}\) into the
previously obtained inequality, we get the desired result.
\end{example}

\section{Nice Identities}
\label{develop--math3273:inner-product-space:page--norm.adoc---nice-identities}

\subsection{Identity 1}
\label{develop--math3273:inner-product-space:page--norm.adoc---identity-1}

Let \(\vec{v},\vec{w} \in V\) then

\begin{equation*}
\abrack{\vec{v},\vec{w}}
= \frac{1}{4} \left(
    \|\vec{v} + \vec{w}\|^2
    + i\|\vec{v} + i\vec{w}\|^2
    + i^2\|\vec{v} + i^2\vec{w}\|^2
    + i^3\|\vec{v} + i^3\vec{w}\|^2
\right)
\end{equation*}

Furthermore, if \(V\) is a real inner product space

\begin{equation*}
\abrack{\vec{v},\vec{w}}
= \frac{1}{4} \left(
    \|\vec{v} + \vec{w}\|^2
    - \|\vec{v} - \vec{w}\|^2
\right)
\end{equation*}

\begin{example}[{Proof for complex IPS}]
Notice

\begin{equation*}
\begin{aligned}
&\sum_{k=0}^3 i^k \|\vec{v} + i^k \vec{w}\|
\\&= \sum_{k=0}^3 i^k \abrack{\vec{v} + i^k \vec{w}, \vec{v} + i^k\vec{w}}
\\&= \sum_{k=0}^3 i^k \left(\|\vec{v}\|^2 + \|i^k\vec{w}\|^2 + \abrack{\vec{v}, i^k\vec{w}} + \abrack{i^k\vec{w}, \vec{v}} \right)
\\&= \sum_{k=0}^3 i^k \left(\|\vec{v}\|^2 + \|\vec{w}\|^2 + i^k\abrack{\vec{v}, \vec{w}} + \overline{i^k}\abrack{\vec{w}, \vec{v}} \right)
\\&= (\|\vec{v}\|^2 + \|\vec{w}\|^2)\sum_{k=0}^3 i^k + \sum_{k=0}^3 i^{2k}\abrack{\vec{v}, \vec{w}} + \sum_{k=0}^3 \abrack{\vec{w}, \vec{v}}
\\&= 0 + 0 + 0 + 4\abrack{\vec{w}, \vec{v}}
\end{aligned}
\end{equation*}

since \(i\) and \(i^2\) are roots of \(x^3+x^2+x+1\).
\end{example}

\begin{example}[{Proof for real IPS}]
Notice

\begin{equation*}
\|\vec{v} + \vec{w}\|^2 = \abrack{\vec{v} + \vec{w}, \vec{v} + \vec{w}} = \|\vec{v}\|^2 + \|\vec{w}\|^2 + 2\abrack{\vec{v}, \vec{w}}
\end{equation*}

and similarly

\begin{equation*}
\|\vec{v} - \vec{w}\|^2 = \abrack{\vec{v} - \vec{w}, \vec{v} - \vec{w}} = \|\vec{v}\|^2 + \|\vec{w}\|^2 - 2\abrack{\vec{v}, \vec{w}}
\end{equation*}

and by taking the difference, we get the desired result.
\end{example}

\section{Isometries}
\label{develop--math3273:inner-product-space:page--norm.adoc---isometries}

Let \(T \in \mathcal{L}(V)\), then \(T\) is an \emph{isometry} if

\begin{equation*}
\forall \vec{v} \in V: \|T(\vec{v})\| = \|\vec{v}\|
\end{equation*}

That is the norm is invariant to \(T\). Furthermore, the following three are equivalent

\begin{itemize}
\item \(T\) is an isometry
\item \(\forall \vec{v}, \vec{w} \in V: \abrack{T(\vec{v}), T(\vec{w})} = \abrack{\vec{v}, \vec{w}}\)
\item \(T^*T = I\) where \(T^*\) is the \myautoref[{adjoint}]{develop--math3273:inner-product-space:page--index.adoc---adjoint} of \(T\).
\end{itemize}

\begin{example}[{Proof}]
\begin{admonition-important}[{}]
This proof is essentially that which was presented in class
\end{admonition-important}

We would show that statements 1 and 2 are equivalent then show that 2 and 3 are equivalent.

\begin{example}[{1 and 2 are equivalent}]
Firstly note that statement 1 is a special case of statement 2. Next suppose that \(T\) is
an isometry, then

\begin{equation*}
\abrack{T(\vec{v}), T(\vec{w})}
= \frac{1}{4} \sum_{k=0}^3 i^k\|T(\vec{v}) + i^k T(\vec{w})\|^2
= \frac{1}{4} \sum_{k=0}^3 i^k\|T(\vec{v} + i^k\vec{w})\|^2
= \frac{1}{4} \sum_{k=0}^3 i^k\|\vec{v} + i^k\vec{w}\|^2
= \abrack{\vec{v},\vec{w}}
\end{equation*}

Note that the proof for real inner product spaces is very similar
\end{example}

\begin{example}[{2 and 3 are equivalent}]
Firstly, from the definition of the adjoint

\begin{equation*}
\forall \vec{v},\vec{w} \in V:
\abrack{T(\vec{v}), T(\vec{w})}
= \abrack{T^*(T\vec{v}), \vec{w}}
\end{equation*}

Now, suppose that statement 2 is true, we get that

\begin{equation*}
\forall \vec{v},\vec{w} \in V:
\abrack{\vec{v},\vec{w}}
= \abrack{T(\vec{v}), T(\vec{w})}
= \abrack{T^*(T\vec{v}), \vec{w}}
\end{equation*}

and hence \(\vec{v} = (T^*T)\vec{v}\) for all \(\vec{v} \in V\). Therefore \(T^*T = I\).
Conversely, if \(T^*T = I\)

\begin{equation*}
\forall \vec{v},\vec{w} \in V:
\abrack{T(\vec{v}), T(\vec{w})}
= \abrack{T^*(T\vec{v}), \vec{w}}
= \abrack{\vec{v},\vec{w}}
\end{equation*}
\end{example}
\end{example}

\subsection{Unitary and Orthogonal matricies}
\label{develop--math3273:inner-product-space:page--norm.adoc---unitary-and-orthogonal-matricies}

Let \(U\) be a complex \(n\times n\) matrix, then \(U\) is \emph{unitary} iff
\(U^*U = I\). Then, the following are equivalent

\begin{itemize}
\item \(U\) is unitary
\item \(U^T\) is unitary
\item \(\overline{U}\) is unitary
\item \(U^*\) is unitary
\item the columns of \(U\) form an orthonormal basis in \(\mathbb{C}^n\)
\item the rows of \(U\) form an orthonormal basis in \(\mathbb{C}^n\)
\end{itemize}

\begin{example}[{Proof}]
If \(U\) is unitary, then

\begin{equation*}
(U^T)^* U^T = \overline{U} U^T = \overline{\overline{\overline{U}U^T}} = \overline{U U^*} = \overline{I} = I
\end{equation*}

then, the first two are equivalent since \((U^T)^T = U\).

Next,

\begin{equation*}
(\overline{U})^* \overline{U} = U^T\overline{U} = I
\end{equation*}

since \(\overline{U}U^T = I\). Again, the first and second are equivalent since \(\overline{\overline{U}} = U\).
Additionally, since \(U^* = \overline{U^T}\), the first and fourth are also equivalent.

Next, let \(\vec{u}_1,\ldots \vec{u}_n\) be the columns of \(U\), then

\begin{equation*}
U^* U
=
\begin{bmatrix}\vec{u}_1^* \\ \vec{u}_2^* \\ \vdots \\ \vec{u}_n^*\end{bmatrix}
\begin{bmatrix}\vec{u}_1 & \vec{u}_2 & \cdots & \vec{u}_n\end{bmatrix}
= \begin{bmatrix}
\vec{u}_1^*\vec{u}_1 & \vec{u}_1^*\vec{u}_2 & \cdots & \vec{u}_1^*\vec{u}_n\\
\vec{u}_2^*\vec{u}_1 & \vec{u}_2^*\vec{u}_2 & \cdots & \vec{u}_2^*\vec{u}_n\\
\vdots & \vdots & \ddots & \vdots\\
\vec{u}_n^*\vec{u}_1 & \vec{u}_n^*\vec{u}_2 & \cdots & \vec{u}_n^*\vec{u}_n
\end{bmatrix}
= \begin{bmatrix}
\abrack{\vec{u}_1, \vec{u}_1} & \abrack{\vec{u}_1,\vec{u}_2} & \cdots & \abrack{\vec{u}_1, \vec{u}_n} \\
\abrack{\vec{u}_2, \vec{u}_1} & \abrack{\vec{u}_2,\vec{u}_2} & \cdots & \abrack{\vec{u}_2, \vec{u}_n} \\
\vdots & \vdots & \ddots & \vdots\\
\abrack{\vec{u}_n, \vec{u}_1} & \abrack{\vec{u}_n,\vec{u}_2} & \cdots & \abrack{\vec{u}_n, \vec{u}_n}
\end{bmatrix}
\end{equation*}

Then,

\begin{equation*}
U^* U = I \iff \abrack{\vec{u}_i, \vec{u}_j} = \delta_{ij} \iff \vec{u}_1, \ldots \vec{u}_n \text{ is an orthonormal basis}
\end{equation*}

Additionally, the 6th follows from a combination of 2 and 5.
\end{example}

Alternatively, if \(Q\) is a real matrix, then \(Q\) is \emph{orthogonal} iff \(Q^TQ = I\) and likewise,
the following are equivalent.

\begin{itemize}
\item \(Q\) is unitary
\item \(Q^T\) is unitary
\item the columns of \(Q\) form an orthonormal basis in \(\mathbb{R}^n\)
\item the rows of \(Q\) form an orthonormal basis in \(\mathbb{R}^n\)
\end{itemize}

The proof is basically identical to that presented for complex matrices.

Finally, if \(U\) and \(V\) are unitary \(n\times n\) matrices, and \(W\) is an \(m\times m\) unitary matrix,

\begin{itemize}
\item \(UV\) is unitary
\item \(U\oplus W = \begin{pmatrix}U & 0 \\ 0 & W\end{pmatrix}\) is unitary
\end{itemize}

Both of the above facts can be shown directly from the definition. Additionally, if \(U\), \(V\) and \(W\) were
instead orthogonal, we get similar results.
\end{document}
